#field inches_reference
#motor_error         0.0
#light_sensor_error  0.0
#gyro_sensor_error   0.0

move_power = 100
turn_power = 20

#
# do a square
#
robot_position      1.0 1.0 270

move_inches          4  move_power
turn_degrees_sensor 90  turn_power
move_inches          4  move_power
turn_degrees_sensor 90  turn_power
move_inches          4  move_power
turn_degrees_sensor 90  turn_power
move_inches          4  move_power

#
# do a square
#
robot_position      10.0 1.0 270

move_inches          4  move_power
turn_degrees        90  turn_power
move_inches          4  move_power
turn_degrees        90  turn_power
move_inches          4  move_power
turn_degrees        90  turn_power
move_inches          4  move_power

#
# do a rounded square
#
robot_position      3.0 6.0 270

move_inches           2  move_power
pivot_degrees        90  turn_power
move_inches           2  move_power
pivot_degrees        90  turn_power
move_inches           2  move_power
pivot_degrees        90  turn_power
move_inches           2  move_power
pivot_degrees        90  turn_power

#
# do a rounded square
#
robot_position      13.0 6.0 270

move_inches            2  move_power
pivot_degrees_sensor  90  turn_power
move_inches            2  move_power
pivot_degrees_sensor  90  turn_power
move_inches            2  move_power
pivot_degrees_sensor  90  turn_power
move_inches            2  move_power
pivot_degrees_sensor  90  turn_power

