#field cargo2021

follow_distance = 60.0
follow_power    = 60.0

robot_position 15.0 30.0 225.0
follow_line follow_distance follow_power 1 right

robot_position 15.0 30.0 225.0
follow_line follow_distance follow_power 1 left

robot_position 15.0 30.0 225.0
follow_line follow_distance follow_power 2 right

robot_position 15.0 30.0 225.0
follow_line follow_distance follow_power 2 left
