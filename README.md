### How do I get set up? ###

Recommend using Visual Studio Code // clone the repository and then run from there -- main class to run is racetrack.gui.RT.

### How do I format files? ###

#### Reserved Header Field Names ####

The following header fields are reserved and should only be used as the application expects:

- timestamp:  timestamp of the record
- beg:  same as timestamp
- timestamp_end:  ending timestamp of the record
- end:  same as timestamp_end
- tags:  special field for encoding multiple values that occur infrequently or have multiple values associated with the record

Note:  Please do not use pipes or other special characters in the header fields.

#### Scalar versus Categorical Columns ####

Scalar fields (integers that should be added arithmetically) are defined by the capitalization of the all of the characters in the header field.  Scalar elements are added together by addition operations.

Categorical fields are indicated by a header name that includes at least one lowercase letter.  Categorical fields are added together by set operations.

#### URL Encoding ####

Individual fields that contain special characters (i.e., commas and percent symbols) should encoded using URL encoding.  In this scheme, any special characters are represented by a percent symbol followed by the two-character hex value for that special character.

For unicode characters, please use a percent symbol followed by a 'u' followed by the four digit hex value for the unicode character.

#### How should timestamps be formatted? ####

Timestamps should be formatted as follows:

- 2014-12-16 14:50:01.452
- 2014-12-16 14:50:01
- 2014-12-16 14
- 2014-12-16

It is preferable to have all time stamps in GMT.

#### What is the tag field format? ####

A single tag entry can be composed of multiple tags.  Each tag should be separated by a pipe.  There are three types of supported tags:

- Simple:  a string without a pipe, equal symbol, or double colons
- Type-value:  color=blue
- Hierarchical:  highest::medium::lowest

### How does... ###

#### How does the expression field work? ####

Expressions allow records to be sorted by a text filter.  Example filters include:

- sip in {10.0.0.1,10.0.0.2} // keeps records where sip is either of the values in the list
- dpt = 80 // keeps records with dpt equal to 80
- dpt = 80 or dpt = 443 // keeps records with dpt equal to 80 or 443

### Known Issues and Limitations ###

#### Too Many To Name... ####
