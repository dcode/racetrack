/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;

import java.awt.geom.Point2D;

import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;

import javax.swing.JComponent;
import javax.swing.JFrame;

import racetrack.visualization.RTColorManager;

/**
 * YetAnotherSpringLayoutInstrumented ... another attempt to make a spring layout.  The IncrementalArrangement 
 * implementation has something wrong with it... i.e., there are parts that don't get a correct
 * layout.
 */
public class YetAnotherSpringLayoutInstrumented {
  /**
   * Graph to apply layout to
   */
  UniGraph g;

  /**
   * Distance function for computing distances between nodes
   */
  DistFunc dist;

  /**
   * Selection ... if set and not empty, only these nodes will be modified
   */
  Set<String> selection;

  /**
   * Map from the node to its xy coordinate
   */
  Map<String,Point2D> world_map;

  /**
   * All of the nodes 
   */
  Set<String> all_nodes;

  /**
   * Constructor
   *
   *@param g0          graph to layout ... assumes a graph that is connected
   *@param dist0       distance function -- if null, will be calculated
   *@param selection0  selected nodes -- if not null/not empty, these are the nodes that will be modified
   *@param world_map0  node to xy coordinates -- will be modified by class
   */
  public YetAnotherSpringLayoutInstrumented(UniGraph            g0, 
                                DistFunc            dist0,
                                Set<String>         selection0,
                                Map<String,Point2D> world_map0) {
    // Copy the params over
    this.g         = g0;
    this.selection = selection0;
    this.world_map = world_map0;

    // Create the distance function (if it's null)
    if (dist0 == null) { 
      System.err.println("Calculating node distance function -- worst case O(n^3)...");
      dist = new DSSSPPDistFunc(g); 
      System.err.println("  Done!");
    } else this.dist = dist0;

    // Other state variables
    this.all_nodes  = new HashSet<String>();
    for (int i=0;i<g.getNumberOfEntities();i++) all_nodes.add(g.getEntityDescription(i));
    this.mu = 1.0 / all_nodes.size();

    // Perform barycentric placement on all 2+ degree nodes (if graph large enough)
    // ... don't if there's a selection...
    if ((selection == null || selection.size() == 0) && init_with_bary && all_nodes.size() > 12) {
      String landmarks[] = selectLandmarks();
      if (landmarks != null && landmarks.length == 3) {
        world_map.put(landmarks[0], new Point2D.Double(0.0, 0.0));
        double d0_1 = dist.distance(landmarks[0],landmarks[1]); world_map.put(landmarks[1], new Point2D.Double(d0_1, 0.0));
        double d0_2 = dist.distance(landmarks[0],landmarks[2]); world_map.put(landmarks[2], new Point2D.Double(0.0, d0_2));
        Iterator<String> it = all_nodes.iterator(); while (it.hasNext()) {
          barycentricNodePlacement(it.next(), landmarks);
        }
      }
    }
  }

  /**
   * Initialize the placement with barycentric algorithm
   */
  private boolean init_with_bary = true;

  /**
   * Get the calculated distance function.
   *
   *@return distance function
   */
  public DistFunc distFunc() { return dist; }

  /**
   * Global number of iterations performed
   */
  private int global_iteration_step = 0;

  /**
   * Iterate the layout the number of steps to take.  Setting up a single step takes
   * resources... this provides the ability to save computations by doing a specified
   * number of steps.
   */
  public double iterateLayout(int steps_to_take) {
    long           t0_setup     = System.currentTimeMillis();
    Thread         threads[]    = new Thread[16];
    WorkerRunnable runnables[] = new WorkerRunnable[threads.length];

    // Split the nodes into sets for the threads 
    Map<Integer,Set<String>> split   = new HashMap<Integer,Set<String>>();
    Set<String>              matches = new HashSet<String>(); // Nodes to apply the layout (intersection of selection with nodes)
    for (int i=0;i<threads.length;i++) split.put(i,new HashSet<String>());
    if (selection != null && selection.size() > 0) {
      for (int i=0;i<g.getNumberOfEntities();i++) 
        if (selection.contains(g.getEntityDescription(i))) 
          matches.add(g.getEntityDescription(i));
    } 
    if (matches.size() == 0) {
      for (int i=0;i<g.getNumberOfEntities();i++)
        matches.add(g.getEntityDescription(i));
    }

    // Now put the resulting set into the split map
    Iterator<String> it = matches.iterator(); int split_i = 0; while (it.hasNext()) {
      split.get(split_i).add(it.next());
      split_i = (split_i + 1) % threads.length;
    }

    // Create the runnables
    for (int i=0;i<runnables.length;i++) runnables[i] = new WorkerRunnable(split.get(i), all_nodes);
    long t1_setup = System.currentTimeMillis();
    synchronized (performance_mutex) { setup_ms += (t1_setup - t0_setup); }

    // Run the specified number of steps
    for (int step=0;step<steps_to_take;step++) {
      global_iteration_step++;

      //
      // First phase is to calculate the adjustment
      long t0_adj = System.currentTimeMillis();
      for (int i=0;i<threads.length;i++) { threads[i] = new Thread(runnables[i]); }
      for (int i=0;i<threads.length;i++) { runnables[i].setCalculatePhase(); threads[i].start(); }
      // -- Join threads together
      for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }
      long t1_adj = System.currentTimeMillis();

      //
      // Second phase is to apply the adjustment
      long t0_apply = System.currentTimeMillis();
      for (int i=0;i<threads.length;i++) { threads[i] = new Thread(runnables[i]); }
      for (int i=0;i<threads.length;i++) { runnables[i].setApplyPhase();     threads[i].start(); }
      // -- Join threads together
      for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }
      long t1_apply = System.currentTimeMillis();

      synchronized (performance_mutex) {
        adjust_ms   += (t1_adj   - t0_adj);
        apply_ms    += (t1_apply - t0_apply);
      }
    }

    return averageVelocity(runnables);
  }

  /**
   * Performance metrics
   */
  Object performance_mutex = new Object();
  long   setup_ms          = 0L;
  long   apply_ms          = 0L;
  long   adjust_ms         = 0L;

  /** 
   * Print out performance information about the running time.
   */
  public void printPerformanceInformation(PrintStream out) {
    out.println("YetAnotherSpringLayoutInstrumented(): setup=" + setup_ms + " / adjust=" + adjust_ms + " / apply=" + apply_ms);
  }

  /**
   * Calculate the average velocity (of node movement) across the threads.
   */
  private double averageVelocity(WorkerRunnable runnables[]) {
    double velocity_sum = 0.0;
    for (int i=0;i<runnables.length;i++) velocity_sum += runnables[i].getAverageVelocity();
    return velocity_sum/runnables.length;
  }

  /**
   * Stress recording
   */
  Map<String,Map<Integer,Double>> stress_rec = new HashMap<String,Map<Integer,Double>>(),

  /**
   * Velocity recording
   */
                                  vel_rec    = new HashMap<String,Map<Integer,Double>>();

  /**
   * Stress and Velocity Plot
   */
  public BufferedImage stressAndVelocityPlot() {
    int graph_w = global_iteration_step, graph_h = 400, ins = 8; Iterator<String> its;

    // Find the maxes
    double max_stress = 1.0, max_vel = 1.0;

    its = stress_rec.keySet().iterator(); while (its.hasNext()) {
      String skey = its.next(); Iterator<Integer> iti = stress_rec.get(skey).keySet().iterator(); while (iti.hasNext()) {
        int ikey = iti.next(); double value = stress_rec.get(skey).get(ikey); if (value > max_stress) max_stress = value;
    } }

    its = vel_rec.keySet().iterator(); while (its.hasNext()) {
      String skey = its.next(); Iterator<Integer> iti = vel_rec.get(skey).keySet().iterator(); while (iti.hasNext()) {
        int ikey = iti.next(); double value = vel_rec.get(skey).get(ikey); if (value > max_vel) max_vel = value;
    } }

    // Make the plots
    BufferedImage bi  = new BufferedImage(graph_w + 2*ins, 2*graph_h + 3*ins, BufferedImage.TYPE_INT_RGB);
    Graphics2D    g2d = (Graphics2D) bi.getGraphics(); g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    g2d.setColor(RTColorManager.getColor("background","default")); g2d.fillRect(0,0,bi.getWidth(),bi.getHeight());
    g2d.setColor(RTColorManager.getColor("axis","major"));

    // Origins of each graph
    int x0_s = ins-1, y0_s = ins+graph_h+1;
    int x0_v = ins-1, y0_v = ins*2+graph_h*2+1;
    
    // Axis for each graph
    g2d.drawLine(x0_s, y0_s - graph_h, x0_s, y0_s); g2d.drawLine(x0_s, y0_s, x0_s + graph_w, y0_s);
    g2d.drawLine(x0_v, y0_v - graph_h, x0_v, y0_v); g2d.drawLine(x0_v, y0_v, x0_v + graph_w, y0_v);

    // Stress graph
    its = stress_rec.keySet().iterator(); while (its.hasNext()) {
      String skey = its.next(); g2d.setColor(RTColorManager.getColor(skey)); for (int x=1;x<global_iteration_step-2;x++) {
        double y0  = stress_rec.get(skey).get(x), 
               y1  = stress_rec.get(skey).get(x+1);
        int    sy0 = (int) (y0_s - graph_h*(y0/max_stress)),
               sy1 = (int) (y0_s - graph_h*(y1/max_stress));

        g2d.drawLine(x0_s + x, sy0, x0_s + x + 1, sy1);
      }
    }

    // Velocity graph
    its = vel_rec.keySet().iterator(); while (its.hasNext()) {
      String skey = its.next(); g2d.setColor(RTColorManager.getColor(skey)); for (int x=1;x<global_iteration_step-2;x++) {
        double y0  = vel_rec.get(skey).get(x), 
               y1  = vel_rec.get(skey).get(x+1);
        int    sy0 = (int) (y0_v - graph_h*(y0/max_vel)),
               sy1 = (int) (y0_v - graph_h*(y1/max_vel));

        g2d.drawLine(x0_v + x, sy0, x0_v + x + 1, sy1);
      }
    }

    g2d.dispose();
    return bi;
  }

  /**
   * Phase of the algorithm
   */
  enum Phase { calculate, adjust };

  /**
   * Worker runnable to calculate and apply adjustments
   */
  class WorkerRunnable implements Runnable {
    /**
     * Nodes to adjust
     */
    Set<String> nodes,

    /**
     * Compare to these nodes (all == standard spring layout)
     */
                compare_to;

    /**
     * Phase of the operation
     */
    Phase phase = Phase.calculate;

    /**
     * Constructor
     */
    public WorkerRunnable(Set<String> nodes0, Set<String> compare_to0) { 
      this.nodes      = nodes0; 
      this.compare_to = compare_to0;
    }

    /**
     * Set the phase to calculate the adjustment for the nodes
     */
    public void setCalculatePhase() { stress_sum = 0.0;  stress_samples = 0;  phase = Phase.calculate; }

    /**
     * Set the phase to apply the adjustment for the node
     */
    public void setApplyPhase()     { vel_sum = 0.0;     vel_samples = 0;     phase = Phase.adjust; }

    /**
     * X adjustment values per node
     */
    Map<String,Double> x_adj = new HashMap<String,Double>(),

    /**
     * Y adjustment values per node
     */
                       y_adj = new HashMap<String,Double>();

    /**
     * Velocity sum
     */
    double vel_sum = 0.0,

    /**
     * Stress sum
     */
           stress_sum = 0.0;

    /**
     * Velocity samples
     */
    int vel_samples = 0,

    /**
     * Stress samples
     */
        stress_samples = 0;

    /**
     * Perform the phase operation
     */
    public void run() {
      Iterator<String> it = nodes.iterator(); while (it.hasNext()) {
        String node = it.next();

        //
        // Calculate Adjustment
        //
        if        (phase == Phase.calculate) {
          double stress = calculateNodeAdjustment(node, x_adj, y_adj, compare_to); 
          stress_sum += stress;
          stress_samples++;

          if (stress_rec.containsKey(node) == false) stress_rec.put(node, new HashMap<Integer,Double>());
          stress_rec.get(node).put(global_iteration_step, stress);

        //
        // Apply Adjustment
        //
        } else if (phase == Phase.adjust) {
          if (x_adj.containsKey(node)) {
            Point2D orig = world_map.get(node);
            world_map.put(node, new Point2D.Double(orig.getX() + x_adj.get(node), 
                                                   orig.getY() + y_adj.get(node)));
            double vel = Math.sqrt(x_adj.get(node)*x_adj.get(node) + y_adj.get(node)*y_adj.get(node));
            vel_sum += vel;
            vel_samples++;

            if (vel_rec.containsKey(node) == false) vel_rec.put(node, new HashMap<Integer,Double>());
            vel_rec.get(node).put(global_iteration_step, vel);

          }
        } else throw new RuntimeException("Unknown Phase \"" + phase + "\"...");
      }
    }

    /**
     * Return the average velocity of node movement.
     */
    public double getAverageVelocity() {
      if (vel_samples == 0) vel_samples = 1;
      return vel_sum / vel_samples;
    }
  }

  /**
   * Spring proportionality
   * - k == 0.0 // absolute stress
   * - k == 1.0 // semi-proportional stress
   * - k == 2.0 // proportional stress
   */
  final double k = 0.0;

  /**
   * Weighting for each iteration
   */
  double mu = 1.0;

  /**
   * Determine the adjustment for a single node and store the deltas in the supplied maps.
   *
   * Description       | Nodes  | setup |   adjust |   apply
   * Literally Nothing | 5210   |   769 |   16589  |   15793
   *
   *@param node   node to adjust
   *@param x_adj  x coordinate adjustment map
   *@param y_adj  y coordinate adjustment map
   *
   *@return stress of the system
   */
  protected double calculateNodeAdjustment(String               node_i, 
                                           Map<String,Double>   x_adj, 
                                           Map<String,Double>   y_adj, 
                                           Set<String>          compare_to) {
    Point2D pt_i = world_map.get(node_i); double sum_dx = 0.0, sum_dy = 0.0, stress_sum = 0.0;
    Iterator<String> it_other = compare_to.iterator(); while (it_other.hasNext()) {
      String  node_j = it_other.next();        if (node_j.equals(node_i)) continue;
      Point2D pt_j   = world_map.get(node_j);
       double t      = dist.distance(node_i,node_j);
       double dx     = pt_i.getX() - pt_j.getX(), dy     = pt_i.getY() - pt_j.getY();
       double dx2    = dx*dx,                     dy2    = dy*dy;
       double d      = Math.sqrt(dx2 + dy2);
       double exp    = Math.pow(t,k);
       if (d   < 0.001) d   = 0.001; // Prevent NaN
       if (exp < 0.001) exp = 0.001; // Prevent NaN
       sum_dx     += (2*dx*(1.0 - t/d))/exp;
       sum_dy     += (2*dy*(1.0 - t/d))/exp;
       stress_sum += (t - d) * (t - d);
    }
    x_adj.put(node_i, -mu * sum_dx);
    y_adj.put(node_i, -mu * sum_dy);

    return stress_sum/compare_to.size();
  }

  /**
   * Selects three landmark nodes ... furthest nodes aparts (non-one-degrees).  For barycentric initialization.
   */
  private String[] selectLandmarks() {
    LandmarkSelection landmark_selection = new LandmarkSelection(g, true, 1.0, 3);
    Set<String> set = landmark_selection.landmarks(); 
    if (set.size() >= 3) {
      String lms[] = new String[3]; int i = 0;
      Iterator<String> it = set.iterator(); while (it.hasNext()) lms[i++] = it.next();
      return lms;
    } else return null;
  }

  /**
   * Place a single node in a barycentric strategy.
   */
  private void barycentricNodePlacement(String node, String landmarks[]) {
    if (node.equals(landmarks[0]) || node.equals(landmarks[1]) || node.equals(landmarks[2])) return;

    double  d0     = dist.distance(node, landmarks[0]),
            d1     = dist.distance(node, landmarks[1]),
            d2     = dist.distance(node, landmarks[2]);

    Point2D pt0    = world_map.get(landmarks[0]), 
            pt1    = world_map.get(landmarks[1]), 
            pt2    = world_map.get(landmarks[2]);

    double  d_tot  = d0 + d1 + d2; if (d_tot == 0.0) d_tot = 1;

    double  perc0  = (d_tot - d0)/d_tot,
            perc1  = (d_tot - d1)/d_tot,
            perc2  = (d_tot - d2)/d_tot;
    
    // Scale the point based on the percents of the landmark points... add a little randomness so that the one degrees
    // aren't co-located...
    world_map.put(node, new Point2D.Double(perc0 * pt0.getX() + perc1 * pt1.getX() + perc2 * pt2.getX() + (0.05 - 0.1*Math.random()),
                                           perc0 * pt0.getY() + perc1 * pt1.getY() + perc2 * pt2.getY() + (0.05 - 0.1*Math.random())));
  }

  // For testing modifications against all of the graph factor instances

  /**
   *
   */
  public static void main(String args[]) { LayoutTestsInstrumented lt = new LayoutTestsInstrumented(); lt.setVisible(true); lt.run(); }
}

/**
 * Frame to render all of the graph factory graphs as they are processed iteratively with this layout.
 */
class LayoutTestsInstrumented extends JFrame {

  /**
   *
   */
  List<LayoutTestComponent> comps_list = new ArrayList<LayoutTestComponent>();

  GraphFactory.Type graph_types[] = { GraphFactory.Type.RING,
                                      GraphFactory.Type.BINARYTREE,
                                      GraphFactory.Type.QUADTREE,
                                      GraphFactory.Type.RING_SPOKES,
                                      GraphFactory.Type.MESH };

  /**
   *
   */
  public LayoutTestsInstrumented() {
    getContentPane().setLayout(new GridLayout(3,3));

    for (int i=0;i<graph_types.length;i++) {
      comps_list.add(new LayoutTestComponent(GraphFactory.createInstance(graph_types[i], null))); 
      getContentPane().add(comps_list.get(comps_list.size()-1));
    }

    pack(); setSize(1000,1000);
  }

  /**
   *
   */
  public void run() {
    for (int steps=0;steps<800;steps++) for (int i=0;i<comps_list.size();i++) {
      comps_list.get(i).yasl.iterateLayout(1);
      comps_list.get(i).repaint();
    }

    for (int i=0;i<comps_list.size();i++) {
      try { 
        ImageIO.write(comps_list.get(i).yasl.stressAndVelocityPlot(), "PNG", new FileOutputStream(new File(graph_types[i] + "_stress_vel_plot.png")));
      } catch (IOException ioe) {
        System.err.println("IOException: " + ioe);
        ioe.printStackTrace(System.err);
      }
    }

    System.exit(0);
  }

  /**
   *
   */
  class LayoutTestComponent extends JComponent {
    UniGraph               graph; 
    YetAnotherSpringLayoutInstrumented yasl;
    Map<String,Point2D>    world_map;
    public LayoutTestComponent(MyGraph g) {
      graph = new UniGraph(g);

      world_map = new HashMap<String,Point2D>();
      for (int i=0;i<graph.getNumberOfEntities();i++) world_map.put(graph.getEntityDescription(i), new Point2D.Double(Math.random(),Math.random()));

      yasl = new YetAnotherSpringLayoutInstrumented(graph, null, null, world_map);

      Dimension dimension = new Dimension(264,264); setMinimumSize(dimension);
    }
    public void paintComponent(Graphics g) { g.drawImage(GraphUtils.render(graph,world_map),0,0,null); }
  }
}

