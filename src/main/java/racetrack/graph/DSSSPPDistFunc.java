/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * DijkstraSingleSourceShortestPath parallel distance calculations.  Optimized to just produce
 * the shortest distances (without remembering paths).  Removes one degrees at the start to
 * shorten the runtime.  Splits the work across multiple threads.
 *
 * 3185 Node Graph
 *
 * OptDistFunc      DSSSPPDistFunc (This Impl)
 * 17.522 secs      5.694 secs
 * 13.498 secs      5.264 secs
 * 13.569 secs      6.497 secs
 *
 */
class DSSSPPDistFunc implements DistFunc {
  /**
   *
   */
  public DSSSPPDistFunc(UniGraph g) {
    // Remove one degress 
    Set<String> keep = new HashSet<String>(), // Nodes with more than one neighbor
                hubs = new HashSet<String>(); // Nodes that have many (only) 1-degree neighbors
    for (int node_i=0;node_i<g.getNumberOfEntities();node_i++) {
      String node = g.getEntityDescription(node_i); entities.add(node);
      if (g.getNumberOfNeighbors(node_i) > 1) { // First check -- does it one (or less) neighbors... 
        for (int i=0;i<g.getNumberOfNeighbors(node_i);i++) {
          int nbor_i = g.getNeighbor(node_i, i);
          if (g.getNumberOfNeighbors(nbor_i) > 1) { keep.add(node); break; } // It's a keeper
        }
        if (keep.contains(node) == false) hubs.add(node); // hubs
      }
    }

    // System.err.println("keep = " + keep); System.err.println("hubs = " + hubs); // DEBUG

    // Create the graph of only the keepers and separate into connected components
    UniGraph          g_2p       = GraphUtils.createGraphWithOnly(g, keep);
    Set<Set<String>>  g_2p_comps = GraphUtils.connectedComponents(g_2p);

    // For each connected component, run the Dijkstra algorithm (assuming that there are enough nodes in the graph)
    Iterator<Set<String>> it_comps = g_2p_comps.iterator(); while (it_comps.hasNext()) {
      Set<String> comp = it_comps.next(); UniGraph g_comp = GraphUtils.createGraphWithOnly(g_2p, comp);
      // System.err.println("comp = " + comp); // DEBUG
      if (comp.size() > 32) { // Parallelize it...
        // Create the work queue
        thread_queue = new LinkedList<String>();
        thread_queue.addAll(comp);

        // Make the threads and start them
        Thread threads[] = new Thread[16];
        for (int i=0;i<threads.length;i++) {
          threads[i] = new Thread(new WorkerRunnable(g_comp));
          threads[i].start();
        }

        // Join the threads
        for (int i=0;i<threads.length;i++) try { threads[i].join(); } catch (InterruptedException ie) { }

      } else {                // Just do it serially...
        Iterator<String> it = comp.iterator(); while (it.hasNext()) {
          String node = it.next();
          distances.put(node, dijkstraSingleSourceShortestPath(g_comp, node));
        }
      }
    }

    // Add the one degrees back in and compute their distance ... special case of hub and spoke...
    // ... probably a good test would be to figure out if the one degrees should never be added back in...
    // ... then the distances method could just figure it out each time... unsure space/time tradeoff
    Set<String> ones = new HashSet<String>(); ones.addAll(entities); ones.removeAll(keep);
    Iterator<String> it = ones.iterator(); while (it.hasNext()) {
      String node = it.next(); int node_i = g.getEntityIndex(node); distances.put(node, new HashMap<String,Integer>());
      if (g.getNumberOfNeighbors(node_i) != 0) { // No neighbors, no distances...

        //
        // Hub
        //
        if (hubs.contains(node)) { 
          if (distances.containsKey(node) == false) distances.put(node, new HashMap<String,Integer>());
          distances.get(node).put(node, 0);
          for (int i=0;i<g.getNumberOfNeighbors(node_i);i++) {
            int nbor_i = g.getNeighbor(node_i, i); String nbor = g.getEntityDescription(nbor_i); if (distances.containsKey(nbor) == false) distances.put(nbor, new HashMap<String,Integer>());
            distances.get(nbor).put(node, 1);
            distances.get(node).put(nbor, 1);
            for (int j=0;j<g.getNumberOfNeighbors(node_i);j++) {
              int nbor_j = g.getNeighbor(node_i, j); String nbor_other = g.getEntityDescription(nbor_j); if (distances.containsKey(nbor_other) == false) distances.put(nbor_other, new HashMap<String,Integer>());
              if (nbor.equals(nbor_other) == false) {
                distances.get(nbor).put(nbor_other, 2);
                distances.get(nbor_other).put(nbor, 2);
              } else distances.get(nbor).put(nbor, 0);
            }
          }

        //
        // Not a hub
        //
        } else {
          int nbor_i = g.getNeighbor(node_i,0); String nbor = g.getEntityDescription(nbor_i); if (distances.containsKey(nbor)) {
            Set<String> copy = new HashSet<String>(); copy.addAll(distances.get(nbor).keySet());
            Iterator<String> it2 = copy.iterator(); while (it2.hasNext()) {
              String far_node = it2.next(); double d = distances.get(nbor).get(far_node);
              distances.get(node).put(far_node, ((int) d) + 1);
              if (distances.containsKey(far_node) == false) distances.put(far_node, new HashMap<String,Integer>());
              distances.get(far_node).put(node, ((int) d) + 1);
            }
          } else { // Single edge ... node_1 <===> node_2 ... and that's it
            distances.get(node).put(nbor, 1);
            if (distances.containsKey(nbor) == false) distances.put(nbor, new HashMap<String,Integer>());
            distances.get(nbor).put(node, 1);
          }
        }
      }
    }
  }

  /**
   * Queue for the threads to pull source nodes from.  Should only be accessed by the nextSourceNode()
   * method -- it's synchronized...
   */
  private LinkedList<String> thread_queue = null;

  /**
   * Pull the next node out for the worker runnables.  Return null if no more elements are available.
   */
  public synchronized String nextSourceNode() {
    if (thread_queue.size() > 0) { return thread_queue.remove(); } else return null;
  }

  /**
   * Worker Runnable implementation.  Synchronize the put's into the distances HashMap...
   */
  class WorkerRunnable implements Runnable {
    UniGraph g_comp;
    public WorkerRunnable(UniGraph g_comp0) { this.g_comp = g_comp0; }
    public void run() {
      String source = null;
      while ((source = nextSourceNode()) != null) {
        Map<String,Integer> results = dijkstraSingleSourceShortestPath(g_comp, source);
        synchronized (distances) { distances.put(source, results); }
      }
    }
  }

  /**
   * Dijkstra Single Source Shortest path ... caveats: g is completely connected...
   *
   * Derived from: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
   */
  public static Map<String,Integer> dijkstraSingleSourceShortestPath(UniGraph g, String source) {
    Map<String,Integer> d = new HashMap<String,Integer>();

    // Initialization
    d.put(source, 0);

    // Distance lookup and priority queue initialization
    TreeSet<DSSSPVisit> q = new TreeSet<DSSSPVisit>();
    for (int node_i=0;node_i<g.getNumberOfEntities();node_i++) {
      String node = g.getEntityDescription(node_i);
      if (node.equals(source) == false) {
        d.put(node, Integer.MAX_VALUE);
      }
      q.add(new DSSSPVisit(node_i, d.get(node)));
    }

    // Main loop
    while (q.size() > 0) {
      DSSSPVisit u = q.first(); q.remove(u); String node = g.getEntityDescription(u.node_i);
      for (int i=0;i<g.getNumberOfNeighbors(u.node_i);i++) {
        int nbor_i = g.getNeighbor(u.node_i,i); String nbor = g.getEntityDescription(nbor_i);
        int alt = d.get(node) + 1;
        if (alt < d.get(nbor)) {
          d.put(nbor, alt);
          q.add(new DSSSPVisit(nbor_i, d.get(nbor))); // Not exactly correct... but no decrease_priority in TreeSet
        }
      }
    }

    return d;
  }

  /**
   * Distance Map ...
   */
  Map<String,Map<String,Integer>> distances = new HashMap<String,Map<String,Integer>>();

  /**
   * Entities... not sure why i made this part of this interface
   */
  Set<String> entities = new HashSet<String>(); 

  /**
   * Return the distance between two entities.  Alternatively, return
   * a non-zero similarity score (closer to 0 is more similar).
   *
   *@param str_i first entity
   *@param str_j second entity
   *
   *@return distance between entities, similarity between two entities
   */
  public double           distance(String str_i, String str_j) {
    if (str_i.equals(str_j)) return 0.0;
    if (distances.containsKey(str_i)) {
      if (distances.get(str_i).containsKey(str_j)) return distances.get(str_i).get(str_j);
    }
    if (distances.containsKey(str_j)) {
      if (distances.get(str_j).containsKey(str_i)) return distances.get(str_j).get(str_i);
    }
    return Double.POSITIVE_INFINITY;
  }

  /**
   * Return an iterator that goes through all of the entities encapsulated
   * by this distance function.
   *
   *@return iterator over all entities
   */
  public Iterator<String> entityIterator() { return entities.iterator(); }

  /**
   * Return the total number of entities encapsulated by this distance function.
   *
   *@return number of entities
   */
  public int              numberOfEntities() { return entities.size(); }

  /**
   * Test method
   */
  public static void main(String args[]) {
    UniGraph g = new UniGraph();
    g.addNeighbor("ahub", "aspoke0");
    g.addNeighbor("ahub", "aspoke1");
    g.addNeighbor("ahub", "aspoke2");
    g.addNeighbor("ahub", "aspoke3");

    g.addNeighbor("bbow0",   "bbow1");
    g.addNeighbor("bbow0_1", "bbow0");
    g.addNeighbor("bbow0_2", "bbow0");
    g.addNeighbor("bbow0_3", "bbow0");
    g.addNeighbor("bbow1_1", "bbow1");
    g.addNeighbor("bbow1_2", "bbow1");
    g.addNeighbor("bbow1_3", "bbow1");

    g.addNeighbor("cedge0",  "cedge1");

    g.addNeighbor("dtri0",   "dtri1");
    g.addNeighbor("dtri1",   "dtri2");
    g.addNeighbor("dtri2",   "dtri0");

    g.addNeighbor("esingle", "esingle");

    DSSSPPDistFunc dist_func = new DSSSPPDistFunc(g);
    List<String> sorted = new ArrayList<String>();
    Iterator<String> it = dist_func.entityIterator(); while (it.hasNext()) sorted.add(it.next());
    Collections.sort(sorted);

    // Header
    int pad_w = 9;
    System.out.print(pad("",pad_w));
    for (int i=0;i<sorted.size();i++) { System.out.print(pad(sorted.get(i),pad_w)); }
    System.out.println("");
    for (int i=0;i<sorted.size();i++) {
      System.out.print(pad(sorted.get(i), pad_w));
      for (int j=0;j<sorted.size();j++) {
        double d  = dist_func.distance(sorted.get(i), sorted.get(j));
        if (Double.isInfinite(d)) System.out.print(pad("-", pad_w));
        else                      System.out.print(pad("" + ((int) (d)), pad_w));
      }
      System.out.println();
    }
  }
  private static String pad(String s, int len) {
    while (s.length() < len) s += " ";
    return s;
  }
}

/**
 *
 */
class DSSSPVisit implements Comparable<DSSSPVisit> {
  int node_i; int dist;
  public     DSSSPVisit(int e, int d) { node_i = e; dist = d; }
  public int compareTo(DSSSPVisit v) {
    if      (dist   < v.dist)   return -1;
    else if (dist   > v.dist)   return  1;
    else if (node_i < v.node_i) return -1; // TreeSet fails if different objects are equal...
    else if (node_i > v.node_i) return  1;
    else                        return  0; // TreeSet fails if the same object is not equal...
  }
}

