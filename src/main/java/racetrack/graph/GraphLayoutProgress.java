/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.awt.image.BufferedImage;

/**
 * Provide a method for the graph layout to update the user interface on the layout status.
 * Provide a method for the the user interface to cancel the layout operation.
 */
public interface GraphLayoutProgress {
  /**
   * Provide an update on the graph layout progress - provide an option to cancel the layout.
   *
   *@param velocity       current velocity
   *@param iteration      current iteration
   *@param max_iterations maximum number of iterations
   *
   *@return true to cancel the layout operation
   */
  public boolean update(double velocity, int iteration, int max_iterations);

  /**
   * Determine if the user has canceled the layout algorithm.
   *
   *@return true if layout operation has been canceled by the user
   */
  public boolean canceled();

  /**
   * Set a small image that shows what the graph looks like at this stage of the layout.
   *
   *@param image image of graph layout ... 256x256
   *
   *@return true to cancel layout operation
   */
  public boolean setGraphImage(BufferedImage image);
}

