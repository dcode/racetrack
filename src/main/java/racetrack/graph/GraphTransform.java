/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.awt.geom.Rectangle2D;

/**
 * World to Screen and Screen to World Transforms.  Duplicative of multiple other methods...
 * Needs to be refactored.
 */
public class GraphTransform {
  /**
   * Width of the screen
   */
  int         my_w,
  /**
   * Height of the screen
   */
              my_h;
  /**
   * World coordinate extents
   */
  Rectangle2D extents;

  /**
   * Return the screen width.
   *
   *@return screen width
   */
  public int getScreenWidth() { return my_w; }

  /**
   * Return the screen height.
   *
   *@return screen height
   */
  public int getScreenHeight() { return my_h; }

  /**
   * Copy the width, height, and extent to local class variables as the construction process.
   *
   *@param my_w    screen width
   *@param my_h    screen height
   *@param extents world extents of the view port
   */
  public GraphTransform(int my_w, int my_h, Rectangle2D extents) { this.my_w = my_w; this.my_h = my_h; this.extents = extents; }

  /**
   * Convert a world x coordinate into a screen x coordiante.
   *
   *@param wx world x coordinate
   *
   *@return screen x coordinate
   */
  public int     wxToSx  (double wx)     { return (int) (my_w * (wx - extents.getMinX()) / extents.getWidth());  }

  /**
   * Convert a world y coordinate into a screen y coordiante.
   *
   *@param wy world y coordinate
   *
   *@return screen y coordinate
   */
  public int     wyToSy  (double wy)     { return (int) (my_h * (wy - extents.getMinY()) / extents.getHeight()); }

  /**
   * Convert a screen x coordinate into a world x coordiante.
   *
   *@param sx screen x coordinate
   *
   *@return world x coordinate
   */
  public double  sxToWx  (int    sx)     { return ((sx * extents.getWidth()) /my_w)  + extents.getMinX(); }

  /**
   * Convert a screen y coordinate into a world y coordiante.
   *
   *@param sy screen y coordinate
   *
   *@return world y coordinate
   */
  public double  syToWy  (int    sy)     { return ((sy * extents.getHeight())/my_h) + extents.getMinY(); }
}

