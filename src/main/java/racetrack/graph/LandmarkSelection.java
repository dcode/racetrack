/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Provide a class to determine the best landmarks in a graph.
 */
public class LandmarkSelection {
  /**
   * Percentage of nodes as landmarks
   */
  double lm_perc = 0.3;

  /**
   * Nodes as landmarks fixed
   */
  int    lm_max = 100;

  /**
   * Keep a copy of the single source shortest paths - useful if a follow on request occurs for the
   * distance matrix.
   */
  Map<String,DijkstraSingleSourceShortestPath> shortests = new HashMap<String,DijkstraSingleSourceShortestPath>();

  /**
   * Construct class and determine landmarks.
   *
   *@param g            any graph
   *@param exclude_ones exclude one degrees from the calculation
   */
  public LandmarkSelection(MyGraph g_orig, boolean exclude_ones) { this(g_orig, exclude_ones, 0.3, 100); }

  /**
   * Construct class and determine landmarks.
   *
   *@param g            any graph
   *@param exclude_ones exclude one degrees from the calculation
   *@param perc         percentage of nodes that should be landmarks
   *@param max_nodes    number of nodes that should be landmarks (max)
   */
  public LandmarkSelection(MyGraph g_orig, boolean exclude_ones, double perc, int max_nodes) {
    lm_perc = perc; lm_max = max_nodes;

    // Make an undirected graph
    UniGraph g;
    if (exclude_ones) g = new UniGraph(new UniTwoPlusDegreeGraph(g_orig)); else g = new UniGraph(g_orig);

    // Break into connected components -- iterate over each component
    Set<Set<String>> components_set = GraphUtils.connectedComponents(g);
    Iterator<Set<String>> it_sets = components_set.iterator(); while (it_sets.hasNext()) {
      Set<String> component = it_sets.next(); List<String> nodes = new ArrayList<String>(); nodes.addAll(component);

      // Minimum distance from current landmarks..  Sum of distance from landmarks
      Map<String,Double> mins = new HashMap<String,Double>(),
                         sums = new HashMap<String,Double>();
      Iterator<String> it = nodes.iterator(); while (it.hasNext()) { String node = it.next(); mins.put(node, Double.POSITIVE_INFINITY); sums.put(node, 0.0); }

      // Pick a random node to start with
      String next_node = nodes.get(((int) (Math.random() * Integer.MAX_VALUE))%nodes.size());
      Set<String> found = new HashSet<String>(); found.add(next_node);

      // Goal for number of landmarks
      int lm_goal = (int) (lm_perc * nodes.size());
      if (lm_goal > lm_max) lm_goal = lm_max;

      // System.err.println("lm_goal = " + lm_goal + " (component size = " + nodes.size() + ")");

      // Find each landmark iteratively
      while (found.size() < component.size() && found.size() < lm_goal) {
        // Calculate single source shortest paths
        DijkstraSingleSourceShortestPath shortest = new DijkstraSingleSourceShortestPath(g, next_node); shortests.put(next_node, shortest);

        // Update the sums and the mins -- find the max value in the sums;
        double max_sum = Double.NEGATIVE_INFINITY; String max_sum_node = null; double max_sum_min = Double.POSITIVE_INFINITY;

        // Iterator over the nodes updating the sums/mins
        it = nodes.iterator(); while (it.hasNext()) {
          String node = it.next(); double d = shortest.getDistanceTo(node);

          // Update mins and sums
          if (mins.get(node) > d) mins.put(node, d);
          sums.put(node, sums.get(node) + d); 
          
          // Keep the max sum node
          if (sums.get(node) > max_sum && found.contains(node) == false) { max_sum = sums.get(node); max_sum_node = node; max_sum_min = mins.get(node); }
        }

        // Find all the nodes that have that max sum - 
        Set<String> max_sum_set = new HashSet<String>(); max_sum_set.add(max_sum_node);
        it = nodes.iterator(); while (it.hasNext()) { String node = it.next(); if (sums.get(node) == max_sum) max_sum_set.add(node); }

        // If only one, then that's next -- otherwise, choose the one with the highest min
        if (max_sum_set.size() <= 1) next_node = max_sum_node; else {
          it = max_sum_set.iterator(); while (it.hasNext()) { 
            String node = it.next();
            if (mins.get(node) == max_sum_min) { max_sum_node = node; max_sum_min = mins.get(node); }
          }
          next_node = max_sum_node;
        }
        found.add(next_node);
      }
      if (shortests.containsKey(next_node) == false) shortests.put(next_node, new DijkstraSingleSourceShortestPath(g, next_node));

      landmarks.addAll(found);
    }
  }

  /**
   * Set of the calculated landmarks
   */
  Set<String> landmarks = new HashSet<String>();

  /**
   * Return the results of the calculation.
   *
   *@return landmarks as a string set
   */
  public Set<String> landmarks() { return landmarks; }
}
