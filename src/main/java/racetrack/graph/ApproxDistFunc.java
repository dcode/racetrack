/* 

Copyright 2018 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Simple interface to encapsulate distances between different entities.
 */
public class ApproxDistFunc implements DistFunc {
  /**
   * Lookup to find node's approx structure
   */
  Map<String,Approx> approx_lu = new HashMap<String,Approx>();

  /**
   * Construct the approximation distance function.
   *
   *@param g        graph for algorithm
   *@param lm_perc  percentage of nodes for landmarks OR minimum distance to calculate to if greater than 1.0
   */
  public ApproxDistFunc(UniGraph g, double lm_perc) {
    // Separate into connected components
    Set<Set<String>> subs = GraphUtils.connectedComponents(g);

    // Run the algorithm on a per component basis
    Iterator<Set<String>> it_subs = subs.iterator(); while (it_subs.hasNext()) {

      // Create a separate graph for the component
      Set<String> comp = it_subs.next(); UniGraph sub_g = new UniGraph();
      Iterator<String> it = comp.iterator(); while (it.hasNext()) {
        String node = it.next(); int node_i = g.getEntityIndex(node);
        for (int i=0;i<g.getNumberOfNeighbors(node_i);i++) {
          String nbor = g.getEntityDescription(g.getNeighbor(node_i, i));
          sub_g.addNeighbor(node, nbor);
        }
      }

      // Implement the actual algorithm
      Approx approx = new Approx(sub_g, lm_perc);

      // Implement a lookup for the structure
      it = comp.iterator(); while (it.hasNext()) approx_lu.put(it.next(), approx);
    }
  }

  /**
   * Return the number of landmarks.
   *
   *@return number of landmarks
   */
  public int landmarkCount() {
    int landmarks = 0; Set<Approx> done = new HashSet<Approx>();
    Iterator<String> it = approx_lu.keySet().iterator(); 
    while (it.hasNext()) { 
      Approx approx = approx_lu.get(it.next());
      if (done.contains(approx) == false) {
        landmarks += approx.landmarkCount(); 
        done.add(approx);
      }
    }
    return landmarks;
  }

  /**
   * Return the distance between two entities.  Alternatively, return
   * a non-zero similarity score (closer to 0 is more similar).
   *
   *@param str_i first entity
   *@param str_j second entity
   *@param use_min use the minimum of three calculations
   *
   *@return distance between entities, similarity between two entities
   */
  public double           distance(String str_i, String str_j, boolean use_min) {
    if (approx_lu.containsKey(str_i) == false ||
        approx_lu.containsKey(str_j) == false ||
        approx_lu.get(str_i) != approx_lu.get(str_j)) return Double.POSITIVE_INFINITY;
    else return approx_lu.get(str_i).distance(str_i, str_j, use_min);
  }

  /**
   * Return the distance between two entities.  Alternatively, return
   * a non-zero similarity score (closer to 0 is more similar).
   *
   *@param str_i first entity
   *@param str_j second entity
   *
   *@return distance between entities, similarity between two entities
   */
  public synchronized double distance(String str_i, String str_j) { 
    // Check the cache
    if (d_cache.containsKey(str_i) && d_cache.get(str_i).containsKey(str_j)) return d_cache.get(str_i).get(str_j);
    if (d_cache.containsKey(str_j) && d_cache.get(str_j).containsKey(str_i)) return d_cache.get(str_j).get(str_i);

    // Calculate the distance
    double d = distance(str_i, str_j, true);

    // Fill the cache
    if (d_cache.containsKey(str_i) == false) d_cache.put(str_i, new HashMap<String,Double>());
    d_cache.get(str_i).put(str_j, d);

    // Return the distance
    return d;
  }

  /**
   * Cache for the distance lookups
   */
  Map<String,Map<String,Double>> d_cache = new HashMap<String,Map<String,Double>>();

  /**
   * Return an iterator that goes through all of the entities encapsulated
   * by this distance function.
   *
   *@return iterator over all entities
   */
  public Iterator<String> entityIterator() { return (new HashSet<String>(approx_lu.keySet())).iterator(); }

  /**
   * Return the total number of entities encapsulated by this distance function.
   *
   *@return number of entities
   */
  public int              numberOfEntities() {
    return approx_lu.keySet().size();
  }

  /**
   * Approximation structure
   */
  class Approx {
    /**
     * One degree lookup - maps one degree nodes to their neighbor
     */
    Map<String,String> onedeg_lu = new HashMap<String,String>();

    /**
     * For cases where the graph is too small for the approximation algorithm, just
     * make an opt dist func version.
     */
    OptDistFunc opt_dist_func = null;

    /**
     * Lookup for the landmarks to find the single source shortest path
     */
    Map<String,DijkstraSingleSourceShortestPath> sssp_lu = new HashMap<String,DijkstraSingleSourceShortestPath>();

    /**
     * Distance of the node index to its closest landmark
     */
    double closest_d[];

    /**
     * Closest landmark to a node index
     */
    String closest_landmark[];

    /**
     * Return an approximate distance between the two nodes
     *
     *@param u       first node
     *@param v       second node
     *
     *@return approximate distance between the two nodes
     */
    public double distance(String u, String v) { return distance(u, v, true); }

    /**
     * Return an approximate distance between the two nodes
     *
     *@param u       first node
     *@param v       second node
     *@param use_min flag indicating to use the minimum of three approximations
     *
     *@return approximate distance between the two nodes
     */
    public double distance(String u, String v, boolean use_min) {
      // If the optimum distance function exists, just return that
      if (opt_dist_func != null) return opt_dist_func.distance(u,v);

      double adder = 0.0; // Additional distance for one degree nodes
      
      // Determine if the nodes are one degrees -- if so, set them to their neighbor
      if (onedeg_lu.containsKey(u)) { u = onedeg_lu.get(u); adder += 1.0; }
      if (onedeg_lu.containsKey(v)) { v = onedeg_lu.get(v); adder += 1.0; }

      // Find the closest landmark to both nodes
      String lm_u = closest_landmark[twodeg.getEntityIndex(u)],
             lm_v = closest_landmark[twodeg.getEntityIndex(v)];

      // Return the approximate distance

      double lm_path_d = sssp_lu.get(lm_u).getDistanceTo(u)    +
                         sssp_lu.get(lm_u).getDistanceTo(lm_v) +
                         sssp_lu.get(lm_v).getDistanceTo(v)    + adder;

      if (use_min) return min(lm_path_d
                              ,
                              sssp_lu.get(lm_u).getDistanceTo(u) +
                              sssp_lu.get(lm_u).getDistanceTo(v) + adder
                              ,
                              sssp_lu.get(lm_v).getDistanceTo(u) +
                              sssp_lu.get(lm_v).getDistanceTo(v) + adder);
      else         return lm_path_d;
    }

    /**
     * Return the minimum of three doubles.
     *
     *@param a first value
     *@param b second value
     *@param c third value
     *
     *@return smallest of the three values
     */
    public double min(double a, double b, double c) {
      if      (a < b && a < c) return a;
      else if (b < a && b < c) return b;
      else                     return c;
    }

    /**
     * Return the number of landmarks in this approximation.
     *
     *@return number of landmarks
     */
    public int landmarkCount() {
      if (opt_dist_func != null) {
        // System.err.println("OP=" + opt_dist_func.numberOfEntities());
        return opt_dist_func.numberOfEntities();
      } else {
        // System.err.println("AP=" + sssp_lu.keySet().size());
        return sssp_lu.keySet().size();
      }
    }

    /**
     * Undirectional graph that has removed the one degree nodes
     */
    UniGraph twodeg;

    /**
     * Constructor
     *
     *@param connected connected graph
     *@param lm_perc   percentage of landmarks to create if less than 1.0.  If greater than 1.0,
     *                 calculate the approximation until the the greatest minimum distance is this
     *                 number.
     */
    public Approx(UniGraph connected, double lm_perc) {
      // Graph with single degrees removed
      twodeg = new UniGraph();

      // Form a graph that removes all of the single degree nodes
      for (int node_i=0;node_i<connected.getNumberOfEntities();node_i++) {
        String node = connected.getEntityDescription(node_i);
       
        // For single degree nodes, keep a lookup
        if (connected.getNumberOfNeighbors(node_i) == 1) {
          int nbor_i = connected.getNeighbor(node_i, 0); String nbor = connected.getEntityDescription(nbor_i);
          onedeg_lu.put(node, nbor);

        // For two degrees and greater, add them to the twodeg graph
        } else {
          for (int i=0;i<connected.getNumberOfNeighbors(node_i);i++) {
            int nbor_i = connected.getNeighbor(node_i, i); String nbor = connected.getEntityDescription(nbor_i);
            if (connected.getNumberOfNeighbors(nbor_i) != 1) twodeg.addNeighbor(node, nbor);
          }
        }
      }

      // Make sure we have a graph now -- make sure it's big enough... if not, just make an OptDistFunc... otherwise, construct the approximation
      if (twodeg.getNumberOfEntities() < 20) {
        opt_dist_func = new OptDistFunc(connected);

      } else {
        int num_landmarks = (int) (twodeg.getNumberOfEntities() * lm_perc); if (num_landmarks < 10) num_landmarks = 10;

        // Pick a random landmark and calculate all paths from this node
        String landmark = twodeg.getEntityDescription(0);
        DijkstraSingleSourceShortestPath sssp = new DijkstraSingleSourceShortestPath(twodeg, landmark);
        sssp_lu.put(landmark, sssp);

        // Keep track of the closest found distance to a node -- this will be used to find the next landmark
        closest_d        = new double[twodeg.getNumberOfEntities()];
        closest_landmark = new String[twodeg.getNumberOfEntities()];
        for (int i=0;i<closest_d.length;i++) { closest_d[i]        = sssp.getDistanceTo(twodeg.getEntityDescription(i));
                                               closest_landmark[i] = landmark; }

        // Find the furthest distance in the closest array
        int furthest_i = 0;
        for (int i=1;i<closest_d.length;i++) if (closest_d[i] > closest_d[furthest_i]) furthest_i = i;

        double furthest_d = lm_perc + 1.0; // So that the loop starts

        // Fill up the landmarks
        while ((lm_perc <  1.0 && sssp_lu.keySet().size() < num_landmarks) ||
               (lm_perc >= 1.0 && furthest_d              > lm_perc)) {

          // Get the new landmark and calculate all paths from it
          landmark = twodeg.getEntityDescription(furthest_i);
          sssp = new DijkstraSingleSourceShortestPath(twodeg, landmark);
          sssp_lu.put(landmark, sssp);

          // Update the closest array
          for (int i=0;i<closest_d.length;i++) {
            double d = sssp.getDistanceTo(twodeg.getEntityDescription(i));
            if (d < closest_d[i]) { closest_d[i]        = d;
                                    closest_landmark[i] = landmark; }
          }

          // Find the furthest distance in the closest array
          furthest_i = 0;
          for (int i=1;i<closest_d.length;i++) if (closest_d[i] > closest_d[furthest_i]) furthest_i = i;
          furthest_d = closest_d[furthest_i];
        }
      }
    }
  }

  /**
   * Test method
   */
  public static void main(String args[]) {
    try {
      System.out.println("graph_type,NODES,LANDMARKS,algorithm,CREATE_TIME,ALL_ACCESS_TIME,error,error_min");

      // Iterate over graph factor types
      Iterator<GraphFactory.Type> it = GraphFactory.graphTypeIterator(); while (it.hasNext()) {

        // Create the type
        GraphFactory.Type type      = it.next();
        MyGraph           my_graph  = GraphFactory.createInstance(type, null);
        UniGraph          graph     = new UniGraph(my_graph);

        long ts0, ts1;

        // Create the approximation
        ts0 = System.currentTimeMillis(); ApproxDistFunc    approx05_df = new ApproxDistFunc(graph, 0.05); ts1 = System.currentTimeMillis();
        long approx_05_dur = ts1 - ts0;
        ts0 = System.currentTimeMillis(); ApproxDistFunc    approx10_df = new ApproxDistFunc(graph, 0.10); ts1 = System.currentTimeMillis();
        long approx_10_dur = ts1 - ts0;
        ts0 = System.currentTimeMillis(); ApproxDistFunc    approx20_df = new ApproxDistFunc(graph, 0.20); ts1 = System.currentTimeMillis();
        long approx_20_dur = ts1 - ts0;
        ts0 = System.currentTimeMillis(); ApproxDistFunc    approxd4_df = new ApproxDistFunc(graph, 4.00); ts1 = System.currentTimeMillis();
        long approx_d4_dur = ts1 - ts0;
        ts0 = System.currentTimeMillis(); ApproxDistFunc    approxd3_df = new ApproxDistFunc(graph, 3.00); ts1 = System.currentTimeMillis();
        long approx_d3_dur = ts1 - ts0;

        // Create the optimal
        ts0 = System.currentTimeMillis(); OptDistFunc       opt_df    = new OptDistFunc(graph);          ts1 = System.currentTimeMillis();
        long optimal_dur = ts1 - ts0;

        // Cummulative access times
        ts0 = System.currentTimeMillis(); allDistances(graph, approx05_df); ts1 = System.currentTimeMillis();
        long approx_05_allaccess = ts1 - ts0;
        ts0 = System.currentTimeMillis(); allDistances(graph, approx10_df); ts1 = System.currentTimeMillis();
        long approx_10_allaccess = ts1 - ts0;
        ts0 = System.currentTimeMillis(); allDistances(graph, approx20_df); ts1 = System.currentTimeMillis();
        long approx_20_allaccess = ts1 - ts0;
        ts0 = System.currentTimeMillis(); allDistances(graph, approxd4_df); ts1 = System.currentTimeMillis();
        long approx_d4_allaccess = ts1 - ts0;
        ts0 = System.currentTimeMillis(); allDistances(graph, approxd3_df); ts1 = System.currentTimeMillis();
        long approx_d3_allaccess = ts1 - ts0;
        ts0 = System.currentTimeMillis(); allDistances(graph, opt_df); ts1 = System.currentTimeMillis();
        long opt_allaccess = ts1 - ts0;

        double approx_05_min_error = allError(graph, approx05_df, opt_df, true);
        double approx_10_min_error = allError(graph, approx10_df, opt_df, true);
        double approx_20_min_error = allError(graph, approx20_df, opt_df, true);
        double approx_d4_min_error = allError(graph, approxd4_df, opt_df, true);
        double approx_d3_min_error = allError(graph, approxd3_df, opt_df, true);

        double approx_05_error = allError(graph, approx05_df, opt_df, false);
        double approx_10_error = allError(graph, approx10_df, opt_df, false);
        double approx_20_error = allError(graph, approx20_df, opt_df, false);
        double approx_d4_error = allError(graph, approxd4_df, opt_df, false);
        double approx_d3_error = allError(graph, approxd3_df, opt_df, false);

        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + approx05_df.landmarkCount() + ",approx_05," + approx_05_dur + "," + approx_05_allaccess + "," + approx_05_error + "," + approx_05_min_error);
        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + approx10_df.landmarkCount() + ",approx_10," + approx_10_dur + "," + approx_10_allaccess + "," + approx_10_error + "," + approx_10_min_error);
        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + approx20_df.landmarkCount() + ",approx_20," + approx_20_dur + "," + approx_20_allaccess + "," + approx_20_error + "," + approx_20_min_error);
        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + approxd4_df.landmarkCount() + ",approx_d4," + approx_d4_dur + "," + approx_d4_allaccess + "," + approx_d4_error + "," + approx_d4_min_error);
        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + approxd3_df.landmarkCount() + ",approx_d3," + approx_d3_dur + "," + approx_d3_allaccess + "," + approx_d3_error + "," + approx_d3_min_error);
        System.out.println(""  + type + "," + graph.getNumberOfEntities() + "," + graph.getNumberOfEntities() + ",optimum,"   + optimal_dur   + "," + opt_allaccess       + ",0.0,0.0");
      }
    } catch (Throwable t) { System.err.println("Throwable: " + t); t.printStackTrace(System.err); }
  }

  /**
   * Helper method to add all the distances in the graph.
   */
  private static double allDistances(UniGraph graph, DistFunc df) {
    double accum = 0.0;
    for (int i=0;i<graph.getNumberOfEntities();i++) {
      String node_i = graph.getEntityDescription(i);
      for (int j=0;j<graph.getNumberOfEntities();j++) {
        String node_j = graph.getEntityDescription(j);
        double d  = df.distance(node_i, node_j);
        accum += d;
      }
    }
    return accum;
  }

  /**
   * Helper method to compare error metrics between two distance functions.
   */
  private static double allError(UniGraph graph, DistFunc df0, DistFunc df1, boolean use_min) {
    double accum = 0.0; int samples = 0;
    for (int i=0;i<graph.getNumberOfEntities();i++) {
      String node_i = graph.getEntityDescription(i);
      for (int j=0;j<graph.getNumberOfEntities();j++) {
        String node_j = graph.getEntityDescription(j);
        double d0, d1;

        if (df0 instanceof ApproxDistFunc) d0 = ((ApproxDistFunc) df0).distance(node_i, node_j, use_min);
        else                               d0 = df0.distance(node_i, node_j);

        if (df1 instanceof ApproxDistFunc) d1 = ((ApproxDistFunc) df1).distance(node_i, node_j, use_min);
        else                               d1 = df1.distance(node_i, node_j);

        if (Double.isInfinite(d0) || Double.isInfinite(d1)) { } else {
          accum += (d1 - d0)*(d1 - d0);
          samples++;
        }
      }
    }
    return Math.sqrt(accum) / samples;
  }
}

