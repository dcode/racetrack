/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.visualization.GrayColorScale;

/**
 * Slightly better approximation of edge crossings (than the original version in the
 * DSquaredMapping class.  Uses more memory.
 */
public class EdgeCrossingsPlus {
  /**
   * Pixelated version of the number of lines that go through each element in the screen.
   * Will by counts[y-value][x-value];
   */
  Set<String> onpix[][];
  Set<String> diags[][];

  /**
   * Transformation for world to screen and vice versa
   */
  GraphTransform trans;

  /**
   * Construct the edge crossing map.
   *
   *@param g     graph
   *@param map   map from the graph node to the world coordinate
   *@param trans transform from the world coordinate to the screen coordinate
   */
  public EdgeCrossingsPlus(UniGraph g, Map<String,Point2D> map, GraphTransform trans) {
    // Keep a local copy
    this.trans = trans;

    // Construct the counts portion
    onpix = new HashSet[trans.getScreenHeight()][trans.getScreenWidth()];
    diags = new HashSet[trans.getScreenHeight()][trans.getScreenWidth()];
    for (int y=0;y<onpix.length;y++) for (int x=0;x<onpix[y].length;x++) {
      onpix[y][x] = new HashSet<String>();
      diags[y][x] = new HashSet<String>();
    }

    // Accumulate the lines
    for (int node_i=0;node_i<g.getNumberOfEntities();node_i++) {
      String node = g.getEntityDescription(node_i);
      for (int j=0;j<g.getNumberOfNeighbors(node_i);j++) {
        String nbor   = g.getEntityDescription(g.getNeighbor(node_i, j));
        int    nbor_i = g.getEntityIndex(nbor);

        // Unique edge string
        String e_str; if (node_i < nbor_i) e_str = "" + node_i + ":" + nbor_i; else e_str = "" + nbor_i + ":" + node_i;

        // Get the screen coordinates
        int sx0 = trans.wxToSx(map.get(node).getX()), sy0 = trans.wyToSy(map.get(node).getY()),
            sx1 = trans.wxToSx(map.get(nbor).getX()), sy1 = trans.wyToSy(map.get(nbor).getY());

        // Accumulate the line (note that this really assumes that the viewport covers the world coordinate extents
        // If that's not true then this is really, really inefficient

        //
        // Vertical line case
        //
        if        (sx0 == sx1) { if (sy0 > sy1) { for (int y=sy1;y<=sy0;y++) { accum(sx0,   y, Integer.MAX_VALUE, Integer.MAX_VALUE, e_str); } }
                                 else           { for (int y=sy0;y<=sy1;y++) { accum(sx0,   y, Integer.MAX_VALUE, Integer.MAX_VALUE, e_str); } }
        //
        // Horizontal line case
        //
        } else if (sy0 == sy1) { if (sx0 > sx1) { for (int x=sx1;x<=sx0;x++) { accum(x,   sy0, Integer.MAX_VALUE, Integer.MAX_VALUE, e_str); } }
                                 else           { for (int x=sx0;x<=sx1;x++) { accum(x,   sy0, Integer.MAX_VALUE, Integer.MAX_VALUE, e_str); } } 
        //
        // Some variant of Bresenham's line algorithm -- see http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
        //
        } else                 { bresenhams(sx0,sy0,sx1,sy1,e_str); }
      }
    }
  }

  /**
   * Check to see if the two edges share a node -- if so,
   * they aren't an edge crossing.
   *
   *@param e0 edge string 0
   *@param e1 edge string 1
   *
   *@return true if the edges share a node
   */
  protected boolean sameNode(String e0, String e1) {
    String n0 = e0.substring(0,                e0.indexOf(":")),
           n1 = e0.substring(e0.indexOf(":")+1,e0.length()),
           m0 = e1.substring(0,                e1.indexOf(":")),
           m1 = e1.substring(e1.indexOf(":")+1,e1.length());
    return n0.equals(m0) || n0.equals(m1) || n1.equals(m0) || n1.equals(m1);
  }

  /**
   * Variable to avoid calculating the crossings more than once -- set to -1 to indicate that
   * the crossings have not been calculated.
   */
  int crossings_size = -1; // not calculated yet

  /**
   * Compute the total number of crossings for this mapping.  The return is
   * an approximation.
   *
   *@return approximate number of crossings
   */
  public int totalCrossings() {
    if (crossings_size >= 0) return crossings_size;
    Set<String> crossings = new HashSet<String>();
    for (int y=0;y<trans.getScreenHeight();y++) for (int x=0;x<trans.getScreenWidth();x++) {

      // On Pixels
      if (onpix[y][x].size() > 1) {
        List<String> list = new ArrayList<String>(); list.addAll(onpix[y][x]);
        Collections.sort(list);
        for (int i=0;i<list.size();i++) { 
          for (int j=i+1;j<list.size();j++) { 
            if (sameNode(list.get(i),list.get(j)) == false) crossings.add(list.get(i) + "|" + list.get(j)); 
          } 
        }
      }

      // Diagonals
      if (diags[y][x].size() > 1) {
        List<String> list = new ArrayList<String>(); list.addAll(diags[y][x]);
        Collections.sort(list);
        for (int i=0;i<list.size();i++) { 
          for (int j=i+1;j<list.size();j++) { 
            if (sameNode(list.get(i),list.get(j)) == false) crossings.add(list.get(i) + "|" + list.get(j)); 
          } 
        }
      }
    }
    crossings_size = crossings.size();
    return crossings_size;
  }

  /**
   * Some variant of Bresenham's line algorithm -- see http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
   */
  private void bresenhams(int sx0, int sy0, int sx1, int sy1, String e_str) {
    int last_x = Integer.MAX_VALUE, last_y = Integer.MAX_VALUE;
    // Absolute dx and dy
    int adx = sx1 - sx0, ady = sy1 - sy0; if (adx < 0) adx = -adx; if (ady < 0) ady = -ady;
    //
    // Loop over y
    //
    if (ady > adx) {
      if (sy0 > sy1) { int tmpx = sx0, tmpy = sy0; sx0 = sx1; sy0 =sy1; sx1 = tmpx; sy1 = tmpy; } // Swap so that point 0 is less than
      // Increasing x
      if (sx1 > sx0) {
        int dx = sx1 - sx0, dy = sy1 - sy0, D = dx - dy, x = sx0;
        for (int y=sy0;y<=sy1;y++) { accum(x,y,last_x,last_y,e_str); last_x = x; last_y = y; if (D >= 0) { x++; D-=dy; } D += dx; }
      // Decreasing x
      } else         {
        int dx = sx0 - sx1, dy = sy1 - sy0, D = dx - dy, x = sx0;
        for (int y=sy0;y<=sy1;y++) { accum(x,y,last_x,last_y,e_str); last_x = x; last_y = y; if (D >= 0) { x--; D-=dy; } D += dx; }
      }
    //
    // Loop over x
    //
    } else         {
      if (sx0 > sx1) { int tmpx = sx0, tmpy = sy0; sx0 = sx1; sy0 =sy1; sx1 = tmpx; sy1 = tmpy; } // Swap so that point 0 is less than
      // Increasing y
      // -- Pretty much an exact copy of the wikipedia integer algorithm version
      if (sy1 > sy0) {
        int dx = sx1 - sx0, dy = sy1 - sy0, D = dy - dx, y = sy0;
        for (int x=sx0;x<=sx1;x++) { accum(x,y,last_x,last_y,e_str); if (D >= 0) { y++; D-=dx; } D += dy; }
      // Decreasing y
      } else         {
        int dx = sx1 - sx0, dy = sy0 - sy1, D = dy - dx, y = sy0;
        for (int x=sx0;x<=sx1;x++) { accum(x,y,last_x,last_y,e_str); if (D >= 0) { y--; D-=dx; } D += dy; }
      }
    }
  }

  /**
   * Accumulate into the counts map as long as x and y are within the array bounds.
   *
   *@param x pixel x coordinate
   *@param y pixel y coordinate
   */
  private void accum(int x, int y, int last_x, int last_y, String e_str) { 
    if (x >= 0 && x < trans.getScreenWidth() && y >= 0 && y < trans.getScreenHeight()) { 
      (onpix[y][x]).add(e_str); 
      if (last_x != Integer.MAX_VALUE && last_x != x && last_y != y) {
        int smaller_x = last_x < x ? last_x : x, smaller_y = last_y < y ? last_y : y;
        if (smaller_x >= 0 && smaller_x < trans.getScreenWidth() && smaller_y >= 0 && smaller_y < trans.getScreenHeight()) {
          (diags[smaller_y][smaller_x]).add(e_str);
        }
      }
    }
  }

  /**
   * Return the value from the counts map as long as x and y are within the array bounds.
   *
   *@param x pixel x coordinate
   *@param y pixel y coordinate
   *
   *@return counts map value
   */
  private int  value(int x, int y) { if (x >= 0 && x < trans.getScreenWidth() && y >= 0 && y < trans.getScreenHeight()) return (onpix[y][x]).size(); else return 0; }

  /**
   * Return an image that maps the counts into a gray colorscale.
   *
   *@return grayscale version of the counts map
   */
  public BufferedImage grayMap() {
    // Find the max value
    int max = 0;
    for (int y=0;y<trans.getScreenHeight();y++) for (int x=0;x<trans.getScreenWidth();x++) { int count = value(x,y); if (max < count) max = count; }
    if (max == 0) max = 1;

    // Allocate the image
    BufferedImage bi = new BufferedImage(trans.getScreenWidth(), trans.getScreenHeight(), BufferedImage.TYPE_INT_RGB);

    // Apply the color map
    GrayColorScale cs = new GrayColorScale();
    for (int y=0;y<trans.getScreenHeight();y++) for (int x=0;x<trans.getScreenWidth();x++) {
      float f     = value(x,y) / ((float) max);
      bi.setRGB(x,y,cs.at(f).getRGB());
    }

    return bi;
  }
}

