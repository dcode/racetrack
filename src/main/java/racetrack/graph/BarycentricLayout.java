/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.awt.geom.Point2D;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Layout for barycentric placement of nodes.  Assumes a connected graph as input.
 */
public class BarycentricLayout {
  /**
   * Graph to apply layout to
   */
  UniGraph g;

  /**
   * Distance function for computing distances between nodes
   */
  DistFunc dist;

  /**
   * Selection ... if set and not empty, only these nodes will be modified
   */
  Set<String> selection;

  /**
   * Map from the node to its xy coordinate
   */
  Map<String,Point2D> world_map;

  /**
   * All nodes from the graph
   */
  Set<String> all_nodes = new HashSet<String>();

  /**
   * Number of landmarks to use
   */
  int landmarks_to_use = 3;

  /**
   * Constructor
   *
   *@param g0                 graph to layout ... assumes a graph that is connected
   *@param dist0              distance function -- if null, will be calculated
   *@param landmarks_to_use0  number of landmarks to use (unless the selection is set)
   *@param selection0         selected nodes -- if not null/not empty, these nodes will be used as landmarks
   *@param world_map0         node to xy coordinates -- will be modified by class
   */
  public BarycentricLayout(UniGraph            g0, 
                           DistFunc            dist0,
                           int                 landmarks_to_use0,
                           Set<String>         selection0,
                           Map<String,Point2D> world_map0,
                           GraphLayoutProgress layout_progress) {
    // Copy the params over
    this.g                = g0;
    this.landmarks_to_use = landmarks_to_use0;
    this.selection        = new HashSet<String>(); if (selection0 != null) selection.addAll(selection0);
    this.world_map        = world_map0;

    // Make sure the landmarks to use setting is sane
    if (landmarks_to_use < 3) landmarks_to_use = 3;

    // Create the all_nodes set
    for (int i=0;i<g.getNumberOfEntities();i++) all_nodes.add(g.getEntityDescription(i));

    // Make sure the node is x times the size of the landmarks to use setting
    if (g.getNumberOfEntities() <= landmarks_to_use*4) return;

    // Create the distance function (if it's null)
    if (dist0 == null) { 
      System.err.println("Calculating node distance function -- worst case O(n^3)...");
      dist = new DSSSPPDistFunc(g); 
      System.err.println("  Done!");
    } else this.dist = dist0;

    // Make sure all of the selection nodes are actually in the all_nodes set
    Iterator<String> it = selection.iterator(); while (it.hasNext()) { if (all_nodes.contains(it.next()) == false) it.remove(); }

    //
    // Perform the layout
    // -- first step is to choose the landmarks ... if the selection size is three or greater, 
    //    then choose those... otherwise pick a specified number
    String landmarks[] = null;
    if (selection != null && selection.size() >= 3) {
      landmarks = new String[selection.size()]; 
      it = selection.iterator(); int i = 0; while (it.hasNext()) { landmarks[i++] = it.next(); }
    } else { landmarks = selectLandmarks(); }

    //
    // -- second step, place the landmarks...
    //    ... with three, it's easy and doesn't matter... unsure what's optimal for more than three
    //    ... will use a circle
    if (landmarks != null && landmarks.length >= 3) {
      Set<String> landmarks_set = new HashSet<String>(); for (int i=0;i<landmarks.length;i++) landmarks_set.add(landmarks[i]);

      // Circle placement (probably should think about optimizing order [ or which nodes are closest to each other ])
      // ... hierarchical clustering would be good if we had the distance metric setup for landmarks
      double angle_inc = 2.0 * Math.PI / landmarks.length;
      double angle     = 0.0;
      for (int i=0;i<landmarks.length;i++,angle+=angle_inc) {
        world_map.put(landmarks[i], new Point2D.Double(Math.cos(angle),Math.sin(angle)));
      }

      //
      // -- third step, place all the other nodes
      //
      it = all_nodes.iterator(); while (it.hasNext()) {
        String node = it.next(); if (landmarks_set.contains(node) == false) {
          barycentricNodePlacement(node, landmarks);
        }
      }

      //
      // -- third point 5 -- run a few iterations of the spring layout on the landmarks
      //    (the landmarks were arranged in a circle... the order was random)
      //
      YetAnotherSpringLayout yasl = new YetAnotherSpringLayout(g, dist, landmarks_set, world_map, false);
      for (int i=0;i<landmarks_set.size()*3;i++) yasl.iterateLayout(landmarks_set.size());

      //
      // -- fourth step, apply some intra-node arrangements based on randomizing the landmarks
      //
      List<String> node_list = new ArrayList<String>(); node_list.addAll(all_nodes); // For picking for randomization

      int max_iters = all_nodes.size()*all_nodes.size();
      for (int iters=0;iters<max_iters;iters++) {
        if (layout_progress != null) {
          layout_progress.update(0.0, iters, max_iters);
          if (layout_progress.setGraphImage(GraphUtils.render(g,world_map))) return;
        }

        // Apply the barycentric placement to all of the nodes
        it = all_nodes.iterator(); while (it.hasNext()) {
          String node = it.next(); // if (landmarks_set.contains(node)) continue;

          // Select a random set ... equal to landmarks_to_use setting
          Set<String> randoms = new HashSet<String>(); while (randoms.size() < landmarks_to_use) {
            int    random_i = (int) ((Math.random() * 3 * node_list.size())%node_list.size());
            String random   = node_list.get(random_i);
            if (node.equals(random) == false) randoms.add(random);
          }

          // Adjust that placement of the node
          String as_array[] = new String[randoms.size()]; it = randoms.iterator(); for (int i=0;i<as_array.length;i++) as_array[i] = it.next();
          barycentricNodePlacement(node, as_array);
        }

        // Apply Springs
        yasl = new YetAnotherSpringLayout(g, dist, null, world_map, false);
        yasl.iterateLayout(9);
      }
    }
  }

  /**
   * Get the calculated distance function.
   *
   *@return distance function
   */
  public DistFunc distFunc() { return dist; }

  /**
   * Selects landmark nodes ... furthest nodes aparts (non-one-degrees).
   */
  private String[] selectLandmarks() {
    LandmarkSelection landmark_selection = new LandmarkSelection(g, true, 1.0, landmarks_to_use);
    Set<String> set = landmark_selection.landmarks(); 
    if (set.size() >= landmarks_to_use) {
      String lms[] = new String[landmarks_to_use]; int i = 0;
      Iterator<String> it = set.iterator(); while (it.hasNext()) lms[i++] = it.next();
      return lms;
    } else return null;
  }

  /**
   * Place a single node in a barycentric strategy.
   *
   * ... note -- one limitation is that if a node is close to one landmark... but further away from
   *             the other landmarks, then this node should be outside of the bounds set by the
   *             original landmark placement.
   */
  private void barycentricNodePlacement(String node, String landmarks[]) {
    // Sum of the distances from the node to the landmarks
    double d_max = 0.0; for (int i=0;i<landmarks.length;i++) { 
      double d = dist.distance(node, landmarks[i]); 
      if (d > d_max) d_max = d;
    } 

    double d_total = 0.0; for (int i=0;i<landmarks.length;i++) {
      double d = dist.distance(node, landmarks[i]); 
      d_total += (d_max - d);
    }

    // As a percentage, the amount of pull each landmarks has on the node
    double x = 0.0, y = 0.0;
    for (int i=0;i<landmarks.length;i++) {
      double  d           = dist.distance(node, landmarks[i]);
      Point2D landmark_pt = world_map.get(landmarks[i]);
      x += landmark_pt.getX() * (d_max - d) / d_total;
      y += landmark_pt.getY() * (d_max - d) / d_total;
    }

    // Set the nodes location
    world_map.put(node, new Point2D.Double(x,y));
  }
}

