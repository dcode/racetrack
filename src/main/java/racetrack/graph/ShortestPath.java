/* 

Copyright 2018 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Calculate the shortest path from a one node to another.
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class ShortestPath {
  /**
   * Source index node
   */
  int      src_i, 

  /**
   * Destination index node
   */
           dst_i;


  /**
   * Algorithm finished successfully
   */
  boolean  finished = false;

  /**
   * Traces the path back as nodes are discovered
   */
  Map<Integer,Integer> traceback = new HashMap<Integer,Integer>();

  /**
   * Return the results as a path of nodes.
   *
   *@return array of nodes -- 0 is the src_i and (n-1) is the dst_i...  return null if no path found
   */
  public int[]  getPath() {
    if (finished == false) return null;

    List<Integer> list = new ArrayList<Integer>(); list.add(dst_i);
    // System.out.print(g.getEntityDescription(dst_i));
    while (list.get(list.size()-1) != src_i) { 
      list.add(traceback.get(list.get(list.size()-1))); 
      // System.out.print(" => " + g.getEntityDescription(list.get(list.size()-1)));
    }
    // System.out.println();

    int array[] = new int[list.size()];
    for (int i=0;i<array.length;i++) array[i] = list.get(list.size() - 1 - i);
    return array;
  }

  /**
   * Copy of the original graph (only needed for debugging)
   */
  MyGraph g;

  /**
   * Constructor and Algorithm Implementation
   */
  public ShortestPath(MyGraph g, int src_i, int dst_i) {
    this.g = g; this.src_i = src_i; this.dst_i = dst_i;
    Queue<Integer> queue = new LinkedList<Integer>(); Set<Integer> done = new HashSet<Integer>();
    queue.add(src_i);
    while (queue.size() > 0) {
      int next = queue.remove(); 
      // System.out.println("Evaluating " + g.getEntityDescription(next));
      if (done.contains(next) == false) {
        done.add(next);
        // System.out.println("  Not Done Yet");
        for (int i=0;i<g.getNumberOfNeighbors(next);i++) {
          int nbor = g.getNeighbor(next, i);
          // System.out.println("    Examining Neighbor " + i + " == " + g.getEntityDescription(nbor));
          if (done.contains(nbor) == false) {
            // System.out.println("      Add Neighbor To Queue");
            queue.add(nbor);
            traceback.put(nbor, next);
            // System.out.println("      Setting Traceback " + g.getEntityDescription(nbor) + " => " + g.getEntityDescription(next));
            if (nbor == dst_i) { 
              finished = true; 
              // System.out.println("      Found Destination Node - Exiting");
              return; 
            }
          }
        }
      }
    }
    // System.out.println("Exiting Constructor Without Path");
  }
}

