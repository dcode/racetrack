/*

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import java.awt.geom.Area;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;

import racetrack.framework.BundlesDT;

import racetrack.util.CSVTokenConsumer;
import racetrack.util.RFC4180CSVReader;
import racetrack.util.Utils;

/**
 * Interactively create a hexagonal map of the world.
 */
public class MakeAHexMap extends JFrame {
  /**
   * Shape records
   */
  ShapeFile   shape_file;

  /**
   * Reference information for country codes... it's messy
   */
  WorldHexMap world_hex_map;

  /**
   * a3 country code to the loaded input shape description
   */
  Map<String,Set<ShapeRecord>> a3_to_shapes = new HashMap<String,Set<ShapeRecord>>();

  /**
   * To save some of the expense, create the area object for each country
   */
  Map<String,Area>             a3_to_area   = new HashMap<String,Area>();

  /**
   * loaded input shape description to the a3 -- should be 1-to-1 / believe that's validated in the code
   */
  Map<ShapeRecord,String> shape_to_a3 = new HashMap<ShapeRecord,String>();

  /**
   *
   */
  JCheckBox draw_background_cb, 
            draw_all_hexes_cb, 
            draw_a3_hexes_cb,
            draw_hexmap_cb,
            highlight_countries_cb,
            show_missing_countries_cb;

  /**
   * Constructor
   *
   *@param shapes          shape file
   *@param csv_mappings    corresponding csv file for the shapes
   *@param make_a_hex_file saved make a hex state -- can be null
   */
  public MakeAHexMap(File shapes, File csv_mappings, File make_a_hex_file) throws IOException {
    // Get the shapes
    shape_file = new ShapeFile(shapes, true);

    // Get the reference codes
    world_hex_map = new WorldHexMap();

    // Get the shape to country mappings
    new RFC4180CSVReader(csv_mappings, new CSVTokenConsumer() {
      public boolean consume(String tokens[], String line, int line_no) {

        if (line_no == 1) return true; // header row

        // Indexes were determined by example ... should be re-vetted with new files
        // ... ne_110m_admin_0_sovereignty.csv
        // System.out.println(); for (int i=0;i<tokens.length;i++) System.out.println(line_no + "-" + i + " : \"" + tokens[i] + "\"");

        String a3code_sov = tokens[4],
               a3code_adm = tokens[9],
               iso_a2     = tokens[45],
               iso_a3     = tokens[46],
               continent  = tokens[59],
               region     = tokens[60];

        ShapeRecord shape_record = shape_file.getShape(line_no-1); // looks like the record numbers are one-based
        if (shape_record == null) System.err.println("No Shape Found For \"" + a3code_sov + "\"");

        if      (a3code_sov.equals("US1")) iso_a3 = "USA";
        else if (a3code_sov.equals("GB1")) iso_a3 = "GBR";
        else if (a3code_sov.equals("DN1")) iso_a3 = "DNK"; // Denmark or Germany?
        else if (a3code_sov.equals("FR1")) iso_a3 = "FRA";
        else if (a3code_sov.equals("IS1")) iso_a3 = "ISR"; // Israel?
        else if (a3code_sov.equals("NL1")) iso_a3 = "NLD"; // Netherlands
        else if (a3code_sov.equals("NZ1")) iso_a3 = "NZL";
        else if (a3code_sov.equals("AU1")) iso_a3 = "AUS";
        else if (a3code_sov.equals("CH1")) iso_a3 = "CHN";
        else if (a3code_sov.equals("FI1")) iso_a3 = "FIN"; // Finland?
        // else if (a3code_sov.equals("CYN")) iso_a3 =
        // else if (a3code_sov.equals("SOL")) iso_a3 =
        // else if (a3code_sov.equals("KOS")) iso_a3 =

        String using_a3 = world_hex_map.a3code(iso_a3); if (using_a3.equals(BundlesDT.NOTSET)) using_a3 = world_hex_map.a3code(a3code_sov);

        if (using_a3.equals(BundlesDT.NOTSET) == false) {
          if (a3_to_shapes.containsKey(using_a3) == false) a3_to_shapes.put(using_a3, new HashSet<ShapeRecord>());
          a3_to_shapes.get(using_a3).add(shape_record);
          if (shape_to_a3.containsKey(shape_record)) System.err.println("Shape Already Matches An A3");
          else shape_to_a3.put(shape_record, using_a3);
        } else { 
          System.err.print("MakeAHexMap:  No A3Code for \"" + iso_a3 + "\" | ");
          System.err.print("  a3code_sov = \"" + a3code_sov + "\"");
          System.err.print("  a3code_adm = \"" + a3code_adm + "\"");
          System.err.print("  iso_a2 = \"" + iso_a2 + "\"");
          System.err.println();
        } 

        return true;
      }
      public void commentLine(String str) { System.err.println("commentLine(\"" + str + "\" ... shouldn't be here..."); }
    } );

    // Dump the a3's found to the screen
    System.err.println(a3_to_shapes.keySet());

    // Calculate the areas ... hopefully this is faster for comparisons
    Iterator<String> it_a3 = a3_to_shapes.keySet().iterator(); while (it_a3.hasNext()) {
      String a3 = it_a3.next(); Set<ShapeRecord> shaperecs = a3_to_shapes.get(a3); Area area = new Area(); 
      Iterator<ShapeRecord> it_shaperec = shaperecs.iterator(); while (it_shaperec.hasNext()) {
        ShapeRecord shaperec = it_shaperec.next(); for (int i=0;i<shaperec.getNumberOfShapes();i++) {
          area.add(new Area(shaperec.getShape(i)));
        }
      }
      a3_to_area.put(a3, area);
    }

    // Figure out the bounding box
    Iterator<ShapeRecord> it = shape_file.shapeRecordIterator(); while (it.hasNext()) {
      ShapeRecord record = it.next(); if (shape_to_a3.containsKey(record) == false) continue;
      for (int i=0;i<record.getNumberOfShapes();i++) {
        Shape shape = record.getShape(i);
        Rectangle2D bounds = shape.getBounds2D();
        if (x_min > bounds.getMinX()) x_min = bounds.getMinX();
        if (y_min > bounds.getMinY()) y_min = bounds.getMinY();
        if (x_max < bounds.getMaxX()) x_max = bounds.getMaxX();
        if (y_max < bounds.getMaxY()) y_max = bounds.getMaxY();
      }
    }
    // Add 2 percent
    double two = (x_max - x_min) * 0.05; x_max += two; x_min -= two;
           two = (y_max - y_min) * 0.05; y_max += two; y_min -= two;

    // Validate which country codes we have / which are missing
    Iterator<String> it_cc = world_hex_map.countryCodeIterator(); while (it_cc.hasNext()) {
      String cc = it_cc.next();
      if (a3_to_shapes.containsKey(cc) == false) System.err.println("MakeAHexMap:  missing country code \"" + cc + "\"");
    }

    // Calculate the initial version of the hexagons
    createHexagons(false);

    // Construct the GUI
    getContentPane().add("Center", my_component = new MyComponent());

    JPanel south = new JPanel(new FlowLayout());
      south.add(draw_background_cb = new JCheckBox("Background", true));
      south.add(draw_all_hexes_cb  = new JCheckBox("Hexes",      false));
      south.add(draw_a3_hexes_cb   = new JCheckBox("CC Hexes",   true));
      south.add(draw_hexmap_cb     = new JCheckBox("HexMap",     true));

      south.add(highlight_countries_cb    = new JCheckBox("HL Counties", true));
      south.add(show_missing_countries_cb = new JCheckBox("Missing", true));

      draw_background_cb.       addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { my_component.repaint(); } } );
      draw_all_hexes_cb.        addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { my_component.repaint(); } } );
      draw_a3_hexes_cb.         addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { my_component.repaint(); } } );
      draw_hexmap_cb.           addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { my_component.repaint(); } } );
      show_missing_countries_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { my_component.repaint(); } } );

      JButton bt;

      south.add(bt = new JButton("Init Hex"));
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        createHexagons(true);
        my_component.repaint();
      } } );

      south.add(bt = new JButton("Hex Area"));
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        createCountryAreasFromHexMap();
        my_component.repaint();
      } } );

      south.add(bt = new JButton("Save"));
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveFile(new File("make_a_hex.txt")); } } );

      south.add(bt = new JButton("Export"));
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { exportShapesAsCSV(); } } );

    getContentPane().add("South", south);

    setSize(1680,1000); setVisible(true);

    if (make_a_hex_file != null) { loadFile(make_a_hex_file); my_component.repaint(); }
  }

  /**
   * Save the state of this application to a file.
   *
   *@param file file to save
   */
  public void saveFile(File file) {
    try {
      PrintStream out = new PrintStream(new FileOutputStream(file));
        out.println(hex_xoff + "|" + hex_yoff + "|" + hex_side);
        Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) {
          Path2D hex = it_hex.next();
          String a3  = hex_to_a3.get(hex);
          out.println(hex_to_index.get(hex) + "|" + a3);
        }
      out.close();
    } catch (IOException ioe) { System.err.println("IOE: " + ioe); ioe.printStackTrace(System.err); }
  }

  /**
   * Load a file to restore the state of this application.
   *
   *@param file file to load
   */
  public void loadFile(File file) {
    try {
      BufferedReader in = new BufferedReader(new FileReader(file));

      // Get basic parameters for hexagon creation ... then create the hexagons
      String line = in.readLine();
        StringTokenizer st = new StringTokenizer(line, "|");
          hex_xoff = Double.parseDouble(st.nextToken());
          hex_yoff = Double.parseDouble(st.nextToken());
          hex_side = Double.parseDouble(st.nextToken());
        createHexagons(true);

        // Clear them all
        // Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) it_hex.remove();
        hex_to_a3 = new HashMap<Path2D,String>();

        // Set them based on the file contents
        while ((line = in.readLine()) != null) {
          st = new StringTokenizer(line, "|");
          String x_str = st.nextToken(),
                 y_str = st.nextToken(),
                 a3    = st.nextToken();
          Path2D hex = index_to_hex.get(x_str + "|" + y_str);
          hex_to_a3.put(hex,a3);
        }
      in.close();
    } catch (IOException ioe) { System.err.println("IOE: " + ioe); ioe.printStackTrace(System.err); }
  }

  /**
   * Return the a3 country code strings of what overlaps with a shape.
   */
  Set<String> countryOverlaps(Path2D hex) {
    Set<String> set = new HashSet<String>();
    Iterator<String> it = a3_to_area.keySet().iterator(); while (it.hasNext()) {
      String a3 = it.next(); Area a3_area = a3_to_area.get(a3);
      if (a3_area.intersects(hex.getBounds2D())) {
        Area expensive_check = new Area(hex); expensive_check.intersect(a3_area);
        if (expensive_check.isEmpty() == false) set.add(a3);
      }
    }
    return set;
  }

  /**
   * Determine the overlap between a shape and the country shape
   */
  public Area footprint(Path2D hex, String a3) {
    Area area = new Area(hex); area.intersect(a3_to_area.get(a3)); return area;
  }

  /**
   * Return the area of a rectangle
   */
  public double size(Rectangle2D rect) { return rect.getWidth() * rect.getHeight(); }

  /**
   * Determine the a3 with the most overlap in the hex...
   * ... this is an approximate.. uses the boundary of the intersection as a proxy for its size
   */
  public String mostOverlap(Path2D hex, Set<String> a3s) {
    Iterator<String> it = a3s.iterator();
    String best_a3 = it.next(); Area best_a3_hex_footprint = footprint(hex,best_a3);
    while (it.hasNext()) {
      String a3 = it.next(); Area a3_hex_footprint = footprint(hex,a3);
      if (size(a3_hex_footprint.getBounds2D()) > size(best_a3_hex_footprint.getBounds2D())) {
        best_a3 = a3; best_a3_hex_footprint = a3_hex_footprint;
      }
    }
    return best_a3;
  }

  /**
   * True if the hex is mostly water.  Only compares with the area
   * of the specified country code.
   */
  public boolean hexIsMostlyWater(Path2D hex, String a3) {
    Area inter = new Area(hex); inter.intersect(a3_to_area.get(a3));
    Rectangle2D hex_bounds   = hex.getBounds2D();    double hex_bounds_area   = hex_bounds.getWidth()   * hex_bounds.getHeight();
    Rectangle2D inter_bounds = inter.getBounds2D();  double inter_bounds_area = inter_bounds.getWidth() * inter_bounds.getHeight();

    double perc50 = 0.50 * hex_bounds_area;

    if (hex_bounds_area - inter_bounds_area > perc50) return true;

    return false;
  }

  /**
   * Find an edge of the hexagon... bad ascii art:
   *
   *      1-- 1 --2
   *     /         \
   *    0           2
   *   /             \
   *  0               3
   *   \             /
   *    5           3
   *     \         /
   *      5-- 4 --4
   *
   */
  public Line2D edge(Path2D hex, int edge_no) {
    return edge_lu.get(hex).get(edge_no);
  }

  /**
   * Bad idea... but it solves a re-calc problem...
   */
  Map<Path2D,Map<Integer,Line2D>> edge_lu = new HashMap<Path2D,Map<Integer,Line2D>>();

  public void resetEdgeLookup() { edge_lu = new HashMap<Path2D,Map<Integer,Line2D>>(); }

  /**
   * Create a hexagonal shape via the specific parameters.
   */
  public Path2D makeHex(double my_x, double my_y, double my_hex_side, double my_hex_height) {
    Path2D path = new Path2D.Double(); double x0,y0,x1,y1,x2,y2,x3,y3,x4,y4,x5,y5;

    path.moveTo(x0 = my_x - my_hex_side,     y0 = my_y); 
    path.lineTo(x1 = my_x - my_hex_side/2.0, y1 = my_y + my_hex_height/2.0); 
    path.lineTo(x2 = my_x + my_hex_side/2.0, y2 = my_y + my_hex_height/2.0);
    path.lineTo(x3 = my_x + my_hex_side,     y3 = my_y); 
    path.lineTo(x4 = my_x + my_hex_side/2.0, y4 = my_y - my_hex_height/2.0); 
    path.lineTo(x5 = my_x - my_hex_side/2.0, y5 = my_y - my_hex_height/2.0); 
    path.lineTo(     my_x - my_hex_side,          my_y);
    path.closePath();

    edge_lu.put(path,new HashMap<Integer,Line2D>());

    edge_lu.get(path).put(0, new Line2D.Double(x0,y0,x1,y1));
    edge_lu.get(path).put(1, new Line2D.Double(x1,y1,x2,y2));
    edge_lu.get(path).put(2, new Line2D.Double(x2,y2,x3,y3));
    edge_lu.get(path).put(3, new Line2D.Double(x3,y3,x4,y4));
    edge_lu.get(path).put(4, new Line2D.Double(x4,y4,x5,y5));
    edge_lu.get(path).put(5, new Line2D.Double(x5,y5,x0,y0));

    return path;
  }

  /**
   * Calculate the hexagonal height based on the side length.
   */
  public double hexHeight() { return Math.sqrt(3.0*hex_side*hex_side/4.0); }

  /**
   * Hexagon parameters
   */
  double hex_xoff =  0.0, 
         hex_yoff =  0.0, 
         hex_side =  2.0;

  /**
   * Hex to a3 country code lookup
   */
  Map<Path2D,String> hex_to_a3 = new HashMap<Path2D,String>();

  /**
   * Hex to all overlapping a3s
   */
  Map<Path2D,Set<String>> hex_to_a3s = new HashMap<Path2D,Set<String>>();

  /**
   * All hex shapes created... sometimes paired down to just the ones overlapping with countries
   */
  Set<Path2D>        all_hex   = new HashSet<Path2D>();

  /**
   * index string to the hex ... index string is "x|y"
   */
  Map<String,Path2D> index_to_hex = new HashMap<String,Path2D>();

  /**
   * hex to the index string ... index string is "x|y"
   */
  Map<Path2D,String> hex_to_index = new HashMap<Path2D,String>();

  /**
   * a3 to a shape formed by all of the a3_to_hex components
   */
  Map<String,Set<Path2D>> a3_to_hexmaps = new HashMap<String,Set<Path2D>>();

  /**
   * From the current hex_to_a3 mapping, create area maps for each represented country code
   */
  public synchronized void createCountryAreasFromHexMap() {
    // Find all hexes per country
    Map<String,Set<Path2D>> my_map = new HashMap<String,Set<Path2D>>();
    Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) {
      Path2D hex = it_hex.next(); String a3 = hex_to_a3.get(hex);
      if (my_map.containsKey(a3) == false) my_map.put(a3, new HashSet<Path2D>());
      my_map.get(a3).add(hex);
    }

    // For each country, make the conglomerate shape out of the hexes
    Iterator<String> it_a3 = my_map.keySet().iterator(); while (it_a3.hasNext()) {
      String a3 = it_a3.next();
      
      // Determine the connected components
      Set<Set<Path2D>> all_connected = new HashSet<Set<Path2D>>();
      Set<Path2D> handled = new HashSet<Path2D>(); handled.addAll(my_map.get(a3));

      while (handled.size() > 0) {
        Set<Path2D> connected = findOneConnected(handled);
        all_connected.add(connected);
      }

      // Check the connected component for holes...  separate the ones with holes...
      // ... we can't make areas w/ holes in them... (or maybe we can... rather not)
      Set<Set<Path2D>> without_holes = new HashSet<Set<Path2D>>();
      Iterator<Set<Path2D>> it_connected = all_connected.iterator(); while (it_connected.hasNext()) {
        Set<Path2D> connected = it_connected.next(); Set<Path2D> holes = findHoles(connected);
        if (holes.size() > 0) {
          System.err.println("" + a3 + " | Has Holes..."); // abc missing code here
        } else without_holes.add(connected);
      }

      // Create the new Path2D out of the perimeter path
      a3_to_hexmaps.put(a3, new HashSet<Path2D>());
      Iterator<Set<Path2D>> it_wout_holes = without_holes.iterator(); while (it_wout_holes.hasNext()) {
        Set<Path2D> hex_set = it_wout_holes.next(); List<Line2D> perimeter = calcPerimeter(hex_set);
        a3_to_hexmaps.get(a3).add(createPath2D(perimeter));
      }
    }
  }

  /**
   * Find the hexagon holes in a set of hexagons... return those hexagons
   * as a set...
   */
  public Set<Path2D> findHoles(Set<Path2D> set) {
    Set<Path2D> holes = new HashSet<Path2D>();
    // abc .. missing code here
    return holes;
  }

  /**
   * Find the perimeter edges for a connected set of hexagons.
   * ... as an ordered list...
   *
   *      1-- 1 --2
   *     /         \
   *    0           2
   *   /             \
   *  0               3
   *   \             /
   *    5           3
   *     \         /
   *      5-- 4 --4
   *
   */
  public List<Line2D> calcPerimeter(Set<Path2D> set) {
    List<Line2D> edges = new ArrayList<Line2D>();
    // Find the lowermost, leftmost hexagon... start there
    Iterator<Path2D> it = set.iterator();
    Path2D lowerleft = it.next(); while (it.hasNext()) {
      Path2D hex = it.next(); 
      if      (hex.getBounds2D().getMinY() <   lowerleft.getBounds2D().getMinY()) lowerleft = hex;
      else if (hex.getBounds2D().getMinY() ==  lowerleft.getBounds2D().getMinY() &&
               hex.getBounds2D().getMinX() <   lowerleft.getBounds2D().getMinX()) lowerleft = hex;
    }

    // Add that bottom-most edge
    edges.add(edge(lowerleft,4)); // this edge is definitely on the perimeter... 98% sure here...
    double  x_start = edges.get(0).getX1(), 
            y_start = edges.get(0).getY1();
    Path2D  at_hex  = lowerleft;
    int     state   = 45;

    Path2D shared_hex = null;

    // Determine the stopping conditions
    Path2D hex34 = at_hex; 
    Path2D hex50 = findNeighbor(at_hex, 3);
    boolean keep_going = true;


    while (keep_going) {
      shared_hex = null;
      switch (state) {
        case  1: shared_hex = findNeighbor(at_hex, 1); if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,    1)); state = 12;                      }
                 else                    { edges.add(edge(shared_hex,5)); state = 50; at_hex = shared_hex; }
                 break;

        case 12: shared_hex = findNeighbor(at_hex, 2); if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,    2)); state = 23;                      }
                 else                    { edges.add(edge(shared_hex,0)); state =  1; at_hex = shared_hex; }
                 break;

        case 23: shared_hex = findNeighbor(at_hex, 3); if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,    3)); state = 34;                      }
                 else                    { edges.add(edge(shared_hex,1)); state = 12; at_hex = shared_hex; }
                 break;

        case 34: shared_hex = findNeighbor(at_hex, 4); if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,    4)); state = 45;                       }
                 else                    { edges.add(edge(shared_hex,2)); state = 23; at_hex = shared_hex;  }
                 break;

        case 45: shared_hex = findNeighbor(at_hex, 5); if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,    5)); state = 50;                      }
                 else                    { edges.add(edge(shared_hex,3)); state = 34; at_hex = shared_hex; }
                 break;

        case 50: shared_hex = findNeighbor(at_hex, 0);  if (set.contains(shared_hex) == false) shared_hex = null;
                 if (shared_hex == null) { edges.add(edge(at_hex,     0)); state =  1;                      }
                 else                    { edges.add(edge(shared_hex, 4)); state = 45; at_hex = shared_hex; }
                 break;
      }

      if ((at_hex == hex34 && state == 34) || (hex50 != null && at_hex == hex50 && state == 50)) keep_going = false;
    }

    return edges;
  }

  /** 
   *
   */
  public Path2D createPath2D(List<Line2D> perimeter) {
    Path2D.Double path2d = new Path2D.Double();
    path2d.moveTo(perimeter.get(0).getX1(), perimeter.get(0).getY1());
    for (int i=0;i<perimeter.size();i++) path2d.lineTo(perimeter.get(i).getX2(), perimeter.get(i).getY2());
    path2d.closePath();
    return path2d;
  }

  /**
   * For the specified set, find all the connected hexagons from the seed.  Return this
   * connected component as a set.
   *
   *@param set set to examine -- will be modified to remove all that were added to the return set
   *
   *@return connected component
   */
  public Set<Path2D> findOneConnected(Set<Path2D> set) {
    Set<Path2D> connected = new HashSet<Path2D>(); Queue<Path2D> queue = new LinkedList<Path2D>(); queue.add(set.iterator().next());

    while (queue.size() > 0) {
      Path2D hex = queue.remove(); set.remove(hex); connected.add(hex);
      Iterator<Path2D> it = findAdjacent(hex).iterator();
      while (it.hasNext()) {
        Path2D adj = it.next(); if (set.contains(adj) == true && queue.contains(adj) == false) queue.add(adj);
      }
    }

    return connected;
  }

  /**
   * Create the hexagons, determine which countries overlap, and then resolve conflicts if possible
   */
  public synchronized void createHexagons(boolean full_render) {
    // Local structures to fill... will switch over if this all finishes
    Map<Path2D,String>       my_hex_to_a3    = new HashMap<Path2D,String>(); 
    Map<Path2D,Set<String>>  my_hex_to_a3s   = new HashMap<Path2D,Set<String>>();
    Set<Path2D>              my_all_hex      = new HashSet<Path2D>();
    Map<Path2D,String>       my_hex_to_index = new HashMap<Path2D,String>();
    Map<String,Path2D>       my_index_to_hex = new HashMap<String,Path2D>();

    long ts_start = System.currentTimeMillis();

    // Parameters for hex creation
    double my_hex_xoff = hex_xoff, my_hex_yoff = hex_yoff, my_hex_side = hex_side; // Stupid way to figure out if we need to abort this run
    double hex_height = hexHeight(); 
    double  x       = x_min - hex_side   + hex_xoff - 1.5 * hex_side,
            y       = y_min - hex_height + hex_yoff;
    int     x_index = 0,
            y_index = 0;
    boolean even    = true;

    resetEdgeLookup();

    if (full_render) { System.err.println(); System.err.println("createHexagons( hex_xoff=" + my_hex_xoff + " , hex_yoff=" + my_hex_yoff + " , hex_side=" + my_hex_side + " )"); }

    // Create all possible hexes
    while ((y < y_max + hex_height) && (hex_xoff == my_hex_xoff && hex_yoff == my_hex_yoff && hex_side == my_hex_side)) {
      // Make the hex
      Path2D path = makeHex(x, y, hex_side, hex_height);

      // add to the all hexes
      my_all_hex.add(path); String index_str = x_index + "|" + y_index;
      my_hex_to_index.put(path,index_str); my_index_to_hex.put(index_str,path);

      // Increment to the next hex location
      x += hex_side * 3; x_index++;
      if (x > x_max + hex_side) {
        if (even) { x = x_min - hex_side + hex_xoff;                  x_index = 0; }
        else      { x = x_min - hex_side + hex_xoff - 1.5 * hex_side; x_index = 0; }
        even = !even;
        y += hex_height/2.0; y_index++;
      }
    }

    // ... now figure out the overlap.... and try to resolve...
    // ... Need to thread this... it's slow :(
    if (full_render) {
      Thread threads[] = new Thread[32];
      Queue<Path2D>            hex_queue     = new LinkedList<Path2D>();          hex_queue.addAll(my_all_hex);
      for (int i=0;i<threads.length;i++) { threads[i] = new Thread(new MyRunnable(hex_queue,my_hex_to_a3s)); threads[i].start(); }
      for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }

      // Figure out where an a3 has at least one 
      Set<String> at_least_one = new HashSet<String>();
      Iterator<Path2D> it = my_hex_to_a3s.keySet().iterator(); while (it.hasNext()) {
        Path2D path = it.next(); Set<String> a3s = my_hex_to_a3s.get(path);
        if (a3s.size() == 1) { String a3 = a3s.iterator().next(); at_least_one.add(a3); my_hex_to_a3.put(path,a3); }
      }
      System.err.println("createHexagons():  |at_least_one| = " + at_least_one.size() + " / " + a3_to_shapes.keySet().size());

      // Now find the conflicts... if there are missing ones in the conflict, pick one of those at random and put it in the resolved set
      Set<String> resolved = new HashSet<String>();
      it = my_hex_to_a3s.keySet().iterator(); while (it.hasNext()) {
        Path2D path = it.next(); Set<String> a3s = my_hex_to_a3s.get(path); if (a3s.size() > 1) {
          Set<String> missing = new HashSet<String>();
          Iterator<String> it_a3 = a3s.iterator(); while (it_a3.hasNext()) { String a3 = it_a3.next(); if (at_least_one.contains(a3) == false) missing.add(a3); }
          String assign; if (missing.size() > 0) { assign = random(missing); resolved.add(assign); } else assign = mostOverlap(path, a3s);
          my_hex_to_a3.put(path,assign);
          System.err.print(".");
        }
      }
      System.err.println();
      System.err.println("createHexagons():  |resolved|     = " + resolved.size() + " ... total = " + (at_least_one.size() + resolved.size()) + " / " + a3_to_shapes.keySet().size());

      // Print out countries that don't have any hexes
      Set<String> whats_left = new HashSet<String>(); whats_left.addAll(a3_to_shapes.keySet());
      whats_left.removeAll(at_least_one); whats_left.removeAll(resolved);
      System.err.println("createHexagons(): Whats Left = " + whats_left);

      // Shrink countries with lots of hexes that are on the water ... if the hex only covers a little land
      Set<String> ccs_mapped = new HashSet<String>(); it = my_hex_to_a3s.keySet().iterator(); while (it.hasNext()) ccs_mapped.addAll(my_hex_to_a3s.get(it.next()));

      Set<Path2D> hexes_to_remove = new HashSet<Path2D>(); // to avoid concurrent modification exception

      Iterator<String> it_a3 = ccs_mapped.iterator(); while (it_a3.hasNext()) {
        String a3 = it_a3.next(); int hex_coverage = 0; it = my_hex_to_a3.keySet().iterator(); while (it.hasNext()) if (my_hex_to_a3.get(it.next()).equals(a3)) hex_coverage++;
        // If the country has at least three hexes, let's see if we can remove some that are on the water
        if (hex_coverage > 3) {
          Iterator<Path2D> it_local = my_hex_to_a3.keySet().iterator(); while (it_local.hasNext() && hex_coverage > 3) {
            Path2D hex = it_local.next(); 
            if (my_hex_to_a3.get(hex) != null    && 
                my_hex_to_a3.get(hex).equals(a3) && 
                my_hex_to_a3s.get(hex).size() == 1) {
              if (hexIsMostlyWater(hex,a3)) { hexes_to_remove.add(hex); hex_coverage--; }
            }
          }
        }
      }

      // Remove after to avoid concurrent modification exception
      it = hexes_to_remove.iterator(); while (it.hasNext()) my_hex_to_a3.remove(it.next());
    }

    long ts_end = System.currentTimeMillis(); if (full_render) { System.err.println("createHexagons(): ... time: " + (ts_end - ts_start) + " ms"); }

    if (hex_xoff == my_hex_xoff && hex_yoff == my_hex_yoff && hex_side == my_hex_side) { 
      hex_to_a3s    = my_hex_to_a3s;
      hex_to_a3     = my_hex_to_a3; 
      all_hex       = my_all_hex; 
      hex_to_index  = my_hex_to_index;
      index_to_hex  = my_index_to_hex;
      a3_to_hexmaps = new HashMap<String,Set<Path2D>>();
    }
  }

  /**
   * Find the adjacent hexes... the passed in parameter needs to be a hex in the hex_to_a3 structure.
   *
   *      1-- 1 --2
   *     /         \
   *    0           2
   *   /             \
   *  0               3
   *   \             /
   *    5           3
   *     \         /
   *      5-- 5 --4
   */
  public List<Path2D> findAdjacent(Path2D hex) {
    List<Path2D> list = new ArrayList<Path2D>();
    if (hex_to_index.containsKey(hex)) {
      int xy[] = indices(hex_to_index.get(hex)); String s;
      int x = xy[0], y = xy[1]; /* System.err.println(x + "|" + y); */

      if ((y%2) == 0) {
        s = (x-1) + "|" + (y+1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y+2); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y+1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y-1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y-2); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x-1) + "|" + (y-1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
      } else              {
        s = (x+0) + "|" + (y+1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y+2); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+1) + "|" + (y+1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+1) + "|" + (y-1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y-2); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
        s = (x+0) + "|" + (y-1); if (index_to_hex.containsKey(s)) list.add(index_to_hex.get(s));
      }
    }
    return list;
  }
  // Helper to separate the string coordinates...
  private int[] indices(String str) { 
    int xy[] = new int[2];
    xy[0] = Integer.parseInt(str.substring(0,str.indexOf("|")));
    xy[1] = Integer.parseInt(str.substring(str.indexOf("|")+1,str.length()));
    return xy;
  }

  /**
   * Find the neighbor that shares the specific edge. See the bad ascii rep elsewhere for the numbering.
   */
  public Path2D findNeighbor(Path2D hex, int shared_edge) {
    String s = null;
    if (hex_to_index.containsKey(hex)) {
      int xy[] = indices(hex_to_index.get(hex));
      int x = xy[0], y = xy[1];

      if ((y%2) == 0) {
        if      (shared_edge == 0) s = (x-1) + "|" + (y+1);
        else if (shared_edge == 1) s = (x+0) + "|" + (y+2);
        else if (shared_edge == 2) s = (x+0) + "|" + (y+1);
        else if (shared_edge == 3) s = (x+0) + "|" + (y-1);
        else if (shared_edge == 4) s = (x+0) + "|" + (y-2);
        else                       s = (x-1) + "|" + (y-1);
      } else              {
        if      (shared_edge == 0) s = (x+0) + "|" + (y+1);
        else if (shared_edge == 1) s = (x+0) + "|" + (y+2);
        else if (shared_edge == 2) s = (x+1) + "|" + (y+1);
        else if (shared_edge == 3) s = (x+1) + "|" + (y-1);
        else if (shared_edge == 4) s = (x+0) + "|" + (y-2);
        else                       s = (x+0) + "|" + (y-1);
      }
    }
    if (s != null && index_to_hex.containsKey(s)) return index_to_hex.get(s);
    else                                          return null;
  }

  /**
   * Pick a random element...
   */
  public String random(Set<String> set) {
    if (set.size() == 1) return set.iterator().next();
    List<String> list = new ArrayList<String>(); list.addAll(set);
    int index = ((int) (Math.random() * Integer.MAX_VALUE))%list.size();
    return list.get(index);
  }

  /**
   * Country overlap thread
   */
  class MyRunnable implements Runnable {
    Queue<Path2D> queue; Map<Path2D,Set<String>> my_hex_to_a3s;
    public MyRunnable(Queue<Path2D> queue, Map<Path2D,Set<String>> my_hex_to_a3s) {
      this.queue = queue; this.my_hex_to_a3s = my_hex_to_a3s;
    }
    public Path2D getNext() { Path2D next = null; synchronized (queue) { next = queue.poll(); } return next; }
    public void run() {
      Path2D next = null;
      while ((next = getNext()) != null) {
        Set<String> set = countryOverlaps(next);
        if (set.size() > 0) { synchronized (my_hex_to_a3s) { my_hex_to_a3s.put(next,set); } }
      }
    }
  }

  /**
   * Render Component
   */
  MyComponent my_component;

  /**
   * Bounds of the all of the shapes
   */
  double x_min = Double.POSITIVE_INFINITY,
         y_min = Double.POSITIVE_INFINITY, 
         x_max = Double.NEGATIVE_INFINITY, 
         y_max = Double.NEGATIVE_INFINITY;

  /**
   * world-to-screen and screen-to-world transforms
   */
  double wxToSx(double wx) { return ( (my_component.getWidth() *(wx - x_min)/(x_max - x_min)) ); }
  double sxToWx(int    sx) { return ((sx * (x_max - x_min)) / my_component.getWidth()) + x_min; }

  double wyToSy(double wy) { return ( my_component.getHeight() - (my_component.getHeight()*(wy - y_min)/(y_max - y_min)) ); }
  double syToWy(int    sy) { int h = my_component.getHeight();
                             return (h * (y_max - y_min) - sy * (y_max - y_min))/h + y_min; }

  /**
   * Apply the world to screen transform on a Path2D.
   */
  Path2D worldPath2DToScreenPath2D(Path2D wpath) {
    Path2D spath = new Path2D.Double(); PathIterator iterator = wpath.getPathIterator(null,1.0); while (iterator.isDone() == false) {
      double points[] = new double[6];
      int type = iterator.currentSegment(points);
      if        (type == PathIterator.SEG_MOVETO) { spath.moveTo(wxToSx(points[0]),wyToSy(points[1]));
      } else if (type == PathIterator.SEG_LINETO) { spath.lineTo(wxToSx(points[0]),wyToSy(points[1]));
      }
      iterator.next();
    }
    return spath;
  }

  /**
   * Apply the world to screen transform on a Line2D.
   */
  Line2D worldLine2DToScreenLine2D(Line2D wline) {
    return new Line2D.Double(wxToSx(wline.getX1()), wyToSy(wline.getY1()),
                             wxToSx(wline.getX2()), wyToSy(wline.getY2()));
  }

  /**
   * My component ... mostly just handles the direct render and takes the mouse motions for adjusting the hexagonal grid
   */
  class MyComponent extends JComponent implements MouseListener, MouseMotionListener, MouseWheelListener{
    public MyComponent() { addMouseListener(this); addMouseMotionListener(this); addMouseWheelListener(this); }
    @Override
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g; g2d.setColor(Color.black); g2d.fillRect(0,0,getWidth(),getHeight());
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // Draw the regular world map
      if (draw_background_cb.isSelected()) {
        Iterator<ShapeRecord> it_rec = shape_file.shapeRecordIterator(); while (it_rec.hasNext()) {
          ShapeRecord record = it_rec.next(); if (shape_to_a3.containsKey(record) == false) continue;
          for (int i=0;i<record.getNumberOfShapes();i++) {
            Shape shape = record.getShape(i); if (shape instanceof Path2D) {
              Path2D spath = worldPath2DToScreenPath2D((Path2D) shape);
              g2d.setColor(RTColorManager.getColor(shape_to_a3.get(record))); g2d.fill(spath);
            } else System.err.println("Not A Path2D");
          }
        }
      }

      // Draw one hex for sizing
      g2d.setColor(Color.white);
      g2d.draw(worldPath2DToScreenPath2D(makeHex(-160.0, -60.0, hex_side, hexHeight())));

      Composite orig_comp = g2d.getComposite();

      // Draw the hex version
      if (mouse_pressed || draw_all_hexes_cb.isSelected()) {
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
        Iterator<Path2D> it_hex = all_hex.iterator(); while (it_hex.hasNext()) {
          Path2D path2d = it_hex.next();
          g2d.setColor(Color.white); g2d.draw(worldPath2DToScreenPath2D(path2d));
        } 
        g2d.setComposite(orig_comp);
      } 

      // Draw the hex version
      if (draw_a3_hexes_cb.isSelected()) {
        if (draw_background_cb.isSelected()) g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
        Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) {
          Path2D path2d = it_hex.next();
          g2d.setColor(RTColorManager.getColor(hex_to_a3.get(path2d)));                          g2d.fill(worldPath2DToScreenPath2D(path2d));
          g2d.setColor(BrewerColorScale.darken(RTColorManager.getColor(hex_to_a3.get(path2d)))); g2d.draw(worldPath2DToScreenPath2D(path2d));
        }
        g2d.setComposite(orig_comp);
      }

      // Draw the hex version
      if (draw_hexmap_cb.isSelected()) {
        g2d.setColor(Color.black);
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.75f));
        g2d.fillRect(0,0,getWidth(),getHeight());
        g2d.setComposite(orig_comp);

        Iterator<String> it_a3 = a3_to_hexmaps.keySet().iterator(); while (it_a3.hasNext()) {
          String a3 = it_a3.next(); Iterator<Path2D> it_hexmap = a3_to_hexmaps.get(a3).iterator();
          while (it_hexmap.hasNext()) {
            Path2D hexmap = it_hexmap.next();
            g2d.setColor(RTColorManager.getColor(a3));                          g2d.fill(worldPath2DToScreenPath2D(hexmap));
            g2d.setColor(BrewerColorScale.darken(RTColorManager.getColor(a3))); g2d.draw(worldPath2DToScreenPath2D(hexmap));
          }
        }
      }

      //
      // Draw the overhex information
      //
      Path2D over_hex_copy = over_hex; g2d.setFont(new Font("Lucida", Font.PLAIN, 18));
      if (over_hex_copy != null && hex_to_a3.containsKey(over_hex_copy)) {

        // Highlights this country and it's related hexes
       if (highlight_countries_cb.isSelected()) {
        String over_a3 = hex_to_a3.get(over_hex_copy); if (over_a3 != null) {
          g2d.setColor(Color.black); g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
          g2d.fillRect(0,0,getWidth(),getHeight());
          g2d.setComposite(orig_comp);

          g2d.setColor(Color.white); int hex_coverage = 0; Set<Path2D> a3_hexes = new HashSet<Path2D>();
          Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) {
            Path2D hex = it_hex.next(); if (hex_to_a3.get(hex).equals(over_a3)) {
              g2d.draw(worldPath2DToScreenPath2D(hex)); hex_coverage++; a3_hexes.add(hex);
            }
          }

          // Draw the perimeter of each of the shapes... more for debugging...
          /*
          while (a3_hexes.size() > 0) {
            Set<Path2D>  connected = findOneConnected(a3_hexes);
            List<Line2D> perimeter = calcPerimeter(connected);
            g2d.setColor(Color.red);
            for (int i=0;i<perimeter.size();i++) g2d.draw(worldLine2DToScreenLine2D(perimeter.get(i)));
            if (perimeter.size() > 0) {
              int sx = (int) wxToSx(perimeter.get(perimeter.size()-1).getX2()),
                  sy = (int) wyToSy(perimeter.get(perimeter.size()-1).getY2());
              g2d.drawLine(sx-2,sy,sx+2,sy); g2d.drawLine(sx,sy-2,sx,sy+2);
            }
          }
          */

          g2d.drawString(over_a3.toUpperCase() + " | hexes = " + hex_coverage, 5, getHeight() - 15 - Utils.txtH(g2d,"1"));
        }
       }

        // Shows the possible a3s for this hex
        if (hex_to_a3s.containsKey(over_hex_copy)) {
          g2d.setColor(Color.white);
          g2d.drawString(hex_to_a3s.get(over_hex_copy).toString(), 5, getHeight() - 5);
        }

        // For debugging the findAdjacent() method
        /* 
        List<Path2D> adjacents = findAdjacent(over_hex_copy); 
        for (int i=0;i<adjacents.size();i++) {
          Path2D to_screen = worldPath2DToScreenPath2D(adjacents.get(i));

          g2d.setColor(Color.red);    g2d.fill(to_screen);
          g2d.setColor(Color.white);  g2d.drawString("" + i, (int) (to_screen.getBounds2D().getCenterX()), (int) (to_screen.getBounds2D().getCenterY()));
        }
        */
      }

      //
      // Show missing countries
      //
      if (show_missing_countries_cb.isSelected()) {
        // which countries are mapped?
        Set<String> have_mapping = new HashSet<String>();
        Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) have_mapping.add(hex_to_a3.get(it_hex.next()));

        // Render a list along the left-hand side with lines to the center of the country area
        g2d.setColor(Color.white);
        int txt_h = Utils.txtH(g2d,"1"); int y = 5 + txt_h;
        Iterator<String> it_a3 = a3_to_area.keySet().iterator(); while (it_a3.hasNext()) {
          String a3 = it_a3.next(); if (have_mapping.contains(a3) == false) {
            g2d.drawString(a3, 5, y);
            Rectangle2D bounds = a3_to_area.get(a3).getBounds2D();
            g2d.drawLine(10 + Utils.txtW(g2d,a3),           y - txt_h/2,
                         (int) wxToSx(bounds.getCenterX()), (int) wyToSy(bounds.getCenterY()));
            Set<ShapeRecord> a3_shapes = a3_to_shapes.get(a3); Iterator<ShapeRecord> it_shape = a3_shapes.iterator(); while (it_shape.hasNext()) {
              ShapeRecord shape = it_shape.next(); for (int i=0;i<shape.getNumberOfShapes();i++) g2d.draw(worldPath2DToScreenPath2D((Path2D) shape.getShape(i)));
            }
            y += txt_h;
          }
        }
      }
    }

    /**
     * Determine which countries are missing from the hex mapping...
     */
    Set<String> missingA3s() {
      Set<String> have_mapping = new HashSet<String>(), missing = new HashSet<String>();
      Iterator<Path2D> it_hex = hex_to_a3.keySet().iterator(); while (it_hex.hasNext()) have_mapping.add(hex_to_a3.get(it_hex.next()));
      Iterator<String> it_a3 = a3_to_area.keySet().iterator(); while (it_a3.hasNext()) {
        String a3 = it_a3.next(); if (have_mapping.contains(a3) == false) missing.add(a3);
      }
      return missing;
    }

    @Override
    public void mousePressed(MouseEvent me)  { if (me.getButton() == MouseEvent.BUTTON3) mouse_pressed = true;  repaint(); }

    @Override
    public void mouseReleased(MouseEvent me) { mouse_pressed = false; repaint(); }

    /**
     * Find the hexagon underneath the specified coordinates. ... choose from the hex_to_a3s first...
     * ... if those don't match, then choose from the complete hex map...
     */
    public Path2D findHexKey(double wx, double wy) {

      Iterator<Path2D> it_hex = hex_to_a3s.keySet().iterator(); while (it_hex.hasNext()) {
        Path2D hex = it_hex.next(); if (hex.contains(wx,wy)) return hex;
      }

      it_hex = all_hex.iterator(); while (it_hex.hasNext()) {
        Path2D hex = it_hex.next(); if (hex.contains(wx,wy)) return hex;
      }

      return null;
    }

    /**
     * Upon mouse click, cycle through the countries assigned to the hex.
     */
    @Override
    public void mouseClicked(MouseEvent me)  { 
      Path2D found_hex = findHexKey(sxToWx(me.getX()), syToWy(me.getY()));

      // Is it found?
      if (found_hex != null) {
        Set<String> set_to_use = null;

        // If there's an underlying set of a3's, use those.  Else use the a3s that haven't been assigned
        if (hex_to_a3s.containsKey(found_hex)) {
          set_to_use = hex_to_a3s.get(found_hex);
        } else { 
          set_to_use = missingA3s(); 
          if (hex_to_a3.containsKey(found_hex)) set_to_use.add(hex_to_a3.get(found_hex)); 
          if (set_to_use.size() == 0)           set_to_use = null; 
        }

        if (set_to_use != null) {
          List<String> sort = new ArrayList<String>(); sort.addAll(set_to_use); Collections.sort(sort);

          // It's blank -- use the first element of the list
          if (hex_to_a3.containsKey(found_hex) == false) {
            hex_to_a3.put(found_hex,sort.get(0));

          // Else, increment through the elements... if it's past the last, empty it
          } else {

            int index = sort.indexOf(hex_to_a3.get(found_hex));

            if (index == sort.size()-1) hex_to_a3.remove(found_hex); 
            else                        hex_to_a3.put(found_hex, sort.get(index+1));

          }
        }
      }

      repaint(); 
    }

    @Override
    public void mouseEntered(MouseEvent me)  { mouse_pressed = false; over_hex = null; repaint(); }

    @Override
    public void mouseExited(MouseEvent me)   { mouse_pressed = false; over_hex = null; repaint(); }

    /**
     * Mouse is currently pressed
     */
    boolean mouse_pressed = false; 

    /**
     * Last recorded mouse x coordinate
     */
    int     mx = 0, 

    /**
     * Last recorded mouse y coordinate
     */
            my = 0;

    /**
     * Which hex are we currentl over?
     */
    Path2D over_hex = null;

    /**
     * Track which hex the mouse is over -- ask for repaints as it changes.
     */
    @Override
    public void mouseMoved(MouseEvent me) { 
      mx = me.getX(); my = me.getY();
      Path2D found_hex = findHexKey(sxToWx(me.getX()), syToWy(me.getY()));
      if (found_hex != over_hex) {
        over_hex = found_hex;
        repaint();
      }
    }

    /**
     *
     */
    @Override
    public void mouseDragged(MouseEvent me) {
     if (mouse_pressed) {
      int dx = me.getX() - mx, 
          dy = me.getY() - my;

      hex_xoff += dx/20.0;
      hex_yoff -= dy/20.0;

      createHexagons(false);
      repaint();

     }
     mx = me.getX(); my = me.getY();
    }
    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
      hex_side += mwe.getWheelRotation()/20.0;
      createHexagons(false);
      repaint();
    }
  }

  /**
   * Export the current shapes as a CSV file
   * ... not really CSV since the line lengths won't be fixed
   */
  public void exportShapesAsCSV() {
    StringBuffer lines = new StringBuffer();

    createCountryAreasFromHexMap();
    Iterator<String> it_a3 = a3_to_hexmaps.keySet().iterator(); while (it_a3.hasNext()) {
      String a3 = it_a3.next(); 

      Iterator<Path2D> it_path2d = a3_to_hexmaps.get(a3).iterator(); int path_i = 0;
      while (it_path2d.hasNext()) {
        Path2D path2d = it_path2d.next();
        double as_point_array[] = asPointArray(path2d);

        lines.append(a3);
        for (int i=0;i<as_point_array.length;i++) lines.append("," + as_point_array[i]);
        lines.append("\n");
      }
    }

    try {
     PrintStream out = new PrintStream(new FileOutputStream(new File(System.currentTimeMillis() + "made_hexmaps.txt"))); out.println(lines.toString()); out.close();
    } catch (IOException ioe) { System.err.println("IOException: " + ioe); ioe.printStackTrace(System.err); }
  }

  Map<String,Set<Double[]>> something;

  /**
   * Convert a path2d to a point array... only works with the path2d's created in this class...
   */
  protected double[] asPointArray(Path2D path2d) {
    List<Double> list = new ArrayList<Double>();

    // Iterator over the path elements
    PathIterator it = path2d.getPathIterator(null,1.0); while (it.isDone() == false) {
      double points[] = new double[6];
      int type = it.currentSegment(points);
      if        (type == PathIterator.SEG_MOVETO) { 
        if (list.size() > 0) System.err.println("asPointArray() | SEG_MOVETO | Shouldn't Occur Except At First Point...");
        list.add(points[0]); list.add(points[1]);
      } else if (type == PathIterator.SEG_LINETO) { 
        list.add(points[0]); list.add(points[1]);
      }
      it.next();
    }
    
    // Convert to array and return
    double array[] = new double[list.size()]; for (int i=0;i<list.size();i++) array[i] = list.get(i); return array;
  }

  /**
   *
   */
  public static void main(String args[]) {
    try {
      if (args.length != 2 && args.length != 3) { System.err.println("Usage:  java MakeAHexMap shape-file csv-mapping-file [save-a-hex.txt]"); } else {

        // Check for a load file
        File make_a_hex_file = null; 
        if (args.length == 3) {
          make_a_hex_file = new File(args[2]);
          if (make_a_hex_file.exists() == false) make_a_hex_file = null;
        }

        // Create the application
        new MakeAHexMap(new File(args[0]), new File(args[1]), make_a_hex_file);
      }
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }
}

