/*

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import racetrack.framework.BundlesDT;
import racetrack.util.Utils;

/**
 * Class to render the world as a hexagonal map where each country is represented
 * by the exact same amount of space.  Relative locations are approximate.  The source 
 * for this representation can be found at the following URL:
 *
 * https://questionsindataviz.com/2017/11/05/how-do-you-tile-the-world/
 *
 * Original creator of this specific representation:  Neil Richards
 *
 */
public class WorldHexMap extends WorldMap {
  /**
   * Construct the map
   */
  public WorldHexMap() {
    super(); 

    // Hex mappings...
    String[][] x_strs = {
      { "can", "usa", "mex", "gtm", "slv" },
      { "blz", "hnd", "nic" },
      { "cri" },
      { "bhs", "cub", "jam", "pan" },
      { "hti", "col", "ecu", "per" },
      { "dom", "kna", "abw", "ven", "guy", "bol", "pry", "chl", "arg" },
      { "atg", "lca", "vct", "dma", "grd", "tto", "sur", "ury" },
      { "brb", "bra" },
      { },
      { "irl", "cpv" },
      { "isl", "gbr", "prt", "mlt", "mrt", "gnb", "gin", "stp" },
      { "fra", "esp", "mar", "gmb", "sle", "lbr", "civ" },
      { "nld", "bel", "lux", "ita", "dza", "sen", "bfa", "gha", "nga", "gnq", "ago" },
      { "dnk", "deu", "che", "svn", "hrv", "tun", "mli", "tcd", "tgo", "cmr", "cog", "gab", "zmb" },
      { "nor", "pol", "cze", "aut", "bih", "mne", "alb", "lby", "ner", "ssd", "ben", "cod", "bdi", "mwi", "bwa", "nam" },
      { "swe", "ltu", "svk", "hun", "srb", "grc", "egy", "sdn", "eri", "caf", "uga", "rwa", "moz", "zwe", "swz", "zaf" },
      { "fin", "est", "lva", "blr", "ukr", "rou", "bgr", "mkd", "cyp", "dji", "eth", "ken", "tza", "lso" },
      { "mda", "tur", "jor", "lbn", "isr", "yem", "som", "syc", "com" },
      { "syr", "sau", "qat", "omn", "mdg", "mus" },
      { "arm", "irq", "irn", "bhr", "are", "kwt" },
      { "geo", "aze", "pak", "mdv" },
      { "uzb", "tkm", "afg", "ind", "lka" },
      { "kgz", "tjk", "bgd", "npl" },
      { "kaz", "chn", "btn", "mmr", "tha", "mys", "sgp", "aus" },
      { "rus", "mng", "prk", "kor", "lao", "khm", "brn", "idn", "tls", "plw", "png" },
      { "vnm", "phl", "fsm", "nru", "slb", "vut", "nzl" },
      { "jpn", "twn", "kir", "tuv", "fji" },
      { "wsm", "ton", "mhl" } };

    String[][] y_strs = {
      { "can", "isl", "nor", "fin", "bhs", "swe" },
      { "usa", "est", "cub", "dnk" },
      { "mex", "pol", "lva", "jam", "dom", "irl", "deu", "ltu" },
      { "gtm", "hti", "atg", "gbr", "nld", "cze", "blr", "rus", "kna", "fra", "che", "svk", "mda", "kaz" },
      { "slv", "lca", "bel", "aut", "ukr", "mng", "blz", "brb", "esp", "svn", "hun", "arm", "uzb", "chn" },
      { "vct", "prt", "lux", "bih", "rou", "geo", "kgz", "prk", "jpn", "hnd", "hrv", "srb", "tur", "irq", "tkm", "btn" },
      { "abw", "dma", "ita", "mne", "bgr", "syr", "aze", "tjk", "kor", "twn", "nic", "jor", "irn", "afg", "mmr" } ,
      { "cri", "grd", "mlt", "alb", "mkd", "pak", "bgd", "pan", "ven", "grc", "lbn", "bhr", "vnm" },
      { "col", "tto", "sau", "npl", "lao", "guy", "isr", "are", "ind", "tha" },
      { "ecu", "dza", "lby", "cyp", "qat", "khm", "bol", "mar", "tun", "egy", "yem", "lka", "mys", "phl", "kwt" },
      { "per", "sur", "sen", "ner", "omn", "pry", "bra", "gmb", "mli", "sdn" },
      { "ury", "mrt", "bfa", "ssd", "dji", "brn", "chl", "sle", "tcd", "eri", "sgp" },
      { "gnb", "gha", "ben", "eth", "mdv", "idn", "arg", "lbr", "tgo", "caf", "som" },
      { "gin", "nga", "cod", "ken", "tls", "cpv", "civ", "cmr", "uga" },
      { "gnq", "bdi", "tza", "cog", "rwa", "fsm", "mhl" },
      { "stp", "ago", "mwi", "plw", "gab", "moz", "syc", "nru" },
      { "bwa", "png", "kir", "zmb", "zwe", "com", "slb", "wsm" },
      { "nam", "mdg", "tuv", "swz", "aus", "vut", "ton" },
      { "lso", "mus", "fji", "zaf" },
      { "nzl" } };

    // Put the a3's into maps for x and y coordinates
    x_map = new HashMap<String,Integer>();
    for (int x=0;x<x_strs.length;x++) {
      for (int i=0;i<x_strs[x].length;i++) x_map.put(x_strs[x][i], x);
    }
    y_map = new HashMap<String,Integer>();
    for (int y=0;y<y_strs.length;y++) {
      for (int i=0;i<y_strs[y].length;i++) y_map.put(y_strs[y][i], y);
    }

    // Double check we got that right...
    Set<String> all = new HashSet<String>();
    all.addAll(x_map.keySet()); all.addAll(y_map.keySet());

    if (all.size() != x_map.keySet().size() ||
        all.size() != y_map.keySet().size()) throw new RuntimeException("Transposition Error!");

    // Track the mins and maxes for the bounds
    double min_x, min_y, max_x, max_y;
    max_x = max_y = Double.NEGATIVE_INFINITY;
    min_x = min_y = Double.POSITIVE_INFINITY;

    // Make the shapes
    double side   = 32.0;
    double height = Math.sqrt(3.0 * side * side / 4.0);
    Iterator<String> it = all.iterator(); while (it.hasNext()) {
      String a3 = it.next(); a3codeLookup.put(a3.toLowerCase(),a3.toLowerCase()); a3codeLookup.put(a3.toUpperCase(),a3.toLowerCase());
      int    x_i = x_map.get(a3), y_i = y_map.get(a3);
      double x   = (double) x_i * side * 1.5;
      double y   = (double) y_i * height;
      if ((x_i%2) == 1) y += height/2.0;

      Path2D path = new Path2D.Double();
      path.moveTo(x - side,      y);
      path.lineTo(x - side/2.0,  y + height/2.0);
      path.lineTo(x + side/2.0,  y + height/2.0);
      path.lineTo(x + side,      y);
      path.lineTo(x + side/2.0,  y - height/2.0);
      path.lineTo(x - side/2.0,  y - height/2.0);
      path.closePath();

      shape_map.put(a3, path);

      if (max_x < x+side)   max_x = x+side;
      if (max_y < y+height) max_y = y+height;
      if (min_x > x-side)   min_x = x-side;
      if (min_y < y-height) min_y = y-height;
    }

    bounds = new Rectangle2D.Double(min_x, min_y, max_x + side/2 - min_x, max_y - min_y);

    // Get conversions for the country names and 2-letter codes to the three letter codes
    // ... do some checking to make sure that we aren't overwriting codes or that we aren't missing shapes
    for (int i=0;i<iso_lookups.length;i++) {
      String /* country_name        = iso_lookups[i][0].toLowerCase(), */
             /* official_state_name = iso_lookups[i][1].toLowerCase(), */
             /* a2code              = iso_lookups[i][3].toLowerCase(), */
             a3code              = iso_lookups[i][4].toLowerCase();

      if (sovereign_set.contains(a3code) == false) {
        if (shape_map.containsKey(a3code) == false) { 
          String err_msg = "No shape found for a3code \"" + a3code + "\"...";
          if (err_msg_shown.contains(err_msg) == false) { System.err.println(err_msg); err_msg_shown.add(err_msg); }
        }
      }
    }
  }

  /**
   * bounds of the world
   */
  private Rectangle2D bounds;

  /**
   * Return the bounds of the world.
   *
   *@return rectangular boundary
   */
  @Override
  public Rectangle2D getBounds() { return bounds; }

  /**
   * Error messages already shown ,,, globally
   */
  static Set<String> err_msg_shown = new HashSet<String>();

  /**
   * Country code to the hexagon shape
   */
  Map<String,Shape> shape_map = new HashMap<String,Shape>();

  /**
   * Country code to the column
   */
  Map<String,Integer> x_map,

  /**
   * Country code to the row... note that the hexagons are
   * interweaved...
   */
                      y_map;

  /**
   * Return an iterator over the country codes.
   *
   *@return iterator over country code strings
   */
  @Override
  public Iterator<String> countryCodeIterator() {
    return shape_map.keySet().iterator();
  }

  /**
   * Return the shapes for a country code.
   *
   *@param cc country code
   *
   *@return set of shapesshape
   */
  @Override
  public Set<Shape> getCountryShapes(String cc) {
    if (shape_map.containsKey(cc)) { return asSet(shape_map.get(cc)); } else {
      String err_msg = "No shape found for \"" + cc + "\" [2]...";
      if (err_msg_shown.contains(err_msg) == false) {
        System.err.println(err_msg);
        err_msg_shown.add(err_msg);
      }
      return null;
    }
  }
  private Set<Shape> asSet(Shape shape) { Set<Shape> set = new HashSet<Shape>(); set.add(shape); return set; }

  /**
   * Test main ... makes the image and saves it out to a file.
   */
  public static void main(String args[]) {
    WorldHexMap   whm = new WorldHexMap();
    BufferedImage bi  = new BufferedImage(1400, 650, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2d = (Graphics2D) bi.getGraphics(); g2d.setColor(Color.darkGray); g2d.fillRect(0,0,bi.getWidth(),bi.getHeight());
    g2d.translate(50, 50);
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    Iterator<String> it = whm.countryCodeIterator(); while (it.hasNext()) {
      String cc = it.next();
      Shape shape = whm.getCountryShapes(cc).iterator().next(); // assumes one shape per country which is valid for this representation...
      g2d.setColor(Color.black);
      g2d.fill(shape);
      g2d.setColor(Color.lightGray);
      g2d.draw(shape);

      g2d.setColor(RTColorManager.getColor(whm.getRegion(cc)));
      Rectangle2D rect = shape.getBounds();
      g2d.drawString(cc, (int) (rect.getCenterX() - Utils.txtW(g2d, cc)/2.0), (int) (rect.getCenterY() + Utils.txtH(g2d, cc)/2.0));
    }

    try {
      ImageIO.write(bi, "PNG", new FileOutputStream(new File("test.png")));
    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
    }
  }
}

