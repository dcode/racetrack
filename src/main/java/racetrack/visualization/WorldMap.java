/*

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Shape;

import java.awt.geom.PathIterator;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import racetrack.framework.BundlesDT;

/**
 * Abstract version of a world map that has the base information (country-level information).
 * ... provides the necessary framework for downstream classes to visualization world map information.
 */
public abstract class WorldMap {
  /**
   * Construct the map
   */
  public WorldMap() {
    // Organize into regions
    String regions[][] = {
     { "can", "usa", "mex" },                             // North America
     { "gtm", "slv", "blz", "hnd", "nic", "cri", "pan" }, // Central America
     { "ven", "col", "guy", "ecu", "bol", "per", "pry",
       "sur", "bra", "ury", "chl", "arg" },               // South America
     { "bhs", "cub", "jam", "hti", "dom", "atg", "kna",
       "lca", "brb", "vct", "dma", "grd", "tto", "abw" }, // Caribbean
     { "irl", "isl", "gbr", "prt", "mlt", "fra",
       "esp", "nld", "bel", "lux", "ita", "dnk", "deu",
       "che", "svn", "hrv", "nor", "pol", "cze", "aut",
       "bih", "mne", "alb", "swe", "ltu", "svk", "hun",
       "srb", "grc", "fin", "est", "lva", "blr", "ukr",
       "rou", "bgr", "mkd", "cyp", "mda" },               // Europe
     { "cpv", "mrt", "gnb", "gin", "stp", "mar", "gmb",
       "sle", "lbr", "civ", "dza", "sen", "bfa", "gha",
       "nga", "gnq", "ago", "tun", "mli", "tcd", "tgo",
       "cmr", "cog", "gab", "zmb", "lby", "ner", "ssd",
       "ben", "cod", "bdi", "mwi", "bwa", "nam", "egy",
       "sdn", "eri", "caf", "uga", "rwa", "moz", "zwe",
       "swz", "zaf", "dji", "eth", "ken", "tza", "lso",
       "som", "syc", "com", "mdg", "mus" },               // Africa
     { "tur", "jor", "lbn", "isr", "yem", "syr", "sau",
       "qat", "omn", "arm", "irq", "irn", "bhr", "are",
       "geo", "aze", "pak", "mdv", "uzb", "tkm", "afg",
       "ind", "lka", "kgz", "tjk", "bgd", "npl", "kaz",
       "chn", "btn", "mmr", "tha", "mys", "sgp", "rus",
       "mng", "prk", "kor", "lao", "khm", "brn", "idn",
       "tls", "vnm", "phl", "jpn", "twn", "kwt" },        // Asia
     { "aus", "plw", "png", "fsm", "nru", "slb", "vut",
       "nzl", "kir", "tuv", "fji", "wsm", "ton", "mhl" } // Oceania
    };

    fillRegionLookup(regions[0], "n america");
    fillRegionLookup(regions[1], "c america");
    fillRegionLookup(regions[2], "s america");
    fillRegionLookup(regions[3], "caribbean");
    fillRegionLookup(regions[4], "europe");
    fillRegionLookup(regions[5], "africa");
    fillRegionLookup(regions[6], "asia");
    fillRegionLookup(regions[7], "oceania");

    /**
     * Map 3-letter codes to their sovereign state ... avoids having tons of small islands as hexagons
     */
    a3codeLookup.put("aia", "gbr"); // Great Britain
    a3codeLookup.put("ala", "fra"); // France
    a3codeLookup.put("asm", "usa"); // United States
    a3codeLookup.put("atf", "fra"); // France
    a3codeLookup.put("awb", "nld"); // Netherlands
    a3codeLookup.put("bes", "nld"); // Netherlands
    a3codeLookup.put("blm", "fra"); // France
    a3codeLookup.put("bmu", "gbr"); // Great Britain
    a3codeLookup.put("bvt", "nor"); // Norway
    a3codeLookup.put("cck", "aus"); // Australia
    a3codeLookup.put("cok", "nzl"); // New Zealand
    a3codeLookup.put("cuw", "nld"); // Netherlands
    a3codeLookup.put("cxr", "aus"); // Australia
    a3codeLookup.put("cym", "gbr"); // Great Britain
    a3codeLookup.put("flk", "gbr"); // Great Britain
    a3codeLookup.put("fro", "dnk"); // Denmark
    a3codeLookup.put("gib", "gbr"); // Great Britain
    a3codeLookup.put("glp", "fra"); // France
    a3codeLookup.put("grl", "dnk"); // Denmark
    a3codeLookup.put("guf", "fra"); // France
    a3codeLookup.put("gum", "usa"); // United States
    a3codeLookup.put("hkg", "chn"); // China
    a3codeLookup.put("hmd", "aus"); // Australia
    a3codeLookup.put("iot", "gbr"); // Great Britain
    a3codeLookup.put("mac", "chn"); // China
    a3codeLookup.put("maf", "fra"); // France
    a3codeLookup.put("mnp", "usa"); // United States
    a3codeLookup.put("msr", "gbr"); // Great Britain
    a3codeLookup.put("mtq", "fra"); // France
    a3codeLookup.put("myt", "fra"); // France
    a3codeLookup.put("ncl", "fra"); // France
    a3codeLookup.put("nfk", "aus"); // Australia
    a3codeLookup.put("niu", "nzl"); // New Zealand
    a3codeLookup.put("pcn", "gbr"); // Great Britain
    a3codeLookup.put("pri", "usa"); // United States
    a3codeLookup.put("pyf", "fra"); // France
    a3codeLookup.put("reu", "fra"); // France
    a3codeLookup.put("sgs", "gbr"); // Great Britain
    a3codeLookup.put("shn", "gbr"); // Great Britain
    a3codeLookup.put("sjm", "nor"); // Norway
    a3codeLookup.put("spm", "fra"); // France
    a3codeLookup.put("sxm", "nld"); // Netherlands
    a3codeLookup.put("tca", "gbr"); // Great Britain
    a3codeLookup.put("tkl", "nzl"); // New Zealand
    a3codeLookup.put("wlf", "fra"); // France
    a3codeLookup.put("umi", "usa"); // United States
    a3codeLookup.put("vgb", "gbr"); // Great Britain
    a3codeLookup.put("vir", "usa"); // United States

    // Not sure what to do with these
    // and -- andorra
    // ata -- antarctica
    // esh -- western sahara
    // ggy -- guernsey (british crown)
    // imn -- isle of man (british crown)
    // jey -- jersey (british crown)
    // lie -- liechtenstein
    // mco -- monaco
    // smr -- san marino
    // vat -- vatican
    // pse -- palestine

    // Found these through trial and error...
    a3codeLookup.put("united states",       "usa");
    a3codeLookup.put("philippines",         "phl");
    a3codeLookup.put("republic of korea",   "kor");
    a3codeLookup.put("bahamas",             "bhs");
    a3codeLookup.put("netherlands",         "nld");
    a3codeLookup.put("russian federation",  "rus");
    a3codeLookup.put("czech republic",      "cze");
    a3codeLookup.put("united kingdom",      "gbr");
    a3codeLookup.put("macau",               "chn");
    a3codeLookup.put("united arab emirates",         "are");
    a3codeLookup.put("saint vincent and grenadines", "vct");

    sovereign_set.addAll(a3codeLookup.keySet());

    // Get conversions for the country names and 2-letter codes to the three letter codes
    // ... do some checking to make sure that we aren't overwriting codes or that we aren't missing shapes
    for (int i=0;i<iso_lookups.length;i++) {
      String country_name        = iso_lookups[i][0].toLowerCase(),
             official_state_name = iso_lookups[i][1].toLowerCase(),
             // sovereignty         = iso_lookups[i][2].toLowerCase(),
             a2code              = iso_lookups[i][3].toLowerCase(),
             a3code              = iso_lookups[i][4].toLowerCase();
  
      // Save the row off
      a3_to_row.put(a3code.toLowerCase(), iso_lookups[i]);

      if (sovereign_set.contains(a3code) == false) {
        if (a3codeLookup.containsKey(country_name) &&
            a3codeLookup.get(country_name).equals(a3code)        == false) System.err.println("a3codeLookup() -- already have \"" + country_name + "\"...");
        a3codeLookup.put(country_name,        a3code);

        if (a3codeLookup.containsKey(official_state_name) &&
            a3codeLookup.get(official_state_name).equals(a3code) == false) System.err.println("a3codeLookup() -- already have \"" + official_state_name + "\"...");
        a3codeLookup.put(official_state_name, a3code);

        if (a3codeLookup.containsKey(a2code) &&
            a3codeLookup.get(a2code).equals(a3code)              == false) System.err.println("a3codeLookup() -- already have \"" + a2code + "\"...");
        a3codeLookup.put(a2code,              a3code);

      } else {
        String sovern_lu = a3codeLookup.get(a3code);      // Map them all to the sovereign code
        a3codeLookup.put(country_name,        sovern_lu);
        a3codeLookup.put(official_state_name, sovern_lu);
        a3codeLookup.put(a2code,              sovern_lu);
      }
    }
  }

  /**
   * Return the bounds of the world.
   *
   *@return rectangular boundary
   */
  public abstract Rectangle2D getBounds();

  /**
   * Convert a world x coordinate to a screen coordinate.  Uses the bounds of this class' shapes.
   */
  public int wxToSx(double wx, int sw) {
    return (int) (sw * (wx - getBounds().getX())/(getBounds().getWidth()));
  }

  /**
   * Convert a world x coordinate to a screen coordinate.  Uses the bounds of this class' shapes.
   */
  public int wyToSy(double wy, int sh) {
    return (int) (sh - (sh * (wy - getBounds().getY())/(getBounds().getHeight())));
  }

  /**
   * Converts a world shape to a screen shape.  Only works with Path2D shapes...
   *
   *@param wshape shape to convert in world coordinates
   *@param sw     screen width
   *@param sh     screen height
   *
   *@return shape in screen coordinates
   */
  public Shape worldShapeToScreenShape(Shape wshape, int sw, int sh) {
    if (wshape instanceof Path2D == false) throw new RuntimeException("Not a Path2D... no conversion implemented");
    Path2D wpath = (Path2D) wshape;

    Path2D spath = new Path2D.Double(); PathIterator iterator = wpath.getPathIterator(null,1.0); while (iterator.isDone() == false) {
      double points[] = new double[6];
      int type = iterator.currentSegment(points);
      if        (type == PathIterator.SEG_MOVETO) { spath.moveTo(wxToSx(points[0],sw),wyToSy(points[1],sh));
      } else if (type == PathIterator.SEG_LINETO) { spath.lineTo(wxToSx(points[0],sw),wyToSy(points[1],sh));
      }
      iterator.next();
    }
    return spath;
  }

  /**
   * Converts two-letter codes to three-letter codes (three letter codes are what is used for the geometry)
   * ... tries with both country name and official state name... but those are going to be hard to get an exact match
   */
  Map<String,String> a3codeLookup = new HashMap<String,String>();

  /**
   * Sovereign set
   */
  Set<String> sovereign_set = new HashSet<String>();

  //
  // Derived from https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
  //
  // {"Country Name","Official State Name","Sovereignty","a2code","a3code","numeric code","tld"},
  //
  String[][] iso_lookups = {
    {"Aruba","Aruba","Netherlands","AW","ABW","533",".aw"},
    {"Afghanistan","The Islamic Republic of Afghanistan","UN member state","AF","AFG","4",".af"},
    {"Angola","The Republic of Angola","UN member state","AO","AGO","24",".ao"},
    {"Anguilla","Anguilla","United Kingdom","AI","AIA","660",".ai"},
    {"Åland Islands","Åland","Finland","AX","ALA","248",".ax"},
    {"Albania","The Republic of Albania","UN member state","AL","ALB","8",".al"},
    {"Andorra","The Principality of Andorra","UN member state","AD","AND","20",".ad"},
    {"United Arab Emirates (the)","The United Arab Emirates","UN member state","AE","ARE","784",".ae"},
    {"Argentina","The Argentine Republic","UN member state","AR","ARG","32",".ar"},
    {"Armenia","The Republic of Armenia","UN member state","AM","ARM","51",".am"},
    {"American Samoa","The Territory of American Samoa","United States","AS","ASM","16",".as"},
    {"Antarctica","All land and ice shelves south of the 60th parallel south","Antarctic Treaty","AQ","ATA","10",".aq"},
    {"French Southern Territories (the)","The French Southern and Antarctic Lands","France","TF","ATF","260",".tf"},
    {"Antigua and Barbuda","Antigua and Barbuda","UN member state","AG","ATG","28",".ag"},
    {"Australia","The Commonwealth of Australia","UN member state","AU","AUS","36",".au"},
    {"Austria","The Republic of Austria","UN member state","AT","AUT","40",".at"},
    {"Azerbaijan","The Republic of Azerbaijan","UN member state","AZ","AZE","31",".az"},
    {"Burundi","The Republic of Burundi","UN member state","BI","BDI","108",".bi"},
    {"Belgium","The Kingdom of Belgium","UN member state","BE","BEL","56",".be"},
    {"Benin","The Republic of Benin","UN member state","BJ","BEN","204",".bj"},
    {"Burkina Faso","Burkina Faso","UN member state","BF","BFA","854",".bf"},
    {"Bangladesh","The People's Republic of Bangladesh","UN member state","BD","BGD","50",".bd"},
    {"Bulgaria","The Republic of Bulgaria","UN member state","BG","BGR","100",".bg"},
    {"Bahrain","The Kingdom of Bahrain","UN member state","BH","BHR","48",".bh"},
    {"Bahamas (the)","The Commonwealth of The Bahamas","UN member state","BS","BHS","44",".bs"},
    {"Bosnia and Herzegovina","Bosnia and Herzegovina","UN member state","BA","BIH","70",".ba"},
    {"Saint Barthélemy","The Collectivity of Saint-Barthélemy","France","BL","BLM","652",".bl"},
    {"Belarus","The Republic of Belarus","UN member state","BY","BLR","112",".by"},
    {"Belize","Belize","UN member state","BZ","BLZ","84",".bz"},
    {"Bermuda","Bermuda","United Kingdom","BM","BMU","60",".bm"},
    {"Bolivia (Plurinational State of)","The Plurinational State of Bolivia","UN member state","BO","BOL","68",".bo"},
    {"Brazil","The Federative Republic of Brazil","UN member state","BR","BRA","76",".br"},
    {"Barbados","Barbados","UN member state","BB","BRB","52",".bb"},
    {"Bhutan","The Kingdom of Bhutan","UN member state","BT","BTN","64",".bt"},
    {"Bouvet Island","Bouvet Island","Norway","BV","BVT","74",""},
    {"Botswana","The Republic of Botswana","UN member state","BW","BWA","72",".bw"},
    {"Central African Republic (the)","The Central African Republic","UN member state","CF","CAF","140",".cf"},
    {"Canada","Canada","UN member state","CA","CAN","124",".ca"},
    {"Cocos (Keeling) Islands (the)","The Territory of Cocos (Keeling) Islands","Australia","CC","CCK","166",".cc"},
    {"Switzerland","The Swiss Confederation","UN member state","CH","CHE","756",".ch"},
    {"Chile","The Republic of Chile","UN member state","CL","CHL","152",".cl"},
    {"China","The People's Republic of China","UN member state","CN","CHN","156",".cn"},
    {"Cameroon","The Republic of Cameroon","UN member state","CM","CMR","120",".cm"},
    {"Congo (the Democratic Republic of the)","The Democratic Republic of the Congo","UN member state","CD","COD","180",".cd"},
    {"Congo (the)","The Republic of the Congo","UN member state","CG","COG","178",".cg"},
    {"Cook Islands (the)","The Cook Islands","New Zealand","CK","COK","184",".ck"},
    {"Colombia","The Republic of Colombia","UN member state","CO","COL","170",".co"},
    {"Comoros (the)","The Union of the Comoros","UN member state","KM","COM","174",".km"},
    {"Cabo Verde","The Republic of Cabo Verde","UN member state","CV","CPV","132",".cv"},
    {"Costa Rica","The Republic of Costa Rica","UN member state","CR","CRI","188",".cr"},
    {"Cuba","The Republic of Cuba","UN member state","CU","CUB","192",".cu"},
    {"Curaçao","The Country of Curaçao","Netherlands","CW","CUW","531",".cw"},
    {"Christmas Island","The Territory of Christmas Island","Australia","CX","CXR","162",".cx"},
    {"Cayman Islands (the)","The Cayman Islands","United Kingdom","KY","CYM","136",".ky"},
    {"Cyprus","The Republic of Cyprus","UN member state","CY","CYP","196",".cy"},
    {"Czechia","The Czech Republic","UN member state","CZ","CZE","203",".cz"},
    {"Germany","The Federal Republic of Germany","UN member state","DE","DEU","276",".de"},
    {"Djibouti","The Republic of Djibouti","UN member state","DJ","DJI","262",".dj"},
    {"Dominica","The Commonwealth of Dominica","UN member state","DM","DMA","212",".dm"},
    {"Denmark","The Kingdom of Denmark","UN member state","DK","DNK","208",".dk"},
    {"Dominican Republic (the)","The Dominican Republic","UN member state","DO","DOM","214",".do"},
    {"Algeria","The People's Democratic Republic of Algeria","UN member state","DZ","DZA","12",".dz"},
    {"Ecuador","The Republic of Ecuador","UN member state","EC","ECU","218",".ec"},
    {"Egypt","The Arab Republic of Egypt","UN member state","EG","EGY","818",".eg"},
    {"Eritrea","The State of Eritrea","UN member state","ER","ERI","232",".er"},
    {"Western Sahara","The Sahrawi Arab Democratic Republic","disputed","EH","ESH","732",""},
    {"Spain","The Kingdom of Spain","UN member state","ES","ESP","724",".es"},
    {"Estonia","The Republic of Estonia","UN member state","EE","EST","233",".ee"},
    {"Ethiopia","The Federal Democratic Republic of Ethiopia","UN member state","ET","ETH","231",".et"},
    {"Finland","The Republic of Finland","UN member state","FI","FIN","246",".fi"},
    {"Fiji","The Republic of Fiji","UN member state","FJ","FJI","242",".fj"},
    {"Falkland Islands (the) [Malvinas]","The Falkland Islands","United Kingdom","FK","FLK","238",".fk"},
    {"France","The French Republic","UN member state","FR","FRA","250",".fr"},
    {"Faroe Islands (the)","The Faroe Islands","Denmark","FO","FRO","234",".fo"},
    {"Micronesia (Federated States of)","The Federated States of Micronesia","UN member state","FM","FSM","583",".fm"},
    {"Gabon","The Gabonese Republic","UN member state","GA","GAB","266",".ga"},
    {"United Kingdom of Great Britain and Northern Ireland (the)","The United Kingdom of Great Britain and Northern Ireland","UN member state","GB","GBR","826",".gb .uk"},
    {"Georgia","Georgia","UN member state","GE","GEO","268",".ge"},
    {"Guernsey","The Bailiwick of Guernsey","British Crown","GG","GGY","831",".gg"},
    {"Ghana","The Republic of Ghana","UN member state","GH","GHA","288",".gh"},
    {"Gibraltar","Gibraltar","United Kingdom","GI","GIB","292",".gi"},
    {"Guinea","The Republic of Guinea","UN member state","GN","GIN","324",".gn"},
    {"Guadeloupe","Guadeloupe","France","GP","GLP","312",".gp"},
    {"Gambia (the)","The Republic of The Gambia","UN member state","GM","GMB","270",".gm"},
    {"Guinea-Bissau","The Republic of Guinea-Bissau","UN member state","GW","GNB","624",".gw"},
    {"Equatorial Guinea","The Republic of Equatorial Guinea","UN member state","GQ","GNQ","226",".gq"},
    {"Greece","The Hellenic Republic","UN member state","GR","GRC","300",".gr"},
    {"Grenada","Grenada","UN member state","GD","GRD","308",".gd"},
    {"Greenland","Kalaallit Nunaat","Denmark","GL","GRL","304",".gl"},
    {"Guatemala","The Republic of Guatemala","UN member state","GT","GTM","320",".gt"},
    {"French Guiana","Guyane","France","GF","GUF","254",".gf"},
    {"Guam","The Territory of Guam","United States","GU","GUM","316",".gu"},
    {"Guyana","The Co-operative Republic of Guyana","UN member state","GY","GUY","328",".gy"},
    {"Hong Kong","The Hong Kong Special Administrative Region of China","China","HK","HKG","344",".hk"},
    {"Heard Island and McDonald Islands","The Territory of Heard Island and McDonald Islands","Australia","HM","HMD","334",".hm"},
    {"Honduras","The Republic of Honduras","UN member state","HN","HND","340",".hn"},
    {"Croatia","The Republic of Croatia","UN member state","HR","HRV","191",".hr"},
    {"Haiti","The Republic of Haiti","UN member state","HT","HTI","332",".ht"},
    {"Hungary","Hungary","UN member state","HU","HUN","348",".hu"},
    {"Indonesia","The Republic of Indonesia","UN member state","ID","IDN","360",".id"},
    {"Isle of Man","The Isle of Man","British Crown","IM","IMN","833",".im"},
    {"India","The Republic of India","UN member state","IN","IND","356",".in"},
    {"British Indian Ocean Territory (the)","The British Indian Ocean Territory","United Kingdom","IO","IOT","86",".io"},
    {"Ireland","Ireland","UN member state","IE","IRL","372",".ie"},
    {"Iran (Islamic Republic of)","The Islamic Republic of Iran","UN member state","IR","IRN","364",".ir"},
    {"Iraq","The Republic of Iraq","UN member state","IQ","IRQ","368",".iq"},
    {"Iceland","Iceland","UN member state","IS","ISL","352",".is"},
    {"Israel","The State of Israel","UN member state","IL","ISR","376",".il"},
    {"Italy","The Italian Republic","UN member state","IT","ITA","380",".it"},
    {"Jamaica","Jamaica","UN member state","JM","JAM","388",".jm"},
    {"Jersey","The Bailiwick of Jersey","British Crown","JE","JEY","832",".je"},
    {"Jordan","The Hashemite Kingdom of Jordan","UN member state","JO","JOR","400",".jo"},
    {"Japan","Japan","UN member state","JP","JPN","392",".jp"},
    {"Kazakhstan","The Republic of Kazakhstan","UN member state","KZ","KAZ","398",".kz"},
    {"Kenya","The Republic of Kenya","UN member state","KE","KEN","404",".ke"},
    {"Kyrgyzstan","The Kyrgyz Republic","UN member state","KG","KGZ","417",".kg"},
    {"Cambodia","The Kingdom of Cambodia","UN member state","KH","KHM","116",".kh"},
    {"Kiribati","The Republic of Kiribati","UN member state","KI","KIR","296",".ki"},
    {"Saint Kitts and Nevis","Saint Kitts and Nevis","UN member state","KN","KNA","659",".kn"},
    {"Korea (the Republic of)","The Republic of Korea","UN member state","KR","KOR","410",".kr"},
    {"Kuwait","The State of Kuwait","UN member state","KW","KWT","414",".kw"},
    {"Lebanon","The Lebanese Republic","UN member state","LB","LBN","422",".lb"},
    {"Liberia","The Republic of Liberia","UN member state","LR","LBR","430",".lr"},
    {"Libya","The State of Libya","UN member state","LY","LBY","434",".ly"},
    {"Saint Lucia","Saint Lucia","UN member state","LC","LCA","662",".lc"},
    {"Liechtenstein","The Principality of Liechtenstein","UN member state","LI","LIE","438",".li"},
    {"Sri Lanka","The Democratic Socialist Republic of Sri Lanka","UN member state","LK","LKA","144",".lk"},
    {"Lesotho","The Kingdom of Lesotho","UN member state","LS","LSO","426",".ls"},
    {"Lithuania","The Republic of Lithuania","UN member state","LT","LTU","440",".lt"},
    {"Luxembourg","The Grand Duchy of Luxembourg","UN member state","LU","LUX","442",".lu"},
    {"Latvia","The Republic of Latvia","UN member state","LV","LVA","428",".lv"},
    {"Macao","Macao Special Administrative Region of China","China","MO","MAC","446",".mo"},
    {"Saint Martin (French part)","The Collectivity of Saint-Martin","France","MF","MAF","663",".mf"},
    {"Morocco","The Kingdom of Morocco","UN member state","MA","MAR","504",".ma"},
    {"Monaco","The Principality of Monaco","UN member state","MC","MCO","492",".mc"},
    {"Moldova (the Republic of)","The Republic of Moldova","UN member state","MD","MDA","498",".md"},
    {"Madagascar","The Republic of Madagascar","UN member state","MG","MDG","450",".mg"},
    {"Maldives","The Republic of Maldives","UN member state","MV","MDV","462",".mv"},
    {"Mexico","The United Mexican States","UN member state","MX","MEX","484",".mx"},
    {"Marshall Islands (the)","The Republic of the Marshall Islands","UN member state","MH","MHL","584",".mh"},
    {"North Macedonia","Republic of North Macedonia","UN member state","MK","MKD","807",".mk"},
    {"Mali","The Republic of Mali","UN member state","ML","MLI","466",".ml"},
    {"Malta","The Republic of Malta","UN member state","MT","MLT","470",".mt"},
    {"Myanmar","The Republic of the Union of Myanmar","UN member state","MM","MMR","104",".mm"},
    {"Montenegro","Montenegro","UN member state","ME","MNE","499",".me"},
    {"Mongolia","The State of Mongolia","UN member state","MN","MNG","496",".mn"},
    {"Northern Mariana Islands (the)","The Commonwealth of the Northern Mariana Islands","United States","MP","MNP","580",".mp"},
    {"Mozambique","The Republic of Mozambique","UN member state","MZ","MOZ","508",".mz"},
    {"Mauritania","The Islamic Republic of Mauritania","UN member state","MR","MRT","478",".mr"},
    {"Montserrat","Montserrat","United Kingdom","MS","MSR","500",".ms"},
    {"Martinique","Martinique","France","MQ","MTQ","474",".mq"},
    {"Mauritius","The Republic of Mauritius","UN member state","MU","MUS","480",".mu"},
    {"Malawi","The Republic of Malawi","UN member state","MW","MWI","454",".mw"},
    {"Malaysia","Malaysia","UN member state","MY","MYS","458",".my"},
    {"Mayotte","The Department of Mayotte","France","YT","MYT","175",".yt"},
    {"Namibia","The Republic of Namibia","UN member state","NA","NAM","516",".na"},
    {"New Caledonia","New Caledonia","France","NC","NCL","540",".nc"},
    {"Niger (the)","The Republic of the Niger","UN member state","NE","NER","562",".ne"},
    {"Norfolk Island","The Territory of Norfolk Island","Australia","NF","NFK","574",".nf"},
    {"Nigeria","The Federal Republic of Nigeria","UN member state","NG","NGA","566",".ng"},
    {"Nicaragua","The Republic of Nicaragua","UN member state","NI","NIC","558",".ni"},
    {"Niue","Niue","New Zealand","NU","NIU","570",".nu"},
    {"Netherlands (the)","The Kingdom of the Netherlands","UN member state","NL","NLD","528",".nl"},
    {"Norway","The Kingdom of Norway","UN member state","NO","NOR","578",".no"},
    {"Nepal","The Federal Democratic Republic of Nepal","UN member state","NP","NPL","524",".np"},
    {"Nauru","The Republic of Nauru","UN member state","NR","NRU","520",".nr"},
    {"New Zealand","New Zealand","UN member state","NZ","NZL","554",".nz"},
    {"Oman","The Sultanate of Oman","UN member state","OM","OMN","512",".om"},
    {"Pakistan","The Islamic Republic of Pakistan","UN member state","PK","PAK","586",".pk"},
    {"Panama","The Republic of Panamá","UN member state","PA","PAN","591",".pa"},
    {"Peru","The Republic of Perú","UN member state","PE","PER","604",".pe"},
    {"Philippines (the)","The Republic of the Philippines","UN member state","PH","PHL","608",".ph"},
    {"Palau","The Republic of Palau","UN member state","PW","PLW","585",".pw"},
    {"Papua New Guinea","The Independent State of Papua New Guinea","UN member state","PG","PNG","598",".pg"},
    {"Poland","The Republic of Poland","UN member state","PL","POL","616",".pl"},
    {"Puerto Rico","The Commonwealth of Puerto Rico","United States","PR","PRI","630",".pr"},
    {"Portugal","The Portuguese Republic","UN member state","PT","PRT","620",".pt"},
    {"Paraguay","The Republic of Paraguay","UN member state","PY","PRY","600",".py"},
    {"French Polynesia","French Polynesia","France","PF","PYF","258",".pf"},
    {"Qatar","The State of Qatar","UN member state","QA","QAT","634",".qa"},
    {"Réunion","Réunion","France","RE","REU","638",".re"},
    {"Romania","Romania","UN member state","RO","ROU","642",".ro"},
    {"Russian Federation (the)","The Russian Federation","UN member state","RU","RUS","643",".ru"},
    {"Rwanda","The Republic of Rwanda","UN member state","RW","RWA","646",".rw"},
    {"Saudi Arabia","The Kingdom of Saudi Arabia","UN member state","SA","SAU","682",".sa"},
    {"Sudan (the)","The Republic of the Sudan","UN member state","SD","SDN","729",".sd"},
    {"Senegal","The Republic of Senegal","UN member state","SN","SEN","686",".sn"},
    {"Singapore","The Republic of Singapore","UN member state","SG","SGP","702",".sg"},
    {"South Georgia and the South Sandwich Islands","South Georgia and the South Sandwich Islands","United Kingdom","GS","SGS","239",".gs"},
    {"Svalbard","Svalbard and Jan Mayen","Norway","SJ","SJM","744",""},
    {"Jan Mayen","Svalbard and Jan Mayen","Norway","SJ","SJM","744",""},
    {"Solomon Islands","The Solomon Islands","UN member state","SB","SLB","90",".sb"},
    {"Sierra Leone","The Republic of Sierra Leone","UN member state","SL","SLE","694",".sl"},
    {"El Salvador","The Republic of El Salvador","UN member state","SV","SLV","222",".sv"},
    {"San Marino","The Republic of San Marino","UN member state","SM","SMR","674",".sm"},
    {"Somalia","The Federal Republic of Somalia","UN member state","SO","SOM","706",".so"},
    {"Saint Pierre and Miquelon","The Overseas Collectivity of Saint-Pierre and Miquelon","France","PM","SPM","666",".pm"},
    {"Serbia","The Republic of Serbia","UN member state","RS","SRB","688",".rs"},
    {"South Sudan","The Republic of South Sudan","UN member state","SS","SSD","728",".ss"},
    {"Sao Tome and Principe","The Democratic Republic of São Tomé and Príncipe","UN member state","ST","STP","678",".st"},
    {"Suriname","The Republic of Suriname","UN member state","SR","SUR","740",".sr"},
    {"Slovakia","The Slovak Republic","UN member state","SK","SVK","703",".sk"},
    {"Slovenia","The Republic of Slovenia","UN member state","SI","SVN","705",".si"},
    {"Sweden","The Kingdom of Sweden","UN member state","SE","SWE","752",".se"},
    {"Eswatini","The Kingdom of Eswatini","UN member state","SZ","SWZ","748",".sz"},
    {"Sint Maarten (Dutch part)","Sint Maarten","Netherlands","SX","SXM","534",".sx"},
    {"Seychelles","The Republic of Seychelles","UN member state","SC","SYC","690",".sc"},
    {"Syrian Arab Republic (the)","The Syrian Arab Republic","UN member state","SY","SYR","760",".sy"},
    {"Turks and Caicos Islands (the)","The Turks and Caicos Islands","United Kingdom","TC","TCA","796",".tc"},
    {"Chad","The Republic of Chad","UN member state","TD","TCD","148",".td"},
    {"Togo","The Togolese Republic","UN member state","TG","TGO","768",".tg"},
    {"Thailand","The Kingdom of Thailand","UN member state","TH","THA","764",".th"},
    {"Tajikistan","The Republic of Tajikistan","UN member state","TJ","TJK","762",".tj"},
    {"Tokelau","Tokelau","New Zealand","TK","TKL","772",".tk"},
    {"Turkmenistan","Turkmenistan","UN member state","TM","TKM","795",".tm"},
    {"Timor-Leste","The Democratic Republic of Timor-Leste","UN member state","TL","TLS","626",".tl"},
    {"Tonga","The Kingdom of Tonga","UN member state","TO","TON","776",".to"},
    {"Trinidad and Tobago","The Republic of Trinidad and Tobago","UN member state","TT","TTO","780",".tt"},
    {"Tunisia","The Republic of Tunisia","UN member state","TN","TUN","788",".tn"},
    {"Turkey","The Republic of Turkey","UN member state","TR","TUR","792",".tr"},
    {"Tuvalu","Tuvalu","UN member state","TV","TUV","798",".tv"},
    {"Taiwan (Province of China)","The Republic of China","disputed","TW","TWN","158",".tw"},
    {"Uganda","The Republic of Uganda","UN member state","UG","UGA","800",".ug"},
    {"Ukraine","Ukraine","UN member state","UA","UKR","804",".ua"},
    {"Uruguay","The Oriental Republic of Uruguay","UN member state","UY","URY","858",".uy"},
    {"United States of America (the)","The United States of America","UN member state","US","USA","840",".us"},
    {"Uzbekistan","The Republic of Uzbekistan","UN member state","UZ","UZB","860",".uz"},
    {"Holy See (the)","The Holy See","UN observer","VA","VAT","336",".va"},
    {"Saint Vincent and the Grenadines","Saint Vincent and the Grenadines","UN member state","VC","VCT","670",".vc"},
    {"Venezuela (Bolivarian Republic of)","The Bolivarian Republic of Venezuela","UN member state","VE","VEN","862",".ve"},
    {"Virgin Islands (British)","The Virgin Islands","United Kingdom","VG","VGB","92",".vg"},
    {"Virgin Islands (U.S.)","The Virgin Islands of the United States","United States","VI","VIR","850",".vi"},
    {"Viet Nam","The Socialist Republic of Viet Nam","UN member state","VN","VNM","704",".vn"},
    {"Vanuatu","The Republic of Vanuatu","UN member state","VU","VUT","548",".vu"},
    {"Wallis and Futuna","The Territory of the Wallis and Futuna Islands","France","WF","WLF","876",".wf"},
    {"Samoa","The Independent State of Samoa","UN member state","WS","WSM","882",".ws"},
    {"Yemen","The Republic of Yemen","UN member state","YE","YEM","887",".ye"},
    {"South Africa","The Republic of South Africa","UN member state","ZA","ZAF","710",".za"},
    {"Zambia","The Republic of Zambia","UN member state","ZM","ZMB","894",".zm"},
    {"Zimbabwe","The Republic of Zimbabwe","UN member state","ZW","ZWE","716",".zw"},

    {"Bonaire","Bonaire, Sint Eustatius and Saba","Netherlands","BQ","BES","535",".bq .nl"},
    {"Sint Eustatius","Bonaire, Sint Eustatius and Saba","Netherlands","BQ","BES","535",".bq .nl"}, 
    {"Saba","Bonaire, Sint Eustatius and Saba","Netherlands","BQ","BES","535",".bq .nl"},
    {"Brunei Darussalam","The Nation of Brunei, the Abode of Peace","UN member state","BN","BRN","96",".bn"},
    {"Côte d'Ivoire","The Republic of Côte d'Ivoire","UN member state","CI","CIV","384",".ci"},
    {"Tanzania, the United Republic of","The United Republic of Tanzania","UN member state","TZ","TZA","834",".tz"},
    {"United States Minor Outlying Islands (the)","Baker Island, Howland Island, Jarvis Island, Johnston Atoll, Kingman Reef, Midway Atoll, Navassa Island, Palmyra Atoll, and Wake Island","United States","UM","UMI","581",""},
    {"Saint Helena","Saint Helena, Ascension and Tristan da Cunha","United Kingdom","SH","SHN","654",".sh"},
    {"Ascension Island","Saint Helena, Ascension and Tristan da Cunha","United Kingdom","SH","SHN","654",".sh"},
    {"Tristan da Cunha","Saint Helena, Ascension and Tristan da Cunha","United Kingdom","SH","SHN","654",".sh"},
    {"Pitcairn","The Pitcairn, Henderson, Ducie and Oeno Islands","United Kingdom","PN","PCN","612",".pn"},
    {"Palestine, State of","The State of Palestine","UN observer","PS","PSE","275",".ps"},
    {"Korea (the Democratic People's Republic of)","The Democratic People's Republic of Korea","UN member state","KP","PRK","408",".kp"},
    {"Lao People's Democratic Republic (the)","The Lao People's Democratic Republic","UN member state","LA","LAO","418",".la"}
  };

  /**
   * A3Code (lowercase) to the row in the ISO table
   */
  Map<String,String[]> a3_to_row = new HashMap<String,String[]>();
  
  /**
   * Indexes into the rows of the table
   */
  public static final int COUNTRY_NAME_INDEX        = 0,
                          OFFICIAL_STATE_NAME_INDEX = 1,
                          SOVEREIGNTY_INDEX         = 2,
                          A2_CODE_INDEX             = 3,
                          A3_CODE_INDEX             = 4,
                          NUMERIC_CODE_INDEX        = 5,
                          TLD_INDEX                 = 6;

  /**
   * Return the row from the ISO3166 Table based on the a3code
   */
  public String[] getISO3166Row(String a3code) {
    String strs[] = null;
    if (a3_to_row.containsKey(a3code.toLowerCase())) {
      String to_copy[] = a3_to_row.get(a3code.toLowerCase()); 
      strs = new String[to_copy.length];
      System.arraycopy(to_copy, 0, strs, 0, strs.length);
    }
    return strs;
  }

  /**
   * Fill the region lookups
   */
  private void fillRegionLookup(String ccs[], String region) {
    for (int i=0;i<ccs.length;i++) region_lu.put(ccs[i], region);
  }

  /**
   * Region lookups
   */
  Map<String,String> region_lu = new HashMap<String,String>();

  /**
   * Return an iterator over the country codes.
   *
   *@return iterator over country code strings
   */
  public abstract Iterator<String> countryCodeIterator();

  /**
   * Return the shape for a country code.
   *
   *@param cc country code
   *
   *@return shape
   */
  public abstract Set<Shape> getCountryShapes(String cc);

  /**
   * Return the region for a country code.
   *
   *@param cc country code
   *
   *@return region
   */
  public String getRegion(String cc) {
    if (region_lu.containsKey(cc) == false) System.err.println("No CC \"" + cc + "\"");
    return region_lu.get(cc);
  }

  /**
   * Return the a3code for a variety of country inputs.
   *
   *@param str county description
   *
   *@reurn correct a3code
   */
  public String a3code(String str) {
     str = str.toLowerCase();
    if (a3codeLookup.containsKey(str)) return a3codeLookup.get(str);
    else                               return BundlesDT.NOTSET;
  }
}

