/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;


/**
 * Renders XY Charts
 */
public class XYRenderer {
  /**
   * Render a simple xy plot with time in the x axis.  The vector version occurs
   * next -- any changes to this method should have a corresponging change to the
   * vector version.
   *
   *@param to_render        records to render
   *@param w                width of the plot
   *@param h                height of the plot
   *@param global           if not null, use the timestamps from the global records
   *@param use_global_time  use the global time option
   *@param y_axis           the variable to render in the y-axis
   *@param y_axis_equal     apply equal scaling in the y-axis
   *@param color_by         color by value for each dot
   *
   *@return xy rendering
   */
  public static BufferedImage renderTimeInX(Set<Bundle> to_render, 
                                            int w, 
                                            int h, 
                                            Bundles global, 
                                            boolean use_global_time,
                                            String y_axis, 
                                            boolean y_axis_equal,
                                            String color_by) {
 BufferedImage         bi        = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
 Graphics2D            g2d       = null;

 try {

    g2d = (Graphics2D) bi.getGraphics();
    RTColorManager.renderVisualizationBackground(bi, g2d);
    g2d.setColor(RTColorManager.getColor("background", "nearbg"));
    g2d.drawRect(0,0,w-1,h-1);

    // Early termination if it's an empty set...
    if (to_render == null || to_render.size() == 0) return bi;

    int                   x_ins     = 1,            // Inset for the graph in x
                          y_ins     = 1;            // Inset for the graph in y
    int                   graph_w   = w - 2*x_ins,  // actual graph width
                          graph_h   = h - 2*y_ins;  // actual graph height

    Set<Bundle>           applies   = new HashSet<Bundle>();           // Set of the actual bundles that can contribute to the rendering
    Map<Tablet,KeyMaker>  y_km_map  = new HashMap<Tablet,KeyMaker>();  // Keymakers for the y axis (per tablet)

    long                  ts0 = Long.MAX_VALUE,                        // Timeframe for the rendering (start)
                          ts1 = Long.MIN_VALUE;                        // Timeframe for the rendering (end)

    if (use_global_time) { ts0 = global.ts0(); ts1 = global.ts1(); }    // Use global time if not null

    Set<Integer> y_values = new HashSet<Integer>();
    int          y_min    = Integer.MAX_VALUE, 
                 y_max    = Integer.MIN_VALUE;

    // First pass to determine the overall params for the rendering ... placed int the y_values set
    Iterator<Bundle> it_bun = to_render.iterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next(); Tablet tablet = bundle.getTablet(); 

      // Does the tablet satisfy the parameters?
      if (tablet.hasTimeStamps() && KeyMaker.tabletCompletesBlank(tablet, y_axis)) {
        // Let's just handle a bundle once
        applies.add(bundle);

        // Figure out the y contributions of the record
        if (y_km_map.containsKey(tablet) == false) y_km_map.put(tablet, new KeyMaker(tablet, y_axis));
        int y_keys[] = y_km_map.get(tablet).intKeys(bundle); 
        for (int i=0;i<y_keys.length;i++) {
          y_values.add(y_keys[i]);
          if (y_keys[i] < y_min) y_min = y_keys[i];
          if (y_keys[i] > y_max) y_max = y_keys[i];
        }

        // If not global time, figure out the the timestamps
        if (use_global_time == false) {
          if (ts0 > bundle.ts0()) ts0 = bundle.ts0();
          if (ts1 < bundle.ts1()) ts1 = bundle.ts1();
        }
      }
    }

    // Fix some degenerate cases (where the min and max are equal)
    if (y_min == y_max)           { y_min--; y_max++; }
    if (Math.abs(ts1 - ts0) < 3L) { ts0 = ts0 - 1L; ts1 = ts0 + 4L; }

    // Second pass is dependent on the y scaling option
    BundlesCounterContext bcc = new BundlesCounterContext(global, y_axis, color_by); Map<String,Integer> x_map = new HashMap<String,Integer>(), y_map = new HashMap<String,Integer>();
    if (y_axis_equal) {
      //
      // Equal spacing means sorting and tracking position in the sort
      //
      List<Integer>         y_sort = new ArrayList<Integer>(); y_sort.addAll(y_values); Collections.sort(y_sort);
      Map<Integer,Integer>  y_pos  = new HashMap<Integer,Integer>();
      for (int i=0;i<y_sort.size();i++) y_pos.put(y_sort.get(i), (int) (y_ins + (graph_h - (graph_h*i)/y_sort.size())));

      // Almost identical to the linear spacing now...
      it_bun = applies.iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next();
        int x0 = (int) (x_ins  + (graph_w*(bundle.ts0() - ts0))/(ts1 - ts0)),
            x1 = (int) (x_ins  + (graph_w*(bundle.ts1() - ts0))/(ts1 - ts0));

        int y_keys[] = y_km_map.get(bundle.getTablet()).intKeys(bundle);
        for (int y_i=0;y_i<y_keys.length;y_i++) { // Each possible y coordinate
          int y = y_pos.get(y_keys[y_i]);
          for (int x=x0;x<=x1;x++) { // Models duration
           String key = "" + x + "," + y;
           bcc.count(bundle, key);
           x_map.put(key, x); y_map.put(key, y); 
          }
        }
      }

    } else {
      //
      // Linear spacing is much simpler
      //
      it_bun = applies.iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next();
        int x0 = (int) (x_ins  + (graph_w*(bundle.ts0() - ts0))/(ts1 - ts0)),
            x1 = (int) (x_ins  + (graph_w*(bundle.ts1() - ts0))/(ts1 - ts0));

        int y_keys[] = y_km_map.get(bundle.getTablet()).intKeys(bundle);
        for (int y_i=0;y_i<y_keys.length;y_i++) { // Each possible y coordinate
          int y = y_ins + (graph_h - (graph_h*(y_keys[y_i] - y_min))/(y_max - y_min));
          for (int x=x0;x<=x1;x++) { // Models duration
           String key = "" + x + "," + y;
           bcc.count(bundle, key);
           x_map.put(key, x); y_map.put(key, y); 
          }
        }
      }
    }

    // Render it
    Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
      String key = it.next(); int x = x_map.get(key), y = y_map.get(key);
      g2d.setColor(bcc.binColor(key));
      g2d.fillRect(x, y, 1, 1);
    }
  } finally { if (g2d != null) g2d.dispose(); }
 return bi;
 }

  /**
   * Calculate a vector representation for an xy chart... this just copies everything
   * from the renderer version and makes it into a float vector...   Any mods to the
   * above should be reflected int the below version.
   *
   *@param to_render        records to render
   *@param w                width of the plot
   *@param h                height of the plot
   *@param global           if not null, use the timestamps from the global records
   *@param use_global_time  use the global time option
   *@param y_axis           the variable to render in the y-axis
   *@param y_axis_equal     apply equal scaling in the y-axis
   *
   *@return xy rendering
   */
  public static float[] vectorTimeInX(Set<Bundle> to_render, 
                                      int w, 
                                      int h, 
                                      Bundles global, 
                                      boolean use_global_time,
                                      String y_axis, 
                                      boolean y_axis_equal) {
 float vec[] = new float[w*h];

 Set<Bundle>           applies   = new HashSet<Bundle>();           // Set of the actual bundles that can contribute to the rendering
 Map<Tablet,KeyMaker>  y_km_map  = new HashMap<Tablet,KeyMaker>();  // Keymakers for the y axis (per tablet)

 long                  ts0 = Long.MAX_VALUE,                        // Timeframe for the rendering (start)
                       ts1 = Long.MIN_VALUE;                        // Timeframe for the rendering (end)

 if (use_global_time) { ts0 = global.ts0(); ts1 = global.ts1(); }    // Use global time if not null

 Set<Integer> y_values = new HashSet<Integer>();
 int          y_min    = Integer.MAX_VALUE, 
              y_max    = Integer.MIN_VALUE;

 int          graph_w  = w - 1,
              graph_h  = h - 1;

 // First pass to determine the overall params for the rendering ... placed int the y_values set
 Iterator<Bundle> it_bun = to_render.iterator(); while (it_bun.hasNext()) {
   Bundle bundle = it_bun.next(); Tablet tablet = bundle.getTablet(); 

   // Does the tablet satisfy the parameters?
   if (tablet.hasTimeStamps() && KeyMaker.tabletCompletesBlank(tablet, y_axis)) {
     // Let's just handle a bundle once
     applies.add(bundle);

     // Figure out the y contributions of the record
     if (y_km_map.containsKey(tablet) == false) y_km_map.put(tablet, new KeyMaker(tablet, y_axis));
     int y_keys[] = y_km_map.get(tablet).intKeys(bundle); 
     for (int i=0;i<y_keys.length;i++) {
       y_values.add(y_keys[i]);
       if (y_keys[i] < y_min) y_min = y_keys[i];
       if (y_keys[i] > y_max) y_max = y_keys[i];
     }

     // If not global time, figure out the the timestamps
     if (use_global_time == false) {
       if (ts0 > bundle.ts0()) ts0 = bundle.ts0();
       if (ts1 < bundle.ts1()) ts1 = bundle.ts1();
     }
   }
 }

 // Fix some degenerate cases (where the min and max are equal)
 if (y_min == y_max)           { y_min--; y_max++; }
 if (Math.abs(ts1 - ts0) < 3L) { ts0 = ts0 - 1L; ts1 = ts0 + 4L; }

 // Second pass is dependent on the y scaling option
 BundlesCounterContext bcc = new BundlesCounterContext(global, y_axis, null); Map<String,Integer> x_map = new HashMap<String,Integer>(), y_map = new HashMap<String,Integer>();
 if (y_axis_equal) {
   //
   // Equal spacing means sorting and tracking position in the sort
   //
   List<Integer>         y_sort = new ArrayList<Integer>(); y_sort.addAll(y_values); Collections.sort(y_sort);
   Map<Integer,Integer>  y_pos  = new HashMap<Integer,Integer>();
   for (int i=0;i<y_sort.size();i++) y_pos.put(y_sort.get(i), (int) ((graph_h - (graph_h*i)/y_sort.size())));

   // Almost identical to the linear spacing now...
   it_bun = applies.iterator(); while (it_bun.hasNext()) {
     Bundle bundle = it_bun.next();
     int x0 = (int) ((graph_w*(bundle.ts0() - ts0))/(ts1 - ts0)),
         x1 = (int) ((graph_w*(bundle.ts1() - ts0))/(ts1 - ts0));

     int y_keys[] = y_km_map.get(bundle.getTablet()).intKeys(bundle);
     for (int y_i=0;y_i<y_keys.length;y_i++) { // Each possible y coordinate
        int y = y_pos.get(y_keys[y_i]);
        for (int x=x0;x<=x1;x++) { // Models duration
          String key = "" + x + "," + y;
          bcc.count(bundle, key);
          x_map.put(key, x); y_map.put(key, y); 
        }
     }
   }
 } else {
   //
   // Linear spacing is much simpler
   //
   it_bun = applies.iterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next();
      int x0 = (int) ((graph_w*(bundle.ts0() - ts0))/(ts1 - ts0)),
          x1 = (int) ((graph_w*(bundle.ts1() - ts0))/(ts1 - ts0));

      int y_keys[] = y_km_map.get(bundle.getTablet()).intKeys(bundle);
      for (int y_i=0;y_i<y_keys.length;y_i++) { // Each possible y coordinate
        int y = (graph_h - (graph_h*(y_keys[y_i] - y_min))/(y_max - y_min));
        for (int x=x0;x<=x1;x++) { // Models duration
          String key = "" + x + "," + y;
          bcc.count(bundle, key);
          x_map.put(key, x); y_map.put(key, y); 
        }
      }
   }
 }

 // Render it
 Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
   String key = it.next(); int x = x_map.get(key), y = y_map.get(key);
   vec[x + y*w] = (float) bcc.totalNormalized(key);
 }
 return vec;
 }

}

