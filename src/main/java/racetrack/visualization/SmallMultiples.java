/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import tagbio.umap.Umap;

import racetrack.analysis.HierarchicalClustering;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.BCCLite;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.gui.RTYAFTPanel;
import racetrack.gui.XMap;

/**
 *
 */
public class SmallMultiples {
        /**
         * Y insert in pixels
         */
        static final int y_ins = 2,

        /**
         * X insert in pixels
         */
                         x_ins = 2;

        /**
         * Render GPS Information
         *
         *@param lon_field         longitude field
         *@param lat_field         latitude field
         *@param entity_path_field if the points are to be assembled into a path, use this field to group paths together, sort by time, and then draw lines between the points... can be null
         *@param local_extents     use the local extents from the set as the min and max coordinates, otherwise, use the global ones from the passed in bundles
         *
         */
        public static BufferedImage renderGPS(Bundles bs, String count_by, String color_by, Set<Bundle> set, String lon_field, String lat_field, String entity_path_field, boolean local_extents, int width) {
          // Check for null latitude or longitude fields
          if (lon_field == null || lat_field == null) { return new BufferedImage(64, 64, BufferedImage.TYPE_INT_RGB); }

          // KeyMaker lookups... 
          Map<Tablet,KeyMaker> lon_kms = new HashMap<Tablet,KeyMaker>(), lat_kms = new HashMap<Tablet,KeyMaker>(), ent_kms = new HashMap<Tablet,KeyMaker>(); 
          Set<Tablet> not_applicable = new HashSet<Tablet>();

          // Rendering state
          Map<String,Set<Bundle>>   wxy_to_bundles     = new HashMap<String,Set<Bundle>>();
          Map<String,Double>        wxy_to_x           = new HashMap<String,Double>(),
                                    wxy_to_y           = new HashMap<String,Double>();
          Map<Bundle,Set<String>>   bundle_to_wxys     = new HashMap<Bundle,Set<String>>();
          Map<String,List<Bundle>>  ent_to_bundle_list = new HashMap<String,List<Bundle>>();

          //
          // Calculate the extents // start capturing entity information...
          //
          double x_min = Double.POSITIVE_INFINITY, x_max = Double.NEGATIVE_INFINITY, y_min = Double.POSITIVE_INFINITY, y_max = Double.NEGATIVE_INFINITY; 
          //
          // Locally...
          //
          if (local_extents) {
            Iterator<Bundle> it_bun = set.iterator(); while (it_bun.hasNext()) {
              // Get the tablet & make sure it has key makers...
              Bundle bundle = it_bun.next(); Tablet tablet = bundle.getTablet(); if (not_applicable.contains(tablet) == false && lon_kms.containsKey(tablet) == false) {
                if (KeyMaker.tabletCompletesBlank(tablet,lon_field) && KeyMaker.tabletCompletesBlank(tablet,lat_field)) {
                  lon_kms.put(tablet, new KeyMaker(tablet,lon_field)); lat_kms.put(tablet,new KeyMaker(tablet,lat_field));
                  if (entity_path_field != null && KeyMaker.tabletCompletesBlank(tablet,entity_path_field) && tablet.hasTimeStamps()) ent_kms.put(tablet, new KeyMaker(tablet,entity_path_field));
                } else not_applicable.add(tablet);
              }
              if (lon_kms.containsKey(tablet)) {
                String lons[] = lon_kms.get(tablet).stringKeys(bundle), lats[] = lat_kms.get(tablet).stringKeys(bundle);
                if (lons.length > 0 && lons[0].equals(BundlesDT.NOTSET) == false && lats.length > 0 && lats[0].equals(BundlesDT.NOTSET) == false) {

                  bundle_to_wxys.put(bundle,new HashSet<String>()); // Duplicate below needed

                  for (int x_i=0;x_i<lons.length;x_i++) {
                    double x = Double.parseDouble(lons[x_i]);
                    if (x < x_min) x_min = x; if (x > x_max) x_max = x;
                    for (int y_i=0;y_i<lats.length;y_i++) {
                      double y = Double.parseDouble(lats[y_i]);
                      if (y < y_min) y_min = y; if (y > y_max) y_max = y;

                      // Capture enough state to perform the next step in rendering ... duplicated below
                      String wxy = lons[x_i] + "|" + lats[y_i]; wxy_to_x.put(wxy,x); wxy_to_y.put(wxy,y);
                      if (wxy_to_bundles.containsKey(wxy) == false) wxy_to_bundles.put(wxy,new HashSet<Bundle>());
                      wxy_to_bundles.get(wxy).add(bundle); bundle_to_wxys.get(bundle).add(wxy);
                      if (ent_kms.containsKey(tablet)) {
                        String ents[] = ent_kms.get(tablet).stringKeys(bundle); if (ents.length > 0 && ents[0].equals(BundlesDT.NOTSET) == false) {
                          for (int i=0;i<ents.length;i++) {
                            if (ent_to_bundle_list.containsKey(ents[i]) == false) ent_to_bundle_list.put(ents[i], new ArrayList<Bundle>());
                            ent_to_bundle_list.get(ents[i]).add(bundle);
                          }
                        }
                      }
                      // ... end duplicate block

                    }
                  }
                }
              }
            }

          //
          // Globally...
          //
          } else {
            Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
              Tablet tablet = it_tab.next(); if (KeyMaker.tabletCompletesBlank(tablet,lon_field) && KeyMaker.tabletCompletesBlank(tablet,lat_field)) {
                lon_kms.put(tablet, new KeyMaker(tablet,lon_field)); lat_kms.put(tablet,new KeyMaker(tablet,lat_field));
                if (entity_path_field != null && KeyMaker.tabletCompletesBlank(tablet,entity_path_field) && tablet.hasTimeStamps()) ent_kms.put(tablet, new KeyMaker(tablet,entity_path_field));
                Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
                  Bundle bundle = it_bun.next();
                  String lons[] = lon_kms.get(tablet).stringKeys(bundle), lats[] = lat_kms.get(tablet).stringKeys(bundle);
                  if (lons.length > 0 && lons[0].equals(BundlesDT.NOTSET) == false && lats.length > 0 && lats[0].equals(BundlesDT.NOTSET) == false) {

                  if (set.contains(bundle)) bundle_to_wxys.put(bundle,new HashSet<String>()); // Duplicate below needed // Slight deviation...

                    for (int x_i=0;x_i<lons.length;x_i++) {
                      double x = Double.parseDouble(lons[x_i]);
                      if (x < x_min) x_min = x; if (x > x_max) x_max = x;
                      for (int y_i=0;y_i<lats.length;y_i++) {
                        double y = Double.parseDouble(lats[y_i]);
                        if (y < y_min) y_min = y; if (y > y_max) y_max = y;

                    if (set.contains(bundle)) {
                      // Capture enough state to perform the next step in rendering ... duplicated below
                      String wxy = lons[x_i] + "|" + lats[y_i]; wxy_to_x.put(wxy,x); wxy_to_y.put(wxy,y);
                      if (wxy_to_bundles.containsKey(wxy) == false) wxy_to_bundles.put(wxy,new HashSet<Bundle>());
                      wxy_to_bundles.get(wxy).add(bundle); bundle_to_wxys.get(bundle).add(wxy);
                      if (ent_kms.containsKey(tablet)) {
                        String ents[] = ent_kms.get(tablet).stringKeys(bundle); if (ents.length > 0 && ents[0].equals(BundlesDT.NOTSET) == false) {
                          for (int i=0;i<ents.length;i++) {
                            if (ent_to_bundle_list.containsKey(ents[i]) == false) ent_to_bundle_list.put(ents[i], new ArrayList<Bundle>());
                            ent_to_bundle_list.get(ents[i]).add(bundle);
                          }
                        }
                      }
                      // ... end duplicate block
                    }

                      }
                    }
                  }
                }
              } else not_applicable.add(tablet);
            }
          }

          // Create the image ... make sure the extents are sane
          BufferedImage bi  = new BufferedImage(width, width, BufferedImage.TYPE_INT_ARGB); if (Double.isInfinite(x_min) == false && Double.isInfinite(y_min) == false) {
            Graphics2D    g2d = null; try {
              g2d = (Graphics2D) bi.getGraphics();
              g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
              g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0, width,  width);
              g2d.setColor(RTColorManager.getColor("background", "nearbg"));  g2d.drawRect(0,0, width-1,width-1);

              int graph_w = width - 2*x_ins, graph_h = width - 2*y_ins;

              // fix the aspect ... i.e., choose the same ratio in x and in y...
              if ((x_max - x_min) > (y_max - y_min)) { double diff = (x_max - x_min) - (y_max - y_min); y_max += diff/2.0; y_min -= diff/2.0;
              } else                                 { double diff = (y_max - y_min) - (x_max - x_min); x_max += diff/2.0; x_min -= diff/2.0; }

              // Transform all of the wxy's into their corresponding sxy's...
              BundlesCounterContext   bcc            = new BundlesCounterContext(bs, count_by, color_by);
              Map<String,Integer>     sxy_to_sx      = new HashMap<String,Integer>(),
                                      sxy_to_sy      = new HashMap<String,Integer>();
              Map<Bundle,Set<String>> bundle_to_sxys = new HashMap<Bundle,Set<String>>();

              Iterator<String> it_wxy = wxy_to_bundles.keySet().iterator(); while (it_wxy.hasNext()) {
                String wxy = it_wxy.next(); double wx = wxy_to_x.get(wxy), wy = wxy_to_y.get(wxy);
                int sx =           (int) (graph_w*(wx - x_min)/(x_max - x_min)),
                    sy = graph_h - (int) (graph_h*(wy - y_min)/(y_max - y_min));

                String sxy = sx + "|" + sy; sxy_to_sx.put(sxy,sx); sxy_to_sy.put(sxy,sy);
                Iterator<Bundle> it_bun = wxy_to_bundles.get(wxy).iterator(); while (it_bun.hasNext()) {
                  Bundle bundle = it_bun.next();
                  if (bundle_to_sxys.containsKey(bundle) == false) bundle_to_sxys.put(bundle, new HashSet<String>()); bundle_to_sxys.get(bundle).add(sxy);
                  bcc.count(bundle,sxy);
                }
              }

              // Entity paths?

              // Points from the counter context
              Iterator<String> it_sxy = bcc.binIterator(); while (it_sxy.hasNext()) {
                String sxy = it_sxy.next(); int sx = sxy_to_sx.get(sxy), sy = sxy_to_sy.get(sxy);
                g2d.setColor(bcc.binColor(sxy));
                g2d.fillRect(sx,sy,1,1);
              }
            } finally { g2d.dispose(); }
          }
          return bi;
        }

        /**
         * For country-level small multiples...
         * ... feels like this should just be the magnitude color scheme...
         */
        public static BufferedImage renderCountry(Bundles bs, String count_by, String color_by, Set<Bundle> set, String country_field, int width) {
          // Figure out the geometry and create the image
          int height = (width * 2)/3;
          BufferedImage bi  = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

          // Configure the graphics
          Graphics2D    g2d = null; try {
            g2d = (Graphics2D) bi.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0, width,  height);
            g2d.setColor(RTColorManager.getColor("background", "nearbg"));  g2d.drawRect(0,0, width-1,height-1);
            ColorScale cs = RTColorManager.getContinuousColorScale(); // new BrightGrayColorScale();

            // Get the worldmap for small multiples
            WorldMap world_map = null;
            try { 
              if (width >= 192) world_map = MadeHexMaps.getInstance(MadeHexMaps.ShapeQuality.SMALL_MULTIPLES_50M_MEDRES); 
              else              world_map = MadeHexMaps.getInstance(MadeHexMaps.ShapeQuality.SMALL_MULTIPLES_50M_LOWRES); 
            } catch (IOException ioe) { System.err.println("IOException: " + ioe); }

            // Make sure we have data
            if (set != null && set.size() > 0) {
              // Create the counter context and fill it
              BundlesCounterContext bcc = new BundlesCounterContext(bs, count_by, color_by);

              // Need to apply the a3code transform, so have to do this by hand
              Map<Tablet,KeyMaker> km_lu = new HashMap<Tablet,KeyMaker>();
              Iterator<Bundle> it_bundle = set.iterator(); while (it_bundle.hasNext()) {
                // Figure out the keymaker for this tablet ... if this tablet doesn't fit, make the km lookup null
                Bundle bundle = it_bundle.next();
                Tablet tablet = bundle.getTablet();
                if (km_lu.containsKey(tablet) == false) {
                  if (KeyMaker.tabletCompletesBlank(tablet, country_field) && (count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by)))
                    km_lu.put(tablet, new KeyMaker(tablet,country_field));
                  else
                    km_lu.put(tablet, null);
                }

                // Convert it to country code keys and count them
                KeyMaker km = km_lu.get(tablet); if (km != null) {
                  String strs[] = km.stringKeys(bundle);
                  if (strs != null) { for (int i=0;i<strs.length;i++) bcc.count(bundle, world_map.a3code(strs[i])); }
                }
              }


              // Now perform the rendering
              Iterator<String> it_a3 = bcc.binIterator(); while (it_a3.hasNext()) {
                String a3 = it_a3.next();

                // Figure out the color
                g2d.setColor(cs.at((float) bcc.totalNormalized(a3)));

                // Render the shapes
                Set<Shape> shapes = world_map.getCountryShapes(a3);
                if (shapes != null && shapes.size() > 0) {
                  Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
                    Shape wshape = it_shape.next(); Shape sshape = world_map.worldShapeToScreenShape(wshape,width,height);
                    g2d.fill(sshape); g2d.draw(sshape);
                  }
                }
              }
            }
          } finally { if (g2d != null) g2d.dispose(); }
          return bi;
        }

        /**
         * For the piechart mode.
         *
         * 2021-12-31:  so... giving this some more thought...  the ideal implementation sorts the bins by size and then renders
         * from largest to smallest... and once the bins become too small, aggregates them into a generic color.  So... if you
         * have 1000's of 0.1 degree slices, those just get aggregated into one 100 degree slice of a the multiset color.
         * ... but... since we need the small multiples to be consistentl colored -- i.e., the same coloring order, this 
         * complicates the rendering since the smallest bins in this specific pie chart may come at the beginning or in the
         * middle...
         *
         *@param cbins color bin ordering (needs to be from global state)
         */
        public static BufferedImage renderPie(Bundles bs, String count_by, String color_by, Set<Bundle> set, List<String> cbins, int size) {
          int total_w, total_h; total_w = total_h = size;
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_ARGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          // Geometry
          double circle_x = 2, circle_y = 2, circle_d = total_w - 4;

          // Check for a null or empty set...
          if (set == null || set.size() == 0) {
            g2d.setColor(RTColorManager.getColor("background", "nearbg"));
            g2d.draw(new Ellipse2D.Double(circle_x, circle_y, circle_d, circle_d));
            return bi;
          }

          // If no color_by, then just make it a small dot
          if (color_by == null) { 
            g2d.setColor(RTColorManager.getColor("label","default")); g2d.fill(new Ellipse2D.Double(circle_x, circle_y, circle_d, circle_d));

          // Else, calculate the slices based on the the set
          } else {
            // Run through the records and place them into the pie slices
            BundlesCounterContext bcc = new BundlesCounterContext(bs, count_by, color_by, set, color_by);

            double total            = bcc.totalCounts(),
                   deg_start        = 90.0,
                   render_deg_start = 90.0;

            // Draw the color bins from greatest size to smallest (overall to make consistent)...
            // ... not every color bin may be present in the local counter context...
            // ... only draw up to 355 degrees... then make the rest just gray...
            int cbin_i = cbins.size()-1;
            while (cbin_i >= 0) {
              String cbin = cbins.get(cbin_i);

              // Get the total for this bin... if it contains a count, render the slice
              if (bcc.hasBin(cbin)) { double total_bin = bcc.total(cbin); if (total_bin > 0.0) {
                // Incremental degrees
                double deg_inc = (total_bin / total) * 360.0;

                // Render
                if (deg_inc > 3.0) {
                  g2d.setColor(RTColorManager.getColor(cbin));
                  g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, render_deg_start, deg_inc, Arc2D.PIE));
                  render_deg_start += deg_inc;
                }
                deg_start += deg_inc;
              }
            } cbin_i--; }

            // Draw the left overs
            if (render_deg_start < 360.0 + 90.0) {
              g2d.setColor(RTColorManager.getColor("set", "multi"));
              g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, render_deg_start, 360.0 + 90.0 - render_deg_start, Arc2D.PIE));
            }
          }

          } finally { if (g2d != null) g2d.dispose(); }

          return bi;
        }

        /**
         * Render best auto-time continuous version.
         */
        public static BufferedImage renderAutoTime(Bundles bs, String count_by, String color_by, Set<Bundle> set, List<String> cbins, int total_w, int total_h) {
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_RGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();

          // Draw background and a frame
          g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,total_w,  total_h);
          g2d.setColor(RTColorManager.getColor("axis",       "minor"));   g2d.drawRect(0,0,total_w-1,total_h-1);

          // Dimensions
          int graph_w = total_w - x_ins*2,
              graph_h = total_h - y_ins*2;

          // Check for a null or empty set...
          if (set == null || set.size() == 0) {
            g2d.setColor(RTColorManager.getColor("background", "nearbg"));
            g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight()-1);
            return bi;
          }

          // Determine which tablets can count
          Set<String> tablet_counts = new HashSet<String>();
          Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
            Tablet tablet = it_tab.next(); if (tablet.hasTimeStamps()) {
              if (count_by.equals(KeyMaker.RECORD_COUNT_STR) || KeyMaker.tabletCompletesBlank(tablet, count_by)) tablet_counts.add(tablet.fileHeader());
            }
          }

          // Determine the time resolution for the render
          long time_delta = bs.ts1() - bs.ts0(), ts0 = bs.ts0(), ts1 = bs.ts1(); XMap xmap = new XMap();
          long time_bins = RTYAFTPanel.calculateTimeBin(time_delta, graph_w); int top_line_count = RTYAFTPanel.fillXMapForLinear(xmap, time_bins, bs, ts0, ts1);

          // Accumulate the records
          Map<Tablet,KeyMaker> km_map = new HashMap<Tablet,KeyMaker>(); Map<String,Long> bin_to_long = new HashMap<String,Long>(); 
          BundlesCounterContext counter_context = new BundlesCounterContext(bs, count_by, color_by);

          Iterator<Bundle> it_bun = set.iterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next(); Tablet tablet = bundle.getTablet(); if (tablet_counts.contains(tablet.fileHeader())) {
              if (km_map.containsKey(tablet) == false) km_map.put(tablet, new KeyMaker(tablet, RTYAFTPanel.keyMakerBlank(time_bins)));
              long key = km_map.get(tablet).timeStampKey(bundle); String key_str = "" + key;
              if (bin_to_long.containsKey(key_str) == false) bin_to_long.put(key_str, key);
              counter_context.count(bundle, key_str);
            }
          }

          // Render the chart
          int bar_gap = 0, bar_w = RTYAFTPanel.calculateBarWidth(time_delta, graph_w); if (bar_w > 3) { bar_w--; bar_gap = 1; }
          Iterator<String> it_bin = counter_context.binIterator(); while (it_bin.hasNext()) {
            // Bin...
            String bin = it_bin.next(); long bin_l = bin_to_long.get(bin); int xs = xmap.ts_to_xbin.get(bin_l)*(bar_w+bar_gap);
            // Geometry...
            int bar_h = (int) (counter_context.totalNormalized(bin)*graph_h);
            Rectangle2D overall_bar = new Rectangle2D.Double(x_ins + xs, y_ins + graph_h - bar_h, bar_w, bar_h);
            // Render complete bar...
            g2d.setColor(counter_context.binColor(bin));
            g2d.fill(overall_bar);
            // By color... (if set)
            if (color_by != null && cbins != null) {
              int y_inc = y_ins + graph_h; for (int i=cbins.size()-1;i>=0;i--) {
                String cbin = cbins.get(i); double ctotal = counter_context.total(bin,cbin);
                if (ctotal > 0L) {
                  int sub_h = (int) ((ctotal * bar_h)/counter_context.binColorTotal(bin)); if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x_ins + xs, y_inc - sub_h, bar_w, sub_h);
                    y_inc -= sub_h;
                  }
                }
              }
            }
          }

          } finally { if (g2d != null) g2d.dispose(); }

          return bi;
        }

        /**
         * For the two continuous modes
         *
         *@param cbins color bins ordered globally... can be null if color_by is null
         */
        public static BufferedImage renderContinuous(Bundles bs, String count_by, String color_by, Set<Bundle> set, List<String> cbins, int total_w, int total_h) {
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_RGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();

          // Draw background and a frame
          g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,total_w,  total_h);
          g2d.setColor(RTColorManager.getColor("axis",       "minor"));   g2d.drawRect(0,0,total_w-1,total_h-1);

          // Dimensions
          int graph_w = total_w - x_ins*2,
              graph_h = total_h - y_ins*2;

          // Check for a null or empty set...
          if (set == null || set.size() == 0) {
            g2d.setColor(RTColorManager.getColor("background", "nearbg"));
            g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight()-1);
            return bi;
          }

          // Determine which tablets can count
          Set<String> tablet_counts = new HashSet<String>();
          Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
            Tablet tablet = it_tab.next();
            if (KeyMaker.tabletCompletesBlank(tablet, count_by)) tablet_counts.add(tablet.fileHeader());
          }

          Map<String,Integer> x_map = new HashMap<String,Integer>();

          // Run through the records and place them on the x axis
          BundlesCounterContext bcc_lite = new BundlesCounterContext(bs, count_by, color_by);
          Iterator<Bundle> it = set.iterator(); while (it.hasNext()) {
            Bundle bundle = it.next(); if (bundle.hasTime() && tablet_counts.contains(bundle.getTablet().fileHeader())) {
              int    x     = (int) (((graph_w) * (bundle.ts0() - bs.ts0())) / (bs.ts1() - bs.ts0())) + x_ins;
              String x_str = "" + x; x_map.put(x_str, x);
              bcc_lite.count(bundle, x_str);
            }
          }
          
          // Render
          Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
            String bin = it_bin.next(); int h = (int) (bcc_lite.totalNormalized(bin) * graph_h); if (h == 0) h = 1;

            // No Color Version
            if (color_by == null) {
              g2d.setColor(RTColorManager.getColor("data", "default"));
              // g2d.fillRect(x_map.get(bin), graph_h - y_ins - h, 1, h); // no space at top?
              g2d.fillRect(x_map.get(bin), graph_h - h + y_ins, 1, h);
            } else {
              int y_inc = graph_h - y_ins - h; // seems to leave no space at the top

              for (int i=0;i<cbins.size();i++) {
                String cbin   = cbins.get(i);
                double ctotal = bcc_lite.total(bin,cbin);
                if (ctotal > 0.0) {
                  int sub_h = (int) ((ctotal * h)/bcc_lite.binColorTotal(bin));
                  if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x_map.get(bin), y_inc + 2*y_ins, 1, sub_h);
                    y_inc += sub_h;
                  }
                }
              }

              // Draw the left overs
              if (y_inc != graph_h - y_ins) {
                g2d.setColor(RTColorManager.getColor("set", "multi"));
                g2d.fillRect(x_map.get(bin), y_inc + 2*y_ins, 1, graph_h - y_ins - y_inc);
              }
            }
          }

          } finally { if (g2d != null) g2d.dispose(); }

          return bi;
        }

        //
        // Ordering... for vector creation and small multiples rendering
        //
        static final String sm_months[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" },
                            sm_dows[]   = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
                            sm_hours[]  = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                                            "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };

        /**
         * For discrete versions of the keymaker
         *
         *@param cbins color bins ordered globally... can be null if the color_by string is also null
         */
        public static BufferedImage renderDiscrete(Bundles      bs, 
                                                   String       count_by, 
                                                   String       color_by, 
                                                   Set<Bundle>  set, 
                                                   List<String> cbins, 
                                                   String       km_str) {
          int total_bars = 1, bar_w = 1, bar_gap = 1, max_bar_h = 10;
          String order[] = null;

          // Determine the geometry of the small multiple
          if        (km_str.equals(KeyMaker.BY_MONTH_STR))     {
            total_bars = 12; bar_w = 2; bar_gap = 1; max_bar_h = 16;  order = sm_months;
          } else if (km_str.equals(KeyMaker.BY_DAYOFWEEK_STR)) {
            total_bars =  7; bar_w = 3; bar_gap = 1; max_bar_h = 14;  order = sm_dows;
          } else if (km_str.equals(KeyMaker.BY_HOUR_STR))      {
             total_bars = 24; bar_w = 2; bar_gap = 0; max_bar_h = 20; order = sm_hours;
          } else if (km_str.equals(KeyMaker.BY_DAYOFWEEK_HOUR_STR)) {
             total_bars = 24*7; bar_w = 1; bar_gap = 0; max_bar_h = 20;
             order = new String[7*24]; 
             for (int i=0;i<sm_dows.length;i++) for (int j=0;j<sm_hours.length;j++) order[i*sm_hours.length+j] = sm_dows[i] + " " + sm_hours[j];
          }

          // Determine the overall size of the small multiple
          int total_w = x_ins + total_bars * bar_w + bar_gap * (bar_w - 1) + x_ins;
          int total_h = y_ins + max_bar_h + y_ins;

          // Allocate the image
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_RGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();

          // Draw background and a frame
          g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,total_w,  total_h);
          g2d.setColor(RTColorManager.getColor("axis",       "minor"));   g2d.drawRect(0,0,total_w-1,total_h-1);

          // Check for a null or empty set...
          if (set == null || set.size() == 0) {
            g2d.setColor(RTColorManager.getColor("background", "nearbg"));
            g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight()-1);
            return bi;
          }

          // Map records into the bins for the small multiples
          BundlesCounterContext bcc_lite = new BundlesCounterContext(bs, count_by, color_by, set, km_str);

          // Calculate x offsets for the bins
          Map<String,Integer> x_map = new HashMap<String,Integer>();
          int x = x_ins; for (int i=0;i<order.length;i++) { x_map.put(order[i], x); x += bar_w + bar_gap; }

          // Go through bins and render
          Iterator<String> it = bcc_lite.binIterator(); while (it.hasNext()) {
            // Determine the total height
            String bin = it.next(); int h   = (int) (bcc_lite.totalNormalized(bin) * max_bar_h); if (h == 0) h = 1;

            // Determine coloring options
            // -- No Color
            if (color_by == null) {
              g2d.setColor(RTColorManager.getColor("data", "default"));
              g2d.fillRect(x_map.get(bin), total_h - y_ins - h, bar_w, h);

            // - Color
            } else {
              int y_inc = total_h - y_ins - h;
              for (int i=0;i<cbins.size();i++) {
                String cbin   = cbins.get(i);
                double ctotal = bcc_lite.total(bin,cbin);
                if (ctotal > 0.0) {
                  int sub_h = (int) ((ctotal * h)/bcc_lite.binColorTotal(bin));
                  if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x_map.get(bin), y_inc, bar_w, sub_h);
                    y_inc += sub_h;
                  }
                }
              }

              // Draw the left overs
              if (y_inc != total_h - y_ins) {
                g2d.setColor(RTColorManager.getColor("set", "multi"));
                g2d.fillRect(x_map.get(bin), y_inc, bar_w, total_h - y_ins - y_inc);
              }
            }
          }

        } finally { if (g2d != null) g2d.dispose(); }

        return bi;
      } 

  /**
   * Debug method for displaying the feature vectors data structure
   */ 
  private static void printFeatureVectors(PrintStream out, Map<Set<Bundle>,Map<String,Double>> fvecs) {
    Iterator<Set<Bundle>> it_set = fvecs.keySet().iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next(); Map<String,Double> fvec = fvecs.get(set);
      out.println();
      out.println("=== %< === %< === %< === %< ===");
      out.println();
      out.println("Key == Set of ...");
      Iterator<Bundle> it_bun = set.iterator(); while (it_bun.hasNext()) out.println("\t" + it_bun.next());
      out.println("Features");
      List<String> sort = new ArrayList<String>(); sort.addAll(fvec.keySet()); Collections.sort(sort);
      for (int i=0;i<sort.size();i++) out.println("\t" + sort.get(i) + "\t" + fvec.get(sort.get(i)));
    }
  }

  //
  //
  // KMeans Section
  //
  //

  /**
   * Create a k-means center feature vector.
   */
  public static Map<String,Double> kMeansCenter(Set<Map<String,Double>> set) {
    Map<String,Double>  center = new HashMap<String,Double>();   // Calculated sums
    Map<String,Integer> count  = new HashMap<String,Integer>();  // Count of the contributors to the sum (for dividing)

    // If empty, return an empty feature vector set
    if (set.size() == 0) return new HashMap<String,Double>();

    // Accumulate
    Iterator<Map<String,Double>> it_fvecs = set.iterator(); while (it_fvecs.hasNext()) {
      Map<String,Double> fvec = it_fvecs.next();
      Iterator<String> it_feature = fvec.keySet().iterator(); while (it_feature.hasNext()) {
        String feature = it_feature.next(); double value = fvec.get(feature);
        if (center.containsKey(feature)) {
          center.put(feature, center.get(feature) + value);
          count.put(feature, count.get(feature)+1);
        } else { center.put(feature,value); count.put(feature,1); }
      }
    }

    // Divide through
    Map<String,Double> result = new HashMap<String,Double>();
    Iterator<String> it = center.keySet().iterator(); while (it.hasNext()) {
      String feature = it.next(); double sum = center.get(feature); int n = count.get(feature);
      result.put(feature, sum/n);
    }

    // Maybe this is already true?
    normalizeFeatureVector(result);

    return result;
  }

  /**
   * Calculate the within-cluster sum of square 
   */
  public static double kMeansWSS(Bundles               bundles, 
                                 String                count_by, 
                                 String                color_by, 
                                 String                country_field, 
                                 Set<Set<Bundle>>      sets, 
                                 FeatureVector         feature_vector, 
                                 Set<Set<Set<Bundle>>> clusters) {
    // Create the feature vector for each set in the sets parameter ... 
    Map<Set<Bundle>,Map<String,Double>> fvecs     = createFeatureVectors(bundles, count_by, color_by, country_field, sets, feature_vector);

    double squared_sum = 0.0;
   
    // For each cluster...
    Iterator<Set<Set<Bundle>>> it_cluster = clusters.iterator(); while (it_cluster.hasNext()) {
      Set<Set<Bundle>> cluster = it_cluster.next();

      // Calculate the cluster center
      Set<Map<String,Double>> cluster_fvecs = new HashSet<Map<String,Double>>(); Iterator<Set<Bundle>> it_set = cluster.iterator(); while (it_set.hasNext()) {
        Set<Bundle> set = it_set.next(); Map<String,Double> fvec = fvecs.get(set); cluster_fvecs.add(fvec);
      }
      Map<String,Double> cluster_center_fvec = kMeansCenter(cluster_fvecs);


      // For each point in that cluster, add the sum squared distance
      it_set = cluster.iterator(); while (it_set.hasNext()) {
        Set<Bundle> set = it_set.next(); double d = distanceBetweenFeatureVectors(fvecs.get(set), cluster_center_fvec);
        if (Double.isInfinite(d) == false) squared_sum += d*d; // probably incorrect...
      }
    }

    return squared_sum;
  }

  /**
   * Calculate the Silhouette score for the clustering.
   */
  public static double kMeansSilhouette(Bundles               bundles, 
                                        String                count_by, 
                                        String                color_by, 
                                        String                country_field, 
                                        Set<Set<Bundle>>      sets, 
                                        FeatureVector         feature_vector, 
                                        Set<Set<Set<Bundle>>> clusters) {
    // Create the feature vector for each set in the sets parameter ... 
    Map<Set<Bundle>,Map<String,Double>> fvecs     = createFeatureVectors(bundles, count_by, color_by, country_field, sets, feature_vector);

    // Create all of the cluster centers
    Map<Set<Set<Bundle>>,Map<String,Double>> cluster_fvec_lu = new HashMap<Set<Set<Bundle>>,Map<String,Double>>();

    Iterator<Set<Set<Bundle>>> it_cluster = clusters.iterator(); while (it_cluster.hasNext()) {
      Set<Set<Bundle>> cluster = it_cluster.next(); Set<Map<String,Double>> cluster_fvecs = new HashSet<Map<String,Double>>();
      Iterator<Set<Bundle>> it_point = cluster.iterator(); while (it_point.hasNext()) cluster_fvecs.add(fvecs.get(it_point.next()));
      Map<String,Double> cluster_fvec = kMeansCenter(cluster_fvecs);
      cluster_fvec_lu.put(cluster,cluster_fvec);
    }
   
    double s_sum = 0.0; int s_samples = 0;

    // For each cluster...
    it_cluster = clusters.iterator(); while (it_cluster.hasNext()) {
      Set<Set<Bundle>> cluster = it_cluster.next();


      // Compute A ... average distance to the cluster center from the points within the cluster
      double a_sum = 0.0; int a_samples = 0;
      Iterator<Set<Bundle>> it_point = cluster.iterator(); while (it_point.hasNext()) {
        Set<Bundle> point = it_point.next(); double d = distanceBetweenFeatureVectors(cluster_fvec_lu.get(cluster),fvecs.get(point));
        a_sum += d; a_samples++;
      }

      // Find the closest cluster to this one
      double closest_d = Double.POSITIVE_INFINITY; Set<Set<Bundle>> closest_cluster = null;
      Iterator<Set<Set<Bundle>>> it_closest = clusters.iterator(); while (it_closest.hasNext()) {
        Set<Set<Bundle>> cluster2 = it_closest.next(); if (cluster == cluster2) continue;
        double d = distanceBetweenFeatureVectors(cluster_fvec_lu.get(cluster),cluster_fvec_lu.get(cluster2));
        if (d < closest_d) { closest_d = d; closest_cluster = cluster2; }
      }

      // Compute B ... average distance to the cluster center from the points within the closest other cluster
      double b_sum = 0.0; int b_samples = 0;
      it_point = closest_cluster.iterator(); while (it_point.hasNext()) {
        Set<Bundle> point = it_point.next(); double d = distanceBetweenFeatureVectors(cluster_fvec_lu.get(cluster),fvecs.get(point));
        b_sum += d; b_samples++;
      }

      // Compute the silhouette value
      double a_i = a_sum / a_samples, b_i = b_sum / b_samples, s_i;
      if (b_i > a_i) s_i = (b_i - a_i)/b_i; else s_i = (b_i - a_i)/a_i;  // Use the max of b_i and a_i...

      s_sum += s_i; s_samples++;
    }

    return s_sum / s_samples;
  }

  /**
   * K-Means Algorithm
   */
  public static Set<Set<Set<Bundle>>> kMeans(Bundles bundles, String count_by, String color_by, String country_field, Set<Set<Bundle>> sets, FeatureVector feature_vector, int k, int iterations) {
    if (k > sets.size()) throw new RuntimeException("kMeans():  k is greater than the number of input sets");

    // System.err.println("kMeans():  sets.size() = " + sets.size()); // DEBUG

    // Create the feature vector for each set in the sets parameter ... and make the reversible version
    Map<Set<Bundle>,Map<String,Double>>      fvecs     = createFeatureVectors(bundles, count_by, color_by, country_field, sets, feature_vector);
    Map<Map<String,Double>,Set<Set<Bundle>>> fvecs_rev = new HashMap<Map<String,Double>,Set<Set<Bundle>>>();
    Iterator<Set<Bundle>> it_set = fvecs.keySet().iterator(); while (it_set.hasNext()) { 
      Set<Bundle>        set  = it_set.next(); 
      Map<String,Double> fvec = fvecs.get(set);
      if (fvecs_rev.containsKey(fvec) == false) fvecs_rev.put(fvec, new HashSet<Set<Bundle>>());
      fvecs_rev.get(fvec).add(set);
    }

    // System.err.println("kMeans():  fvecs.keySet().size() = " + fvecs.keySet().size()); // DEBUG
    // System.err.println("kMeans():  fvecs_rev.keySet().size() = " + fvecs_rev.keySet().size()); // DEBUG

    // Create a list version of the feature vectors for random sampling
    List<Map<String,Double>> fvecs_list = new ArrayList<Map<String,Double>>(); fvecs_list.addAll(fvecs_rev.keySet());

    // Assign the fvecs to a cluster ... should be random but suspect that these data structures are probably deterministic...
    KMeansCluster km_clusters[] = new KMeansCluster[k];
    for (int i=0;i<km_clusters.length;i++) km_clusters[i] = new KMeansCluster();
    Iterator<Map<String,Double>> it_fvec = fvecs_rev.keySet().iterator();
    int m = 0; while (it_fvec.hasNext()) { km_clusters[m%km_clusters.length].addFeatureVector(it_fvec.next()); m++; }

    // Run the iterations
    for (int iters=0;iters<iterations;iters++) {
      // Create the centers from the last run (km_clusters)
      KMeansCluster km_clusters_new[] = new KMeansCluster[k];
      for (int i=0;i<km_clusters_new.length;i++) {
        // It's possible that the cluster is empty... so... in that case, put a random center in...
        if (km_clusters[i].size() > 0) km_clusters_new[i] = km_clusters[i].justTheCenter(); else {
          KMeansCluster kmc = new KMeansCluster(); 
          int n = (int) (((int) (Math.random() * 3 * fvecs_list.size()))%fvecs_list.size());
          kmc.addFeatureVector(fvecs_list.get(n));
          km_clusters_new[i] = kmc.justTheCenter();
        }
      }

      // For each original feature vector, add it to the closest center
      it_fvec = fvecs_rev.keySet().iterator(); int fvecs_assigned = 0;
      while (it_fvec.hasNext()) {
        Map<String,Double> fvec = it_fvec.next();
        int closest_j = 0; double closest_d = distanceBetweenFeatureVectors(km_clusters_new[0].centerFeatureVector(), fvec);
        for (int j=1;j<km_clusters_new.length;j++) {
          double d = distanceBetweenFeatureVectors(km_clusters_new[j].centerFeatureVector(), fvec);
          if (d < closest_d) { closest_d = d; closest_j = j; }
        }
        // If the closest is infinite, add it to a random cluster... else add it to the closest
        // ... maybe this should really be "create a new cluster center" ...  
        if (Double.isInfinite(closest_d)) {
          int r = ((int) ((Math.random() * 3 * km_clusters_new.length)))%km_clusters_new.length;
          km_clusters_new[r].addFeatureVector(fvec); fvecs_assigned++;
        } else { km_clusters_new[closest_j].addFeatureVector(fvec); fvecs_assigned++; }
      }
      // System.err.println("kMeans(): iteration: " + iters + " | fvecs_assigned = " + fvecs_assigned); // DEBUG

      // Set the new clusters to the old variable for the next iteration
      km_clusters = km_clusters_new;
    }

    // Transpose the km_clusters class into the set of sets of sets...
    Set<Set<Set<Bundle>>> result = new HashSet<Set<Set<Bundle>>>(); int sets_accounted_for = 0;
    for (int i=0;i<km_clusters.length;i++) {
      Set<Set<Bundle>> cluster_set = new HashSet<Set<Bundle>>();

      it_fvec = km_clusters[i].featureVectorIterator(); while (it_fvec.hasNext()) {
        Map<String,Double> fvec = it_fvec.next(); Set<Set<Bundle>> fvec_sets = fvecs_rev.get(fvec);
        cluster_set.addAll(fvec_sets); sets_accounted_for += fvec_sets.size();
      }

      if (cluster_set.size() > 0) result.add(cluster_set);
    }

    // System.err.println("kMeans():  sets_accounted_for = " + sets_accounted_for); // DEBUG

    return result;
  }

  //
  //
  // Hierarchical Clustering Section
  //
  //

  /**
   * Simplified helper method...
   */
  public static List<Set<Bundle>> sortByHierarchicalClustering(Bundles bundles, String count_by, String color_by, Set<Set<Bundle>> sets, FeatureVector feature_vector) {
    return sortByHierarchicalClustering(bundles, count_by, color_by, null, sets, feature_vector);
  }

  /**
   * Organize the specified sets of records by their small multiple similarity.
   *
   *@param country_field field for country names for the country small multiple... should be null for the other types...
   */
  public static List<Set<Bundle>> sortByHierarchicalClustering(Bundles bundles, String count_by, String color_by, String country_field, Set<Set<Bundle>> sets, FeatureVector feature_vector) {

    // Create the feature vector for each set in the sets parameter
    Map<Set<Bundle>,Map<String,Double>> fvecs = createFeatureVectors(bundles, count_by, color_by, country_field, sets, feature_vector);
    // printFeatureVectors(System.err, fvecs); // DEBUG

    // Flatten the feature vectors into arrays... because that's what the hierarchical clustering takes... this won't scale for sparse feature vectors...
    // ... first get all of the keys used for the features ... and we'll need a string rep for the sets...
    Set<String> feature_key_set = new HashSet<String>(); Map<Set<Bundle>,String> set_to_str = new HashMap<Set<Bundle>,String>(); Map<String,Set<Bundle>> str_to_set = new HashMap<String,Set<Bundle>>();
    int i = 0; Iterator<Set<Bundle>> it_set = fvecs.keySet().iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next(); Map<String,Double> fvec = fvecs.get(set); feature_key_set.addAll(fvec.keySet());
      set_to_str.put(set,""+i); str_to_set.put(""+i,set); i++;
    }
    // ... put them into an ordered list
    List<String> feature_key_ordered = new ArrayList<String>(); feature_key_ordered.addAll(feature_key_set);
    // ... construct a new data structure
    Map<String,double[]> fvecs_array = new HashMap<String,double[]>();
    it_set = fvecs.keySet().iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next(); Map<String,Double> fvec = fvecs.get(set); String str = set_to_str.get(set);
      double as_array[] = new double[feature_key_ordered.size()];
      for (i=0;i<as_array.length;i++) if (fvec.containsKey(feature_key_ordered.get(i))) as_array[i] = fvec.get(feature_key_ordered.get(i)); else as_array[i] = 0.0;
      fvecs_array.put(str,as_array);
    }

    // Run the hierarhical clustering
    HierarchicalClustering hc = new HierarchicalClustering(fvecs_array, HierarchicalClustering.VectorDistance.euclidean);
    List<String> hc_order = hc.dendrogramOrder();

    // Return data structure ... using the str_to_set lookup...
    List<Set<Bundle>> ordered = new ArrayList<Set<Bundle>>();
    for (i=0;i<hc_order.size();i++) { ordered.add(str_to_set.get(hc_order.get(i))); }

    return ordered;
  }

  //
  //
  // UMAP Section
  //
  //

  /**
   * Simplified helper method...
   *
   *@param bundles        bundles reference
   *@param count_by       field to count by
   *@param color_by       field to color by
   *@param sets           set of sets of records -- each set of record is an entity for the layout
   *@param feature_vector feature vector to use
   *
   *@return mapping from each set of records to the 2d point
   *
   */
  public static Map<Set<Bundle>,Point2D> umapSmallMultiplesLayout(Bundles bundles, String count_by, String color_by, Set<Set<Bundle>> sets, FeatureVector feature_vector) {
    return umapSmallMultiplesLayout(bundles, count_by, color_by, null, sets, feature_vector);
  }

  /**
   * Perform a UMAP layout on the small multiples for the specified bundles sets.
   *
   *@param bundles        bundles reference
   *@param count_by       field to count by
   *@param color_by       field to color by
   *@param country_field  field for country names for the country small multiple... should be null for the other types...
   *@param sets           set of sets of records -- each set of record is an entity for the layout
   *@param feature_vector feature vector to use
   *
   *@return mapping from each set of records to the 2d point
   *
   */
  public static Map<Set<Bundle>,Point2D> umapSmallMultiplesLayout(Bundles bundles, String count_by, String color_by, String country_field, Set<Set<Bundle>> sets, FeatureVector feature_vector) {

    // Create the feature vector for each set in the sets parameter
    Map<Set<Bundle>,Map<String,Double>> fvecs = createFeatureVectors(bundles, count_by, color_by, country_field, sets, feature_vector);

    // Make an order out of the sets
    List<Set<Bundle>> order = new ArrayList<Set<Bundle>>(); order.addAll(sets);

    // Create the distance matrix
    float dmat[][] = new float[order.size()][order.size()];
    for (int i=0;i<order.size();i++) {
      for (int j=0;j<i;j++) {
        double d = distanceBetweenFeatureVectors(fvecs.get(order.get(i)),fvecs.get(order.get(j)));
        dmat[i][j] = dmat[j][i] = (float) d;
      }
    }

    // Run umap on them
    Umap umap = new Umap();
    umap.setNumberComponents(2);
    umap.setNumberNearestNeighbours(15);
    umap.setThreads(8);
    float results[][] = umap.fitTransform(dmat);

    // Copy the restuls to the return variable
    Map<Set<Bundle>,Point2D> points_lu = new HashMap<Set<Bundle>,Point2D>();
    for (int i=0;i<order.size();i++) { points_lu.put(order.get(i), new Point2D.Double(results[i][0],results[i][1])); }
    return points_lu;
  }

  //
  //
  // Feature Vector Comparison
  //
  //

  /**
   * Determine the distance between feature vectors.  Not the most efficient thing...  but will handle sparse versions of these things...
   *
   *@param v0 first feature vector
   *@param v1 second feature vector
   *
   *@return distance between the two feature vectors
   */
  public static double distanceBetweenFeatureVectors(Map<String,Double> v0, Map<String,Double> v1) {
    double sq_sum = 0.0; int samples = 0; Iterator<String> it;

    // Start with the first vector ... for any overlap, add the square difference to the sum... for no overlap, add that specific square
    it = v0.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); 
      if (v1.containsKey(key)) sq_sum += (v0.get(key) - v1.get(key))*(v0.get(key) - v1.get(key));
      else                     sq_sum += (v0.get(key))              *(v0.get(key));
      samples++;
      if (Double.isInfinite(v0.get(key)) || Double.isNaN(v0.get(key))) System.err.println("Key \"" + key + "\" Return Problem - " + v0.get(key)); // Highlight to user
    }

    // For the second vector, only add any that weren't covered in the first pass
    it = v1.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); 
      if (v0.containsKey(key) == false) { sq_sum += (v1.get(key))*(v1.get(key)); samples++; }
      if (Double.isInfinite(v1.get(key)) || Double.isNaN(v1.get(key))) System.err.println("Key \"" + key + "\" Return Problem - " + v1.get(key)); // Highlight to user
    }

    if (samples < 1) return Double.POSITIVE_INFINITY;  // No overlap?  infinite distance... probably shouldn't ever happen
    else             return Math.sqrt(sq_sum/samples);
  }

  //
  //
  // Feature Vector Creation
  //
  //

  /**
   * FeatureVector enumeration
   */
  public enum FeatureVector { // Discrete Options
                              ByDayOfWeek,
                              ByDayOfWeekHour,
                              BySecond,
                              ByMinute,
                              ByMinuteSecond,
                              ByHour,
                              ByHourMinute,
                              ByMonth,
                              ByYear,
                              ByYearMonth,
                              ByYearMonthDay,

                              // Pie Chart Option
                              Pie,

                              // Continuous Time Option
                              Continuous,

                              // Country
                              Country };

  /**
   * Create feature vector for the sets of sets of bundles...
   *
   *@param bundles         bundles reference
   *@param count_by        field to count by
   *@param color_by        field to color by // important for the pie vector
   *@param country_field   only for the country feature vector
   *@param sets            set of sets of records -- the sets of records will correspond to a feature record
   *@param feature_vector  feature vector type
   *
   *@return map from the set of records to the feature vector
   */
  public static Map<Set<Bundle>,Map<String,Double>> createFeatureVectors(Bundles          bundles,
                                                                         String           count_by,
                                                                         String           color_by,
                                                                         String           country_field, // Only valid for Country FeatureVector type...
                                                                         Set<Set<Bundle>> sets,
                                                                         FeatureVector    feature_vector) {
    Map<Set<Bundle>,Map<String,Double>> fvecs = new HashMap<Set<Bundle>,Map<String,Double>>();

    // Figure out the keymaker string...
    // ... copies of this exact case statement are mirrored in the RTEntitiesPanel().  And maybe the RTGraphPanel()...
    String km_str = null;
    switch (feature_vector) {
      case ByDayOfWeek:      km_str = KeyMaker.BY_DAYOFWEEK_STR;       break;
      case ByDayOfWeekHour:  km_str = KeyMaker.BY_DAYOFWEEK_HOUR_STR;  break;
      case BySecond:         km_str = KeyMaker.BY_SECOND_STR;          break;
      case ByMinute:         km_str = KeyMaker.BY_MINUTE_STR;          break;
      case ByMinuteSecond:   km_str = KeyMaker.BY_MINUTE_SECOND_STR;   break;
      case ByHour:           km_str = KeyMaker.BY_HOUR_STR;            break;
      case ByHourMinute:     km_str = KeyMaker.BY_HOUR_MINUTE_STR;     break;
      case ByMonth:          km_str = KeyMaker.BY_MONTH_STR;           break;
      case ByYear:           km_str = KeyMaker.BY_YEAR_STR;            break;
      case ByYearMonth:      km_str = KeyMaker.BY_YEAR_MONTH_STR;      break;
      case ByYearMonthDay:   km_str = KeyMaker.BY_YEAR_MONTH_DAY_STR;  break;
      case Pie:                                                        break;
      case Continuous:                                                 break;
      case Country:                                                    break;
    }

    // Call the specific types of feature vector calculation
    if        (km_str         != null)                     { createFeatureVectorsDiscrete  (bundles, count_by, color_by, sets, km_str,                fvecs);
    } else if (feature_vector == FeatureVector.Pie)        { createFeatureVectorsPie       (bundles, count_by, color_by, sets,                        fvecs);
    } else if (feature_vector == FeatureVector.Continuous) { createFeatureVectorsContinuous(bundles, count_by, color_by, sets,                        fvecs);
    } else if (feature_vector == FeatureVector.Country)    { createFeatureVectorsCountry   (bundles, count_by, color_by, sets,         country_field, fvecs);
    }

    return fvecs;
  }

  // Normalize the feature vector... square sum needs to be one
  private static void normalizeFeatureVector(Map<String,Double> fvec) {
    double sq_sum = 0.0; Iterator<String> it = fvec.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); sq_sum += fvec.get(key)*fvec.get(key);
    }
    double sq = Math.sqrt(sq_sum); if (sq < 0.0001) sq = 1.0;
    it = fvec.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); fvec.put(key, fvec.get(key)/sq);
    }
  }

  // Feature vector for discrete bins
  private static void createFeatureVectorsDiscrete(Bundles                             bundles,
                                                   String                              count_by,
                                                   String                              color_by,
                                                   Set<Set<Bundle>>                    sets,
                                                   String                              km_str,
                                                   Map<Set<Bundle>,Map<String,Double>> fvecs) {
    // For each set of bundles
    Iterator<Set<Bundle>> it_set = sets.iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next();

      // Create a counter context and accumulate this specific set
      BCCLite bcc_lite = new BCCLite(bundles, count_by, color_by, set, km_str);

      // Make the feature vector
      Map<String,Double> fvec = new HashMap<String,Double>();
      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); double value = bcc_lite.totalNormalized(bin);
        fvec.put(bin, value);
      }
      normalizeFeatureVector(fvec);

      // Put it into the feature vector map
      fvecs.put(set,fvec);
    }
  }

  // Feature vector for pie charts ... pie slices are about color
  private static void createFeatureVectorsPie(     Bundles                             bundles,
                                                   String                              count_by,
                                                   String                              color_by,
                                                   Set<Set<Bundle>>                    sets,
                                                   Map<Set<Bundle>,Map<String,Double>> fvecs) {
    // For each set of bundles
    Iterator<Set<Bundle>> it_set = sets.iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next();

      // Create a counter context
      BCCLite bcc_lite = new BCCLite(bundles, count_by, color_by, set, color_by);

      // Make the feature vector
      Map<String,Double> fvec = new HashMap<String,Double>();
      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); double value = bcc_lite.totalNormalized(bin);
        fvec.put(bin, value);
      }
      normalizeFeatureVector(fvec);

      // Put it into the feature vector map
      fvecs.put(set,fvec);
    }
  }

  // Feature vector for continuous time ... will choose a binning... so may not exactly match what's on the screen
  // - doesn't handle durations...
  private static void createFeatureVectorsContinuous(Bundles                             bundles,
                                                     String                              count_by,
                                                     String                              color_by,
                                                     Set<Set<Bundle>>                    sets,
                                                     Map<Set<Bundle>,Map<String,Double>> fvecs) {
    // Global time delta
    long global_time_diff = bundles.ts1() - bundles.ts0(); if (global_time_diff < 1L) global_time_diff = 1L;

    // For each set of bundles
    Iterator<Set<Bundle>> it_set = sets.iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next();

      // Create a counter context and accumulute this specific set
      BCCLite bcc_lite = new BCCLite(bundles, count_by, color_by);
      Iterator<Bundle> it_bun = set.iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); if (bundle.hasTime()) {
          int index = (int) ((100 * (bundle.ts0() - bundles.ts0()))/global_time_diff);
          bcc_lite.count(bundle, ""+index);
        }
      }

      // Make the feature vector
      Map<String,Double> fvec = new HashMap<String,Double>();
      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); double value = bcc_lite.totalNormalized(bin);
        fvec.put(bin, value);
      }
      normalizeFeatureVector(fvec);

      // Put it into the feature vector map
      fvecs.put(set,fvec);
    }
  }

  // Feature vector for countries...
  private static void createFeatureVectorsCountry(Bundles                             bundles,
                                                  String                              count_by,
                                                  String                              color_by,
                                                  Set<Set<Bundle>>                    sets,
                                                  String                              country_field,
                                                  Map<Set<Bundle>,Map<String,Double>> fvecs) {
    // Map for the tablet to the keymaker
    Map<Tablet,KeyMaker> tablet_km_lu = new HashMap<Tablet,KeyMaker>();

    // For each set of bundles
    Iterator<Set<Bundle>> it_set = sets.iterator(); while (it_set.hasNext()) {
      Set<Bundle> set = it_set.next();

      // Create a counter context and accumulute this specific set
      BundlesCounterContext bcc = new BundlesCounterContext(bundles, count_by, color_by);
      Iterator<Bundle> it_bun = set.iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); Tablet tablet = bundle.getTablet(); 

        // Make the key maker (if the tablet can be used for this...)
        if (tablet_km_lu.containsKey(tablet) == false) {
          if (KeyMaker.tabletCompletesBlank(tablet, country_field) &&
              (count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet,count_by))) {
            tablet_km_lu.put(tablet, new KeyMaker(tablet,country_field));
          }
        }

        // Get the key maker... and count this bundle
        KeyMaker km = tablet_km_lu.get(tablet); if (km != null) {
          String a3s[] = km.stringKeys(bundle); if (a3s != null) {
            for (int i=0;i<a3s.length;i++) bcc.count(bundle,a3s[i]);
          }
        }
      }

      // Make the feature vector
      Map<String,Double> fvec = new HashMap<String,Double>();
      Iterator<String> it_bin = bcc.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); double value = bcc.totalNormalized(bin);
        fvec.put(bin, value);
      }
      normalizeFeatureVector(fvec);

      // Put it into the feature vector map
      fvecs.put(set,fvec);
    }
  }
}

/**
 * Represents a k-means cluster ... used for the k-means algorithm above.
 */
class KMeansCluster {
  Map<String,Double> center_fvec;

  /**
   * Create a new kmeans cluster with just the center fvec (calculated from these feature vectors)
   */
  public KMeansCluster justTheCenter() {
    KMeansCluster kmc = new KMeansCluster();
    kmc.center_fvec = SmallMultiples.kMeansCenter(fvecs);
    return kmc;
  }

  /**
   * Return the number of feature vectors in this cluster.
   *
   *@return number of feature vectors
   */
  public int size() { return fvecs.size(); }

  /**
   * Return the center feature vector -- note this assumes the instance was created with the justTheCenter() method.
   */
  public Map<String,Double> centerFeatureVector() { return center_fvec; }

  /**
   * Feature vectors that have been added
   */
  Set<Map<String,Double>> fvecs = new HashSet<Map<String,Double>>();

  /**
   * Add a feature vector to this cluster center -- does not update the center.
   *
   *@param fvec feature vector to add
   */
  public void addFeatureVector(Map<String,Double> fvec) { fvecs.add(fvec); }

  /**
   * Return an iterator over the added feature vectors.
   *
   *@return iterator over added feature vectors
   */
  public Iterator<Map<String,Double>> featureVectorIterator() { return fvecs.iterator(); }
}

