/*

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Shape;

import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Rectangle2D;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Hexagonl Grid shell for the MakeAHexMap results...
 */
public class MadeHexMaps extends WorldMap {
  /**
   * Instance shape quality
   */
  public enum ShapeQuality { SMALL_MULTIPLES_50M_LOWRES,
                             SMALL_MULTIPLES_50M_MEDRES,
                             REGULAR_50M_HIGHRES };

  /**
   * Instance lookups...
   */
  private static Map<ShapeQuality,MadeHexMaps> instance_lookup = new HashMap<ShapeQuality,MadeHexMaps>();
  private static MadeHexMaps                   empty           = new MadeHexMaps();

  /**
   * getInstance() -- returns a singleton...
   */
  public static MadeHexMaps getInstance(ShapeQuality shape_quality) throws IOException {
    if (instance_lookup.containsKey(shape_quality) == false) {
      String resource_str = null;
      switch (shape_quality) {
        case SMALL_MULTIPLES_50M_LOWRES: resource_str = "/hexagonalmaps/small_multiples_made_hex_map_50m.csv"; break;
        case SMALL_MULTIPLES_50M_MEDRES: resource_str = "/hexagonalmaps/20211103_made_hex_map_50m.csv";        break;
        case REGULAR_50M_HIGHRES:        resource_str = "/hexagonalmaps/made_hex_map_50m.csv";                 break;
        default:                         throw new RuntimeException("Do Not Have Instance Type \"" + shape_quality + "\"");
      }
      MadeHexMaps mhm = new MadeHexMaps(empty.getClass().getResourceAsStream(resource_str));
      instance_lookup.put(shape_quality, mhm);
    }
    return instance_lookup.get(shape_quality);
  }

  /**
   * Stupid... need an instance to use the getClass() method...  this creats an empty version...
   */
  private MadeHexMaps() { super(); }

  /**
   * a3 country code to the shapes making up those countries
   */
  Map<String,Set<Shape>> a3_to_shapes = new HashMap<String,Set<Shape>>();

  /**
   * Construct the map ... private because we don't want multiple instances created...
   */
  private MadeHexMaps(InputStream hexagonal_csv_in) throws IOException { 
    super(); 

    // Read the input buffer stream of country names and shapes
    BufferedReader in = new BufferedReader(new InputStreamReader(hexagonal_csv_in));
    String line; while ((line = in.readLine()) != null) {
      if (line.equals("")) { } else {
        StringTokenizer st = new StringTokenizer(line, ",");

        // Get the A3 country code
        String a3 = st.nextToken();
        a3codeLookup.put(a3.toLowerCase(),a3.toLowerCase());
        a3codeLookup.put(a3.toUpperCase(),a3.toLowerCase());

        // Create the shape based on the points in the line
        Path2D path2d = new Path2D.Double();
        double x      = Double.parseDouble(st.nextToken()),
               y      = Double.parseDouble(st.nextToken());
        path2d.moveTo(x,y);
        while (st.hasMoreTokens()) {
          x = Double.parseDouble(st.nextToken());
          y = Double.parseDouble(st.nextToken());
          path2d.lineTo(x,y);
        }
        path2d.closePath();

        // Save it into the data structure
        if (a3_to_shapes.containsKey(a3) == false) a3_to_shapes.put(a3, new HashSet<Shape>());
        a3_to_shapes.get(a3).add(path2d);
      }
    }

    calculateBounds(); 
  }

  /**
   * bounds of the world
   */
  private Rectangle2D bounds = new Rectangle2D.Double(-180.0,-90.0,180.0,90.0);

  /** 
   * Calculate the bounds of the shapes
   */
  private void calculateBounds() {
    double x_min = Double.POSITIVE_INFINITY, y_min = Double.POSITIVE_INFINITY,
           x_max = Double.NEGATIVE_INFINITY, y_max = Double.NEGATIVE_INFINITY;
    Iterator<String> it_a3 = a3_to_shapes.keySet().iterator(); while (it_a3.hasNext()) {
      String a3 = it_a3.next(); Iterator<Shape> it_shape = a3_to_shapes.get(a3).iterator(); while (it_shape.hasNext()) {
        Shape shape = it_shape.next(); Rectangle2D rect = shape.getBounds2D();
        if (rect.getX() < x_min) x_min = rect.getX();
        if (rect.getY() < y_min) y_min = rect.getY();
        if ((rect.getX()+rect.getWidth())  > x_max) x_max = rect.getX() + rect.getWidth();
        if ((rect.getY()+rect.getHeight()) > y_max) y_max = rect.getY() + rect.getHeight();
      }
    }
    if (Double.isInfinite(x_min) == false) { bounds = new Rectangle2D.Double(x_min,y_min,x_max-x_min,y_max-y_min); } else System.err.println("calculateBounds() - Infinites Found");
  }

  /**
   * Return the bounds of the world.
   *
   *@return rectangular boundary
   */
  @Override
  public Rectangle2D getBounds() { return bounds; }

  /**
   * Return an iterator over the country codes.
   *
   *@return iterator over country code strings
   */
  @Override
  public Iterator<String> countryCodeIterator() { return a3_to_shapes.keySet().iterator(); }

  /**
   * Return the shape for a country code.
   *
   *@param cc country code
   *
   *@return shape
   */
  @Override
  public Set<Shape> getCountryShapes(String cc) { return a3_to_shapes.get(cc.toLowerCase()); }
}

