/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.visualization;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

import racetrack.util.Utils;

/**
 * Implements a treemap layout algorithm.
 *
 *@version 0.1
 */
public class TreeMap<K,V> {
  /**
   * Groupings... K will be used as a key that goes to the layout
   */
  Map<K,Set<V>> grps;

  /**
   * Total in the groupings
   */
  int           total,

  /**
   * Square root of the total (ceiling)
   */
                total_sqrt;

  /**
   * Construct the TreeMap by obtaining the groupings and counting the total number.
   *
   *@param groupings groupings for treemap... the value sets are the actual objects that need to fit into the tree map
   */
  public TreeMap(Map<K,Set<V>> groupings) { 
    this.grps = groupings;
    Iterator<K> it = grps.keySet().iterator(); while (it.hasNext()) {
      K key = it.next(); Set<V> set = grps.get(key);
      total += set.size();
    }
    total_sqrt = (int) Math.ceil(Math.sqrt(total)); if (total_sqrt <= 1) total_sqrt = 1;
  }

  /**
   * Derive the layout for the tile mapping.
   */
  public Map<K,Rectangle2D> squarifiedTileMapping() {
    // Sort the list by size
    List<Sortable> sort = new ArrayList<Sortable>();
    Iterator<K> it = grps.keySet().iterator(); while (it.hasNext()) {
      K key = it.next();
      int size = grps.get(key).size(); 
      sort.add(new Sortable(key, size));
    }
    Collections.sort(sort);

    // Initialize the area for the layout
    Rectangle2D        rect    = new Rectangle2D.Double(0.0, 0.0, total_sqrt, total_sqrt);
    Map<K,Rectangle2D> results = new HashMap<K,Rectangle2D>();
    squarify(results, rect, true, sort, sort.size()-1);

    return results;
  }

  /**
   * Recursive procedure to layout the results in a squarifed format.
   */
  protected void squarify(Map<K,Rectangle2D> results, Rectangle2D rect, boolean vertical, List<Sortable> list, int list_i) {
    // Figure out how many items fit along the current rect's edge
    double along_edge = vertical ? rect.getHeight() : rect.getWidth();

    // Get the next item to place
    List<Sortable> placed = new ArrayList<Sortable>(); // Structure to keep track of what we've placed so far
    // K key = list.get(list_i).key; 

    int size = list.get(list_i).size; 
    placed.add(list.get(list_i)); 
    list_i--; // definitely placing the first one...

    // Figure out the other edge
    double other_edge = size/along_edge;

    // Find the initial ratio
    double ratio = (along_edge > other_edge) ? (along_edge/other_edge) : (other_edge/along_edge); // Choose greater than 1.0 answer
    // System.out.println("int_ratio = " + ratio);

    // Add additional items as long as ratio gets better... better is closer to one
    double new_ratio;
    while (list_i >= 0 && (new_ratio = newRatio(list.get(list_i).size, placed, along_edge)) < ratio) { 
      ratio = new_ratio;
      placed.add(list.get(list_i)); 
      list_i--; 
    }

    // Do the placement (i.e., update the results structure)
    Rectangle2D new_rect = place(results, rect, vertical, placed);

    // Recurse if anything is left
    if (list_i >= 0) { squarify(results, new_rect, !vertical, list, list_i); }
  }

  /**
   * Determine the new ratio if the next size is added to the already placed along this edge.
   *
   *@param size       new size to add
   *@param placed     sizes already placed along the edge
   *@param along_edge how many fit along an edge
   *@param other_edge fit along the other edge
   *
   *@return new ratio if the size is added
   */
  protected double newRatio(int size, List<Sortable> placed, double along_edge) {
    int size_sum = 0; for (int i=0;i<placed.size();i++) size_sum += placed.get(i).size; size_sum += size;
    double other_edge = size_sum/along_edge;
           along_edge = size / other_edge; // Recalculate the along edge for this specific placement
    double new_ratio  =  (along_edge > other_edge) ? (along_edge/other_edge) : (other_edge/along_edge); // Choose greater than 1.0 answer
    // System.out.println("  new_ratio = " + new_ratio);
    return new_ratio;
  }

  /**
   * Put the results for this placement... return a new rectangle that reflects what's left
   *
   *@param results    results from the placement
   *@param rect       existing rectangle
   *@param vertical   arrange new regions vertically
   *@param placed     sizes already placed along the edge
   *@param along_edge how many fit along an edge
   *
   *@return rectangle describing what's left over
   */
  protected Rectangle2D place(Map<K,Rectangle2D> results, Rectangle2D rect, boolean vertical, List<Sortable> placed) {
    // Get the total size
    int size_sum = 0; for (int i=0;i<placed.size();i++) size_sum += placed.get(i).size;

    double along_edge = vertical ? rect.getHeight() : rect.getWidth();
    double other_edge = size_sum / along_edge;

    double x = rect.getX(), y = rect.getY();

    if (vertical) {
      // Vertical placement
      for (int i=0;i<placed.size();i++) {
        double h = placed.get(i).size / other_edge;
        results.put(placed.get(i).key, new Rectangle2D.Double(x,y,other_edge,h));
        y += h;
      }
      return new Rectangle2D.Double(rect.getX() + other_edge, rect.getY(), rect.getWidth() - other_edge, rect.getHeight());

    } else        {
      // Horizontal placement
      for (int i=0;i<placed.size();i++) {
        double w = placed.get(i).size / other_edge;
        results.put(placed.get(i).key, new Rectangle2D.Double(x,y,w,other_edge));
        x += w;
      }
      return new Rectangle2D.Double(rect.getX(), rect.getY() + other_edge, rect.getWidth(), rect.getHeight() - other_edge);

    }
  }

  /**
   * Simple sorting class
   */
  protected class Sortable implements Comparable<Sortable> {
    K key; int size;
    public Sortable(K key, int size) { this.key = key; this.size = size; }
    public int compareTo(Sortable o) { if (size < o.size) return -1; else if (size == o.size) return 0; else return 1; }
  }

  /**
   * For the results, apply them to a world mapping component.  Modification is to fit the return within the
   * specified coordinates (x0,y0) to (x1,y1).
   *
   *@param regions results from the squarifiedTileMapping call above
   *@param wxy     world x,y mapping -- modified for return
   *@param x0      min x
   *@param y0      min y
   *@param x1      max x
   *@param y1      max y
   *
   */
  public void transformWorldPoints(Map<K,Rectangle2D> regions, Map<V,Point2D> wxy, double x0, double y0, double x1, double y1) {
    // Run the default transform
    transformWorldPoints(regions,wxy); 

    // Figure out the coordinate extents
    double x_min = Double.POSITIVE_INFINITY, y_min = Double.POSITIVE_INFINITY, 
           x_max = Double.NEGATIVE_INFINITY, y_max = Double.NEGATIVE_INFINITY;
    Iterator<K> itk = regions.keySet().iterator(); while (itk.hasNext()) {
      K k = itk.next(); Iterator<V> itv = grps.get(k).iterator(); while (itv.hasNext()) {
        V v = itv.next(); Point2D pt = wxy.get(v);
        if (pt.getX() < x_min) x_min = pt.getX();
        if (pt.getX() > x_max) x_max = pt.getX();
        if (pt.getY() < y_min) y_min = pt.getY();
        if (pt.getY() > y_max) y_max = pt.getY();
      }
    }

    // Make sure they aren't the same
    if (x_min == x_max) { x_min -= 0.5; x_max += 0.5; }
    if (y_min == y_max) { y_min -= 0.5; y_max += 0.5; }
    if (x0    == x1)    { x0    -= 0.5; x1    += 0.5; }
    if (y0    == y1)    { y0    -= 0.5; y1    += 0.5; }

    // Scale to fit within the specified coordinates
    itk = regions.keySet().iterator(); while (itk.hasNext()) {
      K k = itk.next(); Iterator<V> itv = grps.get(k).iterator(); while (itv.hasNext()) {
        V v = itv.next(); Point2D pt = wxy.get(v);

        // Normalize 0.0 ... 1.0
        double wx = (pt.getX() - x_min)/(x_max - x_min),
               wy = (pt.getY() - y_min)/(y_max - y_min);

        // Rescale
        wx = wx * (x1 - x0) + x0;
        wy = wy * (y1 - y0) + y0;

        // Store
        wxy.put(v,new Point2D.Double(wx,wy));
      }
    }
  }

  /**
   * For the results, apply them to a world mapping component.
   *
   *@param regions results from the squarifiedTileMapping call above
   *@param wxy     world x,y mapping -- modified for return
   */
  public void transformWorldPoints(Map<K,Rectangle2D> regions, Map<V,Point2D> wxy) {
    // Iterate over each region -- for the region, set the point locations
    Iterator<K> its = regions.keySet().iterator(); while (its.hasNext()) {
      K           k      = its.next();
      Rectangle2D region = regions.get(k); 
                  region = new Rectangle2D.Double(region.getX() + region.getWidth()  * 0.1, // Make it a little smaller
                                                  region.getY() + region.getHeight() * 0.1,
                                                  region.getWidth() * 0.8, region.getHeight() * 0.8);
      Set<V>      set    = grps.get(k);

      // Determine the edge size ... idealized if it were a square
      int         edge   = (int) Math.sqrt(set.size()); edge++;

      double      ratio  = region.getWidth() / region.getHeight();
      int         x_edge = (int) (ratio * edge); if (x_edge < 1) x_edge = 1;
      int         y_edge  = set.size() / x_edge; if (y_edge < 1) y_edge = 1; y_edge++;

      // Place all of the points in this set with the rectangle
      int x = 0, 
          y = 0;
      Iterator<V> itp    = set.iterator(); while (itp.hasNext()) {
        V pt = itp.next();

        double wx, wy; 
        if ((x_edge%2) == 1) { // Odd
          if        (x == x_edge/2) { wx = region.getX() + region.getWidth()/2.0;
          } else if (x <  x_edge/2) { wx = region.getX() + region.getWidth()/2.0 - (x_edge/2 - x) * region.getWidth()/x_edge;
          } else                    { wx = region.getX() + region.getWidth()/2.0 + (x - x_edge/2) * region.getWidth()/x_edge;
          }
        } else {               // Even
          wx = region.getX() + region.getWidth()/2.0;
          double extra = region.getWidth()/(2*x_edge);
          if        (x <  x_edge/2) { wx = region.getX() + region.getWidth()/2.0 + extra - (x_edge/2 - x) * region.getWidth()/x_edge;
          } else                    { wx = region.getX() + region.getWidth()/2.0 + extra + (x - x_edge/2) * region.getWidth()/x_edge;
          }
        }

        if ((y_edge%2) == 1) { // Odd
          if        (y == y_edge/2) { wy = region.getY() + region.getHeight()/2.0;
          } else if (y <  y_edge/2) { wy = region.getY() + region.getHeight()/2.0 - (y_edge/2 - y) * region.getHeight()/y_edge;
          } else                    { wy = region.getY() + region.getHeight()/2.0 + (y - y_edge/2) * region.getHeight()/y_edge;
          }
        } else {               // Even
          wy = region.getY() + region.getHeight()/2.0;
          double extra = region.getHeight()/(2*y_edge);
          if        (y <  y_edge/2) { wy = region.getY() + region.getHeight()/2.0 + extra - (y_edge/2 - y) * region.getHeight()/y_edge;
          } else                    { wy = region.getY() + region.getHeight()/2.0 + extra + (y - y_edge/2) * region.getHeight()/y_edge;
          }
        }

        wxy.put(pt, new Point2D.Double(wx,wy));

        x++; if (x >= x_edge) { x = 0; y++; }
      }
    }
  }

  /**
   * For the results, apply them to a world mapping component. Old version... tried to make the results too square.
   *
   *@param regions results from the squarifiedTileMapping call above
   *@param wxy     world x,y mapping
   */
  public void transformWorldPointsXXX(Map<K,Rectangle2D> regions, Map<V,Point2D> wxy) {
    // Iterate over each region -- for the region, set the point locations
    Iterator<K> its = regions.keySet().iterator(); while (its.hasNext()) {
      K           k      = its.next();
      Rectangle2D region = regions.get(k); 
                  region = new Rectangle2D.Double(region.getX() + region.getWidth() * 0.1, region.getY() + region.getHeight() * 0.1, 
                                                  region.getWidth() * 0.8, region.getHeight() * 0.8);
      Set<V>      set    = grps.get(k);

      // Determine the edge size
      int         edge   = (int) Math.sqrt(set.size()); edge++;

      // Place all of the points in this set with the rectangle
      int x = 0, y = 0;
      Iterator<V> itp    = set.iterator(); while (itp.hasNext()) {
        V pt = itp.next();
        wxy.put(pt, new Point2D.Double(region.getX() + region.getWidth() * x / edge, region.getY() + region.getHeight() * y / edge));
        x++; if (x >= edge) { x = 0; y++; }
      }
    }
  }

  /**
   *
   */
  protected static void testData(Map<String,Set<String>> map, Map<String,String> map_rev) {
    int number_of_sets = randInt(5,30);
    for (int i=0;i<number_of_sets;i++) {
      String set_name = "set_" + i;
      map.put(set_name, new HashSet<String>());
      int number_of_elements = randInt(1,100);
      for (int j=0;j<number_of_elements;j++) {
        String element_name = set_name + "_" + j;
        map.get(set_name).add(element_name);
        map_rev.put(element_name, set_name);
      }
    }
  }
  protected static int randInt(int min, int max) { return (int) (min + Math.random() * (max - min)); }

  /**
   * Test main routine
   */
  public static void main(String args[]) {
    Map<String,Set<String>> test     = new HashMap<String,Set<String>>(); // SetName => Set of Elements
    Map<String,String>      test_rev = new HashMap<String,String>();      // Set Element => SetName
    testData(test, test_rev);

    // Run the tree map algorithm and get the results
    TreeMap<String,String>  treemap = new TreeMap<String,String>(test);
    Map<String,Rectangle2D> results = treemap.squarifiedTileMapping();
    Map<String,Point2D>     wxy     = new HashMap<String,Point2D>();
    treemap.transformWorldPoints(results, wxy);

    // Find the mins and maxes
    double x_min = Double.POSITIVE_INFINITY, y_min = Double.POSITIVE_INFINITY,
           x_max = Double.NEGATIVE_INFINITY, y_max = Double.NEGATIVE_INFINITY;

    Iterator<String> it = results.keySet().iterator(); while (it.hasNext()) {
      String      key  = it.next();
      Rectangle2D rect = results.get(key);
      if (rect.getX() < x_min) x_min = rect.getX();
      if (rect.getY() < y_min) y_min = rect.getY();
      if ((rect.getX()+rect.getWidth())  > x_max) x_max = rect.getX()+rect.getWidth();
      if ((rect.getY()+rect.getHeight()) > y_max) y_max = rect.getY()+rect.getHeight();
    }

    // Plot them in an image
    BufferedImage bi  = new BufferedImage(500, 500, BufferedImage.TYPE_INT_RGB);
    Graphics2D    g2d = (Graphics2D) bi.getGraphics();
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    // Plot the actual points
    it = wxy.keySet().iterator(); while (it.hasNext()) {
      String  key  = it.next();
      Point2D pt   = wxy.get(key);
      g2d.setColor(RTColorManager.getColor(test_rev.get(key)));
      int x = (int) (bi.getWidth()  * (pt.getX() - x_min)/(x_max - x_min)),
          y = (int) (bi.getHeight() * (pt.getY() - y_min)/(y_max - y_min));
      g2d.fill(new Ellipse2D.Double(x-1,y-1,3,3));
    }

    // Plot the rectangular regions
    it = results.keySet().iterator(); while (it.hasNext()) {
      String      key  = it.next();
      Rectangle2D rect = results.get(key);

      double x0 = (rect.getX() - x_min)/(x_max - x_min),
             y0 = (rect.getY() - y_min)/(y_max - y_min),
             x1 = ((rect.getX() + rect.getWidth())  - x_min)/(x_max - x_min),
             y1 = ((rect.getY() + rect.getHeight()) - y_min)/(y_max - y_min);
      System.err.println(x0 + "," + y0 + " x " + x1 + "," + y1);
      Rectangle2D trans   = new Rectangle2D.Double(x0*bi.getWidth(),  
                                                   y0*bi.getHeight(),
                                                   (x1-x0)*bi.getWidth(),
                                                   (y1-y0)*bi.getHeight()); 
      Rectangle2D smaller = new Rectangle2D.Double(x0*bi.getWidth()  + (x1-x0)*bi.getWidth() *0.01,
                                                   y0*bi.getHeight() + (y1-y0)*bi.getHeight()*0.01,
                                                   (x1-x0)*bi.getWidth()  * 0.98,
                                                   (y1-y0)*bi.getHeight() * 0.98);
      g2d.setColor(Color.white);
      g2d.draw(trans);
      g2d.setColor(RTColorManager.getColor(key));
      g2d.draw(smaller);

      int x_cen = (int) (bi.getWidth()  * (x0 + x1)/2.0),
          y_cen = (int) (bi.getHeight() * (y0 + y1)/2.0);
      int txt_h = Utils.txtH(g2d, key),
          txt_w = Utils.txtW(g2d, key);
      g2d.setColor(Color.white);
      g2d.drawString(key, x_cen - txt_w/2, y_cen + txt_h /2);
    }

    // Write to disk
    try { ImageIO.write(bi, "PNG", new FileOutputStream(new File("treemap_example.png")));
    } catch (IOException ioe) { System.err.println("IOException: " + ioe); }
  }
}

