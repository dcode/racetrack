/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import racetrack.util.EntityExtractor;
import racetrack.util.SubText;
import racetrack.util.Utils;

/**
 * Analytics for text.
 */
public class TextAnalytics {
  /**
   * Textual distance method to use
   */
  public enum DistanceMethod { 
  /**
   * Known entity types
   */
                             ENTITIES, 
  /**
   * All regular words - lowercased
   */
                             WORDS, 
  /**
   * All regular words - lowercased and stemmed
   */
                             WORDS_STEMMED };
  /**
   * Determine the distance between to strings of text based on the specified method.
   * A lower score indicates that the texts are similar -- i.e., a zero indicates that
   * they contain the exact same word histograms (not necessarily in the same order).
   *
   *@param s0     first text
   *@param s1     second text
   *@param method distance method
   *
   *@return distance method from 0.0 (exactly the same) to 1.0 (not similar at all)
   */
  public double distance(String s0, String s1, DistanceMethod method) {
    /**
     * Get the occurences in each text.
     */
    Map<String,Integer> s0_occ = occurences(s0, method),
                        s1_occ = occurences(s1, method);

    /**
     * Construct the union and intersection
     */
    Map<String,Integer> union  = new HashMap<String,Integer>(),
                        inter  = new HashMap<String,Integer>();
    // Iterate over words in s0 first
    Iterator<String> it = s0_occ.keySet().iterator(); while (it.hasNext()) {
      String s = it.next();
      if (s1_occ.containsKey(s)) {
        int s0_count = s0_occ.get(s), s1_count = s1_occ.get(s); int min, max;
        if (s0_count > s1_count) { max = s0_count; min = s1_count; }
        else                     { max = s1_count; min = s0_count; }
        union.put(s, max); inter.put(s, min);
      } else union.put(s, s0_occ.get(s));
    }
    // Iterate over words in s1 next... simpler because the intersection has already been completed
    it = s1_occ.keySet().iterator(); while (it.hasNext()) {
      String s = it.next();
      if (s0_occ.containsKey(s) == false) { union.put(s, s1_occ.get(s)); }
    }

    /**
     * Determine the ratio between the two
     */
    int union_count = 0, inter_count = 0;
    it = union.keySet().iterator(); while (it.hasNext()) union_count += union.get(it.next());
    it = inter.keySet().iterator(); while (it.hasNext()) inter_count += inter.get(it.next());

    /**
     * Protect against NaN
     */
    double d = ((double) inter_count)/((double) union_count);
    if (Double.isNaN(d)) return 1.0; else return d;
  }

  /**
   * Return a map of the occurences to counts.
   *
   *@param s      text
   *@param method method to use for extracting words
   *
   *@return map of the identified string in the text to the number of occurences
   */
  public Map<String,Integer> occurences(String s, DistanceMethod method) {
    Map<String,Integer> map = new HashMap<String,Integer>();
    if (method == DistanceMethod.ENTITIES) {
      Iterator<SubText> it = EntityExtractor.list(s).iterator();
      while (it.hasNext()) {
        SubText subtext = it.next(); String word = subtext.toString().toLowerCase();
        if (map.containsKey(word) == false) map.put(word, 1); else map.put(word, map.get(word) + 1);
      }
    } else {
      StringTokenizer st = new StringTokenizer(s, "\r\n\t -.!?(),;:/[]{}\"\'");
      while (st.hasMoreTokens()) {
        String word = st.nextToken().toLowerCase();
        if (method == DistanceMethod.WORDS_STEMMED) word = stem(word);
        if (map.containsKey(word) == false) map.put(word, 1); else map.put(word, map.get(word) + 1);
      }
    }
    return map;
  }

  /**
   * Stem a word into its root.  Really dumb implementation... should use the Porter Stemmer instead...
   *
   *@param s string to stem
   *
   *@return stemmed string
   */
  public static String stem(String s) {
    s = s.toLowerCase();
    char c1 = ' ', c2 = ' ', c3 = ' ', c4 = ' ', c5 = ' ';

    if (s.length() > 1) c1 = s.charAt(s.length()-1);
    if (s.length() > 2) c2 = s.charAt(s.length()-2);
    if (s.length() > 3) c3 = s.charAt(s.length()-3);
    if (s.length() > 4) c4 = s.charAt(s.length()-4);
    if (s.length() > 5) c5 = s.charAt(s.length()-5);

    if (c1 == 's') {
      if        (c3 != 's' && c2 == 'e') { s = s.substring(0,s.length()-2);
      } else if (             c2 == 's') { // do nothing
      } else                             { s = s.substring(0,s.length()-1); }
    }

    if (c4 == 'm' && c3 == 'e' && c2 == 'n' && c1 == 't') s = s.substring(0,s.length()-4);

    if (                          c2 == 'l' && c1 == 'y') s = s.substring(0,s.length()-2);

    if (                          c2 == 'e' && c1 == 'd') s = s.substring(0,s.length()-2);

    if (             c3 == 'i' && c2 == 'n' && c1 == 'g' && s.length() > 6) s = s.substring(0,s.length()-3);

    return s;
  }

  /**
   * Stop words for the tokenizer.  Original list is from:
   *
   * https://www.textfixer.com/tutorials/common-english-words.txt
   *
   * Added some contractions... and other words as then came up -- e.g., numbers
   */
  private static Set<String> stop_words;
  static {
    stop_words = new HashSet<String>();
    String strs[] = { "a","able","about","across","after","all","almost","also","am","among","an","and","any","are",
                      "aren't","as","at","be","because","been","before","being","but","by","can","can't","cannot","could","could've",
                      "couldn't","dear","did","didn't","do","don't","does","doesn't","during","either","else","ever","every",
                      "for","from","get","goes","gone","got","gotten","had","has","hasn't","have","haven't","he","her","hers","him",
                      "his","how","however","i","if","in","into","is","isn't","it","its","it's","just","least","let",
                      "like","likely","may","me","might","most","must","must've","my","neither","no","nor","not","of",
                      "off","often","on","only","or","other","our","out","own","rather","said","say","says","she","should",
                      "shouldn't","should've","since","so","some","than","that","the","their","them","then","there",
                      "these","they","they're","this","tis","to","too","twas","us","wants","was","wasn't","we","were",
                      "weren't","what","when","where","which","while","who","whom","why","will","with","won't","would",
                      "wouldn't","would've","yet","you","your", "you're", "",
                      "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
                      "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty" };

    for (int i=0;i<strs.length;i++) stop_words.add(strs[i]);
  }

  /**
   * Tokenize a string using standard delimiters.
   *
   *@param str   string to tokenize
   *@param stem  flag indicating that the tokens should be stemmed
   *
   *@return list of tokens
   */
  public static String[] tokenize(String str, boolean stem) {
    StringTokenizer st = new StringTokenizer(str, " ,!;:-(){}[]?+*\"\r\n\t");
    List<String> list = new ArrayList<String>();
    while (st.hasMoreTokens()) {
      String token = st.nextToken().toLowerCase();

      token = removeBeginsEnds(token, ".");
      token = removeBeginsEnds(token, "\'");

      if (stop_words.contains(token) == false) {
        if (stem) token = TextAnalytics.stem(token);
        if (Utils.allNumbers(token)) { } else list.add(token);
      }
    }

    String array[] = new String[list.size()];
    for (int i=0;i<array.length;i++) array[i] = list.get(i);
    return array;
  }

  /**
   * Remove all occurrences of to_remove from the beginning and ending of the string.
   *
   *@param str string to modify
   *@param to_remove substring to remove
   *
   *@return string with to_remove removed from beginning and ending
   */
  private static String removeBeginsEnds(String str, String to_remove) {
    while (str.startsWith(to_remove)) str = str.substring(to_remove.length(),str.length());
    while (str.endsWith  (to_remove)) str = str.substring(0,str.length() - to_remove.length());
    return str;
  }

  /**
   * Add to stop words.
   *
   *@param to_add stop words to add
   */
  public static void addStopWords(Collection<String> to_add) {
    Iterator<String> it = to_add.iterator(); while (it.hasNext()) {
      String str = it.next().toLowerCase();
      stop_words.add(str);
    }
  }

  /**
   * Clear all stop words.
   */
  public static void clearStopWords() { stop_words.clear(); }

  /**
   * Set the stop words.
   *
   *@param to_set stop words to use
   */
  public static void setStopWords(Collection<String> to_set) { clearStopWords(); addStopWords(to_set); }

  /**
   * Test stub
   */
  public static void main(String args[]) {
    TextAnalytics ta = new TextAnalytics();

    String tests[][] = { { "Mary Had A Little Lamb",                 "A Little Lamb, A Little Lamb" },
                         { "127.0.0.1 and 127.0.0.1",                "127.0.0.1 and 192.168.0.1 and 10.0.0.1" },
                         { "a b c d e f g h i j k l",                "a b c d e f g h i j k l m" },
                         { "a b c d e f g h i j k l",                "b b b b e f g h i j k l m" },
                         { "a b c d e f g h i j k l",                "b b c d e f g h i j k l m n" },
                         { "A B C D E F G H I J K L",                "b b c d e f g h i j k l m n" } };
    for (int i=0;i<tests.length;i++) {
      System.out.println("*******************************************************\n");
      System.out.println(" s0 = \"" + tests[i][0] + "\"");
      System.out.println(" s1 = \"" + tests[i][1] + "\"");
      for (DistanceMethod dm : DistanceMethod.values()) { System.out.println(" Method : " + dm + " : distance = " + ta.distance(tests[i][0],tests[i][1], dm)); }
      System.out.println();
    }

    System.out.println("**\n** Stemmed Testing\n**");

    String stems[] = { "swing", "tests", "and", "a", "an", "", "to", "from", "statement", "swiftly", "running", "swinging", "jumping", "testing",
                       "ring",  "string", "sting", "bing" };
     
    for (int i=0;i<stems.length;i++) { System.out.println("\"" + stems[i] + "\" ==> \"" + stem(stems[i]) + "\""); }
  }
}

