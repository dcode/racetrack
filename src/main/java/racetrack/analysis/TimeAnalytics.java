/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

/**
 * Analytics for time.
 */
public class TimeAnalytics {
  /**
   * For the specified entities, determine where they are active (timewise) within the visible set.  Then return all
   * the records from the root set if the record (1) matches the original entity AND (2) in an active period from the visible
   * set.  Active can be specified by the slop parameters... i.e., if the entity is active at t0... then acceptable
   * matches will to t0 +/- slop.
   *
   *@param entities entities to match on ... any part of the record can match...
   *@param visible  matching timeframes to use
   *@param all      all the records
   *@param slop     timeframe plus or minus threshold
   *
   *@return set of all records that match... should be a strict superset of visible
   */
  public static Set<Bundle> bundlesInTimeFrame(Set<String> entities, Bundles visible, Bundles all, long slop) {
    Map<String,List<long[]>> activity_timeframes = new HashMap<String,List<long[]>>();

    // Construct where those entities are active in the visible bundles
    Iterator<Tablet> it_tab = visible.tabletIterator(); while (it_tab.hasNext()) {

      // Determine if the tablet has timestamps... if not... then ignore
      Tablet tablet = it_tab.next(); if (tablet.hasTimeStamps()) {
        KeyMaker all_km = new KeyMaker(tablet, KeyMaker.ALL_ENTITIES_STR); // All KM -- turns a record into all of its fields
        
        // Go through each record
        Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
          // See if the record has the entity
          Bundle bundle = it_bun.next();
          String strs[] = all_km.stringKeys(bundle);
          for (int i=0;i<strs.length;i++) if (entities.contains(strs[i])) {

            // If so, add it to the activity timeline
            if (activity_timeframes.containsKey(strs[i]) == false) activity_timeframes.put(strs[i], new ArrayList<long[]>());

            long times[];
            if (bundle.hasDuration()) { times = new long[2]; times[0] = bundle.ts0(); times[1] = bundle.ts1(); } else { times = new long[1]; times[0] = bundle.ts0(); }

            activity_timeframes.get(strs[i]).add(times);
          }
        }
      }
    }

    // Construct a more searchable timeline for each entity
    Map<String,TimelineMatcher> timeline_matchers = new HashMap<String,TimelineMatcher>();
    Iterator<String> it = activity_timeframes.keySet().iterator(); while (it.hasNext()) {
      String entity = it.next();  timeline_matchers.put(entity, new TimelineMatcher(activity_timeframes.get(entity), slop));
    }

    // Go through the all record set and align the times up... keep records that contain the entity... and that match the timeframe
    Set<Bundle> matching = new HashSet<Bundle>(); it_tab = all.tabletIterator(); while (it_tab.hasNext()) {

      // Get the tablet and construct the all entities keymaker
      Tablet tablet = it_tab.next(); KeyMaker all_km = new KeyMaker(tablet, KeyMaker.ALL_ENTITIES_STR); 

      // Go through each record for this tablet...
      Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
        // See if the record has the entity
        Bundle bundle = it_bun.next();
        String strs[] = all_km.stringKeys(bundle);
        for (int i=0;i<strs.length;i++) if (entities.contains(strs[i])) {
          if        (bundle.hasDuration())  { // Check if the duration coincides with the activity timeline (for this entity)
            if (timeline_matchers.containsKey(strs[i]) && timeline_matchers.get(strs[i]).matches(bundle.ts0(), bundle.ts1())) matching.add(bundle);

          } else if (bundle.hasTime())      { // Check if the timestamp coincides with the activity timeline (for this entity)
            if (timeline_matchers.containsKey(strs[i]) && timeline_matchers.get(strs[i]).matches(bundle.ts0()))               matching.add(bundle);
            
          } else                            { // Just add it to the return set... time doesn't matter
            matching.add(bundle);
          }
        }
      }
    }

    return matching;
  }

  /**
   * Looks like this is the xth attempt at this idea... but maybe this one will stick.  From tablet 1/field 1, extract
   * all of the entities and their timeframes.  Then match entity exact against tablet 2/field 2 within a time interval.
   *
   * First pass... just look at time initial timestamp -- i.e., ignore durations
   *
   *@param tab1 tablet 1
   *@param fld1 field from tablet 1 to use
   *@param tab2 tablet 2
   *@param fld2 field from tablet 2 to use
   *@param slop how close they need to match in time in millis
   */
  public static Set<Bundle> overlapBetweenTabletsCheap(Tablet tab1, String fld1, Tablet tab2, String fld2, long slop) {
    Set<Bundle> matching_set = new HashSet<Bundle>();

    // Sanity check
    if (tab1.hasTimeStamps()                     == false) throw new RuntimeException("Tablet 1 does not have timestamps");
    if (tab2.hasTimeStamps()                     == false) throw new RuntimeException("Tablet 2 does not have timestamps");
    if (KeyMaker.tabletCompletesBlank(tab1,fld1) == false) throw new RuntimeException("Tablet 1 does not contain field \"" + fld1 + "\"");
    if (KeyMaker.tabletCompletesBlank(tab2,fld2) == false) throw new RuntimeException("Tablet 2 does not contain field \"" + fld2 + "\"");

    // Structure for the correlations
    Map<String,Map<Long,Set<Bundle>>> map = new HashMap<String,Map<Long,Set<Bundle>>>();

    // Map the first tablet's entities into a data structure
    KeyMaker km1 = new KeyMaker(tab1,fld1), km2 = new KeyMaker(tab2,fld2);
    Iterator<Bundle> it_bun = tab1.bundleIterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next(); String keys[] = km1.stringKeys(bundle); long ts0 = bundle.ts0();

      // Skip if empty or not set
      if (keys == null || keys.length == 0 || (keys.length == 1 && keys[0].equals(BundlesDT.NOTSET))) continue;

      // For each key/entity, put into a data structure
      for (int i=0;i<keys.length;i++) {
        String key = keys[i]; if (map.containsKey(key) == false) map.put(key,new HashMap<Long,Set<Bundle>>());
        if (map.get(key).containsKey(ts0) == false) map.get(key).put(ts0, new HashSet<Bundle>());
        map.get(key).get(ts0).add(bundle);
      }
    }

    // Sort the keys for searching
    Set<Long> times = new HashSet<Long>(); Iterator<String> it = map.keySet().iterator(); while (it.hasNext()) times.addAll(map.get(it.next()).keySet());
    List<Long> sorted = new ArrayList<Long>(); sorted.addAll(times); Collections.sort(sorted);

    // Compare the second tablet's entities
    it_bun = tab2.bundleIterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next(); String keys[] = km2.stringKeys(bundle); long ts0 = bundle.ts0();

      // Skip if empty or not set
      if (keys == null || keys.length == 0 || (keys.length == 1 && keys[0].equals(BundlesDT.NOTSET))) continue;

      // For each key/entity, put into a data structure
      for (int i=0;i<keys.length;i++) {
        String key = keys[i]; if (map.containsKey(key)) {
          // Find closest timestamp
          int index = Collections.binarySearch(sorted, ts0); int offset;
          // Search both up and down from that timestamp
          offset = 0;  while ((index+offset) >= 0 && (index+offset) < sorted.size() && Math.abs(sorted.get(index+offset) - ts0) <= slop) { 
            matching_set.add(bundle); matching_set.addAll(map.get(key).get(sorted.get(index+offset))); offset++; }

          offset = -1; while ((index+offset) >= 0 && (index+offset) < sorted.size() && Math.abs(sorted.get(index+offset) - ts0) <= slop) { 
            matching_set.add(bundle); matching_set.addAll(map.get(key).get(sorted.get(index+offset))); offset--; }
        }
      }
    }

    return matching_set;
  }

  /**
   * Test method for the timeline matcher...
   */
  public static void main(String args[]) {
    List<long[]> list = new ArrayList<long[]>(); long arr[];

    arr = new long[2]; arr[0] = 10L; arr[1] = 12L; list.add(arr);
    arr = new long[1]; arr[0] = 20L;               list.add(arr);
    arr = new long[1]; arr[0] = 21L;               list.add(arr);
    arr = new long[2]; arr[0] = 25L; arr[1] = 35L; list.add(arr);
    arr = new long[1]; arr[0] = 36L;               list.add(arr);
    arr = new long[1]; arr[0] = 40L;               list.add(arr);
    arr = new long[1]; arr[0] = 50L;               list.add(arr);
    arr = new long[1]; arr[0] = 60L;               list.add(arr);
    arr = new long[2]; arr[0] = 62L; arr[1] = 70L; list.add(arr);

    TimelineMatcher tlm = new TimelineMatcher(list, 1L);

    System.err.println("Merged List");
    System.err.println(tlm.toString());
    System.err.println("\n");
    System.err.println("###\tts\tts+2\tts+10");
    System.err.println(); for (int i=10;i<80;i+=2) System.err.println("" + i + "\t" + tlm.matches(i) + "\t" + tlm.matches(i,i+2) + "\t" + tlm.matches(i,i+10));
  }
}

/**
 * Timeline matcher -- constructs a timeline of either single events or durational events.  Then applies
 * that timeline to match other timestamps (or durations).
 */
class TimelineMatcher implements Comparator<long[]> {
  /**
   * Merged together time segments...
   */
  List<long[]> merged = new ArrayList<long[]>();

  /**
   * Constructor ... sort the time segments and merge them together...
   */
  public  TimelineMatcher(List<long[]> timeframes, long slop) {
    // First step is to make everything into a duration ... and add the slop into the mix
    for (int i=0;i<timeframes.size();i++) {
      long l[] = timeframes.get(i), l2[] = new long[2];
      if (l.length == 1) { l2[0] = l[0] - slop; l2[1] = l[0] + slop; } else { l2[0] = l[0] - slop; l2[1] = l[1] + slop; }
      merged.add(l2);
    }

    // Sort so that they are ordered...
    Collections.sort(merged, this);

    // Merge them together
    int i = 0;
    while (i < (merged.size()-1)) {
      if (merged.get(i)[1] > merged.get(i+1)[0]) { // To merge... overlap detected
        long merge[] = new long[2];
        merge[0] = merged.get(i)[0];
        if (merged.get(i)[1] > merged.get(i+1)[1]) merge[1] = merged.get(i)  [1];
        else                                   merge[1] = merged.get(i+1)[1];
        merged.remove(i); merged.remove(i); merged.add(i, merge);
      } else { // Just add... no overlap
        i++;
      }
    }
  }

  /**
   * Return the merged list as a string
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<merged.size();i++) sb.append(i + "\t\t" + merged.get(i)[0] + " - " + merged.get(i)[1] + "\n"); 
    return sb.toString();
  }

  /**
   * Is a timestamp within a time segment?
   */
  public  boolean matches (long ts0) {
    for (int i=0;i<merged.size();i++) {
      long l[] = merged.get(i);
      if (l[0] <= ts0 && l[1] >= ts0) return true;
    }
    return false;
  }

  /**
   * Does a timesegment overlap with a time segment?
   */
  public  boolean matches (long ts0, long ts1) {
    for (int i=0;i<merged.size();i++) {
      long l[] = merged.get(i);
      if (l[0] <= ts0 && l[1] >= ts0) return true;
      if (l[0] <= ts1 && l[1] >= ts1) return true;
      if (l[0] >= ts0 && l[1] <= ts1) return true;
    }
    return false;
  }

  /**
   * Comparator for long arrays...
   */
  public int compare(long a[], long b[]) {
    if      (a[0] < b[0]) return -1;
    else if (a[0] > b[0]) return  1;
    else if (a[1] < b[1]) return -1;
    else if (a[1] > b[1]) return  1;
    else                  return  0;
  }
}

