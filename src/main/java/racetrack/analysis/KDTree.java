/* 

Copyright 2022 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *  KDTree implementation to support HDBSCAN Algorithm.
 *
 *@param T lookup for the feature vector
 *@param F feature vector space key
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class KDTree<T,K> {
  /**
   * Ordered dimensions
   */
  K[] ks;

  /**
   * Root of the tree
   */
  Node root;

  /**
   * Maximum depth of the tree
   */
  int  max_depth = 0;

  /**
   * Node for tree
   */
  class Node { double location; Map<K,Double> fvec; Node left_child, right_child; }

  /**
   * Construct the KDTree.
   *
   *@param to_fvecs map to the feature vectors
   */
  public KDTree(Map<T,Map<K,Double>> to_fvecs) {
    // Determine all of the possible fvec keys ... dimensions
    Set<Map<K,Double>> fvecs = new HashSet<Map<K,Double>>(); Set<K> fvec_set = new HashSet<K>(); 
    Iterator<T> it_t = to_fvecs.keySet().iterator(); while (it_t.hasNext()) {
      T t = it_t.next(); Map<K,Double> fvec = to_fvecs.get(t); fvec_set.addAll(fvec.keySet()); fvecs.add(fvec);
    }

    // Order dimensions
    ks = (K[])new Object[fvec_set.size()]; Iterator<K> it_k = fvec_set.iterator(); for (int i=0;i<ks.length;i++) ks[i] = it_k.next();

    // Recurse and create the tree
    root = recursiveCreate(fvecs,0); 

    // Print out some stats
    /*
    System.err.println("KDTree Constructor");
    System.err.println("  Points    = " + to_fvecs.keySet().size());
    System.err.println("  Max Depth = " + max_depth);
    */
  }

  /**
   * Recursively create the tree from the feature vectors.
   *
   * From http://wikipedia.org/wiki/K-d_tree
   *
   *@param point_list points to handle within this (sub)tree
   *@param depth      current depth of the tree // root is depth == 0
   *
   *@return node created for the kd-tree
   */
  private Node recursiveCreate(Set<Map<K,Double>> point_list, int depth) {
    if (point_list.size() == 0) return null;

    // Select the axis based on depth so that axis cycles through all valid values
    K axis = ks[depth%ks.length];

    // Determine the median value
    double values[] = new double[point_list.size()]; int values_i = 0; Iterator<Map<K,Double>> it_fvec = point_list.iterator(); while (it_fvec.hasNext()) {
      Map<K,Double> fvec = it_fvec.next(); if (fvec.containsKey(axis)) values[values_i++] = fvec.get(axis); else values[values_i++] = 0.0;
    }
    double median = findMedian(values);

    // Split along the median
    Set<Map<K,Double>> left_point_list  = new HashSet<Map<K,Double>>(),
                       right_point_list = new HashSet<Map<K,Double>>();
    Map<K,Double> median_fvec = null;
    it_fvec = point_list.iterator(); while (it_fvec.hasNext()) {
      Map<K,Double> fvec = it_fvec.next(); double value = (fvec.containsKey(axis)) ? fvec.get(axis) : 0.0;
      if      (value == median && median_fvec == null)  median_fvec = fvec;
      else if (value <= median)                         left_point_list.add(fvec); 
      else                                              right_point_list.add(fvec);
    }

    // Keep track of the max depth
    if (max_depth < depth) max_depth = depth;

    // Create the node and construct subtree
    Node node = new Node();
    node.location    = median;
    node.fvec        = median_fvec;
    node.left_child  = recursiveCreate(left_point_list,  depth+1);
    node.right_child = recursiveCreate(right_point_list, depth+1);

    return node;
  }

  /**
   * Find the kth nearest neighbor to a general point (in the high dimensional space).
   *
   *@param point point to find the nearest neighbor for
   *@param k     kth nearest neighbor ... k == 0 would mean the nearest neighbor
   *
   *@return kth nearest neighbor for the specific point
   */
  public Map<K,Double> kthNearestNeighbor(Map<K,Double> point, int k) {
    double distances[] = new double[k+1]; for (int i=0;i<distances.length;i++) distances[i] = Double.POSITIVE_INFINITY;
    Object nodes[]     = new Object[k+1];
    kthNearestNeighbor(root, point, 0, distances, nodes);
    return findFurthest(distances, nodes);
  }

  /**
   * Modification of the nearestNeighbor implementation below.  Recursive implementation of the kth nearest neighbor.
   *
   *@param parent    current node
   *@param target    target point
   *@param depth     current depth of the recusion
   *@param distances currently found distances -- pairing is with the nodes array
   *@param nodes     currently found nodes -- pairing is with the distances array
   *
   */
  private void kthNearestNeighbor(Node parent, Map<K,Double> target, int depth, double distances[], Object nodes[]) {
    if (parent == null) return;

    // Get the dimension to be compared
    double value; K axis = ks[depth%ks.length]; if (target.containsKey(axis)) value = target.get(axis); else value = 0.0;

    // Determine the next branch to take
    Node next_branch, other_branch; next_branch = other_branch = null;
    if (value <= parent.location) { next_branch = parent.left_child;  other_branch = parent.right_child; 
    } else                        { next_branch = parent.right_child; other_branch = parent.left_child;   }

    // Keep going down
    kthNearestNeighbor(next_branch, target, depth+1, distances, nodes);
    replaceFurthest(parent, target, distances, nodes);

    // Check the other branch if necessary
    double radius_squared = distanceSquared(target, findFurthest(distances,nodes));
    double dist           = value - parent.location;
    if (radius_squared >= dist*dist) {
      kthNearestNeighbor(other_branch, target, depth+1, distances, nodes);
    }
  }

  /**
   * Replace the furthest node in the list if it is further from the node.  Replace null spots...
   * Helper method for the kthNearestNeighbor() subroutine.
   *
   *@param node    node to insert
   *@param target  target point
   *@param distances currently found distances -- pairing is with the nodes array
   *@param nodes     currently found nodes -- pairing is with the distances array
   */
  private void replaceFurthest(Node node, Map<K,Double> target, double distances[], Object nodes[]) {
    double furthest_d = -1.0; int furthest_i = 0;
    // See if there are any infinites
    for (int i=0;i<distances.length;i++) {
      if (Double.isInfinite(distances[i])) {
        distances[i] = distanceSquared(target,node.fvec);
        nodes[i]     = node;
        return;
      } else if (furthest_d < distances[i]) {
        furthest_d = distances[i];
        furthest_i = i;
      }
    }

    // Otherwise, use the furthest found
    double distance_squared = distanceSquared(target,node.fvec);
    if (distance_squared < furthest_d) {
      distances[furthest_i] = distance_squared;
      nodes[furthest_i]     = node;
    }
  }

  /**
   * Find the furthest in the list // that isn't null...
   * Helper method for the kthNearestNeighbor() subroutine.
   *
   *@param distances currently found distances -- pairing is with the nodes array
   *@param nodes     currently found nodes -- pairing is with the distances array
   *
   *@return furthest feature vector found
   */
  private Map<K,Double> findFurthest(double distances[], Object nodes[]) {
    if (Double.isInfinite(distances[0])) return null;

    double furthest_d = distances[0]; int furthest_i = 0;
    for (int i=1;i<distances.length;i++) {
      if (Double.isInfinite(distances[i]) == false && furthest_d < distances[i]) {
        furthest_d = distances[i];
        furthest_i = i;
      }
    }
    return ((Node) nodes[furthest_i]).fvec;
  }

  /**
   * Find the nearest neighbor to a general point (in the high dimensional space).
   *
   *@param point target point
   *
   *@return nearest neighbor to that point
   */
  public Map<K,Double> findNearestNeighbor(Map<K,Double> point) {
    Node nearest = nearestNeighbor(root, point, 0);
    return nearest.fvec;
  }

  /**
   * Recursive implementation for finding the nearest neighbor.
   *
   * Source:  https://www.youtube.com/watch?v=Glp7THUpGow
   *
   *@param parent current node under examination
   *@param target target point
   *@param depth  current depth within the tree -- used to calculate the axis
   *
   *@return node that is the nearest neighbor
   */
  private Node nearestNeighbor(Node parent, Map<K,Double> target, int depth) {
    if (parent == null) return null;

    // Get the dimension to be compared
    double value; 
    K axis = ks[depth%ks.length]; if (target.containsKey(axis)) value = target.get(axis); else value = 0.0;

    // Figure out the next branch to take
    Node   next_branch  = null;
    Node   other_branch = null;
    if (value <= parent.location) { next_branch = parent.left_child;  other_branch = parent.right_child;
    } else                        { next_branch = parent.right_child; other_branch = parent.left_child; }

    // Compare the current versus the best on the next branch
    Node temp = nearestNeighbor(next_branch, target, depth + 1);
    Node best = closest(target, temp, parent);

    // check the other branch (if necessary)
    double radius_squared = distanceSquared(target, best.fvec);
    double dist           = value - parent.location;
    if (radius_squared >= dist*dist) {
      temp = nearestNeighbor(other_branch, target, depth + 1);
      best = closest(target, temp, best);
    }
    
    return best;
  }

  /**
   * Return the closer of the two nodes to the target
   *
   *@param target target point
   *@param n0     node 0
   *@param n1     node 1
   *
   *@return closer of the two nodes the specified target point
   */
  private Node closest(Map<K,Double> target, Node n0, Node n1) { 
    if      (n0 == null && n1 == null) return null;
    else if (n0 == null)               return n1;
    else if (              n1 == null) return n0;
    else if (distanceSquared(target,n0.fvec) < distanceSquared(target,n1.fvec)) return n0; 
    else                                                                        return n1; 
  }

  /**
   * Return the distance squared between the two feature vectors.  Uses the dimension list from the constructor...
   *
   *@param v0 feature vector 0
   *@param v1 feature vector 1
   *
   *@return distance squared between the two feature vectors
   */
  private double distanceSquared(Map<K,Double> v0, Map<K,Double> v1) {
    double sum_squared = 0.0; for (int i=0;i<ks.length;i++) {
      double v0_d = (v0.containsKey(ks[i]) ? v0.get(ks[i]) : 0.0),
             v1_d = (v1.containsKey(ks[i]) ? v1.get(ks[i]) : 0.0);
      sum_squared += (v0_d - v1_d) * (v0_d - v1_d);
    }
    return sum_squared;
  }

  /**
   * Find the median -- based on the quick select algorithm described at the following wikipedia page
   * http://wikipedia.org/wiki/Quickselect
   *
   *@param values array of double values // will be modified by this routine ... i.e., partially sorted
   *
   *@return median value from the array
   */
  public static double findMedian(double values[]) { 
    int median_index = values.length/2;
    return qs_select(values, 0, values.length, median_index);
  } 
  // http://wikipedia.org/wiki/Quickselect
  public static int    qs_partition(double list[], int left, int right, int pivot_index) {
    double pivot_value = list[pivot_index];
    swap(list,pivot_index,right-1); // move pivot to end
    int store_index = left; for (int i=left;i<right-1;i++) { if (list[i] < pivot_value) { swap(list,store_index,i); store_index++; } }
    swap(list,right-1,store_index); // move pivot to its final place
    return store_index;
  }
  // http://wikipedia.org/wiki/Quickselect
  // Returns the k-th smallest element of list within left..right inclusive (i.e., left <= k <= right)
  public static double qs_select(double list[], int left, int right, int k) {
    if (left == right) { return list[left]; } // If the list only contains one element, return that element
    int pivot_index = left + ((int) ((Math.random() * 3 * (right - left)))%(right - left));
    pivot_index = qs_partition(list, left, right, pivot_index);
    // The pivot is in its final sorted position
    if      (k == pivot_index) return list[k];
    else if (k <  pivot_index) return qs_select(list, left,          pivot_index, k);
    else                       return qs_select(list, pivot_index+1, right,       k);
  }
  // http://wikipedia.org/wiki/Quickselect
  private static void swap(double list[], int i, int j) { double d_i = list[i], d_j = list[j]; list[i] = d_j; list[j] = d_i; }

  /**
   * Run some tests...
   */
  public static void main(String args[]) {
    // Median tests
    int success = 0, fail = 0; for (int t=0;t<100;t++) {
      // Randomized lengths (after the first 10 or so)
      int len; if (t < 10) len = t+1;  else len = (int) (1 + Math.random() * 100);
      // Randomize the arrays
      double array[] = new double[len]; for (int j=0;j<array.length;j++) array[j] = Math.random() * 100.0 - 50.0;
      // Quick find median
      double median = findMedian(array); 
      // Check by completely sorting
      Arrays.sort(array);
      if (median == array[array.length/2]) success++; else fail++;
    }
    System.err.println("Median Tests: " + success + " / " + (success + fail));

    // Nearest neighbor for 2d
    success = 0; fail = 0; for (int t=0;t<100;t++) { 

      // Randomize the number of points (after the first 10 or so)
      int points;
      if (t < 10) points = t+1;
      else        points = (int) (1 + Math.random()*100); 

      // Create random points in 2d space
      Map<Integer,Map<String,Double>> to_fvecs = new HashMap<Integer,Map<String,Double>>(); for (int i=0;i<points;i++) {
        double x = Math.random() * 100.0 - 50.0, y = Math.random() * 100.0 - 50.0;  
        Map<String,Double> fvec = new HashMap<String,Double>(); fvec.put("x", x); fvec.put("y", y);
        to_fvecs.put(i, fvec);
      }

      // Construct the KD Tree for 2D
      KDTree<Integer,String> kdtree_2d = new KDTree<Integer,String>(to_fvecs);

      // General nearest points
      for (int i=0;i<points;i++) {
        double x = Math.random() * 100.0 - 50.0, y = Math.random() * 100.0 - 50.0;  
        Map<String,Double> fvec  = new HashMap<String,Double>(); fvec.put("x", x); fvec.put("y", y);
        Map<String,Double> found = kdtree_2d.findNearestNeighbor(fvec);

        Map<String,Double> closest = null; double closest_d2 = Double.POSITIVE_INFINITY;
        Iterator<Integer> it_i = to_fvecs.keySet().iterator(); while (it_i.hasNext()) {
          Map<String,Double> to_check = to_fvecs.get(it_i.next());
          double d2 = (x - to_check.get("x"))*(x - to_check.get("x")) + (y - to_check.get("y"))*(y - to_check.get("y"));
          if (d2 < closest_d2) { closest = to_check; closest_d2 = d2; }
        }

        if (found.get("x") == closest.get("x") && found.get("y") == closest.get("y")) success++; else fail++;
      }

      // kth nearest points
      if (points > 10) {
        for (int i=0;i<points;i++) {
         for (int kth=1;kth<6;kth++) {
          double x = Math.random() * 100.0 - 50.0, y = Math.random() * 100.0 - 50.0;  

          Map<String,Double> fvec  = new HashMap<String,Double>(); fvec.put("x", x); fvec.put("y", y);
          Map<String,Double> found = kdtree_2d.kthNearestNeighbor(fvec,kth);
          double distances[] = new double[kth+1]; Object fvecs[] = new Object[kth+1];

          Iterator<Integer> it_i = to_fvecs.keySet().iterator(); while (it_i.hasNext()) {
            Map<String,Double> to_check = to_fvecs.get(it_i.next());
            double d2 = (x - to_check.get("x"))*(x - to_check.get("x")) + (y - to_check.get("y"))*(y - to_check.get("y"));

            // Replace the first null... or the furthest found so far...
            int furthest_i = 0; double furthest_d = 0.0; if (fvecs[0] != null) furthest_d = distances[0];
            for (int j=0;j<distances.length;j++) {
              if        (fvecs[j] == null)          { furthest_i = j; break;
              } else if (distances[j] > furthest_d) { furthest_i = j; furthest_d = distances[j]; }
            }

            if      (fvecs[furthest_i] == null) { distances[furthest_i] = d2; fvecs[furthest_i] = to_check; }
            else if (d2 < furthest_d)           { distances[furthest_i] = d2; fvecs[furthest_i] = to_check; }
          }

          // Verify the found vector
          int furthest_i = 0; double furthest_d = distances[0];
          for (int j=0;j<distances.length;j++) { if (distances[j] > furthest_d) { furthest_i = j; furthest_d = distances[j]; } }

          if (((Map<String,Double>) fvecs[furthest_i]) == found) success++; else fail++;
         }
        }
      }
    }
    System.err.println("Nearest Tests: " + success + " / " + (success + fail));
  }
}

