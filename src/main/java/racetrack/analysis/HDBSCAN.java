/* 

Copyright 2022 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import racetrack.util.IntCountSorterD;
import racetrack.util.Utils;

import racetrack.visualization.AbridgedSpectra;
import racetrack.visualization.RTColorManager;

// Imports for the Hierarchy Explorer GUI
import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Stroke;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JComponent;
import javax.swing.JFrame;

/**
 * HDBSCAN Implementation... suboptimal :(
 *
 *@param T lookup for the feature vector
 *@param F feature vector space key
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class HDBSCAN<T,K> {
  // Booking for the T's to their integer equivalents
  Map<T,Integer> t_to_i = new HashMap<T,Integer>();
  Map<Integer,T> i_to_t = new HashMap<Integer,T>();

  // List of all K's seen...
  K ks[];

  // Minimum spanning tree
  List<EdgeWeight<T>> minimum_spanning_tree;

  // Edge w/ Weight
  class EdgeWeight<T> implements Comparable<EdgeWeight> { 
    T q, r; 
    double w; 
    public int compareTo(EdgeWeight o) {
      if      (w < o.w)               return -1;
      else if (w > o.w)               return  1;
      else                            return  0;
    }
  }

  // kth nearest neighbor -- used for mutual reachability distance calc // HDBSCAN Parameter
  int kth;

  // Map to the kth nearest neighbor for each T
  Map<T,Double> kth_nearest = new HashMap<T,Double>();

  // Minimum cluster size // HDBSCAN Parameter
  int min_size;

  // Original input
  Map<T,Map<K,Double>> to_fvecs;

  // Cluster hierarchy root node
  HNode cluster_hierarchy_root;

  // Final results
  Set<Set<T>> final_clusters;

  /**
   * Return the final clusters.
   *
   *@return final clusters organized into sets of sets
   */
  Set<Set<T>> finalClusters() { return final_clusters; }

  /**
   * Simplified static version that creates the clusters and returns the final version.
   *
   *@param to_fvecs    elements to their feature vectors
   *@param kth         mutual reachability setting
   *@param min_size    minimum cluster size
   *
   *@return hdbscan-based clustering results as a set of sets of the original elements
   */
  public static <T,K> Set<Set<T>> cluster(Map<T,Map<K,Double>> to_fvecs, int kth, int min_size) {
    HDBSCAN hdbscan = new HDBSCAN(to_fvecs, kth, min_size);
    return hdbscan.finalClusters();
  }

  /**
   * Perform the HDBSCAN Algorithm.
   * ... mostly from the following web page:
   * ... https://hdbscan.readthedocs.io/en/latest/how_hdbscan_works.html
   * ... primary mod was the use of Prim's EMST vs Dual Tree Boruvka EMST
   *
   *@param to_fvecs map to the feature vectors
   *@param kth      kth nearest neighbor to use for the mutual reachability distance
   *@param min_size minimum cluster size
   */
  public HDBSCAN(Map<T,Map<K,Double>> to_fvecs, int kth, int min_size) {
    this.kth = kth; this.min_size = min_size; this.to_fvecs = to_fvecs;
    // Figure out the dimensions ... used for distance calculation
    // ... assign integers to the keySet() values -- i.e., T's to i and vice versa
    Set<K> k_set = new HashSet<K>(); Iterator<T> it_t =  to_fvecs.keySet().iterator(); while (it_t.hasNext()) {
      T t = it_t.next(); Map<K,Double> fvec = to_fvecs.get(t); int t_i = t_to_i.keySet().size(); t_to_i.put(t, t_i); i_to_t.put(t_i, t);
      k_set.addAll(fvec.keySet());
    }
    ks = (K[]) new Object[k_set.size()]; Iterator<K> it_k = k_set.iterator(); for (int i=0;i<ks.length;i++) ks[i] = it_k.next();

    // Need the kth nearest neighbor for each element -- for the mutual reachability calculation
    long t0_kdtree = System.currentTimeMillis();
    KDTree<T,K> kdtree = new KDTree<T,K>(to_fvecs);
    it_t = to_fvecs.keySet().iterator(); while (it_t.hasNext()) {
      T t = it_t.next(); kth_nearest.put(t, distanceSquared(to_fvecs.get(t), kdtree.kthNearestNeighbor(to_fvecs.get(t), kth)));
    }
    long t1_kdtree = System.currentTimeMillis(); System.err.println("HDBSCAN (Kth Nearest Neighbor) Time: " + (t1_kdtree - t0_kdtree) + " ms");

    // Figure out the minimum spanning tree ... uses a slightly modified Prim's algorithm... 
    // ... for more scalability, should use Dual Tree Boruvka's...
    long t0_emst = System.currentTimeMillis();
    PriorityQueue<EdgeWeight<T>> emst_edges = new PriorityQueue<EdgeWeight<T>>();
    minimum_spanning_tree = calculatePrimsEMST(emst_edges);
    long t1_emst = System.currentTimeMillis(); System.err.println("HDBSCAN (Prims EMST) Time: " + (t1_emst - t0_emst) + " ms");

    // Build the cluster hierarchy
    long t0_cluster_hierarchy = System.currentTimeMillis();
    cluster_hierarchy_root = buildClusterHierarchy(emst_edges);
    long t1_cluster_hierarchy = System.currentTimeMillis(); System.err.println("HDBSCAN (Cluster Hierarchy) Time: " + (t1_cluster_hierarchy - t0_cluster_hierarchy) + " ms");

    // Condense the cluster hierarchy
    long t0_condense = System.currentTimeMillis();
    Map<Integer,Double> cluster_w_average = new HashMap<Integer,Double>();
    Map<Integer,Double> cluster_stability = condenseClusterHierarchy(cluster_hierarchy_root, min_size, cluster_w_average);
    long t1_condense = System.currentTimeMillis(); System.err.println("HDBSCAN (Condense Cluster Hierarchy) Time: " + (t1_condense - t0_condense) + " ms");

    // Extract out the clusters
    long t0_extract = System.currentTimeMillis();
    final_clusters = extractClusters(cluster_hierarchy_root, cluster_stability, cluster_w_average);
    long t1_extract = System.currentTimeMillis(); System.err.println("HDBSSCAN (Extract Clusters) Time: " + (t1_extract - t0_extract) + " ms");

    if (xy()) writeEMSTImage                 (cluster_hierarchy_root, "hdbscan_emst.png", false);         // For xy fvecs, write out an image // DEBUG
    if (xy()) writeClusterHierarchyImage     (cluster_hierarchy_root, "hdbscan_cluster_hierarchy.png");   // For xy fvecs, write out an image // DEBUG
    if (xy()) writeClusterSmallMultiplesImage(cluster_hierarchy_root, 
                                              cluster_w_average,
                                              cluster_stability,      "hdbscan_cluster_smalls.png");      // For xy fvecs, write out an image // DEBUG
    if (xy()) writeEMSTImage                 (cluster_hierarchy_root, "hdbscan_emst_final.png", true);    // For xy fvecs, write out an image // DEBUG
    if (xy()) new HierarchyExplorer<T,K>(cluster_hierarchy_root, to_fvecs,cluster_w_average,cluster_stability);
  }

  // Calcuate the distance squared between two feature vectors
  private double distanceSquared(Map<K,Double> v0, Map<K,Double> v1) {
    double sum_sq = 0.0;
    for (int i=0;i<ks.length;i++) {
      if      (v0.containsKey(ks[i]) && v1.containsKey(ks[i])) { double diff = v0.get(ks[i]) - v1.get(ks[i]); sum_sq += diff*diff; }
      else if (v0.containsKey(ks[i]))                          { double diff = v0.get(ks[i]);                 sum_sq += diff*diff; }
      else if (                         v1.containsKey(ks[i])) { double diff =                 v1.get(ks[i]); sum_sq += diff*diff; }
    }
    return sum_sq;
  }

  // Calculate the mutual reachability between two points...
  private double mutualReachability(T p0, T p1) {
    double d_sq = distanceSquared(to_fvecs.get(p0), to_fvecs.get(p1)),
           d_p0 = kth_nearest.get(p0),
           d_p1 = kth_nearest.get(p1);
    if      (d_sq >= d_p0 && d_sq >= d_p1) return d_sq;
    else if (d_p0 >= d_sq && d_p0 >= d_p1) return d_p0;
    else                                   return d_p1;
  }

  /**
   * Calculate the Euclidean Minimum Spanning Tree (EMST) using Prim's Algorithm.
   */
  private List<EdgeWeight<T>> calculatePrimsEMST(PriorityQueue<EdgeWeight<T>> emst_edges) {
    List<EdgeWeight<T>> mst = new ArrayList<EdgeWeight<T>>(); Set<T> done = new HashSet<T>(); Set<T> left = new HashSet<T>(); left.addAll(to_fvecs.keySet());
    PriorityQueue<EdgeWeight<T>> queue = new PriorityQueue<EdgeWeight<T>>();

    // Get the start node and add those edges to the priority queue
    Iterator<T> it_t = to_fvecs.keySet().iterator(); T seed = it_t.next(); done.add(seed); left.remove(seed);
    while (it_t.hasNext()) {
      T to = it_t.next(); EdgeWeight<T> ew = new EdgeWeight<T>(); ew.q = seed; ew.r = to; ew.w = mutualReachability(ew.q, ew.r);
      queue.add(ew);
    }

    // Heart of Prim's Algorithm
    while (done.size() != to_fvecs.keySet().size()) {
      // Get the lowest weight edge in the priority queue and make sure it's not to a point already added
      EdgeWeight<T> ew = queue.poll(); if (done.contains(ew.r) == false) {
        // Add the edge to the minimum spanning tree... and bookkeep on the done and left sets
        mst.add(ew); done.add(ew.r); left.remove(ew.r);
        // Add to the emst priority queue
        emst_edges.add(ew);
        // Add the new edges to the priority queue
        it_t = left.iterator(); while (it_t.hasNext()) {
          T to = it_t.next(); EdgeWeight<T> new_ew = new EdgeWeight<T>(); new_ew.q = ew.r; new_ew.r = to; new_ew.w = mutualReachability(new_ew.q, new_ew.r);
          queue.add(new_ew);
        }
      }
    }

    return mst;
  }

  /**
   * Build the cluster hierarchy from the edges in the minimum spanning tree.
   */
  private HNode buildClusterHierarchy(PriorityQueue<EdgeWeight<T>> emst_edges) { 
    HNode root_node = null; Map<T,HNode> node_lu = new HashMap<T,HNode>();
    while (emst_edges.size() > 0) {
      EdgeWeight<T> ew = emst_edges.poll(); HNode child_q, child_r;
      if (node_lu.containsKey(ew.q) == false) { node_lu.put(ew.q, child_q = new HNode(ew.q)); }
      else                                    {                   child_q = findRoot(node_lu.get(ew.q)); }
      if (node_lu.containsKey(ew.r) == false) { node_lu.put(ew.r, child_r = new HNode(ew.r)); }
      else                                    {                   child_r = findRoot(node_lu.get(ew.r)); }
      HNode new_node = new HNode(child_q, child_r); new_node.join_weight = ew.w;
      child_q.parent = child_r.parent = new_node;
      root_node = new_node; // Eventually this will be true?
    }
    return root_node;
  }

  // Hierarchical node // approximates the union find data structure
  class HNode {
    public HNode(T leaf_t)                     { this.leaf_t = leaf_t; }
    public HNode(HNode child_l, HNode child_r) { this.child_l = child_l; this.child_r = child_r; }

    // Added Members to facilitate the algorithm
    int          child_leaves       = -1;   // number of leaves that are children // condensing step
    int          cluster_id         = -1;   // cluster identity                   // condensing step
    HNode        split_node         = null; // node where this split happened     // condensing step & extraction step
    int          depth              = -1;   // depth of tree                      // condensing step & extraction step
    Set<Integer> possible_clusters  = null; // possible cluster ids for this node // condensing step & extraction step
    HNode        calc_root          = null; // Used for the union find            // condensing step
    int          final_cluster_id   = -1;   // Final cluster id assignment        // extraction step // only for visualizing & debugging
    double       join_weight        = 0.0;  // Weight that was used to join       // condensing step & extraction step

    // Base Members
    T     leaf_t       = null; // If this is set, then the node is a leaf // no children
    HNode child_l      = null, 
          child_r      = null;
    HNode parent       = null; // Should be null for root
  }

  // Find the root of the tree ... and update the best calc_root...
  private HNode findRoot(HNode hnode) { if      (hnode.parent    == null) {                                              return hnode;           }
                                        else if (hnode.calc_root != null) { hnode.calc_root = findRoot(hnode.calc_root); return hnode.calc_root; }
                                        else                              { hnode.calc_root = findRoot(hnode.parent);    return hnode.calc_root; } }

  /**
   * Condense the cluster hierarchy based on the minimum cluster size.
   */
  private Map<Integer,Double> condenseClusterHierarchy(HNode root, int min_size, Map<Integer,Double> cluster_w_average) {
    calculateChildLeavesCount(root, 0);
    Map<Integer,HNode>       cluster_map       = new HashMap<Integer,HNode>();  cluster_map.put(0,root);  // Only used to make unique id's
    Set<Integer>             possible_clusters = new HashSet<Integer>();        possible_clusters.add(0); // Used to track all the possible clusters a point may be in
    Map<Integer,Double>      cluster_w_sum     = new HashMap<Integer,Double>();                           // Sum of all the weights for each possible cluster id
    Map<Integer,Set<HNode>>  cluster_nodes     = new HashMap<Integer,Set<HNode>>();                       // All the nodes that belong to this cluster id

    labelClusterIDs(root, root, min_size, 0, cluster_map, possible_clusters, cluster_w_sum, cluster_nodes);

    // Calculate the stability ... first the average weight within each cluster id
    Map<Integer,Double> cluster_stability = new HashMap<Integer,Double>();
    Iterator<Integer> it = cluster_w_sum.keySet().iterator(); while (it.hasNext()) {
      int id = it.next(); cluster_w_average.put(id, cluster_w_sum.get(id) / cluster_nodes.get(id).size());
      cluster_stability.put(id, 0.0);
    }

    // Next the delta between the average and the joining weights ... then RMS that ... which is the standard deviation...
    clusterStability(root, cluster_stability, cluster_w_average);
    it = cluster_stability.keySet().iterator(); while (it.hasNext()) {
      int id = it.next(); int n = cluster_nodes.get(id).size(); double sum = cluster_stability.get(id);
      cluster_stability.put(id, Math.sqrt(sum/n));
    }

    return cluster_stability;
  }

  // Cluster stability
  private void clusterStability(HNode node, Map<Integer,Double> cluster_stability, Map<Integer,Double> cluster_w_average) {
    if (node.leaf_t == null) {
      // All possible clusters this could be part of
      Iterator<Integer> it = node.possible_clusters.iterator(); while (it.hasNext()) {
        int id = it.next(); double w = node.join_weight; double diff = w - cluster_w_average.get(id); 
        double sum = cluster_stability.get(id); sum += diff*diff; cluster_stability.put(id, sum);
      }
      
      // Descend in both trees
      clusterStability(node.child_l, cluster_stability, cluster_w_average);
      clusterStability(node.child_r, cluster_stability, cluster_w_average);
    }
  }

  // Calculate the number of child leaves
  private int calculateChildLeavesCount(HNode node, int depth) {
    node.depth = depth;
    if (node.leaf_t != null) { 
      node.child_leaves = 1;
    } else {
      node.child_leaves = calculateChildLeavesCount(node.child_l, depth+1) + calculateChildLeavesCount(node.child_r, depth+1);
    }
    return node.child_leaves;
  }

  // Label cluster identities based on the minimum cluster size ... assumes child_leaves is already calculated
  private void labelClusterIDs(HNode node, HNode split_node, int min_size, int cluster_id, Map<Integer,HNode> cluster_map, Set<Integer> possible_clusters, Map<Integer,Double> cluster_w_sum, Map<Integer,Set<HNode>> cluster_nodes) {
    // Bookkeeping for this node
    node.cluster_id = cluster_id; node.split_node = split_node; node.possible_clusters = possible_clusters;

    // Make sure it's not a leaf... and determine if it splits or not
    if (node.leaf_t == null) {
      // Add the weights to the cluster_w_sum parts... and track the nodes
      Iterator<Integer> it = possible_clusters.iterator(); while (it.hasNext()) {
        int id = it.next(); if (cluster_w_sum.containsKey(id) == false) { cluster_w_sum.put(id, 0.0); cluster_nodes.put(id, new HashSet<HNode>()); }
        cluster_w_sum.put(id, cluster_w_sum.get(id) + node.join_weight); cluster_nodes.get(id).add(node);
      }

      // Cluster split
      if (node.child_l.child_leaves >= min_size && node.child_r.child_leaves >= min_size) {
        int l_id = cluster_map.keySet().size(); cluster_map.put(l_id, node.child_l);
        int r_id = cluster_map.keySet().size(); cluster_map.put(r_id, node.child_r);

        Set<Integer> possible_clusters_l = new HashSet<Integer>(), possible_clusters_r = new HashSet<Integer>();
        possible_clusters_l.addAll(possible_clusters); possible_clusters_l.add(l_id);
        possible_clusters_r.addAll(possible_clusters); possible_clusters_r.add(r_id);

        labelClusterIDs(node.child_l, node.child_l, min_size, l_id, cluster_map, possible_clusters_l, cluster_w_sum, cluster_nodes);
        labelClusterIDs(node.child_r, node.child_r, min_size, r_id, cluster_map, possible_clusters_r, cluster_w_sum, cluster_nodes);

      // Just points falling off
      } else {
        labelClusterIDs(node.child_l, split_node, min_size, cluster_id, cluster_map, possible_clusters, cluster_w_sum, cluster_nodes);
        labelClusterIDs(node.child_r, split_node, min_size, cluster_id, cluster_map, possible_clusters, cluster_w_sum, cluster_nodes);
      }
    }
  }

  /**
   * Extract Clusters
   */
  private Set<Set<T>> extractClusters(HNode root,Map<Integer,Double> cluster_stability, Map<Integer,Double> cluster_w_average) {
    // Select the clusters per the cluster stability approach described in the algorithm
    // ... Set all leaf cluster id's as selected
    Set<Integer> selected = new HashSet<Integer>();
    selectAllLeaves(root, selected);

    // ... then iterate through the trees & merge clusters as needed
    Map<Integer,Integer> parent_lu = new HashMap<Integer,Integer>();  // mapping to show how clusters were merged
    selectClusters(root, cluster_stability, selected, parent_lu, cluster_w_average);

    // Separate the clusters by their original T value
    Map<Integer,Set<T>> to_clusters = new HashMap<Integer,Set<T>>();
    Iterator<Integer> it = selected.iterator(); while (it.hasNext()) to_clusters.put(it.next(), new HashSet<T>());
    toClusters(root, to_clusters, parent_lu);

    // Flatten & return
    Set<Set<T>> clusters = new HashSet<Set<T>>(); it = selected.iterator(); while (it.hasNext()) {
      clusters.add(to_clusters.get(it.next()));
    }

    return clusters;
  }

  // Select just the leaves
  private void selectAllLeaves(HNode node, Set<Integer> selected) {
    if (node.leaf_t == null) {
      selectAllLeaves(node.child_l, selected);
      selectAllLeaves(node.child_r, selected);
    } else selected.add(node.cluster_id);
  }

  // Reverse topological sort to calculate cluster stability
  private void selectClusters(HNode node, Map<Integer,Double> cluster_stability, Set<Integer> selected, Map<Integer,Integer> parent_lu, Map<Integer,Double> cluster_w_average) {
    // For intermediate nodes...
    if (node.leaf_t == null) {
      selectClusters(node.child_l, cluster_stability, selected, parent_lu, cluster_w_average);
      selectClusters(node.child_r, cluster_stability, selected, parent_lu, cluster_w_average);

      // If we are at the split, compare stability of children with this nodes stability ... determine if a merge is necessary or not
      if (node.child_l.cluster_id != node.cluster_id) {

        double this_stability  = cluster_stability.get(node.cluster_id),         // stability appears to just be standard deviation
               left_stability  = cluster_stability.get(node.child_l.cluster_id),
               right_stability = cluster_stability.get(node.child_r.cluster_id),

               this_avg_w      = cluster_w_average.get(node.cluster_id),         // average weight joins within the cluster
               left_avg_w      = cluster_w_average.get(node.child_l.cluster_id),
               right_avg_w     = cluster_w_average.get(node.child_r.cluster_id),

               this_join_w     = node.join_weight;

        boolean merge = false; // abc

        // if ((this_stability) < (left_stability + right_stability)) merge = true; // 2022-03-27 0946 ... sortof works... but not really
        // if ((this_stability) < (left_stability + right_stability)) merge = true; // 2022-03-27 0946 ... sortof works... but not really

        // 2022-04-10 ... if the two sides have about the same density... and the join weight is within some standard deviations of the two... then join
        if (left_avg_w  >= right_avg_w - 3.0*right_stability &&
            left_avg_w  <= right_avg_w + 3.0*right_stability &&
            this_join_w >= right_avg_w - 4.0*right_stability &&
            this_join_w <= right_avg_w + 4.0*right_stability) merge = true;

        if (right_avg_w >= left_avg_w  - 3.0*left_stability &&
            right_avg_w <= left_avg_w  + 3.0*left_stability &&
            this_join_w >= left_avg_w  - 4.0*left_stability &&
            this_join_w <= left_avg_w  + 4.0*left_stability) merge = true;

        if (merge) {
          // Merge

          selected.remove(node.child_l.cluster_id);
          selected.remove(node.child_r.cluster_id);
          selected.add(node.cluster_id);
          // cluster_stability.put(node.cluster_id, left_stability + right_stability);

          parent_lu.put(node.child_l.cluster_id, node.cluster_id);
          parent_lu.put(node.child_r.cluster_id, node.cluster_id);
        } else {
          // Leave Separate
        }
      }
    }
  }

  // Separate the leaves into their assigned clusters
  private void toClusters(HNode node, Map<Integer,Set<T>> to_clusters, Map<Integer,Integer> parent_lu) {
    if (node.leaf_t == null) {

      // Not really necessary ... just for the visualization
      int assign_to = node.cluster_id; while (parent_lu.containsKey(assign_to)) assign_to = parent_lu.get(assign_to);
      node.final_cluster_id = assign_to;

      // Recursive tree walk
      toClusters(node.child_l, to_clusters, parent_lu);
      toClusters(node.child_r, to_clusters, parent_lu);

    } else {

      // Determine how this cluster id was merged & assign the application reference to the right cluster
      int assign_to = node.cluster_id; while (parent_lu.containsKey(assign_to)) assign_to = parent_lu.get(assign_to);
      to_clusters.get(assign_to).add(node.leaf_t);
      node.final_cluster_id = assign_to;
    }
  }

  /**
   * Debugging code
   */
  // Determines if the features are only xy coordinates
  private boolean xy() {
    return (ks.length == 2) &&
           (
             (ks[0] instanceof String && ((String) ks[0]).equals("x") && ks[1] instanceof String && ((String) ks[1]).equals("y")) ||
             (ks[1] instanceof String && ((String) ks[1]).equals("x") && ks[0] instanceof String && ((String) ks[0]).equals("y"))
           );
  }

  /**
   * Debugging code
   */
  // Writes out an image showing the euclidean mst
  private void writeEMSTImage(HNode root, String filename, boolean use_final) {
    BufferedImage bi = new BufferedImage(1024,1024,BufferedImage.TYPE_INT_RGB); Graphics2D g2d = (Graphics2D) bi.getGraphics();
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    // Use the cluster id as the color
    Map<T,Color> t_to_color = new HashMap<T,Color>(); assignLeafColors(root, t_to_color, use_final);

    // Draw the edges
    for (int i=0;i<minimum_spanning_tree.size();i++) {
      EdgeWeight ew = minimum_spanning_tree.get(i);
      double x0 = to_fvecs.get(ew.q).get("x"), y0 = to_fvecs.get(ew.q).get("y"); int x0_i = (int) (bi.getWidth()*x0), y0_i = (int) (bi.getHeight()*y0);
      double x1 = to_fvecs.get(ew.r).get("x"), y1 = to_fvecs.get(ew.r).get("y"); int x1_i = (int) (bi.getWidth()*x1), y1_i = (int) (bi.getHeight()*y1);
      g2d.setColor(Color.darkGray); g2d.drawLine(x0_i,y0_i,x1_i,y1_i);
    }

    // Draw the data points
    Iterator<T> it_t = to_fvecs.keySet().iterator(); while (it_t.hasNext()) {
      T t = it_t.next(); Map<K,Double> fvec = to_fvecs.get(t);
      double x = fvec.get("x"), y = fvec.get("y"); int x_i = (int) (bi.getWidth()*x), y_i = (int) (bi.getHeight()*y);
      g2d.setColor(t_to_color.get(t)); g2d.fillRect(x_i,y_i,3,3);
    }

    g2d.dispose(); 

    try { ImageIO.write(bi, "PNG", new File(filename)); } catch (IOException ioe) { }
  }

  // Assign colors to the leaves based on their cluster_id
  private void assignLeafColors(HNode node, Map<T,Color> t_to_color, boolean use_final) {
    if (node.leaf_t == null) {
      assignLeafColors(node.child_l, t_to_color, use_final);
      assignLeafColors(node.child_r, t_to_color, use_final);
    } else if (use_final) {
      t_to_color.put(node.leaf_t, RTColorManager.getColor("xyz" + node.final_cluster_id));
    } else {
      t_to_color.put(node.leaf_t, RTColorManager.getColor("xyz" + node.cluster_id));
    }
  }

  // Writes out an image showing the cluster hierarchy
  private void writeClusterHierarchyImage(HNode root, String filename) {
    // Get the leaves in order...  a dendrogram...
    List<HNode> leaves = new ArrayList<HNode>(); Map<HNode,Integer> node_depth = new HashMap<HNode,Integer>();
    int height = leafList(root, leaves, 0, node_depth);

    // Figure out the node to x values...
    Map<HNode,Double> node_to_x = new HashMap<HNode,Double>();
    for (int i=0;i<leaves.size();i++) node_to_x.put(leaves.get(i), ((double) i)/(leaves.size()-1));
    fillNodeToX(root, node_to_x);

    BufferedImage bi = new BufferedImage(1900, height*9+18, BufferedImage.TYPE_INT_RGB); Graphics2D g2d = (Graphics2D) bi.getGraphics();
    renderDendrogram(g2d, bi, root, node_depth, node_to_x);    

    g2d.dispose();

    try { ImageIO.write(bi, "PNG", new File(filename)); } catch (IOException ioe) { }
  }

  // Get the ordered leaves ... for a dendrogram ... figure out the tree height
  private int leafList(HNode node, List<HNode> leaves, int depth, Map<HNode,Integer> node_depth) { 
    node_depth.put(node, depth);
    if (node.leaf_t != null) { 
      leaves.add(node); 
      return depth;
    } else { 
      int depth_l = leafList(node.child_l, leaves, depth+1, node_depth); 
      int depth_r = leafList(node.child_r, leaves, depth+1, node_depth); 
      if (depth_l > depth_r) return depth_l; else return depth_r;
    }
  }

  // Fill in the node_to_x map by walking the tree
  private double fillNodeToX(HNode node, Map<HNode,Double> node_to_x) {
    if (node_to_x.containsKey(node) == false) {
      double min_x = fillNodeToX(node.child_l, node_to_x),
             max_x = fillNodeToX(node.child_r, node_to_x);
      node_to_x.put(node, (min_x + max_x)/2);
    }
    return node_to_x.get(node);
  }

  // Calculate a nodes location
  private Point2D nodePoint(BufferedImage bi, HNode node, Map<HNode,Integer> node_depth, Map<HNode,Double> node_to_x) {
    return new Point2D.Double(16 + (bi.getWidth() - 32)*node_to_x.get(node), 4 + 9*node_depth.get(node));
  }

  // Render the dendrogram
  private void renderDendrogram(Graphics2D g2d, BufferedImage bi, HNode node, Map<HNode,Integer> node_depth, Map<HNode,Double> node_to_x) {

    Point2D pt = nodePoint(bi,node,node_depth,node_to_x);

    if (node.parent != null) {
      Point2D parent_pt = nodePoint(bi,node.parent,node_depth,node_to_x);
      g2d.setColor(Color.darkGray);
      g2d.draw(new Line2D.Double(pt.getX(),       pt.getY(),                          pt.getX(),       (parent_pt.getY() + pt.getY())/2.0));
      g2d.draw(new Line2D.Double(pt.getX(),       (parent_pt.getY() + pt.getY())/2.0, parent_pt.getX(),(parent_pt.getY() + pt.getY())/2.0));
      g2d.draw(new Line2D.Double(parent_pt.getX(),parent_pt.getY(),                   parent_pt.getX(),(parent_pt.getY() + pt.getY())/2.0));
    }

    if (node.child_l != null) renderDendrogram(g2d, bi, node.child_l, node_depth, node_to_x);
    if (node.child_r != null) renderDendrogram(g2d, bi, node.child_r, node_depth, node_to_x);

    g2d.setColor(RTColorManager.getColor("xyz"+node.cluster_id));
    g2d.fillRect(((int) pt.getX())-2, ((int) pt.getY())-2, 5, 5);
  }

  final static int sm_sz = 128;

  // Iterator by a score
  private Iterator<Integer> iteratorByScore(Map<Integer,Double> scores) {
    List<IntCountSorterD> sorter = new ArrayList<IntCountSorterD>(); Iterator<Integer> it = scores.keySet().iterator();
    while (it.hasNext()) { int i = it.next(); double d = scores.get(i); sorter.add(new IntCountSorterD(i, d)); }
    Collections.sort(sorter);
    List<Integer> int_list = new ArrayList<Integer>(); for (int i=0;i<sorter.size();i++) int_list.add(sorter.get(i).toInt());
    return int_list.iterator();
  }

  // Write out a small multiples image showing all the possible cluster id groupings
  private void writeClusterSmallMultiplesImage(HNode root, Map<Integer,Double> cluster_w_average, Map<Integer,Double> cluster_stability, String filename) {
    Map<Integer,BufferedImage> cluster_id_to_image = new HashMap<Integer,BufferedImage>();
    addToSmallMultiples(root,cluster_id_to_image);
    int edge = (int) (Math.sqrt(cluster_id_to_image.keySet().size()) + 1);

    Iterator<Integer> it = null;

    for (int i=0;i<3;i++) {
      String pre = "notset";
      if      (i == 0) { it = cluster_id_to_image.keySet().iterator(); pre = "id";   }
      else if (i == 1) { it = iteratorByScore(cluster_w_average);      pre = "wavg"; }
      else if (i == 2) { it = iteratorByScore(cluster_stability);      pre = "stab"; }

      BufferedImage all = new BufferedImage(edge*(sm_sz+2), edge*(sm_sz+2), BufferedImage.TYPE_INT_RGB); int tile_x = 0, tile_y = 0;
      Graphics2D g2d = (Graphics2D) all.getGraphics(); g2d.setColor(Color.darkGray); g2d.fillRect(0,0,all.getWidth(),all.getHeight());
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      while (it.hasNext()) {
        int cluster_id = it.next(); BufferedImage sm = cluster_id_to_image.get(cluster_id);
        int sx = tile_x*(sm_sz+2), sy = tile_y*(sm_sz+2);
        g2d.drawImage(sm, sx, sy, null); g2d.setColor(Color.white); g2d.drawString(""+cluster_id, sx+5, sy+sm.getHeight()-5);
        tile_x++; if (tile_x >= edge) { tile_y++; tile_x = 0; }
      }
      g2d.dispose();

      try { ImageIO.write(all, "PNG", new File(pre + "_" + filename)); } catch (IOException ioe) { }
    }

  }

  // Walk the tree... adding each node to the appropriate small multiples image
  private void addToSmallMultiples(HNode node, Map<Integer,BufferedImage> map) {
    if (node.leaf_t == null) { addToSmallMultiples(node.child_l,map); addToSmallMultiples(node.child_r,map); } else {
      int sx = (int) (to_fvecs.get(node.leaf_t).get("x")*(sm_sz-1)),
          sy = (int) (to_fvecs.get(node.leaf_t).get("y")*(sm_sz-1));
      Iterator<Integer> it = node.possible_clusters.iterator(); while (it.hasNext()) {
        int cluster_id = it.next();
        if (map.containsKey(cluster_id) == false) map.put(cluster_id, new BufferedImage(sm_sz, sm_sz, BufferedImage.TYPE_INT_RGB));
        map.get(cluster_id).setRGB(sx,sy,RTColorManager.getColor("xyz"+cluster_id).getRGB());
      }
    }
  }

  /**
   * Run some tests...
   */
  public static void main(String args[]) {
    try {
      // Create a 2d dataset from a base image with a variable number of points
      BufferedImage bi = ImageIO.read(new File(args[0]));
      int n = 4000; double noise = 0.001; // number of points
      System.err.print("Creating " + n + " Points From Image File \"" + args[0] + "\" ... ");
      Map<Integer,Map<String,Double>> to_fvecs = new HashMap<Integer,Map<String,Double>>();
      while (to_fvecs.keySet().size() < n) {
        double x    = Math.random(), 
               y    = Math.random();
        int    x_i  = (int) (bi.getWidth()  * x),
               y_i  = (int) (bi.getHeight() * y);
        int    gray = bi.getRGB(x_i,y_i) & 0x00ff;

        boolean point = false;
        if      (gray == 0)                                         point = true;  // high density areas
        else if (gray == 255 && Math.random() < noise)              point = true;  // noise
        else if (gray != 0   && Math.random() < (1.0 - gray/255.0)) point = true;  // variable density areas

        if (point) {
          Map<String,Double> fvec = new HashMap<String,Double>(); fvec.put("x", x); fvec.put("y", y);
          to_fvecs.put(to_fvecs.keySet().size(), fvec);
        } 
      }
      System.err.println("  Done!");

      // Run the algorithm
      HDBSCAN hdbscan = new HDBSCAN(to_fvecs, 5, 5);

    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }
}


/** 
 * GUI to explore the cluster hierarchy...
 * ... coded to work with just "x,y" feature vectors :(
 */
class HierarchyExplorer<T,K> extends JFrame {
  /**
   * Root of the tree
   */
  HDBSCAN.HNode root, 

  /**
   * Current focus node
   */
                focus;

  // Copies from the clustering mechanics
  Map<T,Map<K,Double>> to_fvecs;
  Map<Integer,Double>  cluster_w_average;
  Map<Integer,Double>  cluster_stability;

  /**
   * Construct the gui
   */
  public HierarchyExplorer(HDBSCAN.HNode root, Map<T,Map<K,Double>> to_fvecs, Map<Integer,Double> cluster_w_average, Map<Integer,Double> cluster_stability) {
    this.focus = this.root = root; this.to_fvecs = to_fvecs;
    this.cluster_w_average = cluster_w_average;
    this.cluster_stability = cluster_stability;

    getContentPane().add("Center", new HEComponent());

    setSize(1024,1024); setVisible(true);
  }

  /**
   *
   */
  class HEComponent extends JComponent implements KeyListener, MouseListener, MouseMotionListener {
    public HEComponent() { addKeyListener(this); addMouseListener(this); addMouseMotionListener(this); }

    // Mouse Listener... just to grab focus
    public void mouseEntered(MouseEvent me) { grabFocus(); }
    public void mouseExited(MouseEvent me) { }
    public void mousePressed(MouseEvent me) { }
    public void mouseReleased(MouseEvent me) { }
    public void mouseClicked(MouseEvent me) { }

    // Mouse motion listener ... apparently, to do more than just grabbing focus...
    public void mouseDragged(MouseEvent me) { }
    public void mouseMoved(MouseEvent me) { 
      BufferedImage bi = new BufferedImage(10,10,BufferedImage.TYPE_INT_RGB); Graphics2D g2d = (Graphics2D) bi.getGraphics(); // for font metrics...
      TV found_tv = null;

      Map<String,List<TV>> base_tv_copy = base_tv; if (base_tv_copy != null) {
        Iterator<String> it = base_tv_copy.keySet().iterator(); while (it.hasNext()) {
          String t = it.next(); Iterator<TV> it_tv = base_tv_copy.get(t).iterator(); while (it_tv.hasNext()) {
            TV tv = it_tv.next(); String s = tv.t + ":" + tv.v; int txt_w = Utils.txtW(g2d,s), txt_h = Utils.txtH(g2d,s);
            int x0 = tv.x - txt_w/2, x1 = tv.x + txt_w/2, y0 = tv.y - txt_h, y1 = tv.y;
            if ((me.getX() > x0 && me.getX() < x1) && (me.getY() > y0 && me.getY() < y1)) found_tv = tv;
          }
        }
      }

      if (found_tv != in_tv) { in_tv = found_tv; repaint(); }

      g2d.dispose();
    }

    // Key Listener
    public void keyTyped    (KeyEvent ke) { }
    public void keyReleased (KeyEvent ke) { }
    public void keyPressed  (KeyEvent ke) { if      (ke.getKeyCode() == KeyEvent.VK_UP    && focus.parent  != null) { focus = focus.parent;  base_bi = null; repaint(); }
                                            else if (ke.getKeyCode() == KeyEvent.VK_LEFT  && focus.child_l != null) { focus = focus.child_l; base_bi = null; repaint(); }
                                            else if (ke.getKeyCode() == KeyEvent.VK_RIGHT && focus.child_r != null) { focus = focus.child_r; base_bi = null; repaint(); } }

    // base image and base type-values
    BufferedImage        base_bi = null;
    Map<String,List<TV>> base_tv = null;
    TV                   in_tv   = null;

    // Paint the component
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g; g.setColor(Color.black); int w = getWidth(), h = getHeight(); g.fillRect(0,0,w,h);
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // Base Image
      if (base_bi == null) {
        Map<String,List<TV>> tvs = new HashMap<String,List<TV>>();
        base_bi = renderBase(tvs);
        base_tv = tvs;
      }
      g2d.drawImage(base_bi, 0, 0, null);

      // Type Value Interaction
      Map<String,List<TV>> base_tv_copy = base_tv; TV in_tv_copy = in_tv;
      if (base_tv_copy != null && in_tv_copy != null) {
        double v0 = in_tv_copy.v, v1 = in_tv_copy.v;

        // Mins and maxes
        Iterator<TV> it = base_tv_copy.get(in_tv_copy.t).iterator(); while (it.hasNext()) {
          TV tv = it.next(); if (v0 > tv.v) v0 = tv.v; if (v1 < tv.v) v1 = tv.v;
        }
        if (v0 == v1) { v0 -= 0.1; v1 += 0.1; } // in case it's the same value...

        // Draw the axis
        g2d.setColor(Color.white); g2d.drawLine(10, h - 20, w - 10, h - 20);

        // Draw the pointers
        AbridgedSpectra cs = new AbridgedSpectra();
        it = base_tv_copy.get(in_tv_copy.t).iterator(); while (it.hasNext()) {
          TV tv = it.next();
          double ratio = (tv.v - v0)/(v1-v0);
          int x = (int) (10 + (w-20) * ratio);
          g2d.setColor(cs.at((float) ratio));
          g2d.drawLine(tv.x, tv.y, x, h - 20);
        }
      }
    }

    // Render the base image
    private BufferedImage renderBase(Map<String,List<TV>> tvs) {
      int w = getWidth(), h = getHeight();
      BufferedImage bi = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB); Graphics2D g2d = (Graphics2D) bi.getGraphics();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // Place the focal node
      int focus_x = w/2, focus_y = h/3, x_spc = w/4, y_spc = h/4;

      // Start with the linkages
      g2d.setColor(Color.lightGray); Stroke orig_stroke = g2d.getStroke(); g2d.setStroke(new BasicStroke(2.0f));
      int p_x  = focus_x,                    p_y  = focus_y - y_spc,
          po_x = focus_x,                    po_y = focus_y,             // parent other child

          l_x  = focus_x - x_spc,            l_y  = focus_y + y_spc,
          ll_x = focus_x - x_spc - x_spc/2,  ll_y = focus_y + 2*y_spc,
          lr_x = focus_x - x_spc + x_spc/2,  lr_y = focus_y + 2*y_spc,

          r_x  = focus_x + x_spc,            r_y  = focus_y + y_spc,
          rl_x = focus_x + x_spc - x_spc/2,  rl_y = focus_y + 2*y_spc,
          rr_x = focus_x + x_spc + x_spc/2,  rr_y = focus_y + 2*y_spc;

      if (focus.parent != null) {
        if (focus.parent.child_l == focus) { p_x = focus_x + x_spc/2; po_x = focus_x + x_spc; } 
        else                               { p_x = focus_x - x_spc/2; po_x = focus_x - x_spc; }
        g2d.drawLine(p_x,p_y,focus_x,focus_y);
        g2d.drawLine(p_x,p_y,po_x,   po_y);
        if (focus.parent.parent != null) g2d.drawLine(p_x,p_y,p_x,0);
      }
      if (focus.child_l != null) g2d.drawLine(l_x,l_y,focus_x,focus_y);
        if (focus.child_l != null && focus.child_l.child_l != null) g2d.drawLine(l_x,l_y,ll_x,ll_y);
        if (focus.child_l != null && focus.child_l.child_r != null) g2d.drawLine(l_x,l_y,lr_x,lr_y);

      if (focus.child_r != null) g2d.drawLine(r_x,r_y,focus_x,focus_y);
        if (focus.child_r != null && focus.child_r.child_l != null) g2d.drawLine(r_x,r_y,rl_x,rl_y);
        if (focus.child_r != null && focus.child_r.child_r != null) g2d.drawLine(r_x,r_y,rr_x,rr_y);

      // Render the nodes
      renderNodes(g2d, focus, focus_x, focus_y, false, tvs);
      if (focus.parent  != null) {
        renderNodes(g2d, focus.parent,  p_x, p_y, false, tvs);
        if (focus.parent.child_l == focus) renderNodes(g2d, focus.parent.child_r, po_x, po_y, true, tvs);
        if (focus.parent.child_r == focus) renderNodes(g2d, focus.parent.child_l, po_x, po_y, true, tvs);
      }
      if (focus.child_l != null) renderNodes(g2d, focus.child_l, l_x, l_y, false, tvs);
        if (focus.child_l != null && focus.child_l.child_l != null) renderNodes(g2d, focus.child_l.child_l, ll_x,ll_y, true, tvs);
        if (focus.child_l != null && focus.child_l.child_r != null) renderNodes(g2d, focus.child_l.child_r, lr_x,lr_y, true, tvs);
      if (focus.child_r != null) renderNodes(g2d, focus.child_r, r_x, r_y, false, tvs);
        if (focus.child_r != null && focus.child_r.child_l != null) renderNodes(g2d, focus.child_r.child_l, rl_x,rl_y, true, tvs);
        if (focus.child_r != null && focus.child_r.child_r != null) renderNodes(g2d, focus.child_r.child_r, rr_x,rr_y, true, tvs);

      // Render the TVs
      Iterator<String> it = tvs.keySet().iterator(); while (it.hasNext()) {
        String t = it.next(); g2d.setColor(RTColorManager.getColor(t)); Iterator<TV> it_tv = tvs.get(t).iterator(); while (it_tv.hasNext()) {
          TV tv = it_tv.next(); String s = t + ":" + tv.v; g2d.drawString(s, tv.x - Utils.txtW(g2d,s)/2, tv.y);
        }
      }

      g2d.dispose(); return bi;
    }

    // Class to hold type-value string renders
    class TV { String t; double v; int x, y; }
    private void addTV(Map<String,List<TV>> tvs, String t, double v, int x, int y) {
      if (tvs.containsKey(t) == false) tvs.put(t, new ArrayList<TV>());
      TV tv = new TV(); tv.t = t; tv.v = v; tv.x = x; tv.y = y;
      tvs.get(t).add(tv);
    }

    // Render the nodes
    private void renderNodes(Graphics2D g2d, HDBSCAN.HNode node, int node_x, int node_y, boolean draw_children_edges, Map<String,List<TV>> tvs) {
      // Child edges
      if (draw_children_edges) {
        g2d.setColor(Color.lightGray); if (node.child_l != null) { g2d.drawLine(node_x, node_y, node_x - 50, node_y + 50); }
        g2d.setColor(Color.lightGray); if (node.child_r != null) { g2d.drawLine(node_x, node_y, node_x + 50, node_y + 50); }
      }

      // Points under this node
      BufferedImage bi = new BufferedImage(64, 64, BufferedImage.TYPE_INT_RGB); renderSmallMultiple(bi, node);
      g2d.setColor(RTColorManager.getColor("xyz" + node.final_cluster_id));
      g2d.fillRect(node_x - bi.getWidth()/2 - 2, node_y - bi.getHeight()/2 - 2, bi.getWidth() + 4, bi.getHeight() + 4);
      g2d.drawImage(bi, node_x - bi.getWidth()/2, node_y - bi.getHeight()/2, null);

      String str;
      str = ""+countLeaves(node.child_l); g2d.drawString(str, node_x - bi.getWidth()/2 - 4 - Utils.txtW(g2d,str), node_y + Utils.txtH(g2d,str));
      str = ""+countLeaves(node.child_r); g2d.drawString(str, node_x + bi.getWidth()/2 + 4,                       node_y + Utils.txtH(g2d,str));

      g2d.setColor(Color.white);
      int txt_y = node_y + bi.getHeight()/2 + 4 + Utils.txtH(g2d, "0");
      str = "cid:"  + node.cluster_id + "/" + node.final_cluster_id; g2d.drawString(str, node_x - Utils.txtW(g2d,str)/2, txt_y); txt_y += Utils.txtH(g2d,str);

      // Type values...
      if (node.join_weight != 0.0)                        { addTV(tvs, "jonw", node.join_weight,                       node_x, txt_y);   txt_y += Utils.txtH(g2d,str); }
      if (cluster_w_average.containsKey(node.cluster_id)) { addTV(tvs, "wavg", cluster_w_average.get(node.cluster_id), node_x, txt_y); } txt_y += Utils.txtH(g2d,str);
      if (cluster_stability.containsKey(node.cluster_id)) { addTV(tvs, "std",  cluster_stability.get(node.cluster_id), node_x, txt_y); } txt_y += Utils.txtH(g2d,str);
    }

    // Leaves
    private int countLeaves(HDBSCAN.HNode node) {
      if (node == null) return 0;
      if (node.leaf_t == null) { return countLeaves(node.child_l) + countLeaves(node.child_r); } else { return 1; }
    }

    // Recursive render for small multiple
    private void renderSmallMultiple(BufferedImage bi, HDBSCAN.HNode node) {
      if (node.leaf_t == null) { renderSmallMultiple(bi,node.child_l); renderSmallMultiple(bi,node.child_r); } else {
        int sx = 1 + (int) (to_fvecs.get(node.leaf_t).get("x")*(bi.getWidth() - 2)),
            sy = 1 + (int) (to_fvecs.get(node.leaf_t).get("y")*(bi.getHeight()- 2));
        bi.setRGB(sx,sy,RTColorManager.getColor("xyz"+node.final_cluster_id).getRGB());
      }
    }
  }
}

