/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.StringTokenizer;

import racetrack.util.Utils;

/**
 * Hierarchically Cluster Data
 *
 * @author  D. Trimm
 * @version 0.1
 */
public class HierarchicalClustering {
  /**
   *
   */
  public enum VectorDistance { euclidean };

  /**
   * Perform hierarchical clustering on the specified feature vectors with the specified vector_distance.
   *
   *@param feature_vectors feature vector lookups -- note that these arrays will be normalized
   *@param vector_distance vector distance to use
   */
  public HierarchicalClustering(Map<String,double[]> feature_vectors, VectorDistance vector_distance) {
    // Degenerate cases
    if (feature_vectors.keySet().size() == 0) { root = new TreeNode("empty set", null, null); return; }
    if (feature_vectors.keySet().size() == 1) { root = new TreeNode(feature_vectors.keySet().iterator().next(), null, null); return; }

    // Normalize the feature_vectors... find mins and maxes...
    String key        = feature_vectors.keySet().iterator().next();
    double  key_vec[]  = feature_vectors.get(key);
    double  mins[]     = new double[key_vec.length],
            maxs[]     = new double[key_vec.length];
    for (int i=0;i<key_vec.length;i++) mins[i] = maxs[i] = key_vec[i];
    Iterator<String> it_key = feature_vectors.keySet().iterator(); while (it_key.hasNext()) {
      key = it_key.next(); key_vec = feature_vectors.get(key);
      for (int i=0;i<key_vec.length;i++) {
        if (mins[i] > key_vec[i]) mins[i] = key_vec[i];
        if (maxs[i] < key_vec[i]) maxs[i] = key_vec[i];
      }
    }

    // Make sure the mins and maxes differ
    for (int i=0;i<mins.length;i++) { 
      // System.err.println("before : mins / maxs @ " + i + " = " + mins[i] + " / " + maxs[i]); // DEBUG
      if (Math.abs(mins[i] - maxs[i]) < 1.0) maxs[i] = mins[i] + 1.0f;
      // System.err.println("after  : mins / maxs @ " + i + " = " + mins[i] + " / " + maxs[i]); // DEBUG
    } 

    // Normalize the structure...
    it_key = feature_vectors.keySet().iterator(); while (it_key.hasNext()) {
      key = it_key.next(); key_vec = feature_vectors.get(key);
      // System.err.print("Before: "); for (int i=0;i<key_vec.length;i++) System.err.print(key_vec[i] + " "); System.err.println(); // DEBUG
      for (int i=0;i<key_vec.length;i++) { key_vec[i] = (key_vec[i] - mins[i]) / (maxs[i] - mins[i]); }
      // System.err.print("After:  "); for (int i=0;i<key_vec.length;i++) System.err.print(key_vec[i] + " "); System.err.println(); System.err.println(); // DEBUG
    }

    // DEBUG vvvv
/*
    Iterator<String> itfv = feature_vectors.keySet().iterator(); while (itfv.hasNext()) {
      String name = itfv.next();
      System.out.print("double[] vec_" + name + " = { ");
      double vec[] = feature_vectors.get(name);
      System.out.print(vec[0] + "f");
      for (int i=1;i<vec.length;i++) System.out.print(" , " + vec[i] + "f");
      System.out.print(" }; feature_vectors.put(\"" + name + "\", vec_" + name + ");");
      System.out.println();
    }
*/
    // DEBUG ^^^^

    // Create the distance metric method and the binary tree structure
    Distance              distance    = createDistanceMethod(vector_distance);
    Map<String,TreeNode> tree_node_lu = new HashMap<String,TreeNode>();

    // Determine the initial distance between all elements and place them into a priority queue
    List<String>                  ls             = new ArrayList<String>(); ls.addAll(feature_vectors.keySet()); // As a list ... just for the distance matrix creation
    PriorityQueue<ClusterElement> priority_queue = new PriorityQueue<ClusterElement>();    // Closest elements
    Map<String,double[]>          urlenc_map     = new HashMap<String,double[]>();         // With the elements url encoded (for using pipes)
    Set<String>                   leaves         = new HashSet<String>();                  // Remaining elements to be clustered
    for (int i=0;i<ls.size();i++) { 
      String encoded = Utils.encToURL(ls.get(i));
      leaves.add(encoded);
      tree_node_lu.put(encoded, new TreeNode(encoded,null,null));
      urlenc_map.put(Utils.encToURL(ls.get(i)), feature_vectors.get(ls.get(i)));
      for (int j=i+1;j<ls.size();j++) {
        double d = distance.d(feature_vectors.get(ls.get(i)),
                             feature_vectors.get(ls.get(j)));
        ClusterElement ce = new ClusterElement(Utils.encToURL(ls.get(i)), Utils.encToURL(ls.get(j)), d, true); // using pipes as separators for mergers
        priority_queue.add(ce);
    } }
    ls.clear(); // don't use again

    // Merge the two closest elements ... and the cluster back to the priority queue w/ all distances examined
    while (priority_queue.size() > 0) {
      ClusterElement head   = priority_queue.poll();
      // System.err.println("Priority Queue Head = " + head); // DEBUG
      if (leaves.contains(head.element_0) && leaves.contains(head.element_1)) { // if something was already merged, ignore...
        String         parent    = head.element_0 + "|" + head.element_1; // new parent
        // System.err.println("Merging...  Parent Name = \"" + parent + "\""); // DEBUG
        TreeNode       tree_node = new TreeNode(parent, tree_node_lu.get(head.element_0), tree_node_lu.get(head.element_1));
        tree_node_lu.put(parent, tree_node);
        root = tree_node; // last one assigned should actually be the root...  i think

        // Calculate the centroid for the parent
        StringTokenizer st = new StringTokenizer(parent, "|"); 
        double vec[]     = urlenc_map.get(st.nextToken());
        double new_vec[] = new double[vec.length];
        for (int i=0;i<vec.length;i++) new_vec[i] = vec[i];
        int   samples   = 1;
        while (st.hasMoreTokens()) {
          vec = urlenc_map.get(st.nextToken()); 
          for (int i=0;i<vec.length;i++) new_vec[i] += vec[i];
          samples++;
        }
        for (int i=0;i<new_vec.length;i++) new_vec[i] /= samples;
        urlenc_map.put(parent, new_vec);

        // Remove the two leaves that were merged together
        leaves.remove(head.element_0); leaves.remove(head.element_1);

        // Calculate all the distances to the leaves
        Iterator<String> it = leaves.iterator(); while (it.hasNext()) {
          String leaf = it.next();
          ClusterElement ce = new ClusterElement(parent, leaf, distance.d(new_vec,urlenc_map.get(leaf)), false);
          priority_queue.add(ce);
        }

        // Add the new node as a leaf
        leaves.add(parent);
      }
    }
  }

  /**
   * Root of the dendrogram
   */
  TreeNode root;

  /**
   *
   */
  public List<String> dendrogramOrder() {
    List<String> ls = new ArrayList<String>(); 
    traversal(root,ls);
    return ls;
  }

  /**
   * Recursive traversal - Left / Right / not Parent
   */
  protected void traversal(TreeNode node, List<String> ls) { 
    // System.err.println("traversal: node.name = \"" + node.name + "\" ... child0 = " + node.child0 + " | child1 = " + node.child1);
    if (node.isLeaf()) ls.add(Utils.decFmURL(node.name)); else { 
      traversal(node.child0,ls); 
      traversal(node.child1,ls); 
    } }

  /**
   *
   */
  class ClusterElement  implements Comparable<ClusterElement> {
    String element_0, element_1; double d; boolean leaf;
    public ClusterElement(String el0, String el1, double d0, boolean leaf0) {
      this.element_0 = el0; this.element_1 = el1; this.d = d0; this.leaf = leaf0;
    }
    public int compareTo(ClusterElement other) {
      if      (d < other.d) return -1;
      else if (d > other.d) return  1;
      else                  return  0;
    }
    public String toString() { return "ClusterElement [ " + element_0 + " , " + element_1 + " , " + d + " , leaf=" + leaf + "]"; }
  }

  /**
   * Push the shorter tree towards first index... probably don't really
   * need this if I were smarter about putting things together...
   */
  class TreeNode {
    String   name;
    TreeNode child0, child1;
    public TreeNode(String name0, TreeNode c0, TreeNode c1) {
      this.name = name0; if (c0 == null) return;
      int c0_pipes = countPipes(c0.name),
          c1_pipes = countPipes(c1.name);
      if (c0_pipes < c1_pipes) { child0 = c0; child1 = c1; }
      else                     { child0 = c1; child1 = c0; }
    }
    public int countPipes(String str) {
      int count = 0;
      for (int i=0;i<str.length();i++) if (str.charAt(i) == '|') count++;
      return count;
    }
    public boolean isLeaf() { return child0 == null; }
  }

  /**
   * Simple interface for distance metric
   */
  interface Distance { public double d(double v0[], double v1[]); }

  /**
   * Create the distance method.
   *
   *@param vd distance method description
   */
  protected Distance createDistanceMethod(VectorDistance vd) {
    switch (vd) {
      case euclidean:
      default:     return new EuclideanDistance();
    }
  }

  /**
   * Euclidean distance implementations
   */
  class EuclideanDistance implements Distance {
    public double d(double v0[], double v1[]) {
      double sum_sq = 0.0f;
      for (int i=0;i<v0.length;i++) sum_sq += ((v0[i] - v1[i])*(v0[i] - v1[i]));
      return (double) (Math.sqrt(sum_sq)/v0.length);
    }
  }

  /**
   * Run a test... run some performance numbers
   */
  public static void main(String args[]) {
    // Test method
    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();
    double[] veca0 = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("a0", veca0);
    double[] veca1 = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("a1", veca1);
    double[] veca2 = { 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("a2", veca2);

    double[] vecb0 = { 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("b0", vecb0);
    double[] vecb1 = { 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("b1", vecb1);
    double[] vecb2 = { 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f }; feature_vectors.put("b2", vecb2);

    double[] vecc0 = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f }; feature_vectors.put("c0", vecc0);
    double[] vecc1 = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f }; feature_vectors.put("c1", vecc1);

    HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
    List<String> ls = hc.dendrogramOrder();
    System.out.println("Order = " + ls);
/*
    // Scalability method ... looks reasonable upto 400 elements...
    System.out.println();
    System.out.println("ELEMENTS,VECLENGTH,TIMETAKEN");
    for (int elements=20;elements<3000;elements+=400) {
      for (int vec_length=10;vec_length<100;vec_length+=40) {
        feature_vectors = new HashMap<String,double[]>();
        for (int i=0;i<elements;i++) {
          String name  = "" + i;
          double  vec[] = new double[vec_length];
          for (int j=0;j<vec_length;j++) { vec[j] = (double) Math.random(); }
          feature_vectors.put(name, vec);
        }

        long t0 = System.currentTimeMillis();
        hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
        long t1 = System.currentTimeMillis();

        System.out.println("" + elements + "," + vec_length + "," + (t1 - t0));

      }
    }
*/
  }
}

