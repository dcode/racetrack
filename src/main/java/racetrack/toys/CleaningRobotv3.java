/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.toys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.*;

public class CleaningRobotv3 {
  int    THREADS      =  100;
  int    TESTS        =  3;
  int    MAX_GENS     =  4000;
  int    tiles_w      =  20,    // Width of the room in cells
         tiles_h      =  20,    // Height of the room in cells
         steps        =  800,   // Number of steps robot gets to clean ... 400 squares (20x20)... cleaning all would be 800 moves
         winners      =  20,    // Number of robots that win each generation // Zero just shows how well random works...
         pool         =  400,   // Number of robots that compete each generation
         pt_clean     =  16,    // Points robot gets for picking up trash
         pt_clean_err = -2,     // Points deducted if the robot tries to clean a spot with no trash
         pt_dirty     = -3,     // Points deducted at end of round for any remaining trash
         pt_wall      = -4;     // Points deducted if the robot hits a wall
  double trash_pr     =  0.50;  // Probability that trash is in a cell
  double mutate_pr    =  0.2;   // Mutation probability

  Robot top_scorer       =  null;
  int   top_scorer_score =  Integer.MIN_VALUE;

  Robot robots[]         =  new Robot[pool];

  Score scores[]         =  new Score[pool];
  int   scored           =  0; Object scored_mutex = new Object();

  // Work queue -- work that needs to get done...
  LinkedList<Runnable> work_queue = new LinkedList<Runnable>();

  // Worker thread to take work off of the work queue and run it... Just run forever
  class WorkerThread extends Thread {
    public void run() {
      while (true) {
        Runnable runnable = null;
        synchronized (work_queue) { if (work_queue.size() > 0) runnable = work_queue.remove(); }
        if (runnable != null) { runnable.run(); } else { try { Thread.sleep(1); } catch (InterruptedException ie) { } }
      }
    }
  }
  
  /**
   * Construct and run the simulation...
   */
  public CleaningRobotv3() throws IOException {
    PrintStream log = null;
    File file = new File("cleaning_robot_v3_log.csv");
    if (file.exists()) log = new PrintStream(new FileOutputStream(file, true));
    else               { log = new PrintStream(new FileOutputStream(file));
                         log.println("sim_step,TOPSCORE,GENERATION,TILES_W,TILES_H,STEPS,WINNERS,POOL,PT_CLEAN,PT_CLEAN_ERR,PT_DIRTY,PT_WALL,sim_desc"); }
    


    RobotRunnable robot_runnables[] = new RobotRunnable[robots.length];

    // Create initial pool of robots 
    for (int i=0;i<robots.length;i++) robots[i] = new Robot();

    // Robot runnables... these can be re-used over and over again without recreating...
    // -- they are paired by index with both the robots array and the scores array
    for (int i=0;i<robot_runnables.length;i++) robot_runnables[i] = new RobotRunnable(i);

    // Create the threads to pull the work queue items
    for (int i=0;i<THREADS;i++) (new WorkerThread()).start();

    // Forever loop
    int generation = 1;
    while (true) {
      // Create the work queue to run all of the existing robots ... wait until all scores are filled in
      synchronized (scored_mutex) { scored = 0; }
      synchronized (work_queue) { for (int i=0;i<robot_runnables.length;i++) { scores[i] = null; work_queue.add(robot_runnables[i]); } }
      while (scored != robots.length) { try { Thread.sleep(1); } catch (InterruptedException ie) { } }

      // Create the next generation of robots ... sort the scores... keep the top scores... transfer winners...  and create next generation
      Arrays.sort(scores); 
      if (scores[0].score > top_scorer_score) { top_scorer = scores[0].robot; top_scorer_score = scores[0].score; }

      if (winners > 0) {
        int i=0; 
        robots[i++] = top_scorer; // re-introduce the overall top scorer into every generation
        while (i < winners) { robots[i] = scores[i].robot; robots[i].wins++; i++; }   // Winners
        while (i < robots.length) { robots[i++] = new Robot(randomParent(), randomParent()); } // Next generation
      } else {
        int i=0; while (i < robots.length) { robots[i++] = new Robot(); }
      }

      // Print status update
      System.err.println(generation + " | " + top_scorer_score + " (id="    + top_scorer.id + 
                                                                 " | gen="  + top_scorer.generation +
                                                                 " | wins=" + top_scorer.wins + 
                                                                 " | p0= "  + top_scorer.parent0_id +
                                                                 " | p1= "  + top_scorer.parent1_id +
                                                                 ")");

      String desc = tiles_w + "-" + tiles_h + "-" +  steps + "-" + winners + "-" + pool + "-" +
                    pt_clean + "-" + pt_clean_err + "-" + pt_dirty + "-" + pt_wall + "-" + mutate_pr;
      log.println(generation + "," + top_scorer_score + "," + top_scorer.generation + "," +
                  tiles_w + "," + tiles_h + "," +  steps + "," + winners + "," + pool + "," +
                  pt_clean + "," + pt_clean_err + "," + pt_dirty + "," + pt_wall + "," + desc);

      generation++;
      if (generation > MAX_GENS) System.exit(0);
    }
  }

  /**
   * Pick a random robot from the winners...
   */
  public Robot randomParent() { if (winners == 0) return null; int i = ((int) (Integer.MAX_VALUE * Math.random()))%winners; return robots[i]; }

  /**
   * Thread to execute a run of robot simulations for a limited set of the total population.
   */
  class RobotRunnable implements Runnable {
    int robot_i; public RobotRunnable(int robot_i) { this.robot_i = robot_i; }
    public void run() {
      int best_score = Integer.MIN_VALUE; Room best_room = null;
      for (int t=0;t<TESTS;t++) {
        Room room = new Room();
        int score = room.simulate(robots[robot_i]);
        if (score > best_score) { best_score = score; best_room = room; }
      }
      scores[robot_i] = new Score(robots[robot_i], best_score, best_room);
      synchronized (scored_mutex) { scored++; }
    }
  }

  /**
   * Class to hold a score, the robot, and the room.  Sortable...
   */
  class Score implements Comparable<Score> {
    int score; Robot robot; Room room;
    public Score(Robot robot, int score, Room room) { this.robot = robot; this.score = score; this.room = room; }
    public int compareTo(Score other) {
      if      (other.score > score) return  1;
      else if (other.score < score) return -1;
      else if (other.robot.prog.keySet().size() > robot.prog.keySet().size()) return  1;
      else if (other.robot.prog.keySet().size() < robot.prog.keySet().size()) return -1;
      else return 0;
    }
    public String toString() { return "" + score; }
  }

  /**
   * Actions that the robot can take at each step.
   */

  enum Action { PICKUP_TRASH, MOVE_NE, MOVE_N, MOVE_NW, MOVE_W, MOVE_SW, MOVE_S, MOVE_SE, MOVE_E, MOVE_RANDOM };

  /**
   * State of a cell in the room.
   */
  enum CellState { WALL, TRASH, EMPTY };

  /**
   * Class representing the robot and its state machine for cleaning a room.
   */
  class Robot {
    int                wins       = 0;
    int                id         = 1000000 + ((int) (Math.random()*Integer.MAX_VALUE))%50000;
    int                generation = 0;
    Map<String,Action> prog = new HashMap<String,Action>();
    int                parent0_id = -1,
                       parent1_id = -1;

    /**
     * Save the robot to a file.
     */
    public void saveToFile(File file) throws IOException {
      PrintStream out = new PrintStream(new FileOutputStream(file)); out.println(id); 
      Iterator<String> it = prog.keySet().iterator(); while (it.hasNext()) { String key = it.next(); out.println(key + "," + prog.get(key)); }
      out.close();
    }

    /**
     * Create a blank robot.
     */
    public Robot() { }

    /**
     * Create a robot that's a blend of mommy and daddy.
     */
    public Robot(Robot mommy, Robot daddy) {
      parent0_id = mommy.id; parent1_id = daddy.id;

      if (mommy.generation > daddy.generation) generation = mommy.generation + 1;
      else                                     generation = daddy.generation + 1;

      // Mommy's contributions -- have to deconflict when they are in Daddy's genes as well
      Iterator<String> it = mommy.prog.keySet().iterator();
      while (it.hasNext()) {
        String encoded = it.next();
        if (daddy.prog.containsKey(encoded)) {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else {
            if (Math.random() < 0.5) prog.put(encoded, mommy.prog.get(encoded));
            else                     prog.put(encoded, daddy.prog.get(encoded));
          }
        } else {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else prog.put(encoded, mommy.prog.get(encoded));
        }
      }

      // Now Daddy's unique contributions
      it = daddy.prog.keySet().iterator();
      while (it.hasNext()) {
        String encoded = it.next();
        if (prog.containsKey(encoded) == false) {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else prog.put(encoded, daddy.prog.get(encoded));
        }
      }
    }

    /**
     * Create a random action for the robot.
     */
    public Action randomAction() {
        Action action = Action.PICKUP_TRASH; int index = ((int) (Math.random() * Integer.MAX_VALUE))%10;
        if      (index == 0)  action = Action.PICKUP_TRASH;
        else if (index == 1)  action = Action.MOVE_NE;
        else if (index == 2)  action = Action.MOVE_N;
        else if (index == 3)  action = Action.MOVE_NW;
        else if (index == 4)  action = Action.MOVE_W;
        else if (index == 5)  action = Action.MOVE_SW;
        else if (index == 6)  action = Action.MOVE_S;
        else if (index == 7)  action = Action.MOVE_SE;
        else if (index == 8)  action = Action.MOVE_E;
        else if (index == 9)  action = Action.MOVE_RANDOM;
        return action;
    }

    /**
     * For an environmental cell, determine the action of the robot.  This is a state machine--if the
     * environmental state is not present, then a random action will be chosen.
     */
    public Action determineAction(CellState state[][]) {
      String encoded = encode(state);
      if (prog.containsKey(encoded) == false) { prog.put(encoded, randomAction()); }
      return prog.get(encoded);
    }

    /**
     * Encode the cell state as a string --- makes for easier lookups into a hashmap.
     */
    private String encode(CellState state[][]) {
      StringBuffer sb = new StringBuffer();
      for (int i=0;i<state.length;i++) for (int j=0;j<state[i].length;j++) {
        if      (state[i][j] == CellState.WALL)  sb.append("W");
        else if (state[i][j] == CellState.TRASH) sb.append("T");
        else if (state[i][j] == CellState.EMPTY) sb.append("E");
      }
      return sb.toString();
    }
  }

  /**
   * Class representing room to clean.
   */
  class Room {
    int     score     = 0;
    boolean trash[][] = new boolean[tiles_h][tiles_w]; int robot_x, robot_y;

    /**
     * Create a random room.
     */
    public Room() {
      // Randomly put trash down
      for (int y=0;y<trash.length;y++) for (int x=0;x<trash[y].length;x++) {
        if (Math.random() <= trash_pr) trash[y][x] = true; else trash[y][x] = false; 
      }

      // Randomly place the robot
      robot_x = ((int) (Math.random() * Integer.MAX_VALUE))%tiles_w;
      robot_y = ((int) (Math.random() * Integer.MAX_VALUE))%tiles_h;
    }

    /**
     * Simulate the robot cleaning this room.
     */
    public int simulate(Robot robot) {
      // Go through the number of steps for the simulation
      for (int i=0;i<steps;i++) {
        // Get the robots action based on its position and the room
        CellState state[][] = new CellState[3][3];
        for (int dy=-1;dy<=1;dy++) for (int dx=-1;dx<=1;dx++) {
          int x = robot_x + dx, y = robot_y + dy;
          if      (x < 0 || x >= trash[0].length || y < 0 || y >= trash.length) state[dy+1][dx+1] = CellState.WALL;
          else if (trash[y][x])                                                 state[dy+1][dx+1] = CellState.TRASH;
          else                                                                  state[dy+1][dx+1] = CellState.EMPTY;
        }
        Action action = robot.determineAction(state);
        histories.add(new History(robot_x, robot_y, action, trash, score));

        // Execute action
        switch (action) {
          case PICKUP_TRASH: if (trash[robot_y][robot_x]) { score += pt_clean; } else { score += pt_clean_err; }
                             trash[robot_y][robot_x] = false; break;
          case MOVE_NE: move(+1,+1); break;
          case MOVE_N:  move( 0,+1); break;
          case MOVE_NW: move(-1,+1); break;
          case MOVE_W:  move(-1, 0); break;
          case MOVE_SW: move(-1,-1); break;
          case MOVE_S:  move( 0,-1); break;
          case MOVE_SE: move(+1,-1); break;
          case MOVE_E:  move(+1, 0); break;
          case MOVE_RANDOM:          int dx = 0, dy = 0;
                                     while (Math.abs(dx) + Math.abs(dy) == 0.0) { // Has to move some direction
                                       double r = Math.random(); if (r < 0.333) dx = -1; else if (r < 0.666) dx = 0; else dx = 1;
                                              r = Math.random(); if (r < 0.333) dy = -1; else if (r < 0.666) dy = 0; else dy = 1;
                                     }
                                     move(dx,dy);
                                     break;
        }
      }
      // Total up the remaining trash
      for (int y=0;y<trash.length;y++) for (int x=0;x<trash[y].length;x++) if (trash[y][x]) score += pt_dirty;
      histories.add(new History(robot_x, robot_y, null, trash, score));

      // Return the score
      return score;
    }

    /**
     * Move the robot.  Penalize the robot for going off the screen.
     */
    private void move(int dx, int dy) {
      int x = robot_x + dx,
          y = robot_y + dy;
      if (x < 0 || x >= trash[0].length) { score += pt_wall; } else robot_x = x;
      if (y < 0 || y >= trash.length)    { score += pt_wall; } else robot_y = y;
    }

    /**
     * List of histories describing robot cleaning the room.
     */
    List<History> histories = new ArrayList<History>();

    /**
     * Create an image that describes the robot's cleaning steps for this room.
     */
    public BufferedImage historyTrace() {
      int sw = 20, sh = 20;
      BufferedImage bi  = new BufferedImage(trash[0].length*sw + 2, trash.length*sh + 2, BufferedImage.TYPE_INT_RGB); if (histories.size() == 0) return bi;
      Graphics2D    g2d = null;
      try {
        g2d = (Graphics2D) bi.getGraphics(); g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.white); g2d.fillRect(0, 0, bi.getWidth(), bi.getHeight());

        History history = histories.get(0), final_history = histories.get(histories.size() - 1);
        for (int y=0;y<trash.length;y++) {
          for (int x=0;x<trash[y].length;x++) {
            int x0 = x*sw, y0 = y*sh;
            g2d.setColor(Color.lightGray); g2d.drawRect(x0, y0, sw, sh);
            if (history.state[y][x]) { 
              g2d.setColor(Color.green); g2d.fillRect(x0 + 2, y0 + 2, sw - 4, sh - 4); 
              if (final_history.state[y][x] == false) {
                g2d.setColor(Color.white); g2d.fillRect(x0 + 4, y0 + 4, sw - 8, sh - 8); 
              }
            }
          }
        }

        for (int i=1;i<histories.size();i++) {
          history = histories.get(i); History last_history = histories.get(i-1);
          int rx0 = last_history.x * sw + sw/2,
              ry0 = last_history.y * sh + sh/2,
              rx1 =      history.x * sw + sw/2,
              ry1 =      history.y * sh + sh/2;
          if (history.score < last_history.score) { g2d.setColor(Color.red); g2d.fillOval(rx1-2,ry1-2,5,5); }
          double dx  = rx1 - rx0, dy  = ry1 - ry0; double len = Math.sqrt(dx*dx+dy*dy); 
          if (len > 0.1) {
            g2d.setColor(Color.black);
            g2d.drawLine(rx0,ry0,rx1,ry1);
            dx /= len; dy /= len;
            double pdx = -dy,       pdy = dx;
            g2d.drawLine(rx1, ry1, (int) (rx1 + pdx*3 - dx*4), (int) (ry1 + pdy*3 - dy*4));
            g2d.drawLine(rx1, ry1, (int) (rx1 - pdx*3 - dx*4), (int) (ry1 - pdy*3 - dy*4));
          }
        }
      } finally { if (g2d != null) g2d.dispose(); }
      return bi;
    }
  }

  /**
   * Class containing history of the robot as it cleans the room.
   */
  class History {
    int x, y, score; boolean state[][]; Action action;
    public History(int x, int y, Action action, boolean trash[][], int score) {
      this.x = x; this.y = y; this.score = score; this.action = action;
      state = new boolean[trash.length][trash[0].length];
      for (int i=0;i<state.length;i++) for (int j=0;j<state[i].length;j++) state[i][j] = trash[i][j];
    }
  }

  /**
   *
   */
  public static void main(String args[]) {
    try { new CleaningRobotv3(); } catch (Throwable t) { 
      System.err.println("Throwable: " + t); 
      t.printStackTrace(System.err);
    } 
  }
}

