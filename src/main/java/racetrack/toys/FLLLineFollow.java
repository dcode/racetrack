/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.toys;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;

import java.util.Iterator;
import java.util.LinkedList;

import javax.imageio.ImageIO;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.JToggleButton;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import racetrack.util.Utils;

public class FLLLineFollow extends JFrame implements Runnable {
  /**
   * Component that shows the field
   */
  FieldComponent      field_component;

  /**
   * Sensor view components
   */
  SensorViewComponent sensor_1_view,
                      sensor_2_view;

  /**
   * Component for adjusting the wheel distance and sensor placement
   */
  RobotConfigurationComponent robot_configuration_component;

  /**
   * Constructor
   */
  public FLLLineFollow() {
    super("FLL Line Follower Simulator");
    getContentPane().setLayout(new BorderLayout(4,4));
    getContentPane().add("Center", field_component = new FieldComponent());
    getContentPane().add("East",   createEasternPanel());
    pack();
    setVisible(true);

    // Get the independent threads running
    (new Thread(sensor_1_view)).start(); // Sensor 1 View
    (new Thread(sensor_2_view)).start(); // Sensor 2 View
    (new Thread(this)).         start(); // Simulation Thread
  }

  JSlider    s1_max_sl,          // Sensor 1 max
             s1_min_sl,          // Sensor 1 min
             s1_err_sl,          // Sensor 1 err
             s2_max_sl,          // Sensor 2 max
             s2_min_sl,          // Sensor 2 min
             s2_err_sl,          // Sensor 2 err
             movement_err_sl,    // Movement err
             movement_speed_sl,  // Movement speed
             simulation_rate_sl; // Simulation rate

  JTextField s1_max_tf, // Sensor 1 max text
             s1_min_tf, // Sensor 1 min text
             s2_max_tf, // Sensor 2 max text
             s2_min_tf; // Sensor 2 min text

  JToggleButton start_stop_tb;     // Toggle button to start and stop the simulation
  JButton       reset_position_bt, // Reset position button -- resets the robot's position to the last save
                save_position_bt;  // Save position button -- saves the robot's position 

  /**
   * Create the eastern panel -- all of the options (sliders, robot configuration) and sensor views
   */
  protected JPanel createEasternPanel() {
    JPanel east_panel = new JPanel(new BorderLayout()); JPanel panel;

    JPanel labels  = new JPanel(new GridLayout(0,1));
    JPanel sliders = new JPanel(new GridLayout(0,1));

    labels.add(new JLabel("Sensor 1 (Max)"));

      panel = new JPanel(new BorderLayout(4,4));
      panel.add("Center", s1_max_tf = new JTextField(3)); s1_max_tf.setEditable(false); s1_max_tf.setText("100");
      panel.add("East",   s1_max_sl = new JSlider(1, 100, 100)); s1_max_sl.addChangeListener(new ChangeListener() { 
        public void stateChanged(ChangeEvent ce) { s1_max_tf.setText("" + s1_max_sl.getValue()); 
                                                   if (s1_max_sl.getValue() <= s1_min_sl.getValue()) s1_min_sl.setValue(s1_max_sl.getValue()-1); } } );
      sliders.add(panel);

    labels.add(new JLabel("Sensor 1 (Min)"));

      panel = new JPanel(new BorderLayout(4,4));
      panel.add("Center", s1_min_tf = new JTextField(3)); s1_min_tf.setEditable(false); s1_min_tf.setText("0");
      panel.add("East",   s1_min_sl = new JSlider(0, 99, 0)); s1_min_sl.addChangeListener(new ChangeListener() { 
        public void stateChanged(ChangeEvent ce) { s1_min_tf.setText("" + s1_min_sl.getValue()); 
                                                   if (s1_min_sl.getValue() >= s1_max_sl.getValue()) s1_max_sl.setValue(s1_min_sl.getValue()+1); } } );
      sliders.add(panel);

    labels.add(new JLabel("Sensor 1 (Err)"));

      sliders.add(s1_err_sl = new JSlider(0, 100, 0)); s1_err_sl.setToolTipText("Sensor 1 Error");

    labels.add(new JLabel("Sensor 2 (Max)"));

      panel = new JPanel(new BorderLayout(4,4));
      panel.add("Center", s2_max_tf = new JTextField(3)); s2_max_tf.setEditable(false); s2_max_tf.setText("100");
      panel.add("East",   s2_max_sl = new JSlider(1, 100, 100)); s2_max_sl.addChangeListener(new ChangeListener() { 
        public void stateChanged(ChangeEvent ce) { s2_max_tf.setText("" + s2_max_sl.getValue()); 
                                                   if (s2_max_sl.getValue() <= s2_min_sl.getValue()) s2_min_sl.setValue(s2_max_sl.getValue()-1); } } );
      sliders.add(panel);

    labels.add(new JLabel("Sensor 2 (Min)"));

      panel = new JPanel(new BorderLayout(4,4));
      panel.add("Center", s2_min_tf = new JTextField(3)); s2_min_tf.setEditable(false); s2_min_tf.setText("0");
      panel.add("East",   s2_min_sl = new JSlider(0, 99, 0)); s2_min_sl.addChangeListener(new ChangeListener() { 
        public void stateChanged(ChangeEvent ce) { s2_min_tf.setText("" + s2_min_sl.getValue()); 
                                                   if (s2_min_sl.getValue() >= s2_max_sl.getValue()) s2_max_sl.setValue(s2_min_sl.getValue()+1); } } );
      sliders.add(panel);

    labels.add(new JLabel("Sensor 2 (Err)"));

      sliders.add(s2_err_sl = new JSlider(0, 100, 0)); s2_err_sl.setToolTipText("Sensor 2 Error");

    labels.add(new JLabel("Movement Error"));
      sliders.add(movement_err_sl = new JSlider(0, 100, 0));

    labels.add(new JLabel("Movement Speed"));
      sliders.add(movement_speed_sl = new JSlider(1, 100, 25));

    labels.add(new JLabel("Simulation Rate"));
      sliders.add(simulation_rate_sl = new JSlider(1, 100, 10));

    algorithms = new Algorithm[6];
    algorithms[0] = new ForwardAlgorithm();
    algorithms[1] = new PivotTurnAlgorithm();
    algorithms[2] = new CenterRotateAlgorithm();
    algorithms[3] = new SlightTurnAlgorithm();
    algorithms[4] = new FixedLineFollowerAlgorithm();
    algorithms[5] = new DifferentialLineFollowerAlgorithm();
    String algorithm_strs[] = new String[algorithms.length];
    for (int i=0;i<algorithm_strs.length;i++) algorithm_strs[i] = algorithms[i].algorithmDescription();

    labels.add(new JLabel("Algorithm"));
      sliders.add(algorithm_cb = new JComboBox(algorithm_strs));
    algorithm_cb.setSelectedItem("Differential Line Follower");

    labels.add(new JLabel("Control"));     
      JPanel bt_panel = new JPanel(new GridLayout(1,3));
      bt_panel.add(start_stop_tb     = new JToggleButton("Go"));
      bt_panel.add(reset_position_bt = new JButton("Rst"));  
      bt_panel.add(save_position_bt  = new JButton("Save"));   
      sliders.add(bt_panel);

   reset_position_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      robotX(saved_robot_x); robotY(saved_robot_y); robotDir(saved_robot_dir); field_component.repaint(); } } );

    save_position_bt. addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      saved_robot_x = robotX(); saved_robot_y = robotY(); saved_robot_dir = robotDir(); } } );

    JPanel combined = new JPanel(new BorderLayout()); combined.add("West",  labels); combined.add("East",  sliders); 
    east_panel.add("North", combined);

    east_panel.add("Center", robot_configuration_component = new RobotConfigurationComponent());

    // Sensor View Panel
    JPanel sensor_panel = new JPanel(new GridLayout(1,2));
    sensor_panel.add(sensor_2_view = new SensorViewComponent(2));
    sensor_panel.add(sensor_1_view = new SensorViewComponent(1));

    // Algorithm Options Panel
    alg_opts_panel = new JPanel(alg_opts_cards = new CardLayout());
    for (int i=0;i<algorithms.length;i++) alg_opts_panel.add(algorithms[i].algorithmDescription(), algorithms[i].optionsPanel());
    alg_opts_cards.show(alg_opts_panel, algorithms[0].algorithmDescription());

    panel = new JPanel(new GridLayout(2,1,5,5));
    panel.add(alg_opts_panel);
    panel.add(sensor_panel);

    east_panel.add("South", panel);

    // ComboBox Listener to display the right card
    algorithm_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) {
      int i = algorithm_cb.getSelectedIndex();
      alg_opts_cards.show(alg_opts_panel, algorithms[i].algorithmDescription());
    } } );

    return east_panel;
  }

  /**
   * Algorithm selection combobox
   */
  JComboBox algorithm_cb;

  /**
   * Cards for algorithm options
   */
  CardLayout alg_opts_cards;

  /**
   * Algorithm options panel
   */
  JPanel alg_opts_panel;

  /**
   * Instantiated algorithms
   */
  Algorithm algorithms[];

  /**
   * Algorithm abstraction and implementations
   */
  abstract class Algorithm {
    JPanel opts;
    public abstract String  algorithmDescription();
    public abstract int[]   motorSetting();
    public          JPanel  optionsPanel() { if (opts == null) opts = new JPanel(); return opts; }
    public          String  informationString() { return null; }
  }

  /**
   * Forward ... just goes straight... 
   */
  class ForwardAlgorithm extends Algorithm {
    public String algorithmDescription() { return "Forward"; }
    public int[]  motorSetting() {
      int settings[] = new int[2];
      settings[0] = settings[1] = movement_speed_sl.getValue();
      return settings;
    }
  }

  /**
   * Pivot turn -- rotates around one of the wheels.
   */
  class PivotTurnAlgorithm extends Algorithm {
    public String algorithmDescription() { return "Pivot Turn"; }
    public int[] motorSetting() {
      int settings[] = new int[2];
      settings[0] = 0; settings[1] = movement_speed_sl.getValue();
      return settings;
    }
  }

  /**
   * Center rotate -- rotates around the center of the robot.
   */
  class CenterRotateAlgorithm extends Algorithm {
    public String algorithmDescription() { return "Center Rotate"; }
    public int[] motorSetting() {
      int settings[] = new int[2];
      settings[0] = -1 * movement_speed_sl.getValue(); settings[1] = movement_speed_sl.getValue();
      return settings;
    }
  }

  /**
   * Slight turn -- implements a slight turn.
   */
  class SlightTurnAlgorithm extends Algorithm {
    JSlider difference_sl;
    public SlightTurnAlgorithm() {
      opts = new JPanel(new BorderLayout(5,5)); 
      opts.add("West", new JLabel("Difference"));
      opts.add("Center", difference_sl = new JSlider(1,20,10));
    }
    public String algorithmDescription() { return "Slight Turn"; }
    public int[] motorSetting() {
      int settings[] = new int[2];
      settings[0] = movement_speed_sl.getValue() - (difference_sl.getValue() - 10); if (settings[0] < -100) settings[0] = -100;
      settings[1] = movement_speed_sl.getValue() + (difference_sl.getValue() - 10); if (settings[1] >  100) settings[1] =  100;
      return settings;
    }
  }

  /**
   * Fixed line follower -- uses fixed threshold for turning (or going straight).
   */
  class FixedLineFollowerAlgorithm extends Algorithm {
    JSlider lower, upper, steerage;
    public FixedLineFollowerAlgorithm() {
      opts = new JPanel(new BorderLayout());
      JPanel labels   = new JPanel(new GridLayout(0,1));
      JPanel controls = new JPanel(new GridLayout(0,1));
        labels.add(new JLabel("Lower Threshold"));
        controls.add(lower = new JSlider(1,99,25));
        labels.add(new JLabel("Upper Threshold"));
        controls.add(upper = new JSlider(1,99,75));
        labels.add(new JLabel("Steerage"));
        controls.add(steerage = new JSlider(1,40,10));
      opts.add("West",   labels);
      opts.add("Center", controls);
    }
    public String algorithmDescription() { return "Fixed Line Follower"; }
    public int[] motorSetting() {
      int settings[] = new int[2];

      settings[0] = movement_speed_sl.getValue();
      settings[1] = movement_speed_sl.getValue();

      int sensor = sensorValue(1);

      if (sensor < lower.getValue()) settings[1] += steerage.getValue();
      if (sensor > upper.getValue()) settings[0] += steerage.getValue();

      return settings;
    }
    public String informationString() { 
      return "Steerage = "+ steerage.getValue() + " | " +
             "Lower = " +   lower.getValue()    + " | " +
             "Upper = " +   upper.getValue();
    }
  }

  /**
   * Differential line follower -- uses the difference in the middle
   * of the sensor to adjust steering.
   */
  class DifferentialLineFollowerAlgorithm extends Algorithm {
    JSlider multiplier_sl, middle_sl;
    public DifferentialLineFollowerAlgorithm() {
      opts = new JPanel(new BorderLayout());

      JPanel labels   = new JPanel(new GridLayout(0,1));
      JPanel controls = new JPanel(new GridLayout(0,1));
        labels.add(new JLabel("Multipler"));
        controls.add(multiplier_sl = new JSlider(1,100,10));
        labels.add(new JLabel("Middle"));
        controls.add(middle_sl = new JSlider(1,99,50));

      opts.add("West",   labels);
      opts.add("Center", controls);
    }
    public String algorithmDescription() { return "Differential Line Follower"; }

    public int[] motorSetting() {
      int settings[] = new int[2];
      settings[0] = movement_speed_sl.getValue();
      settings[1] = movement_speed_sl.getValue();
      int sensor = sensorValue(1);
      double mult = multiplier_sl.getValue() / 10.0;

      if (sensor != middle_sl.getValue()) {
        if (s1_y_off >= 0.0) {
          settings[0] -= (int) (mult * (middle_sl.getValue() - sensor));
          settings[1] += (int) (mult * (middle_sl.getValue() - sensor));
        } else {
          settings[0] -= (int) (mult * (middle_sl.getValue() - sensor));
          settings[1] += (int) (mult * (middle_sl.getValue() - sensor));
        }
      }
      System.err.println(settings[0] + " | " + settings[1]);


      return settings;
    }

    // This version works fine if the sensor is in front of the wheels...
    public int[] motorSettingXXX() {
      int settings[] = new int[2];

      settings[0] = movement_speed_sl.getValue();
      settings[1] = movement_speed_sl.getValue();

      int sensor = sensorValue(1);

      double mult = multiplier_sl.getValue() / 10.0;

      if (sensor != middle_sl.getValue()) {
        settings[0] -= (int) (mult * (middle_sl.getValue() - sensor));
        settings[1] += (int) (mult * (middle_sl.getValue() - sensor));
      }

      return settings;
    }
    public String informationString() {
      double mult = multiplier_sl.getValue() / 10.0;
      return "Multiplier = " + mult + " | " +
             "Middle = " + middle_sl.getValue();
    }
  }

  /**
   * Set the power to the two motors.
   */
  public void setMotorPower(int settings[]) { 
    synchronized (motor_mutex) {
      motor_a = settings[0]; motor_b = settings[1]; 
  } }

  /**
   * Motor A Power -- 0 ... 100
   */
  int motor_a = 0,

  /**
   * Motor B Power -- 0 ... 100
   */
      motor_b = 0;

  Object motor_mutex = new Object();

  /**
   * Update the position based on the motor powers ... include the error rate here.
   *
   * Formulas from youtube video "Control of Mobile Robots - 2.2 Differential Drive Robots"
   * ... http://www.youtube.com/watch?v=aE7RQNhwnPQ
   */
  public void updateRobotPosition() {
    // Get the settings
    double motor_a_adj, motor_b_adj;
    synchronized (motor_mutex) {
      if (motor_a == 0 && motor_b == 0) return;
      motor_a_adj = motor_a;
      motor_b_adj = motor_b;
    }

    // Apply the error (if any)
    int movement_err = movement_err_sl.getValue();
    if (movement_err > 0) {
      motor_a_adj += Math.random() * Math.sqrt(movement_err);
      motor_b_adj += Math.random() * Math.sqrt(movement_err);
    }

    // Variable names from youtube video
    double R_adj = 12.1;

    double R    = 1.0, L = 2.0 * track_half_in * R_adj;
    double step = 100.0;
    double phi  = robotDir() + Math.PI/2.0, x0 = robotX(), y0 = robotY();

    // Differential x, y, phi calcs
    double dx   = (R/2.0) * (motor_a_adj/step + motor_b_adj/step) * Math.cos(phi),
           dy   = (R/2.0) * (motor_a_adj/step + motor_b_adj/step) * Math.sin(phi);
    double dphi = (R/L)   * (motor_a_adj/step - motor_b_adj/step);

    robotUpdate(x0 + dx, y0 + dy, phi + dphi - Math.PI/2.0);
    field_component.repaint();
  }

  /**
   * Simulation thread
   */
  public void run() {
    while (true) {
      if (start_stop_tb.isSelected()) {

        int motor_settings[] = algorithms[algorithm_cb.getSelectedIndex()].motorSetting();

        setMotorPower(motor_settings);

        updateRobotPosition();

        try { Thread.sleep(simulation_rate_sl.getValue()); } catch (InterruptedException ie) { }
      } else { try { Thread.sleep(50); field_component.repaint(); } catch (InterruptedException ie) { } }
    }
  }

  /**
   * Mutex for accessing the last robot transform
   */
  Object          last_robot_trans_mutex = new Object();

  /**
   * Last robot transform... believe that it's used for the sensor view subimage
   */
  AffineTransform last_robot_trans       = null;

  /**
   * The field image
   */
  BufferedImage   field_image; 

  /**
   * Scale of the map... empirically derived... probabaly won't work for the FLL maps
   */
  double          x_scale = 0.122, // 0.11
                  y_scale = 0.122; // 0.11

  /**
   * Dots per inch... not really... just some number I made up
   */
  double          dpi = 100;

  /**
   * Draws the field and robot...  enables moving the robot around using the mouse...
   */
  class FieldComponent extends JComponent implements MouseListener, MouseMotionListener, MouseWheelListener {
    /**
     * Constructor
     */
    public FieldComponent() {
      field_image = createPlayground();
      setPreferredSize(new Dimension((int) (x_scale * field_image.getWidth()),
                                     (int) (y_scale * field_image.getHeight())));
      addMouseListener(this); addMouseMotionListener(this); addMouseWheelListener(this);
    }

    /**
     * Original settings upon mouse press
     */
    int    mouse_orig_x, mouse_orig_y;
    double robot_orig_x, robot_orig_y;

    /**
     * Mouse Listeners... handles user interaction with positioning/orienting the robot
     */
    public void mouseEntered   (MouseEvent me) { }
    public void mouseExited    (MouseEvent me) { }
    public void mousePressed   (MouseEvent me) { 
      robot_orig_x = robotX();  robot_orig_y = robotY();
      mouse_orig_x = me.getX(); mouse_orig_y = me.getY();
    }
    public void mouseDragged   (MouseEvent me) { 
      int dx = me.getX() - mouse_orig_x,
          dy = me.getY() - mouse_orig_y;

      robotX(robot_orig_x + dx); // * x_scale;
      robotY(robot_orig_y + dy); // * y_scale;

      repaint();
    }
    public void mouseReleased  (MouseEvent me) { }
    public void mouseClicked   (MouseEvent me) { }
    public void mouseMoved     (MouseEvent me) { }
    public void mouseWheelMoved(MouseWheelEvent me) { 
      double new_dir = robotDir() + Math.PI * me.getWheelRotation()/45.0;
      while (new_dir < 0.0)       new_dir += 2*Math.PI;
      while (new_dir > 2*Math.PI) new_dir -= 2*Math.PI;
      robotDir(new_dir); repaint();
    }

    /**
     * Render the field and the robot / robot details
     */
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      AffineTransform trans_orig = g2d.getTransform();

      // Draw the base image
      g2d.scale(x_scale, y_scale);
      g2d.drawImage(field_image, 0, 0, null);

      // Draw the robot
      g2d.setTransform(trans_orig);
      g2d.translate(robotX(),
                    robotY());
      g2d.scale(    x_scale*dpi, 
                    y_scale*dpi);
      g2d.rotate(   robotDir());

      g2d.setStroke(new BasicStroke((float) x_scale));
      g2d.setColor(Color.blue);

      double in = 0.25;
      g2d.draw(new Line2D.Double(-in, -in,  in, in));
      g2d.draw(new Line2D.Double( in, -in, -in, in));

      g2d.fill(new Rectangle2D.Double((-track_half_in - 0.2), -0.8, 0.4, 1.6));
      g2d.fill(new Rectangle2D.Double(( track_half_in - 0.2), -0.8, 0.4, 1.6));

      g2d.setColor(Color.green);
      g2d.draw(new Rectangle2D.Double(s1_x_off - 0.2, s1_y_off - 0.2, 0.4, 0.4));
      g2d.draw(new Rectangle2D.Double(s2_x_off - 0.2, s2_y_off - 0.2, 0.4, 0.4));

      synchronized (last_robot_trans_mutex) { last_robot_trans = g2d.getTransform(); }

      // Reset the transform and draw a frame
      g2d.setTransform(trans_orig);

      g2d.setColor(Color.black);
      g2d.drawRect(0,0,getWidth()-1,getHeight()-1);

      String alg_info_str = algorithms[algorithm_cb.getSelectedIndex()].informationString();
      if (alg_info_str != null) {
        g2d.drawString(alg_info_str, 5, 5 + Utils.txtH(g2d, "0"));
      }
    }
  }

  /**
   * Robot's X Coordinate
   */
  private double robot_x = 160.0,

  /**
   * Robot's Y Coordinate
   */
                 robot_y =  93.0,

  /**
   * Robot's Direction (Angle)
   */
                 robot_dir = -2.0*Math.PI/4.0;

  Object robot_mutex = new Object();

  public double robotX()           { synchronized (robot_mutex) { return robot_x; } }
  public void   robotX(double d)   { synchronized (robot_mutex) { robot_x = d;    } }
  public double robotY()           { synchronized (robot_mutex) { return robot_y; } }
  public void   robotY(double d)   { synchronized (robot_mutex) { robot_y = d;    } }
  public double robotDir()         { synchronized (robot_mutex) { return robot_dir; } }
  public void   robotDir(double d) { 
    synchronized (robot_mutex) { 
      // Keep it bound between 0.0 and 2*pi
      while (d < 0.0)           d += Math.PI * 2;
      while (d > 2.0 * Math.PI) d -= Math.PI *2;
      robot_dir = d;    
  } }
  public void robotUpdate(double new_x, double new_y, double new_dir) {
    synchronized (robot_mutex) {
      robot_x = new_x; robot_y = new_y; robot_dir = new_dir;
    }
  }

  /**
   * Saved values for easy restoration...
   */
  private double saved_robot_x   = robotX(),
                 saved_robot_y   = robotY(),
                 saved_robot_dir = robotDir();

  /**
   * Inches from the center point to each wheel
   */
  private double track_half_in = 2.0; 

  /**
   * Sensor 1 and 2 x/y coordinate offsets from the center point
   */
  private double s1_x_off = -2.0, s1_y_off = -2.0,
                 s2_x_off =  2.0, s2_y_off = -2.0;

  /**
   *
   */
  class RobotConfigurationComponent extends JComponent implements MouseListener, MouseMotionListener {

    /**
     * Constructor
     */
    public RobotConfigurationComponent() {
      setPreferredSize(new Dimension(192, 192));
      addMouseListener(this);
      addMouseMotionListener(this);
    }

    public void mousePressed  (MouseEvent me) { 
      if        (wheel_p_rect.contains(me.getX(), me.getY()) ||
                 wheel_n_rect.contains(me.getX(), me.getY())) { moving_wheels  = true;
      } else if (sensor1_rect.contains(me.getX(), me.getY())) { moving_sensor1 = true;
      } else if (sensor2_rect.contains(me.getX(), me.getY())) { moving_sensor2 = true; }
    }
    public void mouseReleased (MouseEvent me) { moving_wheels = moving_sensor1 = moving_sensor2 = false; }
    public void mouseClicked  (MouseEvent me) { }
    public void mouseEntered  (MouseEvent me) { }
    public void mouseExited   (MouseEvent me) { moving_wheels = moving_sensor1 = moving_sensor2 = false; }
    public void mouseMoved    (MouseEvent me) { }
    public void mouseDragged  (MouseEvent me) { 

      int x_axis_ycoord = getHeight() /2,
          y_axis_xcoord = getWidth()  /2;

      if        (moving_wheels)  {
        track_half_in = Math.abs(-me.getX() + y_axis_xcoord)/scale;
      } else if (moving_sensor1) {
        s1_x_off = (-me.getX() + y_axis_xcoord)/scale;
        s1_y_off = (-me.getY() + x_axis_ycoord)/scale;
      } else if (moving_sensor2) {
        s2_x_off = (-me.getX() + y_axis_xcoord)/scale;
        s2_y_off = (-me.getY() + x_axis_ycoord)/scale;
      }
      repaint(); field_component.repaint();
    }

    boolean moving_wheels  = false,
            moving_sensor1 = false,
            moving_sensor2 = false;

    /**
     * Scale of the rendering
     */
    double scale = 50.0;

    /**
     * various geometries
     */
    Rectangle2D wheel_p_rect, wheel_n_rect, sensor1_rect, sensor2_rect;

    /**
     * Render the component
     */
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;

      int x_axis_ycoord = getHeight() /2,
          y_axis_xcoord = getWidth()  /2;

      g2d.setColor(Color.lightGray);
      g2d.drawLine(0,            x_axis_ycoord, getWidth(),    x_axis_ycoord);
      g2d.drawLine(y_axis_xcoord, 0,            y_axis_xcoord, getHeight());

      int x,y;

      x  = y_axis_xcoord + ((int) (scale * track_half_in));
      wheel_p_rect = new Rectangle2D.Double(x - 6, x_axis_ycoord - 24, 12, 48);
      x  = y_axis_xcoord - ((int) (scale * track_half_in));
      wheel_n_rect = new Rectangle2D.Double(x - 6, x_axis_ycoord - 24, 12, 48);

      x = y_axis_xcoord - ((int) (scale * s1_x_off)); y = x_axis_ycoord - ((int) (scale * s1_y_off));
      sensor1_rect = new Rectangle2D.Double(x - 6, y - 6, 12, 12);
      x = y_axis_xcoord - ((int) (scale * s2_x_off)); y = x_axis_ycoord - ((int) (scale * s2_y_off));
      sensor2_rect = new Rectangle2D.Double(x - 6, y - 6, 12, 12);

      g2d.setColor(Color.blue);
      g2d.fill(wheel_p_rect);
      g2d.fill(wheel_n_rect);

      g2d.setColor(Color.green);
      g2d.fill(sensor1_rect);
      g2d.fill(sensor2_rect);

      g2d.setColor(Color.black); 
      g2d.draw(wheel_p_rect);
      g2d.draw(wheel_n_rect);
      g2d.draw(sensor1_rect);
      g2d.draw(sensor2_rect);

      g2d.drawString("1", (int) (sensor1_rect.getCenterX() - Utils.txtW(g2d, "1")/2), (int) (sensor1_rect.getY() - 2));
      g2d.drawString("2", (int) (sensor2_rect.getCenterX() - Utils.txtW(g2d, "2")/2), (int) (sensor2_rect.getY() - 2));

      String str = "" + track_half_in;
      g2d.drawString(str, y_axis_xcoord - Utils.txtW(g2d, str)/2, x_axis_ycoord);

      str = "( " + s1_x_off + " , " + s1_y_off + " )";
      g2d.drawString(str, 5, 5 + Utils.txtH(g2d, str));

      str = "( " + s2_x_off + " , " + s2_y_off + " )";
      g2d.drawString(str, getWidth() - 5 - Utils.txtW(g2d, str), 5 + Utils.txtH(g2d, str));

      g2d.drawRect(0,0,getWidth()-1,getHeight()-1);

      g2d.setStroke(new BasicStroke(3.0f));
      int y_arrow = getHeight()/4;
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - Utils.txtH(g2d, "0") - 2, 
                   y_axis_xcoord,      x_axis_ycoord - y_arrow);
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - y_arrow,
                   y_axis_xcoord - 10, x_axis_ycoord - y_arrow + 10);
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - y_arrow,
                   y_axis_xcoord + 10, x_axis_ycoord - y_arrow + 10);
    }
  }

  /**
   * Convenience method for reading the sensors
   */
  public int sensorValue(int sensor) {
    int value = 50;
    if (sensor == 1) { synchronized (s1_mutex) { value = s1_value; } }
    else             { synchronized (s2_mutex) { value = s2_value; } }
    return value;
  }

  /**
   * Sensor Values (Updated by the Sensor View Component via Thread)
   */
  private int s1_value = 50,
              s2_value = 50;

  /**
   * Mutexes for reading the sensor vales
   */
  private Object s1_mutex = new Object(),
                 s2_mutex = new Object();

  /**
   *
   */
  class SensorViewComponent extends JComponent implements Runnable {
    int sensor_number;
    public SensorViewComponent(int sensor_number) { 
      this.sensor_number = sensor_number; 
      setPreferredSize(new Dimension(96,96));
    }
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;
      
      double x_off, y_off;
      if (sensor_number == 1) { x_off = s1_x_off; y_off = s1_y_off; }
      else                    { x_off = s2_x_off; y_off = s2_y_off; }

      AffineTransform my_robot_trans = last_robot_trans; if (my_robot_trans != null) {
        Point2D pt = my_robot_trans.transform(new Point2D.Double(x_off, y_off), null);
        int x_field = (int) (pt.getX()/x_scale),
            y_field = (int) (pt.getY()/y_scale);

        // What can the sensor see?
        int sensor_view = (int) (dpi/2); // int x0 = x_field - sensor_view/2, y0 = y_field - sensor_view/2;

        // Extract the image from the field that fits this view
        int x0          = x_field - getWidth()/2, 
            y0          = y_field - getHeight()/2;
        if (x0 < 0)                                         x0 = 0; 
        if (y0 < 0)                                         y0 = 0;
        if (x0 > field_image.getWidth () - getWidth () - 1) x0 = field_image.getWidth () - getWidth()  - 1;
        if (y0 > field_image.getHeight() - getHeight() - 1) y0 = field_image.getHeight() - getHeight() - 1;
        g2d.drawImage(field_image.getSubimage(x_field - getWidth()/2,
                                              y_field - getHeight()/2,
                                              getWidth(),
                                              getHeight()), 0, 0, null);
        // Show the sensor view as a green rectangle
        g2d.setColor(Color.green);
        g2d.drawRect(getWidth()/2  - sensor_view/2,
                     getHeight()/2 - sensor_view/2, sensor_view, sensor_view);

        // Draw sensor number information
        g2d.setColor(Color.darkGray); 
        g2d.drawString(""+sensor_number, getWidth() - 5 - Utils.txtW(g2d, ""+sensor_number), Utils.txtH(g2d, ""+sensor_number) + 5);

        // Recalculate the geometry for the sensor
        x0 = x_field - sensor_view/2; 
        y0 = y_field - sensor_view/2;
        if (x0 < 0)                                         x0 = 0; 
        if (y0 < 0)                                         y0 = 0;
        if (x0 > field_image.getWidth () - sensor_view - 1) x0 = field_image.getWidth () - sensor_view - 1;
        if (y0 > field_image.getHeight() - sensor_view - 1) y0 = field_image.getHeight() - sensor_view - 1;

        // Calculate the gray value / intensity of what the sensor sees
        int gray_value = calculateAverageGrayValue(field_image.getSubimage(x0, y0, sensor_view, sensor_view)); // 0 ... 255

        // Do some math to adjust to user-supplied parameters (min, max, err)
        double d = ((double) gray_value)/254.0;
        int adj_value;

        if (sensor_number == 1) {
          adj_value = (int) (s1_min_sl.getValue() + (s1_max_sl.getValue() - s1_min_sl.getValue()) * d);
          if (s1_err_sl.getValue() > 0) { // Add Gaussian Noise...
            double noise = Math.random() * Math.sqrt(s1_err_sl.getValue()) + adj_value;
            adj_value = (int) noise;
            if (adj_value < s1_min_sl.getValue()) adj_value = s1_min_sl.getValue();
            if (adj_value > s1_max_sl.getValue()) adj_value = s1_max_sl.getValue();
          }
          synchronized (s1_mutex) { s1_value = adj_value; }
        } else {
          adj_value = (int) (s2_min_sl.getValue() + (s2_max_sl.getValue() - s2_min_sl.getValue()) * d);
          if (s2_err_sl.getValue() > 0) { // Add Gaussian Noise...
            double noise = Math.random() * Math.sqrt(s2_err_sl.getValue()) + adj_value;
            adj_value = (int) noise;
            if (adj_value < s2_min_sl.getValue()) adj_value = s2_min_sl.getValue();
            if (adj_value > s2_max_sl.getValue()) adj_value = s2_max_sl.getValue();
          }
          synchronized (s2_mutex) { s2_value = adj_value; }
        }

        // Maintain a history
        history.add(adj_value); while (history.size() > getWidth()) history.remove();

        // Draw the values
        g2d.setColor(Color.red);
        g2d.drawString("" + gray_value + " (" + adj_value + ")", 5, 5 + Utils.txtH(g2d, "0"));

        // Draw the history
        if (history.size() > 5) {
          Iterator<Integer> it = history.iterator(); int x = 0;
          int last = it.next(); while (it.hasNext()) {
            int now = it.next();
            g2d.drawLine(x, getHeight() - 5 - last/2, x+1, getHeight() - 5 - now/2);
            last = now; x++;
          }
        }
      }

      g.setColor(Color.black); g.drawRect(0,0,getWidth(),getHeight());
    }

    /**
     * History of the readings for a graph of the trend...
     */
    LinkedList<Integer> history = new LinkedList<Integer>();

    // Refresh at some rate... probably should be at least 1/10th of a second... or faster
    public void run() { while (true) { try { Thread.sleep(25); repaint(); } catch (InterruptedException ie) { } } }

    // Calculate average gray value
    public int calculateAverageGrayValue(BufferedImage img) {
      long sum = 0L; int samples = 0;

      for (int x=0;x<img.getWidth();x++) for (int y=0;y<img.getHeight();y++) {
        int rgb  = img.getRGB(x,y);
        int gray = (int) (((rgb >> 16) & 0x00ff) * 0.2989 + ((rgb >>  8) & 0x00ff) * 0.5870 + ((rgb >>  0) & 0x00ff) * 0.1140);
        sum += gray; samples++;
      }

      if (samples == 0) samples = 1;
      return (int) (sum/samples);
    }
  }

  /**
   *
   */
  public static void main(String args[]) {
    try { new FLLLineFollow(); } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }


  /**
   * Create a base image with multiple examples of lines.
   */
  public static BufferedImage createPlayground() {
    BufferedImage bi = new BufferedImage(11925,6713,BufferedImage.TYPE_INT_RGB); // Latest size of FLL 2020 map
    Graphics2D g2d = (Graphics2D) bi.getGraphics();
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    g2d.setColor(Color.white);
    g2d.fillRect(0,0,bi.getWidth(),bi.getHeight());

    g2d.setStroke(new BasicStroke(127.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    g2d.setColor(Color.black);

    Path2D path;

    /* Straight Line */
    g2d.drawLine(900, 900, 11000, 900);

    /* Mostly Straight Line -- One Bend */
    path = new Path2D.Double(); path.moveTo(900, 1800);
    path.lineTo(2000,   1800);
    path.lineTo(3000,   2200);
    path.lineTo(8000,   2200);
    path.lineTo(8500,   1800);
    path.lineTo(11000,  1800);
    g2d.draw(path);

    /* Circle */
    g2d.drawOval(1000, 2600, 3200, 3200);
    g2d.drawOval(1600, 3200, 2000, 2000);

    // Write the image to disk
    // try { ImageIO.write(bi, "png", new File("test.png")); } catch (IOException ioe) { }

    g2d.dispose(); return bi;
  }
}

