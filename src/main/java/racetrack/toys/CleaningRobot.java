/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.toys;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.*;
import javax.imageio.*;

public class CleaningRobot {
  int    THREADS      =  10;    // Needs to evenly divide into pool
  int    TESTS        =  3;
  int    tiles_w      =  20,    // Width of the room in cells
         tiles_h      =  20,    // Height of the room in cells
         steps        =  800,   // Number of steps robot gets to clean ... 400 squares (20x20)... cleaning all would be 800 moves
         winners      =  10,    // Number of robots that win each generation
         pool         =  100,   // Number of robots that compete each generation // evenly divisible by threads
         pt_clean     =  16,     // Points robot gets for picking up trash
         pt_clean_err = -2,     // Points deducted if the robot tries to clean a spot with no trash
         pt_dirty     = -3,     // Points deducted at end of round for any remaining trash
         pt_wall      = -4;     // Points deducted if the robot hits a wall
  double trash_pr     =  0.50;  // Probability that trash is in a cell
  int    gens         =  10000; // Number of generations to simulate
  double mutate_pr    =  0.02;  // Mutation probability

  Robot top_scorer       =  null;
  int   top_scorer_score =  Integer.MIN_VALUE;
  Robot robots[]         =  new Robot[pool];
  Score scores[]         =  new Score[pool];

  /**
   * Run the simulation...
   */
  public CleaningRobot() throws IOException {
    PrintStream log = new PrintStream(new FileOutputStream("cleaner_log.csv"));

    List<Integer> top_score_history = new ArrayList<Integer>(); int high_score = 0;

    for (int i=0;i<robots.length;i++) robots[i] = new Robot();
    for (int gen=0;gen<gens;gen++) {
      // Compete
      RobotThread threads[]  = new RobotThread[THREADS];
      int         per_thread = robots.length / THREADS + 1;
      for (int i=0;i<threads.length;i++) {
        int i0 = i*per_thread, i1 = i0 + per_thread; 
        if (i0 > robots.length) i0 = robots.length;
        if (i1 > robots.length) i1 = robots.length;
        if (i0 != i1) { threads[i] = new RobotThread(i0,i1); threads[i].start(); }
      }
      for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }
      Arrays.sort(scores);
      // for (int i=0;i<scores.length;i++)  { System.out.println("" + i + " : " + scores[i]); }

      // For high scores, save the image and the robot to a file.
      if (scores[0].score > high_score) {
        String filename = "" + gen; while (filename.length() < 5) filename = "0" + filename;
        BufferedImage bi = scores[0].room.historyTrace();
        try { ImageIO.write(bi, "png", new File("gen_" + filename + "_" + scores[0].score + ".png")); } catch (IOException ioe) { } 
        high_score = scores[0].score;
        try { scores[0].robot.saveToFile(new File("gen_" + gen + "_" + scores[0].score + ".robot")); } catch (IOException ioe) { }
      }

      // Print out a line for each generation
      System.out.print("Gen " + gen + " : Scores = ");
      for (int i=0;i<10;i++) {
        String s = "" + scores[i].score; while (s.length() < 4) s = " " + s;
        System.out.print(s + " (" + (scores[i].robot.wins == 0 ? " " : "" + scores[i].robot.wins) +") ");
      }

      // Update the top scorer history... and print out stats about this generation... add a log line
      top_score_history.add(scores[0].score); if (top_score_history.size() > 100) top_score_history.remove(0);
      double sum = 0.0; for (int i=0;i<top_score_history.size();i++) sum += top_score_history.get(i);
      System.out.print(" (Sz=" + scores[0].robot.prog.keySet().size() + ") ");
      if (top_scorer != null) System.out.print(" [" + top_scorer.id + ":" + top_scorer_score + " (" + top_scorer.wins +")]");
      System.out.print(" TopScoreAve=" + (sum/top_score_history.size()));
      System.out.println("");

      if (top_scorer != null) log.println(gen + "," + scores[0].score + "," + (sum/top_score_history.size()) + "," + top_scorer.id + "," + top_scorer_score + "," + top_scorer.wins);

      // Prepare the next generation
      int robot = 0;

      // Keep the winners
      while (robot < (winners-1)) { robots[robot] = scores[robot].robot; robot++; }

      // Keep track of the best
      if (top_scorer == null || top_scorer_score < scores[0].score) { top_scorer = scores[0].robot; top_scorer_score = scores[0].score; }
      robots[robot++] = top_scorer; scores[0].robot.wins++;

      // Create robots as children of the top robots
      List<MergeThread> merges = new ArrayList<MergeThread>();
      while (robot < robots.length) {
        int mommy = robot % winners, daddy = ((int) (Math.random() * Integer.MAX_VALUE)) % winners;
        MergeThread thread; merges.add(thread = new MergeThread(robot, scores[mommy].robot, scores[daddy].robot)); thread.start();
        robot++;
      }
      for (int i=0;i<merges.size();i++) { try { merges.get(i).join(); } catch (InterruptedException ie) { } }
    }
  }

  /**
   * Thread to create a new robot from a mommy and daddy.
   */
  class MergeThread extends Thread {
    int index; Robot mommy, daddy;
    public MergeThread(int index, Robot mommy, Robot daddy) { this.index = index; this.mommy = mommy; this.daddy = daddy; }
    public void run() { robots[index] = new Robot(mommy, daddy); }
  }

  /**
   * Thread to execute a run of robot simulations for a limited set of the total population.
   */
  class RobotThread extends Thread {
    int i0, i1;
    public RobotThread(int i0, int i1) { this.i0 = i0; this.i1 = i1; }
    public void run() {
      for (int i=i0;i<i1;i++) {
        Robot robot = robots[i]; int best_score = Integer.MIN_VALUE; Room best_room = null;
        for (int t=0;t<TESTS;t++) {
          Room room  = new Room(); 
          int  score = room.simulate(robot);
          if (score > best_score) { best_score = score; best_room = room; }
        }
        scores[i] = new Score(robot, best_score, best_room);
      }
    }
  }

  /**
   * Class to hold a score, the robot, and the room.  Sortable...
   */
  class Score implements Comparable<Score> {
    int score; Robot robot; Room room;
    public Score(Robot robot, int score, Room room) { this.robot = robot; this.score = score; this.room = room; }
    public int compareTo(Score other) {
      if      (other.score > score) return  1;
      else if (other.score < score) return -1;
      else if (other.robot.prog.keySet().size() > robot.prog.keySet().size()) return  1;
      else if (other.robot.prog.keySet().size() < robot.prog.keySet().size()) return -1;
      else return 0;
    }
    public String toString() { return "" + score; }
  }

  /**
   * Actions that the robot can take at each step.
   */

  enum Action { PICKUP_TRASH, MOVE_NE, MOVE_N, MOVE_NW, MOVE_W, MOVE_SW, MOVE_S, MOVE_SE, MOVE_E, MOVE_RANDOM };

  /**
   * State of a cell in the room.
   */
  enum CellState { WALL, TRASH, EMPTY };

  /**
   * Class representing the robot and its state machine for cleaning a room.
   */
  class Robot {
    int                wins = 0;
    int                id   = 100000 + ((int) (Math.random()*Integer.MAX_VALUE))%50000;
    Map<String,Action> prog = new HashMap<String,Action>();

    /**
     * Save the robot to a file.
     */
    public void saveToFile(File file) throws IOException {
      PrintStream out = new PrintStream(new FileOutputStream(file)); out.println(id); 
      Iterator<String> it = prog.keySet().iterator(); while (it.hasNext()) { String key = it.next(); out.println(key + "," + prog.get(key)); }
      out.close();
    }

    /**
     * Create a blank robot.
     */
    public Robot() { }

    /**
     * Create a robot that's a blend of mommy and daddy.
     */
    public Robot(Robot mommy, Robot daddy) {
      // Mommy's contributions -- have to deconflict when they are in Daddy's genes as well
      Iterator<String> it = mommy.prog.keySet().iterator();
      while (it.hasNext()) {
        String encoded = it.next();
        if (daddy.prog.containsKey(encoded)) {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else {
            if (Math.random() < 0.5) prog.put(encoded, mommy.prog.get(encoded));
            else                     prog.put(encoded, daddy.prog.get(encoded));
          }
        } else {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else prog.put(encoded, mommy.prog.get(encoded));
        }
      }

      // Now Daddy's unique contributions
      it = daddy.prog.keySet().iterator();
      while (it.hasNext()) {
        String encoded = it.next();
        if (prog.containsKey(encoded) == false) {
          if (Math.random() < mutate_pr) { prog.put(encoded, randomAction());
          } else prog.put(encoded, daddy.prog.get(encoded));
        }
      }
    }

    /**
     * Create a random action for the robot.
     */
    public Action randomAction() {
        Action action = Action.PICKUP_TRASH; int index = ((int) (Math.random() * Integer.MAX_VALUE))%10;
        if      (index == 0)  action = Action.PICKUP_TRASH;
        else if (index == 1)  action = Action.MOVE_NE;
        else if (index == 2)  action = Action.MOVE_N;
        else if (index == 3)  action = Action.MOVE_NW;
        else if (index == 4)  action = Action.MOVE_W;
        else if (index == 5)  action = Action.MOVE_SW;
        else if (index == 6)  action = Action.MOVE_S;
        else if (index == 7)  action = Action.MOVE_SE;
        else if (index == 8)  action = Action.MOVE_E;
        else if (index == 9)  action = Action.MOVE_RANDOM;
        return action;
    }

    /**
     * For an environmental cell, determine the action of the robot.  This is a state machine--if the
     * environmental state is not present, then a random action will be chosen.
     */
    public Action determineAction(CellState state[][]) {
      String encoded = encode(state);
      if (prog.containsKey(encoded) == false) { prog.put(encoded, randomAction()); }
      return prog.get(encoded);
    }

    /**
     * Encode the cell state as a string --- makes for easier lookups into a hashmap.
     */
    private String encode(CellState state[][]) {
      StringBuffer sb = new StringBuffer();
      for (int i=0;i<state.length;i++) for (int j=0;j<state[i].length;j++) {
        if      (state[i][j] == CellState.WALL)  sb.append("W");
        else if (state[i][j] == CellState.TRASH) sb.append("T");
        else if (state[i][j] == CellState.EMPTY) sb.append("E");
      }
      return sb.toString();
    }
  }

  /**
   * Class representing room to clean.
   */
  class Room {
    int     score     = 0;
    boolean trash[][] = new boolean[tiles_h][tiles_w]; int robot_x, robot_y;

    /**
     * Create a random room.
     */
    public Room() {
      // Randomly put trash down
      for (int y=0;y<trash.length;y++) for (int x=0;x<trash[y].length;x++) {
        if (Math.random() <= trash_pr) trash[y][x] = true; else trash[y][x] = false; 
      }

      // Randomly place the robot
      robot_x = ((int) (Math.random() * Integer.MAX_VALUE))%tiles_w;
      robot_y = ((int) (Math.random() * Integer.MAX_VALUE))%tiles_h;
    }

    /**
     * Simulate the robot cleaning this room.
     */
    public int simulate(Robot robot) {
      // Go through the number of steps for the simulation
      for (int i=0;i<steps;i++) {
        // Get the robots action based on its position and the room
        CellState state[][] = new CellState[3][3];
        for (int dy=-1;dy<=1;dy++) for (int dx=-1;dx<=1;dx++) {
          int x = robot_x + dx, y = robot_y + dy;
          if      (x < 0 || x >= trash[0].length || y < 0 || y >= trash.length) state[dy+1][dx+1] = CellState.WALL;
          else if (trash[y][x])                                                 state[dy+1][dx+1] = CellState.TRASH;
          else                                                                  state[dy+1][dx+1] = CellState.EMPTY;
        }
        Action action = robot.determineAction(state);
        histories.add(new History(robot_x, robot_y, action, trash, score));

        // Execute action
        switch (action) {
          case PICKUP_TRASH: if (trash[robot_y][robot_x]) { score += pt_clean; } else { score += pt_clean_err; }
                             trash[robot_y][robot_x] = false; break;
          case MOVE_NE: move(+1,+1); break;
          case MOVE_N:  move( 0,+1); break;
          case MOVE_NW: move(-1,+1); break;
          case MOVE_W:  move(-1, 0); break;
          case MOVE_SW: move(-1,-1); break;
          case MOVE_S:  move( 0,-1); break;
          case MOVE_SE: move(+1,-1); break;
          case MOVE_E:  move(+1, 0); break;
          case MOVE_RANDOM:          int dx = 0, dy = 0;
                                     while (Math.abs(dx) + Math.abs(dy) == 0.0) { // Has to move some direction
                                       double r = Math.random(); if (r < 0.333) dx = -1; else if (r < 0.666) dx = 0; else dx = 1;
                                              r = Math.random(); if (r < 0.333) dy = -1; else if (r < 0.666) dy = 0; else dy = 1;
                                     }
                                     move(dx,dy);
                                     break;
        }
      }
      // Total up the remaining trash
      for (int y=0;y<trash.length;y++) for (int x=0;x<trash[y].length;x++) if (trash[y][x]) score += pt_dirty;
      histories.add(new History(robot_x, robot_y, null, trash, score));

      // Return the score
      return score;
    }

    /**
     * Move the robot.  Penalize the robot for going off the screen.
     */
    private void move(int dx, int dy) {
      int x = robot_x + dx,
          y = robot_y + dy;
      if (x < 0 || x >= trash[0].length) { score += pt_wall; } else robot_x = x;
      if (y < 0 || y >= trash.length)    { score += pt_wall; } else robot_y = y;
    }

    /**
     * List of histories describing robot cleaning the room.
     */
    List<History> histories = new ArrayList<History>();

    /**
     * Create an image that describes the robot's cleaning steps for this room.
     */
    public BufferedImage historyTrace() {
      int sw = 20, sh = 20;
      BufferedImage bi  = new BufferedImage(trash[0].length*sw + 2, trash.length*sh + 2, BufferedImage.TYPE_INT_RGB); if (histories.size() == 0) return bi;
      Graphics2D    g2d = null;
      try {
        g2d = (Graphics2D) bi.getGraphics(); g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.white); g2d.fillRect(0, 0, bi.getWidth(), bi.getHeight());

        History history = histories.get(0), final_history = histories.get(histories.size() - 1);
        for (int y=0;y<trash.length;y++) {
          for (int x=0;x<trash[y].length;x++) {
            int x0 = x*sw, y0 = y*sh;
            g2d.setColor(Color.lightGray); g2d.drawRect(x0, y0, sw, sh);
            if (history.state[y][x]) { 
              g2d.setColor(Color.green); g2d.fillRect(x0 + 2, y0 + 2, sw - 4, sh - 4); 
              if (final_history.state[y][x] == false) {
                g2d.setColor(Color.white); g2d.fillRect(x0 + 4, y0 + 4, sw - 8, sh - 8); 
              }
            }
          }
        }

        for (int i=1;i<histories.size();i++) {
          history = histories.get(i); History last_history = histories.get(i-1);
          int rx0 = last_history.x * sw + sw/2,
              ry0 = last_history.y * sh + sh/2,
              rx1 =      history.x * sw + sw/2,
              ry1 =      history.y * sh + sh/2;
          if (history.score < last_history.score) { g2d.setColor(Color.red); g2d.fillOval(rx1-2,ry1-2,5,5); }
          double dx  = rx1 - rx0, dy  = ry1 - ry0; double len = Math.sqrt(dx*dx+dy*dy); 
          if (len > 0.1) {
            g2d.setColor(Color.black);
            g2d.drawLine(rx0,ry0,rx1,ry1);
            dx /= len; dy /= len;
            double pdx = -dy,       pdy = dx;
            g2d.drawLine(rx1, ry1, (int) (rx1 + pdx*3 - dx*4), (int) (ry1 + pdy*3 - dy*4));
            g2d.drawLine(rx1, ry1, (int) (rx1 - pdx*3 - dx*4), (int) (ry1 - pdy*3 - dy*4));
          }
        }
      } finally { if (g2d != null) g2d.dispose(); }
      return bi;
    }

    /**
     * Prior version of the history trace.
     */
    public BufferedImage historyTrace2() {
      int sw = 10, sh = 10, inter_h = 30, inter_w = 5; int across = 10, down = histories.size()/across + 1;
      BufferedImage bi  = new BufferedImage((trash[0].length*sw + inter_w) * across, 
                                            (trash.length*sh    + inter_h) * down, 
                                            BufferedImage.TYPE_INT_RGB);
      Graphics2D    g2d = null;
      try {
        g2d = (Graphics2D) bi.getGraphics(); g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setColor(Color.white); g2d.fillRect(0, 0, bi.getWidth(), bi.getHeight());
        for (int i=0;i<histories.size();i++) {
          History history = histories.get(i);

          int base_x = (i%across) * (trash[0].length * sw + inter_w),
              base_y = (i/across) * (trash.length    * sh + inter_h);
          int x0     = base_x, 
              y0     = base_y + inter_h;

          g2d.drawString("" + history.action + " || " + history.score, base_x+2, base_y+inter_h-2);
          for (int y=0;y<trash.length;y++) for (int x=0;x<trash[0].length;x++) {
            int cx = x0 + x*sw, cy = y0 + y*sh; int mx = cx+sw/2, my = cy+sh/2;
            g2d.setColor(Color.black); g2d.drawRect(cx, cy, sw, sh);
            if (history.state[y][x]) { g2d.setColor(Color.green); g2d.fillRect(cx+2, cy+2, sw-4, sh-4); }
            if (y == history.y && x == history.x) { g2d.setColor(Color.black); g2d.drawLine(mx-3, my, mx+3, my); g2d.drawLine(mx,my-3,mx,my+3); }
          }
        }
      } finally { if (g2d != null) g2d.dispose(); }
      return bi;
    }
  }

  /**
   * Class containing history of the robot as it cleans the room.
   */
  class History {
    int x, y, score; boolean state[][]; Action action;
    public History(int x, int y, Action action, boolean trash[][], int score) {
      this.x = x; this.y = y; this.score = score; this.action = action;
      state = new boolean[trash.length][trash[0].length];
      for (int i=0;i<state.length;i++) for (int j=0;j<state[i].length;j++) state[i][j] = trash[i][j];
    }
  }

  /**
   *
   */
  public static void main(String args[]) {
    try { new CleaningRobot(); } catch (Throwable t) { 
      System.err.println("Throwable: " + t); 
      t.printStackTrace(System.err);
    } 
  }
}

