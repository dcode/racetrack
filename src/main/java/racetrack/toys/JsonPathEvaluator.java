/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.toys;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

// import com.jayway.jsonpath.Configuration;
// import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

/**
 *
 */
public class JsonPathEvaluator extends JFrame implements CaretListener {
  JTextField  jsonpath_tf,
              class_tf;
  JTextArea   input_ta,
              output_ta;
  JScrollPane output_scroll;

  /**
   * Constructor
   */
  public JsonPathEvaluator() {
    getContentPane().setLayout(new BorderLayout());
      JPanel top_panel = new JPanel(new BorderLayout(20,20));
      top_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "JsonPath"));

        JPanel labels = new JPanel(new GridLayout(2,1,5,5));
          labels.add(new JLabel("JsonPath"));
          labels.add(new JLabel("Return Class"));
        top_panel.add("West", labels);

        JPanel fields = new JPanel(new GridLayout(2,1,5,5));
          fields.add(jsonpath_tf = new JTextField());
          fields.add(class_tf    = new JTextField()); class_tf.setEditable(false);
        top_panel.add("Center", fields);

      getContentPane().add("North", top_panel);

      JPanel textareas = new JPanel(new GridLayout(1,2,5,5));

        JPanel input_panel = new JPanel(new BorderLayout());
          input_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Input JSON"));
          input_panel.add("Center", new JScrollPane(input_ta = new JTextArea()));
            input_ta.setLineWrap(true);
        textareas.add(input_panel);

        JPanel output_panel = new JPanel(new BorderLayout());
          output_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "JsonPath Results"));
          // output_panel.add("Center", output_scroll = new JScrollPane(output_ta = new JTextArea())); 
          output_panel.add("Center", output_ta = new JTextArea()); 
            output_ta.setEditable(false);
            output_ta.setLineWrap(true);
        textareas.add(output_panel);

      getContentPane().add("Center", textareas);

      jsonpath_tf.addCaretListener(this); input_ta.addCaretListener(this);

    setSize(800,800); setVisible(true);
  }

  /**
   * CaretListener ... run the input json against the jsonpath and print the results in the output textareas/textfields...
   */
  public void caretUpdate(CaretEvent ce) {
    try {
      Object results = JsonPath.parse(input_ta.getText()).read(jsonpath_tf.getText());
      class_tf.setText("" + results.getClass());
      output_ta.setText("" + results);
    } catch (Throwable t) {
      class_tf.setText("Throwable Caught");
      output_ta.setText("" + t);
    }

    // output_scroll.getVerticalScrollBar().setValue(0);
    // output_scroll.getHorizontalScrollBar().setValue(0);
  }

  /**
   * Main entry point... just making the jframe...
   */
  public static void main(String args[]) { new JsonPathEvaluator(); }
}


