/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.toys;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.awt.geom.Rectangle2D;

import java.io.File;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JFrame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import racetrack.util.CSVTokenConsumer;
import racetrack.util.RFC4180CSVReader;
import racetrack.util.Utils;

/**
 * Simple GameShow app...
 */
public class GameShow extends JFrame {

  /**
   * Header map
   */
  Map<String,Integer> hdr_map = null;

  /**
   * Category to values to question-and-answers
   */
  Map<String,Map<Integer,Set<QandA>>> cat_to_value_to_qandas = new HashMap<String,Map<Integer,Set<QandA>>>();

  /**
   * Values found
   */
  Set<Integer> values = new HashSet<Integer>(); List<Integer> values_sorted = new ArrayList<Integer>();

  /**
   * Categories sorted
   */
  List<String> categories_sorted = new ArrayList<String>();

  /**
   * Question and Answer Class
   */
  class QandA {
    String category, question, answer; int value;
    public QandA(String category, String question, String answer, int value) {
      this.category = category; this.question = question; this.answer = answer; this.value = value;
    }
  }

  /**
   * Constructor
   */
  public GameShow(File questions_file) {
   super("Game Show");
   try {
    new RFC4180CSVReader(questions_file, new CSVTokenConsumer() {
      public boolean consume(String tokens[], String line, int line_no) {
        if (tokens.length != 4) { System.err.println("Skipping Line " + line_no + " \"" + line + "\""); return true; }

        if (hdr_map == null) {
          // Map the header names to integers
          hdr_map = new HashMap<String,Integer>();
          for (int i=0;i<tokens.length;i++) hdr_map.put(tokens[i], i);

          // Check the sanity of the data
          if (hdr_map.keySet().contains("category") == false) throw new RuntimeException("Missing Header \"category\"");
          if (hdr_map.keySet().contains("question") == false) throw new RuntimeException("Missing Header \"question\"");
          if (hdr_map.keySet().contains("answer")   == false) throw new RuntimeException("Missing Header \"answer\"");
          if (hdr_map.keySet().contains("value")    == false) throw new RuntimeException("Missing Header \"value\"");

        } else {

          String category  = tokens[hdr_map.get("category")],
                 question  = tokens[hdr_map.get("question")],
                 answer    = tokens[hdr_map.get("answer")],
                 value_str = tokens[hdr_map.get("value")];
          int    value     = Integer.parseInt(value_str); 

          if (cat_to_value_to_qandas.containsKey(category) == false) cat_to_value_to_qandas.put(category, new HashMap<Integer,Set<QandA>>());
          if (cat_to_value_to_qandas.get(category).containsKey(value) == false) cat_to_value_to_qandas.get(category).put(value, new HashSet<QandA>());
          cat_to_value_to_qandas.get(category).get(value).add(new QandA(category,question,answer,value));
          values.add(value);
        }
        return true;
      }
      public void commentLine(String line) { }
    } );

    // Organize the information for render
    values_sorted.addAll(values); Collections.sort(values_sorted);
    categories_sorted.addAll(cat_to_value_to_qandas.keySet());

   } catch (IOException ioe) { System.err.println("IOException: " + ioe); ioe.printStackTrace(System.err); }

   getContentPane().add(new GameShowComponent());

   setSize(800,600); setVisible(true);
  }

  // Mode of the application
  enum GameMode { BOARD, TRANS_QUESTION, QUESTION, TRANS_ANSWER, ANSWER, TRANS_BOARD };

  // Current game mode
  GameMode game_mode = GameMode.BOARD;

  // Mode step increment
  int  game_mode_step;

  // Colors .... brewer colors
  Color bg_color            = new Color( 94,  60, 153),
        line_color          = new Color(178, 171, 210),
        cat_color           = new Color(230,  97,   1),
        value_color         = new Color(253, 184,  99),
        question_color      = new Color(230,  97,   1),
        question_done_color = new Color(178, 171, 210),
        answer_color        = new Color(253, 184,  99);

  // Font metrics
  String font_name_str = "Serif";
  int    cat_sz        = 32,
         value_sz      = 28,
         question_sz   = 32,
         answer_sz     = 32,
         min_font_sz   = 16;

  // Questions that are already answered
  Map<String,Set<Integer>> cell_done = new HashMap<String,Set<Integer>>();

  // Current question and answer
  QandA qanda_selected = null;

  /**
   * Primary game show component
   */
  class GameShowComponent extends JComponent implements MouseListener {
    /**
     * Construct -- add this class as a mouse listener
     */
    public GameShowComponent() { addMouseListener(this); }

    public void mousePressed  (MouseEvent me) { }
    public void mouseReleased (MouseEvent me) { }
    public void mouseEntered  (MouseEvent me) { }
    public void mouseExited   (MouseEvent me) { }

    public void mouseClicked  (MouseEvent me) { 
      if        (game_mode == GameMode.BOARD)    {
        if (geom_to_qanda != null) {
          Iterator<Rectangle2D> it = geom_to_qanda.keySet().iterator(); while (it.hasNext()) {
            Rectangle2D geom = it.next(); if (geom.contains(me.getX(),me.getY())) {
              qanda_selected = randomPick(geom_to_qanda.get(geom));
              if (cell_done.containsKey(qanda_selected.category) == false) cell_done.put(qanda_selected.category, new HashSet<Integer>());
              cell_done.get(qanda_selected.category).add(qanda_selected.value);
              game_mode = GameMode.TRANS_QUESTION; game_mode_step = 0; repaint();
            }
          }
        }
      } else if (game_mode == GameMode.QUESTION) { game_mode = GameMode.TRANS_ANSWER; game_mode_step = 0; repaint();
      } else if (game_mode == GameMode.ANSWER)   { game_mode = GameMode.TRANS_BOARD;  game_mode_step = 0; repaint();
      }
    }

    // Randomly pick a qanda from a set of them
    private QandA randomPick(Set<QandA> set) {
      if (set.size() == 1) return set.iterator().next();
      List<QandA> as_list = new ArrayList<QandA>(); as_list.addAll(set);
      int i = ((int) (Math.random() * 10*as_list.size()))%as_list.size();
      return as_list.get(i);
    }

    /**
     * Render the component
     */
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2d.setColor(bg_color); g2d.fillRect(0,0,getWidth(),getHeight());

      switch (game_mode) {
        case BOARD:          renderBoard(g2d);                  break;
        case TRANS_QUESTION: renderTransitionToQuestion(g2d);   break;
        case QUESTION:       renderQuestion(g2d);               break;
        case TRANS_ANSWER:   renderTransitionToAnswer(g2d);     break;
        case ANSWER:         renderAnswer(g2d);                 break;
        case TRANS_BOARD:    renderTransitionToBoard(g2d);      break;
      }
    }

    // Geometry to the Q-and-A set
    Map<Rectangle2D,Set<QandA>> geom_to_qanda = null;

    /**
     * Render the board.
     */
    protected void renderBoard(Graphics2D g2d) {
      g2d.setFont(new Font(font_name_str, Font.PLAIN, value_sz));
      int txt_h = Utils.txtH(g2d,"0123456789");

      int cell_w = getWidth() / categories_sorted.size();
      int hdr_h  = 80;
      int cell_h = 3*txt_h;

      g2d.setColor(line_color);
      for (int i=0;i<categories_sorted.size();i++) g2d.drawLine(cell_w*i, 0,                 cell_w*i,    getHeight());
      for (int i=0;i<values_sorted.size()+1;i++)   g2d.drawLine(0,        hdr_h + i*cell_h,  getWidth(),  hdr_h + i*cell_h);

      geom_to_qanda = new HashMap<Rectangle2D,Set<QandA>>();

      // Render the value cells
      for (int i=0;i<categories_sorted.size();i++) {
        for (int j=0;j<values_sorted.size();j++) {
          int x = i*cell_w, y = hdr_h + j*cell_h; boolean answered_already = false;
          if (cell_done.containsKey(categories_sorted.get(i)) &&
              cell_done.get(categories_sorted.get(i)).contains(values_sorted.get(j))) { g2d.setColor(question_done_color); answered_already = true; }
          else g2d.setColor(question_color);

          if (cat_to_value_to_qandas.get(categories_sorted.get(i)).containsKey(values_sorted.get(j))) {
            String value_str = "" + values_sorted.get(j);
            g2d.drawString(value_str, x + cell_w/2 - Utils.txtW(value_str)/2, y + cell_h/2 + Utils.txtH(value_str)/2);
            Rectangle2D geom = new Rectangle2D.Double(x, y, cell_w, cell_h);
            if (answered_already == false) geom_to_qanda.put(geom, cat_to_value_to_qandas.get(categories_sorted.get(i)).get(values_sorted.get(j)));
          }
        }
      }

      // Render the category cells
      g2d.setColor(cat_color);
      for (int i=0;i<categories_sorted.size();i++) {
        String cat_str = categories_sorted.get(i);
        int    x       = i*cell_w,
               y       = 0;
        fitAndRenderString(g2d, cat_str, x, y, cell_w, hdr_h, font_name_str, Font.BOLD, cat_sz);
      }
    }

    /**
     * Render the question.
     */
    protected void renderQuestion(Graphics2D g2d) {
      String str = qanda_selected.question;
      g2d.setColor(question_color); g2d.setFont(new Font(font_name_str, Font.BOLD, question_sz)); int txt_h = Utils.txtH(g2d,str);

      int no_of_lines = 1; while (no_of_lines < 8) {
        String lines[] = splitInto(str, no_of_lines); int max_txt_w = maxLineWidth(g2d,lines); if (max_txt_w < getWidth()-4) {
          int y = getHeight()/2; if (no_of_lines%2 == 1) { y += txt_h/2; y -= (lines.length/2)*txt_h; } else y -= (lines.length/2)*txt_h;
          for (int i=0;i<lines.length;i++) { g2d.drawString(lines[i], getWidth()/2 - Utils.txtW(g2d,lines[i])/2, y); y += txt_h; }
          break;
        } else no_of_lines++;
      }
    }

    /**
     * Render the answer (with the questions as context).
     */
    protected void renderAnswer(Graphics2D g2d) {
      g2d.setFont(new Font(font_name_str, Font.BOLD, question_sz)); int txt_h = Utils.txtH(g2d,"0123");

      int no_of_lines = 0; String str = null;

      g2d.setColor(question_color); str = qanda_selected.question; 
      no_of_lines = 1; while (no_of_lines < 8) {
        String lines[] = splitInto(str, no_of_lines); int max_txt_w = maxLineWidth(g2d,lines); if (max_txt_w < getWidth()-4) {
          int y = getHeight()/2 - txt_h/2; y -= lines.length*txt_h;
          for (int i=0;i<lines.length;i++) { g2d.drawString(lines[i], getWidth()/2 - Utils.txtW(g2d,lines[i])/2, y); y += txt_h; }
          break;
        } else no_of_lines++;
      }

      g2d.setColor(answer_color); str = qanda_selected.answer; 
      no_of_lines = 1; while (no_of_lines < 8) {
        String lines[] = splitInto(str, no_of_lines); int max_txt_w = maxLineWidth(g2d,lines); if (max_txt_w < getWidth()-4) {
          int y = getHeight()/2 + 3*txt_h/2;
          for (int i=0;i<lines.length;i++) { g2d.drawString(lines[i], getWidth()/2 - Utils.txtW(g2d,lines[i])/2, y); y += txt_h; }
          break;
        } else no_of_lines++;
      }
    }

    /**
     * Delay in millis on the animation for the transitions.
     */
    long animation_ms = 30L;

    /**
     * Render the transition to the question animation.
     */
    protected void renderTransitionToQuestion(Graphics2D g2d) {
      g2d.setColor(Color.white); /* g2d.drawString("Transition to question..." + game_mode_step, 20, 20); // Place holder */
      game_mode_step++; if (game_mode_step >= 10) game_mode = GameMode.QUESTION;
      try { Thread.sleep(animation_ms); } catch (InterruptedException ie) { } repaint();
    }

    /**
     * Render the transition to the answer animation.
     */
    protected void renderTransitionToAnswer(Graphics2D g2d) {
      g2d.setColor(Color.white); /* g2d.drawString("Transition to answer... " + game_mode_step, 20, 20); // Place holder */
      game_mode_step++; if (game_mode_step >= 10) game_mode = GameMode.ANSWER;
      try { Thread.sleep(animation_ms); } catch (InterruptedException ie) { } repaint();
    }

    /**
     * Render the transition to the board animation.
     */
    protected void renderTransitionToBoard(Graphics2D g2d) {
      g2d.setColor(Color.white); /* g2d.drawString("Transition to board... " + game_mode_step, 20, 20); // Place holder */
      game_mode_step++; if (game_mode_step >= 10) game_mode = GameMode.BOARD;
      try { Thread.sleep(animation_ms); } catch (InterruptedException ie) { } repaint();
    }


    /**
     * Fit and render a string based on the specified constraints
     */
    protected void fitAndRenderString(Graphics2D g2d, String str, int x, int y, int w, int h, String font, int font_props, int font_sz) {
      while (font_sz > min_font_sz) {
        g2d.setFont(new Font(font, font_props, font_sz));
        int txt_h = Utils.txtH(g2d,str);

        if        (Utils.txtW(g2d,str) <  w - 4) {
          g2d.drawString(str, x + w/2 - Utils.txtW(g2d,str)/2, y + h/2 + Utils.txtH(g2d,str));
          return;
        } else if (str.indexOf(" ")    >= 0)     {
          int possible_splits = (new StringTokenizer(str," ")).countTokens();
          for (int i=2;i<possible_splits+1;i++) {
            String lines[] = splitInto(str, i);
            int    max_txt_w = Utils.txtW(g2d,lines[0]); for (int j=1;j<lines.length;j++) { int txt_w = Utils.txtW(g2d,lines[j]); if (txt_w > max_txt_w) max_txt_w = txt_w; }
            if (max_txt_w < w - 4) {
              int txt_h_sum = Utils.txtH(g2d,lines[0]); for (int j=1;j<lines.length;j++) txt_h_sum += Utils.txtH(g2d, lines[j]) + 2;
              if (txt_h_sum < h) {

                for (int j=0;j<lines.length;j++) { g2d.drawString(lines[j], 
                                                                  x + w/2 - Utils.txtW(g2d,lines[j])/2, 
                                                                  (y + h - txt_h_sum - 4) + (j+1)*(txt_h+2)); }

                return;
              }
            }
          }
        }
        font_sz--;
      }
      // give up :(
    }
  }

  /**
   * Max line width of an array of strings.
   */
  protected int maxLineWidth(Graphics2D g2d, String strs[]) {
    int max_w = Utils.txtW(g2d,strs[0]);
    for (int i=1;i<strs.length;i++) { int w = Utils.txtW(g2d,strs[i]); if (w > max_w) max_w = w; }
    return max_w;
  }

  /**
   * Split a line into a specified number of parts with roughly the same length.
   */
  protected String[] splitInto(String str, int parts) {
    String strs[] = new String[parts]; for (int i=0;i<strs.length;i++) strs[i] = "";
    if (parts == 1) { strs[0] = str; return strs; }

    StringTokenizer st = new StringTokenizer(str, " \t\r\n"); List<String> ls = new ArrayList<String>(); while (st.hasMoreTokens()) ls.add(st.nextToken());

    int goal_length = str.length()/parts; int parts_i = 0, ls_i = 0;
    while (ls_i < ls.size()) {
      if (strs[parts_i].length() > goal_length) { parts_i++; }
      if (strs[parts_i].length() > 0)           { strs[parts_i] += " "; }
      strs[parts_i] += " " + ls.get(ls_i++);
    }

    /* // Debug
    System.err.println(); System.err.println("\"" + str + "\"");
    for (int i=0;i<strs.length;i++) System.err.println(i + " : " + strs[i]);
    */

    return strs;
  }

  /**
   * Main 
   */
  public static void main(String args[]) {
    GameShow game_show = new GameShow(new File(args[0]));
  }
}

