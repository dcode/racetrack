/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.toys;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSlider;
import javax.swing.JTextField;

import racetrack.util.Utils;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

/**
 *
 */
public class FLLSimEV3Classroom extends JFrame implements SimulationStepRenderConsumer {
  // Field selection
  JComboBox<String> field_cb;

  // Motor error slider
  JSlider motor_error_slider,

  // Light sensor error slider
          light_sensor_error_slider,

  // Gyro sensor error slider
          gyro_sensor_error_slider,

  // Simulation speed slider ... milliseconds between renders
          simulation_speed_slider,

  // Simulation trials for Monte Carlo Simulation
          trials_slider;

  // Simulation mode radio button
  JRadioButton simulation_mode_rb,

  // Monte Carlo mode radio button
               monte_carlo_mode_rb;

  // Texfield displaying filename
  JTextField   file_tf;

  // Component for configuring robot
  RobotConfigurationComponent robot_configuration_component;

  // Just a component to display an image 
  SimpleImageComponent simple_image_component;

  /**
   *
   */
  public FLLSimEV3Classroom() {
    // Field Panel
    JPanel field_panel         = new JPanel();
    field_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Field"));
      field_panel.add(field_cb = new JComboBox<String>(FLLFieldSetup.fieldStrings()));

    // Robot Configuration Panel
    JPanel robot_config_panel  = new JPanel(new BorderLayout(10,10));
    robot_config_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Robot Configuration"));
      robot_config_panel.add("Center", robot_configuration_component = new RobotConfigurationComponent());

    // Error Panel
    JPanel error_panel         = new JPanel(); error_panel.setLayout(new BorderLayout(10,10));
    error_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Error Settings"));
      JPanel labels = new JPanel(new GridLayout(3,1,5,5));
        labels.add(new JLabel("Motor"));
        labels.add(new JLabel("Light Sensor"));
        labels.add(new JLabel("Gyro Sensor"));
        error_panel.add("West", labels);
      JPanel sliders = new JPanel(new GridLayout(3,1,5,5));
        sliders.add(motor_error_slider        = new JSlider(0,1000,0));
        sliders.add(light_sensor_error_slider = new JSlider(0,1000,0));
        sliders.add(gyro_sensor_error_slider  = new JSlider(0,1000,0));
        error_panel.add("Center", sliders);

    // Simulation mode and settings panel ... file to parse, mode (simulation or monte carlo), number of runs, simulation speed
    JPanel mode_and_file_panel = new JPanel(); JButton bt;
    mode_and_file_panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Simulation Settings"));
      labels  = new JPanel(new GridLayout(0,1,5,5));
      labels.add(bt = new JButton("Set EV3 File")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setEV3File(); } } );
      labels.add(new JLabel("Simulation Mode"));
      labels.add(new JLabel("Simulation Speed"));
      labels.add(new JLabel());
      labels.add(new JLabel("Monte Caro"));
      labels.add(new JLabel("Simulation Trials"));
      labels.add(new JLabel());
      labels.add(new JLabel("Re-run"));
      mode_and_file_panel.add("West", labels);

      sliders = new JPanel(new GridLayout(0,1,5,5)); ButtonGroup bg = new ButtonGroup();
      sliders.add(file_tf = new JTextField()); file_tf.setEditable(false);
      sliders.add(simulation_mode_rb = new JRadioButton("Sim", true));       bg.add(simulation_mode_rb);
      sliders.add(simulation_speed_slider = new JSlider(0,50,5));
      sliders.add(new JLabel());
      sliders.add(monte_carlo_mode_rb = new JRadioButton("Monte Carlo", false));     bg.add(monte_carlo_mode_rb);
      sliders.add(trials_slider = new JSlider(1,20,5));
      sliders.add(new JLabel());
      sliders.add(go_bt = new JButton("Go!")); go_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { go(); } } );
        go_bt.setEnabled(false);

      mode_and_file_panel.add("Center", sliders);

    // Accumulate the western panel (field, robot, error, settings)
    JPanel west_panel          = new JPanel(new BorderLayout(9,10));
    west_panel.add("North", field_panel);
      JPanel west2_panel = new JPanel(new BorderLayout(10,10)); west_panel.add("Center", west2_panel);
      west2_panel.add("North", robot_config_panel);
        JPanel west3_panel = new JPanel(new BorderLayout(10,10)); west2_panel.add("Center", west3_panel);
        west3_panel.add("North",  error_panel);
        west3_panel.add("Center", mode_and_file_panel);

    getContentPane().add("West",   west_panel);
    getContentPane().add("Center", simple_image_component = new SimpleImageComponent());

    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { System.exit(0); } } );

    // Display it...
    pack(); setSize(1024,1024); setVisible(true);
  }

  // File chooser for ev3 file
  JFileChooser file_chooser = new JFileChooser(".");

  // EV3 Classroom File
  File ev3_file = null;

  /**
   * Show the file chooser ... if it comes back successfull, put the filename into the textfield
   */
  protected void setEV3File() {
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      ev3_file = file_chooser.getSelectedFile();
      file_tf.setText(ev3_file.getName());
      go_bt.setEnabled(true);
    }
  }

  // "Go" button
  JButton go_bt;

  // Simple thread control...
  boolean running_simulation = false;
  boolean stop_simulation    = true;

  /**
   * Run the simulation / monte carlo trials
   */
  protected void go() {
   // Button will be dual use...
   if (running_simulation) { stop_simulation = true; go_bt.setText("Go!"); return; }

   try {
    // Make sure the ev3 file exists
    if (ev3_file.exists() == false) {
      JOptionPane.showMessageDialog(this, "File \"" + ev3_file.getName() + "\" Doesn't Exist", "File Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // Get all of the settings...
    FLLFieldSetup.FLLYear field_enum         = FLLFieldSetup.stringToFLLYear((String) field_cb.getSelectedItem());
    double                motor_error        = motor_error_slider.        getValue()/500.0,
                          light_sensor_error = light_sensor_error_slider. getValue()/500.0,
                          gyro_sensor_error  = gyro_sensor_error_slider.  getValue()/500.0;
    int                   trials             = trials_slider.             getValue();

    // Create the configurations
    FLLFieldSetup          field_setup  = FLLFieldSetup.getInstance(field_enum);
    FLLRobotConfiguration  robot_config = new FLLRobotConfiguration(robot_configuration_component.getTrackHalf(),
                                                                    robot_configuration_component.getSensorXOffset(1),
                                                                    robot_configuration_component.getSensorYOffset(1),
                                                                    robot_configuration_component.getSensorXOffset(2),
                                                                    robot_configuration_component.getSensorYOffset(2));
    FLLErrorConfig         error_config = new FLLErrorConfig(motor_error, light_sensor_error, gyro_sensor_error);

    // Parse the ev3 file
    List<String> lines;

    if      (ev3_file.getName().toLowerCase().endsWith(".lmsp")) lines = parseEV3ClassroomFile(ev3_file);
    else if (ev3_file.getName().toLowerCase().endsWith(".txt"))  lines = readTextFile(ev3_file);
    else throw new RuntimeException("Do Not Know How To Parse \"" + ev3_file.getName() + "\"");

    // Run the simulation...
    if        (simulation_mode_rb.isSelected())  {
      go_bt.setText("Cancel");
      running_simulation = true; stop_simulation = false; // Simulation control
      (new Thread(new SimulationRunnable(field_setup, robot_config, lines, error_config, this))).start();
    } else if (monte_carlo_mode_rb.isSelected()) {
      try {
        go_bt.setText("Cancel");
        List<FLLMonteCarlo> monte_carlos = new ArrayList<FLLMonteCarlo>(); double time_sum = 0.0, time_min = Double.POSITIVE_INFINITY, time_max = Double.NEGATIVE_INFINITY;
        for (int i=0;i<trials;i++) { 
          monte_carlos.add(new FLLMonteCarlo(field_setup, robot_config, lines ,error_config ,null)); 
          double time = monte_carlos.get(monte_carlos.size()-1).simulationTime();
          time_sum += time;
          if (time < time_min) time_min = time;
          if (time > time_max) time_max = time;
        }
        BufferedImage bi = FLLMonteCarlo.createCompositeImage(monte_carlos,field_setup);
        String info_str = "Min:" + time_min + " ... Avg:" + (time_sum/monte_carlos.size()) + " ... Max:" + time_max;
        simple_image_component.setImage(bi, info_str);
      } finally { go_bt.setText("Go!"); }
    }
   } catch (IOException ioe) {
      JOptionPane.showMessageDialog(this, ioe.toString(), "IOException", JOptionPane.ERROR_MESSAGE);
   } finally { }
  }

  /**
   * Simulation renderer -- implementation for SimulationStepRenderConsumer
   */
  public boolean simulationRender(BufferedImage bi, int sim_step, double sim_time) {
    simple_image_component.setImage(bi);
    int delay = simulation_speed_slider.getValue();
    if (delay > 0) { try { Thread.sleep(delay); } catch (InterruptedException ie) { } }
    if (stop_simulation) return false; else return true;
  }
  
  /**
   * Simulation thread
   */
  class SimulationRunnable implements Runnable {
    FLLFieldSetup                field_setup;
    FLLRobotConfiguration        robot_config;
    List<String>                 lines;
    FLLErrorConfig               error_config;
    SimulationStepRenderConsumer render_consumer;

    public SimulationRunnable(FLLFieldSetup                my_field_setup,
                              FLLRobotConfiguration        my_robot_config,
                              List<String>                 my_lines,
                              FLLErrorConfig               my_error_config,
                              SimulationStepRenderConsumer my_render_consumer) {
      this.field_setup       = my_field_setup;
      this.robot_config      = my_robot_config;
      this.lines             = my_lines;
      this.error_config      = my_error_config;
      this.render_consumer   = my_render_consumer;
    }
    public void run() { 
      try { new FLLMonteCarlo(field_setup, robot_config, lines, error_config, render_consumer); 
      } finally { stop_simulation = true; running_simulation = false; go_bt.setText("Go!"); } 
    }
  }

  /**
   * Read in a text file of instructions
   */
  public static List<String> readTextFile(File file) throws IOException {
    List<String> lines = new ArrayList<String>();
    BufferedReader in = new BufferedReader(new FileReader(file));
    String line; while ((line = in.readLine()) != null) lines.add(line);
    in.close();
    return lines;
  }

  /**
   * Parse the EV3 Classroom file...
   */
  public static List<String> parseEV3ClassroomFile(File file) throws IOException {
    List<String> lines = new ArrayList<String>();
    ZipInputStream zip_in = new ZipInputStream(new FileInputStream(file)); ZipEntry zip_entry = null;
      while ((zip_entry = zip_in.getNextEntry()) != null) {
        if (zip_entry.getName().equals("scratch.sb3")) {
          ZipInputStream zip2_in = new ZipInputStream(zip_in); ZipEntry zip2_entry = null;
          while ((zip2_entry = zip2_in.getNextEntry()) != null) {
            if (zip2_entry.getName().equals("project.json")) {
              BufferedReader in = new BufferedReader(new InputStreamReader(zip2_in)); StringBuffer sb = new StringBuffer();
              String line = null; while ((line = in.readLine()) != null) {
                if (sb.length() > 0) sb.append("\n");
                sb.append(line);
              }
              lines.addAll(jsonToInstructions(sb.toString()));
            }
          }
        }
      }
    zip_in.close();

    // Dump the program to console
    for (int i=0;i<lines.size();i++) System.err.println(lines.get(i));

    return lines;
  }

  /**
   * Build escaped-sequence path for jsonpath queries.
   */
  private static String escape(String str) {
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<str.length();i++) {
      char c = str.charAt(i);
      if (c == ',')  sb.append("\\" + c); // wtf ... why does comma require escaping?
      else           sb.append(c);
    }
    return sb.toString();
  }

  /**
   * Convert a json string into the simulation instructions
   */
  private static List<String> jsonToInstructions(String json_str) {
    // Parse the JSON String
    DocumentContext dc = JsonPath.parse(json_str);

    // Get the blocks list ... to search for the program start block
    List blocks = (List) dc.read("$.targets[1].blocks.*~");

    // Find the "when program starts block"...
    for (int i=0;i<blocks.size();i++) {
      // If found, follow the next blocks for the entire instruction sequence
      if (JsonPath.parse(blocks.get(i)).read("$.opcode").equals("ev3events_whenProgramStarts")) {
        // Parse the block locally
        DocumentContext start_block = JsonPath.parse(blocks.get(i));
        String next   = start_block.read("$.next");
        List<String> instructions = followInstructionThread(dc, next);
        return instructions;
      }
    }

    return new ArrayList<String>();
  }

  private static List<String> followInstructionThread(DocumentContext dc, String next) {
    List<String> instructions = new ArrayList<String>();

        while (next != null) {
          String             next_path = "$.targets[1].blocks[\'" + escape(next) + "\']";
          Map<String,String> next_map  = dc.read(next_path);

          String opcode = dc.read(next_path + ".opcode");
                 next   = dc.read(next_path + ".next");

          //
          // Set variable to
          //
          if        (opcode.equals("data_setvariableto")) {
            String variable     = dc.read(next_path + ".fields.VARIABLE[0]"),
                   set_to_value = dc.read(next_path + ".inputs.VALUE[1][1]");
            instructions.add(variable + " = " + set_to_value);

          //
          // Repeat block
          //
          } else if (opcode.equals("control_repeat")) {
            int     times = Integer.parseInt((String) dc.read(next_path + ".inputs.TIMES[1][1]"));
            String loop_start = dc.read(next_path + ".inputs.SUBSTACK[1]");

            instructions.add("#\n# Looping " + times + "\n#");

            List<String> loop_instructions = followInstructionThread(dc, loop_start);
            for (int i=0;i<times;i++) {
              for (int j=0;j<loop_instructions.size();j++) {
                instructions.add(loop_instructions.get(j));
              }
            }

          //
          // End Program Block
          //
          } else if (opcode.equals("ev3control_stop")) {

          //
          // Custom block
          //
          } else if (next_map.containsKey("mutation"))  {
            String proccode = dc.read(next_path + ".mutation.proccode"); 
            List<String> inputs_list = dc.read(next_path + ".inputs.*[1][1]");

            StringBuffer sb = new StringBuffer();
            sb.append((new StringTokenizer(proccode)).nextToken());
            for (int i=0;i<inputs_list.size();i++) sb.append(" " + inputs_list.get(i));
            instructions.add(sb.toString());

          //
          // Throwable...
          //
          } else throw new RuntimeException("Do Not Understand OpCode \"" + opcode + "\"");
        }

    return instructions;
  }

  /**
   * Main entry point... instantiate the class
   */
  public static void main(String args[]) {
    try { new FLLSimEV3Classroom(); } catch (Throwable t) { System.err.println("Throwable: " + t); t.printStackTrace(System.err); }
  }
}

  /**
   * Really simple image display component
   */
  class SimpleImageComponent extends JComponent {
    BufferedImage bi = null; String info_str = null;
    public void setImage(BufferedImage bi, String info_str) { this.bi = bi; this.info_str = info_str; repaint(); }
    public void setImage(BufferedImage bi) { setImage(bi,null); }
    public void paintComponent(Graphics g) { Graphics2D g2d = (Graphics2D) g; if (bi != null) {
      double hscale = 1.0, vscale = 1.0;
      if (bi.getWidth()  > getWidth())  hscale = ((double) getWidth())  /bi.getWidth();
      if (bi.getHeight() > getHeight()) vscale = ((double) getHeight()) /bi.getHeight();
      double scale = hscale; if (scale > vscale) scale = vscale;

      g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      g2d.drawImage(bi,
                    0,0,(int) (bi.getWidth()*scale),(int) (bi.getHeight()*scale),
                    0,0,bi.getWidth(),bi.getHeight(),
                    null); 
      if (info_str != null) {
        // g2d.setColor(Color.white); g2d.drawString(info_str, 5, 5 + Utils.txtH(g2d,info_str));
        //                            g2d.drawString(info_str, 6, 6 + Utils.txtH(g2d,info_str));
        g2d.setColor(Color.black); g2d.drawString(info_str, 4, 4 + Utils.txtH(g2d,info_str));
      }
    } }
  }

  /**
   *
   */
  class RobotConfigurationComponent extends JComponent implements MouseListener, MouseMotionListener {
     /**
      * Inches from the center point to each wheel
      */
     private double track_half_in = 2.0;
     public double getTrackHalf() { return track_half_in; }

     /**
      * Sensor 1 and 2 x/y coordinate offsets from the center point
      */
     private double s1_x_off = -1.0, 
                    s1_y_off =  1.0,
                    s2_x_off =  1.0, 
                    s2_y_off =  1.0;
     // Flipped due to the orientation of the simulator
     public double getSensorXOffset(int sensor_num) {
       if (sensor_num == 1) return s1_y_off;
       else                 return s2_y_off;
     }
     // Flipped due to the orientation of the simulator
     public double getSensorYOffset(int sensor_num) {
       if (sensor_num == 1) return s1_x_off;
       else                 return s2_x_off;
     }

    /**
     * Constructor
     */
    public RobotConfigurationComponent() {
      setPreferredSize(new Dimension(256, 256));
      addMouseListener(this);
      addMouseMotionListener(this);
    }

    public void mousePressed  (MouseEvent me) { 
      if        (wheel_p_rect.contains(me.getX(), me.getY()) ||
                 wheel_n_rect.contains(me.getX(), me.getY())) { moving_wheels  = true;
      } else if (sensor1_rect.contains(me.getX(), me.getY())) { moving_sensor1 = true;
      } else if (sensor2_rect.contains(me.getX(), me.getY())) { moving_sensor2 = true; }
    }
    public void mouseReleased (MouseEvent me) { moving_wheels = moving_sensor1 = moving_sensor2 = false; }
    public void mouseClicked  (MouseEvent me) { }
    public void mouseEntered  (MouseEvent me) { }
    public void mouseExited   (MouseEvent me) { moving_wheels = moving_sensor1 = moving_sensor2 = false; }
    public void mouseMoved    (MouseEvent me) { }
    public void mouseDragged  (MouseEvent me) { 

      int x_axis_ycoord = getHeight() /2,
          y_axis_xcoord = getWidth()  /2;

      if        (moving_wheels)  {
        track_half_in = Math.abs(-me.getX() + y_axis_xcoord)/scale;
      } else if (moving_sensor1) {
        s1_x_off = (-me.getX() + y_axis_xcoord)/scale;
        s1_y_off = (-me.getY() + x_axis_ycoord)/scale;
      } else if (moving_sensor2) {
        s2_x_off = (-me.getX() + y_axis_xcoord)/scale;
        s2_y_off = (-me.getY() + x_axis_ycoord)/scale;
      }
      repaint();
    }

    boolean moving_wheels  = false,
            moving_sensor1 = false,
            moving_sensor2 = false;

    /**
     * Scale of the rendering
     */
    double scale = 40.0;

    /**
     * various geometries
     */
    Rectangle2D wheel_p_rect, wheel_n_rect, sensor1_rect, sensor2_rect;

    /**
     * Render the component
     */
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g;

      int x_axis_ycoord = getHeight() /2,
          y_axis_xcoord = getWidth()  /2;

      g2d.setColor(Color.lightGray);
      g2d.drawLine(0,            x_axis_ycoord, getWidth(),    x_axis_ycoord);
      g2d.drawLine(y_axis_xcoord, 0,            y_axis_xcoord, getHeight());

      int x,y;

      x  = y_axis_xcoord + ((int) (scale * track_half_in));
      wheel_p_rect = new Rectangle2D.Double(x - 6, x_axis_ycoord - 24, 12, 48);
      x  = y_axis_xcoord - ((int) (scale * track_half_in));
      wheel_n_rect = new Rectangle2D.Double(x - 6, x_axis_ycoord - 24, 12, 48);

      x = y_axis_xcoord - ((int) (scale * s1_x_off)); y = x_axis_ycoord - ((int) (scale * s1_y_off));
      sensor1_rect = new Rectangle2D.Double(x - 6, y - 6, 12, 12);
      x = y_axis_xcoord - ((int) (scale * s2_x_off)); y = x_axis_ycoord - ((int) (scale * s2_y_off));
      sensor2_rect = new Rectangle2D.Double(x - 6, y - 6, 12, 12);

      g2d.setColor(Color.blue);
      g2d.fill(wheel_p_rect);
      g2d.fill(wheel_n_rect);

      g2d.setColor(Color.green);
      g2d.fill(sensor1_rect);
      g2d.fill(sensor2_rect);

      g2d.setColor(Color.black); 
      g2d.draw(wheel_p_rect);
      g2d.draw(wheel_n_rect);
      g2d.draw(sensor1_rect);
      g2d.draw(sensor2_rect);

      g2d.drawString("1", (int) (sensor1_rect.getCenterX() - Utils.txtW(g2d, "1")/2), (int) (sensor1_rect.getY() - 2));
      g2d.drawString("2", (int) (sensor2_rect.getCenterX() - Utils.txtW(g2d, "2")/2), (int) (sensor2_rect.getY() - 2));

      String str = "" + track_half_in;
      g2d.drawString(str, y_axis_xcoord - Utils.txtW(g2d, str)/2, x_axis_ycoord);

      str = "( " + s1_x_off + " , " + s1_y_off + " )";
      g2d.drawString(str, 5, 5 + Utils.txtH(g2d, str));

      str = "( " + s2_x_off + " , " + s2_y_off + " )";
      g2d.drawString(str, getWidth() - 5 - Utils.txtW(g2d, str), 5 + Utils.txtH(g2d, str));

      g2d.drawRect(0,0,getWidth()-1,getHeight()-1);

      g2d.setStroke(new BasicStroke(3.0f));
      int y_arrow = getHeight()/4;
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - Utils.txtH(g2d, "0") - 2, 
                   y_axis_xcoord,      x_axis_ycoord - y_arrow);
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - y_arrow,
                   y_axis_xcoord - 10, x_axis_ycoord - y_arrow + 10);
      g2d.drawLine(y_axis_xcoord,      x_axis_ycoord - y_arrow,
                   y_axis_xcoord + 10, x_axis_ycoord - y_arrow + 10);
    }
  }

