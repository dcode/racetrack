/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.toys;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;

import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.swing.JComponent;
import javax.swing.JFrame;

import javax.imageio.ImageIO;

import racetrack.util.Utils;

/**
 * Base Map + Obstacles
 */
class FLLFieldSetup {
  /**
   * Base image of the field
   */
  BufferedImage field_bi;

  /**
   * Dots per inch in x
   */
  double        dpi_x,

  /**
   * Dots per inch in y
   */
                dpi_y;

  /**
   * Obstacles on the field ... in inches...
   */
  List<Shape>   obstacles;

  /** 
   * Enumeration for specifying the field
   */
  enum FLLYear { InchesReference, Replay_2020 , Cargo_2021 };

  /**
   * Return an array of strings representing the fields.
   */
  public static String[] fieldStrings() {
    String strs[] = new String[3];
    strs[0] = "" + FLLYear.InchesReference;
    strs[1] = "" + FLLYear.Replay_2020;
    strs[2] = "" + FLLYear.Cargo_2021;
    return strs;
  }

  /**
   * Transform a string field into its enumeration value
   */
  public static FLLYear stringToFLLYear(String str) {
    if      (str.equals("" + FLLYear.InchesReference)) return FLLYear.InchesReference;
    else if (str.equals("" + FLLYear.Replay_2020))     return FLLYear.Replay_2020;
    else if (str.equals("" + FLLYear.Cargo_2021))      return FLLYear.Cargo_2021;
    else throw new RuntimeException("Do Not Understand Field String \"" + str + "\"");
  }

  /**
   * Return an instance of a specific year.
   */
  public static FLLFieldSetup getInstance(FLLYear fll_year) throws IOException {
    if      (fll_year == FLLYear.Replay_2020)     return getReplay2020();
    else if (fll_year == FLLYear.Cargo_2021)      return getCargo2021();
    else if (fll_year == FLLYear.InchesReference) return getInchesReference();
    else throw new RuntimeException("Unknown instance \"" + fll_year + "\"");
  }
  private static BufferedImage replay_2020_bi = null; // Only keep a single copy (save memory)
  private static FLLFieldSetup getReplay2020() throws IOException {
    FLLFieldSetup fll_field_setup = new FLLFieldSetup();
    if (replay_2020_bi == null) {
      replay_2020_bi = ImageIO.read(new File("/home/user/Desktop/FLL2020-Replay-Mat-Plain-FullSize-v01F.jpg"));
    }
    fll_field_setup.field_bi = replay_2020_bi;
    fll_field_setup.dpi_x    = 150;
    fll_field_setup.dpi_y    = 150;
    return fll_field_setup;
  }
  private static BufferedImage cargo_2020_bi = null; // Only keep a single copy (save memory)
  private static FLLFieldSetup getCargo2021() throws IOException {
    FLLFieldSetup fll_field_setup = new FLLFieldSetup();
    if (cargo_2020_bi == null) {
      File linux_file   = new File("/home/user/Desktop/FLL2021-Mat-CargoConnections-FullSize.jpg"),
           windows_file = new File("C:\\Users\\user\\Pictures\\FLL2021-Mat-CargoConnections-FullSize.jpg");
      File file;
      if (linux_file.exists()) file = linux_file; else file = windows_file;
      cargo_2020_bi = ImageIO.read(file);
    }
    fll_field_setup.field_bi = cargo_2020_bi;
    fll_field_setup.dpi_x    = 150;
    fll_field_setup.dpi_y    = 150;
    return fll_field_setup;
  }
  private static BufferedImage inches_reference_bi;
  private static FLLFieldSetup getInchesReference() {
    FLLFieldSetup fll_field_setup = new FLLFieldSetup(); int my_dpi = 150;
    if (inches_reference_bi == null) {
      inches_reference_bi = new BufferedImage(11925,6731,BufferedImage.TYPE_INT_RGB);
      Graphics2D g2d = null; try { g2d = (Graphics2D) inches_reference_bi.getGraphics(); 
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      g2d.setColor(Color.white); g2d.fillRect(0,0,inches_reference_bi.getWidth(),inches_reference_bi.getHeight());
      g2d.setColor(Color.black);
        int x = 0; while (x < inches_reference_bi.getWidth()) {
          g2d.fillRect(x,0,3,inches_reference_bi.getHeight());
          x += my_dpi;
        }
        int y = 0; while (y < inches_reference_bi.getHeight()) {
          g2d.fillRect(0,y,inches_reference_bi.getWidth(),3);
          y += my_dpi;
        }
      } finally { if (g2d != null) g2d.dispose(); }
    }
    fll_field_setup.field_bi = inches_reference_bi;
    fll_field_setup.dpi_x    = my_dpi;
    fll_field_setup.dpi_y    = my_dpi;
    return fll_field_setup;
  }
}

/**
 * Wheel Placement + Wheel Size + Sensor Placement + Robot Base / Size
 */
class FLLRobotConfiguration {
  public FLLRobotConfiguration(double my_track_half_in,
                               double my_s1_x_off,
                               double my_s1_y_off,
                               double my_s2_x_off,
                               double my_s2_y_off) {
    this.track_half_in = my_track_half_in;
    this.s1_x_off      = my_s1_x_off;
    this.s1_y_off      = my_s1_y_off;
    this.s2_x_off      = my_s2_x_off;
    this.s2_y_off      = my_s2_y_off;
  }
  public FLLRobotConfiguration() { } 

  /**
   * Inches from the center point to each wheel
   */
  double track_half_in = 2.0; 

  /**
   * Sensor 1 and 2 x/y coordinate offsets (in inches) from the center point
   */
  double s1_x_off =  2.0, s1_y_off =  0.5,
         s2_x_off =  2.0, s2_y_off = -0.5;

  /**
   * Wheel radius in inches
   */
  double wheel_radius = 1.0; 
}

/**
 *
 */
class FLLErrorConfig {
  /**
   *
   */
  public FLLErrorConfig(double motor, double light, double gyro) {
    this.motor_error        = motor;
    this.light_sensor_error = light;
    this.gyro_sensor_error  = gyro;
  }

  /**
   * Motor movement error
   */
  double motor_error     = 0.0;

  /**
   * Light intensity sensor reading error
   */ 
  double light_sensor_error = 0.0;

  /**
   *  Gyroscope error
   */
  double gyro_sensor_error  = 0.0;
}


/**
 *
 */
public class FLLMonteCarlo {
  /**
   *
   */
  FLLFieldSetup             field_setup;

  /**
   *
   */
  FLLRobotConfiguration     robot_config;

  /**
   *
   */
  FLLErrorConfig            error_config;

  /**
   * Render consumer -- can be null
   */
  SimulationStepRenderConsumer render_consumer;

  /**
   * Flag for the render consumer to terminate the simulation.
   */
  boolean continue_simulation = true;

  /**
   * Constructor
   */
  public FLLMonteCarlo(FLLFieldSetup                field_setup, 
                       FLLRobotConfiguration        robot_config, 
                       List<String>                 instructions,
                       FLLErrorConfig               error_config,
                       SimulationStepRenderConsumer render_consumer) {
    this.field_setup     = field_setup;
    this.robot_config    = robot_config;
    this.error_config    = error_config;
    this.render_consumer = render_consumer;

    // Step through the instructions... execute each one
    int i = 0;
    while ((i < instructions.size()) && continue_simulation) {
      simulateInstruction(instructions.get(i), i+1 /* make it 1-based */); 
      i++;
    }
    continue_simulation = false;
  }

  /**
   * Determine if the simulation is still running...
   */
  public boolean stillRunning() { return continue_simulation; }

  /**
   * Simulation an instruction
   *
   * Notes:  power should always be positive
   *
   * robot_position       <x-inches> <y-inches> <direction-degrees>
   *
   * move_inches          <inches>  <power>
   * move_seconds         <seconds> <power>
   *
   * turn_degrees_sensor  <degrees> <power>
   * turn_degrees         <degrees> <power>
   *
   * pivot_degrees_sensor <degrees> <power>
   * pivot_degrees        <degrees> <power>
   *
   * wait_seconds         <seconds>
   *
   * square_line                               // square up on a line on the board
   *
   * square_edge                               // square up against the back of the robot/wall
   *
   * square_obstacle                           // square up on an obstacle
   *
   * do_mission <seconds> <prob1> <score1> <prob2> <score2> ...
   *
   * follow_line          <inches>  <power> <sensor-number> <right|left>
   *
   * follow_until_black   <inches>  <power> <sensor-number> <right|left>
   */
  private void simulateInstruction(String line, int line_no) {
    if (line == null || line.equals("") || line.startsWith("#")) return;

    StringTokenizer st = new StringTokenizer(line, " \t");
    String tokens[] = new String[st.countTokens()];
    for (int i=0;i<tokens.length;i++) tokens[i] = st.nextToken();
    if (tokens.length > 0) {
      if        (tokens[0].equals("robot_position"))        { insRobotPosition      (tokens);
      } else if (tokens[0].equals("move_inches"))           { insMoveInches         (tokens);
      } else if (tokens[0].equals("move_seconds"))          { insMoveSeconds        (tokens);
      } else if (tokens[0].equals("turn_degrees_sensor"))   { insTurnDegreesSensor  (tokens,false);
      } else if (tokens[0].equals("turn_degrees"))          { insTurnDegrees        (tokens,false);
      } else if (tokens[0].equals("pivot_degrees_sensor"))  { insPivotDegreesSensor (tokens);
      } else if (tokens[0].equals("pivot_degrees"))         { insPivotDegrees       (tokens);
      } else if (tokens[0].equals("wait_seconds"))          { insWaitSeconds        (tokens);
      } else if (tokens[0].equals("square_line"))           { insSquareLine         (tokens);
      } else if (tokens[0].equals("square_edge"))           { insSquareEdge         (tokens);
      } else if (tokens[0].equals("square_obstacle"))       { insSquareObstacle     (tokens);
      } else if (tokens[0].equals("follow_line"))           { insFollowLine         (tokens);
      } else if (line.indexOf("=") >= 0)                    { evaluateVariable      (line);
      } else throw new RuntimeException("Do Not Understand Instruction \"" + line + "\" @ Line " + line_no);
    }
  }

  /**
   * Set a variable
   */
  private void evaluateVariable(String line) {
    // Remove spaces
    StringBuffer    sb = new StringBuffer();
    for (int i=0;i<line.length();i++) { if (line.charAt(i) == ' ' || line.charAt(i) == '\t') { } else sb.append(line.charAt(i)); }
    String str = sb.toString();

    // Split on the equals ... there's only one.  there's only one, right?
    StringTokenizer st = new StringTokenizer(str, "=");
    if (st.countTokens() != 2) throw new RuntimeException("Not A Variable Assignment \"" + line + "\"");
    String variable = st.nextToken(), value = st.nextToken();
    variables.put(variable, Double.parseDouble(value));
  }

  /**
   * Evaluate a parameter -- if it's a double, then just parse it... otherwise, check for a variable
   */
  private double evaluateParameter(String str) {
    if ((str.charAt(0) >= 'a' && str.charAt(0) <= 'z') ||
        (str.charAt(0) >= 'A' && str.charAt(0) <= 'Z')) {
      if (variables.containsKey(str)) return variables.get(str);
      else throw new RuntimeException("Variable \"" + str + "\" Not Recognized");
    } else return Double.parseDouble(str);
  }

  /**
   * Variable lookups
   */
  private Map<String,Double> variables = new HashMap<String,Double>();

  /**
   * Instruction implementations
   */
  private void insRobotPosition(String tokens[]) {
    robot_x   = evaluateParameter(tokens[1]);
    robot_y   = evaluateParameter(tokens[2]);
    robot_dir = evaluateParameter(tokens[3]);
    logReposition(robot_x, robot_y, robot_dir);
  }

  /**
   *
   */
  private void insMoveInches          (String tokens[]) {
    double inches     = evaluateParameter(tokens[1]);
    double rotations  = Math.abs(inches / (robot_config.wheel_radius*2.0*Math.PI));

    if (inches > 0) motor_a = motor_b =      evaluateParameter(tokens[2]); 
    else            motor_a = motor_b = -1 * evaluateParameter(tokens[2]); 

    resetWheelRotationCounter();

    while (Math.abs(wheelRotationCounter()) < rotations && continue_simulation) { updateRobotPosition(); }
    motor_a = motor_b = 0.0;
  }

  /**
   *
   */
  private void insMoveSeconds         (String tokens[]) {
    motor_a = motor_b = evaluateParameter(tokens[2]);
    double seconds_to_move = evaluateParameter(tokens[1]);
    double seconds = 0.0; while (seconds < seconds_to_move && continue_simulation) { updateRobotPosition(); }
    motor_a = motor_b = 0.0;
  }

  /**
   *
   */
  private void insTurnDegreesSensor   (String tokens[], boolean pivot) {
    double rel_degrees   = evaluateParameter(tokens[1]);
    double power         = evaluateParameter(tokens[2]);
    double abs_degrees   = robot_dir + rel_degrees + boxMuller(error_config.gyro_sensor_error);
    double reduced_power = power/5; if (reduced_power < 5.0) reduced_power = 5.0;

    // Turn one way
    if        (rel_degrees < 0.0) {
      if (Math.abs(rel_degrees) > 10.0) { // Fast turn at first...
        if (pivot) {                   motor_b =  power; }
        else       { motor_a = -power; motor_b =  power; }
        double abs_degrees_10 = abs_degrees + 10;
        while (robot_dir > abs_degrees_10 && continue_simulation) updateRobotPosition();
      }


      // Slow turn at the end
      if (pivot) {                           motor_b =  reduced_power; }
      else       { motor_a = -reduced_power; motor_b =  reduced_power; }
      while (robot_dir > abs_degrees && continue_simulation) updateRobotPosition();

    // Turn the other way
    } else if (rel_degrees > 0.0) {
      if (Math.abs(rel_degrees) > 10.0) { // Fast turn at first...
        if (pivot) { motor_a =  power;                    }
        else       { motor_a =  power; motor_b = -power;  }
        double abs_degrees_10 = abs_degrees - 10;
        while (robot_dir < abs_degrees_10 && continue_simulation) updateRobotPosition();
      }

      // Slow turn at the end
      if (pivot) { motor_a =  reduced_power;                           }
      else       { motor_a =  reduced_power; motor_b = -reduced_power; }
      while (robot_dir < abs_degrees && continue_simulation) updateRobotPosition();

    }
    motor_a = motor_b = 0.0;
  }

  // static java.io.PrintStream error_out = null; // Debug Code to evaluate error function

  /**
   *
   */
  private void insTurnDegrees         (String tokens[], boolean pivot) {
    double rel_degrees   = evaluateParameter(tokens[1]),
           power         = evaluateParameter(tokens[2]),
           error_degrees = boxMuller(power/100.0),                  // Error increases with power
           abs_degrees   = robot_dir + rel_degrees + error_degrees;

    // Debug Code to evaluate error function
    // if (error_out == null) { try { error_out = new java.io.PrintStream(new java.io.FileOutputStream("error.csv")); error_out.println("ERROR"); } catch (IOException ioe) { } }
    // error_out.println((int) (error_degrees * 1000));

    // Negative turn
    if        (rel_degrees < 0.0) {
      if (pivot) {                   motor_b =  power; }
      else       { motor_a = -power; motor_b =  power; }
      while (robot_dir >= abs_degrees && continue_simulation) updateRobotPosition();

    // Positive turn
    } else if (rel_degrees > 0.0) {
      if (pivot) { motor_a =  power;                   }
      else       { motor_a =  power; motor_b = -power; }
      while (robot_dir <= abs_degrees && continue_simulation) updateRobotPosition();
    }
    motor_a = motor_b = 0.0;
  }
  private void insPivotDegreesSensor  (String tokens[]) { insTurnDegreesSensor(tokens, true); }
  private void insPivotDegrees        (String tokens[]) { insTurnDegrees      (tokens, true); }

  /**
   * Wait a specified number of seconds.
   */
  private void insWaitSeconds         (String tokens[]) {
    double seconds = evaluateParameter(tokens[1]);
    double abs_seconds = sim_time + seconds;
    while (sim_time < abs_seconds && continue_simulation) updateRobotPosition();
  }

  /**
   * Use the sensor to square up on a line.  Assumes that the sensors are equally space
   * out from the robot.
   */
  private void insSquareLine          (String tokens[]) {
    motor_a = motor_b = 0.0;
    while (sensorValue(1) > 128 && continue_simulation) { motor_a =  30.0; updateRobotPosition(); } motor_a = motor_a = 0.0;
    while (sensorValue(2) > 128 && continue_simulation) { motor_b =  30.0; updateRobotPosition(); } motor_a = motor_b = 0.0;

    while (sensorValue(1) < 128 && continue_simulation) { motor_a = -30.0; updateRobotPosition(); } motor_a = motor_a = 0.0;
    while (sensorValue(2) < 128 && continue_simulation) { motor_b = -30.0; updateRobotPosition(); } motor_a = motor_b = 0.0;

    while (sensorValue(1) > 128 && continue_simulation) { motor_a =  10.0; updateRobotPosition(); } motor_a = motor_a = 0.0;
    while (sensorValue(2) > 128 && continue_simulation) { motor_b =  10.0; updateRobotPosition(); } motor_a = motor_b = 0.0;

    while (sensorValue(1) < 128 && continue_simulation) { motor_a = -10.0; updateRobotPosition(); } motor_a = motor_a = 0.0;
    while (sensorValue(2) < 128 && continue_simulation) { motor_b = -10.0; updateRobotPosition(); } motor_a = motor_b = 0.0;
  } 

  /**
   * Square against the edge of the board.  Assumes that the robot has a smooth back.
   */
  private void insSquareEdge          (String tokens[]) {
  }

  /**
   * Square up on an obstacles.  Assumes that the robot footprint fits within the obstacles perimeter.
   */
  private void insSquareObstacle      (String tokens[]) {
  }

  /**
   * Follow a line.
   */
  private void insFollowLine          (String tokens[]) {
    double  inches    = evaluateParameter(tokens[1]),
            power     = evaluateParameter(tokens[2]);
    int     sensor    = Integer.parseInt(tokens[3]);
    boolean right     = tokens[4].toLowerCase().equals("right");
    double  rotations = Math.abs(inches / (robot_config.wheel_radius*2.0*Math.PI));

    resetWheelRotationCounter();
    while (Math.abs(wheelRotationCounter()) < rotations && continue_simulation) { 
      int sensor_value = sensorValue(sensor);

      if        (sensor_value > (128-8) && sensor_value < (128+8)) { motor_a = motor_b = power; 
      } else if (sensor_value <  128)                              { 
        double diff = (128 - sensor_value);
        motor_a = power + diff; motor_b = power - diff;
      } else if (sensor_value >  128)                              { 
        double diff = (sensor_value - 128)*0.5;
        motor_a = power - diff; motor_b = power + diff;
      }

      if (motor_a < -100.0) motor_a = -100.0;
      if (motor_a >  100.0) motor_a =  100.0;

      if (motor_b < -100.0) motor_b = -100.0;
      if (motor_b >  100.0) motor_b =  100.0;

      if (!right) { double swap = motor_a; motor_a = motor_b; motor_b = swap; }

      updateRobotPosition(); 
    } 

    motor_a = motor_b = 0.0;
  }

  /**
   * Gaussian Error Distribution Function
   *
   * From the following: https://www.baeldung.com/cs/uniform-to-normal-distribution
   */
  private static double boxMuller(double mu) { 
    if (mu == 0.0) return 0.0; // No error...
    return Math.sqrt(-2.0 * Math.log(Math.random())) * Math.cos(2.0 * Math.PI * Math.random()) * mu; 
  }

  /**
   * Reset the wheel rotation counter.
   */
  private void resetWheelRotationCounter() { wheel_a_rotation_counter = wheel_b_rotation_counter = 0.0; }

  /**
   * Read the wheel rotation counter
   */
  private double wheelRotationCounter() { return (wheel_a_rotation_counter + wheel_b_rotation_counter)/2.0; }

  /**
   * Wheel rotation counters
   */
  private double wheel_a_rotation_counter = 0.0,
                 wheel_b_rotation_counter = 0.0;

  /**
   * Update the position based on the motor powers ... include the error rate here.
   *
   * Formulas from youtube video "Control of Mobile Robots - 2.2 Differential Drive Robots"
   * ... http://www.youtube.com/watch?v=aE7RQNhwnPQ
   */
  private void updateRobotPosition() {
    // Get the settings ... adjust them for the simulation step size
    // 
    // From the following, it says that the larger motor does 160 - 170 rpms
    // - https://counties.agrilife.org/gillespie/files/2015/04/EV3-Motors-Sensors-Explained.pdf

    // double motor_a_adj = motor_a, // angular velocity for motor a // WORKS! (1)
    //        motor_b_adj = motor_b; // angular velocity for motor b // WORKS! (1)

    double motor_a_adj = (160.0/60.0) * (motor_a/100.0), // angular velocity for motor a // with the 160rpm // WORKS! (2)
           motor_b_adj = (160.0/60.0) * (motor_b/100.0); // angular velocity for motor b // with the 160rpm // WORKS! (2)

    // Apply the error (if any)
    motor_a_adj += boxMuller(error_config.motor_error);
    motor_b_adj += boxMuller(error_config.motor_error);

    // Variable names from youtube video
    // double R_adj = 12.1;

    double R         = robot_config.wheel_radius,
           L         = 2.0 * robot_config.track_half_in /* * R_adj */ ;
    double robot_rad = robot_dir * (2.0 * Math.PI) / 360.0;
    double phi       = robot_rad + Math.PI/2.0, 
           x0        = robot_x, 
           y0        = robot_y;

    // Differential x, y, phi calcs
    double dx   = (R/2.0) * (motor_a_adj + motor_b_adj) * Math.cos(phi),
           dy   = (R/2.0) * (motor_a_adj + motor_b_adj) * Math.sin(phi);
    double dphi = (R/L)   * (motor_a_adj - motor_b_adj);

    // Update the robot's position and direction ... no idea why multiplying times four helps
    double fix = 6.15;

    robot_x   = x0 + dx*time_inc*fix;
    robot_y   = y0 + dy*time_inc*fix;
    robot_rad = phi + dphi*time_inc*fix - Math.PI/2.0;
    robot_dir = robot_rad * 360.0 / (Math.PI * 2.0);

    wheel_a_rotation_counter += motor_a_adj*time_inc;
    wheel_b_rotation_counter += motor_b_adj*time_inc;

    // Check for hitting something...
    boolean collision = false;

    // Update sensor value

    // Record position information
    logTelemetry(collision, robot_x, robot_y, robot_dir);

    // Update timing
    sim_time += time_inc; sim_step++;

    // If a render consumer is set, update it ... check for simulation termination
    if (render_consumer != null) { 
      boolean keep_going = render_consumer.simulationRender(renderSimulation(SIM_RENDER_W, SIM_RENDER_H, SIM_ZOOM_OUT), sim_step, sim_time); 
      if (keep_going == false) { continue_simulation = false; }
    }
  }

  /** 
   * Time increment for each simulation step
   */
  // final double time_inc   = 0.0001; // WORKS! (1)
  final double time_inc   = 0.01; // WORKS! (2)

  /**
   * Simulation time ... incremented by time_inc
   */
  private double sim_time = 0.0;

  /**
   * Accessor for the simulation time.
   *
   *@return simulation time
   */
  public double simulationTime() { return sim_time; }

  /**
   * Discrete step
   */
  private int sim_step = 0;

  /**
   * Robot's X Coordinate ... inches
   */
  private double robot_x = 10.0,

  /**
   * Robot's Y Coordinate ... inches
   */
                 robot_y = 10.0,

  /**
   * Robot's Direction (Angle) ... degrees
   */
                 robot_dir = 0.0;

  /**
   * Motor A power
   */
  private double motor_a = 0.0,

  /**
   * Motor B power
   */
                 motor_b = 0.0;

  /**
   * Calculate the sensor value.
   *
   *@param sensor either 1 or 2
   */
  public int sensorValue(int sensor) {
    BufferedImage bi = sensorImage(sensor);
    return (int) (calculateAverageGrayValue(bi) + boxMuller(error_config.light_sensor_error));
  }

  /**
   * Calculate average gray value.
   */
  public int calculateAverageGrayValue(BufferedImage img) {
    long sum = 0L; int samples = 0;

    for (int x=0;x<img.getWidth();x++) for (int y=0;y<img.getHeight();y++) {
      int rgb  = img.getRGB(x,y);
      int gray = (int) (((rgb >> 16) & 0x00ff) * 0.2989 + ((rgb >>  8) & 0x00ff) * 0.5870 + ((rgb >>  0) & 0x00ff) * 0.1140);
      sum += gray; samples++;
    }

    if (samples == 0) samples = 1;
    return (int) (sum/samples);
  }

  /**
   * Logging
   */
  private void logReposition(double x, double y, double d)                   { log.add(new LogEntry(x, y, d));            }
  private void logTelemetry(boolean collision, double x, double y, double d) { log.add(new LogEntry(collision, x, y, d)); }
  List<LogEntry> log = new ArrayList<LogEntry>();
  class LogEntry { 
    boolean reposition = false;
    boolean my_collision;
    double  my_robot_x, 
            my_robot_y, 
            my_robot_dir;
    public LogEntry(                   double x, double y, double d) {
      this.reposition   = true;
      this.my_robot_x   = x;
      this.my_robot_y   = y;
      this.my_robot_dir = d;
    }
    public LogEntry(boolean collision, double x, double y, double d) {
      this.my_collision = collision;
      this.my_robot_x   = x;
      this.my_robot_y   = y;
      this.my_robot_dir = d;
  } };

  // Simulation Render Geometry
  public static final int     SIM_RENDER_W = 1024, SIM_RENDER_H = 1024;
  public static final double  SIM_ZOOM_OUT = 4.0;

  /**
   * Render the simulation state
   */ 
  private BufferedImage renderSimulation(int render_w, int render_h, double zoom_out) {
    BufferedImage bi = new BufferedImage(render_w, render_h, BufferedImage.TYPE_INT_RGB);
    Graphics2D    g2d = null; try {
      g2d = (Graphics2D) bi.getGraphics();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // go with a third reduction
      int center_wx = (int) (robot_x * field_setup.dpi_x), center_wy = (int) (robot_y * field_setup.dpi_y);
      int center_sx = bi.getWidth()/2, center_sy = bi.getHeight()/2;

      g2d.drawImage(fieldSubImage((int) (center_wx - zoom_out*(bi.getWidth() /2)),
                                  (int) (center_wy - zoom_out*(bi.getHeight()/2)),
                                  (int) (bi.getWidth() *zoom_out),
                                  (int) (bi.getHeight()*zoom_out)), 0, 0, bi.getWidth(), bi.getHeight(), null);

      // Draw the robot
      // - Robot Center
      g2d.setColor(Color.red); g2d.fill(new Ellipse2D.Double(center_sx - 15, center_sy - 15, 30, 30));

      // - Robot Direction
      double dx  = Math.cos((robot_dir+90)*2.0*Math.PI/360),
             dy  = Math.sin((robot_dir+90)*2.0*Math.PI/360);
      double pdx =  dy,
             pdy = -dx;
      Stroke orig_stroke = g2d.getStroke(); g2d.setStroke(new BasicStroke(6.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
      g2d.drawLine(center_sx, center_sy, (int) (center_sx+dx*60.0), (int) (center_sy+dy*60.0));
      g2d.drawLine((int) (center_sx+dx*60.0),            (int) (center_sy+dy*60.0),
                   (int) (center_sx+dx*50.0 + pdx*10.0), (int) (center_sy+dy*50.0 + pdy*10.0));
      g2d.drawLine((int) (center_sx+dx*60.0),            (int) (center_sy+dy*60.0),
                   (int) (center_sx+dx*50.0 - pdx*10.0), (int) (center_sy+dy*50.0 - pdy*10.0));
      g2d.setStroke(orig_stroke);

      // - Robot Wheels ... robot_config.track_half_in ... robot_config.wheel_radius;
      int wheel_sx, wheel_sy;
      wheel_sx = (int) (center_sx + (pdx*robot_config.track_half_in*field_setup.dpi_x /* + pdy*robot_config.track_half_in*field_setup.dpi_y */)/zoom_out);
      wheel_sy = (int) (center_sy + (/* pdx*robot_config.track_half_in*field_setup.dpi_x + */ pdy*robot_config.track_half_in*field_setup.dpi_y)/zoom_out);
      g2d.fill(new Ellipse2D.Double(wheel_sx-5,wheel_sy-5,10,10));
      wheel_sx = (int) (center_sx - (pdx*robot_config.track_half_in*field_setup.dpi_x /* + pdy*robot_config.track_half_in*field_setup.dpi_y */)/zoom_out);
      wheel_sy = (int) (center_sy - (/* pdx*robot_config.track_half_in*field_setup.dpi_x + */ pdy*robot_config.track_half_in*field_setup.dpi_y)/zoom_out);
      g2d.fill(new Ellipse2D.Double(wheel_sx-5,wheel_sy-5,10,10));

      // - Sensor (and sensor views)
      for (int i=0;i<2;i++) {
        double x_off, y_off; BufferedImage sensor_bi; int sensor_bi_x, sensor_bi_y;
        if (i == 0) { x_off = robot_config.s1_x_off; y_off = robot_config.s1_y_off; sensor_bi = sensorImage(1); sensor_bi_x = 3;
        } else      { x_off = robot_config.s2_x_off; y_off = robot_config.s2_y_off; sensor_bi = sensorImage(2); sensor_bi_x = bi.getWidth() - sensor_bi.getWidth() - 3; }
        sensor_bi_y = bi.getHeight() - (6 + sensor_bi.getHeight());

        int sx = (int) (center_sx + (dx *x_off*field_setup.dpi_x + dy *y_off*field_setup.dpi_y)/zoom_out),
            sy = (int) (center_sy + (pdx*x_off*field_setup.dpi_x + pdy*y_off*field_setup.dpi_y)/zoom_out);
        // g2d.drawLine(sx - 10, sy, sx + 10, sy); 
        // g2d.drawLine(sx, sy - 10, sx, sy + 10);
        g2d.setColor(Color.black); g2d.draw(new Ellipse2D.Double(sx-10,sy-10,20,20));

        g2d.setColor(Color.black); g2d.fillRect(sensor_bi_x - 3, sensor_bi_y - 3, sensor_bi.getWidth() + 6, sensor_bi.getHeight() + 6);
        g2d.drawImage(sensor_bi, sensor_bi_x, sensor_bi_y, null);
        g2d.setColor(Color.white); g2d.drawString(""+(i+1), sensor_bi_x + 2, sensor_bi_y + 2 + Utils.txtH(g2d,""+(i+1)));
      }

      // Draw where the robot has been
      g2d.setColor(Color.blue);
      for (int i=0;i<log.size();i++) {
        int sx = (int) ((log.get(i).my_robot_x * field_setup.dpi_x - center_wx)/zoom_out + center_sx),
            sy = (int) ((log.get(i).my_robot_y * field_setup.dpi_y - center_wy)/zoom_out + center_sy);
        g2d.fill(new Ellipse2D.Double(sx-5,sy-5,10,10));
      }
    } finally { if (g2d != null) g2d.dispose(); }
    return bi;
  }

  /**
   *
   */
  private static BufferedImage fieldSubImage(int x, int y, int w, int h, FLLFieldSetup my_field_setup) {
    BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
    Graphics2D g2d = null; try { g2d = (Graphics2D) bi.getGraphics();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      int x_screen = 0, y_screen = 0;
      if (x < 0) { x_screen = (int) Math.abs(x); x = 0; }
      if (y < 0) { y_screen = (int) Math.abs(y); y = 0; }
      int w_extract = w, h_extract = h;
      if ((x+w) >= my_field_setup.field_bi.getWidth ()) w_extract = my_field_setup.field_bi.getWidth()  - x - 1;
      if ((y+h) >= my_field_setup.field_bi.getHeight()) h_extract = my_field_setup.field_bi.getHeight() - y - 1;

      g2d.drawImage(my_field_setup.field_bi.getSubimage(x,y,w_extract,h_extract), x_screen, y_screen, null);

    } finally { if (g2d != null) g2d.dispose(); }
    return bi;
  }
  private BufferedImage fieldSubImage(int x, int y, int w, int h) { return fieldSubImage(x,y,w,h,field_setup); }

  /**
   * Return the sensor view.
   *
   *@param sensor either 1 or 2
   */
  private BufferedImage sensorImage(int sensor) {
    // Copy from the simulation rendering 
    int center_wx = (int) (robot_x * field_setup.dpi_x), center_wy = (int) (robot_y * field_setup.dpi_y);
    double dx  = Math.cos((robot_dir+90)*2.0*Math.PI/360),
           dy  = Math.sin((robot_dir+90)*2.0*Math.PI/360);
    double pdx =  dy,
           pdy = -dx;

    // Mostly a copy from the simulation rendering
    double x_off, y_off;
    if (sensor == 1) { x_off = robot_config.s1_x_off; y_off = robot_config.s1_y_off;
    } else           { x_off = robot_config.s2_x_off; y_off = robot_config.s2_y_off; }

    int x = (int) (center_wx + dx *x_off*field_setup.dpi_x + dy *y_off*field_setup.dpi_y),
        y = (int) (center_wy + pdx*x_off*field_setup.dpi_x + pdy*y_off*field_setup.dpi_y);

    BufferedImage bi = fieldSubImage(x - 64, y - 64, 128, 128);

    // Mask the return to just a circle in the center
    Graphics2D g2d = null; try {
      g2d = (Graphics2D) bi.getGraphics();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      Area area = new Area(new Rectangle2D.Double(0,0,bi.getWidth(),bi.getHeight()));
      area.subtract(new Area(new Ellipse2D.Double(0,0,bi.getWidth(),bi.getHeight())));
      g2d.setColor(Color.black);
      g2d.fill(area);
    } finally { g2d.dispose(); }

    return bi;
  }

  /**
   * Create a composite image of the runs...
   */
  public static BufferedImage createCompositeImage(List<FLLMonteCarlo> runs, FLLFieldSetup my_field_setup) {
    // Find the coordinate bounds of the runs...
    double x0, y0, x1, y1; x0 = y0 = Double.POSITIVE_INFINITY; x1 = y1 = Double.NEGATIVE_INFINITY;
    for (int i=0;i<runs.size();i++) {
      for (int j=0;j<runs.get(i).log.size();j++) {
        double  x         = runs.get(i).log.get(j).my_robot_x, 
                y         = runs.get(i).log.get(j).my_robot_y;
        boolean collision = runs.get(i).log.get(j).my_collision;
        if (x < x0) x0 = x; if (x > x1) x1 = x;
        if (y < y0) y0 = y; if (y > y1) y1 = y;
      }
    }

    // Sanity check...
    if (Double.isInfinite(x0)) { x0 = y0 = 0.0; x1 = y1 = 10.0; }
    if (x0 == x1)              { x0 -= 5.0; x1 += 5.0; }
    if (y0 == y1)              { y0 -= 5.0; y1 += 5.0; }
    double dpi_x = my_field_setup.dpi_x, dpi_y = my_field_setup.dpi_y;

    // Make a border... 10%...
    double perc = (x1 - x0)*0.05; x0 -= perc; x1 += perc;
           perc = (y1 - y0)*0.05; y0 -= perc; y1 += perc;

    // Create the image and do the composite
    BufferedImage bi = new BufferedImage((int) ((x1 - x0)*dpi_x), (int) ((y1 - y0)*dpi_y), BufferedImage.TYPE_INT_RGB); 
    int           w  = bi.getWidth(), 
                  h = bi.getHeight();

    Graphics2D g2d = null; try { g2d = (Graphics2D) bi.getGraphics(); Composite comp_default = g2d.getComposite(); Stroke stroke_default = g2d.getStroke();

      // Draw the base image ... make it slightly faded
      g2d.drawImage(fieldSubImage((int) (x0*dpi_x), (int) (y0*dpi_y), (int) (x1*dpi_x), (int) (y1*dpi_y), my_field_setup), 0, 0, null);
      g2d.setColor(Color.white); g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
      g2d.fillRect(0,0,w,h);

      // Draw each run
      for (int i=0;i<runs.size();i++) { List<LogEntry> log = runs.get(i).log; if (log.size() > 1) {
        boolean collisions_found = false;
        Path2D path2d = new Path2D.Double();
        path2d.moveTo(wxToSx(log.get(0).my_robot_x - x0, my_field_setup), wyToSy(log.get(0).my_robot_y - y0, my_field_setup));

        for (int j=1;j<log.size();j++) { 
          //
          // Reposition Event
          //
          if (log.get(j).reposition) {

            renderPath(g2d, path2d, collisions_found);

            path2d = new Path2D.Double();
            path2d.moveTo(wxToSx(log.get(j).my_robot_x - x0, my_field_setup), wyToSy(log.get(j).my_robot_y - y0, my_field_setup));
            collisions_found = false;

          //
          // Regular Movement Event
          //
          } else {
            path2d.lineTo(wxToSx(log.get(j).my_robot_x - x0, my_field_setup), wyToSy(log.get(j).my_robot_y - y0, my_field_setup)); 
            if (log.get(j).my_collision) {
              collisions_found = true;
              int sx = wxToSx(log.get(j).my_robot_x - x0, my_field_setup), 
                  sy = wyToSy(log.get(j).my_robot_y - y0, my_field_setup);
              g2d.setColor(Color.red);
              g2d.setStroke(new BasicStroke(4.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
              g2d.drawLine(sx - 10, sy - 10, sx + 10, sy + 10);
              g2d.drawLine(sx + 10, sy - 10, sx - 10, sy + 10);
              g2d.setStroke(stroke_default);
            }
          }
        }

        renderPath(g2d, path2d, collisions_found);

      } }

      // Print simulation time information
      double time_sum = 0.0, time_min = Double.POSITIVE_INFINITY, time_max = Double.NEGATIVE_INFINITY;
      for (int i=0;i<runs.size();i++) {
        double time = runs.get(i).sim_time;
        time_sum += time;
        if (time_min > time) time_min = time;
        if (time_max < time) time_max = time;
      }
      String time_str = "Min:" + time_min + " ... Avg:" + (time_sum/runs.size()) + " ... Max:" + time_max;
      g2d.setColor(Color.white);
        g2d.drawString(time_str, 5, 5 + Utils.txtH(g2d, time_str));
        g2d.drawString(time_str, 6, 6 + Utils.txtH(g2d, time_str));
      g2d.setColor(Color.black);
        g2d.drawString(time_str, 4, 4 + Utils.txtH(g2d, time_str));

    } finally { if (g2d != null) g2d.dispose(); }
    return bi;
  }

  private static void renderPath(Graphics2D g2d, Path2D path2d, boolean collisions_found) {
    Stroke    stroke_default = g2d.getStroke();
    Composite comp_default   = g2d.getComposite();

    if (collisions_found) g2d.setColor(Color.red); else g2d.setColor(Color.black);
    g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
    g2d.setStroke(new BasicStroke(8.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
    g2d.draw(path2d);

    g2d.setStroke(stroke_default);
    g2d.setComposite(comp_default);
  }

  /**
   * Convert a world x coordinate (inches) into a screen x coordinate (pixels).
   */
  public static int wxToSx(double wx, FLLFieldSetup my_field_setup) { return (int) (wx * my_field_setup.dpi_x); }

  /**
   * Convert a world y coordinate (inches) into a screen y coordinate (pixels).
   */
  public static int wyToSy(double wy, FLLFieldSetup my_field_setup) { return (int) (wy * my_field_setup.dpi_y); }

  /**
   * Main entry point.
   *
   * java racetrack.toys.FLLMonteCarlo [-runs #] input-file
   */
  public static void main(String args[]) {
    try {
      // Print usage if no arguments provided...
      if (args.length == 0) {
        System.err.println("Usage:\n\njava racetrack.toys.FLLMonteCarlo [-runs NUM] [-composite FILENAME] instructions.txt [instructions.txt...]\n");
        System.exit(0);
      }

      // Variables from the arguments
      int runs = 1; String composite_image_filename = null; List<File> files_to_parse = new ArrayList<File>();

      // Parse the arguments
      int i = 0; while (i < args.length) {
        if        (args[i].equals("-runs"))       {
          if ((i+1) < args.length) { runs = Integer.parseInt(args[i+1]); i += 2; 
          } else throw new RuntimeException("argument error:  -runs needs a number after it...");
        } else if (args[i].equals("-composite")) {
          if ((i+1) < args.length) { composite_image_filename = args[i+1]; i += 2;
          } else throw new RuntimeException("argument error: -composite needs a filename to save to...");
        } else if ((new File(args[i])).exists()) { files_to_parse.add(new File(args[i]));                                i++; 
        } else                                   { System.err.println("Do Not Understand Argument \"" + args[i] + "\""); i++; }
      }

      // JFrame for debugging
      DebugJFrame debug_jframe = null; if (runs == 1) debug_jframe = new DebugJFrame();

      // Perform the runs
      for (i=0;i<files_to_parse.size();i++) {
        FLLFieldSetup.FLLYear field              = FLLFieldSetup.FLLYear.InchesReference;
        double                motor_error        = 0.0;
        double                light_sensor_error = 0.0;
        double                gyro_sensor_error  = 0.0;

        // Read the instructions for the simulation
        List<String> lines = new ArrayList<String>(); 
        BufferedReader in = new BufferedReader(new FileReader(files_to_parse.get(i)));
        String line; while ((line = in.readLine()) != null) {

          // Look for simulation directives
          StringTokenizer st = new StringTokenizer(line, " \t");
          String tokens[] = new String[st.countTokens()]; for (int j=0;j<tokens.length;j++) tokens[j] = st.nextToken();
          if (tokens.length == 2 && tokens[0].equals("#field")) {
            if      (tokens[1].equals("cargo2021"))        field = FLLFieldSetup.FLLYear.Cargo_2021;
            else if (tokens[1].equals("replay2020"))       field = FLLFieldSetup.FLLYear.Replay_2020;
            else if (tokens[1].equals("inches_reference")) field = FLLFieldSetup.FLLYear.InchesReference;
            else throw new RuntimeException("Unknown Simulation Directive for Field \"" + line + "\"");
          } else if (tokens.length == 2 && tokens[0].equals("#motor_error"))        { motor_error        = Double.parseDouble(tokens[1]);
          } else if (tokens.length == 2 && tokens[0].equals("#light_sensor_error")) { light_sensor_error = Double.parseDouble(tokens[1]);
          } else if (tokens.length == 2 && tokens[0].equals("#gyro_sensor_error"))  { gyro_sensor_error  = Double.parseDouble(tokens[1]);
          } else lines.add(line);
        }
        in.close();
        System.err.println("Parsing and simulating file \"" + files_to_parse.get(i) + "\" | Lines = " + lines.size());

        // Prepare the simulations
        List<FLLMonteCarlo> monte_carlos = new ArrayList<FLLMonteCarlo>();
        FLLFieldSetup       field_setup  = FLLFieldSetup.getInstance(field);

        // Run the simulations
        for (int j=0;j<runs;j++) {
          if      ((j%200) == 0) System.err.print(" " + j + " ");
          else if ((j%20)  == 0) System.err.print(".");
          FLLMonteCarlo fmc = new FLLMonteCarlo(field_setup,
                                                new FLLRobotConfiguration(),
                                                lines,
                                                new FLLErrorConfig(motor_error, light_sensor_error, gyro_sensor_error),
                                                debug_jframe);
          monte_carlos.add(fmc);
        }

        // Print information about the how long each took and the number of collisions
        int collisions = 0; double timing_min = Double.POSITIVE_INFINITY, timing_max = Double.NEGATIVE_INFINITY, timing_sum = 0.0;
        for (int j=0;j<monte_carlos.size();j++) {
          // Timing information
          double timing = monte_carlos.get(j).sim_time;
          if (timing < timing_min) timing_min = timing;
          if (timing > timing_max) timing_max = timing;
          timing_sum += timing;

          // Collisions
          boolean collision_found = false;
          for (int k=0;k<monte_carlos.get(j).log.size();k++) {
            if (monte_carlos.get(j).log.get(k).my_collision) collision_found = true;
          }
          if (collision_found) collisions++;
        }
        System.err.println();
        System.err.println(files_to_parse.get(i).getName() + 
                           " | runs = " + runs +
                           " | collisions = " + collisions + 
                           " | Timing Avg / Min / Max = " + (timing_sum/monte_carlos.size()) + " / " + timing_min + " / " + timing_max);

        // Write the composite image if specified
        if (composite_image_filename != null) { 
          System.err.println("Creating and Writing Composite File...");
          BufferedImage composite_bi = FLLMonteCarlo.createCompositeImage(monte_carlos, field_setup); 
          ImageIO.write(composite_bi, "PNG", new File(files_to_parse.get(i).getName() + "_" + runs + "_" + field + "_" + composite_image_filename + ".png"));
        }
      }
    } catch (Throwable t) { System.err.println("Throwable: " + t); t.printStackTrace(System.err); }
  }
}

/**
 *
 */
interface SimulationStepRenderConsumer { public boolean simulationRender(BufferedImage bi, int sim_step, double sim_time); }

/**
 * Window to watch what's happening in the simulation ... really just consumes and image from the simulator and shows it
 */
class DebugJFrame extends JFrame implements SimulationStepRenderConsumer {
  BufferedImage bi = new BufferedImage(FLLMonteCarlo.SIM_RENDER_W, FLLMonteCarlo.SIM_RENDER_H, BufferedImage.TYPE_INT_RGB); RenderComponent render_component;
  public DebugJFrame() {
    getContentPane().add("Center", render_component = new RenderComponent());
    setSize(bi.getWidth(), bi.getHeight()+16);
    setVisible(true);
  }
  public boolean simulationRender(BufferedImage bi, int sim_step, double sim_time) { this.bi = bi; render_component.repaint(); try { Thread.sleep(1); } catch (InterruptedException ie) { } return true; }
  class RenderComponent extends JComponent {
    @Override
    public void paintComponent(Graphics g) { g.drawImage(bi,0,0,null); }
  }
}



