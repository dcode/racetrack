/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.framework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Lite instance of the BundlesCounterContext.  Used for renderings where state isn't
 * need to be kept on how bundles map to bins/etc.  For example, small multiples.
 *
 * @author  D. Trimm
 * @version 1.0
 */
public class BCCLite {
  Bundles bundles;

  String  count_by,

          color_by;

  Counter counter;

  /**
   * Default constructor... requires calling count() to feed it records.
   */
  public BCCLite(Bundles bundles, String count_by, String color_by) {
    this.bundles = bundles; this.count_by = count_by; this.color_by = color_by;

    if      (count_by.equals(BundlesDT.COUNT_BY_BUNS))                                          counter = new BundleCounter();
    else if (count_by.indexOf(BundlesDT.DELIM) >= 0 ||
             bundles.getGlobals().isScalar(bundles.getGlobals().fieldIndex(count_by)) == false) counter = new SetCounter();
    else                                                                                        counter = new ScalarCounter();
  }

  /**
   * Constructor... Apply to all of the bundles in the bundles data structure.
   */
  public BCCLite(Bundles bundles, String count_by, String color_by, String keymaker_str) {
    this(bundles, count_by, color_by);
    Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next();
      if (KeyMaker.tabletCompletesBlank(tablet, keymaker_str) && tabletCounts(tablet)) {
        KeyMaker km = new KeyMaker(tablet, keymaker_str);
        Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          String keys[] = km.stringKeys(bundle);
          // if (keys != null && keys.length > 0 && keys[0].equals(BundlesDT.NOTSET) == false) { // not how it's done manually within the GUI code
          if (keys != null && keys.length > 0) {
            for (int i=0;i<keys.length;i++) count(bundle, keys[i]);
          }
        }
      }
    }
  }

  /**
   * Constructor... Apply to all of the bundles in the collection.
   */
  public BCCLite(Bundles bundles, String count_by, String color_by, Collection<Bundle> collection, String keymaker_str) {
    this(bundles, count_by, color_by);

    Map<Tablet,KeyMaker> kms = new HashMap<Tablet,KeyMaker>(); // key maker lookups

    Iterator<Bundle> it = collection.iterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); Tablet tablet = bundle.getTablet();

      if (kms.containsKey(tablet) == false) {
        if (KeyMaker.tabletCompletesBlank(tablet,keymaker_str) && tabletCounts(tablet)) kms.put(tablet, new KeyMaker(tablet, keymaker_str));
        else                                                                            kms.put(tablet, null);
      }

      if (kms.get(tablet) != null) {
        String keys[] = kms.get(tablet).stringKeys(bundle);
        // if (keys != null && keys.length > 0 && keys[0].equals(BundlesDT.NOTSET) == false) { // not how it's done manually within the GUI code
        if (keys != null && keys.length > 0) {
          for (int i=0;i<keys.length;i++) count(bundle, keys[i]);
        }
      }
    }
  }
  
  /**
   * Determine if a tablet can contribute based on how we are counting each record.
   */
  protected boolean tabletCounts(Tablet tablet) { return count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by); }

  public boolean          hasBin                   (String bin)                { return counter.hasBin(bin);                 }
  public double           count                    (Bundle bundle, String bin) { return counter.count(bundle,bin);           }
  public Iterator<String> binIterator              ()                          { return counter.binIterator();               }
  public double           totalNormalized          (String bin)                { return counter.totalNormalized(bin);        }
  public double           total                    (String bin)                { return counter.total(bin);                  }
  public List<String>     getColorBinsSortedByCount()                          { return counter.getColorBinsSortedByCount(); }
  public double           total                    (String bin, String cbin)   { return counter.total(bin,cbin);             }
  public double           binColorTotal            (String bin)                { return counter.binColorTotal(bin);          }


  /**
   * Sum of all the bins ... can't remember why but this could be different than the total
   */
  public double          totalCounts() {
    if (Double.isNaN(total_counts)) {
      double sum = 0.0; Iterator<String> it = binIterator(); while (it.hasNext()) sum += total(it.next());
      total_counts = sum;
    }
    return total_counts;
  }

  /**
   * Total of all the bins
   */
  protected double total_counts = Double.NaN;

  /**
   * Interface to abstract the ways to count a bundle (by record, by set, by scalar)
   */
  interface Counter {
    public double           count                    (Bundle bundle, String bin);
    public Iterator<String> binIterator              ();
    public double           totalNormalized          (String bin);
    public double           total                    (String bin);
    public List<String>     getColorBinsSortedByCount();
    public double           total                    (String bin, String cbin);
    public double           binColorTotal            (String bin);
    public boolean          hasBin                   (String bin); 
  }

  /**
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   *
   * Counter for counting bundles
   */
  class BundleCounter implements Counter {

    Map<String,Integer>             bin_to_total      = new HashMap<String,Integer>();
    double                          max_bin_total     = 0.0;
    Map<String,Map<String,Integer>> bin_to_cbin_total = new HashMap<String,Map<String,Integer>>();
    Map<String,Integer>             cbin_to_total     = new HashMap<String,Integer>();
    Map<String,Integer>             bin_color_total   = new HashMap<String,Integer>();

    Map<Tablet,KeyMaker>            tab_to_color      = new HashMap<Tablet,KeyMaker>();

    public  boolean         hasBin(String bin)                                   { return bin_to_total.containsKey(bin); }

    public double           count                    (Bundle bundle, String bin) { 
      // Create data structures
      if (bin_to_total.containsKey(bin) == false) {
        bin_to_total.     put(bin, 0);
        bin_to_cbin_total.put(bin, new HashMap<String,Integer>());
        bin_color_total.  put(bin, 0);
      }

      // Do the straightforward update
      bin_to_total.put(bin, bin_to_total.get(bin) + 1);
      if (bin_to_total.get(bin) > max_bin_total) max_bin_total = bin_to_total.get(bin);

      // If record supports coloring, then update color values as well
      if (tab_to_color.containsKey(bundle.getTablet()) == false) {
        if (color_by != null && KeyMaker.tabletCompletesBlank(bundle.getTablet(), color_by)) tab_to_color.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), color_by));
        else                                                                                 tab_to_color.put(bundle.getTablet(), null);
      }

      if (tab_to_color.get(bundle.getTablet()) != null) {
        String cbins[] = tab_to_color.get(bundle.getTablet()).stringKeys(bundle);
        for (int i=0;i<cbins.length;i++) {
          String cbin = cbins[i];

          if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, 0); cbin_to_total.put(cbin,0); }
          bin_to_cbin_total.get(bin).put(cbin, bin_to_cbin_total.get(bin).get(cbin) + 1);
          cbin_to_total.put(cbin, cbin_to_total.get(cbin) + 1);
          bin_color_total.put(bin, bin_color_total.get(bin) + 1);
        }
      } else {
        String cbin = "No Color";

        if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, 0); cbin_to_total.put(cbin,0); }
        bin_to_cbin_total.get(bin).put(cbin, bin_to_cbin_total.get(bin).get(cbin) + 1);
        cbin_to_total.put(cbin, cbin_to_total.get(cbin) + 1);
        bin_color_total.put(bin, bin_color_total.get(bin) + 1);
      }

      return bin_to_total.get(bin);
    }

    public Iterator<String> binIterator              ()                          { return bin_to_total.keySet().iterator(); }
    public double           totalNormalized          (String bin)                { if (max_bin_total == 0.0) max_bin_total = 1.0;
                                                                                   return bin_to_total.get(bin) / max_bin_total;}
    public double           total                    (String bin)                { return bin_to_total.get(bin); }
    public double           binColorTotal            (String bin)                { return bin_color_total.get(bin); }
    public double           total                    (String bin, String cbin)   { if (bin_to_cbin_total.get(bin).containsKey(cbin)) return bin_to_cbin_total.get(bin).get(cbin);
                                                                                   else                                              return 0.0; }
    public List<String>     getColorBinsSortedByCount()                          { 
      List<Sorter> sorter = new ArrayList<Sorter>();
      Iterator<String> it = cbin_to_total.keySet().iterator(); while (it.hasNext()) {
        String key = it.next(); int total = cbin_to_total.get(key); sorter.add(new Sorter(key,total));
      }
      Collections.sort(sorter);
      List<String> ret = new ArrayList<String>();
      for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
      return ret;
    }
  }

  /**
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   *
   * Counter for counting sets
   */
  class SetCounter implements Counter {
    Map<String,Set<String>>             bin_to_total      = new HashMap<String,Set<String>>();
    double                              max_bin_total     = 0.0;
    Map<String,Map<String,Set<String>>> bin_to_cbin_total = new HashMap<String,Map<String,Set<String>>>();
    Map<String,Set<String>>             cbin_to_total     = new HashMap<String,Set<String>>();
    Map<String,Set<String>>             bin_color_total   = new HashMap<String,Set<String>>();

    Map<Tablet,KeyMaker>            tab_to_color     = new HashMap<Tablet,KeyMaker>(),
                                    tab_to_count     = new HashMap<Tablet,KeyMaker>();

    public  boolean         hasBin(String bin)                                   { return bin_to_total.containsKey(bin); }

    public double           count                    (Bundle bundle, String bin) { 
      // Create data structures
      if (bin_to_total.containsKey(bin) == false) {
        bin_to_total.     put(bin, new HashSet<String>());
        bin_to_cbin_total.put(bin, new HashMap<String,Set<String>>());
        bin_color_total.  put(bin, new HashSet<String>());
      }

      // Can this bundle (and associated tablet) count or not?
      if (tab_to_count.containsKey(bundle.getTablet()) == false) {
        if (KeyMaker.tabletCompletesBlank(bundle.getTablet(),count_by)) tab_to_count.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), count_by));
        else                                                            tab_to_count.put(bundle.getTablet(), null);
      }

      if (tab_to_count.get(bundle.getTablet()) != null) {
        String items[] = tab_to_count.get(bundle.getTablet()).stringKeys(bundle);

        for (int j=0;j<items.length;j++) {
          String item = items[j]; if (item.equals(BundlesDT.NOTSET)) continue;

          // Do the straightforward update
          bin_to_total.get(bin).add(item);
          if (bin_to_total.get(bin).size() > max_bin_total) max_bin_total = bin_to_total.get(bin).size();

          // If record supports coloring, then update color values as well
          if (tab_to_color.containsKey(bundle.getTablet()) == false) {
            if (color_by != null && KeyMaker.tabletCompletesBlank(bundle.getTablet(), color_by)) tab_to_color.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), color_by));
            else                                                                                tab_to_color.put(bundle.getTablet(), null);
          }

          if (tab_to_color.get(bundle.getTablet()) != null) {
            String cbins[] = tab_to_color.get(bundle.getTablet()).stringKeys(bundle);
            for (int i=0;i<cbins.length;i++) {
              String cbin = cbins[i];

              if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, new HashSet<String>()); cbin_to_total.put(cbin,new HashSet<String>()); }
              bin_to_cbin_total.get(bin).get(cbin).add(item);
              cbin_to_total.get(cbin).add(item);
              bin_color_total.get(bin).add(item);
            }
          } else {
            String cbin = "No Color";

            if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, new HashSet<String>()); cbin_to_total.put(cbin, new HashSet<String>()); }
            bin_to_cbin_total.get(bin).get(cbin).add(item);
            cbin_to_total.get(cbin).add(item);
            bin_color_total.get(bin).add(item);
          }
        }
      }

      return bin_to_total.get(bin).size();
    }

    public Iterator<String> binIterator              ()                          { return bin_to_total.keySet().iterator(); }
    public double           totalNormalized          (String bin)                { if (max_bin_total == 0.0) max_bin_total = 1.0;
                                                                                   return bin_to_total.get(bin).size() / max_bin_total;}
    public double           total                    (String bin)                { return bin_to_total.get(bin).size(); }
    public double           binColorTotal            (String bin)                { return bin_color_total.get(bin).size(); }
    public double           total                    (String bin, String cbin)   { if (bin_to_cbin_total.get(bin).containsKey(cbin)) return bin_to_cbin_total.get(bin).get(cbin).size();
                                                                                   else                                              return 0.0; }
    public List<String>     getColorBinsSortedByCount()                          { 
      List<Sorter> sorter = new ArrayList<Sorter>();
      Iterator<String> it = cbin_to_total.keySet().iterator(); while (it.hasNext()) {
        String key = it.next(); double total = cbin_to_total.get(key).size(); sorter.add(new Sorter(key,total));
      }
      Collections.sort(sorter);
      List<String> ret = new ArrayList<String>();
      for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
      return ret;
    }
  }

  /**
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   * ==========================================================================================================================================================
   *
   * counter for counting scalars
   */
  class ScalarCounter implements Counter {

    Map<String,Double>             bin_to_total      = new HashMap<String,Double>();
    double                         max_bin_total     = 0.0;
    Map<String,Map<String,Double>> bin_to_cbin_total = new HashMap<String,Map<String,Double>>();
    Map<String,Double>             cbin_to_total     = new HashMap<String,Double>();
    Map<String,Double>             bin_color_total   = new HashMap<String,Double>();

    Map<Tablet,KeyMaker>            tab_to_color     = new HashMap<Tablet,KeyMaker>(),
                                    tab_to_count     = new HashMap<Tablet,KeyMaker>();

    public  boolean         hasBin(String bin)                                   { return bin_to_total.containsKey(bin); }

    public double           count                    (Bundle bundle, String bin) { 
      // Create data structures
      if (bin_to_total.containsKey(bin) == false) {
        bin_to_total.     put(bin, 0.0);
        bin_to_cbin_total.put(bin, new HashMap<String,Double>());
        bin_color_total.  put(bin, 0.0);
      }

      // Can this bundle (and associated tablet) count or not?
      if (tab_to_count.containsKey(bundle.getTablet()) == false) {
        if (KeyMaker.tabletCompletesBlank(bundle.getTablet(),count_by)) tab_to_count.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), count_by));
        else                                                            tab_to_count.put(bundle.getTablet(), null);
      }

      if (tab_to_count.get(bundle.getTablet()) != null) {
        int how_muches[] = tab_to_count.get(bundle.getTablet()).intKeys(bundle);

        for (int j=0;j<how_muches.length;j++) {
          double how_much = how_muches[j];

          // Do the straightforward update
          bin_to_total.put(bin, bin_to_total.get(bin) + how_much);
          if (bin_to_total.get(bin) > max_bin_total) max_bin_total = bin_to_total.get(bin);

          // If record supports coloring, then update color values as well
          if (tab_to_color.containsKey(bundle.getTablet()) == false) {
            if (color_by != null && KeyMaker.tabletCompletesBlank(bundle.getTablet(), color_by)) tab_to_color.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), color_by));
            else                                                                                 tab_to_color.put(bundle.getTablet(), null);
          }

          if (tab_to_color.get(bundle.getTablet()) != null) {
            String cbins[] = tab_to_color.get(bundle.getTablet()).stringKeys(bundle);
            for (int i=0;i<cbins.length;i++) {
              String cbin = cbins[i];

              if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, 0.0); cbin_to_total.put(cbin,0.0); }
              bin_to_cbin_total.get(bin).put(cbin, bin_to_cbin_total.get(bin).get(cbin) + how_much);
              cbin_to_total.put(cbin, cbin_to_total.get(cbin) + how_much);
              bin_color_total.put(bin, bin_color_total.get(bin) + how_much);
            }
          } else {
            String cbin = "No Color";

            if (bin_to_cbin_total.get(bin).containsKey(cbin) == false) { bin_to_cbin_total.get(bin).put(cbin, 0.0); cbin_to_total.put(cbin,0.0); }
            bin_to_cbin_total.get(bin).put(cbin, bin_to_cbin_total.get(bin).get(cbin) + how_much);
            cbin_to_total.put(cbin, cbin_to_total.get(cbin) + how_much);
            bin_color_total.put(bin, bin_color_total.get(bin) + how_much);
          }
        }
      }

      return bin_to_total.get(bin);
    }

    public Iterator<String> binIterator              ()                          { return bin_to_total.keySet().iterator(); }
    public double           totalNormalized          (String bin)                { if (max_bin_total == 0.0) max_bin_total = 1.0;
                                                                                   return bin_to_total.get(bin) / max_bin_total;}
    public double           total                    (String bin)                { return bin_to_total.get(bin); }
    public double           binColorTotal            (String bin)                { return bin_color_total.get(bin); }
    public double           total                    (String bin, String cbin)   { if (bin_to_cbin_total.get(bin).containsKey(cbin)) return bin_to_cbin_total.get(bin).get(cbin);
                                                                                   else                                              return 0.0; }
    public List<String>     getColorBinsSortedByCount()                          { 
      List<Sorter> sorter = new ArrayList<Sorter>();
      Iterator<String> it = cbin_to_total.keySet().iterator(); while (it.hasNext()) {
        String key = it.next(); double total = cbin_to_total.get(key); sorter.add(new Sorter(key,total));
      }
      Collections.sort(sorter);
      List<String> ret = new ArrayList<String>();
      for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
      return ret;
    }
  }

  /**
   * Class used to sort double values that correspond to a specific string.
   */
  class Sorter implements Comparable<Sorter> { String str; double d; public Sorter(String str, double d) { this.str = str; this.d = d; }
    /**
     * Compares one sorter to another.
     * 
     * @param  other other sorter to compare to
     * @return       -1, 0, 1 depending if other sorter is less than, equal to, or greater than
     */
    public int compareTo(Sorter other) {
      if      (d < other.d) return -1;
      else if (d > other.d) return  1;
      else                  return str.compareTo(other.str);
    }

    /**
     * Return the corresponding string value for this sorter.
     *
     * @return the string value for this sorter
     */
    public String toString() { return str; }
  }
}

