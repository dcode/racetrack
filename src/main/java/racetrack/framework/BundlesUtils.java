/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.framework;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import racetrack.gui.RT;
import racetrack.gui.RFC4180ImportDialog;

import racetrack.util.CSVReader;
import racetrack.util.Utils;

/**
 * Utilities for the classes in this file.
 *
 * @author  D. Trimm 
 * @version 1.0
 */
public class BundlesUtils {
  /**
   * Static string for report title
   */
  public static String REPORT_TITLE_STR       = "reportTitle",

  /**
   * Static string for report UUID
   */
                       REPORT_UUID_STR        = "reportUUID", 

  /**
   * Static string for report parent UUID
   */
                       REPORT_PARENT_UUID_STR = "reportParentUUID", 

  /**
   * Static string for report tags
   */
                       REPORT_TAGS_STR        = "tags", 

  /**
   * Static string for report text
   */
                       REPORT_TEXT_STR        = "reportText", 

  /**
   * Static string for report URL
   */
                       REPORT_URL_STR         = "reportURL", 

  /**
   * Static string for report source
   */
                       REPORT_SOURCE_STR      = "reportSource";

  /**
   * Return the full header for the reports tablet.
   *
   *@return full header for report tablet
   */
  public static String[] reportFullHeader() {
    String strs[] = new String[9];
      strs[0] = "timestamp";       strs[1] = "timestamp_end";
      strs[2] = REPORT_TITLE_STR;  strs[3] = REPORT_UUID_STR; strs[4] = REPORT_PARENT_UUID_STR;
      strs[5] = REPORT_TAGS_STR;   strs[6] = REPORT_TEXT_STR; strs[7] = REPORT_URL_STR;
      strs[8] = REPORT_SOURCE_STR;
    return strs;
  }

  /**
   * Return the partial header (minus the timestamp_end field) for the reports tablet.
   *
   *@return partial header for report tablet
   */
  public static String[] reportPartialHeader() {
    String strs[] = new String[8];
      strs[0] = "timestamp";
      strs[1] = REPORT_TITLE_STR;  strs[2] = REPORT_UUID_STR; strs[3] = REPORT_PARENT_UUID_STR;
      strs[4] = REPORT_TAGS_STR;   strs[5] = REPORT_TEXT_STR; strs[6] = REPORT_URL_STR;
      strs[7] = REPORT_SOURCE_STR;
    return strs;
  }

  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  file       file to parse
   * @param  max_lines  maximum number of lines to parse, 0 indicates unlimited
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, File file, int max_lines) { return parse(bundles, null, file, null, max_lines); }

  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  rt         application class
   * @param  file       file to parse
   * @param  appconfigs lines from the parsed file that may indicate application configuration information (output)
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, RT rt, File file, List<String> appconfs) { return parse(bundles, rt, file, appconfs, 0); }

  /**
   * Parse a data file and load it into the application.
   *
   * @param  bundles    application data (output)
   * @param  rt         application class
   * @param  file       file to parse
   * @param  appconfigs lines from the parsed file that may indicate application configuration information (output)
   * @param  max_lines  maximum number of lines to parse, 0 indicates unlimited
   *
   * @return         set of the bundles (records) that were loaded
   */
  public static Set<Bundle> parse(Bundles bundles, RT rt, File file, List<String> appconfs, int max_lines) {
    // Check for Microsoft Excel files -- if so, use the dialog importer
    if (RFC4180Importer.matchesRFC4180(file)) {
      RFC4180ImportDialog import_dialog = new RFC4180ImportDialog(rt.getControlPanel(), file);
      Set<Bundles> imported_bundles = import_dialog.getImportedBundles();

      Set<Bundle>  my_bundles = new HashSet<Bundle>();

      Iterator<Bundles> it_bundles = imported_bundles.iterator(); while (it_bundles.hasNext()) {
        Bundles this_bundles = it_bundles.next();

        Iterator<Tablet> it_tab = this_bundles.tabletIterator(); while (it_tab.hasNext()) {
          Tablet tablet = it_tab.next();

          // Replicate the table into the application's bundles data structure
          // - Find or create a tablet
          int   fields_i[] = tablet.getFields(); List<String> hdrs_al = new ArrayList<String>();
          for (int i=0;i<fields_i.length;i++) {
            if (fields_i[i] >= 0) {
              hdrs_al.add(this_bundles.getGlobals().fieldHeader(fields_i[i]));
              System.err.println(hdrs_al.get(hdrs_al.size()-1));
            }
          }
          if (tablet.hasTimeStamps()) hdrs_al.add("timestamp");
          if (tablet.hasDurations())  hdrs_al.add("timestamp_end");
          String hdrs[] = new String[hdrs_al.size()]; for (int i=0;i<hdrs.length;i++) hdrs[i] = hdrs_al.get(i);
          Tablet persist_tablet = bundles.findOrCreateTablet(hdrs);
          // - Iterate over each bundle and add it
          Iterator<Bundle> it_bun = tablet.bundleSet().iterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next(); Map<String,String> attr = new HashMap<String,String>();
            for (int i=0;i<fields_i.length;i++) {
              if (fields_i[i] >= 0) {
              attr.put(this_bundles.getGlobals().fieldHeader(fields_i[i]), bundle.toString(fields_i[i]));
              }
            }

            if        (tablet.hasDurations())  { persist_tablet.addBundle(attr, Utils.exactDate(bundle.ts0()), Utils.exactDate(bundle.ts1()));
            } else if (tablet.hasTimeStamps()) { persist_tablet.addBundle(attr, Utils.exactDate(bundle.ts0()));
            } else                             { persist_tablet.addBundle(attr); }
          }

          // Add all to a return set
          my_bundles.addAll(tablet.bundleSet());
        }
      }

      // Re-run the transforms for the fast lookup tables
      bundles.getGlobals().resetTransforms();

      // Clear out the datatype cache
      BundlesDT.clearDataTypesCache();

      return my_bundles;
    }

    // Determine if the delimiter is commas, tabs, or pipes
    BufferedReader in = null; Map<String,Map<Integer,Integer>> map = new HashMap<String,Map<Integer,Integer>>();
    map.put(",",  new HashMap<Integer,Integer>()); map.put("|",  new HashMap<Integer,Integer>()); map.put("\t", new HashMap<Integer,Integer>());
    try {
      if (file.getName().toLowerCase().endsWith(".gz")) in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file)))); 
      else                                              in = new BufferedReader(new FileReader(file)); 
      
      // Parse the first ten lines or so to determine the delimiter
      String line; int i = 0;
      while ((line = in.readLine()) != null && line.equals("") == false && i < 10) {
        i++;
        int commas = count(line, ','), pipes  = count(line, '|'), tabs   = count(line, '\t');
        // System.err.println("commas=" + commas + " pipes=" + pipes + " tabs=" + tabs + " \"" + line + "\"");
        if (commas > 0) { if (!map.get(",").containsKey(commas))  map.get(",").put(commas, 0);  map.get(",").put(commas,  map.get(",").get(commas)  + 1); }
        if (pipes  > 0) { if (!map.get("|").containsKey(pipes))   map.get("|").put(pipes,  0);  map.get("|").put(pipes,   map.get("|").get(pipes)   + 1); }
        if (tabs   > 0) { if (!map.get("\t").containsKey(tabs))   map.get("\t").put(tabs,  0);  map.get("\t").put(tabs,   map.get("\t").get(tabs)   + 1); }
      }
    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
      ioe.printStackTrace(System.err);
    } finally {
      if (in != null) try { in.close(); } catch (IOException ioe) { } 
    }

    // Determine the strongest pattern for the delimiter -- checks for comma, pipe, and tab
    String delims = ","; int c_strength, p_strength, t_strength;
    if (map.get(","). keySet().size() == 1) { c_strength = map.get(","). get(map.get(","). keySet().iterator().next()); } else c_strength = -1;
    if (map.get("|"). keySet().size() == 1) { p_strength = map.get("|"). get(map.get("|"). keySet().iterator().next()); } else p_strength = -1;
    if (map.get("\t").keySet().size() == 1) { t_strength = map.get("\t").get(map.get("\t").keySet().iterator().next()); } else t_strength = -1;

    if      (c_strength > p_strength && c_strength > t_strength) delims = ",";
    else if (p_strength > c_strength && p_strength > c_strength) delims = "|";
    else if (t_strength > p_strength && t_strength > c_strength) delims = "\t";
    // System.err.println("c=" + c_strength + " | p=" + p_strength + " | t=" + t_strength); // DEBUG

    // Create the set for storage
    Set<Bundle> set = new HashSet<Bundle>();
    // Execute the parser
    CSVParser csv_parser = null;
    try { 
      // System.err.println("Creating CSVReader..."); // DEBUG
      new CSVReader(file, csv_parser = new CSVParser(bundles, rt, set, max_lines), delims, true); 
      // System.err.println("  CSVReader Complete!"); // DEBUG
    } catch (IOException ioe) { 
      System.err.println("IOException: " + ioe); ioe.printStackTrace(System.err); 
    }
    // Make sure that the class had a chance to add the lists to main class
    if (csv_parser != null) { csv_parser.addListsToRT(); if (appconfs != null) appconfs.addAll(csv_parser.getAppConfigs()); }

    // Re-run the transforms for the fast lookup tables
    bundles.getGlobals().resetTransforms();
    return set;
  }

  /**
   * Count the number of character occurences within a string.
   *
   *@param str string to examine
   *@param c   charater to count
   *
   *@return number of times character occurs in string
   */
  public static int count(String str, char c) {
    int sum = 0;
    for (int i=0;i<str.length();i++) if (str.charAt(i) == c) sum++;
    return sum;
  }

  /**
   * Return a deduped version of the bundle set that is passed in.
   *
   *@param orig original set of records -- probably contains duplicates
   *
   *@return record set not containing duplicates
   */
  public static Set<Bundle> dedupe(Set<Bundle> orig) {
    Set<Bundle> ret = new HashSet<Bundle>(); Set<String> already_seen = new HashSet<String>();

    Iterator<Bundle> it = orig.iterator(); while (it.hasNext()) {
      Bundle bundle         = it.next();
      String canonical_form = canonicalStringFormat(bundle);
      if (already_seen.contains(canonical_form) == false) { ret.add(bundle); already_seen.add(canonical_form); }
    }

    return ret;
  }

  /**
   * Create a single string representing the bundle -- rep is used for deduping.
   *
   *@param bundle
   *
   *@return string that can be used to identify duplicate bundles
   */
  public static String canonicalStringFormat(Bundle bundle) {
    StringBuffer sb = new StringBuffer(); // contains the increment string

    Tablet tablet = bundle.getTablet(); sb.append(tablet.fileHeader());
    int flds[] = tablet.getFields();
    for (int i=0;i<flds.length;i++) { if (flds[i] >= 0) sb.append(BundlesDT.DELIM + Utils.encToURL(bundle.toString(i))); }
    if (tablet.hasTimeStamps()) sb.append(BundlesDT.DELIM + Utils.exactDate(bundle.ts0()));
    if (tablet.hasDurations())  sb.append(BundlesDT.DELIM + Utils.exactDate(bundle.ts1()));

    return sb.toString();
  }

  /**
   * Convert a scalar or float field into a double array.
   *
   *@param bundles records
   *@param field   field in bundles -- either a
   */
  public static double[] asDoubleArray(Bundles bundles, String field) {
    // Figure out which tablets have the field
    List<String> strs = new ArrayList<String>(); Map<String,Tablet> tabs = new HashMap<String,Tablet>();  int sum = 0;
    Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next();
      if (KeyMaker.tabletCompletesBlank(tablet, field)) { tabs.put("" + tablet, tablet); strs.add("" + tablet); sum += tablet.size(); }
    }

    // Sort them to be consistent
    Collections.sort(strs);

    // Allocate the array
    double d[] = new double[sum]; int d_i = 0;

    // Add the values into the array
    BundlesG globals = bundles.getGlobals();
    for (int i=0;i<strs.size();i++) {
      Tablet   tablet = tabs.get(strs.get(i));
      KeyMaker km     = new KeyMaker(tablet, field);

      Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(globals.fieldIndex(field));
      if        (datatypes.contains(BundlesDT.DT.FLOAT)   && datatypes.size() == 1) {
        Iterator<Bundle> it = tablet.bundleIterator(); while (it.hasNext()) {
          int ints[] = km.intKeys(it.next());
          d[d_i++] = Float.intBitsToFloat(ints[0]);
        }
      } else if (datatypes.contains(BundlesDT.DT.INTEGER) && datatypes.size() == 1) {
        Iterator<Bundle> it = tablet.bundleIterator(); while (it.hasNext()) {
          int ints[] = km.intKeys(it.next());
          d[d_i++] = ints[0];
        }
      } else throw new RuntimeException("Field \"" + field + "\" is not integer or float");
    }

    return d;
  }

  /**
   * Convert a scalar or float field into a set of double arrays.  This method keeps the
   * elements paired with their same bundle.
   *
   *@param bundles records
   *@param fields  list of fields -- will be the order of the arrays
   */
  public static double[][] asDoubleArray(Bundles bundles, List<String> fields) {
    // Figure out which tablets have the field
    int size = 0; List<Tablet> tablets = new ArrayList<Tablet>(); Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
      Tablet  tablet        = it_tab.next();
      boolean all_satisfied = true;
      for (int i=0;i<fields.size();i++) if (KeyMaker.tabletCompletesBlank(tablet, fields.get(i)) == false) all_satisfied = false;
      if (all_satisfied) { tablets.add(tablet); size += tablet.size(); }
    }

    // Determine how to parse the fields
    boolean parse_float[] = new boolean[fields.size()]; BundlesG globals = bundles.getGlobals();
    for (int i=0;i<fields.size();i++) {
      Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(globals.fieldIndex(fields.get(i)));
      if      (datatypes.contains(BundlesDT.DT.FLOAT)   && datatypes.size() == 1) parse_float[i] = true;
      else if (datatypes.contains(BundlesDT.DT.INTEGER) && datatypes.size() == 1) parse_float[i] = false;
      else throw new RuntimeException("Field \"" + fields.get(i) + "\" is not integer or float");
    }

    // Create the arrays
    double d[][] = new double[fields.size()][size];

    // Fill the arrays
    int index = 0;
    for (int i_tab=0;i_tab<tablets.size();i_tab++) {
      // Create key makers for all of the fields
      KeyMaker kms[] = new KeyMaker[fields.size()];
      for (int i=0;i<kms.length;i++) kms[i] = new KeyMaker(tablets.get(i_tab), fields.get(i));

      // Go through the records
      Iterator<Bundle> it = tablets.get(i_tab).bundleIterator(); while (it.hasNext()) {
        Bundle bundle = it.next();
        for (int i=0;i<fields.size();i++) {
          double value = 0.0; 

          int ints[] = kms[i].intKeys(bundle);

          // The assumption should be that every record has a filled in field... but we'll check anyway...
          if (ints != null && ints.length > 0) {
            if (parse_float[i]) value = Float.intBitsToFloat(ints[0]);
            else                value = ints[0];
          }

          d[i][index] = value;
        }
        index++;
      }
    }

    return d;
  }


  /**
   * Create a map for a primary (and option secondary) field to the set of bundles that represent
   * that pairing.
   */
  private static Map<String,Set<Bundle>> constructJoinLookup(Tablet tab, String pri, String sec, Set<String> only_keep) {
    Map<String,Set<Bundle>> map = new HashMap<String,Set<Bundle>>();

    // System.err.println("constructJoinLookup() - " + tab);
    
    // Gather the values that will be joined... if it doesn't exist in the mod_tab, then it doesn't matter
    KeyMaker         km_pri = null;                  km_pri = new KeyMaker(tab, pri);
    KeyMaker         km_sec = null; if (sec != null) km_sec = new KeyMaker(tab, sec);

    Iterator<Bundle> it = tab.bundleIterator(); while (it.hasNext()) {
      Bundle bundle = it.next();
      String keys_pri[] = km_pri.stringKeys(bundle);
      if (km_sec == null) {
        for (int i=0;i<keys_pri.length;i++) {
          // System.err.println("  keys_pri[" + i + "] = " + keys_pri[i] + " for " + bundle);
          String enc = Utils.encToURL(keys_pri[i]);

          if (only_keep != null && only_keep.contains(enc) == false) continue;

          if (map.containsKey(enc) == false) map.put(enc, new HashSet<Bundle>());
          map.get(enc).add(bundle);
        }
      } else {
        String keys_sec[] = km_sec.stringKeys(bundle);
        for (int i=0;i<keys_pri.length;i++) {
          for (int j=0;j<keys_sec.length;j++) {
            String enc = Utils.encToURL(keys_pri[i]) + "|" + Utils.encToURL(keys_sec[j]);

            if (only_keep != null && only_keep.contains(enc) == false) continue;

            if (map.containsKey(enc) == false) map.put(enc, new HashSet<Bundle>());
            map.get(enc).add(bundle);
          }
        }
      }
    }
    // System.err.println("  Returning map.keySet() = " + map.keySet());
    return map;
  }

  /**
   * 
   */
  public static void tabletJoinAddColumns(Bundles root_bundles,
                                          Tablet  mod_tab, String pri_to_mod, String sec_to_mod,
                                          Tablet  frm_tab, String pri_from,   String sec_from,
                                          List<String> to_add_cols, String suffix, long time_l) {
    if (suffix == null) suffix = ""; // Just make it empty if null

    // Go through the from tab... only store off matches 
    Map<String,Set<Bundle>> mod_map = constructJoinLookup(mod_tab, pri_to_mod, sec_to_mod, null),
                            frm_map = constructJoinLookup(frm_tab, pri_from,   sec_from,   mod_map.keySet());

    // -- Method to add a column (field) to a set of bundles
    // root_bundles.getGlobals().setField(toset_bundles, root_bundles, field, value_tf.getText());

    // Create all the key makers
    Map<String,KeyMaker> kms = new HashMap<String,KeyMaker>(); Iterator<String> it = to_add_cols.iterator(); while (it.hasNext()) {
      String field = it.next(); kms.put(field, new KeyMaker(frm_tab, field));
    }

    // Iterate over the from map... should be the same size or less than the mod map
    it = frm_map.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); if (mod_map.containsKey(key)) { // should always be true...
        Set<Bundle> mod_set = mod_map.get(key), // Records to modify
                    frm_set = frm_map.get(key); // Records to pull from for the modifications
        //
        // No Time Considerations
        //
        if (time_l == 0L) { 
          Map<String,Map<String,Integer>> col_map = new HashMap<String,Map<String,Integer>>(); // Keep fields => values => counts

          // Gather up all the columns
          Iterator<Bundle> it_b = frm_set.iterator(); while (it_b.hasNext()) {
            Bundle bundle = it_b.next(); Iterator<String> it_f = kms.keySet().iterator(); while (it_f.hasNext()) {
              String field = it_f.next(); String values[] = kms.get(field).stringKeys(bundle); 
              if (col_map.containsKey(field) == false) col_map.put(field, new HashMap<String,Integer>());
              if (values != null && values.length > 0 && values[0].equals(BundlesDT.NOTSET) == false) {
                for (int i=0;i<values.length;i++) {
                  if (col_map.get(field).containsKey(values[i]) == false) col_map.get(field).put(values[i], 0);
                  col_map.get(field).put(values[i], col_map.get(field).get(values[i]) + 1);
                }
              }
            }
          }

          // Apply the fields to the subset of bundles ... not a good bulk version...
          Bundles to_apply = mod_tab.getBundles().subset(mod_set);
          Iterator<String> it_f = col_map.keySet().iterator(); while (it_f.hasNext()) {
            String field = it_f.next(); String value = Utils.findMaxString(col_map.get(field));
            if (value != null) {

              // Get the modified field name... make sure it's all caps (a scalar field) if the origination field is all caps
              String field_mod = field+suffix; 
              if (root_bundles.getGlobals().isScalar(root_bundles.getGlobals().fieldIndex(field))) field_mod = field_mod.toUpperCase();

              // Add and set the column
              root_bundles.getGlobals().setField(to_apply, root_bundles, field_mod, value);
            }
          }

        //
        // Time Considerations -- find the closest record in time and then apply the time constraint
        //
        } else {

          // For every mod bundle...
          Iterator<Bundle> it_mod_bundle = mod_set.iterator(); while (it_mod_bundle.hasNext()) {
            Bundle mod_bundle = it_mod_bundle.next();

            // Find the closest frm bundle in time
            Iterator<Bundle> it_frm_bundle = frm_set.iterator();
            Bundle frm_bundle = it_frm_bundle.next(); Bundle closest_frm_bundle = frm_bundle;
            while (it_frm_bundle.hasNext()) {
              frm_bundle = it_frm_bundle.next();
              if (Math.abs(frm_bundle.ts0() - mod_bundle.ts0()) < (closest_frm_bundle.ts0() - mod_bundle.ts0())) closest_frm_bundle = frm_bundle;
            }

            // If within the constraint timeframe, then tag the row
            if (Math.abs(closest_frm_bundle.ts0() - mod_bundle.ts0()) <= time_l) {
              Map<String,String> fields_map = new HashMap<String,String>();

              // Gather up the columns ... choose the first value found (hopefully there's only one... but that's the limits with this type of join)
              Iterator<String> it_f = kms.keySet().iterator(); while (it_f.hasNext()) {
                String field = it_f.next(); String values[] = kms.get(field).stringKeys(closest_frm_bundle); 
                if (values != null && values.length > 0 && values[0].equals(BundlesDT.NOTSET) == false) {
                  fields_map.put(field,values[0]);
                }
              }

              // Apply tags to this specific bundle
              Set<Bundle> set_of_one = new HashSet<Bundle>(); set_of_one.add(mod_bundle); Bundles to_apply = mod_tab.getBundles().subset(set_of_one);
              it_f = fields_map.keySet().iterator(); while (it_f.hasNext()) {
                String field = it_f.next(); String value = fields_map.get(field);

                // Get the modified field name... make sure it's all caps (a scalar field) if the origination field is all caps
                String field_mod = field+suffix; 
                if (root_bundles.getGlobals().isScalar(root_bundles.getGlobals().fieldIndex(field))) field_mod = field_mod.toUpperCase();

                // Add and set the column
                root_bundles.getGlobals().setField(to_apply, root_bundles, field_mod, value);
              }
            }
          }
        }
      }
    }
  }

  /**
   * 
   *@param sec_to_mod second field to use from the mod_tab ... can be null -- i.e., no second field
   *@param sec_from   second field to use from the frm_tab ... can be null -- i.e., no second field
   *@param time_l     time constraints... if 0L, then no time constraints... or the tablets don't have timestamps
   */
  public static void tabletJoinTagRows(   Bundles root_bundles,
                                          Tablet  mod_tab, String pri_to_mod, String sec_to_mod,
                                          Tablet  frm_tab, String pri_from,   String sec_from,
                                          List<String> to_add_cols, String suffix, long time_l) {
    if (suffix == null) suffix = ""; // Just make it empty if null

    // Go through the from tab... only store off matches 
    Map<String,Set<Bundle>> mod_map = constructJoinLookup(mod_tab, pri_to_mod, sec_to_mod, null),
                            frm_map = constructJoinLookup(frm_tab, pri_from,   sec_from,   mod_map.keySet()); // filtered by the mod_map set


    // Create all the key makers
    Map<String,KeyMaker> kms = new HashMap<String,KeyMaker>(); Iterator<String> it = to_add_cols.iterator(); while (it.hasNext()) {
      String field = it.next(); kms.put(field, new KeyMaker(frm_tab, field));
    }

    // Iterate over the from map... should be the same size or less than the mod map
    it = frm_map.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); if (mod_map.containsKey(key)) { // should always be true...
        Set<Bundle> mod_set = mod_map.get(key), // Records to modify
                    frm_set = frm_map.get(key); // Records to pull from for the modifications
        //
        // No Time Considerations
        //
        if (time_l == 0L) { 
          Set<String> tags = new HashSet<String>();

          // Gather up all the tags
          Iterator<Bundle> it_b = frm_set.iterator(); while (it_b.hasNext()) {
            Bundle bundle = it_b.next(); Iterator<String> it_f = kms.keySet().iterator(); while (it_f.hasNext()) {
              String field = it_f.next(); String values[] = kms.get(field).stringKeys(bundle); 
              if (values != null && values.length > 0 && values[0].equals(BundlesDT.NOTSET) == false) {
                for (int i=0;i<values.length;i++) tags.add(field + suffix + "=" + Utils.encToURL(values[i]));
              }
            }
          }

          // Roll the tags into a single string
          StringBuffer sb = new StringBuffer(); Iterator<String> it_tags = tags.iterator(); while (it_tags.hasNext()) { if (sb.length() > 0) sb.append("|"); sb.append(it_tags.next()); }
          String tags_str = sb.toString();

          // Apply them to this subset of bundles ... not a replace... so chance that this is just going to add a lot of stuff if run repeatedly.
          // ... inefficient since i'm calling the bulk version... but don't want to further complicate the framework by creating new functions
          Bundles to_apply = mod_tab.getBundles().subset(mod_set);
          root_bundles.addTags(to_apply, tags_str);

        //
        // Time Considerations -- find the closest record in time and then apply the time constraint
        //
        } else {

          // For every mod bundle...
          Iterator<Bundle> it_mod_bundle = mod_set.iterator(); while (it_mod_bundle.hasNext()) {
            Bundle mod_bundle = it_mod_bundle.next();

            // Find the closest frm bundle in time
            Iterator<Bundle> it_frm_bundle = frm_set.iterator();
            Bundle frm_bundle = it_frm_bundle.next(); Bundle closest_frm_bundle = frm_bundle;
            while (it_frm_bundle.hasNext()) {
              frm_bundle = it_frm_bundle.next();
              if (Math.abs(frm_bundle.ts0() - mod_bundle.ts0()) < (closest_frm_bundle.ts0() - mod_bundle.ts0())) closest_frm_bundle = frm_bundle;
            }

            // If within the constraint timeframe, then tag the row
            if (Math.abs(closest_frm_bundle.ts0() - mod_bundle.ts0()) <= time_l) {
              // Gather up the tags
              Set<String> tags = new HashSet<String>();
              Iterator<String> it_f = kms.keySet().iterator(); while (it_f.hasNext()) {
                String field = it_f.next(); String values[] = kms.get(field).stringKeys(closest_frm_bundle); 
                if (values != null && values.length > 0 && values[0].equals(BundlesDT.NOTSET) == false) {
                  for (int i=0;i<values.length;i++) tags.add(field + suffix + "=" + Utils.encToURL(values[i]));
                }
              }

              // Roll the tags into a single string
              StringBuffer sb = new StringBuffer(); Iterator<String> it_tags = tags.iterator(); while (it_tags.hasNext()) { if (sb.length() > 0) sb.append("|"); sb.append(it_tags.next()); }
              String tags_str = sb.toString();
              
              // Apply tags to this specific bundle ... not a replace... so chance that this is just going to add a lot of stuff if run repeatedly.
              // ... really inefficient since i'm calling the bulk version... but don't want to further complicate the framework by creating new functions
              Set<Bundle> set_of_one = new HashSet<Bundle>(); set_of_one.add(mod_bundle);
              Bundles to_apply = mod_tab.getBundles().subset(set_of_one);
              root_bundles.addTags(to_apply, tags_str);
            }
          }
        }
      }
    }
  }
}

