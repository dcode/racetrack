/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.framework;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import racetrack.util.CacheManager;
import racetrack.util.EntityExtractor;
import racetrack.util.Interval;
import racetrack.util.SubText;
import racetrack.util.TimeStamp;
import racetrack.util.Utils;

/**
 * A dynamic post processor to run an entity extraction across unstructured text.
 *
 * @author  D. Trimm
 * @version 1.0
 */
class EntityExtractorPostProcess implements PostProc {
  /**
   * Private constructor
   */
  private EntityExtractorPostProcess() {
    cache = new HashMap<String,String[]>();
    CacheManager.registerCache("EntityExtractorPostProcess", cache);
  }

  /**
   * Get the singleton for this class.
   */
  public static EntityExtractorPostProcess getInstance() {
    if (singleton == null) singleton = new EntityExtractorPostProcess();
    return singleton;
  }

  /**
   * Singleton instance
   */
  private static EntityExtractorPostProcess singleton = null;

  /**
   * Return the corresponding datatype for this postprocessor.
   *
   * @return post process datatype
   */
  @Override
  public Set<BundlesDT.DT> type() { Set<BundlesDT.DT> set = new HashSet<BundlesDT.DT>(); set.add(BundlesDT.DT.UTEXT); return set; }

  /**
   * Post process a string into it's respective data types based on a transform.  Assumption here
   * is to make everything lowercase.  Also, the extractor ignore both timestamps and intervals.
   *
   * @param  str  string to post process
   * @return post processed version of this data type
   */
  @Override
  public String[]     postProcess(String str) { 
    if (cache.containsKey(str) == false) {
      // Extract the entities
      List<SubText> subtexts = EntityExtractor.list(str); List<String> ents = new ArrayList<String>();

      // Go through them and add appropriate ones to the return structure
      Iterator<SubText> it = subtexts.iterator(); while (it.hasNext()) { 
        SubText subtext = it.next();

        // Don't include timestamps or intervals
        if (subtext instanceof Interval || subtext instanceof TimeStamp) { 
        } else {
          String to_str = subtext.toString().toLowerCase();
          ents.add(to_str);
        } 
      }

      // Transform it back to an array and cache it
      String as_array[] = new String[ents.size()];
      for (int i=0;i<as_array.length;i++) as_array[i] = ents.get(i);
      cache.put(str, as_array);
    }
    return cache.get(str);
  }

  /**
   * Cache for the extraction
   */
  protected Map<String,String[]> cache = null;
}

