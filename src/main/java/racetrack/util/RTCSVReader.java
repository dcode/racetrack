/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.util.zip.GZIPInputStream;

import racetrack.framework.BundlesDT;

/**
 * Read a CSV encoded using the URL encoding scheme that racetrack uses.
 * - Files ending with a .gz will be automatically unzipped
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class RTCSVReader {
  /**
   * Consumer for the parsed tokens
   */
  private CSVTokenConsumer consumer;

  /**
   * Flag to indicate that parsing should continue
   */
  private boolean          keep_going = true;

  /**
   * Read the file, tokenize the lines, perform the URL decode and then push to the convsumer.
   *
   *@param file         csv file to parse
   *@param consumer     consumer to direct tokens to
   */
  public RTCSVReader(File file, CSVTokenConsumer consumer) throws IOException {
    this.consumer = consumer; BufferedReader in = null;

    // Open the file
    if (file.getName().toLowerCase().endsWith(".gz")) in = new BufferedReader(new InputStreamReader(new GZIPInputStream(new FileInputStream(file))));
    else                                              in = new BufferedReader(new FileReader(file));

    // Read the lines, tokenize them, and push them to the consumer
    String line; int line_no = 0; 
    while ((line = in.readLine()) != null && keep_going) { 
      line_no++;

      if (line.startsWith("#")) { consumer.commentLine(line);
      } else {
        // Break into tokens ... since it's URL encoded, there shouldn't be commas within the cells
        String tokens[] = Utils.tokenize(line, ",");
        // Decode the tokens
        for (int i=0;i<tokens.length;i++) tokens[i] = Utils.decFmURL(tokens[i]);
        // Push to the consumer
        keep_going = consumer.consume(tokens, line, line_no);
      }
    }

    in.close();
  }

  /**
   * Read the file off of the command line, parse them and produce stats...
   */
  public static void main(String args[]) {
    try {
      for (int i=0;i<args.length;i++) {
        CSVStats stats = new CSVStats();
        new RTCSVReader(new File(args[i]), stats);
        stats.printStats(args[i], System.err);
      }

    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
    }
  }
}

/**
 * Stats Found...
 */
class CSVStats implements CSVTokenConsumer {
  int                           comment_lines        = 0, 
                                header_lines         = 0, 
                                record_lines         = 0,
                                blank_lines          = 0,
                                mismatched_lines     = 0;
  List<String>                  headers              = new ArrayList<String>();
  Map<String,Set<BundlesDT.DT>> field_to_datatypes   = new HashMap<String,Set<BundlesDT.DT>>();
  Map<String,Integer>           header_to_row_counts = new HashMap<String,Integer>();
  String                        in_header            = null;
  String                        fields[]             = null;

  /**
   *
   */
  public void printStats(String filename, PrintStream out) {
    out.println("\n**\n** Stats for File \"" + filename + "\"\n**\n");

    out.println("headers = " + header_lines + " | records = " + record_lines + " | blanks = " + blank_lines + " | comments = " + 
                comment_lines + " | mismatches = " + mismatched_lines + "\n");

    out.println("header info");
    out.println("-----------");
    List<String> sort = new ArrayList<String>(); sort.addAll(header_to_row_counts.keySet()); Collections.sort(sort);
    for (int i=0;i<sort.size();i++) {
      String hdr = sort.get(i);
      out.println(spacer("" + header_to_row_counts.get(hdr), 20) + hdr);
    }
    out.println();
    out.println("field info");
    out.println("==========");
    sort = new ArrayList<String>(); sort.addAll(field_to_datatypes.keySet()); Collections.sort(sort);
    for (int i=0;i<sort.size();i++) {
      String field = sort.get(i);
      out.println(spacer(field, 20) + field_to_datatypes.get(field));
    }
    out.println();
  }

  private String spacer(String str, int pad) { while (str.length() < pad) str += " "; return str; }

  /**
   * Tabulate various stats...
   */
  public boolean consume(String tokens[], String line, int line_no) {
    if        (in_header == null && tokens.length == 0)             {                                    blank_lines++;
    } else if (in_header == null && tokens.length >  0)             { in_header = line; fields = tokens; header_lines++;
    } else if (in_header != null && tokens.length == 0)             { in_header = null; fields = null;   blank_lines++;
    } else if (in_header != null && tokens.length != fields.length) {                                    mismatched_lines++;
    } else {

      if (header_to_row_counts.containsKey(in_header) == false) header_to_row_counts.put(in_header, 1);
      else                                                      header_to_row_counts.put(in_header, header_to_row_counts.get(in_header)+1);

      for (int i=0;i<fields.length;i++) {
        if (field_to_datatypes.containsKey(fields[i]) == false) field_to_datatypes.put(fields[i], new HashSet<BundlesDT.DT>());
        BundlesDT.DT datatype = BundlesDT.getEntityDataType(tokens[i]);
        if (datatype != null) field_to_datatypes.get(fields[i]).add(datatype);
      }
      record_lines++;
    }

    return true;
  }

  /**
   * Comment lines... just record the number of lines...
   */
  public void commentLine(String line) { comment_lines++; }
}

