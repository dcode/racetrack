/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.jayway.jsonpath.Configuration;

import racetrack.kb.EntityTag;

/**
 *
 */
public class JSONToTreeFile {
  /**
   *
   */
  public JSONToTreeFile(String json_str) {
    Object document_object = Configuration.defaultConfiguration().jsonProvider().parse(json_str);
    recurse(document_object, "root", "root_0", 1);

    long millis = System.currentTimeMillis();

    Iterator<String> it;

    // Tree-like stuff...
    System.out.println("parent,parent_local,child,child_local,DEPTH,type,name");
    it = branches.iterator(); while (it.hasNext()) System.out.println(it.next());

    // Arrays...
    System.out.println();
    System.out.println("parent,parent_local,child_local,arrayelement,DEPTH,type");
    it = arrays.iterator(); while (it.hasNext()) System.out.println(it.next());

    // Leaves...
    System.out.println();
    System.out.println("branch,leaf,DEPTH,type");
    it = leaves.iterator(); while (it.hasNext()) System.out.println(it.next());

    // Entity names...  local naming
    System.out.println();
    System.out.println(EntityTag.getFileHeader());

    // Names...
    it = entity_name_map.keySet().iterator(); while (it.hasNext()) {
      String node_str       = it.next();
      System.out.println(UUID.randomUUID() + "," +
                         Utils.encToURL(node_str) + "," +
                         "name="  + Utils.encToURL(entity_name_map.get(node_str)) + "," +
                         Utils.exactDate(EntityTag.t0_forever) + "," +
                         Utils.exactDate(EntityTag.t1_forever) + "," + 
                         Utils.exactDate(millis) + "," +
                         "JSONToTreeFile");
    }

    // Depths...
    it = entity_depth_map.keySet().iterator(); while (it.hasNext()) {
      String node_str       = it.next();
      System.out.println(UUID.randomUUID() + "," +
                         Utils.encToURL(node_str) + "," +
                         "depth=" + entity_depth_map.get(node_str) + "," +
                         Utils.exactDate(EntityTag.t0_forever) + "," +
                         Utils.exactDate(EntityTag.t1_forever) + "," + 
                         Utils.exactDate(millis) + "," +
                         "JSONToTreeFile");
    }

    // Resolutions...
    it = resolution.keySet().iterator(); while (it.hasNext()) {
      String node_str       = it.next();
      System.out.println(UUID.randomUUID() + "," +
                         Utils.encToURL(node_str) + "," +
                         "setting=" + Utils.encToURL(resolution.get(node_str)) + "," +
                         Utils.exactDate(EntityTag.t0_forever) + "," +
                         Utils.exactDate(EntityTag.t1_forever) + "," + 
                         Utils.exactDate(millis) + "," +
                         "JSONToTreeFile");
    }
  }

  /**
   * Final leaves
   */
  Set<String> leaves = new HashSet<String>(),

  /**
   * Branches
   */

              branches = new HashSet<String>(),

  /**
   * Arrays
   */
              arrays = new HashSet<String>();

  /**
   * Tree string equivalent to the local name
   */
  Map<String,String> entity_name_map = new HashMap<String,String>();

  /**
   * Entity depth mapping
   */
  Map<String,Integer> entity_depth_map = new HashMap<String,Integer>();

  /**
   * Resolution of a value (leaves...)
   */
  Map<String,String> resolution = new HashMap<String,String>();

  /**
   * Recurse the tree...
   */
  private void recurse(Object obj, String prefix, String prefix_local, int depth) {
    if        (obj instanceof List) {
      List as_list = (List) obj;
      for (int i=0;i<as_list.size();i++) {
        String array_ref = prefix + "_" + i;
        arrays.add(Utils.encToURL(prefix)                           + "," + 
                   Utils.encToURL(prefix_local)                     + "," +
                   Utils.encToURL(prefix_local + "_array_" + depth) + "," +
                   Utils.encToURL(array_ref)                        + "," + 
                   depth                                            + "," + 
                   "array");
        entity_name_map.put(array_ref, "array" + i);
        entity_depth_map.put(array_ref, depth);
        entity_depth_map.put(prefix_local + "_array_" + depth, depth);
        recurse(as_list.get(i), array_ref, prefix_local + "_array_" + depth, depth+1);
      }

    } else if (obj instanceof Map)  {
      Map as_map = (Map) obj;
      Iterator it = as_map.keySet().iterator(); while (it.hasNext()) {
        Object key = it.next();
        String branch_ref = prefix + "_" + ((String) key);
        branches.add(Utils.encToURL(prefix)                       + "," + 
                     Utils.encToURL(prefix_local)                 + "," +
                     Utils.encToURL(branch_ref)                   + "," + 
                     Utils.encToURL(((String) key) + "_" + depth) + "," +
                     depth                                        + "," +
                     "map"                                        + "," +
                     Utils.encToURL((String) key));
        entity_name_map.put(branch_ref, ((String) key));
        entity_depth_map.put(branch_ref, depth);
        entity_depth_map.put(((String) key) + "_" + depth, depth);
        recurse(as_map.get(key), branch_ref, ((String) key) + "_" + depth, depth+1);
      }
    } else if (obj instanceof String) {

      String leaf_ref = prefix + "_" + ((String) obj);

      leaves.add(Utils.encToURL(prefix) + "," + Utils.encToURL(leaf_ref) + "," + depth + ",leaf");
      resolution.put(prefix, ((String) obj));
      entity_name_map.put(leaf_ref, ((String) obj));
      entity_depth_map.put(leaf_ref, depth);

    } else if (obj == null            ||
               obj instanceof Double  ||
               obj instanceof Integer ||
               obj instanceof Boolean ||
               obj instanceof Long) {

      String as_string = "" + obj;

      String leaf_ref = prefix + "_" + as_string;
      leaves.add(Utils.encToURL(prefix) + "," + Utils.encToURL(leaf_ref) + "," + depth + ",leaf");
      resolution.put(prefix, as_string);
      entity_name_map.put(leaf_ref, as_string);
      entity_depth_map.put(leaf_ref, depth);

    } else System.err.println("Do Not Understand Obj \"" + obj + "\"...");
  }

  /**
   *
   */
  public static void main(String args[]) {
    try {
      // Read the file into a stringbuffer
      StringBuffer sb = new StringBuffer();
      BufferedReader in = new BufferedReader(new FileReader(new File(args[0])));
      String line; while ((line = in.readLine()) != null) sb.append(line);
      in.close();

      new JSONToTreeFile(sb.toString());

    } catch (Throwable throwable) {
      System.err.println("Throwable: " + throwable);
      throwable.printStackTrace(System.err);
    }
  }
}



