package racetrack.util;

import java.awt.image.BufferedImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

/**
 *
 */
public class SmallMultiplesPlotter {
  /**
   *  Files to parse
   */
  List<File>  files;

  /**
   * Categorization field
   */
  String      cat_field;

  /**
   * Timestamp field
   */
  String      timestamp_field;

  /**
   * Render Type
   */
  RType       render_type;

  /**
   * Count By Field ... all caps means that it's a scalar field... null means to count by records... everything else is counted by set
   */
  String      count_by_field;

  /**
   * Color By Field .. null means no colorization
   */
  String      color_by_field;

  /**
   * Bar Width (-1 means to calculate it dynamically)
   */
  int         bar_w; 

  /**
   * Bar Height (-1 means to calculate it dynamically)
   */
  int         bar_h; 

  /**
   * Bar Gap (-1 means to calculate it dynamically)
   */
  int         bar_g;

  /**
   * Parse the lines using the excel-csv parser
   */
  boolean     excel_csv_parse;

  /**
   *
   */
  public SmallMultiplesPlotter(List<File>  files, 
                               String      cat_field, 
                               String      timestamp_field, 
                               RType       render_type,
                               String      count_by_field, 
                               String      color_by_field, 
                               int         bar_w, 
                               int         bar_h, 
                               int         bar_g, 
                               boolean     excel_csv_parse,
                               String      delims) {
    this.files            = new ArrayList<File>(); this.files.addAll(files); // Make a copy
    this.cat_field        = cat_field;
    this.timestamp_field  = timestamp_field;
    this.render_type      = render_type;
    this.count_by_field   = count_by_field;
    this.color_by_field   = color_by_field;
    this.bar_w            = bar_w;
    this.bar_h            = bar_h;
    this.bar_g            = bar_g;
    this.excel_csv_parse  = excel_csv_parse;
    this.delims           = delims;
  }

  /**
   * Render Types ... mirrors what's found in the KeyMaker class
   */
  public enum RType { YR, YR_MO, YR_MO_DAY, MO, DOFW, DOFW_HR, HR, HR_MIN };

  /**
   *
   */
  public static RType parseRenderType(String str) {
    str = str.toLowerCase();
    if      (str.equals("yr"))        return RType.YR;
    else if (str.equals("yr/mo"))     return RType.YR_MO;
    else if (str.equals("yr/mo/day")) return RType.YR_MO_DAY;
    else if (str.equals("mo"))        return RType.MO;
    else if (str.equals("dofw"))      return RType.DOFW;
    else if (str.equals("dofw/hr"))   return RType.DOFW_HR;
    else if (str.equals("hr"))        return RType.HR;
    else if (str.equals("hr/min"))    return RType.HR_MIN;
    else throw new RuntimeException("parseRenderType(str=\"" + str + "\") -- type not found");
  }

  /**
   * When null, use the first line parsed to pick the delim out...
   */
  String delims = null;

  /**
   * Determine the delimiter used... try commas, pipes, then tabs... then fail.
   */
  protected void determineDelims(String str_1, String str_2) {
    int comma_1 = count(str_1, ','),   comma_2 = count(str_2, ','),
        pipes_1 = count(str_1, '|'),   pipes_2 = count(str_2, '|'),
        tabs_1  = count(str_1, '\t'),  tabs_2  = count(str_2, '\t');
    if      (comma_1 > pipes_1 && comma_1 > tabs_1  && Math.abs(comma_1 - comma_2) <= 3) delims = ",";
    else if (pipes_1 > comma_1 && pipes_1 > tabs_1  && Math.abs(pipes_1 - pipes_2) <= 3) delims = "|";
    else if (tabs_1  > comma_1 && tabs_1  > pipes_1 && Math.abs(tabs_1  - tabs_2)  <= 3) delims = "\t";
    else throw new RuntimeException("Could Not Determine Delimiter -- Please Specify As Option");
  }

  /**
   * Count number of instances of character in string.
   */
  private int count(String str, char c) {
    int sum = 0;
    for (int i=0;i<str.length();i++) if (str.charAt(i) == c) sum++;
    return sum;
  }

  /**
   * Tokenize a string into it's parts... 
   */
  public String[] tokenize(String str) {
    if (excel_csv_parse) return Utils.tokenizeExcelLine(str);
    else                 return Utils.tokenize(str, delims);
  }

  /**
   *
   */
  public void phaseOneParsing() throws IOException {
    Map<String,Integer> hdr_map = null;

    // Scan the headers of all the files first... make sure they align with the settings
    for (int i=0;i<files.size();i++) {
      BufferedReader in = new BufferedReader(new FileReader(files.get(i)));
      // Get first two lines -- header and a data row for comparison
      String hdr_str  = in.readLine(); 
      String data_str = in.readLine(); 
      in.close();
      
      // Determine the delims if they aren't set (or not parsing excel csv)
      if (delims == null && excel_csv_parse == false) determineDelims(hdr_str, data_str);

      // Tokenize lines
      String hdr[]    = tokenize(hdr_str);
      String data[]   = tokenize(data_str);

      if (hdr.length != data.length) throw new RuntimeException("First and Second Row of \"" + files.get(i) + "\" Have Different Number Of Tokens");

      // Construct the header map
      hdr_map = new HashMap<String,Integer>();
      for (int j=0;j<hdr.length;j++) { hdr_map.put(hdr[j], j); }

      // Make sure the fields are present in the header...
      if (hdr_map.keySet().contains(timestamp_field) == false) throw new RuntimeException("Timestamp Field \"" + timestamp_field + "\" Not Found In \"" + files.get(i) + "\"");
      if (count_by_field != null &&
          hdr_map.keySet().contains(count_by_field)  == false) throw new RuntimeException("Count By Field \""  + count_by_field  + "\" Not Found In \"" + files.get(i) + "\"");
      if (color_by_field != null &&
          hdr_map.keySet().contains(color_by_field)  == false) throw new RuntimeException("Color By Field \""  + color_by_field  + "\" Not Found In \"" + files.get(i) + "\"");
      if (hdr_map.keySet().contains(cat_field)       == false) throw new RuntimeException("Category Field \""  + cat_field       + "\" Not Found In \"" + files.get(i) + "\"");
      
      // Check basics -- scalar fields (all caps) should be integers...
      if (count_by_field != null && allCaps(count_by_field)) {
        count_by_is_scalar = true;
        try { Integer.parseInt(data[hdr_map.get(count_by_field)]); } catch (NumberFormatException nfe) {
          throw new RuntimeException("Scalar Field \"" + count_by_field + "\" Not Parseable As Integer Values (example \"" + data[hdr_map.get(count_by_field)] + "\") In \"" + files.get(i) + "\"");
        }
      }

      // Check basics -- timestamp field should be parseable as a timestamp
      if (Utils.isTimeStamp(data[hdr_map.get(timestamp_field)]) == false) {
        throw new RuntimeException("Timestamp Field \"" + timestamp_field + "\" Is Not Timestamp (example \"" + data[hdr_map.get(timestamp_field)] + "\") In \"" + files.get(i) + "\"");
      }
    }

    // Now parse all the files all the way through accumulating timestamps and categories
    long ts0, ts1;
    for (int i=0;i<files.size();i++) {
      ts0 = ts1 = -1L; // Reset the timestamps
      BufferedReader in      = new BufferedReader(new FileReader(files.get(i)));
      String         hdr_str = in.readLine(); 
      String         hdr[]   = tokenize(hdr_str);
      hdr_map = new HashMap<String,Integer>();
      for (int j=0;j<hdr.length;j++) { hdr_map.put(hdr[j], j); }

      String line; int line_no = 1; while ((line = in.readLine()) != null) {
        String tokens[] = tokenize(line); line_no++;

        if (tokens.length != hdr_map.keySet().size()) {
          in.close();
          throw new RuntimeException("At Line " + line_no + " In \"" + files.get(i) + "\" -- incorrect number of tokens for line \"" + line + "\"");
        }

        String ts_str  = tokens[hdr_map.get(timestamp_field)],
               cat_str = tokens[hdr_map.get(cat_field)];

        // Get the timestamp and keep bounds on the minimum and maximum timestamp observed
        long ts = Utils.parseTimeStamp(ts_str);
        if (ts0 == -1L) ts0 = ts; if (ts0 > ts) ts0 = ts;
        if (ts1 == -1L) ts1 = ts; if (ts1 < ts) ts1 = ts;

        // Keep track of the categories
        categories.add(cat_str);

        // Check the count by
        if (count_by_field != null && count_by_is_scalar) {
          try { Integer.parseInt(tokens[hdr_map.get(count_by_field)]); } catch (NumberFormatException nfe) {
            in.close();
            throw new RuntimeException("At Line " + line_no + " In \"" + files.get(i) + "\" Count By Value \"" + tokens[hdr_map.get(count_by_field)] + "\" Is Not An Integer");
          }
        }
      }

      System.err.println("For File \"" + files.get(i) + "\" (lines = " + line_no +"):");
      System.err.println("  Time Range: " + Utils.exactDate(ts0) + " to " + Utils.exactDate(ts1));
      System.err.println("  Cats Found: " + categories.size());
      System.err.println("");

      // Keep track of the global timestamp mins and maxes
      if (global_ts0 == -1L) global_ts0 = ts0;
      if (ts0 < global_ts0)  global_ts0 = ts0;

      if (global_ts1 == -1L) global_ts1 = ts1;
      if (ts1 > global_ts1)  global_ts1 = ts1;
    } 
  }

  /**
   * Earliest timestamp across all of the files
   */
  long global_ts0 = -1L,

  /**
   * Latest timestamp across all of the files
   */
       global_ts1 = -1L;

  /**
   * Set of the categories found across all the files
   */
  Set<String> categories = new HashSet<String>();

  /**
   *  Flag to indicate that count by uses a scalar (integer) field
   */
  boolean count_by_is_scalar = false;

  /**
   * Determine if a string only contains capitals.  Used to determine if a field is a scalar field.
   */
  private boolean allCaps(String str) {
    for (int i=0;i<str.length();i++) if (str.charAt(i) >= 'a' && str.charAt(i) <= 'z') return false;
    return true;
  }

  /**
   * Time transformer abstract class.
   */
  abstract class TimeTrans { 
    Calendar cal; 
    RType rtype; long t0, t1;
    public TimeTrans(RType rtype) { 
      this.rtype = rtype; t0 = t1 = -1L; 
      cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    }
    public TimeTrans(RType rtype, long t0, long t1) { 
      this.rtype = rtype; this.t0 = t0; this.t1 = t1; 
      cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    }
    public abstract int           trans(String ts_str);
    public abstract List<Integer> orderedBins();
  }

  /**
   * Time Transformer
   */
  class YearTimeTrans extends TimeTrans {
    public YearTimeTrans(long t0, long t1) { super(RType.YR, t0, t1); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return cal.get(Calendar.YEAR); }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        Calendar cal_inc  = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_inc. setTimeInMillis(t0);
        Calendar cal_stop = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_stop.setTimeInMillis(t1);
        while (cal_inc.get(Calendar.YEAR) != cal_stop.get(Calendar.YEAR)) { ls.add(cal_inc.get(Calendar.YEAR)); cal_inc.add(Calendar.YEAR, 1); }
        ls.add(cal_inc.get(Calendar.YEAR));
      }
      return ls;
    }
  }

  /**
   * Time Transformer
   */
  class YearMonthTimeTrans extends TimeTrans {
    public YearMonthTimeTrans(long t0, long t1) { super(RType.YR_MO, t0, t1); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return asInt(cal); }
    private int asInt(Calendar calendar) {
      int  year  = calendar.get(Calendar.YEAR), month = calendar.get(Calendar.MONTH);
      // System.err.println("year = " + year + " | month = " + month); // DEBUG
      return year * 100 + month;
    }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        Calendar cal_inc  = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_inc. setTimeInMillis(t0); cal_inc.set(Calendar.DAY_OF_MONTH, 1);
        Calendar cal_stop = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_stop.setTimeInMillis(t1);
        // System.err.println("cal_inc  init = " + asInt(cal_inc));  // DEBUG
        // System.err.println("cal_stop      = " + asInt(cal_stop)); // DEBUG
        while (cal_inc.get(Calendar.YEAR)  != cal_stop.get(Calendar.YEAR) ||
               cal_inc.get(Calendar.MONTH) != cal_stop.get(Calendar.MONTH)) { 
          // System.err.println("adding " + asInt(cal_inc) + " to list..."); // DEBUG
          ls.add(asInt(cal_inc));
          cal_inc.add(Calendar.MONTH, 1); 
        }
        // System.err.println("adding " + asInt(cal_inc) + " to list..."); // DEBUG
        ls.add(asInt(cal_inc));
      }
      return ls;
    }
  }

  /**
   * Time Transformer
   */
  class YearMonthDayTimeTrans extends TimeTrans {
    public YearMonthDayTimeTrans(long t0, long t1) { super(RType.YR_MO_DAY, t0, t1); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return asInt(cal); }
    private int asInt(Calendar calendar) {
      int year = calendar.get(Calendar.YEAR), month = calendar.get(Calendar.MONTH), day = calendar.get(Calendar.DAY_OF_MONTH);
      return year * 10000 + month * 100 + day;
    }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        Calendar cal_inc  = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_inc. setTimeInMillis(t0);
        Calendar cal_stop = Calendar.getInstance(TimeZone.getTimeZone("GMT")); cal_stop.setTimeInMillis(t1);
        while (cal_inc.get(Calendar.YEAR)         != cal_stop.get(Calendar.YEAR)  ||
               cal_inc.get(Calendar.MONTH)        != cal_stop.get(Calendar.MONTH) ||
               cal_inc.get(Calendar.DAY_OF_MONTH) != cal_stop.get(Calendar.DAY_OF_MONTH)) { 
           ls.add(asInt(cal_inc));
           cal_inc.add(Calendar.DAY_OF_MONTH, 1);
        }
        ls.add(asInt(cal_inc));
      }
      return ls;
    }
  }

  /**
   *
   */
  class MonthTimeTrans extends TimeTrans {
    public MonthTimeTrans() { super(RType.MO); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return cal.get(Calendar.MONTH); }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        for (int i=0;i<12;i++) ls.add(i);
      }
      return ls;
    }
  }

  /**
   *
   */
  class DofWTimeTrans extends TimeTrans {
    public DofWTimeTrans() { super(RType.DOFW); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return cal.get(Calendar.DAY_OF_WEEK); }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        for (int i=0;i<7;i++) ls.add(i+1);
      }
      return ls;
    }
  }

  /**
   *
   */
  class DofWHourTimeTrans extends TimeTrans {
    public DofWHourTimeTrans() { super(RType.DOFW_HR); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return asInt(cal); }
    private int asInt(Calendar cal) { int dow = cal.get(Calendar.DAY_OF_WEEK), hour = cal.get(Calendar.HOUR_OF_DAY); return dow * 100 + hour; }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        for (int i=0;i<7;i++) for (int j=0;j<24;j++) ls.add((i+1) * 100 + j); 
      }
      return ls;
    }
  }

  /**
   *
   */
  class HourTimeTrans extends TimeTrans {
    public HourTimeTrans() { super(RType.HR); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return cal.get(Calendar.HOUR_OF_DAY); }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        for (int i=0;i<24;i++) ls.add(i);
      }
      return ls;
    }
  }

  /**
   *
   */
  class HourMinTimeTrans extends TimeTrans {
    public HourMinTimeTrans() { super(RType.HR_MIN); }
    public int trans(String ts_str) { long ts = Utils.parseTimeStamp(ts_str); cal.setTimeInMillis(ts); return asInt(cal); }
    public int asInt(Calendar calendar) { int hour = calendar.get(Calendar.HOUR_OF_DAY), min = calendar.get(Calendar.MINUTE); return hour * 100 + min; }
    List<Integer> ls = null;
    public List<Integer> orderedBins() {
      if (ls == null) {
        ls = new ArrayList<Integer>();
        for (int i=0;i<24;i++) for (int j=0;j<60;j++) ls.add(i*100 + j);
      }
      return ls;
    }
  }

  /**
   *
   */
  protected TimeTrans createTimeTrans() {
    switch (render_type) {
      case YR:        return new YearTimeTrans(global_ts0, global_ts1);
      case YR_MO:     return new YearMonthTimeTrans(global_ts0, global_ts1);
      case YR_MO_DAY: return new YearMonthDayTimeTrans(global_ts0, global_ts1);
      case MO:        return new MonthTimeTrans();
      case DOFW:      return new DofWTimeTrans();
      case DOFW_HR:   return new DofWHourTimeTrans();
      case HR:        return new HourTimeTrans();
      case HR_MIN:    return new HourMinTimeTrans();
      default: return null;
    }
  }

  class BinCounter {
    public BinCounter() {
    }
    public void count(String tokens[], Map<String,Integer> hdr_map) {
    }
  }

  /**
   *
   */
  public void phaseTwoPlotting() throws IOException {
    // Construct the temporal transformation
    TimeTrans time_trans = createTimeTrans();

    // For each category, allocate the bins
    Map<Integer,BinCounter>             all  = new HashMap<Integer,BinCounter>();
    Map<String,Map<Integer,BinCounter>> map  = new HashMap<String,Map<Integer,BinCounter>>();
    List<Integer>                       bins = time_trans.orderedBins();

    // Fill the "all" structure int
    for (int i=0;i<bins.size();i++) {
      // System.err.println("bins @ " + i + " = " + bins.get(i)); // DEBUG
      all.put(bins.get(i),new BinCounter());
    }

    // Fill each category in
    Iterator<String> it_cat = categories.iterator(); while (it_cat.hasNext()) {
      String cat = it_cat.next();
      map.put(cat, new HashMap<Integer,BinCounter>());
      for (int i=0;i<bins.size();i++) map.get(cat).put(bins.get(i),new BinCounter());
    }

    // Re-parse the files and fill in the bins
    for (int i=0;i<files.size();i++) {
      BufferedReader in      = new BufferedReader(new FileReader(files.get(i)));
      String         hdr_str = in.readLine(); 
      String         hdr[]   = tokenize(hdr_str);
      HashMap<String,Integer> hdr_map = new HashMap<String,Integer>();
      for (int j=0;j<hdr.length;j++) { hdr_map.put(hdr[j], j); }

      // For line, have the bin counter count it
      String line; while ((line = in.readLine()) != null) {
        String tokens[] = tokenize(line);
        String cat      = tokens[hdr_map.get(cat_field)];
        int    bin      = time_trans.trans(tokens[hdr_map.get(timestamp_field)]);
        // System.err.println("bin = " + bin); // DEBUG
        map.get(cat).get(bin).count(tokens,hdr_map);
      }
      in.close();
    }
  }

  /**
   *
   */
  public BufferedImage renderImage() {
    return new BufferedImage(32, 32, BufferedImage.TYPE_INT_RGB);
  }

  /**
   * Print the command line usage for the program.
   *
   *@param out output stream
   */
  public static void printUsage(PrintStream out) {
        out.println       ("Usage:\n\n" +
                           "java SmallMultiplesPlotter [options] csv-file-1 cat-field timestamp-field render-type count-by-field color-by-field [csv-file-2 ...]\n\n" +
                           "Options:\n" +
                           " -barw  VALUE     // bar width\n" +
                           " -barh  VALUE     // max bar height\n" +
                           " -barg  VALUE     // bar gap\n" +
                           " -excelcsv        // parse excel-csv files (w/ no intra-cell line breaks)\n" + 
                           " -d     DELIMS    // set the delimiter(s) for tokenization\n" +
// Need "-strips   // saves small multiple strips into different files"
// Need "-noimage  // don't create a large image"
                           "\n" +
                           "Render Types:\n" +
                           "  Yr\n" +
                           "  Yr/Mo\n" +
                           "  Yr/Mo/Day\n" + 
                           "  Mo\n" +
                           "  DofW\n" +
                           "  DofW/Hr\n" +
                           "  Hr\n" +
                           "  Hr/Min\n\n" +
                           "Count-by-field:\n" + 
                           "  RECS            // to count by records\n\n" +
                           "Color-by-field:\n" +
                           "  NONE            // no coloring");
  }

  /**
   *
   */
  public static void main(String args[]) {
    try {
      // Print Usage
      if (args.length == 0) { printUsage(System.err); System.exit(0); }

      // Parse arguments
      boolean    in_options          = true,
                 cat_field_set       = false,
                 timestamp_field_set = false,
                 render_type_set     = false,
                 count_by_set        = false,
                 color_by_set        = false;
 
      int        bar_w               = -1,
                 bar_h               = -1,
                 bar_g               = -1;

      boolean    excel_csv_parse     = false;

      String     cat_field           = null,
                 timestamp_field     = null,
                 count_by_field      = null,
                 color_by_field      = null,
                 delims              = null;

      RType      render_type         = null;

      List<File> files = new ArrayList<File>();

      int i = 0; while (i < args.length) {
        if (in_options) {
          if        (args[i].startsWith("-"))      {
            if        (args[i].equals("-barw"))     { bar_w  = Integer.parseInt(args[i+1]); i+=2;
            } else if (args[i].equals("-barh"))     { bar_h  = Integer.parseInt(args[i+1]); i+=2;
            } else if (args[i].equals("-barg"))     { bar_g  = Integer.parseInt(args[i+1]); i+=2;
            } else if (args[i].equals("-excelcsv")) { excel_csv_parse = true;               i++;
            } else if (args[i].equals("-d"))        { delims = args[i+1];                   i+=2;
            } else { System.err.println("Do Not Understand Options \"" + args[i]); System.exit(0); }
          } else if ((new File(args[i])).exists()) {
            in_options = false;
          } else { System.err.println("Do Not Understand Options \"" + args[i]); System.exit(0); }
        } else if (files.size()        == 0)     {
          if ((new File(args[i])).exists()) files.add(new File(args[i++]));
          else { System.err.println("File \"" + args[i] + "\" Does Not Exist"); System.exit(0); }
        } else if (cat_field_set       == false) { cat_field_set       = true; cat_field = args[i++]; 
        } else if (timestamp_field_set == false) { timestamp_field_set = true; timestamp_field = args[i++];
        } else if (render_type_set     == false) { render_type_set     = true; render_type = parseRenderType(args[i++]);
        } else if (count_by_set        == false) { count_by_set        = true; if (args[i].equals("RECS")) { count_by_field = null; i++; } else count_by_field = args[i++];
        } else if (color_by_set        == false) { color_by_set        = true; if (args[i].equals("NONE")) { color_by_field = null; i++; } else color_by_field = args[i++];
        } else {
          if ((new File(args[i])).exists()) files.add(new File(args[i++]));
          else { System.err.println("File \"" + args[i] + "\" Does Not Exist"); System.exit(0); }
        }
      }

      // Make sure the arguments are sane
      if        (files.size()        == 0)     {
        System.err.println("No Files Supplied\n\n");
        printUsage(System.err); System.exit(0);
      } else if (cat_field_set       == false) { 
        System.err.println("Categorization Field Not Set\n\n");
        printUsage(System.err); System.exit(0);
      } else if (timestamp_field_set == false) {
        System.err.println("Timestamp Field Not Set\n\n");
        printUsage(System.err); System.exit(0);
      } else if (render_type_set     == false) {
        System.err.println("Render Type Not Set\n\n");
        printUsage(System.err); System.exit(0);
      } else if (count_by_set        == false) {
        System.err.println("Count By Not Set\n\n");
        printUsage(System.err); System.exit(0);
      } else if (color_by_set        == false) {
        System.err.println("Color By Not Set\n\n");
        printUsage(System.err); System.exit(0);
      }

      // Create the plotter
      SmallMultiplesPlotter plotter = new SmallMultiplesPlotter(files, cat_field, timestamp_field, render_type, count_by_field, color_by_field, bar_w, bar_h, bar_g, excel_csv_parse, delims);

      // Run phase 1...
      plotter.phaseOneParsing();
      
      // Do the plot
      plotter.phaseTwoPlotting();
      
      // Save it out
      BufferedImage bi = plotter.renderImage();

    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }
}

