/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

/**
 * Normalize a linux dirwalk.  Dir walk needs to be generated with the following syntax:
 *
 * ls -alR --full-time <directory>
 * 
 * @author  D. Trimm
 * @version 0.1
 */
public class NormalizeLinuxDirWalk {
  /**
   * Construct the instance...
   */
  public NormalizeLinuxDirWalk() { }

  /**
   * Flag indicating that a parse error occurred
   */
  boolean parse_errors = false;

  /*
   * Error flag - ints exceeded max and were converted back to the max
   */
  boolean ints_exceed_max = false;

  /**
   * Print out parser errors.
   *
   *@param out print stream
   */
  public void printParseErrors(PrintStream out) {
    if (parse_errors) {
      out.println("**\n** Parse Errors\n**\n");
      if (ints_exceed_max)             out.println("  Ints exceed max int value and were set to max");
      out.println("\n\n");
    }
    out.println("\n\n**\n** Notes\n**");
    out.println("No Conversion For Timezone Offsets...");
  }

  /**
   * Parser state enumeration
   */
  enum ParseState { directory_next, total_next, files };

  /**
   * Read a file in, normalize the header and the records and dump to the provided PrintStream.
   *
   *@param file file to parse
   *@param out  PrintStream for normalized results
   */
  public void normalize(File file) throws IOException {
    ParseState state = ParseState.directory_next; String directory = null;

    BufferedReader in = new BufferedReader(new FileReader(file)); 
    String line = null; /* int line_no = 0; */ while ((line = in.readLine()) != null) { /* line_no++; */
      switch (state) {
        // Parse the directory line next
        case directory_next: directory = line; if (directory.endsWith(":")) directory = directory.substring(0,line.length()-1);
                             // System.err.println("Directory = \"" + directory + "\"");
                             if (directory.indexOf("\\/") >= 0) System.err.println("Directory \"" + directory + "\" Has Backslashed Escapes - Will Probably Be Incorrect");
                             mapper.put(unescape(directory), new ArrayList<Rec>());
                             state = ParseState.total_next;
                             break;
        // Parse the total line next
        case total_next:     state = ParseState.files;
                             break;

        // If the line is blank, go back to the next directory state - otherwise, parse as a file
        case files:          if (lineEmpty(line)) { state = ParseState.directory_next; } else { parseFile(line, directory); }
                             break;
      }
    }
    in.close();
  }

  /**
   * Tabulation based on the directory string of the files
   */
  Map<String,List<Rec>> mapper = new HashMap<String,List<Rec>>();

  /**
   * Determine if a line is empty
   */
  protected boolean lineEmpty(String line) {
    if (line.length() == 0) return true;
    for (int i=0;i<line.length();i++) { char c = line.charAt(i); if (c == ' ' || c == '\t') { } else return false; }
    return true;
  }

  /**
   * Parse a file line.
   *
   *@param line      line to parse
   *@param directory current director
   */
  protected void parseFile(String line, String directory) {
    StringTokenizer st = new StringTokenizer(line, " \t", true);
    String perms = st.nextToken();
    String num   = nextRealToken(st);
    String owner = nextRealToken(st);
    String group = nextRealToken(st);
    String size  = nextRealToken(st);
    String ymd   = nextRealToken(st);
    String hms   = nextRealToken(st);
    String off   = nextRealToken(st);
    StringBuffer filename  = new StringBuffer();
    filename.append(nextRealToken(st)); while (st.hasMoreTokens()) filename.append(st.nextToken());
    if (filename.toString().equals(".") || filename.toString().equals("..") || directory == null || mapper.get(directory) == null) { 
      // Don't record
    } else {
      // if (directory             == null) System.err.println("Directory = null");
      // if (mapper.get(directory) == null) System.err.println("mapper.get(" + directory + ") == null");
      mapper.get(directory).add(new Rec(perms, num, owner, group, size, ymd + " " + hms, off, unescape(filename.toString()), directory));
    }
  }

  /**
   * Return the next real token -- i.e., not whitespace.
   */
  private String nextRealToken(StringTokenizer st) {
    String token = null; while (st.hasMoreTokens() && isReal(token = st.nextToken()) == false) { } return token;
  }
  private boolean isReal(String s) {
    for (int i=0;i<s.length();i++) { char c = s.charAt(i); if (c == ' ' || c == '\t') { } else return true; }
    return false;
  }
  private String unescape(String s) {
    StringBuffer sb = new StringBuffer(); int i = 0;
    while (i < s.length()) {
      char c = s.charAt(i);
      if (c == '\\' && (i+1) < s.length()) { i++; } // next character
      sb.append(s.charAt(i));
      i++;
    }
    return sb.toString();
  }

  /**
   * File record
   */
  class Rec {
    String perms, num, owner, group, ymd_hms, ts_off, filename, directory; boolean is_dir = false; int size;
    public Rec(String perms,
               String num,
               String owner,
               String group,
               String size_str,
               String ymd_hms,
               String ts_off,
               String filename,
               String directory) {
      this.perms     = perms; if (perms.charAt(0) == 'd') is_dir = true;
      this.num       = num;
      this.owner     = owner;
      this.group     = group;
      this.ymd_hms   = ymd_hms;
      this.ts_off    = ts_off;
      this.filename  = filename;
      this.directory = directory;
      try { size = Integer.parseInt(size_str); } catch (NumberFormatException nfe) { size = Integer.MAX_VALUE; ints_exceed_max = true; }
      if (is_dir) size = -1; // Flag to indicate that the directory size not yet calculated
    }
    public int calculateSize() {
      if (is_dir && size == -1) { // Hasn't been calculated yet
        long size_long = 0L; String complete_path = directory + "/" + filename;
        List<Rec> files = mapper.get(complete_path);
        if (files != null) {
          for (int i=0;i<files.size();i++) { size_long += files.get(i).calculateSize(); }
          if (size_long < Integer.MAX_VALUE) size = (int) size_long; else { ints_exceed_max = true; size = Integer.MAX_VALUE; }
        } else System.err.println("No Mapping For Directory \"" + complete_path + "\"...");
      }
      return size;
    }
    public String toString() {
      child_to_parent.add(Utils.encToURL(directory + "/" + filename) + " => " + Utils.encToURL(directory));
      return ymd_hms                                    + "," +
             is_dir                                     + "," +
             Utils.encToURL(directory + "/" + filename) + "," +
             Utils.encToURL(filename)                   + "," +
             Utils.encToURL(directory)                  + "," +
             (is_dir ? 0 : size)                        + "," +
             size                                       + "," +
             Utils.encToURL(owner)                      + "," +
             Utils.encToURL(group)                      + "," +
             Utils.encToURL(fileExtension(filename))    + "," +
             "NormalizeLinuxDirWalk";
    }
    public String fileExtension(String s) {
      int    index = s.lastIndexOf("."); if (index < 0) return "";
      String ext   = s.substring(index, s.length());
      return ext.toLowerCase();
    }
  }

  /**
   * Line for the files portion header.
   */
  public String filesHeader() {
    return "timestamp" + "," +
           "is_dir"    + "," +
           "path"      + "," +
           "filename"  + "," +
           "parent"    + "," +
           "FILESIZE"  + "," +
           "TOTALSIZE" + "," +
           "owner"     + "," +
           "group"     + "," +
           "extension" + "," +
           "source";
  }

  /**
   * Records which children to parent records have been reported
   */
  Set<String> child_to_parent = new HashSet<String>();

  /**
   * Print the results to an output stream.
   *
   *@param out output stream
   */
  public void print(PrintStream out) {
    out.println(filesHeader());
    Iterator<String> it = mapper.keySet().iterator(); while (it.hasNext()) {
      String    directory = it.next();
      List<Rec> recs      = mapper.get(directory);
      for (int i=0;i<recs.size();i++) out.println(recs.get(i).toString());
    }

    // Print any subdir to dir that weren't printed in the files section
    Set<String> to_print = new HashSet<String>();
    it = mapper.keySet().iterator(); while (it.hasNext()) {
      String subdir = it.next();
      while (subdir.lastIndexOf("/") >= 0) {
        String parent = subdir.substring(0, subdir.lastIndexOf("/"));
        String key    = Utils.encToURL(subdir) + " => " + Utils.encToURL(parent);
        if (child_to_parent.contains(key) == false) {
          to_print.add(Utils.encToURL(subdir) + "," + Utils.encToURL(parent));
          child_to_parent.add(key);
        }
        subdir = parent;
      }
    }

    if (to_print.size() > 0) {
      out.println("");
      out.println("path,parent");
      it = to_print.iterator(); while (it.hasNext()) out.println(it.next());
    }
  }

  /**
   * Main body...  show license, [parse params], normalize supplied files, print to stdout
   *
   *@param args input arguments
   */
  public static void main(String args[]) {
    // Print out the apache license info...
    System.err.println("License Information");
    System.err.println("");
    System.err.println("Copyright 2017 David Trimm");
    System.err.println("");
    System.err.println("Licensed under the Apache License, Version 2.0 (the \"License\");");
    System.err.println("you may not use this file except in compliance with the License.");
    System.err.println("You may obtain a copy of the License at");
    System.err.println("");
    System.err.println("http://www.apache.org/licenses/LICENSE-2.0");
    System.err.println("");
    System.err.println("Unless required by applicable law or agreed to in writing, software");
    System.err.println("distributed under the License is distributed on an \"AS IS\" BASIS,");
    System.err.println("WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.");
    System.err.println("See the License for the specific language governing permissions and");
    System.err.println("limitations under the License.");
    System.err.println("");

    try {
      // Construct the normalizer
      NormalizeLinuxDirWalk normalizer = new NormalizeLinuxDirWalk();

      // Parse the options (nothing here yet)
      
      // Parse the files and dump the output
      for (int i=0;i<args.length;i++) { normalizer.normalize(new File(args[i])); }

      // Print out the normalized output
      normalizer.print(System.out);

      // Print out parse errors (if any existed)
      normalizer.printParseErrors(System.err);
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }
}

