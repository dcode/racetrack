/* 

Copyright 2022 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.util.Arrays;
import java.util.List;

/**
 * Class for calculating quick statistics on an array or List of integers, floats, or doubles.
 */
public class QuickStatsDouble extends QuickStats {
  /**
   * Construct the class by calculating all quick stats variables.
   *
   *@param doubles list of doubles -- converted over to an array for processing
   */
  public QuickStatsDouble(List<Double> doubles) {
    double as_array[] = new double[doubles.size()];
    for (int i=0;i<as_array.length;i++) as_array[i] = doubles.get(i);
    constructWithArray(as_array);
  }

  /**
   * Construct the class by calculating all quick stats variables.
   *
   *@param doubles double values in array
   */
  public QuickStatsDouble(double doubles[]) { constructWithArray(doubles); }

  /**
   * Copy of my doubles
   */
  double my_doubles[];

  /**
   * Generic constructor based on array of doubles.
   *
   *@param doubles double values in array
   */
  private void constructWithArray(double doubles[]) {
    my_doubles = doubles;

    // Min, max, and sum/average
    for (int i=0;i<doubles.length;i++) { 
      sum += doubles[i]; samples++;
      if (doubles[i] < min_value) min_value = doubles[i];
      if (doubles[i] > max_value) max_value = doubles[i];
    }
  }

  /**
   * Return the median value of the array.
   *
   *@return median value
   */
  @Override public double median() {
    if (Double.isNaN(median_value)) {
      median_value = qSortMedian(my_doubles);
    }
    return median_value;
  }
    
  /**
   * Return the standard deviation for the array.
   *
   *@return standard deviation
   */
  @Override public double stdev() {
    if (Double.isNaN(stdev_value)) {
      double avg = sum/samples;
      for (int i=0;i<my_doubles.length;i++) {
        stdev_value += (my_doubles[i] - avg) * (my_doubles[i] - avg);
      }
      stdev_value = Math.sqrt(stdev_value/samples);
    }
    return stdev_value;
  }

  /**
   *
   */
  protected double qSortMedian(double doubles[]) {
    int i0 = 0, i1 = doubles.length-1;
    while (true) {
      int l = i1 - i0 + 1;
      if        (l == 1) {                                                               return doubles[doubles.length/2];
      } else if (l == 2) { if (doubles[i0]   > doubles[i1])   { swap(doubles,i0,  i1); } return doubles[doubles.length/2];
      } else if (l == 3) { if (doubles[i0]   > doubles[i0+1]) { swap(doubles,i0,  i0+1); }
                           if (doubles[i0+1] > doubles[i0+2]) { swap(doubles,i0+1,i0+2); }
                           if (doubles[i0]   > doubles[i0+1]) { swap(doubles,i0,  i0+1); }
                           return doubles[doubles.length/2];
      } else             {
        // pick the best of three for a pivot point
        double s[] = new double[3]; int in[] = new int[3]; s[0] = doubles[i0]; in[0] = i0; s[1] = doubles[(i0+i1)/2]; in[1] = (i0+i1)/2; s[2] = doubles[i1]; in[2] = i1;
        if (s[0] > s[1]) swap(s,in,0,1); if (s[1] > s[2]) swap(s,in,1,2); if (s[0] > s[1]) swap(s,in,0,1); 

        // put the pivot at the beginning of the array
        double pivot = s[1]; int pivot_i = in[1];
        if      (pivot_i == (i0+i1)/2) swap(doubles,i0,(i0+i1)/2);
        else if (pivot_i == i1)        swap(doubles,i0,i1);

        // partition the array
        int i = i0+1, j = i1;
        while ((j-i) > 1) {
          while (i != j && doubles[i] <= pivot) i++;
          while (i != j && doubles[j] >  pivot) j--;
          if (i != j) swap(doubles,i,j);
        }

        // put the pivot in the middle
        if (doubles[i] > pivot) { swap(doubles,i-1,i0); pivot_i = i-1; }
        else                    { swap(doubles,i,  i0); pivot_i = i;   }

        // partition the next set that contains the median
        if      (pivot_i == doubles.length/2)  { return doubles[doubles.length/2]; }
        else if ((doubles.length/2) < pivot_i) { /* i0 = i0; */    i1 = pivot_i-1; }
        else                                   { i0 = pivot_i+1;   /* i1 = i1; */       }
      }
    }
  }

  protected void swap(double doubles[],            int i, int j) { double tmp = doubles[i]; doubles[i] = doubles[j]; doubles[j] = tmp; }
  protected void swap(double doubles[], int ins[], int i, int j) { swap(doubles,i,j); swap(ins,i,j); }

  /**
   * Test for class
   */
  public static void main(String args[]) {
    for (int exp=1;exp<7;exp++) {
     int samples = (int) Math.pow(10,exp); System.out.println("**\n** Samples = " + samples + "\n**");
     for (int tests=0;tests<50;tests++) {
      double doubles[]  = new double[samples];
      for (int i=0;i<doubles.length;i++) doubles[i] = (double) (Math.random());

      long t0 = System.currentTimeMillis(); QuickStatsDouble qs = new QuickStatsDouble(doubles); double median   = (double) qs.median();      long t1 = System.currentTimeMillis();
      long t2 = System.currentTimeMillis(); Arrays.sort(doubles);                                double median_r = doubles[doubles.length/2]; long t3 = System.currentTimeMillis();

      System.out.println("" + (median == median_r) + " med [" + median + "] t=" + (t1-t0) + " ...... acc [" + median_r + "] t=" + (t3-t2));
     }
    }
  }
}

