/**
 *
 */
package racetrack.util;

import java.io.*;
import java.util.*;
import java.util.regex.*;

/**
 * Extracts TLD's from the following html file -- you have to download the html file first...
 *
 * https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains
 */
public class ExtractTLDs {
  public static void main(String args[]) {
    try {
      StringBuffer sb = new StringBuffer();

      BufferedReader in = new BufferedReader(new FileReader(args[0]));
      String line; while ((line = in.readLine()) != null) { sb.append(line + "\r\n"); }
      in.close();

      Set<String> tlds = new HashSet<String>();

      Pattern pattern; Matcher matcher;

      pattern = Pattern.compile("<td>([.]\\w+)</td>");                                   matcher = pattern.matcher(sb.toString()); while (matcher.find()) { tlds.add(matcher.group(1)); }
      pattern = Pattern.compile(" title=\"([.]\\w+)\"");                                 matcher = pattern.matcher(sb.toString()); while (matcher.find()) { tlds.add(matcher.group(1)); }
      pattern = Pattern.compile("<td>([.]\\w+)</td>", Pattern.UNICODE_CHARACTER_CLASS);  matcher = pattern.matcher(sb.toString()); while (matcher.find()) { tlds.add(matcher.group(1)); }
      pattern = Pattern.compile(">([.]\\w+)<",        Pattern.UNICODE_CHARACTER_CLASS);  matcher = pattern.matcher(sb.toString()); while (matcher.find()) { tlds.add(matcher.group(1)); }

      System.err.println("tlds.size() = " + tlds.size());

      System.err.print("tlds = [");
      List<String> sort = new ArrayList<String>(); sort.addAll(tlds); Collections.sort(sort);
      for (int i=0;i<sort.size();i++) {
        if (i != 0) System.err.print(", ");
        System.err.print("\"" + sort.get(i) + "\"");
      }
      System.err.println("]");

    } catch (Throwable t) { System.err.println("Throwable: " + t); t.printStackTrace(System.err); }
  }
}

