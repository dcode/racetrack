/* 

Copyright 2018 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.StringTokenizer;

/**
 * Normalize Excel unicode files into the URL-encoded racetrack format.
 * At this point, this is a very simplified converter... It does not take
 * into consideration the following:  (1) tabs in cells and (2) line breaks 
 * in cells.
 * 
 * @author  D. Trimm
 * @version 0.1
 */
public class NormalizeExcelUnicode { 
  /**
   * Constructor -- and implementation for the class/utility.
   *
   *@param f_in  input file
   *@param f_out output file
   */
  public NormalizeExcelUnicode(File f_in, File f_out) throws IOException {
    BufferedReader in  = new BufferedReader(new InputStreamReader(new FileInputStream(f_in), "UTF-16"));
    PrintStream    out = new PrintStream(new FileOutputStream(f_out));
    String line;
    while ((line = in.readLine()) != null) {
      StringBuffer    sb = new StringBuffer();
      StringTokenizer st = new StringTokenizer(line, "\t", true);
      while (st.hasMoreTokens()) {
        String token = st.nextToken();
        if (token.equals("\t")) { sb.append(","); } else sb.append(Utils.encToURL(token));
      }
      out.println(sb.toString());
    }
    out.close();
    in.close();
  }

  /**
   * Main entry point for the program.
   */
  public static void main(String args[]) {
    try {
      if (args.length == 2) {
        File input_file  = new File(args[0]),
             output_file = new File(args[1]);
        if        (input_file.exists()  == false) { System.err.println("Input file \"" + args[0] + "\" does not exist");
        // } else if (output_file.exists() == true)  { System.err.println("Output file \"" + args[1] + "\" exists and will not be overwritten");
        } else new NormalizeExcelUnicode(input_file, output_file);
      } else System.err.println("Usage:\tjava racetrack.util.NormalizeExcelUnicode excel_unicode_output.txt racetrack_norm.csv");
    } catch (Throwable t) { System.err.println("Throwable: " + t); }
  }
}

