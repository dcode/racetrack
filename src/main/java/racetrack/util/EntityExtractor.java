/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Set;

import racetrack.framework.BundlesDT;

/**
 * Extract entities from a text block.  Encode them as subtexts for
 * overlay into the visualization.
 *
 *@author  D. Trimm
 *@version 2.0
 */
public class EntityExtractor {
  /**
   * (Pre-defined) Entity patterns
   */
  enum FindType { ENTITY, 
  /**
   * Relatinoship subtext (not implemented)
   */
                  RELATIONSHIP, 
  /**
   * Timestamp subtext
   */
                  TIMESTAMP,
  /**
   * Interval (duration, begin/end time) subtext
   */
                  INTERVAL };

  /**
   * For a block of text, extract all of the known subtext types and return
   * them as a list.
   *
   *@param  text text block
   *
   *@return list of extracted subtexts
   */
  public static List<SubText> list(String text) {
    List<SubText> subs = new ArrayList<SubText>();

    // Prepare the queue of regex's to run -- have to order properly -- e.g., intervals before just a timestamp
    List<String> regexes = new ArrayList<String>(); Map<String,FindType> type_map = new HashMap<String,FindType>();

    // Intervals
    String s;
    s = "(" + Utils.getTimeStampRegex() + ")\\s+[Tt][Hh][Rr][Uu]\\s+("               + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.INTERVAL);
    s = "(" + Utils.getTimeStampRegex() + ")\\s+[Tt][Oo]\\s+("                       + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.INTERVAL);
    s = "(" + Utils.getTimeStampRegex() + ")\\s+[Tt][Hh][Rr][Oo][Uu][Gg][Hh]\\s+("   + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.INTERVAL);
    s = "[Bb][Ee][Tt][Ww][Ee][Ee][Nn] (" + Utils.getTimeStampRegex() + ")\\s+[Aa][Nn][Dd]\\s+(" + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.INTERVAL);
    s = "(" + Utils.getTimeStampRegex() + ")\\s+[-]\\s+("                            + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.INTERVAL);

    // Timestamps
    s = "(" + Utils.getTimeStampRegex() + ")"; regexes.add(s); type_map.put(s, FindType.TIMESTAMP);

    // Entities
    s = "(" + Utils.getURLRegex()      + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getMD5Regex()      + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getEmailRegex()    + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getDomainRegex()   + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getIPv4CIDRRegex() + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getIPv4Regex()     + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);
    s = "(" + Utils.getIPv6Regex()     + ")"; regexes.add(s); type_map.put(s, FindType.ENTITY);

    recurse(text, 0, text, regexes, 0, type_map, subs);

    return subs;
  }

  /**
   * Recursively find the patterns -- at each find, split the string and recurse for the next regex index.
   */
  protected static void recurse(String all_text, int offset, String text, List<String> regexes, int regex_i, Map<String,FindType> type_map, List<SubText> subs) {
    if (regex_i >= regexes.size()) return; // Done with the recursion

    // Create the pattern matcher instance and run it against this subset of text
    Pattern pattern = Pattern.compile(regexes.get(regex_i)); Matcher matcher = pattern.matcher(text);

    // For all the matched patterns, run the recursion against the pre-string then create a subtext for the found pattern
    int start = 0;
    while (matcher.find()) {
      // Get the matching pattern
      int i0 = matcher.start(), i1 = matcher.end(); 
      String matched = Utils.stripSpaces(matcher.group());

      // Recurse on the pre-string
      String pre_str = text.substring(start, i0);
      recurse(all_text, offset + start, pre_str, regexes, regex_i+1, type_map, subs);
      start = i1; // Save off the start for next iteration

      // Convert the found item into a subtext
      switch (type_map.get(regexes.get(regex_i))) {
          case ENTITY:       BundlesDT.DT datatype = BundlesDT.getEntityDataType(matched);
                             // System.err.println("matched:" + matched + " == DT:" + datatype);
                             subs.add(new Entity      (all_text, matched, datatype, offset + i0, offset + i1)); break;
                case RELATIONSHIP: subs.add(new Relationship(all_text, matched,           offset + i0, offset + i1)); break;
                case INTERVAL:     subs.add(new Interval    (all_text, matched,           offset + i0, offset + i1)); break;
                case TIMESTAMP:    long precise_ms = Utils.timeStampPrecision(matched);
                                   if (precise_ms >= Utils.HOURS) { subs.add(new Interval (all_text, matched, offset + i0, offset + i1, precise_ms - 1L));
                                   } else                         { subs.add(new TimeStamp(all_text, matched, offset + i0, offset + i1)); }
                                               break;
      }
    }

    // At the end, run the rest of the text
    String pst_str = text.substring(start, text.length());
    recurse(all_text, offset + start, pst_str, regexes, regex_i+1, type_map, subs);
  }

  /**
   * Extract the entities but return the substrings as a set.
   *
   *@param  str text block
   *@return set of extracted entity strings
   */
  public static Set<String> stringSet(String str) {
    List<SubText> al = list(str);
    Set<String> set = new HashSet<String>();
    Iterator<SubText> it = al.iterator();
    while (it.hasNext()) set.add(it.next().toString());
    return set;
  }

  /**
   * Test methods for this class.
   */
  public static void main(String args[]) {
    try {
      String test_text; int entity_count = 0;

      //
      // No args - use a default string
      //
      if (args.length == 0) {
        test_text = "blah blah blah 2015-02-03 to 2015-02-04.  Then something at 2015-02-01 12:45.  " +
                    "Next was from 2015-02-05 13:00 to 2015-02-07 14:15." + "\n" +
                    "something something something 1.2.3.4 and then something 2.3.4.5." + "\n" +                  // 2 entities
                    "blah blah blah 2015-02-03 to 2015-02-04.  Then something at 2015-02-01 12:45.  " +
                    "more text ... more text... 1[.]2[.]3[.]4 and 1[.]2[.]3[.]4:80 and 2.3.4[.]5 blah blah.  " +  // 3 entities
                    "then user@email.com and someone@gmail.com and test@mail[.]com blah blah blah.  " +           // 3 entities
                    "domain.com and domain2[.]com and something.domain.com and something2[.]domain2[.]net.  " +   // 4 entities
                    "Next was from 2015-02-15 16:00 through 2015-02-18 00:15.";
              entity_count = 12;

      //
      // Parse the first arg as an int - that's the text length -- make a random (but deterministic string of that length)
      //
      } else {
        int len = Integer.parseInt(args[0]);
              List<SubText> authoritative_list = new ArrayList<SubText>();
              test_text = createRandomText(len, authoritative_list);
              entity_count = authoritative_list.size();
      }

      if (test_text.length() < 2048) System.err.println("**\n** Test Text\n**\n" + test_text + "\n\n");

      long t0 = System.currentTimeMillis();
      List<SubText> subtexts = EntityExtractor.list(test_text);
      long t1 = System.currentTimeMillis();
      System.err.println("Extraction Time: " + (t1 - t0) + " ms | Entity Count = " + entity_count + " ... Found = " + subtexts.size());

      //
      // Only print out for the simple default text string... not the user defined version
      // 
      if (test_text.length() < 2048) for (int i=0;i<subtexts.size();i++) { System.err.println("  " + subtexts.get(i) + " " + subtexts.get(i).getIndex0() + "..." + subtexts.get(i).getIndex1()); }

    } catch (Throwable t) { System.err.println("Throwable: " + t); }
  }

  /**
   * Create a random text for testing purposes.
   *
   *@param len                length of text
   *@param authoritative_list as entities are added, they are inserted here for testing
   *
   *@return random text
   */
  public static String createRandomText(int len, List<SubText> authoritative_list) {
        StringBuffer sb = new StringBuffer();
        Random random = new Random(len ^ 0x00L); // Make it deterministic

        for (int i=0;i<len;i++) {
                SubText subtext = null; 
                if (random.nextDouble() < 0.01) {
                  double d = random.nextDouble(); String s;
                  if      (d < 0.33) { s = createInterval (random); 
                                       subtext = new Interval  (sb.toString(), s,                                 sb.length(), sb.length() + s.length()); }
                  else if (d < 0.66) { s = createTimeStamp(random); 
                                       subtext = new TimeStamp (sb.toString(), s,                                 sb.length(), sb.length() + s.length()); }
                  else               { String split[] = createEntity(random);
                                       // split[0] == actual entity
                                       s = split[1]; // possibly obfuscated version
                                       subtext = new Entity    (sb.toString(), split[0], BundlesDT.getEntityDataType(s), sb.length(), sb.length() + s.length()); }
                  sb.append(" " + s + " ");
                } else                          {
                  int c = posInt(random) % 27;
                  if (c == 26) sb.append(" "); else sb.append((char) ('a' + c));
                }
          
              if (subtext != null) authoritative_list.add(subtext);
            }
    return sb.toString();
  }

  /**
   * Create a random interval string... technically, will not be correct because the timestamps won't be ordered...
   */
  public static String createInterval(Random random) {
    String ts0 = createTimeStamp(random), ts1 = createTimeStamp(random);
    double d   = random.nextDouble();
    if      (d < 0.2) return ts0 + " to "      + ts1;
    else if (d < 0.4) return ts0 + " - "       + ts1;
    else if (d < 0.6) return ts0 + " through " + ts1;
    else if (d < 0.8) return ts0 + " thru "    + ts1;
    else              return "between " + ts0 + " and " + ts1;
  }

  /**
   * Create a random timestamp string
   */
  public static String createTimeStamp(Random random) {
    int year = 2000 + (posInt(random) % 20),
        mon  = 1    + (posInt(random) % 12),
        day  = 1    + (posInt(random) % 26),
        hor =  0    + (posInt(random) % 24),
        min =  0    + (posInt(random) % 60),
        sec =  0    + (posInt(random) % 60);

    String mon_str = "" + mon; if (mon_str.length() == 1) mon_str = "0" + mon_str;
    String day_str = "" + day; if (day_str.length() == 1) day_str = "0" + day_str;
    String hor_str = "" + hor; if (hor_str.length() == 1) hor_str = "0" + hor_str;
    String min_str = "" + min; if (min_str.length() == 1) min_str = "0" + min_str;
    String sec_str = "" + sec; if (sec_str.length() == 1) sec_str = "0" + sec_str;

    double d_format = random.nextDouble();
    if      (d_format < 0.33) return year + "-" + mon_str + "-" + day_str + " " + hor_str + ":" + min_str + ":" + sec_str;
    else if (d_format < 0.66) return year + "-" + mon_str + "-" + day_str;
    else                      return mon_str + "/" + day_str + "/" + year;
  }

  /**
   * Create a random entity String.
   */
  public static String[] createEntity(Random random) {
    String ret[] = new String[2]; // ret[0] = actual entity ... ret[1] = obfuscated entity (could be the same)
    double d = random.nextDouble();
    if        (d < 0.33) { // IP Address
      int first  = (posInt(random)%256),
          second = (posInt(random)%256),
          third  = (posInt(random)%256),
          fourth = (posInt(random)%256);
      ret[0] = first + "." + second + "." + third + "." + fourth;
      
      double d_obf = random.nextDouble();
      if      (d_obf < 0.33) ret[1] = ret[0];
      else if (d_obf < 0.66) ret[1] = first + "."   + second + "."   + third + "[.]" + fourth;
      else                   ret[1] = first + "[.]" + second + "[.]" + third + "[.]" + fourth;

      return ret;
    } else if (d < 0.66) { // Domains
      String  first  = alphaString(random, 4 + posInt(random)%12);
      String  second = alphaString(random, 4 + posInt(random)%12);
      double  d_tld = random.nextDouble(); String tld;
      if      (d_tld < 0.33) tld = "com";
      else if (d_tld < 0.66) tld = "net";
      else                   tld = "org";

      ret[0] = first + "." + second + "." + tld;

      double d_obf = random.nextDouble();
      if      (d_obf < 0.33) ret[1] = ret[0];
      else if (d_obf < 0.66) ret[1] = first + "."   + second + "[.]" + tld;
      else                   ret[1] = first + "[.]" + second + "[.]" + tld;

      return ret;
    } else               { // Email Address
      String user  = alphaString(random, 4 + posInt(random)%12),
             first = alphaString(random, 4 + posInt(random)%12);
      double d_tld = random.nextDouble(); String tld;
      if      (d_tld < 0.33) tld = "com";
      else if (d_tld < 0.66) tld = "net";
      else                   tld = "org";
    
      ret[0] = user + "@" + first + "." + tld;

      double d_obf = random.nextDouble();
      if      (d_obf < 0.33) ret[1] = ret[0];
      else if (d_obf < 0.66) ret[1] = user + "[@]" + first + "."   + tld;
      else                   ret[1] = user + "@"   + first + "[.]" + tld;
           
      return ret;
    }
  }

  /**
   * Create a random alphabet string of the specified length.
   */
  public static String alphaString(Random random, int len) {
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<len;i++) {
      int c = posInt(random) % 26;
      sb.append((char) (c + 'a'));
    }
    return sb.toString();
  }

  /**
   * Return the next positive int from the random generator.
   */
  public static int posInt(Random random) {
    int i = random.nextInt();
    if (i < 0) i = i * -1;
    return i;
  }
}

