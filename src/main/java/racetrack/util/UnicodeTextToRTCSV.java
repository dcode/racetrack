/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Convert an excel unicode text file into a racetrack csv file.
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class UnicodeTextToRTCSV {

  /**
   * Construct the reader and run it through the file.
   *
   *@param fm_file
   *@param to_file
   */
  public UnicodeTextToRTCSV(File fm_file, File to_file) throws IOException {
    // Read the input completely into memory as bytes
    InputStream in         = new FileInputStream(fm_file);
    byte        in_bytes[] = new byte[(int) fm_file.length()];
    int in_read = in.read(in_bytes); in.close();
    if (in_read != in_bytes.length) throw new RuntimeException("Input Bytes Not All Read");

    // Transform into unicode string
    String in_str = new String(in_bytes, "UTF-16");

    // Open output stream
    PrintStream out_print = new PrintStream(new FileOutputStream(to_file));

    // Parse the tokens
    List<String> tokens  = new ArrayList<String>();
    int          index   = 0,
                 line_no = 0;

    while ((index = Utils.tokenizeRFC4180Line(index, in_str, tokens, '\t')) >= 0) {
      if (tokens.size() >= 0) {
        line_no++;
        pushTokens(tokens, out_print, line_no);
        tokens.clear();
      }
    }

    if (tokens.size() > 0) pushTokens(tokens, out_print, line_no);

    out_print.close();
  }

  /**
   * Write to an output file as a URL-encoded CSV file.
   */
  protected void pushTokens(List<String> tokens, PrintStream out, int line_no) throws IOException {
    for (int i=0;i<tokens.size();i++) {
      if (i != 0) out.print(",");
      String str = tokens.get(i);
      if (Utils.allNumbers(str)) {
        try { Integer.parseInt(str); } catch (NumberFormatException nfe) { str = "x" + str; }
        out.print(str);
      } else out.print(Utils.encToURL(str));
    }
    out.println();
  }

  /**
   * Read arguments and convert them.
   */
  public static void main(String args[]) {
    try {
      for (int i=0;i<args.length;i++) {
        String to_file_str = args[i] + "_rt.csv";
        System.err.println("Converting \"" + args[i] + "\" to \"" + to_file_str + "\"...");
        new UnicodeTextToRTCSV(new File(args[i]), new File(to_file_str));
      }
    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
      ioe.printStackTrace(System.err);
    }
  }
}

