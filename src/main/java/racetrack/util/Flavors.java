/* 

Copyright 2019 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.io.File;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesRecs;
import racetrack.framework.BundlesUtils;

import racetrack.visualization.StatsOverlay;

/**
 * Determine the first flavor of a set of files.
 */
public class Flavors {

  /**
   * 
   */
  public static void main(String args[]) {
    try {
      if (args.length == 0) {
        System.err.println("Usage:" + "\n" +
                           ""       + "\n" +
                           "  java racetrack.util.Flavors file [file...]" + "\n");
      } else {
        for (int i=0;i<args.length;i++) {
          File file = new File(args[i]);
          if (file.exists()) {
            Bundles                        bundles  = new BundlesRecs();
            Set<Bundle>                    set      = BundlesUtils.parse(bundles, file, 10);
            Map<String,Map<String,String>> flavors  = StatsOverlay.dataFlavors(set);
            if (flavors != null && flavors.keySet().size() > 0) {
              List<String> sorted = new ArrayList<String>(); sorted.addAll(flavors.keySet()); Collections.sort(sorted);
              System.out.println("File \"" + file + "\"...");
              for (int j=0;j<sorted.size();j++) System.out.println("  " + sorted.get(j));
              System.out.println("");
            } else { System.out.println("File \"" + file + "\" Has No Flavors"); }
          } else System.err.println("File \"" + file + "\" Does Not Exist");
        }
      }
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }
}

