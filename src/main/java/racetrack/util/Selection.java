/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import java.util.regex.Pattern;

/**
 * Handles how to select across different user interface components -- namely entities and graph...
 * ... refactored from Entities and Graph component
 */
public class Selection {
  /**
   * Selection operation
   */
  enum Op { Replace, Union, Subtract, Intersect };

  /**
   * Return if the selection starts with a set operation character.
   *
   *@param selection selection string
   *
   *@return true if first character is set operation
   */
  public static boolean selectionContainsSetOperation(String selection) {
    if (selection == null) return false;
    else                   return selection.startsWith("!") ||
                                  selection.startsWith("*") ||
                                  selection.startsWith("+") ||
                                  selection.startsWith("-");
  }

  /**
   * Remove the set operation from the selection string (if there is one) else
   * just return what was passed in...
   */
  public static String removeSetOperation(String selection) {
    if (selectionContainsSetOperation(selection)) return selection.substring(1,selection.length()); 
    else                                          return selection;
  }

  /**
   * Return if the selection works on tags.
   */
  public static boolean isTagRelated(String selection) {
    String str = removeSetOperation(selection);
    return str.startsWith("tag ") ||
           str.startsWith("tag:") ||
           str.startsWith("tag=");
  }

  /**
   * Apply the set operation (if there is one) to the existing set...
   *
   *@param selection     selection criteria
   *@param new_set       set of strings that match the criteria
   *@param existing_set  existing set ... for exampe, the currently selected strings
   *@param all_possible  all possible strings ... for example, all the ones that are currently rendered
   *
   *@return set operation results
   */
  public static Set<String> applySetOperation(String selection, Set<String> new_set, Set<String> existing_set, Set<String> all_possible) {
    if (existing_set == null) existing_set = new HashSet<String>();

    Set<String> to_return = new HashSet<String>();

    if        (selection.startsWith("!")) { Iterator<String> it = all_possible.iterator(); 
                                            while (it.hasNext()) { String str = it.next(); if (new_set.contains(str) == false) to_return.add(str); }
    } else if (selection.startsWith("+")) { to_return.addAll(existing_set); to_return.addAll(new_set);
    } else if (selection.startsWith("-")) { to_return.addAll(existing_set); to_return.removeAll(new_set);
    } else if (selection.startsWith("*")) { to_return.addAll(existing_set); to_return.retainAll(new_set);
    } else                                { to_return.addAll(new_set); }

    return to_return;
  }

  /**
   * Clipboard version...
   */
  public static Set<String> selectClipboard(Set<String> possibles, String clipboard_str) {
    Set<String> set = new HashSet<String>();
    
    StringTokenizer st = new StringTokenizer(clipboard_str, "\r\n"); while (st.hasMoreTokens()) {
      String token = st.nextToken();
      set.addAll(select(possibles,token));
    }

    return set;
  }

  /**
   *  Simplified version
   */
  public static Set<String> select(Set<String> possibles, Set<String> existing, String selection) {
    return select(possibles, existing, selection, Op.Replace);
  }

  /**
   * Simplified version
   */
  public static Set<String> select(Set<String> possibles, String selection) {
    return select(possibles, null, selection, Op.Replace);
  }

  /**
   * Overarching method to split the selection operation.
   *
   * Selection Criteria:
   *
   *   "REGEX:"  Use the pattern after the colon as a regex 
   *   "!"       Invert the selection based on existing
   *   "+"       Add the selection to the existing
   *   "-"       Subtract the selection from the existing
   *   "*"       Intersect the selection from the existing
   *   "tag " or 
   *   "tag:" or 
   *   "tag="    Tag selection ... select entities from existing by the tagged information
   *
   * To Match Examples
   *
   *   Entities and Graph Examples:
   *     IP
   *     Number
   *     IP|Number
   *
   *   Entities example:
   *     IP_[Number]        // Where the number is just to deconflict identically named entities
   *     IP|Number_[Number] // Where the second number is just to deconflict identically named entities
   *     Entity_[Number]    // Where the number is just to deconflict identically named entities
   *
   *@param possibles  strings to select from
   *@param existing   can be null.. but if supplied, then a set operation is possible ... a copy is made and used
   *@param selection  selection criteria
   *@param op         selection operation
   *
   *@return matching strings
   */
  public static Set<String> select(Set<String> possibles, Set<String> existing, String selection, Op op) {
    // Return all
    if (selection == null || selection.length() == 0) return new HashSet<String>();

    // Make a copy of existing...  don't want to mess up the caller (or have to check to make sure i'm not messing up the caller)
    if (existing != null) { Set<String> copy = new HashSet<String>(); copy.addAll(existing); existing = copy; }
    else                  { existing = new HashSet<String>(); }

    // Modifiers ... inversion and the set operation
    boolean invert = false;
    char    first = selection.charAt(0);
    if      (first == '!') { invert = true;         selection = selection.substring(1,selection.length()); }
    else if (first == '+') { op     = Op.Union;     selection = selection.substring(1,selection.length()); }
    else if (first == '-') { op     = Op.Subtract;  selection = selection.substring(1,selection.length()); }
    else if (first == '*') { op     = Op.Intersect; selection = selection.substring(1,selection.length()); }

    // Split by regex versus the smart selection
    Set<String> matches = null;
    if (selection.startsWith("REGEX:")) matches = matchByRegex(possibles, selection.substring(("REGEX:").length(),selection.length()));
    else                                matches = smartMatch  (possibles, selection);

    // Do the operation
    if        (invert)             { Set<String> set = new HashSet<String>(); Iterator<String> it = possibles.iterator();
                                     while (it.hasNext()) { String str = it.next(); if (matches.contains(existing) == false) set.add(str); }
                                     return set;
    } else if (op == Op.Subtract)  { if (existing != null) existing.removeAll(matches); return existing;
    } else if (op == Op.Union)     { if (existing != null) matches.addAll(existing);    return matches;
    } else if (op == Op.Intersect) { if (existing != null) matches.retainAll(existing); return matches;
    } else                         { return matches; } // Replace or Set
  }

  /**
   * Match within the set via the regex pattern.
   *
   *@param set       set to match against
   *@param selection (hopefully) a regex pattern
   *
   *@return matching strings
   */
  private static Set<String> matchByRegex(Set<String> set, String selection) {
    Set<String> matches = new HashSet<String>();
    
    Pattern pattern = Pattern.compile(selection);
    Iterator<String> it = set.iterator(); while (it.hasNext()) { String str = it.next();
      if (pattern.matcher(str).matches()) matches.add(str);
    }

    return matches;
  }

  /**
   * Further partition the selection by the type of entity within the selection string.
   *
   *@param set set to match against
   *@param selection selection criteria
   *
   *@return matching strings from the set that match the criteria
   */
  private static Set<String> smartMatch(Set<String> set, String selection) {
    if        (Utils.isIPv4CIDR(selection))  {    return cidrMatch   (set,selection);
    } else if (Utils.isIPv4    (selection))  {    return ipv4Match   (set,selection);
    } else if (Utils.allNumbers(selection))  {    return numbersMatch(set,selection);
    } else if (selection.startsWith("tag=") ||
               selection.startsWith("tag ") ||
               selection.startsWith("tag:")) {    return tagMatch    (set,selection);
    } else /* substring match */             {    return subMatch    (set,selection); }
  }

  /**
   * Find IP addresses that fall within the specified CIDR...  to include
   * unstructured text that contains the IP addresses...
   *
   *@param set set to match against
   *@param selection selection criteria
   *
   *@return matching strings from the set that match the criteria
   */
  private static Set<String> cidrMatch(Set<String> set, String selection) {
    Set<String> matches   = new HashSet<String>();
    int         cidr_bits = Utils.cidrBits(selection),
                cidr_mask = Utils.cidrMask(selection);

    Iterator<String> it = set.iterator(); while (it.hasNext()) { String str = it.next();

      if ((str.length() <= ("255.255.255.255").length()) && // Stupid tests for an ip address
          (str.indexOf(".") >= 0)                        && // Another simple way...
          (Utils.isIPv4(str))) {
        if (Utils.cidrMatch(Utils.strToIPInt(str), cidr_bits, cidr_mask)) matches.add(str);
      } else {
        int i = 0;
        while (i < str.length()) {
          char c = str.charAt(i);
          if (c >= '0' && c <= '9') {
            // See if this is an IP address
            int j = i; while (j < str.length() && ((str.charAt(j) >= '0' && str.charAt(j) <= '9') || str.charAt(j) == '.')) j++;
            String sub =str.substring(i,j);

            // Copy of the above...
            if ((sub.length() <= ("255.255.255.255").length()) && // Stupid tests for an ip address
                (sub.indexOf(".") >= 0)                        && // Another simple way...
                (Utils.isIPv4(sub))) {
              if (Utils.cidrMatch(Utils.strToIPInt(sub), cidr_bits, cidr_mask)) matches.add(str);
            }

            i = j; // In any event, move the index past this substring
          } else i++;
        }
      }
    }

    return matches;
  }

  /**
   * Match IP Addresses... common mistakes are matching a CIDR or matching a versioning string.
   *
   *@param set set to match against
   *@param selection selection criteria
   *
   *@return matching strings from the set that match the criteria
   */
  private static Set<String> ipv4Match(Set<String> set, String selection) {
    Set<String> matches = new HashSet<String>();

    // Start with a substring match... but then make sure the IP is exact...
    Iterator<String> it = set.iterator(); while (it.hasNext()) { String str = it.next();
      // System.err.println("criteria = \"" + selection + "\" vs \"" + str + "\""); // DEBUG
      int i0 = 0;
      while ((i0 = str.indexOf(selection,i0)) >= 0) {
        int  i1 = i0 + selection.length();
        char before = ' '; if ((i0-1) >= 0)           before = str.charAt(i0-1);
        char after  = ' '; if (i1 < str.length())     after  = str.charAt(i1);
        char after2 = ' '; if ((i1+1) < str.length()) after2 = str.charAt(i1+1);
        char after3 = ' '; if ((i1+2) < str.length()) after3 = str.charAt(i1+2);
        char after4 = ' '; if ((i1+3) < str.length()) after4 = str.charAt(i1+3);

        // System.err.println("before = \"" + before + "\" ... after = \"" + after + "\""); //DEBUG
        if      ((before >= '0' && before <= '9') || 
                 (after  >= '0' && after  <= '9') ||
                 (before == '.')                  ||
                 (after  == '.'))                     {                     i0 = i1;           } 
        else if ((after  == '/')                  &&
                 (after2 >= '0' && after2 <= '9') &&
                 (after3 <  '0' || after3 >  '9'))    { /* CIDR ... skip */ i0 = i1;           }
        else if ((after  == '/')                  &&
                 (after2 >= '0' && after2 <= '3') &&
                 (after3 >= '0' && after3 <= '9') &&
                 (after4 <  '0' || after4 >  '9'))    { /* CIDR ... skip */ i0 = i1;           }
        else                                          { matches.add(str);   i0 = str.length(); }
      }
    }

    return matches;
  }

  /**
   * Check for an exact numbers match.
   *
   *@param set       set of strings to check
   *@param selection a number
   *
   *@return matching strings
   */
  private static Set<String> numbersMatch(Set<String> set, String selection) {
    Set<String> matches = new HashSet<String>();

    // Start with a substring match... but then make sure the number is exact...
    Iterator<String> it = set.iterator(); while (it.hasNext()) { String str = it.next();
      // System.err.println("criteria = \"" + selection + "\" vs \"" + str + "\""); // DEBUG
      int i0 = 0;
      while ((i0 = str.indexOf(selection,i0)) >= 0) {
        int  i1 = i0 + selection.length();
        char before; if ((i0-1) >= 0)       before = str.charAt(i0-1); else before = ' ';
        char after;  if (i1 < str.length()) after  = str.charAt(i1);   else after  = ' ';
        // System.err.println("before = \"" + before + "\" ... after = \"" + after + "\""); //DEBUG
        if ((before >= '0' && before <= '9') || (after  >= '0' && after  <= '9')) {                    i0 = i1;           } 
        else                                                                      { matches.add(str);  i0 = str.length(); }
      }
    }

    return matches;
  }

  /**
   * Return tool tip help for the selection syntax.
   *
   *@param string for toop tips
   */
  public static String getToolTipHelp() {
    return "<html>" + 
             "+/-/*/! sub/IP/CIDR/tag /tag:/tag=/REGEX:" + "<br><br>" +
             "Grouped nodes may not work correctly"      + "<br>"     +
             "Off screen labels won't be selected"       + "<br><br>" +
             " \"tag sub\"/\"tag:type\"/\"tag=exact\"";
  }

  /**
   * Match various entities based on tagging...
   * "tag sub"        ... any tags with that substring
   * "tag:type"       ... any tags with the type value of that type
   * "tag=type=value" ... exact matches of type-value strings
   *
   *@param set       set to match against
   *@param selection selection criteria ... needs to start with "tag ", "tag:", or "tag="
   *
   *@param matching strings from the set that match the selection criteria
   */
  private static Set<String> tagMatch(Set<String> set, String selection) {
    Set<String> matches = new HashSet<String>();
    if (selection != null) {
      String specifics = selection.substring(4,selection.length()); Iterator<String> it = set.iterator();
      if        (selection.startsWith("tag:")) { // Specific type
        while (it.hasNext()) { String tag = it.next();
          if (tag.indexOf("=") >= 0 && tag.substring(0,tag.indexOf("=")).equals(specifics)) matches.add(tag);
        }
      } else if (selection.startsWith("tag=")) { // Specific type=values pair
        while (it.hasNext()) { String tag = it.next();
          if (tag.equals(specifics)) matches.add(tag);
        } 
      } else { // Any substring match
        while (it.hasNext()) { String tag = it.next();
          if (tag.indexOf(specifics) >= 0) matches.add(tag);
        }
      }
    }
    return matches;
  }

  /**
   * Perform a substring match...  uses the lowercase version of both selection and the
   * string to match against.
   *
   *@param set set to match against
   *@param selection selection criteria
   *
   *@return matching strings from the set that match the criteria
   */
  private static Set<String> subMatch(Set<String> set, String selection) {
    selection = selection.toLowerCase();

    Set<String> matches = new HashSet<String>();
    Iterator<String> it = set.iterator(); while (it.hasNext()) { String str = it.next();
      if (str.toLowerCase().indexOf(selection) >= 0) matches.add(str);
    }

    return matches;
  }

  /**
   * Test method
   */
  public static void main(String args[]) { 
    // First string is the selection criteria... the rest are the strings to match against...
    String tests[][] = {
      // Numbers matches
      { "123", // What to search for...
        "123", " 123", "123 ", "1234", "123 is a number", "example number if 123", "in the middle 123 number", 
        "bad 1234", "1234 bad", "4123 more bad", "1.1.1.1|123", "1.1.1.1|1234", "1.1.1.1|4123", "1.1.1.1|80 123", 
        "1.1.1.1|80 4123" },

      { "5", // What to search for...
        "5", " 5", "5 ", "there were 5 in the middle", "51", "55", "555", "5 5 5 5" },

      // IP matches
      { "1.2.3.4", // What to search for...
         "1.2.3.4", "21.2.3.4", "1.2.3.44", " 1.2.3.4", "1.2.3.4 ", "this is an ip address 1.2.3.4",
        "1.2.3.4/30","5.6.7.8 1.2.3.4 1.1.1.1", "version.1.2.3.4", "1.2.3.4.5", "1.2.3.4:80", "1.2.3.4|80",
        "1.2.3.4:1.2.3.4", "1.2.3.4 is an ip address", "1.2.3.4/8 is a cidr address", "1.2.3.4/16 is another cidr",
        "1.2.3.4/24 is a third cidr", "1.2.3.4/32 is a fourth cidr" },

      // CIDR matches
      { "192.168.0.0/16", // What to search for
        "192.168.0.0", "192.168.255.255", "192.168.1.2", "192.168.1.5 ", " 192.168.1.5", " 192.168.10.20 ",
        "192.168.512.512", "192.168.1.2.3.4.5.6.7.8.9.10", "192.168.1.2.3.4 192.168.1.2.3.4.5.6.7",
        "192.168.1.2.3.4.5.6 192.168.120.200", "192.168.0.0/16", "192.168.1.0/24", "this is an ip address 192.168.255.10 that should match",
        "this is an ip address 192.169.12.31 that doesn't match", "but 192.169.12.31 with 192.168.200.200" }
    };

    StringBuffer clipboard_sb = new StringBuffer();
    List<String> all_list     = new ArrayList<String>();

    // Single Line Tests
    for (int i=0;i<tests.length;i++) {
      String criteria = tests[i][0]; Set<String> possibles = new HashSet<String>();

      if (clipboard_sb.length() > 0) clipboard_sb.append("\r\n"); clipboard_sb.append(criteria);

      for (int j=1;j<tests[i].length;j++) possibles.add(tests[i][j]);
      Set<String> matches = select(possibles,criteria);

      System.out.println("\"" + criteria + "\"");
      for (int j=1;j<tests[i].length;j++) {
        all_list.add(tests[i][j]);
        if (matches.contains(tests[i][j])) System.out.println("  T \"" + tests[i][j] + "\"");
        else                               System.out.println("  F \"" + tests[i][j] + "\"");
      }
    }

    // Clipboard Tests
    System.out.println(); System.out.println("** Clipboard Test"); System.out.println();
    Set<String> all_set = new HashSet<String>(); all_set.addAll(all_list);
    Set<String> matches = selectClipboard(all_set, clipboard_sb.toString());
    for (int i=0;i<all_set.size();i++) {
      if (matches.contains(all_list.get(i))) System.out.println("  T \"" + all_list.get(i) + "\"");
      else                                   System.out.println("  F \"" + all_list.get(i) + "\"");
    }
  }
}

