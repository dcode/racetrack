/* 

Copyright 2013 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

/**
 * Class to easily sort string:count lists.
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class IntCountSorterD implements Comparable<IntCountSorterD> {
  /**
   * Integer associated with the count
   */
  int i; 

  /**
   * Count
   */
  double d;

  /**
   * Construct a new sort element.
   *
   *@param str string associated with the count
   *@param d   count
   */
  public IntCountSorterD(int i, double d) { this.i = i; this.d = d; }

  /**
   * Compare against another IntCountSorter.  Comparison is done
   * against the counts.
   *
   *@param  other to compare against
   *
   *@return -1 if less than, 1 if greater than, otherwise the compare results for the strings
   */
  public int compareTo(IntCountSorterD other) {
    if      (other.d < d) return -1;
    else if (other.d > d) return  1;
    else if (other.i < i) return -1;
    else if (other.i > i) return  1;
    else                  return  0;
  }

  /**
   * Return the associated string.
   *
   *@return associated string
   */
  public int toInt() { return i; }

  /**
   * Return the count
   *
   *@return count
   */
  public double count()    { return d;   }

  /**
   * Set the count.
   *
   *@param d new count
   */
  public void setCount(double d) { this.d = d; }
}

