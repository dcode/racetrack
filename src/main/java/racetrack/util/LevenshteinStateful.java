/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.util;

import java.util.Map;
import java.util.HashMap;

/**
 * Levenshtein edit distance implementation that uses stateful memory to eliminate duplicate checks -- space
 * time memory tradeoff.
 */
public class LevenshteinStateful {
  /**
   * Constructor (which also executes the algorithm)
   *
   *@param s string 1
   *@param t string 2
   */
  public LevenshteinStateful(String s, String t) { result = levenshteinDistanceTwoRow(s,t); map = null; }

  /**
   * Result of the calculation (i.e, the edit distance)
   */
  protected int result;

  /**
   * Return the result of the edit distance calculation.
   *
   *@return edit distance for input strings
   */
  public int editDistance() { return result; }

  /**
   * Map to remember state
   */
  Map<String,Integer> map = new HashMap<String,Integer>();

  /**
   * Two row implementation fromt he same wikipedia page listed below.
   */
  protected int levenshteinDistanceTwoRow(String s, String t) {
    // create two work vectors of integer distances
    int v0[] = new int[t.length()+1],
        v1[] = new int[t.length()+1];
    
    // initialize v0 (the previous row of distances)
    // this row is A[0][i]:  edit distance for an empty s
    // the distance is just the number of characters to delete from t
    for (int i=0;i<=t.length();i++) v0[i] = i;

    for (int i=0;i<s.length();i++) {
      // calculate v1 (current row distances from the previous row v0

      // first element of v1 is A[i+1][0]
      //   edit distance is delete (i+1) chars from s to match empty t
      v1[0] = i+1;

      // use formula to fill in the rest of the row
      for (int j=0;j<t.length();j++) {
        // calculating costs for A[i+1][j+1]
        int deletionCost  = v0[j+1]+1;
        int insertionCost = v1[j]+1;
        int substitutionCost;
        if (s.charAt(i) == t.charAt(j)) { substitutionCost = v0[j];
        } else                          { substitutionCost = v0[j]+1; }
        v1[j+1] = minimum(deletionCost, insertionCost, substitutionCost);
      }

      // copy v1 (current row) to v0 (previous row) for next iteration
      // since data in v1 is always invalidated, a swap without a copy would be more efficient
      int tmp[] = v0; v0 = v1; v1 = tmp;
    }

    // after the last swap, the results of v1 are now in v0
    return v0[t.length()];
  }

  private int minimum(int a, int b, int c) {
    if      (a <= b && a <= c) return a;
    else if (b <= a && b <= c) return b;
    else                       return c;
  }

  /**
   * Calculate the edit distance between two strings using the Levenshtein algorithm.  Ported from C code listed on:
   *
   * https://en.wikipedia.org/wiki/Levenshtein_distance
   *
   * Relatively expensive operation...
   *
   *@param s string 1
   *@param t string 2
   *
   *@return number of edits to convert string 1 into string 2
   */
  protected int levenshteinDistance(String s, String t) {
    String key = Utils.encToURL(s) + "|" + Utils.encToURL(t);
    if (map.containsKey(key)) return map.get(key);

    // Base case -- one or both of the strings are empty
    if (s.length() == 0) return t.length();
    if (t.length() == 0) return s.length();

    // Determine if the last characters of the string match
    int cost;
    if (s.charAt(s.length()-1) == t.charAt(t.length()-1)) cost = 0; else cost = 1;
    
    // Return the minimum of delete a character from s, from t, or from both
    int r0 = levenshteinDistance(s.substring(0,s.length()-1), t)                           + 1,
        r1 = levenshteinDistance(s,                           t.substring(0,t.length()-1)) + 1,
        r2 = levenshteinDistance(s.substring(0,s.length()-1), t.substring(0,t.length()-1)) + cost;

    // Return the minimum value
    int min = r0; if (r1 < min) min = r1; if (r2 < min) min = r2;

    // Remember the calculation
    map.put(key, min);

    return min;
  }

  /**
   * Test methods
   */
  public static void main(String args[]) {
    String words[] = { "abc", "def", "adc", "fed", "something to look for", "something else" };
    for (int i=0;i<words.length;i++) {
      for (int j=0;j<words.length;j++) {
        LevenshteinStateful ld = new LevenshteinStateful(words[i], words[j]);
        System.out.println(ld.editDistance() + "\t\"" + words[i] + "\"\t\t\"" + words[j] + "\"");
      }
    }
  }
}

