/* 

Copyright 2019 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.util;

import racetrack.framework.BundlesDT;

import racetrack.visualization.AbridgedSpectra;
import racetrack.visualization.BlueColorScale;
import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.BrightGrayColorScale;
import racetrack.visualization.ColorScale;
import racetrack.visualization.CBYellowRedColorScale;
import racetrack.visualization.GrayColorScale;
import racetrack.visualization.GreenYellowRedColorScale;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import java.awt.geom.Ellipse2D;

import java.awt.image.BufferedImage;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

/**
 *
 */
public class XYPlotter {
  /**
   * List of files to parse to include stats about each file.
   */
  List<Parsable> parsables = new ArrayList<Parsable>();

  /**
   * Constructor...  empty.
   */
  public XYPlotter() { }

  /**
   * Parse the files to determine the number of values for primary and secondary
   * as well as the minimum and maximum timestamp information.
   */
  public void parseFiles() throws IOException {
    Iterator<Parsable> it = parsables.iterator(); while (it.hasNext()) {
      Parsable parsable = it.next();
      parsable.parse(null);
      parsable.printTrackingStats(System.err);
    }
  }
  
  /**
   * Abstract mapper class.
   */
  interface Mapper { 
    public int map(String pri, String sec); 

    public String getYMinLabel();

    public String getYMaxLabel();
  }

  /**
   * Global minimum timestamp
   */
  long global_ts0 = Long.MAX_VALUE,

  /**
   * Global maximum timestamp
   */
       global_ts1 = Long.MIN_VALUE;

  /**
   * Map a timestamp into an x coordinate (normalized)
   *
   *@param ts timestamp in millis
   *
   *@return x coordinate (normalized)
   */
  public double xMap(long ts) {
    return (((double) ts)         - ((double) global_ts0))/
           (((double) global_ts1) - ((double) global_ts0));
  }

  /**
   * Render contexts to apply the data to
   */
  Set<RenderContext> render_contexts = new HashSet<RenderContext>();

  /**
   * Create the various render contexts for the files.
   */
  public void createRenderContexts() {
    // Default render contexts count by records
    // - Count record plots for each individual parsable
    Iterator<Parsable> it_p = parsables.iterator(); while (it_p.hasNext()) {
      Parsable parsable = it_p.next();
      Set<Parsable> set_of_one = new HashSet<Parsable>(); set_of_one.add(parsable);
      render_contexts.add(new RenderContext(set_of_one));
    }
    // - Count record plots for all of the parsables
    render_contexts.add(new RenderContext(parsables));

    // Make additional ones if the count_bys set has members
    if (count_bys.size() > 0) {
      Iterator<String> it_cb = count_bys.iterator(); while (it_cb.hasNext()) {
        String count_by = it_cb.next(); 

        boolean count_by_complete = true; // Do all have the field?

        it_p = parsables.iterator(); while (it_p.hasNext()) {
          Parsable parsable = it_p.next();

          if (parsable.containsField(count_by)) {
            Set<Parsable> set_of_one = new HashSet<Parsable>(); set_of_one.add(parsable);
            render_contexts.add(new RenderContext(set_of_one, count_by));
          } else count_by_complete = false;
        }
        if (count_by_complete) render_contexts.add(new RenderContext(parsables, count_by));
      }
    }

    System.err.println("Total RenderContexts = " + render_contexts.size());
  }

  /**
   * point width
   */
  float  point_w = 1.0f;

  /**
   * Set the point width.
   *
   *@param pw point width
   */
  public void setPointWidth(float pw) { point_w = pw; }

  /**
   * buffered image width
   */
  int    bi_w = 900,
  
  /**
   * buffered image height
   */
         bi_h = 500;

  /**
   * Set the rendering width
   *
   *@param w render width
   */
  public void setRenderWidth(int w) { bi_w = w; }

  /**
   * Set the rendering height
   *
   *@param h render height
   */
  public void setRenderHeight(int h) { bi_h = h; }

  /**
   * Add a count-by field.  If the field contains numbers, the sum will be calculated per pixel.
   * If the field is categorical, the sum will be the set size.
   */
  public void addCountBy(String field) {
    count_bys.add(field);
  }

  /**
   * Fields used to count by
   */
  protected Set<String> count_bys = new HashSet<String>();

  /**
   * Set the colorscale to use.
   *
   *@param cs color scale
   */
  public void setColorScale(ColorScale cs) { color_scale = cs; }

  /**
   * Color scale to use
   */
  // protected ColorScale color_scale = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL,9);
  // protected ColorScale color_scale = new BrightGrayColorScale();
  // protected ColorScale color_scale = new AbridgedSpectra();
  protected ColorScale color_scale = new CBYellowRedColorScale();

  /**
   *
   */
  class RenderContext {
    /**
     * Y Mapping Function
     */
    Mapper y_mapper;
  
    /**
     * x insert
     */
    int    x_ins = 16,
  
    /**
     * y insert
     */
           y_ins = 16;
  
    /**
     * graph width
     */
    int    graph_w = bi_w - 2*x_ins,
  
    /**
     * graph height
     */
           graph_h = bi_h - 2*y_ins;
  
    /**
     * Parsables to apply to this render context
     */
    Collection<Parsable> setp;

    /**
     * Summation for the pixels in this rendering
     */
    double sum[][];

    /**
     * Each record should be added by sum
     */
    boolean numeric_sum = true;

    /**
     * Set summation holder
     */
    Object  obj[][];

    /**
     * Valid pixels
     */
    boolean valid[][];

    /**
     * Field to count by -- null means to count by records
     */
    String count_by;

    /**
     * Constructor
     * 
     *@param setp     set of parsables
     *@param count_by field to count by - null means to count by records
     */
    public RenderContext(Collection<Parsable> setp, String count_by) {
      this.setp = setp; this.count_by = count_by;
      calculateMapping();
      valid = new boolean[graph_h+1][graph_w+1];

      if (count_by != null) {
        numeric_sum = setp.iterator().next().fieldIsNumeric(count_by);
        if (numeric_sum) sum = new double [graph_h+1][graph_w+1];
        else             obj = new Object [graph_h+1][graph_w+1];
      } else sum = new double [graph_h+1][graph_w+1];
    }

    /**
     * Constructor
     *
     *@param setp set of parsables
     */
    public RenderContext(Collection<Parsable> setp) { this(setp, null); }

    /**
     * Indicate if this render context should render a specific
     * parsable.
     *
     *@param p parsable to check
     *
     *@return true if the parsable is applicable to this render context
     */
    public boolean containsParsable(Parsable p) { return setp.contains(p); }

    /**
     * Accumulate information for the plot
     */
    public void accumulate(long ts, String pri_str, String sec_str, Map<String,String> attr) {
      int sx = (int) (graph_w * xMap(ts)),
          sy = (int) (graph_h - y_mapper.map(pri_str, sec_str));
      valid[sy][sx] =  true;
      if (obj != null && obj[sy][sx] == null) obj[sy][sx] = new HashSet<String>();

      if (count_by == null) sum[sy][sx] += 1.0; else {
        String str = attr.get(count_by);
        if (numeric_sum) sum[sy][sx] += Double.parseDouble(str);
        else             ((HashSet<String>) (obj[sy][sx])).add(str);
      }
    }

    /**
     * Render the final image.
     *
     *@return buffered image
     */
    public BufferedImage getBufferedImage() {
      // Create buffered image... create graphics primitive
      BufferedImage bi  = new BufferedImage(bi_w, bi_h, BufferedImage.TYPE_INT_RGB);
      Graphics2D    g2d = null; 
      try {
        g2d = (Graphics2D) bi.getGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // draw the axis
        g2d.setColor(Color.black);    g2d.fillRect(0,0,bi_w,bi_h);
        g2d.setColor(Color.darkGray); g2d.drawRect(x_ins, y_ins, graph_w, graph_h);

        // Draw the filename
        String filename_str;
        if (setp.size() == 1) filename_str = setp.iterator().next().filename;
        else                  filename_str = "Multiple Files";
        g2d.setColor(Color.white);
        g2d.drawString(filename_str, bi_w/2 - Utils.txtW(g2d, filename_str)/2, y_ins - 2);

        // draw labels in the x-axis
        g2d.setColor(Color.lightGray);
        String str = Utils.exactDate(global_ts0);
        g2d.drawString(str, x_ins,                                  bi_h - y_ins + Utils.txtH(g2d, "0"));
               str = Utils.exactDate(global_ts1);
        g2d.drawString(str, x_ins + graph_w - Utils.txtW(g2d, str), bi_h - y_ins + Utils.txtH(g2d, "0"));

        // draw labels in the y-axis
        String y_min_str = y_mapper.getYMinLabel(), y_max_str = y_mapper.getYMaxLabel();
        Utils.drawRotatedString(g2d, y_min_str, x_ins, y_ins + graph_h);
        Utils.drawRotatedString(g2d, y_max_str, x_ins, y_ins + Utils.txtW(g2d, y_max_str + " "));

        // draw the count by
        g2d.setColor(Color.yellow);
        if (count_by == null) Utils.drawRotatedString(g2d, "Records", bi_w - 1, y_ins + graph_h/2 - Utils.txtW(g2d, "Records")/2);
        else                  Utils.drawRotatedString(g2d, count_by,  bi_w - 1, y_ins + graph_h/2 - Utils.txtW(g2d, count_by) /2);

        //
        // Double counting version -- Determine the minimum and maximums for the sums
        //
        if (obj == null) { 
          double min_sum = Double.POSITIVE_INFINITY,
                 max_sum = Double.NEGATIVE_INFINITY;

          for (int y=0;y<sum.length;y++) for (int x=0;x<sum[y].length;x++) {
            if (valid[y][x]) {
              if (sum[y][x] < min_sum) min_sum = sum[y][x];
              if (sum[y][x] > max_sum) max_sum = sum[y][x];
            }
          }

          // Colorize the picture
          for (int y=0;y<sum.length;y++) for (int x=0;x<sum[y].length;x++) {
            if (valid[y][x]) {
              double f = (sum[y][x] - min_sum)/(max_sum - min_sum);
              Color color = color_scale.at((float) f); g2d.setColor(color);
              if (point_w == 1.0) g2d.fillRect(                 x_ins + x,               y_ins + graph_h - y,               1,       1);
              else                g2d.fill(new Ellipse2D.Double(x_ins + x - point_w/2.0, y_ins + graph_h - y - point_w/2.0, point_w, point_w));
            }
          }

        //
        // Set-based counting version
        //
        } else {
          int min_sz = Integer.MAX_VALUE, max_sz = 0;
          for (int y=0;y<obj.length;y++) for (int x=0;x<obj[y].length;x++) {
            if (valid[y][x]) {
              int sz = ((HashSet<String>) obj[y][x]).size();
              if (sz < min_sz) min_sz = sz;
              if (sz > max_sz) max_sz = sz;
            }
          }

          for (int y=0;y<obj.length;y++) for (int x=0;x<obj[y].length;x++) {
            if (valid[y][x]) {
              double sz = ((HashSet<String>) obj[y][x]).size();
              double f  = (sz - min_sz)/(max_sz - min_sz);
              Color color = color_scale.at((float) f); g2d.setColor(color);
              if (point_w == 1.0) g2d.fillRect(                 x_ins + x,               y_ins + graph_h - y,               1,       1);
              else                g2d.fill(new Ellipse2D.Double(x_ins + x - point_w/2.0, y_ins + graph_h - y - point_w/2.0, point_w, point_w));
            }
          }
        }

      } catch (Throwable t) { } finally { if (g2d != null) g2d.dispose(); }

      return bi;
    }
  
    /**
     * Calculate the mapping for the rendering.
     */
    public void calculateMapping() {
      boolean only_primaries = true; // Only primary keys were specified
      
      float min  = Float.POSITIVE_INFINITY, max  = Float.NEGATIVE_INFINITY,
            min2 = Float.POSITIVE_INFINITY, max2 = Float.NEGATIVE_INFINITY;

      global_ts0 = Long.MAX_VALUE; 
      global_ts1 = Long.MIN_VALUE;
  
      // Determine the types of the primary keys
      Set<BundlesDT.DT>       pri_types = new HashSet<BundlesDT.DT>(),
                              sec_types = new HashSet<BundlesDT.DT>();
      Set<String>             pris      = new HashSet<String>();
      Map<String,Set<String>> pri_map   = new HashMap<String,Set<String>>();

      // Iterate over all the parsables
      Iterator<Parsable> it       = setp.iterator(); while (it.hasNext()) {
        Parsable         parsable = it.next(); 
  
        if (parsable.sec_f != null) only_primaries = false;
  
        if (global_ts0 > parsable.ts0) global_ts0 = parsable.ts0;
        if (global_ts1 < parsable.ts1) global_ts1 = parsable.ts1;
  
        // Primary keys first
        Iterator<String> it_s     = parsable.pri_set.iterator();
        while (it_s.hasNext()) {
          String       str = it_s.next();                        pris.add(str);
          BundlesDT.DT dt  = BundlesDT.getEntityDataType(str);   pri_types.add(dt);

          if (dt == BundlesDT.DT.INTEGER || dt == BundlesDT.DT.FLOAT) {
            float f = Float.parseFloat(str);
            if (f < min) min = f; if (f > max) max = f;
          }

          // Secondary keys if they exist
          if (parsable.pri_map.containsKey(str)) {
            if (pri_map.containsKey(str) == false) pri_map.put(str, new HashSet<String>());

            Iterator<String> it_s2 = parsable.pri_map.get(str).iterator();
            while (it_s2.hasNext()) {
              String str2 = it_s2.next(); pri_map.get(str).add(str2);
              BundlesDT.DT dt2 = BundlesDT.getEntityDataType(str2);
              sec_types.add(dt2);

              if (dt2 == BundlesDT.DT.INTEGER || dt2 == BundlesDT.DT.FLOAT) {
                float f2 = Float.parseFloat(str2);
                if (f2 < min2) min2 = f2; if (f2 > max2) max2 = f2;
              }
            }
          }
        }
      }
  
      //
      // Only one scale
      //
      if (only_primaries) {
        if        (numbersOnly(pri_types)) { y_mapper = new MapperFloat      (min, max, graph_h);
        } else if (ipv4sOnly(pri_types))   { y_mapper = new MapperIPv4       (pris,     graph_h);
        } else                             { y_mapper = new MapperCategorical(pris,     graph_h);      }

      //
      // Two scales - will most likely be equal by default
      //
      } else {
        if        (ipv4sOnly  (pri_types) && ipv4sOnly  (sec_types)) { y_mapper = new MapperIPv4_by_IPv4 (pri_map, graph_h);
        } else if (ipv4sOnly  (pri_types) && numbersOnly(sec_types)) { y_mapper = new MapperIPv4_by_Float(pri_map, graph_h);
        } else if (numbersOnly(pri_types) && ipv4sOnly  (sec_types)) { y_mapper = new MapperFloat_by_IPv4(pri_map, graph_h);
        } else if (                          ipv4sOnly  (sec_types)) { y_mapper = new MapperCat_by_IPv4  (pri_map, graph_h);
        } else if (ipv4sOnly  (pri_types))                           { y_mapper = new MapperIPv4_by_Cat  (pri_map, graph_h);
        } else                                                       { y_mapper = new MapperCat_by_Cat   (pri_map, graph_h); }
      }
    }
  
    /**
     * Floating point (and Integer) based mapper.
     */
    class MapperFloat implements Mapper {
      float min, max; int graph_h;
      public MapperFloat(float min, float max, int graph_h) { 
        this.min = min; this.max = max; this.graph_h = graph_h; }
      public int map(String str, String sec) {
        float f = Float.parseFloat(str);
        return (int) (graph_h * (f - min)/(max-min));
      }
      public String getYMinLabel() { return "" + min; }
      public String getYMaxLabel() { return "" + max; }
    }
  
    /**
     * Determine if the specified set only has numeric strings.
     *
     *@param types set of types
     *
     *@return true if only numeric strings (integers and/or floats)
     */
    protected boolean numbersOnly(Set<BundlesDT.DT> types) {
      return ((types.size() == 1 && types.contains(BundlesDT.DT.INTEGER)) ||
              (types.size() == 1 && types.contains(BundlesDT.DT.FLOAT))   ||
              (types.size() == 2 && types.contains(BundlesDT.DT.INTEGER) &&
                                    types.contains(BundlesDT.DT.FLOAT)));
    }

    /**
     * Mapper for IPv4 address space.
     */
    class MapperIPv4 implements Mapper {
      /**
       * Map for the IPv4 address to the y value
       */
      Map<String,Integer> map = new HashMap<String,Integer>();

      /**
       * Constructor
       *
       *@param set     set of ipv4 addresses
       *@param graph_h graph height
       */
      public MapperIPv4(Set<String> set, int graph_h) {
        long longs[] = new long[set.size()]; Map<Long,String> rmap = new HashMap<Long,String>(); int i = 0;

        // Go through all the ips and convert them to longs - keep track of the reverse mapping
        Iterator<String> it = set.iterator(); while (it.hasNext()) {
          String str = it.next(); long l = Utils.strToIPInt(str) & 0x00ffffffffL;
          rmap.put(l, str); longs[i++] = l;
        }

        // Sort them
        Arrays.sort(longs);

        // Assign them to coordinates
        for (i=0;i<longs.length;i++) {
          long   l   = longs[i];
          String str = rmap.get(l);
          int    y   = (int) ((graph_h * i)/(longs.length));
          map.put(str, y);
        }
        y_min_label = rmap.get(longs[0]);
        y_max_label = rmap.get(longs[longs.length-1]);
      }

      /**
       * Map an ip address into the y coordinate space.
       *
       *@param str should be an ipv4 address that was in the original constructor set
       *@param sec should be null
       *
       *@return y coordinate of the ipv4 address
       */
      public int map(String str, String sec) { return map.get(str); }

      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }

    /**
     * Check to see if only the IPv4 type is in the data.
     *
     *@param types types set
     *
     *@return true if only ipv4 data
     */
    protected boolean ipv4sOnly(Set<BundlesDT.DT> types) {
      return (types.size() == 1 && types.contains(BundlesDT.DT.IPv4));
    }

    /**
     *
     */
    class MapperCategorical implements Mapper {
      Map<String,Integer> map = new HashMap<String,Integer>();
      public MapperCategorical(Set<String> set, int graph_h) {
        Iterator<String> it = set.iterator(); int i = 0; while (it.hasNext()) {
          String str = it.next();
          if (i == 0)            y_min_label = str;
          if (i == set.size()-1) y_max_label = str;
          int    y   = (int) ((graph_h * i)/(set.size())); i++;
          map.put(str,y);
        }
      }
      public int map(String str, String sec) { return map.get(str); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }


    /**
     *
     */
    class IPSort {
      Set<String>         set;
      List<String>        sorted_list;
      Map<String,Integer> index_lu;

      public IPSort(Set<String> set) {
        this.set = set;

        long             longs[]      = new long[set.size()];
        Map<Long,String> rmap         = new HashMap<Long,String>(); 

        // Go through all the ips and convert them to longs - keep track of the reverse mapping
        int i = 0; Iterator<String> it = set.iterator(); while (it.hasNext()) {
          String str = it.next(); long l = Utils.strToIPInt(str) & 0x00ffffffffL;
          rmap.put(l, str); longs[i++] = l;
        }

        // Sort them
        Arrays.sort(longs);

        // Put them in a structure
        sorted_list = new ArrayList<String>(); index_lu = new HashMap<String,Integer>();
        for (i=0;i<longs.length;i++) { String ip = rmap.get(longs[i]); sorted_list.add(ip); index_lu.put(ip, i); }
      }
    }

    /**
     * IPv4 by IPv4 mapper
     */
    class MapperIPv4_by_IPv4 implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperIPv4_by_IPv4(Map<String,Set<String>> pri_map, int graph_h) {
        // Calculate the total number of rows
        int total_rows = 0;
        Iterator<String> it = pri_map.keySet().iterator(); while (it.hasNext()) {
          String pri = it.next(); total_rows += pri_map.get(pri).size();
        }

        IPSort pri_sort = new IPSort(pri_map.keySet());
        int row = 0; it = pri_sort.sorted_list.iterator(); while (it.hasNext()) {
          String pri = it.next(); map.put(pri, new HashMap<String,Integer>());
          IPSort sec_sort = new IPSort(pri_map.get(pri));
          Iterator<String> it2 = sec_sort.sorted_list.iterator(); while (it2.hasNext()) {
            String sec = it2.next();

            if (row == 0)              y_min_label = pri + ":" + sec;
            if (row == total_rows - 1) y_max_label = pri + ":" + sec;

            int    y   = (row * graph_h)/total_rows; row++;
            map.get(pri).put(sec, y);
          }
        }
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }

    /**
     * IPv4 by Float mapper
     */
    class MapperIPv4_by_Float implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperIPv4_by_Float(Map<String,Set<String>> pri_map, int graph_h) {
        throw new RuntimeException("MapperIPv4_by_Float not implemented yet");
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }

    /**
     * Float by IPv4 mapper
     */
    class MapperFloat_by_IPv4 implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperFloat_by_IPv4(Map<String,Set<String>> pri_map, int graph_h) {
        throw new RuntimeException("MapperFloat_by_IPv4 not implemented yet");
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }


    /**
     * Category by IPv4 mapper
     */
    class MapperCat_by_IPv4 implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperCat_by_IPv4(Map<String,Set<String>> pri_map, int graph_h) {
        // Calculate total rows
        int total_rows = 0; Iterator<String> it = pri_map.keySet().iterator(); while (it.hasNext()) {
          total_rows += pri_map.get(it.next()).size();
        }

        // Sort the categorical...  not sure it usually matters.. but will make different runs consistent
        List<String> sorted = new ArrayList<String>(); sorted.addAll(pri_map.keySet());
        Collections.sort(sorted);

        // construct the rows
        int row = 0; it = sorted.iterator(); while (it.hasNext()) {
          String pri = it.next(); map.put(pri, new HashMap<String,Integer>()); IPSort sec_sort = new IPSort(pri_map.get(pri));
          Iterator<String> it2 = sec_sort.sorted_list.iterator(); while (it2.hasNext()) {
            String sec = it2.next();

            if (row == 0)              y_min_label = pri + ":" + sec;
            if (row == total_rows - 1) y_max_label = pri + ":" + sec;

            int    y   = (row * graph_h)/total_rows; row++;
            map.get(pri).put(sec, y);
          }
        }
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }

    /**
     * IPv4 by Category mapper
     */
    class MapperIPv4_by_Cat implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperIPv4_by_Cat(Map<String,Set<String>> pri_map, int graph_h) {
        int total_rows = 0; Iterator<String> it = pri_map.keySet().iterator(); while (it.hasNext()) {
          total_rows += pri_map.get(it.next()).size();
        }

        IPSort ip_sort = new IPSort(pri_map.keySet());
        int row = 0; it = ip_sort.sorted_list.iterator(); while (it.hasNext()) {
          String pri = it.next(); map.put(pri, new HashMap<String,Integer>());
          List<String> sorted = new ArrayList<String>(); sorted.addAll(pri_map.get(pri)); Collections.sort(sorted);
          Iterator<String> it2 = sorted.iterator(); while (it2.hasNext()) {
            String sec = it2.next();

            if (row == 0)              y_min_label = pri + ":" + sec;
            if (row == total_rows - 1) y_max_label = pri + ":" + sec;

            int    y   = (row * graph_h)/total_rows; row++;
            map.get(pri).put(sec, y);
          }
        }
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }

    /**
     * Category by Category mapper
     */
    class MapperCat_by_Cat implements Mapper {
      /**
       * Map from primary value to secondary value to y value
       */
      Map<String,Map<String,Integer>> map = new HashMap<String,Map<String,Integer>>();

      /**
       * Constructor
       *
       *@param pri_map  Map from the primary value to a set of secondary values
       *@param graph_h  graph_h
       */
      public MapperCat_by_Cat(Map<String,Set<String>> pri_map, int graph_h) {
        int total_rows = 0; Iterator<String> it = pri_map.keySet().iterator(); while (it.hasNext()) {
          total_rows += pri_map.get(it.next()).size();
        }

        List<String> sorted = new ArrayList<String>(); sorted.addAll(pri_map.keySet()); Collections.sort(sorted);
        int row = 0; it = sorted.iterator(); while (it.hasNext()) {
          String pri = it.next(); map.put(pri, new HashMap<String,Integer>());
          List<String> sorted2 = new ArrayList<String>(); sorted2.addAll(pri_map.get(pri)); Collections.sort(sorted2);
          Iterator<String> it2 = sorted2.iterator(); while (it2.hasNext()) {
            String sec = it2.next();

            if (row == 0)              y_min_label = pri + ":" + sec;
            if (row == total_rows - 1) y_max_label = pri + ":" + sec;

            int    y   = (row * graph_h)/total_rows; row++;
            map.get(pri).put(sec, y);
          }
        }
      }

      /**
       * Return the mapping for the primary and second values.
       *
       *@param str primary value
       *@param sec secondary value
       *
       *@return corresponding y value
       */
      public int map(String str, String sec) { return map.get(str).get(sec); }
      String y_min_label = "not-set", y_max_label = "not-set";
      public String getYMinLabel() { return y_min_label; }
      public String getYMaxLabel() { return y_max_label; }
    }
  }

  /**
   * Create the rendering(s)
   */
  public BufferedImage[] renderFiles() throws IOException {
    Iterator<Parsable> it_p = parsables.iterator(); while (it_p.hasNext()) {
      Parsable parsable = it_p.next();
      parsable.parse(render_contexts);
    }

    BufferedImage bis[] = new BufferedImage[render_contexts.size()];
    int rc_i = 0;
    Iterator<RenderContext> it_rc = render_contexts.iterator(); while (it_rc.hasNext()) {
      RenderContext rc = it_rc.next();
      bis[rc_i++] = rc.getBufferedImage();
    }

    return bis;
  }


  /**
   * Parse the header of a file.
   */
  protected Map<String,Integer> parseHeader(String line) {
    String tokens[] = tokenize(line,determineDelimiter(line));
    Map<String,Integer> map = new HashMap<String,Integer>();
    for (int i=0;i<tokens.length;i++) map.put(tokens[i], i);
    return map;
  }

  /**
   * Tokenize a line into discrete strings.
   *
   *@param line      line to tokenize
   *@param delimiter delimiter for the line
   *
   *@return tokenized line as a string array
   */
  protected String[] tokenize(String line, String delimiter) {
    if (delimiter.equals(",") && (line.indexOf("\",") >= 0 || line.indexOf(",\"") >= 0)) {
      throw new RuntimeException("No Support Yet For Parsing Excel CSV Files...");
      // return tokenizeExcelCSV(line); // to implement
    }

    StringTokenizer st = new StringTokenizer(line, delimiter, true);
    ArrayList<String> ls = new ArrayList<String>();
    while (st.hasMoreTokens()) ls.add(st.nextToken());

    // System.err.println("line = \"" + line + "\"");
    // for (int i=0;i<ls.size();i++) System.err.println("ls.get(" + i + ") = \"" + ls.get(i) + "\"");

    // Make sure blank tokens are represented
    if (ls.get(0).          equals(delimiter)) ls.add(0,"");
    if (ls.get(ls.size()-1).equals(delimiter)) ls.add("");
    int i = 0;
    while (i < ls.size()-2) {
      if (ls.get(i).equals(delimiter) && ls.get(i+1).equals(delimiter)) ls.add(i+1,"");
      i++;
    }
    // System.err.println("ls.size() = " + ls.size());
    String tokens[] = new String[ls.size()/2 + 1];
    // System.err.println("tokens.length = " + tokens.length);
    for (i=0;i<tokens.length;i++) tokens[i] = stripSpaces(ls.get(i*2));
    // for (i=0;i<tokens.length;i++) System.err.println("tokens[" + i + "] = \"" + tokens[i] + "\"");
    return tokens;
  }

  /**
   * Strip spaces for a string both before and after.  Not efficient...
   *
   *@param str string to process
   *
   *@return string without spaces at the beginning or the ending.
   */
  private String stripSpaces(String str) {
    while (str.startsWith(" ")) str = str.substring(1,str.length());
    while (str.endsWith  (" ")) str = str.substring(0,str.length()-1);
    return str;
  }

  /**
   * Calculate the delimiter for a string...  checks for commas, pipes, and tabs...
   * chooses the one that has the most counts.  Should only be applied to the header row.
   *
   *@param str
   *
   *@return determined delimiter
   */
  protected String determineDelimiter(String str) {
    int commas = 0, pipes = 0, tabs = 0;
    for (int i=0;i<str.length();i++) {
      if      (str.charAt(i) == ',')  commas++;
      else if (str.charAt(i) == '|')  pipes++;
      else if (str.charAt(i) == '\t') tabs++;
    }
    if      (commas > pipes  && commas > tabs)  return ",";
    else if (pipes  > commas && pipes  > tabs)  return "|";
    else if (tabs   > commas && tabs   > pipes) return "\t";
    else throw new RuntimeException("Cannot Determine Delimiters For Header \"" + str + "\"");
  }

  /**
   * Add a parsable file to the plotter.  Specify which fields to use in the parsing.
   *
   *@param filename filename to parse
   *@param ts_f     field name for timestamp
   *@param pri_f    primary field for the plot
   *@param sec_f    secondary field for the plot
   *
   */
  public void addParsable(String filename, String ts_f, String pri_f, String sec_f) {
    parsables.add(new Parsable(filename, ts_f, pri_f, sec_f));
  }

  /**
   * Parsable data structure
   */
 class Parsable {
    /**
     * Delimiter -- should be set to null before each parsing
     */
    String delimiter = null;

    /**
     * Filename to parse
     */
    String filename, 
    /**
     * Field name for timestamp
     */
           ts_f, 
    /**
     * Primary field for plotting
     */
           pri_f, 
    /**
     * Secondary field for plotting - can be null
     */
           sec_f; 

    /**
     * Number of rows parsed
     */
    int rows_parsed = 0;

    /**
     * Constructor
     *
     *@param fn   filename
     *@param ts   timestamp field
     *@param pri  primary plotting field
     *@param sec  secondary plotting field
     */
    public Parsable(String fn, String ts, String pri, String sec) {
      this.filename = fn; this.ts_f = ts; this.pri_f = pri; this.sec_f = sec;
    }

    /**
     * Minimum timestamp found in the file
     */
    long ts0 = Long.MAX_VALUE, 

    /**
     * Maximum timestamp found in the file
     */
         ts1 = Long.MIN_VALUE;

    /**
     * Set of values found in the primary plotting field
     */
    Set<String>              pri_set = new HashSet<String>();

    /**
     * Map of primary plotting field values and the secondary values found
     */
    Map<String,Set<String>>  pri_map = new HashMap<String,Set<String>>();

    /**
     * Track a row's timestamp and primary/secondary field values
     *
     *@param ts  timestamp value (millis)
     *@param pri primary field value
     *@param sec secondary field value
     */
    public void track(long ts, String pri, String sec) {
      rows_parsed++;
      if (ts < ts0) ts0 = ts; if (ts > ts1) ts1 = ts;
      pri_set.add(pri);
      if (pri_map.containsKey(pri) == false) pri_map.put(pri, new HashSet<String>());
      pri_map.get(pri).add(sec);
    }

    /**
     * Track a row's timestamp and primary field value
     *
     *@param ts timestamp value (millis)
     *@param pri primary field value
     */
    public void track(long ts, String pri) {
      rows_parsed++;
      if (ts < ts0) ts0 = ts; if (ts > ts1) ts1 = ts;
      pri_set.add(pri);
    }

    /**
     * Rows that did not have the right number of tokens... misnamed
     */
    int too_few_tokens = 0;

    /**
     * Track rows that have too few / not the right number of tokens... misnamed
     */
    public void tooFewTokens(String line, int row_i) {
      if (too_few_tokens == 0) System.err.println("Line \"" + line + "\" (" + row_i + ") Has Too Few/Many Tokens (Further Errors Suppressed)");
      too_few_tokens++;
    }

    /**
     * Tracks data types per column
     */
    Map<String,Set<BundlesDT.DT>> types_map = new HashMap<String,Set<BundlesDT.DT>>();

    /**
     * For a row, track all of the data types across the headers.
     *
     *@param tokens tokens for a row
     */
    public void trackTypes(String tokens[]) {
      Iterator<String> it = hdr_map.keySet().iterator(); while (it.hasNext()) {
        String hdr   = it.next();
        String token = tokens[hdr_map.get(hdr)];
        types_map.get(hdr).add(BundlesDT.getEntityDataType(token));
      }
    }

    /**
     * Header map for this parsable.  Converts header names into their column index.
     */
    Map<String,Integer>           hdr_map;

    /**
     * Check to see if the parsable contains a specific field.
     *
     *@param field field to check for
     *
     *@return true if parsable contains field
     */
    public boolean containsField(String field) { return hdr_map.containsKey(field); }

    /**
     * Determine if a field only contains integers or floating point values.
     *
     *@param field field to check
     *
     *@return true if and only if the types are integers, floating points, or integers and floating points
     */
    public boolean fieldIsNumeric(String field) {
      return (types_map.get(field).size() == 1 && types_map.get(field).contains(BundlesDT.DT.INTEGER)) ||
             (types_map.get(field).size() == 1 && types_map.get(field).contains(BundlesDT.DT.FLOAT))   ||
             (types_map.get(field).size() == 2 && types_map.get(field).contains(BundlesDT.DT.INTEGER)  &&
                                                  types_map.get(field).contains(BundlesDT.DT.FLOAT));
    }

    /**
     * Parse the file for tracking purposes.  First stage of a two stage process.  The second stage
     * will use the values of the first stage to calculate render mappings.
     */
    public void parse(Set<RenderContext> rcs) throws IOException {
      BufferedReader in = new BufferedReader(new FileReader(filename));

      // Work out the header
      if (rcs == null) {
        String hdr = in.readLine(); delimiter = determineDelimiter(hdr); hdr_map = parseHeader(hdr); 
        Iterator<String> it = hdr_map.keySet().iterator(); while (it.hasNext()) types_map.put(it.next(), new HashSet<BundlesDT.DT>());

        if (hdr_map.containsKey(ts_f) == false) {
          in.close();
          throw new RuntimeException("Field \"" + ts_f + "\" Not In File \"" + filename + "\"");
        }
        if (hdr_map.containsKey(pri_f) == false) {
          in.close();
          throw new RuntimeException("Field \"" + pri_f + "\" Not In File \"" + filename + "\"");
        }
        if (sec_f != null && hdr_map.containsKey(sec_f) == false) {
          in.close();         
          throw new RuntimeException("Field \"" + sec_f + "\" Not In File \"" + filename + "\"");
        }
      } else in.readLine(); // consume the header row

      // Work throught the file -- assumes that each row is complete (i.e., no split line tokens)
      String line; int row_i = 1; while ((line = in.readLine()) != null) {

        // Carve out the tokens
        String tokens[] = tokenize(line, delimiter); row_i++;

        if (tokens.length != hdr_map.keySet().size()) { tooFewTokens(line, row_i); } else {

          String ts_str       = tokens[hdr_map.get(ts_f)],
                 pri_str      = tokens[hdr_map.get(pri_f)];
          String sec_str      = null;
          if (sec_f != null) sec_str = tokens[hdr_map.get(sec_f)];

          // Validate timestamps
          long ts = Utils.parseTimeStamp(ts_str);

          //
          // Render... or track values
          //
          if (rcs != null) {
            // Store all info in a hashmap
            Map<String,String> attr = new HashMap<String,String>();
            Iterator<String> it_str = hdr_map.keySet().iterator(); while (it_str.hasNext()) {
              String key = it_str.next(); String value = tokens[hdr_map.get(key)];
              attr.put(key, value);
            }

            // Add to each render context
            Iterator<RenderContext> it_rc = rcs.iterator(); while (it_rc.hasNext()) {
              RenderContext rc = it_rc.next();
              if (rc.containsParsable(this)) {
                rc.accumulate(ts, pri_str, sec_str, attr);
              }
            }

          //
          // Else Track Values
          //
          } else {
            if (sec_f != null) track(ts, pri_str, sec_str);
            else               track(ts, pri_str);

            trackTypes(tokens);
          }
        }

      }
      in.close();
    }

    /**
     * Print tracking stats.
     *
     *@param out output print stream
     */
    public void printTrackingStats(PrintStream out) {
      out.println("Tracking Stats for \"" + filename + "\"");
      out.println("  ts0 = " + Utils.exactDate(ts0) + " ... ts1 = " + Utils.exactDate(ts1));
      out.println("  primary field \"" + pri_f + "\" contained " + pri_set.size() + " values...");
      out.println("  rows parsed = " + rows_parsed);
      
      Iterator<String> it = types_map.keySet().iterator(); while (it.hasNext()) {
        String hdr = it.next();
        out.print("    hdr \"" + hdr + "\":");
        Iterator<BundlesDT.DT> it_dt = types_map.get(hdr).iterator();
        while (it_dt.hasNext()) { out.print(" " + it_dt.next()); }
        out.println();
      }

      if (too_few_tokens > 0) out.println("  Lines w/ incorrect token counts: " + too_few_tokens);
      out.println();
    }
  }

  /**
   * Main subroutine - handles parsing of arguments and then calls the plotter functions
   * to both map out the files and then to render them.
   */
  public static void main(String args[]) {
    try {
      if (args.length == 0) { printUsage(System.err); return; }

      XYPlotter plotter = new XYPlotter();

      // Parse the arguments
      int i = 0;
      while (i < args.length) {
        int j = handleOptionArguments(args, i, plotter);
        if (j != i) { i = j; } else {

          String filename = null, ts_field = null, pri_field = null, sec_field = null;
          if (i < args.length && fileExists(args[i]) == true) filename = args[i++];
          else throw new RuntimeException("Argument \"" + args[i] + "\" Is Not A File");

          if (i < args.length) ts_field = args[i++];
          else throw new RuntimeException("No Argument for Timestamp Field for File \"" + filename + "\"");

          if (i < args.length) pri_field = args[i++];
          else throw new RuntimeException("No Argument for Primary Field for File \"" + filename + "\"");

          if (i < args.length && fileExists(args[i]) == false) sec_field = args[i++];

          plotter.addParsable(filename, ts_field, pri_field, sec_field);
        }
      }

      // Parse the files for the information to construct the rendering
      plotter.parseFiles();

      // Calculate the mapping
      plotter.createRenderContexts();

      // Render the files
      BufferedImage renderings[] = plotter.renderFiles();

      // Write them to disk
      String base_str = Utils.fileDateStr();
      for (i=0;i<renderings.length;i++) {
        ImageIO.write(renderings[i], "PNG",  new FileOutputStream(new File(base_str + "_" + i + ".png")));
      }
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }

  /**
   * Checks for file existance.
   *
   *@param str filename
   */
  public static boolean fileExists(String str) throws IOException { return (new File(str)).exists(); }

  /**
   * Handle optional arguments.
   *
   *@param args    arguments array
   *@param arg_i   argument to check for option
   *@param plotter plotter structure to modify
   *
   *@return next argument index to parse
   */
  public static int handleOptionArguments(String args[], int arg_i, XYPlotter plotter) {
    if      (args[arg_i].equals("-h"))       { plotter.setRenderHeight(Integer.parseInt(args[arg_i+1])); arg_i += 2; }
    else if (args[arg_i].equals("-w"))       { plotter.setRenderWidth (Integer.parseInt(args[arg_i+1])); arg_i += 2; }
    else if (args[arg_i].equals("-pw"))      { plotter.setPointWidth  (Float.parseFloat(args[arg_i+1])); arg_i += 2; }
    else if (args[arg_i].equals("-countby")) { plotter.addCountBy     (args[arg_i+1]);                   arg_i += 2; }
    else if (args[arg_i].equals("-cs")) {
      String cs_str = args[arg_i+1];

      if      (cs_str.equals("abridged-spectra"))   plotter.setColorScale(new AbridgedSpectra());
      else if (cs_str.equals("blue"))               plotter.setColorScale(new BlueColorScale());
      else if (cs_str.equals("bright-gray"))        plotter.setColorScale(new BrightGrayColorScale());
      else if (cs_str.equals("yellow-red"))         plotter.setColorScale(new CBYellowRedColorScale());
      else if (cs_str.equals("gray"))               plotter.setColorScale(new GrayColorScale());
      else if (cs_str.equals("green-yellow-red"))   plotter.setColorScale(new GreenYellowRedColorScale());
      else if (cs_str.equals("brewer"))             plotter.setColorScale(new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL,9));
      else throw new RuntimeException("Unknown Color Scale \"" + cs_str + "\"");

      arg_i += 2;
    }

    return arg_i;
  }

  /**
   * Print usage.
   *
   *@param out output print stream
   */
  public static void printUsage(PrintStream out) {
    out.println("**\n** Usage\n**\n");
    out.println("java XYPlotter [options] csv-file-1 timestamp-field primary-field [secondary-field] [csv-file-2 ...]...");
    out.println();
    out.println("  Options:");
    out.println();
    out.println("  -h       1000  : set the render height");
    out.println("  -w       1000  : set the render width");
    out.println("  -pw      2.5   : set the point width");
    out.println("  -countby field : add a field to count by");
    out.println("  -cs      {abridged-spectra, blue, bright-gray, yellow-red, gray, green-yellow-red, brewer}");
    out.println();
  }
}

