/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.geom.Arc2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import racetrack.analysis.TextAnalytics;
import racetrack.analysis.ClassicalMDS;
import racetrack.analysis.HierarchicalClustering;

import racetrack.util.Utils;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.RTColorManager;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;

import cc.mallet.pipe.iterator.ArrayIterator;

import cc.mallet.topics.ParallelTopicModel;

import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;

import tagbio.umap.Umap;

/**
 * Topic modeling dialog...
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class TopicModelDialog extends JDialog {
  private static final long serialVersionUID = -237124942718129248L;

  /**
   * Reference to the control frame / parent
   */
  RTControlFrame      rt_control_frame;

  /**
   * Where to store the final results ... but also the set of strings to topic model.
   */
  Map<String,Point2D> results_e_to_wxy,

  /**
   * Initial results of the calculation and embedding.
   */
                      calc_e_to_wxy;

  /**
   * Apply results button
   */
  JButton apply_bt,

  /**
   * Cancel/close dialog button
   */
          cancel_bt,

  /**
   * Attempt to calculate topic count
   */
          guess_count_bt,

  /**
   * Calculate the topics
   */
          calc_topics_bt,

  /**
   * Calculate the embedding
   */
          calc_embed_bt;

  /**
   * Slider for the topic counts
   */
  JSlider topic_count_sl,

  /**
   * Iterations for topic modeler
   */
          topic_iters_sl,

  /**
   * UMap neighbors
   */
          umap_nbors_sl;

  /**
   * Checkbox indicating that the x coord should be pulled from the last embedding.
   */
  JCheckBox vertical_pull_from_local_cb;

  /**
   * Checkbox indicating that the y coord should be pulled from the last embedding.
   */
  JCheckBox horizontal_pull_from_local_cb;

  /**
   * The actual number of topics chosen... not editable
   */
  JTextField topic_count_tf;

  /**
   * UMap Results Component
   */
  EmbedComponent embed_comp;

  /**
   * Tabbed pane for choosing embedding
   */
  JTabbedPane embed_tabs;

  /**
   * Tab names for embed options
   */
  private static String TAB_UMAP_STR       = "UMAP",
                        TAB_MDS_STR        = "MDS",
                        TAB_CIRCULAR_STR   = "Circle",
                        TAB_VERTICAL_STR   = "Vert",
                        TAB_HORIZONTAL_STR = "Horiz";

  /**
   * Constructor - construct the dialog and copy the supplied values to internal members.
   */
  public TopicModelDialog(RTControlFrame rt_control_frame0, Map<String,Point2D> results_e_to_wxy0) {
    super(rt_control_frame0, "Topic Modeling Dialog", true);

    this.rt_control_frame = rt_control_frame0;
    this.results_e_to_wxy = results_e_to_wxy0;
    this.calc_e_to_wxy    = new HashMap<String,Point2D>();
    Iterator<String> it = results_e_to_wxy.keySet().iterator(); while (it.hasNext()) { String e = it.next(); calc_e_to_wxy.put(e,results_e_to_wxy.get(e)); }

    // Put them into a list
    documents = new ArrayList<String>(); documents.addAll(calc_e_to_wxy.keySet());

    // Options for topic modeling and embeddment
    JPanel options_panel = new JPanel(new BorderLayout(10,10));

    // Topic modeling options
    JPanel topic_modeling_options_panel = new JPanel(new BorderLayout());
    topic_modeling_options_panel.add("North",  guess_count_bt = new JButton("Calc Topics")); 
      guess_count_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { calculateTopicCount(); } } );

      JPanel sliders = new JPanel(new GridLayout(1,2,5,5));
        JPanel subslider = new JPanel(new BorderLayout());
          subslider.add("North",  new JLabel("Topics"));
          subslider.add("Center", topic_count_sl = new JSlider(SwingConstants.VERTICAL,  2,  52, 10)); 
            topic_count_sl.setPaintTicks(true); topic_count_sl.setPaintLabels(true);
            topic_count_sl.setMajorTickSpacing(10);
            topic_count_sl.setMinorTickSpacing(1);
            topic_count_sl.setSnapToTicks(true);
          sliders.add(subslider);
        subslider = new JPanel(new BorderLayout());
          subslider.add("North", new JLabel("Iters"));
          subslider.add("Center", topic_iters_sl = new JSlider(SwingConstants.VERTICAL, 10, 100, 50)); 
            topic_iters_sl.setPaintTicks(true); topic_iters_sl.setPaintLabels(true);
            topic_iters_sl.setMajorTickSpacing(20);
            topic_iters_sl.setMinorTickSpacing(1);
            topic_iters_sl.setSnapToTicks(true);
          sliders.add(subslider);

        topic_modeling_options_panel.add("Center", sliders);

    options_panel.add("West", topic_modeling_options_panel);

    // Individual embed panel options
    JPanel umap_panel = new JPanel(new BorderLayout());
      umap_panel.add("North", new JLabel("Nbors"));
      umap_panel.add("Center", umap_nbors_sl = new JSlider(SwingConstants.VERTICAL,  10,  30, 15)); umap_nbors_sl.setPaintTicks(true);
    JPanel mds_panel        = new JPanel();
    JPanel circular_panel   = new JPanel();
    JPanel vertical_panel   = new JPanel(new BorderLayout());
      vertical_panel.  add("North", vertical_pull_from_local_cb   = new JCheckBox("Pull Local X", false));
    JPanel horizontal_panel = new JPanel(new BorderLayout());
      horizontal_panel.add("North", horizontal_pull_from_local_cb = new JCheckBox("Pull Local Y", false));

    // Tabbed panel for the embed panels
    embed_tabs = new JTabbedPane();

    embed_tabs.add(TAB_UMAP_STR,       umap_panel);
    embed_tabs.add(TAB_MDS_STR,        mds_panel);
    embed_tabs.add(TAB_CIRCULAR_STR,   circular_panel);
    embed_tabs.add(TAB_VERTICAL_STR,   vertical_panel);
    embed_tabs.add(TAB_HORIZONTAL_STR, horizontal_panel);

    options_panel.add("Center", embed_tabs);

    // Topics and Embed action buttons
    JPanel south_panel = new JPanel(new GridLayout(1,2,0,0));
      south_panel.add(calc_topics_bt = new JButton("Topics"));
      south_panel.add(calc_embed_bt  = new JButton("Embed"));    calc_embed_bt.setEnabled(false);

    options_panel.add("South",  south_panel);

    add("West", options_panel);

    // UMap results
    add("Center", embed_comp = new EmbedComponent());

    // Button panel
    JPanel button_panel = new JPanel(new FlowLayout());
    button_panel.add(apply_bt  = new JButton("Apply Results")); apply_bt.setEnabled(false);
      apply_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyResults(); } } );
    button_panel.add(cancel_bt = new JButton("Cancel"));
      cancel_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { closeDialog(); } } );
    add("South", button_panel);

    // Listeners
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { closeDialog(); } } );

    topic_count_sl.  addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { reasonableReCalcs(); } } );
    topic_iters_sl.  addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { reasonableReCalcs(); } } );
    umap_nbors_sl.   addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { reasonableReCalcs(); } } );

    calc_topics_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { 
      try { 
        calc_topics_bt.setEnabled(false); calc_embed_bt.setEnabled(false);  apply_bt.setEnabled(false);
        calculateTopics(); 
        calc_topics_bt.setEnabled(true);  calc_embed_bt.setEnabled(true);   apply_bt.setEnabled(false);
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(getDialog(), "IOException: " + ioe, "IO Exception", JOptionPane.ERROR_MESSAGE);
    } } } );
    calc_embed_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      calc_topics_bt.setEnabled(false); calc_embed_bt.setEnabled(false);  apply_bt.setEnabled(false);

      String selected_tab = embed_tabs.getTitleAt(embed_tabs.getSelectedIndex());
      if      (selected_tab.equals(TAB_UMAP_STR))       calculateUMAPEmbedding();
      else if (selected_tab.equals(TAB_MDS_STR))        calculateMDSEmbedding();
      else if (selected_tab.equals(TAB_CIRCULAR_STR))   calculateCircularEmbedding();
      else if (selected_tab.equals(TAB_HORIZONTAL_STR)) calculateHorizontalEmbedding();
      else if (selected_tab.equals(TAB_VERTICAL_STR))   calculateVerticalEmbedding();
      else JOptionPane.showMessageDialog(getDialog(), "Embed option " + selected_tab + " missing", "Missing Embed Option", JOptionPane.ERROR_MESSAGE);

      calc_topics_bt.setEnabled(true);  calc_embed_bt.setEnabled(true);   apply_bt.setEnabled(true);
    } } );

    // Final step then display
    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  /**
   * If it's a small number of items, do the recalculations interactively
   */
  private void reasonableReCalcs() {
    if (results_e_to_wxy.keySet().size() < 200) {
      // try { calculateTopicsAndEmbedding(); } catch (IOException ioe) { System.err.println("IOException: " + ioe); }
    }
  }

  /**
   * Needed for anonymous classes that need access to the dialog reference.
   */
  public JDialog getDialog() { return this; }

  /**
   * List of the documents ... created and filled at construction - should not be modified
   */
  List<String>       documents = null;

  /**
   * Topic model
   */
  ParallelTopicModel model = null;

  /**
   * Instances to model
   */
  InstanceList       instances = null;

  /**
   * Number of Topics Used for Model
   */
  int                model_topic_count = 10;

  /**
   * Calculate the topics.
   */
  public synchronized void calculateTopics() throws IOException {
    // Minus stop words
    List<String> minus_stopwords = new ArrayList<String>();
    for (int i=0;i<documents.size();i++) { 
      String tokens[] = TextAnalytics.tokenize(documents.get(i),false);
      StringBuffer sb = new StringBuffer(); 
      for (int j=0;j<tokens.length;j++) { 
        // Check the localized stop words
        if (local_stop_words.contains(tokens[j].toLowerCase())) continue;

        // Otherwise append (with a preceding space)
        if (j > 0) sb.append(" "); 
        sb.append(tokens[j]); 
      }
      minus_stopwords.add(sb.toString());
    }

    // Convert to the format needed by mallet -- with some transforms
    // Pipes: lowercase, tokenize, remove stopwords, map to features
    ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
    pipeList.add( new CharSequenceLowercase() );
    pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
    // pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
    pipeList.add( new TokenSequence2FeatureSequence() );

    instances = new InstanceList(new SerialPipes(pipeList));
    instances.addThruPipe(new ArrayIterator(minus_stopwords));

    //
    // Run the topic modeler
    //   Constructor:  ParallelTopicModel(number_of_topics, alpha_sum, beta) ... 
    //   ... unsure what to plug into alpha_sum and beta -- values were based on example code
    //
    model = new ParallelTopicModel(model_topic_count = topic_count_sl.getValue(), 1.0, 0.01);
    model.addInstances(instances);
    model.setNumThreads(8);
    model.setNumIterations(topic_iters_sl.getValue());
    model.estimate();

    // Setup the rendering
    render_topic_info = true;
    render_embed      = false;
    embed_comp.repaint();
  }

  /**
   * Calculate the umap embedding.
   */
  public synchronized void calculateUMAPEmbedding() { 
    // Convert each piece document into it's feature vector and fill in the distance matrix
    float d[][] = new float[documents.size()][documents.size()];
    for (int i=0;i<documents.size();i++) {
      double vec0[] = model.getTopicProbabilities(i);
      for (int j=0;j<i;j++) {
        double vec1[] = model.getTopicProbabilities(j);
        double sum    = 0.0;
        for (int k=0;k<vec0.length;k++) sum += (vec0[k] - vec1[k])*(vec0[k] - vec1[k]);
        d[i][j] = d[j][i] = (float) Math.sqrt(sum/vec0.length);
      }
    }

    // Run the umap embed operation
    Umap umap = new Umap();
    umap.setNumberComponents(2);
    umap.setNumberNearestNeighbours(umap_nbors_sl.getValue());
    umap.setThreads(8);
    float results[][] = umap.fitTransform(d);

    // Copy results to the calc and ask for a render
    synchronized (calc_e_to_wxy) {
      for (int i=0;i<documents.size();i++) { 
        calc_e_to_wxy.put(documents.get(i),new Point2D.Double(results[i][0],results[i][1])); 
      }
    }

    // Setup the rendering
    render_topic_info = false;
    render_embed      = true;
    embed_comp.repaint();
  }

  /**
   * Calculate the mds embedding.
   */
  public synchronized void calculateMDSEmbedding() { 
    // Convert each piece document into it's feature vector and fill in the distance matrix
    double d[][] = new double[documents.size()][documents.size()];
    for (int i=0;i<documents.size();i++) {
      double vec0[] = model.getTopicProbabilities(i);
      for (int j=0;j<i;j++) {
        double vec1[] = model.getTopicProbabilities(j);
        double sum    = 0.0;
        for (int k=0;k<vec0.length;k++) sum += (vec0[k] - vec1[k])*(vec0[k] - vec1[k]);
        d[i][j] = d[j][i] = Math.sqrt(sum/vec0.length);
      }
    }

    // Run the classical embed operation
    ClassicalMDS mds = new ClassicalMDS(d);
    double results[][] = mds.getResults();

    // Copy results to the calc and ask for a render
    synchronized (calc_e_to_wxy) {
      for (int i=0;i<documents.size();i++) { 
        calc_e_to_wxy.put(documents.get(i),new Point2D.Double(results[i][0],results[i][1])); 
      }
    }

    // Setup the rendering
    render_topic_info = false;
    render_embed      = true;
    embed_comp.repaint();
  }

  /**
   * Place the topics in a circle and then use barycentric placement of each document.
   */
  public synchronized void calculateCircularEmbedding() {
    // Calculate the features of the topics themselves
    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();
    for (int i=0;i<model_topic_count;i++) feature_vectors.put(""+i, new double[documents.size()]);
    for (int i=0;i<documents.size();i++) {
      double prob_vec[] = model.getTopicProbabilities(i);
      for (int j=0;j<model_topic_count;j++) {
        double topic_vec[] = feature_vectors.get(""+j);
        topic_vec[i] += prob_vec[j];
      }
    }

    // Normalize each vector?  Let's try without at first...

    // Cluster and get ordering
    HierarchicalClustering hierarchical_clustering = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
    List<String> order = hierarchical_clustering.dendrogramOrder();

    // Make the topic centers... should really be ordered based on some optimization -- i.e. topics that are similar to one another (or share lots of docs)
    Map<Integer,Point2D> topic_centers = new HashMap<Integer,Point2D>();
    for (int i=0;i<order.size();i++) { 
      double r = 2.0 * Math.PI * Integer.parseInt(order.get(i)) / (model_topic_count + 1);
      topic_centers.put(i, new Point2D.Double(Math.cos(r),Math.sin(r))); 
    }

    // Place each point at the barycenter of those points using the weighting
    for (int i=0;i<documents.size();i++) {
      double vec[] = model.getTopicProbabilities(i);
      double x_sum = 0.0, y_sum = 0.0; int samples = 0;

      for (int j=0;j<vec.length;j++) {
        if (vec[j] > 0.0) {
          x_sum += topic_centers.get(j).getX() * vec[j];
          y_sum += topic_centers.get(j).getY() * vec[j];
          samples++;

        }
      }

      if (samples == 0) samples++; // shouldn't happen
      calc_e_to_wxy.put(documents.get(i), new Point2D.Double(x_sum/samples,y_sum/samples));
    }

    // Setup the rendering
    render_topic_info = false;
    render_embed      = true;
    embed_comp.repaint();
  }

  /**
   * Perform hierarchical clustering of the results and then place horizontally in cluster order.
   */
  public synchronized void calculateHorizontalEmbedding() { genericHierarchicalClusterEmbedding(true, horizontal_pull_from_local_cb.isSelected()); }

  /**
   * Perform hierarchical clustering of the results and then place vertically in cluster order.
   */
  public synchronized void calculateVerticalEmbedding() { genericHierarchicalClusterEmbedding(false,  vertical_pull_from_local_cb.isSelected()); }

  /**
   * Perform hierarchical clustering and then place in the specified orientation in cluster ordering.
   *
   *@param horizontal      true to placement element horizontally
   *@param pull_from_local true pulls from the dialogs last embedment for the non-changing coordinate
   */
  public synchronized void genericHierarchicalClusterEmbedding(boolean horizontal, boolean pull_from_local) {
    // Put the topic features into vector form
    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();
    for (int i=0;i<documents.size();i++) {
      double vec[] = model.getTopicProbabilities(i);
      feature_vectors.put(documents.get(i),vec);
    }

    // Run the hierarhical clustering algorithm
    HierarchicalClustering hierarchical_clustering = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);

    // Get the ordering
    List<String> order = hierarchical_clustering.dendrogramOrder();
    
    // Make a copy of local if set
    Map<String,Point2D> local_copy = null;
    if (pull_from_local) {
      local_copy = new HashMap<String,Point2D>();
      Iterator<String> it = calc_e_to_wxy.keySet().iterator(); while (it.hasNext()) { String e = it.next(); local_copy.put(e,calc_e_to_wxy.get(e)); }
    }

    // Placement
    int order_size = order.size(); if (order_size == 0) order_size = 1; double coord = 0.0, coord_inc = 1.0/order_size; 

    Iterator<String> it = order.iterator(); while (it.hasNext()) {
      String e = it.next();
      if (horizontal) {
        if (pull_from_local && local_copy.containsKey(e)) { calc_e_to_wxy.put(e, new Point2D.Double(coord, local_copy.      get(e).getY()));
        } else                                            { calc_e_to_wxy.put(e, new Point2D.Double(coord, results_e_to_wxy.get(e).getY())); }
      } else          {
        if (pull_from_local && local_copy.containsKey(e)) { calc_e_to_wxy.put(e, new Point2D.Double(local_copy.      get(e).getX(), coord)); 
        } else                                            { calc_e_to_wxy.put(e, new Point2D.Double(results_e_to_wxy.get(e).getX(), coord)); }
      }
      coord += coord_inc;
    }

    // Setup the rendering
    render_topic_info = false;
    render_embed      = true;
    embed_comp.repaint();
  }

  /**
   * Flag is to indicate that the results are valid -- set on dialog close based on how dialog is closed
   */
  boolean results_are_valid = false;

  /**
   * For the calling class -- indicates that the results should be applied.
   *
   *@return true if results should be applied
   */
  public boolean resultsAreValid() { return results_are_valid; }

  /**
   * Close the dialog.
   */
  public void closeDialog() {
    results_are_valid = false;
    setVisible(false); dispose();
  }

  /**
   * Apply the results then close the dialog.
   */
  public void applyResults() {
    synchronized (calc_e_to_wxy) {
      Iterator<String> it = calc_e_to_wxy.keySet().iterator(); while (it.hasNext()) {
        String e = it.next();
        results_e_to_wxy.put(e, calc_e_to_wxy.get(e));
      }
    }
    results_are_valid = true;
    setVisible(false); dispose();
  }

  /**
   * Render the topic information 
   */
  boolean render_topic_info = false,

  /**
   * Render the embedded points
   */
          render_embed      = false;

  /**
   * Local stop words... should make it possible to manipulate and add to global...
   */
  Set<String> local_stop_words = new HashSet<String>();

  /**
   * Render a copy of the result coordinates and other information.
   */
  class EmbedComponent extends JComponent implements MouseListener {
    /**
     *
     */
    public EmbedComponent() { Dimension d = new Dimension(512,512); setPreferredSize(d); setMinimumSize(d); addMouseListener(this); }

    /**
     *
     */
    public void mouseEntered(MouseEvent me)  { }
    public void mouseExited(MouseEvent me)   { }
    public void mousePressed(MouseEvent me)  { }
    public void mouseReleased(MouseEvent me) { }
    public void mouseClicked(MouseEvent me)  { 
      Iterator<Rectangle2D> it = geom_to_words.keySet().iterator(); while (it.hasNext()) {
        Rectangle2D rect = it.next();
        if (rect.contains(me.getX(),me.getY())) {
          String word = geom_to_words.get(rect);
          if (local_stop_words.contains(word.toLowerCase())) local_stop_words.remove(word.toLowerCase());
          else                                               local_stop_words.add(word.toLowerCase());
        }
      }
      repaint();
    }

    /**
     * Geometry to words
     */
    Map<Rectangle2D,String> geom_to_words = new HashMap<Rectangle2D,String>();

    /**
     *
     */
    public void paintComponent(Graphics g) {
      Graphics2D g2d = (Graphics2D) g; g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      if        (render_topic_info) { renderTopicInfo(g2d);
      } else if (render_embed)      { renderEmbedding(g2d);
      } else {
        g2d.setColor(Color.black); g2d.fillRect(0,0,getWidth(),getHeight());
        g2d.setColor(Color.white);
        g2d.drawString("Press \'Topics\' then \'UMAP\'...", 20, 20);
      }
    }

    /**
     * Render the topics words... provide a method to add stop words locally.
     */
    private void renderTopicInfo(Graphics2D g2d) {
      int w = getWidth(), h = getHeight(); g2d.setColor(Color.black); g2d.fillRect(0,0,w,h);
      int txt_h = Utils.txtW(g2d,"ABC");

      Map<Rectangle2D,String> my_geom_to_words = new HashMap<Rectangle2D,String>();


      // Get an array of sorted sets of word ID/count pairs
      ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();

      // Get the data alphabet
      Alphabet                     dataAlphabet     = instances.getDataAlphabet();

      // For each topic, print them out
      for (int topic_i=0;topic_i<model_topic_count;topic_i++) {
        Iterator<IDSorter> iterator = topicSortedWords.get(topic_i).iterator(); int rank = 0, x = 2;
        while (iterator.hasNext() && rank < 16) {
          IDSorter idCountPair = iterator.next();
          String   word        = (String) dataAlphabet.lookupObject(idCountPair.getID());
          double   weight      = idCountPair.getWeight();
      
          if (local_stop_words.contains(word.toLowerCase())) g2d.setColor(Color.darkGray);
          else                                               g2d.setColor(Color.white);

          int word_w = Utils.txtW(g2d, word) + 10; int y = (topic_i + 1) * txt_h;

          Rectangle2D rect = new Rectangle2D.Double(x-1,y-txt_h-1,word_w+2,txt_h+2);
          my_geom_to_words.put(rect,word);

          g2d.drawString(word, x, y);
          x += word_w + 10;

          rank++;
        }
      }

      geom_to_words = my_geom_to_words;
    }

    /**
     * Renders the embedding.
     *
     *@param g2d graphics primitive
     */
    private void renderEmbedding(Graphics2D g2d) {
      // Background
      g2d.setColor(Color.black); g2d.fillRect(0,0,getWidth(),getHeight());
      int w = getWidth(), h = getHeight();

      // Synchronize to avoid race conditions / concurrent modifications
      synchronized (calc_e_to_wxy) {
        // Find min & max
        double x0 = Double.POSITIVE_INFINITY, x1 = Double.NEGATIVE_INFINITY,
               y0 = Double.POSITIVE_INFINITY, y1 = Double.NEGATIVE_INFINITY;
        Iterator<String> it = calc_e_to_wxy.keySet().iterator(); while (it.hasNext()) {
          Point2D pt = calc_e_to_wxy.get(it.next());
          if (pt.getX() < x0) x0 = pt.getX(); if (pt.getX() > x1) x1 = pt.getX();
          if (pt.getY() < y0) y0 = pt.getY(); if (pt.getY() > y1) y1 = pt.getY();
        }

        // Make them sane
        if (Double.isInfinite(x0)) { x0 = y0 = 0.0; x1 = y1 = 0.0; }
        if (Math.abs(x0-x1) <= 0.001) { x0 -= 0.5; x1 += 0.5; }
        if (Math.abs(y0-y1) <= 0.001) { y0 -= 0.5; y1 += 0.5; }

        // Add ten percent
        double ten = (x1 - x0)*0.05; x0 -= ten; x1 += ten;
               ten = (y1 - y0)*0.05; y0 -= ten; y1 += ten;

        // For each topic, make a color ... color is based on the top words (sorted) and the uniquified
        Color colors[] = new Color[model_topic_count];

        //
        // Use brewer colors :)
        //
        if (colors.length >= 3 && colors.length <= 12) {
          BrewerColorScale bcs = new BrewerColorScale(BrewerColorScale.BrewerType.QUALITATIVE, colors.length);
          for (int topic_i=0;topic_i<colors.length;topic_i++) colors[topic_i] = bcs.atIndex(topic_i);

        //
        // Use ugly colors :(
        //
        } else {
          ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
          Alphabet                     dataAlphabet     = instances.getDataAlphabet();
          for (int topic_i=0;topic_i<colors.length;topic_i++) {
            StringBuffer sb = new StringBuffer();
            Iterator<IDSorter> iterator = topicSortedWords.get(topic_i).iterator(); int rank = 0, x = 2;
            while (iterator.hasNext() && rank < 16) {
              IDSorter idCountPair = iterator.next();
              String   word        = (String) dataAlphabet.lookupObject(idCountPair.getID());
              if (sb.length() > 0) sb.append("-"); sb.append(word);
            }
            colors[topic_i] = RTColorManager.getColor(sb.toString());
          }
        }

        // Render to a buffer
        Map<String,List<double[]>> sxy_to_probs = new HashMap<String,List<double[]>>();
        Map<String,Integer>        sxy_to_sx    = new HashMap<String,Integer>(),
                                   sxy_to_sy    = new HashMap<String,Integer>();

        it = calc_e_to_wxy.keySet().iterator(); while (it.hasNext()) {
          String content = null; Point2D pt = calc_e_to_wxy.get(content = it.next());

          // Find the screen location
          int x = (int) (w*(pt.getX() - x0)/(x1 - x0)),
              y = (int) (h*(pt.getY() - y0)/(y1 - y0));
        
          // Find the document index and the calculated probabilities
          int document_index = documents.indexOf(content);
          double topic_distribution[] = model.getTopicProbabilities(document_index);

          // Update the mapping
          String sxy = x + "," + y; sxy_to_sx.put(sxy,x); sxy_to_sy.put(sxy,y);
          if (sxy_to_probs.containsKey(sxy) == false) sxy_to_probs.put(sxy, new ArrayList<double[]>());
          sxy_to_probs.get(sxy).add(topic_distribution);
        }

        // Render to screen ... if it's between 3 and 12 render some pie-chart like thing...
        g2d.setColor(Color.white); double radius = 10.0, slice = 360.0/model_topic_count;

        it = sxy_to_probs.keySet().iterator(); while (it.hasNext()) {
          String sxy = it.next(); int sx = sxy_to_sx.get(sxy), sy = sxy_to_sy.get(sxy);

          // Average the vectors stacked here
          List<double[]> probs = sxy_to_probs.get(sxy);
          double sums[] = new double[probs.get(0).length];
          for (int i=0;i<probs.size();i++) for (int j=0;j<sums.length;j++) sums[j] += (probs.get(i))[j];
          for (int j=0;j<sums.length;j++) sums[j] = sums[j] / probs.size();

          double angle = 0.0;
          for (int j=0;j<sums.length;j++) { if (sums[j] > 0.0) {
            g2d.setColor(colors[j]);
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) (sums[j]*0.5+0.5)));
            g2d.fill(new Arc2D.Double(sx - radius, sy - radius, 2*radius, 2*radius, angle, 360.0 * sums[j], Arc2D.PIE));
            angle += 360.0 * sums[j];
          } }
        }
      }
    }
  }

  /**
   * In development...  calculate something that tells the user the approximate number of topics...
   */
  public void calculateTopicCount() {
    List<String> result_strs = new ArrayList<String>();

    int count = 2; while (count < 40) {
     try {
      // Minus stop words
      List<String> minus_stopwords = new ArrayList<String>();
      for (int i=0;i<documents.size();i++) { 
        String tokens[] = TextAnalytics.tokenize(documents.get(i),false);
        StringBuffer sb = new StringBuffer(); 
        for (int j=0;j<tokens.length;j++) { 
          // Check the localized stop words
          if (local_stop_words.contains(tokens[j].toLowerCase())) continue;

          // Otherwise append (with a preceding space)
          if (j > 0) sb.append(" "); 
          sb.append(tokens[j]); 
        }
        minus_stopwords.add(sb.toString());
      }

      // Convert to the format needed by mallet -- with some transforms
      // Pipes: lowercase, tokenize, remove stopwords, map to features
      ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
      pipeList.add( new CharSequenceLowercase() );
      pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
      // pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
      pipeList.add( new TokenSequence2FeatureSequence() );

      InstanceList instance_list = new InstanceList(new SerialPipes(pipeList));
      instance_list.addThruPipe(new ArrayIterator(minus_stopwords));

      //
      // Run the topic modeler
      //   Constructor:  ParallelTopicModel(number_of_topics, alpha_sum, beta) ... 
      //   ... unsure what to plug into alpha_sum and beta -- values were based on example code
      //
      ParallelTopicModel parallel_topic_model = new ParallelTopicModel(count, 1.0, 0.01);
      parallel_topic_model.addInstances(instance_list);
      parallel_topic_model.setNumThreads(8);
      parallel_topic_model.setNumIterations(topic_iters_sl.getValue());
      parallel_topic_model.estimate();

      // Per document... find the closest topic and add that delta to the rms
      double rms_sum = 0.0;
      for (int i=0;i<documents.size();i++) {
        double topic_distribution[] = parallel_topic_model.getTopicProbabilities(i);
        int    largest_j = 0;
        for (int j=1;j<topic_distribution.length;j++) if (topic_distribution[j] > topic_distribution[largest_j]) largest_j = j;
        rms_sum += Math.pow((1.0 - topic_distribution[largest_j]), 2);
      }

      result_strs.add("Topic Count " + count + "\t\t" + Math.sqrt(rms_sum/documents.size()));

      count++;
     } catch (IOException ioe) { System.err.println("IOException @ Topic Count " + count); ioe.printStackTrace(System.err); }
    }

    for (int i=0;i<result_strs.size();i++) System.err.println(result_strs.get(i));
  }
}

/* ** RECIPE **

import java.io.*;
import java.util.*;
import java.util.regex.*;

import racetrack.analysis.*;
import racetrack.util.*;

import cc.mallet.util.*;
import cc.mallet.types.*;
import cc.mallet.pipe.*;
import cc.mallet.pipe.iterator.*;
import cc.mallet.topics.*;

public class MyTM {
  public static void main(String args[]) {
    try {
      // Configuration
      int max_tweets       = 20000;
      int number_of_topics = 20;

      //
      // Read X tweets
      //
      List<String> documents = new ArrayList<String>();
      new RFC4180CSVReader(new File("tweets.csv"), new CSVTokenConsumer() {
        public void commentLine(String str) { }; public boolean consume(String tokens[], String line, int line_no) { 
          if (line_no == 1) { return true; } // Skip the header...
          if (documents.size() < max_tweets) { documents.add(tokens[2].toLowerCase()); return true; } else return false; }
      } );
      System.err.println("documents.size() = " + documents.size());

      long ts0 = System.currentTimeMillis();

      //
      // Apply my own stop words...
      //
      List<String> minus_stopwords = new ArrayList<String>();
      for (int i=0;i<documents.size();i++) {
        String tokens[] = TextAnalytics.tokenize(documents.get(i),false);
        StringBuffer sb = new StringBuffer();
        for (int j=0;j<tokens.length;j++) { if (j > 0) sb.append(" "); sb.append(tokens[j]); }
        minus_stopwords.add(sb.toString());
      }

      // Begin by importing documents from text to feature sequences
      ArrayList<Pipe> pipeList = new ArrayList<Pipe>();

      // Pipes: lowercase, tokenize, remove stopwords, map to features
      pipeList.add( new CharSequenceLowercase() );
      pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
      // pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
      pipeList.add( new TokenSequence2FeatureSequence() );

      InstanceList instances = new InstanceList(new SerialPipes(pipeList));
      instances.addThruPipe(new ArrayIterator(minus_stopwords));

      //
      // Run the topic modeler
      //
      ParallelTopicModel model = new ParallelTopicModel(number_of_topics, 1.0, 0.01);
      model.addInstances(instances);
      model.setNumThreads(8);
      model.setNumIterations(100);
      model.estimate();

      long ts1 = System.currentTimeMillis();
      System.err.println("Topic Modeler for " + max_tweets + " is " + (ts1 - ts0) + " milliseconds");

      //
      // Examine first X instances
      //
      for (int tweet=0;tweet<5;tweet++) {
        System.out.println("**\n** Tweet : " + documents.get(tweet) + "\n** Minus : " + minus_stopwords.get(tweet) + "\n**");

        // Per word topics?
        Alphabet        data_alphabet = instances.getDataAlphabet(); // Maps word IDs to strings
        FeatureSequence tokens        = (FeatureSequence) model.getData().get(tweet).instance.getData();
        LabelSequence   topics        = model.getData().get(tweet).topicSequence;
        for (int i=0;i<tokens.getLength();i++) {
          System.out.print(i + " data_alphabet.lookupObject() = " + data_alphabet.lookupObject(tokens.getIndexAtPosition(i)));
          System.out.println("\t\t| topics.getIndexAtPosition()  = " + topics.getIndexAtPosition(i));
        }
        System.out.println();

        // Probabability it's in which topic
        double topic_distribution[] = model.getTopicProbabilities(tweet);
        for (int i=0;i<topic_distribution.length;i++) {
          System.out.println("topic " + i + " : " + topic_distribution[i]);
        }
        System.out.println();
      }
    } catch (IOException ioe) { System.err.println("ioe: " + ioe); }
  }
}

*/


