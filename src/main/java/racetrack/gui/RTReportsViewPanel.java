/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;

import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;

import racetrack.util.Entity;
import racetrack.util.EntityExtractor;
import racetrack.util.SubText;
import racetrack.util.Utils;

import racetrack.visualization.RTColorManager;

/**
 * Class that implements a dynamically update reports views.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTReportsViewPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -6261167987943441269L;

  /**
   * As selections occur, mark their entity locations in the scrollbar
   */
  JCheckBoxMenuItem mark_selection_in_scrollbar_cbmi,

  /**
   * As selections occur, autoscroll the text to the first selected entity
   */
                     autoscroll_to_selected_cbmi,

  /**
   * As mouse is moved into a report, dynamically highlight the entities within that report
   */
                     dynamically_highlight_entities_cbmi;

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTReportsViewPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   

    // Create the popup menu
    JMenuItem mi;
    getRTPopupMenu().add(mi                                  = new JMenuItem("New Report..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { newReport(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi                                  = new JMenuItem("Edit Report..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { editReport(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi                                  = new JMenuItem("Select Report"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { selectReport(); } } );
    getRTPopupMenu().add(mi                                  = new JMenuItem("Add Report To Selection"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addReportToSelection(); } } );
    getRTPopupMenu().add(mi                                  = new JMenuItem("Deselect Report"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { deselectReport(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mark_selection_in_scrollbar_cbmi    = new JCheckBoxMenuItem("Mark Selection In Scrollbar", true));
    getRTPopupMenu().add(autoscroll_to_selected_cbmi         = new JCheckBoxMenuItem("Autoscroll To Selected", true));
    getRTPopupMenu().add(dynamically_highlight_entities_cbmi = new JCheckBoxMenuItem("Dynamically Highlight Entities", false));

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi                                  = new JMenuItem("Filter Reports without Selected Entities")); 
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { filterReportsWithoutSelectedEntities(); } } );

    // Make the GUI
    add("Center", component = new RTReportsViewComponent());
  }

  /**
   * Get the settings for the dynamically highlighting entities upon mouse over setting.
   *
   *@return true to dynamically highlight entities
   */
  public boolean dynamicallyHighlightEntitiesOnMouseOver() { return dynamically_highlight_entities_cbmi.isSelected(); }

  /**
   * Mark the current selection as ticks in the scrollbar (takes more compution... may not be efficient).
   *
   *@return true to mark ticks in scrollbar
   */
  public boolean markSelectionInScrollBar() { return mark_selection_in_scrollbar_cbmi.isSelected(); }

  /**
   * Automatically scroll to the first selected entities in the displayed reports.
   *
   *@return true to autoscroll
   */
  public boolean autoScrollToSelected() { return autoscroll_to_selected_cbmi.isSelected(); }

  /**
   * Create the dialog to enter a new report.
   */
  public void newReport() { new RTReportDialog(this, null);    }

  /**
   * Edit the report under the mouse.
   */
  public void editReport() { 
    ReportRenderer rr = ((RTReportsViewComponent) getRTComponent()).rr_under_mouse_at_click;
    if (rr != null) new RTReportDialog(this, rr.bundle); 
  }

  /**
   * Select the report under the mouse.
   */
  public void selectReport() {
    ReportRenderer rr = ((RTReportsViewComponent) getRTComponent()).rr_under_mouse_at_click;
    if (rr != null) {
      Set<String> new_sel = new HashSet<String>();
      new_sel.add(rr.title);
      getRTParent().setSelectedEntities(new_sel);
    }
  }

  /**
   * Add report to the selection.
   */
  public void addReportToSelection() {
    ReportRenderer rr = ((RTReportsViewComponent) getRTComponent()).rr_under_mouse_at_click;
    if (rr != null) {
      Set<String> sel = getRTParent().getSelectedEntities();
      sel.add(rr.title);
      getRTParent().setSelectedEntities(sel);
    }
  }

  /**
   * Deselect the report under the mouse.
   */
  public void deselectReport() {
    ReportRenderer rr = ((RTReportsViewComponent) getRTComponent()).rr_under_mouse_at_click;
    if (rr != null) {
      Set<String> sel = getRTParent().getSelectedEntities();
      sel.remove(rr.title);
      getRTParent().setSelectedEntities(sel);
    }
  }

  /**
   * Filter out the reports that do not contain at least one of the selected entities.
   */
  public void filterReportsWithoutSelectedEntities() { getRTParent().filterReportsWithoutSelectedEntities(); }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "reportsview"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTReportsViewPanel";
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTReportsViewPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTReportsViewPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Override the parent method and capture the focus entities.
   */
  @Override
  public void updateFocusEntities(Set<String> focus_entities) { 
    clearFocusReports();
    Set<Bundle> reports = getRTParent().findReportsByTitles(focus_entities);
    if (reports != null && reports.size() > 0) {
      Iterator<Bundle> it_bundle = reports.iterator(); while (it_bundle.hasNext()) {
        Bundle report = it_bundle.next();
        addReport(report, false);
      }
    }
    getRTComponent().render();
  }

  /**
   * Clear out the reports (that came from focus... not selection)
   */
  protected void clearFocusReports() {
    synchronized (viewed_reports) {
      Iterator<ReportRenderer> it = viewed_reports.iterator(); while (it.hasNext()) {
        ReportRenderer rr = it.next();
        if (rr.isPinned() == false && rr.isSelected() == false) it.remove();
      }
    }
  }

  /**
   * Autoscroll all of the viewed reports to the first entity matching the specified set.
   *
   *@param sels entities to match
   */
  public void autoScrollViewedReports(Set<String> sels) {
    if (sels == null) sels = getRTParent().getSelectedEntities();
    if (sels == null || sels.size() == 0) return;

    Iterator<ReportRenderer> it = viewed_reports.iterator(); while (it.hasNext()) { it.next().autoScroll(sels); }

    getRTComponent().render();
  }

  /**
   * Override the parent method and capture the new selected entities.
   */
  @Override
  public void updateSelection() {
    // Find selected reports
    Set<String> sels    = getRTParent().getSelectedEntities();
    Set<Bundle> reports = getRTParent().findReportsByTitles(sels);

    //
    // If no reports in selection, determine if autoscroll is enabled... 
    //
    if (reports != null && reports.size() == 0) { 
      if (autoScrollToSelected() && sels != null && sels.size() > 0) autoScrollViewedReports(sels);

    //
    // Otherwise, there may be new reports, clear out the viewed list and add the new ones
    //
    } else {
      clearReports();
      if (reports != null && reports.size() > 0) {
        Iterator<Bundle> it_bundle = reports.iterator(); while (it_bundle.hasNext()) {
          Bundle report = it_bundle.next();
          addReport(report, true);
        }
      }
    }

    // Ask for a render (even if nothing happend -- updated selection may highlight new entities in the view)
    getRTComponent().render();
  }

  /**
   * Clear out the reports (except for the pinned ones).
   */
  protected void clearReports() {
    synchronized (viewed_reports) {
      Iterator<ReportRenderer> it = viewed_reports.iterator(); while (it.hasNext()) {
        if (it.next().isPinned() == false) it.remove();
      }
    }
  }

  /**
   * Manage a new report that is selected/focused.
   *
   *@param report report to manage
   */
  protected void addReport(Bundle report, boolean selected) {
    synchronized (viewed_reports) {
      // Check to make sure it's not already in the list
      boolean found = false;
      Iterator<ReportRenderer> it = viewed_reports.iterator(); while (it.hasNext()) {
        if (it.next().getBundle() == report) found = true;
      }
      // If not in the list, add it (or if it's changed...)
      if (found == false) {
        if (report_renderer_cache.containsKey(report)        == false ||
            report_renderer_cache.get(report).equals(report) == false) report_renderer_cache.put(report, new ReportRenderer(report));

        ReportRenderer rr = report_renderer_cache.get(report);

        viewed_reports.add(rr);
        rr.selected = selected;
      }
    }
  }

  //
  // NEED WAY TO RESET THIS STRUCTURE IF NEW ROOT BUNDLES ARE SET
  // NEED WAY TO RESET THIS STRUCTURE IF NEW ROOT BUNDLES ARE SET
  // NEED WAY TO RESET THIS STRUCTURE IF NEW ROOT BUNDLES ARE SET
  //

  /**
   * Cache to remember/recall report renderer
   */
  private Map<Bundle,ReportRenderer> report_renderer_cache = new HashMap<Bundle,ReportRenderer>();

  /**
   * Currently viewed reports
   */
  private List<ReportRenderer> viewed_reports = new ArrayList<ReportRenderer>();

  /**
   *
   */
  class ReportRenderer {
    /**
     * Report bundle
     */
    Bundle        bundle; 

    /**
     * Accessor method for the bundle
     */
    Bundle getBundle() { return bundle; }

    /**
     * Comparison method to see if the underlying bundle has changed
     */
    public boolean equals(Bundle compare) { return getRTParent().getReportTitle(compare).equals(title) && getRTParent().getReportText(compare).equals(text); }

    /**
     * Title of the report
     */
    String        title, 

    /**
     * Text of the report
     */
                  text;

    /**
     * Extracted entities
     */
    List<SubText> subtexts;

    /**
     * Return a list of the subtexts within this report.
     *
     *@return list of subtexts
     */
    public List<SubText> getSubTexts() { return subtexts; }

    /**
     * Maps an entity to the line number occurences
     */
    Map<String,Set<Integer>> entity_to_lines = new HashMap<String,Set<Integer>>();

    /**
     * Maps line index to the subtexts contained on that line
     */
    Map<Integer,List<SubText>> line_to_subtexts = new HashMap<Integer,List<SubText>>();

    /**
     * Text broken into lines
     */
    String        lines[];

    /**
     * Corresponds to the lines member but keeps the character index from the original text
     */
    int           line_pos[];

    /**
     * Initial render line (part of the scrolling system)
     */
    int           start_render_line = 0;

    /**
     * Scroll the view by modifying the start_render_line
     */
    public void scrollView(int inc) {
      int new_value = start_render_line + inc; // Use a temporary variable until it's checked
      if (new_value < 0)              new_value = 0;              // start of the text
      if (new_value > lines.length-1) new_value = lines.length-1; // end of the text
      start_render_line = new_value;
    }

    /**
     * Autoscroll to the line with the first subtext matching the specified set.  Disable this
     * feature if the mouse is in this view.
     *
     *@param set string to search for
     */
    public void autoScroll(Set<String> set) {
      if (((RTReportsViewComponent) getRTComponent()).rr_under_mouse == this) return;

      Iterator<SubText> it = subtexts.iterator(); while (it.hasNext()) {
        SubText subtext = it.next();
        if (set.contains(subtext.toString()) || set.contains(subtext.toString().toLowerCase())) {
          Set<Integer> lines = entity_to_lines.get(subtext.toString()); if (lines != null && lines.size() > 0) {
            List<Integer> sort = new ArrayList<Integer>(); sort.addAll(lines); Collections.sort(sort);
            start_render_line = sort.get(0) - last_render_line_count/2;
            if (start_render_line < 0) start_render_line = 0;
            return;
          }
        }
      }
    }

    /**
     * Last time the renderer had an interaction (for round robin style removal/recycle)
     */
    long          last_interaction = System.currentTimeMillis();

    /**
     * For the last render, this is the number of lines that were rendered -- used to center for autoscroll
     */
    int           last_render_line_count = 0;

    /**
     * Pinned by the user (and therefore won't be cycled out)
     */
    boolean       pinned = false;

    /**
     * Is Pinned Check
     */
    boolean isPinned() { return pinned; }

    /**
     * Set the pinned value.
     *
     *@param b new value
     */
    void setPinned(boolean b) { pinned = b; }

    /**
     * Selected by the user (and therefore won't be cycled out for focus events)
     */
    boolean       selected = false;

    /**
     * Is report shown because it was selected (vice focused)?
     *
     *@return true to indicate selected
     */
    boolean isSelected() { return selected; }

    /**
     * Construct the report renderer
     */
    public ReportRenderer(Bundle report) { bundle = report; initialize(); }

    /**
     * Check to see if the underlying report has changed.  If so, reinitialize.
     */
    public void validate() { if (equals(bundle) == false) initialize(); }

    /**
     * Initialize the data structure
     */
    protected void initialize() {
     synchronized (this) {
      title                  = getRTParent().getReportTitle(bundle);
      text                   = getRTParent().getReportText(bundle);
      subtexts               = EntityExtractor.list(text);
      entity_to_lines        = new HashMap<String,Set<Integer>>();
      line_to_subtexts       = new HashMap<Integer,List<SubText>>();
      lines                  = null;
      line_pos               = null;
      local_max_w            = -1;
      last_render_line_count =  0;
      start_render_line      =  0;

      // Sort the subtexts by occurence
      Collections.sort(subtexts, new Comparator<SubText>() {
        public int compare(SubText st1, SubText st2) {
          return st1.getIndex0() - st2.getIndex1();
        }
      } );

      // Break into lines...  and keep track of entity to line correlations
      List<StringBuffer> bufs  = new ArrayList<StringBuffer>(); bufs.add(new StringBuffer());
      List<Integer>      poses = new ArrayList<Integer>();      poses.add(0);
      StringTokenizer st = new StringTokenizer(text, "\n\t ", true); int pos = 0, this_line_pos = 0, st_i = 0;
      while (st.hasMoreTokens()) {
        String next = st.nextToken(); pos += next.length();

        //
        // Just append whitespace to the last line
        //
        if        (next.equals(" ") || next.equals("\t")) {
          bufs.get(bufs.size()-1).append(next);

        //
        // Upon newline, add a new string buffer
        //
        } else if (next.equals("\n")) { 
          bufs.get(bufs.size()-1).append(next); 
          st_i = mapEntities(subtexts, st_i, this_line_pos, pos, bufs.size()-1);
          bufs.add(new StringBuffer()); poses.add(pos);
          this_line_pos = pos; 

        //
        // Else check to see if we'll exceed the maximum line length... arbitrary number...
        // - maybe better to use the pixel width...
        //
        } else {
          // when line length is exceeded, add a new string buffer
          if ((bufs.get(bufs.size()-1).length() + next.length()) > 100) { 
            st_i = mapEntities(subtexts, st_i, this_line_pos, pos - next.length(), bufs.size()-1);
            bufs.add(new StringBuffer());  poses.add(pos - next.length());
            this_line_pos = pos - next.length(); 
          }

          // Append to the last string buffer
          bufs.get(bufs.size()-1).append(next);
        }
      }

      // Get anything that occured at the end / last line
      st_i = mapEntities(subtexts, st_i, this_line_pos, pos, bufs.size()-1);

      // Transfer back to the array
      lines = new String[bufs.size()]; line_pos = new int[bufs.size()]; 
      for (int i=0;i<lines.length;i++) {
        lines[i]    = bufs.get(i).toString();
        line_pos[i] = poses.get(i);
      }
     }
    }

    /**
     * Map the entities in the text to the specific line number in the rendering.  Modifies the entity_to_lines structure.
     *
     *@param subtexts   extracted entities in sorted order
     *@param st_i       next extracted entity to be placed
     *@param line_start start of the line (character position)
     *@param line_end   end of the line (character_position)
     *@param line_no    current line number
     *
     *@return index of next subtext to be placed
     */
    protected int mapEntities(List<SubText> subtexts, int st_i, int line_start, int line_end, int line_no) {
      while (st_i < subtexts.size() && subtexts.get(st_i).getIndex0() < line_end) {
        String entity = subtexts.get(st_i).toString(); if (entity_to_lines.containsKey(entity) == false) entity_to_lines.put(entity, new HashSet<Integer>());
        entity_to_lines.get(entity).add(line_no);
        if (line_to_subtexts.containsKey(line_no) == false) line_to_subtexts.put(line_no, new ArrayList<SubText>());
        line_to_subtexts.get(line_no).add(subtexts.get(st_i));
        st_i++;
      }
      return st_i;
    }

    /**
     * Find the maximum width line based on this specific graphics primitive... will assume for the
     * the sake of efficiency that it's always the same.
     *
     *@param g2d graphics primitive
     */
    public int maxLineWidth(Graphics2D g2d) {
      if (local_max_w >= 0) return local_max_w;

      if (title != null) local_max_w = Utils.txtW(g2d, title);

      for (int i=0;i<lines.length;i++) {
        int w = Utils.txtW(g2d, lines[i]);
        if (w > local_max_w) local_max_w = w;
      }
      return local_max_w;
    }

    /**
     * Previously calculated maximum width for all of the lines.
     */
    private int local_max_w = -1;

    /**
     * Previously rendered bounds for the title
     */
    private Rectangle2D last_title_bounds = null;

    /**
     * Return the last bounds for the title portion of the render
     */
    public Rectangle2D getTitleBoundingBox() { return last_title_bounds; }

    /**
     * Render this report in the specified screen portion.
     *
     *@param g2da    graphics primitive
     *@param bounds  bounds to render the report into
     */
    public void render(Graphics2D g2d, Rectangle2D bounds) {
     synchronized (this) {
      int         txt_h    = Utils.txtH(g2d, "0") + 1; 
      bounds_to_subtexts.clear();

      // Get the selected for additional visual cues
      Set<String> selected = getRTParent().getSelectedEntities(); if (selected == null) selected = new HashSet<String>();
      Set<String> to_add   = new HashSet<String>();
      Iterator<String> it = selected.iterator(); while (it.hasNext()) to_add.add(it.next().toLowerCase()); // Add lowercase versions to...
      selected.addAll(to_add);

      // Border / placement
      double x0 = bounds.getX(),                     y0 = bounds.getY(), 
             w  = bounds.getWidth(),                 h  = bounds.getHeight(),
             x1 = bounds.getX() + bounds.getWidth(), y1 = bounds.getY() + bounds.getHeight();
      RoundRectangle2D border = new RoundRectangle2D.Double(x0 + 2, y0 + 2, w - 4, h - 4, 8, 8);
      last_title_bounds = new Rectangle2D.Double(x0, y0, w, txt_h + 5);

      // Draw the area for this report & constrain drawing to this area only
      // g2d.setColor(RTColorManager.getColor("background", "nearbg"));  g2d.fill(border);
      g2d.setColor(RTColorManager.getColor("data",       "default")); g2d.draw(border);
      Shape orig_clip = g2d.getClip(); g2d.setClip(border);
      if (isPinned()) { 
        g2d.setColor(RTColorManager.getColor("label", "errorbg"));      g2d.fill(last_title_bounds);
        g2d.setColor(RTColorManager.getColor("data",  "default"));      g2d.draw(border); 
      } else {
        g2d.setColor(RTColorManager.getColor("background", "nearbg"));  g2d.fill(last_title_bounds);
        g2d.setColor(RTColorManager.getColor("data",       "default")); g2d.draw(border); 
      }

      // Draw the title
      if (pinned) g2d.setColor(RTColorManager.getColor("label", "errorfg"));
      else        g2d.setColor(RTColorManager.getColor("label", "major"));
      g2d.drawString(title, (int) (x0 + 4), (int) (y0 + 2 + txt_h));

      // Draw the rest of the text
      g2d.setColor(RTColorManager.getColor("label", "minor")); 
      String left_over = null; SubText left_over_subtext = null; // Left over information
      int y = (int) (y0 + 2 + 2 * txt_h + 4); int line_no = start_render_line, lines_drawn = 0; // Positioning and book keeping info
      while (y <= y1 && line_no < lines.length) {

        g2d.drawString(lines[line_no], (int) (x0 + 4), y); 

        // Render the left over (if it exists)
        if (left_over != null) {
          if (selected.contains(left_over_subtext.toString())) g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));
          else                                                 g2d.setColor(RTColorManager.getColor("label",    "major"));
          g2d.drawString(left_over, (int) (x0 + 4), y);
          Rectangle2D subtext_bounds = new Rectangle2D.Double(x0 + 4, y - txt_h, Utils.txtW(g2d, left_over), txt_h);
          bounds_to_subtexts.put(subtext_bounds, left_over_subtext);
          left_over = null; left_over_subtext = null;
          g2d.setColor(RTColorManager.getColor("label", "minor"));
        }

        // Lightly outline the entities on this line
        if (line_to_subtexts.containsKey(line_no)) {
          Iterator<SubText> it_subs = line_to_subtexts.get(line_no).iterator(); while (it_subs.hasNext()) {
            SubText subtext = it_subs.next(); 

            if (selected.contains(subtext.toString())) g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));
            else                                       g2d.setColor(RTColorManager.getColor("label", "major"));

            String orig_text = subtext.getFullText().substring(subtext.getIndex0(), subtext.getIndex1());
            String pre_text  = "";
            if ((subtext.getIndex0() - line_pos[line_no]) <= lines[line_no].length()) pre_text = lines[line_no].substring(0, subtext.getIndex0() - line_pos[line_no]);

            // Determine if it splits the line
            int    total_len = pre_text.length() + orig_text.length(),
                   over_by   = total_len - lines[line_no].length();
            if (over_by > 0) {
              left_over = orig_text.substring(orig_text.length() - over_by, orig_text.length());
              orig_text = orig_text.substring(0, orig_text.length() - left_over.length());
              left_over_subtext = subtext;
            }

            int x_sub = (int) (x0 + 4 + Utils.txtW(g2d, pre_text));
            g2d.drawString(orig_text, x_sub, y);
            Rectangle2D subtext_bounds = new Rectangle2D.Double(x_sub, y - txt_h, Utils.txtW(g2d, orig_text), txt_h);
            bounds_to_subtexts.put(subtext_bounds, subtext);
          }
          g2d.setColor(RTColorManager.getColor("label", "minor"));
        }

        lines_drawn++; y += txt_h; line_no++;
      }

      // Remember the last count of lines rendered (for autoscroll)
      last_render_line_count = lines_drawn;

      // Draw a simple scrollbar
      scroll_bar_all = new Rectangle2D.Double(x1 - 10, y0 + 8, 10, h - 16);
      g2d.setColor(RTColorManager.getColor("background", "default"));
      g2d.fill(scroll_bar_all);

      g2d.setColor(RTColorManager.getColor("data", "default"));
      g2d.draw(scroll_bar_all);
      double y_start = y0 + 8 + ((h-16) * start_render_line)                 / lines.length,
             y_end   = y0 + 8 + ((h-16) * (start_render_line + lines_drawn)) / lines.length;
      if ((y_end - y_start) < 1.0) y_end = y_start + 1;
      scroll_bar = new Rectangle2D.Double(x1 - 10, y_start, 10, y_end - y_start); 
      g2d.setColor(RTColorManager.getColor("background", "nearbg")); g2d.fill(scroll_bar);
      g2d.setColor(RTColorManager.getColor("data", "default"));      g2d.draw(scroll_bar);

      // Put tics where the selected match
      if (markSelectionInScrollBar() && selected.size() > 0) {
        g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));
        it = entity_to_lines.keySet().iterator(); while (it.hasNext()) {
          String entity = it.next(); if (selected.contains(entity)) {
            Set<Integer> line_nos = entity_to_lines.get(entity); 
            Iterator<Integer> it_int = line_nos.iterator(); while (it_int.hasNext()) {
              line_no = it_int.next();
              y       = (int) (y0 + 8 + ((h-16)*line_no)/ lines.length);
              g2d.drawLine((int) (x1 - 9), y, (int) (x1 - 1), y);
            }
          }
        }
      }

      // Reset the clip
      g2d.setClip(orig_clip);
     }
    }

    /**
     * Bounds for the entire scrollbar
     */
    Rectangle2D scroll_bar_all = null,

    /**
     * Bounds for the view of the scrollbar
     */
                scroll_bar     = null;

    /**
     * Maps bounding rectangles to their subtexts
     */
    Map<Rectangle2D,SubText> bounds_to_subtexts = new HashMap<Rectangle2D,SubText>();
  }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTReportsViewComponent extends RTComponent {
    private static final long serialVersionUID = 322433521124728261L;
    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      return shapes; }
    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    /**
     * The report renderer under the mouse at the click.
     */
    ReportRenderer rr_under_mouse_at_click = null;

    /**
     * Override to handle mouse clicks - pinning reports for example and (whatever happens when you click the report text)
     */
    @Override
    public void mouseClicked(MouseEvent me) {
      rr_under_mouse_at_click = underMouse(me);
      if (me.getButton() == MouseEvent.BUTTON1) {
        ReportRenderer rr = underMouse(me); if (rr != null) {
          Rectangle2D title_bounds = rr.getTitleBoundingBox();

          //
          // Click in the title area pins / unpins the report
          //
          if (title_bounds != null && title_bounds.contains(me.getX(), me.getY())) { 
            rr.setPinned(!rr.isPinned()); 
            render(); 

          //
          // Click in the body...
          //
          } else {
            //
            // In the scroll bar
            //
            if (rr.scroll_bar_all != null && rr.scroll_bar_all.contains(me.getX(), me.getY())) {

            //
            // Definitely in the body
            //
            } else {
              boolean shift = me.isShiftDown();
              boolean ctrl  = me.isControlDown();

              // Check to see if the mouse is on top of an entity
              SubText subtext = null;
              Iterator<Rectangle2D> it_rect = rr.bounds_to_subtexts.keySet().iterator(); while (it_rect.hasNext() && subtext == null) {
                Rectangle2D bounds = it_rect.next(); 
                if (bounds.contains(me.getX(), me.getY())) subtext = rr.bounds_to_subtexts.get(bounds);
              }

              // Subtext found!
              Set<String> already_sel = getRTParent().getSelectedEntities();
              if (subtext != null) {
                String ent = subtext.toString().toLowerCase();
                if        (shift && ctrl) { Set<String> set = new HashSet<String>(); set.add(ent); already_sel.retainAll(set);
                } else if (shift)         { already_sel.remove(ent);
                } else if (         ctrl) { already_sel.add(ent);
                } else                    { already_sel = new HashSet<String>(); already_sel.add(ent);
                }

                getRTParent().setSelectedEntities(already_sel);

              // else - some version of select entities
              } else {
                Set<String> set = new HashSet<String>();
                Iterator<SubText> it = rr.subtexts.iterator(); while (it.hasNext()) {
                  subtext = it.next(); if (subtext instanceof Entity == false) continue;
                  set.add(subtext.toString().toLowerCase());
                }

                if        (shift && ctrl) { already_sel.retainAll(set);
                } else if (shift)         { already_sel.removeAll(set);
                } else if (         ctrl) { already_sel.addAll(set);
                } else                    { already_sel = set; 
                }

                getRTParent().setSelectedEntities(already_sel);
              }
            }
          }
        }
      } else super.mouseClicked(me);
    }

    /**
     * Override to handle scrollbar issues.
     */
    @Override
    public void mousePressed(MouseEvent me) {
      if (me.getButton() == MouseEvent.BUTTON1) {
        ReportRenderer rr = underMouse(me); if (rr != null) {
          setScrollBarPosition(me, rr); // when called, checks to see if the mouse is in the scroll bar
          scroll_bar_drag = true; scroll_bar_drag_rr = rr;
        }
      }
    }

    /**
     * Helper method to position the scrollbar based on the mouse movement.
     */
    protected void setScrollBarPosition(MouseEvent me, ReportRenderer rr) {
      if (rr.scroll_bar_all != null && rr.scroll_bar_all.contains(me.getX(), me.getY())) {
        int new_start = (int) ((rr.lines.length * (me.getY() - rr.scroll_bar_all.getY())) / rr.scroll_bar_all.getHeight() - rr.last_render_line_count/2);
        if (new_start < 0)                new_start = 0;
        if (new_start >= rr.lines.length) new_start = rr.lines.length-1;
        rr.start_render_line = new_start;
        render();
      }
    }

    /**
     * Flag to indicate that scrollbar is being dragged by the mouse.
     */
    private boolean scroll_bar_drag = false;

    /**
     * The ReportRenderer associated with the current scrollbar movement.
     */
    private ReportRenderer scroll_bar_drag_rr = null;

    /**
     * Override to handle scrollbar issue
     */
    @Override
    public void mouseReleased(MouseEvent me) {
      scroll_bar_drag    = false;
      scroll_bar_drag_rr = null;
    }

    /**
     *
     */
    @Override
    public void mouseMoved(MouseEvent me) {
      ReportRenderer rr = underMouse(me);
      if (dynamicallyHighlightEntitiesOnMouseOver() && rr != null && rr.getSubTexts() != null) { 
        Set<SubText> as_set = new HashSet<SubText>();
        as_set.addAll(rr.getSubTexts());
        getRTParent().setEntityHighlights(as_set);
      } else getRTParent().setEntityHighlights(null);
    }

    /**
     *
     */
    @Override
    public void mouseExited(MouseEvent me) {
      getRTParent().setEntityHighlights(null);
      scroll_bar_drag    = false;
      scroll_bar_drag_rr = null;
      rr_under_mouse     = null;
    }

    /**
     * Override to handle the scroll bar dragging interaction.
     */
    @Override
    public void mouseDragged(MouseEvent me) {
      underMouse(me);
      if (scroll_bar_drag == true && scroll_bar_drag_rr != null) setScrollBarPosition(me, scroll_bar_drag_rr);
    }

    /**
     * Override the mouse wheel event to implement simple scrolling in an individual report view.
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent mwe) {
      ReportRenderer rr = underMouse(mwe);
      if (rr != null) { rr.scrollView(mwe.getWheelRotation()); render(); }
    }

    /**
     * Report Renderer last seen under the mouse
     */
    ReportRenderer rr_under_mouse = null;

    /**
     * Return the report rendered underneath the mouse.
     *
     *@param me mouse event
     *
     *@return report rendered under the mouse... null if no report renderer found
     */
    protected ReportRenderer underMouse(MouseEvent me) {
      RenderContext myrc = (RenderContext) getRTComponent().rc; if (myrc != null) {
        Iterator<Rectangle2D> it = myrc.bounds_to_renderer.keySet().iterator(); while (it.hasNext()) {
          Rectangle2D bounds = it.next(); if (bounds.contains(me.getX(), me.getY())) {
            return (rr_under_mouse = myrc.bounds_to_renderer.get(bounds));
          }
        }
      }
      rr_under_mouse = null;
      return null;
    }

    /**
     * Override the key pressed routine...
     */
    @Override
    public void keyPressed(KeyEvent ke) {
      super.keyPressed(ke);
      if (ke.getKeyCode() == KeyEvent.VK_A) autoScrollViewedReports(null);
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy();

      // Create the render context based on the user's parameters
      RenderContext myrc = new RenderContext(id, bs, count_by, color_by, getWidth(), getHeight());
      return myrc;
    }
    
    /**
     * RenderContext implementation for the correlation matrix
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by;

      /**
       * Lookup for a record to the string keys
       */
      Map<Bundle,Set<String>> bundle_to_skeys = new HashMap<Bundle,Set<String>>();

      /**
       * Lookup for a string key to the bundles
       */
      Map<String,Set<Bundle>> skey_to_bundles = new HashMap<String,Set<Bundle>>();

      /**
       * Lookup for a string key to the geometry
       */
      Map<String,Rectangle2D> skey_to_geom    = new HashMap<String,Rectangle2D>();

      /**
       * Lookup for a geometry to a string key
       */
      Map<Rectangle2D,String> geom_to_skey    = new HashMap<Rectangle2D,String>();

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;
/*
        Iterator<Tablet> it_tab = bs.tabletIterator();
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          if (tablet.hasTimeStamps() && tablet_can_count) {
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle     = it_bun.next();
            }
          } else { addToNoMappingSet(tablet); }
        }
*/
      }

      /**
       * Map to look-up the report renderer for bounds
       */
      Map<Rectangle2D,ReportRenderer> bounds_to_renderer = new HashMap<Rectangle2D,ReportRenderer>();

      /**
       * Map to look-up the bounds for a report renderer
       */
      Map<ReportRenderer,Rectangle2D> renderer_to_bounds = new HashMap<ReportRenderer,Rectangle2D>();

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);

          // Synchronize around the list object to prevent mods while rendering
          synchronized (viewed_reports) {
            // Figure out the dimensions
            if (viewed_reports.size() > 0) {

              // Figure out the max width
              int global_max_w = 64;
              Iterator<ReportRenderer> it = viewed_reports.iterator(); while (it.hasNext()) {
                ReportRenderer rr = it.next(); rr.validate();
                int w = rr.maxLineWidth(g2d) + 10;
                if (w > global_max_w) global_max_w = w;
              }

              // Figure out the column width
              int columns = getRCWidth() / global_max_w;
              if ((getRCWidth() % global_max_w) > 3*global_max_w/4) columns++;
              if (columns == 0) columns++;
              global_max_w = getRCWidth()/columns;

              // Figure out the rows
              int rows = viewed_reports.size() / columns;
              if ((viewed_reports.size() % columns) > 0) rows++;
              if (rows == 0) rows++;

              // Figure out the height of each one
              int max_h = getRCHeight() / rows;
              if (max_h < 3*Utils.txtH(g2d, "0")+4) max_h = 3*Utils.txtH(g2d, "0")+4;

              int x = 0, y = 0;
              it = viewed_reports.iterator(); while (it.hasNext()) {
                Rectangle2D bounds = new Rectangle2D.Double(x * global_max_w, y * max_h, global_max_w, max_h);
                ReportRenderer rr = it.next();
                rr.render(g2d, bounds); bounds_to_renderer.put(bounds, rr); renderer_to_bounds.put(rr, bounds);
                x++; if (x == columns) { x = 0; y++; }
              }
            }
          }
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }
  }
}

