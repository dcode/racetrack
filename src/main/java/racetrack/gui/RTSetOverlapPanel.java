/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.Composite;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.image.BufferedImage;

import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import racetrack.analysis.HierarchicalClustering;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.StrCountSorterD;
import racetrack.util.Utils;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.ColorScale;
import racetrack.visualization.GreenYellowRedColorScale;
import racetrack.visualization.RTColorManager;

/**
 * Class that implements a set intersection for the count_by field...
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTSetOverlapPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -7263116325912933362L;

  /**
   * Use the chi square calculation for the cell
   */
  private JRadioButtonMenuItem chi_square_rbmi,

  /**
   * Use the absolute value for the cell
   */
                               absolute_rbmi,

  /**
   * Use the row percentage for the cell
   */
                               row_percentage_rbmi,

  /**
   * Use the column percentage for the cell
   */
                               column_percentage_rbmi;

  /**
   * Cell calculation
   */
  enum CellCalc { CHI_SQUARE, ABSOLUTE, ROW_PERCENTAGE, COLUMN_PERCENTAGE } ;

  /**
   * Radio button indicating that the first colorscale should be used (grayscale)
   */
  private JRadioButtonMenuItem colorscale_1_rbmi,
  /**
   * Red/Yellow/Green colorscale
   */
                               colorscale_5_rbmi,
  /**
   * BrewerScale sequential colorscale
   */
                               colorscale_6_rbmi;

  /**
   * Sort the rows alphabetically
   */
  private JRadioButtonMenuItem sort_rows_alpha_rbmi,

  /**
   * Sort the rows by the counts
   */
                               sort_rows_count_rbmi,
  /**
   * Cluster the rows by similarity
   */
                               sort_rows_cluster_rbmi;

  /**
   * Sort the cols alphabetically
   */
  private JRadioButtonMenuItem sort_cols_alpha_rbmi,

  /**
   * Sort the cols by the counts
   */
                               sort_cols_count_rbmi,
  /**
   * Cluster the cols by similarity
   */
                               sort_cols_cluster_rbmi;

  /**
   * Draw labels checkbox menu item
   */
  private JCheckBoxMenuItem    draw_labels_cbmi,

  /**
   * Remove any columns and rows that are equal checkbox menu item
   */
                               remove_equals_cbmi;

  /**
   * First field to use
   */
  public JComboBox<String> field_0_cb,

  /**
   * Second field to use
   */
                           field_1_cb;

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTSetOverlapPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   
    // Make the GUI
    add("Center",  component = new RTSetOverlapComponent());

    // Make the field choices
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(field_0_cb = new JComboBox<String>());
    panel.add(field_1_cb = new JComboBox<String>());
    add("South", panel);

    // Update the menu
    JMenuItem mi;
    getRTPopupMenu().add(mi = new JMenuItem("Copy Table To Clipboard")); 
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { copyTableToClipboard(); } } );
    getRTPopupMenu().addSeparator();

    // - Calculation
    ButtonGroup bg = new ButtonGroup();
    getRTPopupMenu().add(chi_square_rbmi        = new JRadioButtonMenuItem("Chi Square", true));  bg.add(chi_square_rbmi);         defaultListener(chi_square_rbmi);
    getRTPopupMenu().add(absolute_rbmi          = new JRadioButtonMenuItem("Absolute"));          bg.add(absolute_rbmi);           defaultListener(absolute_rbmi);
    getRTPopupMenu().add(row_percentage_rbmi    = new JRadioButtonMenuItem("Row Percentage"));    bg.add(row_percentage_rbmi);     defaultListener(row_percentage_rbmi);
    getRTPopupMenu().add(column_percentage_rbmi = new JRadioButtonMenuItem("Column Percentage")); bg.add(column_percentage_rbmi);  defaultListener(column_percentage_rbmi);
    getRTPopupMenu().addSeparator();

    // - Colorscale options
    bg = new ButtonGroup();
    getRTPopupMenu().add(colorscale_1_rbmi = new JRadioButtonMenuItem("Gray Scale", true)); bg.add(colorscale_1_rbmi); defaultListener(colorscale_1_rbmi);
    getRTPopupMenu().add(colorscale_5_rbmi = new JRadioButtonMenuItem("Green Yellow Red")); bg.add(colorscale_5_rbmi); defaultListener(colorscale_5_rbmi);
    getRTPopupMenu().add(colorscale_6_rbmi = new JRadioButtonMenuItem("Brewer Colors"));    bg.add(colorscale_6_rbmi); defaultListener(colorscale_6_rbmi);

    getRTPopupMenu().addSeparator();

    // - Sorting options
    bg = new ButtonGroup();
    getRTPopupMenu().add(sort_rows_alpha_rbmi   = new JRadioButtonMenuItem("Alpha Sort Rows", true)); bg.add(sort_rows_alpha_rbmi);   defaultListener(sort_rows_alpha_rbmi);
    getRTPopupMenu().add(sort_rows_count_rbmi   = new JRadioButtonMenuItem("Count Sort Rows"));       bg.add(sort_rows_count_rbmi);   defaultListener(sort_rows_count_rbmi);
    getRTPopupMenu().add(sort_rows_cluster_rbmi = new JRadioButtonMenuItem("Cluster Rows"));          bg.add(sort_rows_cluster_rbmi); defaultListener(sort_rows_cluster_rbmi);
    getRTPopupMenu().addSeparator();

    bg = new ButtonGroup();
    getRTPopupMenu().add(sort_cols_alpha_rbmi   = new JRadioButtonMenuItem("Alpha Sort Columns", true)); bg.add(sort_cols_alpha_rbmi);   defaultListener(sort_cols_alpha_rbmi);
    getRTPopupMenu().add(sort_cols_count_rbmi   = new JRadioButtonMenuItem("Count Sort Columns"));       bg.add(sort_cols_count_rbmi);   defaultListener(sort_cols_count_rbmi);
    getRTPopupMenu().add(sort_cols_cluster_rbmi = new JRadioButtonMenuItem("Cluster Columns"));          bg.add(sort_cols_cluster_rbmi); defaultListener(sort_cols_cluster_rbmi);
    getRTPopupMenu().addSeparator();

    // - Other rendering options
    getRTPopupMenu().add(draw_labels_cbmi   = new JCheckBoxMenuItem("Draw Labels", true));               defaultListener(draw_labels_cbmi);
    getRTPopupMenu().add(remove_equals_cbmi = new JCheckBoxMenuItem("Remove Equal Rows/Columns", true)); defaultListener(remove_equals_cbmi);

    defaultListener(field_0_cb);
    defaultListener(field_1_cb);

    updateBys();
  }

  /**
   * Copy the current table to the clipboard.
   */
  protected void copyTableToClipboard() {
    RTSetOverlapComponent.RenderContext myrc = (RTSetOverlapComponent.RenderContext) getRTComponent().rc; if (myrc != null) {
      StringBuffer sb = new StringBuffer();

      // Header first
      for (int i=0;i<myrc.cols_ls.size();i++) sb.append("," + Utils.makeRFC4180SafeString(Utils.decFmURL(myrc.cols_ls.get(i))));
      sb.append("\r\n");

      // Each row
      for (int r=0;r<myrc.rows_ls.size();r++) {
        String row = myrc.rows_ls.get(r);
        sb.append(Utils.makeRFC4180SafeString(Utils.decFmURL(row)));

        // Each column
        for (int c=0;c<myrc.cols_ls.size();c++) {
          String col  = myrc.cols_ls.get(c);
          String skey = row + BundlesDT.DELIM + col;
          if (myrc.abs_map.containsKey(skey)) sb.append("," + myrc.abs_map.get(skey));
          else                                  sb.append(",0");
        }
        sb.append("\r\n");
      }

      Utils.copyToClipboard(sb.toString());
    }
  }

  /**
   * Update the comboboxes with the fields.
   */
  public void updateBys() {
    Bundles bs = getRTParent().getRootBundles();
    String blanks[] = KeyMaker.blanks(bs.getGlobals(), false, true, false, true);
    updateBys(field_0_cb, blanks);
    updateBys(field_1_cb, blanks);
  }
  
  /**
   * Update a single combobox.
   */
  protected void updateBys(JComboBox<String> cb, String blanks[]) {
    String str = (String) cb.getSelectedItem();
    cb.removeAllItems(); 
    for (int i=0;i<blanks.length;i++) {
      if (blanks[i].equals(KeyMaker.TABLET_SEP_STR)   ||
          blanks[i].equals(KeyMaker.ALL_ENTITIES_STR) ||
          blanks[i].equals(KeyMaker.BY_STRAIGHT_STR)  ||
          blanks[i].equals(KeyMaker.BY_AUTOTIME_STR)  ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_STR)     ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_STR) ||
          blanks[i].equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_SEC_STR)) {
      } else cb.addItem(blanks[i]);
    }
    if (str != null) cb.setSelectedItem(str);
  }

  /**
   * Return the cell calculation per the gui setting.
   *
   *@return cell calculation
   */
  public CellCalc cellCalculation() {
    if      (chi_square_rbmi.       isSelected()) return CellCalc.CHI_SQUARE;
    else if (absolute_rbmi.         isSelected()) return CellCalc.ABSOLUTE;
    else if (row_percentage_rbmi.   isSelected()) return CellCalc.ROW_PERCENTAGE;
    else if (column_percentage_rbmi.isSelected()) return CellCalc.COLUMN_PERCENTAGE;
    else                                          return CellCalc.ABSOLUTE;
  }

  /**
   * Set the cell calculation.
   *
   *@param str cell calculation
   */
  public void cellCalculation(String str) {
    if      (str.equals("" + CellCalc.CHI_SQUARE))          chi_square_rbmi.       setSelected(true);
    else if (str.equals("" + CellCalc.ABSOLUTE))            absolute_rbmi.         setSelected(true);
    else if (str.equals("" + CellCalc.ROW_PERCENTAGE))      row_percentage_rbmi.   setSelected(true);
    else if (str.equals("" + CellCalc.COLUMN_PERCENTAGE))   column_percentage_rbmi.setSelected(true);
    else System.err.println("Do Not Understand Cell Calculation \"" + str + "\"");
  }

  /**
   * Different sorting options
   */
  enum SORT { ALPHA, COUNT, CLUSTER };

  /**
   * Return the sort for the rows
   */
  public SORT sortRows() {
    if      (sort_rows_count_rbmi.  isSelected()) return SORT.COUNT;
    else if (sort_rows_cluster_rbmi.isSelected()) return SORT.CLUSTER;
    else                                          return SORT.ALPHA;
  }

  /**
   * Set the rows sort
   */
  public void sortRows(String sort) {
    sort = sort.toLowerCase();
    if      (sort.equals(("" + SORT.COUNT).  toLowerCase())) sort_rows_count_rbmi.  setSelected(true);
    else if (sort.equals(("" + SORT.CLUSTER).toLowerCase())) sort_rows_cluster_rbmi.setSelected(true);
    else                                                     sort_rows_alpha_rbmi.  setSelected(true);
  }

  /**
   * Return the sort for the cols
   */
  public SORT sortCols() {
    if      (sort_cols_count_rbmi.  isSelected()) return SORT.COUNT;
    else if (sort_cols_cluster_rbmi.isSelected()) return SORT.CLUSTER;
    else                                          return SORT.ALPHA;
  }

  /**
   * Set the cols sort
   */
  public void sortCols(String sort) {
    sort = sort.toLowerCase();
    if      (sort.equals(("" + SORT.COUNT).  toLowerCase())) sort_cols_count_rbmi.  setSelected(true);
    else if (sort.equals(("" + SORT.CLUSTER).toLowerCase())) sort_cols_cluster_rbmi.setSelected(true);
    else                                                     sort_cols_alpha_rbmi.  setSelected(true);
  }

  /**
   * Return setting for drawing the labels.
   *
   *@return true to draw labels
   */
  public boolean drawLabels() { return draw_labels_cbmi.isSelected(); }

  /**
   * Set the option for drawing the labels.
   *
   *@param b true to draw the labels
   */
  public void drawLabels(boolean b) { draw_labels_cbmi.setSelected(b); }

  /**
   * Return the setting to remove equal rows/columns.
   *
   *@return true to remove equals
   */
  public boolean removeEquals() { return remove_equals_cbmi.isSelected(); }

  /**
   * Set the option for removing equal rows/columns
   *
   *@param b true to remove equals
   */
  public void removeEquals(boolean b) { remove_equals_cbmi.setSelected(b); }

  /**
   * Get the first field.
   *
   *@return first field
   */
  public String field0() { return (String) field_0_cb.getSelectedItem(); }

  /**
   * Set the first field.
   *
   *@param str new field value
   */
  public void field0(String str) { field_0_cb.setSelectedItem(str); }

  /**
   * Get the second field.
   *
   *@return second field
   */
  public String field1() { return (String) field_1_cb.getSelectedItem(); }

  /**
   * Set the second field.
   *
   *@param str new field value
   */
  public void field1(String str) { field_1_cb.setSelectedItem(str); }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "setoverlap"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTSetOverlapPanel"     + BundlesDT.DELIM +
           "field0="               + Utils.encToURL(field0()) +  BundlesDT.DELIM +
           "field1="               + Utils.encToURL(field1()) +  BundlesDT.DELIM +
           "draw_labels="          + drawLabels()             +  BundlesDT.DELIM +
           "sort_rows="            + sortRows()               +  BundlesDT.DELIM +
           "sort_cols="            + sortCols();
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTSetOverlapPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTSetOverlapPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";

      if      (type.equals("field0"))             field0(Utils.decFmURL(value));
      else if (type.equals("field1"))             field1(Utils.decFmURL(value));
      else if (type.equals("draw_labels"))        drawLabels(value.toLowerCase().equals("true"));
      else if (type.equals("sort_rows"))          sortRows(value);
      else if (type.equals("sort_cols"))          sortCols(value);
      else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Force the component to re-look at the root bundles if it changes.
   */
  @Override
  public void newBundlesRoot(Bundles new_root) { ((RTSetOverlapComponent) getRTComponent()).resetSorts(); }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTSetOverlapComponent extends RTComponent {
    private static final long serialVersionUID = 114252535395118266L;

    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      return shapes; }

    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    /**
     * Draw the closest point interaction - in this case, just specify the row and column that are drawn.
     */
    @Override
    public void addGarnish(Graphics2D g2d, int mx, int my) {
      RenderContext myrc = (RenderContext) rc; if (myrc != null) { boolean found = false;
        Iterator<Rectangle2D> it = myrc.geom_to_skey.keySet().iterator(); while (it.hasNext() && found == false) {
          Rectangle2D rect = (Rectangle2D) it.next();
          Rectangle2D rect_larger = new Rectangle2D.Double(rect.getX(), rect.getY(), rect.getWidth() + 1, rect.getHeight() + 1); // make it one bigger because of the border
          if (rect_larger.contains(mx,my)) {
            found = true; 

            String skey = myrc.geom_to_skey.get(rect);
            StringTokenizer st = new StringTokenizer(skey, BundlesDT.DELIM);
            String row_enc = st.nextToken(),
                   col_enc = st.nextToken();
            String row = Utils.decFmURL(row_enc),
                   col = Utils.decFmURL(col_enc);

            int x0    = (int) rect.getX(),
                y0    = (int) rect.getY(),
                x1    = (int) (rect.getX() + rect.getWidth()),
                y1    = (int) (rect.getY() + rect.getHeight());
            int w_max = myrc.table_x + field1_sort.size() * myrc.cell_w,
                h_max = myrc.table_y + field0_sort.size() * myrc.cell_h;

            Composite orig_comp = g2d.getComposite();
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
            g2d.setColor(RTColorManager.getColor("background", "default"));
            g2d.fillRect(0,  0,  x0 - 1,      y0    - 1);
            g2d.fillRect(0,  y1, x0 - 1,      h_max - y1);
            g2d.fillRect(x1, 0,  w_max - x1,  y0    - 1);
            g2d.fillRect(x1, y1, w_max - x1,  h_max - y1);
            g2d.setComposite(orig_comp);

            // Draw the actual information as text...
            double row_perc = 100.0 * myrc.abs_map.get(skey) / myrc.row_totals.get(row_enc);
            double col_perc = 100.0 * myrc.abs_map.get(skey) / myrc.col_totals.get(col_enc);

            String strs[] = new String[3];
            strs[0] = row + " x " + col + " ... " + myrc.map_to_sets.get(row_enc).size() + " x " + myrc.map_to_sets.get(col_enc).size();
            strs[1] = "Abs = " + Utils.humanReadableDouble(myrc.abs_map.get(skey)) + " | Value = " + Utils.humanReadableDouble(myrc.calc_map.get(skey));
            strs[2] = "Row % = " + Utils.humanReadableDouble(row_perc) + " | Col % = " + Utils.humanReadableDouble(col_perc);
            int str_max_w = Utils.txtW(g2d,strs[0]); for (int i=1;i<strs.length;i++) { int str_w = Utils.txtW(g2d,strs[i]); if (str_max_w < str_w) str_max_w = str_w; }

            // Always place below ... determine which side
            int str_x_base;
            if      (mx < (1*myrc.getRCWidth())/3) str_x_base = (int) (rect.getX());
            else if (mx < (2*myrc.getRCWidth())/3) str_x_base = (int) (rect.getCenterX() - str_max_w/2);
            else                                   str_x_base = (int) (rect.getMaxX() - str_max_w);

            int txt_h = Utils.txtH(g2d, strs[0]);
            Rectangle2D text_frame = new Rectangle2D.Double(str_x_base, rect.getMaxY() + 5, str_max_w + 10, strs.length * (txt_h + 2) + 4);

            g2d.setColor(RTColorManager.getColor("label", "defaultbg")); g2d.fill(text_frame);
            g2d.setColor(RTColorManager.getColor("label", "defaultfg")); g2d.draw(text_frame);
            g2d.setColor(RTColorManager.getColor("label", "default"));
            for (int i=0;i<strs.length;i++) g2d.drawString(strs[i], (int) (text_frame.getMinX() + 5), (int) (text_frame.getMinY() + (1+i)*(txt_h+2)));
          }
        }
      }
    }

    /**
     * Set value for field0 (rows)
     */
    String field0_set = null,

    /**
     * Set value for field1 (cols)
     */
           field1_set = null;

    /**
     * Sorted list of entities from field0
     */
    List<String> field0_sort = new ArrayList<String>(),

    /**
     * Sorted list of entities from field1
     */
                 field1_sort = new ArrayList<String>();

    /**
     * Validate the sorts.  If they are inconsistent, create them again.
     */
    protected synchronized void validateSorts(String f0, String f1) {
      if (f0.equals(field0_set) == false) { fill(field0_sort, getRTParent().getRootBundles(), f0); field0_set = f0; }
      if (f1.equals(field1_set) == false) { fill(field1_sort, getRTParent().getRootBundles(), f1); field1_set = f1; }
    }

    /**
     * Reset the sorts so that they are recalculated.
     */
    public void resetSorts() { field0_set = null; field1_set = null; }

    /**
     * Fill the list with all the unique values that make up a field.
     */
    protected void fill(List<String> list, Bundles root, String field) {
      boolean all_ints = true; // Determines how to sort values
      Set<String> set = new HashSet<String>();
      Iterator<Tablet> it_tab = root.tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next(); if (KeyMaker.tabletCompletesBlank(tablet, field)) {
          KeyMaker km = new KeyMaker(tablet, field);
          Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next(); String keys[] = km.stringKeys(bundle); if (keys != null) {
              for (int i=0;i<keys.length;i++) {
                if (all_ints && Utils.isInteger(keys[i]) == false) all_ints = false;
                set.add(Utils.encToURL(keys[i]));
              }
            }
          }
        }
      }
      list.clear(); list.addAll(set); 
      if (all_ints) Collections.sort(list, new IntegerStringComparator());
      else          Collections.sort(list);
    }

    /**
     * Sorter for strings that are actually just decimal integers.
     */
    class IntegerStringComparator implements Comparator<String> {
      public int compare(String s1, String s2) {
        int i1 = Integer.parseInt(s1), i2 = Integer.parseInt(s2);
        if      (i1 < i2) return -1;
        else if (i1 > i2) return  1;
        else              return  0;
      }
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy(),
                 field_0   = field0(),
                 field_1   = field1();

      // Decode the appropriate colorscale
      ColorScale cs = null; boolean equal_cs = false;
      if      (colorscale_1_rbmi.isSelected()) { cs = RTColorManager.getContinuousColorScale(); }
      else if (colorscale_5_rbmi.isSelected()) { cs = new GreenYellowRedColorScale(); }
      else if (colorscale_6_rbmi.isSelected()) { cs = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL,9); }

      // Create the render context based on the user's parameters
      RenderContext myrc = null;
      if (bs != null && field_0 != null && field_1 != null) { myrc = new RenderContext(id, bs, count_by, color_by, cs, equal_cs, field_0, field_1, 
                                                                                       cellCalculation(), drawLabels(), removeEquals(), sortRows(), sortCols(),  
                                                                                       getWidth(), getHeight()); }
      return myrc;
    }
    
    /**
     * RenderContext implementation for the correlation matrix
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by,

      /**
       * First field for correlation
       */
                            field0,
      /**
       * Second field for correlation
       */
                            field1;
      /**
       * ColorScale to use for this rendering
       */
      ColorScale            cs;

      /**
       * Equal colorscale -- monotonically increasing 
       */
      boolean               equal_cs = false;

      /**
       * Draw the labels
       */
      boolean               draw_labels = true;

      /**
       * Remove equal column and row names from the calculation
       */
      boolean               remove_equals = true;

      /**
       * Sort the rows by...
       */
      SORT                  sort_rows = SORT.ALPHA,

      /**
       * Sort the columns by...
       */
                            sort_cols = SORT.ALPHA;

      /**
       * Count context
       */
      BundlesCounterContext counter_context;

      /**
       * Lookup for a string key to the geometry
       */
      Map<String,Rectangle2D> skey_to_geom    = new HashMap<String,Rectangle2D>();

      /**
       * Lookup for a geometry to a string key
       */
      Map<Rectangle2D,String> geom_to_skey    = new HashMap<Rectangle2D,String>();

      /**
       * Summed value of all of the cells
       */
      double                  value_max = 1.0;

      /**
       * Calculation for the cell
       */
      CellCalc                cell_calc = CellCalc.CHI_SQUARE;

      // For both the rows and the columns, this maps to the items in their respective sets
      Map<String,Set<String>> map_to_sets  = new HashMap<String,Set<String>>();

      /**
       * Max map_to_sets size for rows
       */
      int                     map_to_sets_max_row = 1,

      /**
       * Max map_to_sets_size for cols
       */
                              map_to_sets_max_col = 1;

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param render_value       render value
       *@param cs                 colorscale to use for the rendering
       *@param equals_cs          use equal steps in a colorscale
       *@param field0             first field for correlation
       *@param field1             second field for correlation
       *@param sort_rows0         sort the rows
       *@param sort_cols0         sort the columns
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, ColorScale cs, 
                           boolean equal_cs, String field0, String field1, CellCalc cell_calc0,
                           boolean draw_labels, boolean remove_equals,
                           SORT sort_rows0, SORT sort_cols0,
                           int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;
        this.cs                 = cs;
        this.equal_cs           = equal_cs;
        this.field0             = field0;
        this.field1             = field1;
        this.cell_calc          = cell_calc0;
        this.draw_labels        = draw_labels;
        this.remove_equals      = remove_equals;

        this.sort_rows          = sort_rows0;
        this.sort_cols          = sort_cols0;

        // Early exit for a few show stoppers
        if (count_by.equals(BundlesDT.COUNT_BY_BUNS)) return;

        // Create the sorts -- use the root so that the visualization doesn't jump around
        validateSorts(field0, field1);

        // Sanity checking on the sizes
        if (field0_sort.size() > 1600 || field1_sort.size() > 1600) { System.err.println("RTSetOverlapPanel:  Too Many Rows or Columns To Render"); return; }

        Iterator<Tablet> it_tab = bs.tabletIterator();
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          boolean tablet_can_count = KeyMaker.tabletCompletesBlank(tablet, count_by),
                  tablet_field_0   = KeyMaker.tabletCompletesBlank(tablet, field0);
          KeyMaker km_count = null; if (tablet_can_count) km_count = new KeyMaker(tablet, count_by);

          if (tablet_can_count && tablet_field_0) {
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            KeyMaker         km0      = new KeyMaker(tablet, field0);
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle       = it_bun.next();
              String f0_strs[]    = km0.stringKeys(bundle); 
              String count_strs[] = km_count.stringKeys(bundle);

              if (f0_strs != null && f0_strs.length > 0 && count_strs != null && count_strs.length > 0) {
                for (int i=0;i<f0_strs.length;i++) { 
                  String fld = f0_strs[i]; String fld_enc = Utils.encToURL(fld); rows.add(fld_enc);
                  if (map_to_sets.containsKey(fld_enc) == false) map_to_sets.put(fld_enc, new HashSet<String>());
                  for (int j=0;j<count_strs.length;j++) {
                    String cnt = count_strs[j]; map_to_sets.get(fld_enc).add(cnt);
                  } } } } } 

          boolean tablet_field_1   = KeyMaker.tabletCompletesBlank(tablet, field1);
          if (tablet_can_count && tablet_field_1) {
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            KeyMaker         km1      = new KeyMaker(tablet, field1);
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle       = it_bun.next();
              String f1_strs[]    = km1.stringKeys(bundle); 
              String count_strs[] = km_count.stringKeys(bundle);

              if (f1_strs != null && f1_strs.length > 0 && count_strs != null && count_strs.length > 0) {
                for (int i=0;i<f1_strs.length;i++) { 
                  String fld = f1_strs[i]; String fld_enc = Utils.encToURL(fld); cols.add(fld_enc);
                  if (map_to_sets.containsKey(fld_enc) == false) map_to_sets.put(fld_enc, new HashSet<String>());
                  for (int j=0;j<count_strs.length;j++) {
                    String cnt = count_strs[j]; map_to_sets.get(fld_enc).add(cnt);
                  } } } } } 

          // if (tablet_field_0 == false && tablet_field_1 == false) addToNoMappingSet(tablet);
        }

        // Do the set intersections to calculate the value map
        Iterator<String> it_rows = rows.iterator(); while (it_rows.hasNext()) {
          String row_enc = it_rows.next();
          if (map_to_sets.containsKey(row_enc) == false) continue;

          if (map_to_sets_max_row < map_to_sets.get(row_enc).size()) map_to_sets_max_row = map_to_sets.get(row_enc).size();

          Iterator<String> it_cols = cols.iterator(); while (it_cols.hasNext()) {
            String col_enc = it_cols.next();
            if (map_to_sets.containsKey(col_enc) == false) continue;

            if (map_to_sets_max_col < map_to_sets.get(col_enc).size()) map_to_sets_max_col = map_to_sets.get(col_enc).size();

            if (remove_equals && row_enc.equals(col_enc)) continue; // the equals usually drown everything else out

            String skey     = row_enc + BundlesDT.DELIM + col_enc; skeys.add(skey);

            if (row_totals.containsKey(row_enc) == false) row_totals.put(row_enc, 0.0);
            if (col_totals.containsKey(col_enc) == false) col_totals.put(col_enc, 0.0);

            // See if we can save half of the processing by looking for the reverse skey
            String rev_skey = col_enc + BundlesDT.DELIM + row_enc; 
            if (abs_map.containsKey(rev_skey)) {
              abs_map.put(skey, abs_map.get(rev_skey));
              row_totals.put(row_enc, row_totals.get(row_enc) + abs_map.get(skey));
              col_totals.put(col_enc, col_totals.get(col_enc) + abs_map.get(skey));
              summed += abs_map.get(skey);
              continue;
            }

            // Do the intersection
            Set<String> set = new HashSet<String>(); set.addAll(map_to_sets.get(row_enc)); set.retainAll(map_to_sets.get(col_enc));
            if (set.size() > 0) {
              abs_map.put(skey, (double) set.size());
              row_totals.put(row_enc, row_totals.get(row_enc) + abs_map.get(skey));
              col_totals.put(col_enc, col_totals.get(col_enc) + abs_map.get(skey));
              summed += abs_map.get(skey);

              // Record the max values
              if (abs_map.get(skey) > value_max) value_max = abs_map.get(skey);
            }
          }
        }

        // Compute the cells render value -- taken from the chi-squared example
        Iterator<String> it = abs_map.keySet().iterator(); while (it.hasNext()) {
          String skey = it.next(); 
          String row_enc  = skey.substring(0,skey.indexOf(BundlesDT.DELIM)),
                 col_enc  = skey.substring(skey.indexOf(BundlesDT.DELIM)+1,skey.length());

          switch (cell_calc) {
            case CHI_SQUARE:         // double expected = map_to_sets.get(row_enc).size() * map_to_sets.get(col_enc).size() / summed;
                                     double expected = row_totals.get(row_enc) * col_totals.get(col_enc) / summed;
                                     double chisq    = (abs_map.get(skey) - expected) * (abs_map.get(skey) - expected) / expected;
                                     calc_map.put(skey, chisq);
                                     break;
            case ABSOLUTE:           calc_map.put(skey, abs_map.get(skey));
                                     break;
            case ROW_PERCENTAGE:     calc_map.put(skey, abs_map.get(skey) / row_totals.get(row_enc));
                                     break;
            case COLUMN_PERCENTAGE:  calc_map.put(skey, abs_map.get(skey) / col_totals.get(col_enc));
                                     break;
          }

          // Keep track of the mins and maxes
          if (calc_map.get(skey) > calc_max) calc_max = calc_map.get(skey);
          if (calc_map.get(skey) < calc_min) calc_min = calc_map.get(skey);

        }

        if (Double.isInfinite(calc_min)) { calc_min = 0.0; calc_max = 1.0; }
        if (calc_min == calc_max)       { calc_min =      calc_max - 1.0; }
        if (calc_min <  0.0)             { calc_min = 0.0; calc_max = 1.0; }
      }

      /**
       * All the keys created for this render
       */
      Set<String> skeys = new HashSet<String>(),

      /**
       * Row keys
       */
                  rows  = new HashSet<String>(),

      /**
       * Column keys
       */
                  cols  = new HashSet<String>();

      /**
       * Sum of all of the cells
       */
      double      summed = 0.0;

      /**
       * Actual values (set overlap sizes)
       */
      Map<String,Double> abs_map = new HashMap<String,Double>();

      /**
       * Value displayed calculation
       */
      Map<String,Double> calc_map = new HashMap<String,Double>(),

      /**
       * Total for the row
       */
                         row_totals = new HashMap<String,Double>(),

      /**
       * Total for the column
       */
                         col_totals = new HashMap<String,Double>();

      /**
       * Min value of the calc_map
       */
      double calc_min = Double.POSITIVE_INFINITY;
      
      /**
       * Max value of the calc_map
       */
      double calc_max = Double.NEGATIVE_INFINITY;
        
      /**
       * Text height
       */
      int txt_h,

      /**
       * Cell width
       */
          cell_w,

      /**
       * Cell height
       */
          cell_h,

      /**
       *  Maximum width for row labels
       */
          max0_w,

      /**
       * Maximum width for column labels (they are rotated 90 degrees...)
       */
          max1_w,

      /**
       * Upper left hand corner of the table
       */
          table_x,

      /**
       * Upper left hand corner of the table
       */
          table_y;

      /**
       * Sorted rows and column lists
       */
      List<String> rows_ls = new ArrayList<String>(),
                   cols_ls = new ArrayList<String>();

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);

          txt_h = Utils.txtH(g2d, "0"); cell_w = 2; cell_h = 2;
          max0_w = 0; 
          max1_w = 0; 

          // Figure out the sorting...
          // Sort the rows
          switch (sort_rows) {
            case COUNT:      List<StrCountSorterD> sorter = new ArrayList<StrCountSorterD>();
                               Iterator<String> it = rows.iterator(); while (it.hasNext()) {
                             String row = it.next(); sorter.add(new StrCountSorterD(row, map_to_sets.get(row).size()));
                             }
                             Collections.sort(sorter);
                             for (int i=0;i<sorter.size();i++) rows_ls.add(sorter.get(i).toString());
                             break;
            case CLUSTER:    rows_ls = clusterOrdering(rows, cols, calc_map, true);
                             break;
            case ALPHA:
            default:         rows_ls.addAll(rows); Collections.sort(rows_ls);
          }

          // Sort the cols
          switch (sort_cols) {
            case COUNT:      List<StrCountSorterD> sorter = new ArrayList<StrCountSorterD>();
                             Iterator<String> it = cols.iterator(); while (it.hasNext()) {
                               String col = it.next(); sorter.add(new StrCountSorterD(col, map_to_sets.get(col).size()));
                             }
                             Collections.sort(sorter);
                             for (int i=0;i<sorter.size();i++) cols_ls.add(sorter.get(i).toString());
                             break;
            case CLUSTER:    cols_ls = clusterOrdering(rows, cols, calc_map, false);
                             break;
            case ALPHA:
            default:         cols_ls.addAll(cols); Collections.sort(cols_ls);
          }

          // Figure out the labels size
          if (draw_labels) {
            for (int i=0;i<rows_ls.size();i++) { int txt_w = Utils.txtW(g2d, Utils.decFmURL(rows_ls.get(i)));      if (txt_w > max0_w) max0_w = txt_w; }
            for (int i=0;i<cols_ls.size();i++) { int txt_w = Utils.txtW(g2d, Utils.decFmURL(cols_ls.get(i))) + 10; if (txt_w > max1_w) max1_w = txt_w; }
            cell_w = cell_h = txt_h+2;
          } else {
            max0_w = max1_w = 4;
            cell_w = cell_h = 4;
          }

          table_x = max0_w + 4;
          table_y = max1_w + 4;

          // Draw the labels
          if (draw_labels) {
            g2d.setColor(RTColorManager.getColor("label", "default"));
            for (int i=0;i<rows_ls.size();i++) g2d.drawString(Utils.decFmURL(rows_ls.get(i)), 1, table_y + (i+1) * cell_h - 3);
            for (int i=0;i<cols_ls.size();i++) Utils.drawRotatedString(g2d, Utils.decFmURL(cols_ls.get(i)), table_x + (i+1) * cell_w - 1, table_y - 1);
          }

          // Draw the cell colors
          for (int y_i=0;y_i<rows_ls.size();y_i++) {
            int y = table_y + y_i * cell_h;
            for (int x_i=0;x_i<cols_ls.size();x_i++) {
              int    x      = table_x + x_i * cell_w;

              String skey = rows_ls.get(y_i) + BundlesDT.DELIM + cols_ls.get(x_i);
              if (calc_map.containsKey(skey)) {
                double      chisq = calc_map.get(skey);
                g2d.setColor(cs.at((float) ((chisq - calc_min)/(calc_max - calc_min))));

                // Create the shape and fill
                Rectangle2D rect = new Rectangle2D.Double(x, y, cell_w-1, cell_h-1);
                g2d.fill(rect);

                // Keep track of the correlation to records
                skey_to_geom.put(skey, rect);
                geom_to_skey.put(rect, skey);
              }
            }
          }

          // Render the color scale
          int cs_w = getRCWidth() - 10; 
          int cs_y = table_y + rows_ls.size() * cell_h + 3;
          int la_y = cs_y + txt_h;

          if (cs != null) {
            if (cs_w > 10) {
              for (int x=0;x<cs_w;x++) {
                float f = ((float) x)/((float) (cs_w - 1));
                g2d.setColor(cs.at(f));
                g2d.drawLine(x + 5, cs_y, x + 5, la_y);
              }
            }
          }

          // Draw the numerical scale
          g2d.setColor(RTColorManager.getColor("label", "default")); String s;
          s = Utils.humanReadableDouble(calc_min,3); g2d.drawString(s, 5,                            la_y + txt_h + 2);
          s = Utils.humanReadableDouble(calc_max,3); g2d.drawString(s, 5 + cs_w - Utils.txtW(g2d,s), la_y + txt_h + 2);

          // Add histogram bars on the side and at the bottom for the set size
          // - replicates some of the code above ... but separate in case it needs to be removed  (or made into an option)
          /*
          int histo_max_size = 40;
          g2d.setColor(RTColorManager.getColor("data", "default"));

          int x_histo_base = table_x + cols_ls.size() * cell_w  + cell_w;
          for (int y_i=0;y_i<rows_ls.size();y_i++) {
            int y = table_y + y_i * cell_h;
            int bar_w = (int) (histo_max_size * map_to_sets.get(rows_ls.get(y_i)).size() / map_to_sets_max_row);
            g2d.setColor(RTColorManager.getColor(Utils.decFmURL(rows_ls.get(y_i))));
            g2d.fillRect(x_histo_base, y, bar_w, cell_h-1);
          }

          int y_histo_base = table_y + rows_ls.size() * cell_h + txt_h*3;
          for (int x_i=0;x_i<cols_ls.size();x_i++) {
            int x = table_x + x_i * cell_w;
            int bar_h = (int) (histo_max_size * map_to_sets.get(cols_ls.get(x_i)).size() / map_to_sets_max_col);
            g2d.setColor(RTColorManager.getColor(Utils.decFmURL(cols_ls.get(x_i))));
            g2d.fillRect(x, y_histo_base, cell_w-1, bar_h);
          }
          */
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }

    /**
     * Use hierarchical clustering to order the rows or columns.
     *
     *@param rows       rows to order
     *@param cols       columns to order
     *@param val        value map to use to create the vectors
     *@param order_rows true to order the rows, false to order the columns
     *
     *@return ordered rows or columns
     */
    protected List<String> clusterOrdering(Set<String> rows, Set<String> cols, Map<String,Double> val, boolean order_rows) {
      // Scalability limites... need to add a warning to the user somewhere
      if (order_rows == true  && rows.size() > 400) { List<String> ls = new ArrayList<String>(); ls.addAll(rows); return ls; }
      if (order_rows == false && cols.size() > 400) { List<String> ls = new ArrayList<String>(); ls.addAll(cols); return ls; }

      // Make into lists so that the ordering is consistent
      List<String> rows_ls = new ArrayList<String>(), cols_ls = new ArrayList<String>(); rows_ls.addAll(rows); cols_ls.addAll(cols);

      //
      // Setup
      //
      Map<String,double[]> map = new HashMap<String,double[]>(); double vec[];
      if (order_rows) { for (int r=0;r<rows_ls.size();r++) {
                          vec = new double[cols_ls.size()]; double max = 0.0;
                          map.put(rows_ls.get(r),vec);
                          for (int c=0;c<vec.length;c++) {
                            String key = rows_ls.get(r) + BundlesDT.DELIM + cols_ls.get(c);
                            if (val.containsKey(key)) vec[c] = (double) val.get(key);
                            else                      vec[c] = 0.0;
                            if (vec[c] > max) max = vec[c];
                          }
                          if (max == 0.0) max = 1.0;
                          for (int c=0;c<vec.length;c++) vec[c] /= max;
                        }
      } else          { for (int c=0;c<cols_ls.size();c++) {
                          vec = new double[rows_ls.size()]; double max = 0.0;
                          map.put(cols_ls.get(c),vec);
                          for (int r=0;r<vec.length;r++) {
                            String key = rows_ls.get(r) + BundlesDT.DELIM + cols_ls.get(c);
                            if (val.containsKey(key)) vec[r] = val.get(key);
                            else                      vec[r] = 0.0;
                            if (vec[r] > max) max = vec[r];
                          }
                          if (max == 0.0) max = 1.0;
                          for (int r=0;r<vec.length;r++) vec[r] /= max;
                        }
      }

      //
      // Run the hierarchical clustering algorithm
      //
      HierarchicalClustering hc = new HierarchicalClustering(map, HierarchicalClustering.VectorDistance.euclidean);
      return hc.dendrogramOrder();
    }
  }
}

