/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.gui;

import java.awt.BasicStroke;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;

import java.awt.image.BufferedImage;

import java.awt.geom.Rectangle2D;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.visualization.ColorScale;
import racetrack.visualization.RTColorManager;

import racetrack.util.StrCountSorterD;
import racetrack.util.Utils;

/**
 * Visualization for showing deviations from all for specific bins.
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class RTDeviationsPanel extends RTPanel {
  private static final long serialVersionUID = -1910124342318124600L;

  /**
   * Categories or bins
   */
  JComboBox<String>    bin_cb,

  /**
   * Scalar value for statistics
   */
                       value_cb;

  /**
   * Mean mode
   */
  JRadioButtonMenuItem mean_rbmi,

  /**
   * Median mode
   */
                       median_rbmi;

  /**
   * Label checkbox
   */
  JCheckBoxMenuItem    label_cbmi;

  /**
   * Arrow checkbox
   */
  JCheckBoxMenuItem    arrow_cbmi;

  /**
   * Center the plot on the average/median of all samples
   */
  JCheckBoxMenuItem    center_plot_cbmi;

  /**
   * Mode for the visualization
   */
  enum MODE { MEAN, MEDIAN };

  /**
   * Construct a new panel with the specified GUI parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt      GUI parent
   */
  public RTDeviationsPanel (RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt) {
    super(win_type,win_pos,win_uniq,rt);   
    // Main component
    add("Center", component = new RTDeviationsComponent());

    // Fill out the popup menu
    getRTPopupMenu().add(label_cbmi       = new JCheckBoxMenuItem("Draw Labels", true));
    getRTPopupMenu().addSeparator();
    ButtonGroup bg = new ButtonGroup();
    getRTPopupMenu().add(mean_rbmi        = new JRadioButtonMenuItem("Mean (Mode)", true));  bg.add(mean_rbmi);
    getRTPopupMenu().add(median_rbmi      = new JRadioButtonMenuItem("Median (Mode)"));      bg.add(median_rbmi);
    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(arrow_cbmi       = new JCheckBoxMenuItem("Draw Arrows", true));
    getRTPopupMenu().add(center_plot_cbmi = new JCheckBoxMenuItem("Center Plot", true));

    defaultListener(label_cbmi);
    defaultListener(mean_rbmi);
    defaultListener(median_rbmi);
    defaultListener(arrow_cbmi);
    defaultListener(center_plot_cbmi);

    // Configuration panel
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("Bin"));   panel.add(bin_cb   = new JComboBox<String>());
    panel.add(new JLabel("Value")); panel.add(value_cb = new JComboBox<String>());

    // Add the default listeners
    defaultListener(bin_cb);
    defaultListener(value_cb);

    // Fill the comboboxes
    updateBys();
    add("South", panel);
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "deviations"; }

  /**
   * Get the bin for the x-axis
   *
   *@return bin for x-axis
   */
  public String       bin           ()           { return (String) bin_cb.getSelectedItem(); }

  /**
   * Set the bin for the x-axis
   *
   *@param str bin for x-axis
   */
  public void         bin           (String str) { bin_cb.setSelectedItem(str); }

  /**
   * Return the categories.
   *
   *@return categories header
   */
  public String       valueString()           { return (String) value_cb.getSelectedItem(); }

  /**
   * Set the categories.
   *
   *@param str categories header
   */
  public void         valueString(String str) { value_cb.setSelectedItem(str); }

  /**
   * Return the mode of the visualization.
   */
  public MODE         getMode() {
    if (mean_rbmi.isSelected()) return MODE.MEAN;
    else                        return MODE.MEDIAN;
  }

  /**
   * Set the mode of the visualization.
   */
  public void         setMode(String str) {
    if (str.equals("" + MODE.MEDIAN)) median_rbmi.setSelected(true);
    else                              mean_rbmi.setSelected(true);
  }

  /**
   * Draw the labels
   *
   *@return true to draw the labels
   */
  public boolean drawLabels() { return label_cbmi.isSelected(); }

  /**
   * Set the draw labels option.
   *
   *@param b true to draw labels
   */
  public void drawLabels(boolean b) { label_cbmi.setSelected(b); }

  /**
   * Draw the arrows
   *
   *@return true to draw arrows
   */
  public boolean drawArrows() { return arrow_cbmi.isSelected(); }

  /**
   * Set the draw arrows option.
   *
   *@param b true to draw arrows
   */
  public void drawArrows(boolean b) { arrow_cbmi.setSelected(b); }

  /**
   * Center the plot on the average/median of all the samples.
   *
   *@return true to center plot
   */
  public boolean centerPlot() { return center_plot_cbmi.isSelected(); }

  /**
   * Set the center the plot option
   *
   *@param b true to center the plot
   */
  public void centerPlot(boolean b) { center_plot_cbmi.setSelected(b); }

  /**
   * Get the configuration of this component as a string.  Supposed to be used for
   * bookmarking a view so that it can be re-rendered.
   *
   *@return string representing configuration of the panel
   */
  public String  getConfig()  { return "RTDeviationsPanel" + BundlesDT.DELIM +
                                       "bin="              + Utils.encToURL(bin())          + BundlesDT.DELIM +
                                       "value="            + Utils.encToURL(valueString())  + BundlesDT.DELIM + 
                                       "mode="             + Utils.encToURL("" + getMode()) + BundlesDT.DELIM +
                                       "labels="           + drawLabels()                   + BundlesDT.DELIM +
                                       "centerplot="       + centerPlot()                   + BundlesDT.DELIM + 
                                       "arrows="           + drawArrows(); }

  /**
   * Set the configuration of this component from the string representation.
   *
   *@return previously returned string from the getConfig() method
   */
  public void    setConfig(String str) {
    StringTokenizer st = new StringTokenizer(str,BundlesDT.DELIM);
    if (st.nextToken().equals("RTDeviationsPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTDeviationsPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      if      (type.equals("bin"))        bin(Utils.decFmURL(value));
      else if (type.equals("value"))      valueString(Utils.decFmURL(value));
      else if (type.equals("mode"))       setMode(Utils.decFmURL(value));
      else if (type.equals("labels"))     drawLabels(value.toLowerCase().equals("true"));
      else if (type.equals("arrows"))     drawArrows(value.toLowerCase().equals("true"));
      else if (type.equals("centerplot")) centerPlot(value.toLowerCase().equals("true"));
      else throw new RuntimeException("Do Not Understand Type-Value Pair \"" + type + "\"=\"" + value + "\"");
    }
  }

  /**
   * Update the comboboxes for selecting global fields when new data is
   * loaded.
   */
  public void         updateBys() { 
    if (bin_cb   != null) updateEntityBys(bin_cb); 
    if (value_cb != null) updateScalarBys(value_cb); 
  }

  /**
   * Generic method to update a combobox.
   *
   *@param cb combobox to update
   */
  public void         updateEntityBys(JComboBox<String> cb) {
    String strs[]; Object sel = cb.getSelectedItem();
    cb.removeAllItems();
    strs = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, true, true, true);
    for (int i=0;i<strs.length;i++) cb.addItem(strs[i]);
    if (sel == null) cb.setSelectedIndex(0); else cb.setSelectedItem(sel);
  }

  /**
   * Generic method to update a combobox with just scalar fields.
   *
   *@param cb combobox to update
   */
  public void         updateScalarBys(JComboBox<String> cb) {
    String strs[]; Object sel = cb.getSelectedItem();
    cb.removeAllItems();
    strs = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, false, true, false);
    // cb.addItem(BundlesDT.COUNT_BY_DEFAULT);
    for (int i=0;i<strs.length;i++) cb.addItem(strs[i]);
    if (sel == null) cb.setSelectedIndex(0); else cb.setSelectedItem(sel);
  }

  /**
   * Component that handles painting and interacting with the visualization.
   */
  public class RTDeviationsComponent extends RTComponent {
    private static final long serialVersionUID = -2080294342318124608L;

    /**
     * Add a dynamic overlay to show the actual stat under the mouse.
     *
     *@param g2d graphics primitive
     *@param mx  mouse x
     *@param my  mouse y
     */
    @Override
    public void addGarnish(Graphics2D g2d, int mx, int my) {
      RenderContext myrc = (RenderContext) rc; if (myrc != null) {
        Iterator<Shape> it = myrc.geom_to_key.keySet().iterator(); while (it.hasNext()) {
          Rectangle2D rect = (Rectangle2D) it.next();
          if (rect.contains(mx,my)) {
            String key = myrc.geom_to_key.get(rect);
            if (myrc.stats_map.containsKey(key)) {
              Stats stats = myrc.stats_map.get(key);
              String str = "" + stats.stat();
              clearStr(g2d, str, mx - Utils.txtW(g2d,str)/2, my - 2, RTColorManager.getColor("annotate","labelfg"), RTColorManager.getColor("annotate","labelbg"));
            }
          }
        }
      }
    }

    /**
     * Copy a screenshot of the rendering to the clipboard.
     *
     *@param shft shift key down
     *@param alt  alt key down
     */
    @Override
    public void copyToClipboard    (boolean shft, boolean alt) {
      RenderContext myrc = (RenderContext) getRTComponent().rc;
      if (shft == true && myrc != null)  Utils.copyToClipboard(myrc.getBase());
    }

    /**
     * Return all of the shapes in the current rendering.
     *
     *@return set of rendered shapes
     */
    public Set<Shape>      allShapes()                     { 
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Shape>();
      return myrc.geom_to_key.keySet();
    }

    /**
     * Return the rendered shapes that correspond to the specified bundles.
     *
     *@param  bundles bundles/records to match for shapes
     *
     *@return set of shapes that correspond to the specified bundles
     */
    public Set<Shape>  shapes(Set<Bundle> bundles) { 
      Set<Shape> shapes = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Bundle> it = bundles.iterator();
      while (it.hasNext()) {
        Bundle bundle = it.next();
        if (myrc.bundle_to_keys.containsKey(bundle)) {
          Iterator<String> it_key = myrc.bundle_to_keys.get(bundle).iterator();
          while (it_key.hasNext()) {
            String key = it_key.next();
            shapes.add(myrc.key_to_geom.get(key));
          }
        }
      }
      return shapes;
    }

    /**
     * Return the bundles records associated with the specified shape.  Note that the shape
     * cannot be generic and must have been returned by this component.
     *
     *@param  shape shape to lookup for records
     *
     *@return set of bundles that correspond to the shape
     */
    public Set<Bundle> shapeBundles(Shape shape)       {
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Bundle>();
      Set<Bundle> set = new HashSet<Bundle>();
      String key = myrc.geom_to_key.get(shape);
      if (key != null) {
        Set<Bundle> set_to_add = myrc.key_to_bundles.get(key);
        if (set_to_add != null) set.addAll(set_to_add);
        else System.err.println("RTDeviationsPanel.shapeBundles() - null pointer for key \"" + key + "\"");
      }
      else System.err.println("Key For \"" + shape + "\" Is Null");
      return set;
    }

    /**
     * Find the rendered shapes that overlap with the specified shape.  Note that the specified
     * shape can be generic.
     *
     *@param  shape general shape to match against rendered shapes
     *
     *@return rendered shapes that overlap with the specified shape
     */
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Shape>();
      Set<Shape> shapes = new HashSet<Shape>();
      Iterator<Shape> it = myrc.geom_to_key.keySet().iterator();
      while (it.hasNext()) {
        Shape rendered_shape = it.next();
        if (Utils.genericIntersects(shape, rendered_shape)) shapes.add(rendered_shape);
      }
      return shapes;
    }

    /**
     * Return the rendered shapes that contain the specified x and y coordinate.
     *
     *@param  x x-coordinate
     *@param  y y-coordinate
     *
     *@return set of shapes that contain the x/y coordinate
     */
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> shapes = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Shape> it = myrc.geom_to_key.keySet().iterator();
      while (it.hasNext()) {
        Shape rendered_shape = it.next();
        if (rendered_shape.contains(x,y)) shapes.add(rendered_shape);
      }
      return shapes;
    }

    /**
     * Return the shape used to match shapes directly under the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape under mouse
     */
    public Shape getZeroOrderShape(int x, int y) { return new Rectangle2D.Double(x,y,1,1); }

    /**
     * Return the shape used to match shapes directly near the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape near mouse
     *
     */
    public Shape getFirstOrderShape(int x, int y) { return new Rectangle2D.Double(x,y,1,1); }

    /**
     * Return the shape used to match shapes directly further from the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape further from mouse mouse
     */
    public Shape getSecondOrderShape(int x, int y) { return new Rectangle2D.Double(x,y,1,1); }

    /**
     * Create a render context with the specified render ID.  The render context will be used to 
     * create the actual visualization.  The render id ensures that unused/unneeded visualization
     * renderings will be canceled.
     *
     *@param  id render id for aborting unnecessary renders
     *
     *@return    render context based on visible dataset and GUI parameters
     */
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      Bundles bs               = getRenderBundles();
      String  count_by         = getRTParent().getCountBy(),
              color_by         = getRTParent().getColorBy();

      String  bin_hdr          = bin(),
              value_str        = valueString();

      MODE    mode             = getMode();

      boolean draw_labels      = drawLabels(),
              draw_arrows      = drawArrows(),
              center_plot      = centerPlot();

      if (bs != null && count_by != null && bin_hdr != null && count_by != null && value_str != null) {
        RenderContext myrc = new RenderContext(id, bs, count_by, color_by, bin_hdr, value_str, mode, draw_labels, draw_arrows, center_plot, getWidth(), getHeight());
        return myrc;
      } else return null;
    }

    /**
     * Class to perform the actual rendering of the view.  This class uses several additional inner classes
     * to map the axes and to perform statistical operations on the data.
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Data set to render
       */
      Bundles bs; 
      /**
       * Width (in pixels) of the rendering
       */
      int     w, 
      /**
       * Height (in pixels) of the rendering
       */
              h; 
      /**
       * Field used to count the bars by (or perform stats on)
       */
      String  count_by, 
      /**
       * Field used to color the entities
       */
              color_by, 
      /**
       * Entity to use for the scatter plot
       */
              bin_hdr, 
      /**
       * Value string
       */
              value_str;

      /**
       * Mode of the visualization
       */
      MODE    mean_or_median;

      /**
       * Draw the labels
       */
      boolean draw_labels,

      /**
       * Draw the arrows
       */
              draw_arrows,
      /**
       * Center the plot on the average/median of all the samples
       */
              center_plot;

      /**
       * X inset for rendering
       */
      int                              x_ins = 4, 

      /**
       * Y inset for rendering
       */
                                       y_ins, 
      /**
       * XY Graph width in pixels
       */
                                       graph_w, 
      /**
       * XY Graph height in pixels
       */
                                       graph_h;

      /**
       * map from the geometry to the associated key
       */
      Map<Shape,String>  geom_to_key = new HashMap<Shape,String>();

      /**
       * Map from the panel key to the asssociated geometry
       */
      Map<String,Shape>  key_to_geom = new HashMap<String,Shape>();

      /**
       * Bundle to keys mapping
       */
      Map<Bundle,Set<String>> bundle_to_keys = new HashMap<Bundle,Set<String>>();

      /**
       * Key to bundles mapping
       */
      Map<String,Set<Bundle>> key_to_bundles = new HashMap<String,Set<Bundle>>();

      /**
       * Construct the render context with the specified dataset and GUI configurations.  Use the render ID to 
       * ensure that out-of-date renderings are canceled as soon as possible.
       *
       *@param id                render id to abort unnecessary renderings
       *@param bs                dataset to render
       *@param count_by          Field used to count the entities by (for width of icon in xy grid)
       *@param color_by          Field used to color the entities
       *@param bin_hdr           Bins
       *@param value_str         Value string
       *@param mean_or_median    Mode to use for the visualization
       *@param draw_labels       draw labels
       *@param draw_arrows       draw arrows
       *@param center_plot       center the plot
       *@param w                 Width (in pixels) of the rendering
       *@param h                 Height (in pixels) of the rendering
       */
      public               RenderContext(short   id, 
                                         Bundles bs, 
                                         String  count_by, 
                                         String  color_by, 
                                         String  bin_hdr,
                                         String  value_str, 
                                         MODE    mean_or_median,
                                         boolean draw_labels,
                                         boolean draw_arrows,
                                         boolean center_plot,
                                         int     w, 
                                         int     h) {
        render_id = id; this.bs = bs; this.w = w; this.h = h; this.count_by = count_by; this.color_by = color_by;
        this.bin_hdr = bin_hdr; this.value_str = value_str; this.mean_or_median = mean_or_median;
        this.draw_labels = draw_labels; this.draw_arrows = draw_arrows; this.center_plot = center_plot;

        // BundlesG globals = getRTParent().getRootBundles().getGlobals();

        global_stats = createStats("__global__", mean_or_median);

        // Go through the tablets
        Iterator<Tablet> it_t = bs.tabletIterator();
        while (it_t.hasNext() && currentRenderID() == getRenderID()) {
          Tablet tablet = it_t.next();

          // Check to see what the bundle can provide
          boolean tablet_can_count = KeyMaker.tabletCompletesBlank(tablet, value_str);
          KeyMaker bin_km = null;
          if      (KeyMaker.tabletCompletesBlank(tablet, bin_hdr)) bin_km  = new KeyMaker(tablet, bin_hdr);

          // If it provides something, go through the bundles
          if (bin_km != null && tablet_can_count) {

            KeyMaker value_km = new KeyMaker(tablet, value_str);

            // Go through the bundles
            Iterator<Bundle> it_b = tablet.bundleIterator();
            while (it_b.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle = it_b.next(); bundle_to_keys.put(bundle, new HashSet<String>());
              String strs[] = bin_km.stringKeys(bundle);

              // Bins
              for (int i=0;i<strs.length;i++) {
                  String key = strs[i];

                  // Store of the bundle to keys information
                  bundle_to_keys.get(bundle).add(key);

                  // Track bundles
                  if (key_to_bundles.containsKey(key) == false) key_to_bundles.put(key, new HashSet<Bundle>());
                  key_to_bundles.get(key).add(bundle);

                  int ints[] = value_km.intKeys(bundle);
                  for (int j=0;j<ints.length;j++) {
                    if (stats_map.containsKey(key) == false) stats_map.put(key, createStats(key, mean_or_median));
                    stats_map.get(key).addValue(ints[j]);
                    global_stats.addValue(ints[j]);
                  }
              }
            }
          } else {
            Iterator<Bundle> it_b = tablet.bundleIterator();
            while (it_b.hasNext()) addToNoMappingSet(it_b.next());
          }
        }

        // Calculate all the stats
        global_stats.calc();
        Iterator<String> it = stats_map.keySet().iterator(); while (it.hasNext()) stats_map.get(it.next()).calc();

        // Now sort them
        sorter.add(new StrCountSorterD(global_stats.toString(), 0.0));
        Iterator<String> it_key = stats_map.keySet().iterator(); while (it_key.hasNext()) {
          String key = it_key.next(); sorter.add(new StrCountSorterD(key, stats_map.get(key).stat() - global_stats.stat()));
        }
        Collections.sort(sorter);

        // Find the absolute greatest difference
        double a = Math.abs(sorter.get(0).count()),
               b = Math.abs(sorter.get(sorter.size()-1).count());
        if (a > b) sorter_abs_max = a;
        else       sorter_abs_max = b;
        
        // Find the max and mins
        sorter_min = sorter.get(sorter.size()-1).count();
        sorter_max = sorter.get(0              ).count();
      }

      /**
       * Sorted values
       */
      List<StrCountSorterD> sorter = new ArrayList<StrCountSorterD>();

      /**
       * Maximum difference between global stat and the minimum/maximum one from the sorter ends.
       */
      double sorter_abs_max =   10.0,

      /**
       * Minimum stat
       */
             sorter_min     = -100.0,

      /**
       * Maximum stat
       */
             sorter_max     =  100.0;

      /**
       * Create the right kind of stats.
       *
       *@param key   key for the stats
       *@param mode  type of stats
       *
       *@return Stats structure
       */
      Stats createStats(String key, MODE mode) {
        if (mode == MODE.MEDIAN) return new StatsMedian(key);
        else                     return new StatsMean(key);
      }

      /**
       * Statistic map per key
       */
      Map<String,Stats> stats_map = new HashMap<String,Stats>();

      /**
       * Global stats to compare against
       */
      Stats global_stats;

      /**
       * Return the width of the rendering in pixels.
       *
       *@return width in pixels
       */
      public int           getRCWidth()  { return w; }

      /**
       * Return the height of the rendering in pixels
       *
       *@return height in pixels
       */
      public int           getRCHeight() { return h; }

      /**
       * Copy of the rendered image
       */
      BufferedImage base_bi = null;

      /**
       * Render the previously calculated values to the actual image buffer.  Save a copy in case
       * it is requested again.
       *
       *@return rendered image
       */
      public BufferedImage getBase()     { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);
          int txt_h = Utils.txtH(g2d, "0") + 2;
         
          if (sorter.size() > 2) { 
            // Draw the rows... see if they can all fit or not...
            if (sorter.size() * txt_h < (h - txt_h)) {
              for (int i=0;i<sorter.size();i++) {
                drawRow(sorter.get(i), g2d, txt_h + txt_h*i, txt_h);
              }

            // If they can't all fit... then we'll have to do the ends and then the middles
            } else if (h >= 10*txt_h) {
              // Calculate the middle rows count
              int middle_rows_count = (h - txt_h*7)/txt_h;
              int middle_i = sorter.size()/2; for (int i=0;i<sorter.size();i++) if (sorter.get(i).toString().equals("__global__")) middle_i = i;
              int sorter_i = middle_i - middle_rows_count/2;
              int rr_i     = 0; // render row index
              String skip = "";

              // Make sure the middle doesn't overlap with the ends
              if (sorter_i <= 1)                                     sorter_i = 2;
              if ((sorter_i + middle_rows_count) >= sorter.size()-2) sorter_i = sorter.size() - 2 - middle_rows_count;

              // Start with the top rows...
                      drawRow(sorter.get(0), g2d, txt_h + txt_h*rr_i, txt_h);
              rr_i++; drawRow(sorter.get(1), g2d, txt_h + txt_h*rr_i, txt_h);
              rr_i++; // skip a row...

              // Skipping label
              g2d.setColor(RTColorManager.getColor("label", "minor"));
              skip = "Skipping " + (sorter_i - 2) + "...";
              int skip_x = w/2 - Utils.txtW(g2d, skip)/2,
                  skip_y = txt_h + txt_h*rr_i - 2;
              g2d.drawString(skip, skip_x, skip_y);
              Rectangle2D geom_skip = new Rectangle2D.Double(skip_x, skip_y - txt_h, Utils.txtW(g2d, skip), txt_h);
              Set<Bundle> skip_set = new HashSet<Bundle>();
              for (int i=2;i<sorter_i;i++) skip_set.addAll(key_to_bundles.get(sorter.get(i).toString()));
              key_to_geom.put("__skip_bottom__", geom_skip);
              geom_to_key.put(geom_skip, "__skip_bottom__");
              key_to_bundles.put("__skip_bottom__", skip_set);
              Iterator<Bundle> it = skip_set.iterator(); while (it.hasNext()) bundle_to_keys.get(it.next()).add("__skip_bottom__");

              rr_i++;

              // Do the middles
              for (int i=0;i<middle_rows_count;i++,rr_i++,sorter_i++) drawRow(sorter.get(sorter_i), g2d, txt_h + txt_h*rr_i, txt_h);
              
              // Skipping label
              g2d.setColor(RTColorManager.getColor("label", "minor"));
              skip = "Skipping " + (sorter.size() - 2 - sorter_i) + "...";
              skip_x = w/2 - Utils.txtW(g2d, skip)/2;
              skip_y = txt_h + txt_h*rr_i - 2;
              g2d.drawString(skip, w/2 - Utils.txtW(g2d, skip)/2, txt_h + txt_h*rr_i - 2);
              geom_skip = new Rectangle2D.Double(skip_x, skip_y - txt_h, Utils.txtW(g2d, skip), txt_h);
              skip_set = new HashSet<Bundle>();
              for (int i=sorter_i;i<sorter.size()-2;i++) skip_set.addAll(key_to_bundles.get(sorter.get(i).toString()));
              key_to_geom.put("__skip_top__", geom_skip);
              geom_to_key.put(geom_skip, "__skip_top__");
              key_to_bundles.put("__skip_top__", skip_set);
              it = skip_set.iterator(); while (it.hasNext()) bundle_to_keys.get(it.next()).add("__skip_top__");

              // rr_i++; // skip a row...
              rr_i++; drawRow(sorter.get(sorter.size()-2), g2d, txt_h + txt_h*rr_i, txt_h);
              rr_i++; drawRow(sorter.get(sorter.size()-1), g2d, txt_h + txt_h*rr_i, txt_h);
            } 

            // Draw a histogram at the bottom
            ColorScale cs = RTColorManager.getContinuousColorScale();
            int accum[] = new int[w]; int max = 0; for (int i=0;i<sorter.size();i++) {
              int x = calcX(sorter.get(i).count()); accum[x]++; if (max < accum[x]) max = accum[x];
            }
            for (int i=0;i<accum.length;i++) {
              if (accum[i] > 0) {
                float f = ((float) accum[i]) / max;
                g2d.setColor(cs.at(f));
                g2d.fillRect(i, h - txt_h - 1, 1, 3);
              }
            }

            // Draw scale at the bottom
            StrCountSorterD sd; int x_off; Stats stats;

            g2d.setColor(RTColorManager.getColor("data", "min"));
            sd = sorter.get(sorter.size()-1); x_off = calcX(sd.count()); stats = stats_map.get(sd.toString());
            if (stats != null) {
              g2d.drawString(""+stats.stat(), x_off + 2, h - 2);
              g2d.drawLine(x_off, h - 1, x_off, h - txt_h + 2);
            }

            g2d.setColor(RTColorManager.getColor("data", "max"));
            sd = sorter.get(0);               x_off = calcX(sd.count()); stats = stats_map.get(sd.toString());
            if (stats != null) {
              g2d.drawString(""+stats.stat(), x_off - 2 - Utils.txtW(g2d,""+stats.stat()), h - 2);
              g2d.drawLine(x_off, h - 1, x_off, h - txt_h + 2);
            }
          }
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }

      /**
       *
       */
      public int calcX(double stat) {
        if (center_plot) return (int) (w/2 + ((w - x_ins)/2)*(stat/sorter_abs_max));
        else             return (int) (x_ins + (w - 2*x_ins) * (stat - sorter_min) / (sorter_max - sorter_min));
      }

      /**
       * Arrow width
       */
      final int ARROW_W = 4;

      /**
       *
       */
      protected void drawRow(StrCountSorterD sd, Graphics2D g2d, int y_off, int txt_h) {
        int x_off = calcX(sd.count());
        int x_mid = calcX(0.0);

        if      (sd.count() > 0.0) g2d.setColor(RTColorManager.getColor("data", "max"));
        else if (sd.count() < 0.0) g2d.setColor(RTColorManager.getColor("data", "min"));
        else                       g2d.setColor(RTColorManager.getColor("data", "mean"));

        if (draw_arrows) {
          float dashes[] = new float[2]; dashes[0] = 1.0f; dashes[1] = 2.0f;

          Stroke orig_stroke = g2d.getStroke();
          g2d.setStroke(new BasicStroke(1.0f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));

          g2d.drawLine(x_mid, y_off - txt_h/2, x_off, y_off - txt_h/2);
          if        (sd.count() > 0.0) { g2d.drawLine(x_off,y_off-txt_h/2,x_off - ARROW_W,y_off-txt_h/2 - ARROW_W);
                                         g2d.drawLine(x_off,y_off-txt_h/2,x_off - ARROW_W,y_off-txt_h/2 + ARROW_W);
          } else if (sd.count() < 0.0) { g2d.drawLine(x_off,y_off-txt_h/2,x_off + ARROW_W,y_off-txt_h/2 - ARROW_W);
                                         g2d.drawLine(x_off,y_off-txt_h/2,x_off + ARROW_W,y_off-txt_h/2 + ARROW_W); }
          g2d.setStroke(orig_stroke);
        }

        // Draw the labels
        String str   = sd.toString(); 
        String key   = str; // because the str may be modified for what it writes to the display
        int    txt_w = Utils.txtW(g2d, str);

        if (draw_labels) {
          int    x_str = x_mid - txt_w/2;

          if        (sd.count() > 0.0) {
            if (draw_arrows) x_str = x_mid - txt_w - 6; else x_str = x_off - txt_w - 2;
          } else if (sd.count() < 0.0) {
            if (draw_arrows) x_str = x_mid + 6;         else x_str = x_off + 2;
          }

          if (key.equals("__global__")) { 
            str = "" + global_stats.stat();
            x_str = x_mid + 4; 
          }

          g2d.drawString(str, x_str, y_off - 2);

          if (key.equals("__global__")) {
            if (mean_or_median == MODE.MEDIAN) {
              str = "Median";
              g2d.setColor(RTColorManager.getColor("data","median")); 
            } else {
              str = "Mean";
              g2d.setColor(RTColorManager.getColor("data", "mean"));
            }
            g2d.drawString(str, x_mid - 4 - Utils.txtW(g2d, str), y_off - 2);
          }
        }

        g2d.drawLine(x_off, y_off - 1, x_off, y_off - txt_h + 1);

        // Update the mappings for geometry to keys and vice versa
        if (key.equals("__global__") == false) {
          Rectangle2D.Double geom; 
          if      (sd.count() > 0.0) geom = new Rectangle2D.Double(x_mid - txt_w - 6,  y_off - txt_h, w - x_mid + txt_w + 6, txt_h);
          else if (sd.count() < 0.0) geom = new Rectangle2D.Double(0,                  y_off - txt_h, x_mid + txt_w + 6,     txt_h);
          else                       geom = new Rectangle2D.Double(x_mid - w/4,        y_off - txt_h, w/4,                   txt_h);
          geom_to_key.put(geom, str);
          key_to_geom.put(str, geom);
        }
      }
    }
  }
}

/**
 * Stats interface - abstracts the two types of stats from the application.
 */
interface Stats { public void addValue(int i); public void calc(); public double stat(); }

/**
 * StatsMean - statistics for the mean value.
 */
class StatsMean implements Stats {
  String key; int samples = 0; double sum = 0.0; double mean = 0.0;
  public StatsMean(String key) { this.key = key; }
  public void   addValue(int i) { samples++; sum += i; }
  public void   calc()          { if (samples > 0) mean = sum/samples; else mean = 0.0; }
  public double stat()          { return mean; }
  public String toString()      { return key; }
}

/**
 * StatsMedian - statistics for the median value.
 */
class StatsMedian implements Stats {
  String key; List<Integer> values = new ArrayList<Integer>(); int median = 0;
  public StatsMedian(String key) { this.key = key; }
  public void   addValue(int i) { values.add(i); }
  public void   calc()          { Collections.sort(values); if (values.size() > 0) median = values.get(values.size()/2); }
  public double stat()          { return median; }
  public String toString()      { return key; }
}

