/* 

Copyright 2021 D. Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.IOException;

import java.text.SimpleDateFormat;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import java.util.regex.Pattern;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingConstants;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import racetrack.analysis.HDBSCAN;
import racetrack.analysis.HierarchicalClustering;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesG;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.PostProc;
import racetrack.framework.Tablet;

import racetrack.util.CaseInsensitiveComparator;
import racetrack.util.ColorCountSorter;
import racetrack.util.Entity;
import racetrack.util.EntityExtractor;
import racetrack.util.LevenshteinStateful;
import racetrack.util.QuickStats;
import racetrack.util.QuickStatsDouble;
import racetrack.util.Selection;
import racetrack.util.StrCountSorter;
import racetrack.util.StrCountSorterD;
import racetrack.util.SubText;
import racetrack.util.Utils;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.RTColorManager;
import racetrack.visualization.SmallMultiples;
import racetrack.visualization.TreeMap;
import racetrack.visualization.XYRenderer;

import tagbio.umap.Umap;

/**
 * Manipulation at the record level.  Records can be grouped together by field or manually
 * for analysis.  Additionally, a timeline visualization mode is supported for groups of
 * records.
 *
 *@version 1.0
 */
public class RTEntitiesPanel extends RTPanel {
  /**
   * Something required
   */
  private static final long serialVersionUID = -1260127185012948467L;

  /**
   * Enumeration of mouse events
   */
  enum ME_ENUM        { PRESSED, RELEASED, CLICKED, MOVED, DRAGGED, WHEEL };

  /**
   * Enumeration of different UI interaction events
   */
  enum UI_INTERACTION { NONE, PANNING, SELECTING, MOVING, GRID_LAYOUT, LINE_LAYOUT, CIRCLE_LAYOUT };

  /**
   * Enumeration of the modes for the UI
   */
  enum UI_MODE        { EDIT, FILTER };

  /**
   *  State variable for the ui mode
   */
  protected UI_MODE ui_mode = UI_MODE.EDIT;

  /**
   * Return the current mode of the interactive component.
   */
  public UI_MODE mode() { return ui_mode; }

  /**
   * Toggle the current mode to the next option
   */
  public void toggleMode() { 
    if      (ui_mode == UI_MODE.FILTER) ui_mode = UI_MODE.EDIT;
    else if (ui_mode == UI_MODE.EDIT)   ui_mode = UI_MODE.FILTER;
  }

  /**
   * Checkbox to enable all node labels
   */
  JCheckBox node_labels_cb;

  /**
   * Label selection for entities
   */
  JList<String> entity_label_list;

  /**
   * Color selection for entities
   */
  JList<String> entity_color_list;

  /**
   * Constructor ... create the user interface, assign listeners.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTEntitiesPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt) { 
    super(win_type,win_pos,win_uniq,rt);   

    // Make the GUI
    add("Center",  component = new RTEntitiesComponent());

    // Make the Labels and Node Colors
    add("East", createLabelsPanel(getRTParent().getEntityTagTypes()));

    // Make the southern panel
    JPanel south = new JPanel(new FlowLayout()); JButton bt;

    // - Adding and subtracting records from selection
    south.add(bt = new JButton("+Sch")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      addEntitiesPerSchemasForVisibleBundles();
    } } );
      bt.setToolTipText("Add Visible Per Schema");

    south.add(bt = new JButton("+")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      addVisibleBundlesToSelection();
    } } );
      bt.setToolTipText("Add Visible To Selected Entities OR Create New Entity");

    south.add(bt = new JButton("-")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      removeVisibleBundlesFromSelection();
    } } );
      bt.setToolTipText("Remove Visible From Selected Entities");

    // Layout options
    south.add(bt = new JButton("C")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { nodeColorTreeMapLayout(); } } );
    bt.setToolTipText("Color TreeMap Layout");

    // - Selection
    south.add(new JLabel("Sel"));
    south.add(select_tf = new JTextField(12)); select_tf.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { select(select_tf.getText()); } } );
    select_tf.setToolTipText(Selection.getToolTipHelp());

    // - Tagging
    south.add(new JLabel("Tag"));
    south.add(tag_tf = new JTextField(12));
      tag_tf.        addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { tagSelectedEntities(tag_tf.getText());  } } );

    south.add(bt = new JButton("Clr"));
      bt.setToolTipText("Clear All Entity Tags");
      bt. addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clearEntityTagsForSelectedEntities();   } } );

    south.add(bt = new JButton("Rpl"));
      bt.setToolTipText("Replace The Type-Value w/ New Value");
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { replaceEntityTagForSelectedEntities();  } } );

    south.add(bt = new JButton("Rm"));
      bt.setToolTipText("Remove The Specified Type or Type-Value");
      bt. addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeEntityTagForSelectedEntities();   } } );

    add("South", south);

    // Update the menu
    JMenuItem mi;
    getRTPopupMenu().add(mi = new JMenuItem("Create From Field(s)...")); mi.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) { new SetEntitiesDialog(false); } } );
    getRTPopupMenu().add(show_all_cbmi     = new JCheckBoxMenuItem("Always Show All Entities", true));  defaultListener(show_all_cbmi);
    getRTPopupMenu().add(time_markers_cbmi = new JCheckBoxMenuItem("Draw Time Markers",        true));  defaultListener(time_markers_cbmi);
    getRTPopupMenu().add(timeline_cbmi     = new JCheckBoxMenuItem("Timeline Visualization",   false)); defaultListener(timeline_cbmi);
      timeline_cbmi.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) {
        if (first_time_timeline) {
          if (JOptionPane.showConfirmDialog(getRTPanel(), "Reposition all nodes?", "Reposition?", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) {
            timeline_cbmi.setSelected(false);
            return;
          }
          first_time_timeline = false;
        }
        if (timeline_cbmi.isSelected()) placeNodesForTimeline();
    } } );
    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("Keep Only Visible Records")); mi.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) { keepOnlyVisibleRecords(); } } );
    getRTPopupMenu().add(mi = new JMenuItem("Rename (Selected) Entities Based On Fields..."));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { renameEntitiesBasedOnFields(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("Copy Selected Entities")); mi.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) { copySelection(); } } );
    getRTPopupMenu().add(mi = new JMenuItem("Select From Clipboard"));  mi.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent ae) { selectFromClipboard(); } } );
    getRTPopupMenu().addSeparator();

    ButtonGroup bg; JMenu menu;

    // Entity sizes submenu... to include small multiple versions
    JMenu entity_size_menu = new JMenu("Entity Size"); bg = new ButtonGroup();
      // Entity sizes...
      entity_size_rbmis = new JRadioButtonMenuItem[ENTITY_SIZE_STRS.length];
      for (int i=0;i<ENTITY_SIZE_STRS.length;i++) {
        if (ENTITY_SIZE_STRS[i].equals(KeyMaker.BY_MONTH_STR) ||
            ENTITY_SIZE_STRS[i].equals(ENTITY_SIZE_PIE_SMALL_STR) ||
            ENTITY_SIZE_STRS[i].equals(ENTITY_SIZE_XY_VISIBLE_STR) ||
            ENTITY_SIZE_STRS[i].equals(ENTITY_SIZE_LINKNODE_STR)) entity_size_menu.addSeparator();
        entity_size_menu.add(entity_size_rbmis[i] = new JRadioButtonMenuItem(ENTITY_SIZE_STRS[i], i == 0));
        bg.add(entity_size_rbmis[i]);
        defaultListener(entity_size_rbmis[i]);
      }

      // Timeline-related entity sizes...
      entity_size_menu.addSeparator();
      timeline_entity_size_rbmis = new JRadioButtonMenuItem[TIMELINE_ENTITY_SIZE_STRS.length]; bg = new ButtonGroup();
      for (int i=0;i<TIMELINE_ENTITY_SIZE_STRS.length;i++) {
        entity_size_menu.add(timeline_entity_size_rbmis[i] = new JRadioButtonMenuItem(TIMELINE_ENTITY_SIZE_STRS[i], i == 0));
        bg.add(timeline_entity_size_rbmis[i]);
        defaultListener(timeline_entity_size_rbmis[i]);
      }
    getRTPopupMenu().add(entity_size_menu);

    // Node color submenu...
    JMenu entity_color_menu = new JMenu("Entity Color"); bg = new ButtonGroup();
      entity_color_menu.add(entity_color_default_rbmi = new JRadioButtonMenuItem("Default",true)); bg.add(entity_color_default_rbmi);
      entity_color_menu.add(entity_color_vary_rbmi    = new JRadioButtonMenuItem("Vary"));         bg.add(entity_color_vary_rbmi);
      entity_color_menu.add(entity_color_label_rbmi   = new JRadioButtonMenuItem("Label"));        bg.add(entity_color_label_rbmi);   // Requires a popup to define label
      defaultListener(entity_color_default_rbmi); defaultListener(entity_color_vary_rbmi); defaultListener(entity_color_label_rbmi);
    getRTPopupMenu().add(entity_color_menu);

    // Label multi-line thresholds
    JMenu multiline_menu = new JMenu("Multilines"); bg = new ButtonGroup();
      multiline_menu.add(truncate_single_line_rbmi = new JRadioButtonMenuItem("Truncate to single line")); bg.add(truncate_single_line_rbmi);
      multiline_menu.add(truncate_four_line_rbmi   = new JRadioButtonMenuItem("Truncate to four lines"));  bg.add(truncate_four_line_rbmi);
      multiline_menu.add(no_truncation_rbmi        = new JRadioButtonMenuItem("No Truncation", true));     bg.add(no_truncation_rbmi);

      multiline_menu.addSeparator(); bg = new ButtonGroup();
      multiline_menu.add(multiline_32_rbmi         = new JRadioButtonMenuItem("32 Characters", true));     bg.add(multiline_32_rbmi);
      multiline_menu.add(multiline_64_rbmi         = new JRadioButtonMenuItem("64 Characters"));           bg.add(multiline_64_rbmi);
      multiline_menu.add(multiline_96_rbmi         = new JRadioButtonMenuItem("96 Characters"));           bg.add(multiline_96_rbmi);
      multiline_menu.add(multiline_128_rbmi        = new JRadioButtonMenuItem("128 Characters"));          bg.add(multiline_128_rbmi);

      defaultListener(truncate_single_line_rbmi);
      defaultListener(truncate_four_line_rbmi);
      defaultListener(no_truncation_rbmi);
      defaultListener(multiline_32_rbmi);
      defaultListener(multiline_64_rbmi);
      defaultListener(multiline_96_rbmi);
      defaultListener(multiline_128_rbmi);
    getRTPopupMenu().add(multiline_menu);

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("Node Color TreeMap Layout"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { nodeColorTreeMapLayout(); } } );

    /*
    getRTPopupMenu().add(mi = new JMenuItem("Tag Type TreeMap Layout"));      mi.setEnabled(false);
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { tagTypeTreeMapLayout(); } } );
    */

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("Hierarchical Clustering (First/Last)"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { hierarchicalClusteringFirstLast(); } } );

    getRTPopupMenu().add(mi = new JMenuItem("Hierarchical Clustering (Timing Marks)"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { hierarchicalClusteringTimingMarks(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("UMAP Small Multiples Layout"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { smallMultiplesUMAPLayout(); } } );

    getRTPopupMenu().add(mi = new JMenuItem("K-Means Small Multiples Dialog..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { kMeansSmallMultiplesDialog(); } } );

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("XY Dialog..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { displayXYDialog(); } } );

    JMenu sorts_menu = new JMenu("Sort Preference");
    sort_rbmis = new JRadioButtonMenuItem[SortByEnum.values().length]; bg = new ButtonGroup();
      int sort_by_i = 0;
      for (SortByEnum sort_by : SortByEnum.values()) {
        sorts_menu.add(sort_rbmis[sort_by_i] = new JRadioButtonMenuItem("" + sort_by, sort_by_i == 0)); bg.add(sort_rbmis[sort_by_i]);
        sort_by_i++;
      }
    getRTPopupMenu().add(sorts_menu);

    getRTPopupMenu().addSeparator();

    getRTPopupMenu().add(mi = new JMenuItem("UMAP Distance Metric"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { umapDistanceMetric(); } } );
    JMenu distance_metrics_menu = new JMenu("Distance Metrics"); bg = new ButtonGroup();
      distance_metrics_rbmi = new JRadioButtonMenuItem[3];
      distance_metrics_menu.add(distance_metrics_rbmi[DM_EDIT_DISTANCE]            = new JRadioButtonMenuItem("Edit Distance [Not Scalable]", true));      
        bg.add(distance_metrics_rbmi[DM_EDIT_DISTANCE]);
      distance_metrics_menu.add(distance_metrics_rbmi[DM_LONGEST_COMMON_SUBSTRING] = new JRadioButtonMenuItem("Longest Common Substring [Not Scalable]")); 
        bg.add(distance_metrics_rbmi[DM_LONGEST_COMMON_SUBSTRING]);
      distance_metrics_menu.add(distance_metrics_rbmi[DM_CUSTOM]                   = new JRadioButtonMenuItem("Custom..."));                               
        bg.add(distance_metrics_rbmi[DM_CUSTOM]); // Requires a popup to define
        distance_metrics_rbmi[DM_CUSTOM].setEnabled(false);
    getRTPopupMenu().add(distance_metrics_menu);

    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(mi = new JMenuItem("Topic Modeling Dialog"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { topicModelingDialog(); } } );

    getRTPopupMenu().addSeparator();

    JMenu rare_options_menu = new JMenu("Rare Options");
      rare_options_menu.add(validate_integrity_cbmi = new JCheckBoxMenuItem("Validate Integrity", false));
      rare_options_menu.add(disable_undo_cbmi       = new JCheckBoxMenuItem("Disable Undo",       false));

    getRTPopupMenu().add(rare_options_menu);
  }

  /**
   * Selected fields for GPS
   */
  private boolean gps_fields_selected       = false;
  private String  selected_latitude_field   = null,
                  selected_longitude_field  = null,
                  selected_entity_field     = null;

  /**
   * Simple dialog to select lat/lon fields
   */
  class GPSSelectDialog extends JDialog {
    JComboBox lat_cb, lon_cb, ent_cb;
    public GPSSelectDialog(Frame owner) {
      super(owner, "GPS Select Dialog", false); BundlesG globals = getRTParent().getRootBundles().getGlobals();

      String fields[] = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, true, false); 
        List<String> latlon_fields = new ArrayList<String>();
        List<String> all_fields    = new ArrayList<String>(); all_fields.add("Not Paths");
        for (int i=0;i<fields.length;i++) {
          // All (or most) fields...
          if (fields[i].equals(KeyMaker.ALL_ENTITIES_STR) == false && fields[i].equals(KeyMaker.TABLET_SEP_STR) == false) all_fields.add(fields[i]);

          // Fields likely to be latitudes or longitudes
          Set<BundlesDT.DT> types = globals.getFieldDataTypes(globals.fieldIndex(fields[i]));
          if (types != null && (types.contains(BundlesDT.DT.INTEGER) || types.contains(BundlesDT.DT.FLOAT))) latlon_fields.add(fields[i]);
        }

      JPanel labels_panel = new JPanel(new GridLayout(3,1,5,5));
        labels_panel.add(new JLabel("Longitude"));
        labels_panel.add(new JLabel("Latitude"));
        labels_panel.add(new JLabel("Entity"));

      JPanel combos_panel = new JPanel(new GridLayout(3,1,5,5));
        combos_panel.add(lon_cb = new JComboBox(latlon_fields.toArray()));
        combos_panel.add(lat_cb = new JComboBox(latlon_fields.toArray()));
        combos_panel.add(ent_cb = new JComboBox(all_fields.toArray()));

      setLayout(new BorderLayout());
      add("West",   labels_panel);
      add("Center", combos_panel);

      JPanel buttons_panel = new JPanel(new FlowLayout(FlowLayout.RIGHT)); JButton bt;
      buttons_panel.add(bt = new JButton("Cancel")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cancelDialog(); } } );
      buttons_panel.add(bt = new JButton("Apply"));  bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyDialog();  } } );
      add("South", buttons_panel);

      pack(); setSize(320,240); setLocationRelativeTo(null);
    }
    private void cancelDialog() {
      entitySize(ENTITY_SIZE_LARGE_STR);
      setVisible(false); dispose();
    }
    private void applyDialog() {
      selected_latitude_field  = (String) lat_cb.getSelectedItem();
      selected_longitude_field = (String) lon_cb.getSelectedItem();
      if (((String) ent_cb.getSelectedItem()).equals("Not Paths")) selected_entity_field = null;
      else                                                         selected_entity_field = (String) ent_cb.getSelectedItem();
      setVisible(false); dispose();
    }
  }

  /**
   * Display a dialog to select gps fields...
   */
  private void requestUserGPSSelection() {
    Container container = getParent(); while (container instanceof Frame == false) container = container.getParent();
    GPSSelectDialog gps_select_dialog = new GPSSelectDialog((Frame) container); 
    gps_select_dialog.setVisible(true);
    gps_fields_selected = true;
  }

  /**
   * Return the selected latitude field... or ask the user to pick one...
   */
  public String getLatitudeField() {
    if (gps_fields_selected == false) requestUserGPSSelection();
    return selected_latitude_field;
  }

  /**
   * Return the selected longitude field... or ask the user to pick one...
   */
  public String getLongitudeField() {
    if (gps_fields_selected == false) requestUserGPSSelection();
    return selected_longitude_field;
  }

  /**
   * Return the selected entity field... or ask the user to pick one...
   */
  public String getEntityField() {
    if (gps_fields_selected == false) requestUserGPSSelection();
    return selected_entity_field;
  }

  /**
   * Either find the only country field in the data set... or ask the user to pick one...
   */
  public String getCountryField() {
    // If the field isn't selected yet, figure it out... or ask the user
    if (selected_country_field == null) {
      // Get entity fields only
      String fields[] = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, true, false); List<String> cc_fields = new ArrayList<String>();
      for (int i=0;i<fields.length;i++) if (Utils.countryCodeField(fields[i])) cc_fields.add(fields[i]);

      // If it's just one, use that... otherwise, give the user a dialog...
      if        (cc_fields.size() == 0) { System.err.println("RTEntitiesPanel().getCountryField():  No Fields Have Country Code Information...");
      } else if (cc_fields.size() == 1) { selected_country_field = cc_fields.get(0);
      } else                            {
        fields = new String[cc_fields.size()]; for (int i=0;i<fields.length;i++) fields[i] = cc_fields.get(i);
        JComboBox combo_box = new JComboBox(fields);
        JOptionPane.showMessageDialog(this, combo_box, "Country Field", JOptionPane.QUESTION_MESSAGE);
        selected_country_field = (String) combo_box.getSelectedItem();
      }
    }
    return selected_country_field;
  }

  /**
   * Country field selected by the user
   */
  private String selected_country_field = null;

  /**
   * Display the topic modeling dialog.
   */
  public void topicModelingDialog() {
    // Get the render context... contains info about what nodes haven't been filtered out
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;

    // If it's a selection, then intersect that with this view
    Set<String> intersection = myrc.filterEntities(getRTParent().getSelectedEntities()); // Filter selection to what's visible in this view

    // Make a copy of the points
    Map<String,Point2D> copy = new HashMap<String,Point2D>(); Iterator<String> it; boolean subset = false;
    if (intersection != null && intersection.size() > 0) { it = intersection.iterator(); subset = true; } else { it = entity_to_wxy.keySet().iterator(); subset = false; }
    while (it.hasNext()) { String e = it.next(); copy.put(e,entity_to_wxy.get(e)); }

    // Call the dialog ... blocking...
    TopicModelDialog tmd = new TopicModelDialog(getRTParent().getControlPanel(), copy);

    // Apply the results... if the original was a selection, constrain to that location/area
    if (tmd.resultsAreValid()) {
      // Subset
      if (subset) {
        // Find mins and maxes for both this app window and the topic model results
        double app_x0, app_y0, app_x1, app_y1, top_x0, top_y0, top_x1, top_y1;
        app_x0 = app_y0 = top_x0 = top_y0 = Double.POSITIVE_INFINITY;
        app_x1 = app_y1 = top_x1 = top_y1 = Double.NEGATIVE_INFINITY;

        it = copy.keySet().iterator(); while (it.hasNext()) {
          String e = it.next(); Point2D app_pt = entity_to_wxy.get(e), top_pt = copy.get(e);
          if (app_pt.getX() < app_x0) app_x0 = app_pt.getX();
          if (app_pt.getY() < app_y0) app_y0 = app_pt.getY();
          if (app_pt.getX() > app_x1) app_x1 = app_pt.getX();
          if (app_pt.getY() > app_y1) app_y1 = app_pt.getY();

          if (top_pt.getX() < top_x0) top_x0 = top_pt.getX();
          if (top_pt.getY() < top_y0) top_y0 = top_pt.getY();
          if (top_pt.getX() > top_x1) top_x1 = top_pt.getX();
          if (top_pt.getY() > top_y1) top_y1 = top_pt.getY();
        }

        // Make sure they are sane
        if (Double.isInfinite(app_x0)) { app_x0 = 0.0; app_x1 = 1.0; }
        if (Double.isInfinite(app_y0)) { app_y0 = 0.0; app_y1 = 1.0; }

        if (Double.isInfinite(top_x0)) { top_x0 = 0.0; top_x1 = 1.0; }
        if (Double.isInfinite(top_y0)) { top_y0 = 0.0; top_y1 = 1.0; }

        if (Math.abs(app_x0 - app_x1) < 0.001) { app_x0 -= 0.1; app_x1 += 0.1; }
        if (Math.abs(app_y0 - app_y1) < 0.001) { app_y0 -= 0.1; app_y1 += 0.1; }

        if (Math.abs(top_x0 - top_x1) < 0.001) { top_x0 -= 0.1; top_x1 += 0.1; }
        if (Math.abs(top_y0 - top_y1) < 0.001) { top_y0 -= 0.1; top_y1 += 0.1; }

        // Apply with the calculated scaling
        it = copy.keySet().iterator(); while (it.hasNext()) {
          String e = it.next(); if (entity_to_wxy.containsKey(e)) {  // should always be true
            double nx = (copy.get(e).getX() - top_x0)/(top_x1 - top_x0), ny = (copy.get(e).getY() - top_y0)/(top_y1 - top_y0);
            entity_to_wxy.put(e, new Point2D.Double(app_x0 + (app_x1 - app_x0)*nx, app_y0 + (app_y1 - app_y0)*ny));
          }
        }

      // All
      } else {
        it = copy.keySet().iterator(); while (it.hasNext()) {
          String e = it.next(); if (entity_to_wxy.containsKey(e)) { // should always be true...
            entity_to_wxy.put(e,copy.get(e));
          }
        }
      }

      // Force a render
      getRTComponent().render();
    }
  }

  /**
   * Indexes for the distance metric buttons
   */
  final static int DM_EDIT_DISTANCE            = 0,
                   DM_LONGEST_COMMON_SUBSTRING = 1,
                   DM_CUSTOM                   = 2;

  // Copy below this line from the RTGraphPanel implementation

  /**
   * Refactored into the Utils.selection() class...
   *
   *@param selection pattern for string selection
   */
  public void select(String selection) {
    // Get the render context... contains info about what nodes haven't been filtered out
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;

    // Get the no operation selection ... have to do direct matches first for the three parts... then apply the set operation...
    String noop_selection = selection;
    if (Selection.selectionContainsSetOperation(selection)) noop_selection = Selection.removeSetOperation(selection);

    // Apply selection to visible (visible means it had at least one visible record in the current stack)
    Set<String> direct_set = new HashSet<String>();;
    if (Selection.isTagRelated(noop_selection) == false) direct_set = Selection.select(myrc.entity_to_sxy.keySet(), noop_selection);

    // Re-run with other labels ... not perfect do to grouped entities... those will all have the same labeling
    Set<String> label_set  = new HashSet<String>();
    if (myrc.node_lm != null && Selection.isTagRelated(noop_selection) == false) {
      // Need a lookup from the node key back to the entities within that node (filtered by visible nodes)
      Map<String,Set<String>> node_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it_entity = myrc.entity_to_sxy.keySet().iterator(); while (it_entity.hasNext()) {
        String entity = it_entity.next();
        String node   = myrc.entity_to_sxy.get(entity);
        if (node_to_entities.containsKey(node) == false) node_to_entities.put(node, new HashSet<String>());
        node_to_entities.get(node).add(entity);
      }

      // Now construct the labels shown
      Map<String,Set<String>> label_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it = myrc.sxy_counter_context.binIterator(); while (it.hasNext()) {
        String node = it.next();
        String values[] = myrc.node_lm.toStrings(node); if (values != null) {
          for (int i=0;i<values.length;i++) { 
            if (label_to_entities.containsKey(values[i]) == false) label_to_entities.put(values[i], new HashSet<String>());
            label_to_entities.get(values[i]).addAll(node_to_entities.get(node));
          }
        }
      }
      Set<String> label_set_matches = Selection.select(label_to_entities.keySet(), noop_selection);
      it = label_set_matches.iterator(); while (it.hasNext()) label_set.addAll(label_to_entities.get(it.next()));
    }

    // Re-run with tags ... if the selection is tag related
    Set<String> tag_set = new HashSet<String>();
    if (Selection.isTagRelated(noop_selection)) {
      Map<String,Set<String>> tag_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it = myrc.entity_to_sxy.keySet().iterator(); while (it.hasNext()) { String entity = it.next();
        Set<String> tags = getRTParent().getEntityTags(entity, getRTParent().getRootBundles().ts0(), getRTParent().getRootBundles().ts1());
        Iterator<String> it_tag = tags.iterator(); while (it_tag.hasNext()) {
          String tag = it_tag.next();
          if (tag_to_entities.containsKey(tag) == false) tag_to_entities.put(tag, new HashSet<String>());
          tag_to_entities.get(tag).add(entity);
        }
      }
      Set<String> tag_set_matches = Selection.select(tag_to_entities.keySet(), noop_selection);
      it = tag_set_matches.iterator(); while (it.hasNext()) tag_set.addAll(tag_to_entities.get(it.next()));
    }

    // Union of the three
    Set<String> union = new HashSet<String>(); union.addAll(direct_set); union.addAll(label_set); union.addAll(tag_set);

    // Get the already selected entities for set-based operations
    Set<String> already_selected_entities = myrc.filterEntities(getRTParent().getSelectedEntities()); // Filter selection to what's visible in this view
    Set<String> finalized_set;
    if (noop_selection.equals(selection) == false) {
      finalized_set = Selection.applySetOperation(selection, union, already_selected_entities, myrc.entity_to_sxy.keySet());
    } else { finalized_set = union; }

    // Apply the selection methodology and set application-wide
    getRTParent().setSelectedEntities(finalized_set); 
  }

  /** 
   * Return a list of the labels that are selected for viewing related to entities (nodes).
   *
   *@return list of labels for nodes
   */
  public java.util.List<String> listEntityLabels() { return Utils.jListGetValuesWrapper(entity_label_list); }

  /** 
   * Return a list of the labels for coloring the nodes.
   *
   *@return list of labels for node coloring
   */
  public java.util.List<String> listEntityColor() { return Utils.jListGetValuesWrapper(entity_color_list); }

  /**
   * Draw the entity labels.
   *
   *@return true to draw the entity labels
   */
  public boolean drawEntityLabels() { return node_labels_cb.isSelected(); }

  /**
   * Set the option to draw entity labels.
   *
   *@param b true to draw entity labels
   */
  public void drawEntityLabels(boolean b) { node_labels_cb.setSelected(b); }

  /**
   * Create panel for handling labeling.
   *
   *@param  entity_tag_types types from type-value tags
   *
   *@return gui panel for labeling
   */
  private JPanel createLabelsPanel(Set<String> entity_tag_types) {
    JPanel panel  = new JPanel(new BorderLayout());
     JPanel labels_panel = new JPanel(new GridLayout(1,3,4,4));
      labels_panel.add(node_labels_cb = new JCheckBox("Node Labels", false)); defaultListener(node_labels_cb);
      panel.add("North", labels_panel);

    JPanel center = new JPanel(new GridLayout(2,1)); JScrollPane scroll;
      // Node Labels
      center.add(scroll = new JScrollPane(entity_label_list = new JList<String>())); defaultListener(entity_label_list);
      scroll.setBorder(BorderFactory.createTitledBorder("Node Label"));
      // Node Color
      center.add(scroll = new JScrollPane(entity_color_list = new JList<String>())); defaultListener(entity_color_list);
      scroll.setBorder(BorderFactory.createTitledBorder("Node Color"));
      entity_color_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      panel.add("Center", center);

    updateLabelLists(entity_tag_types);
    return panel;
  }

  /**
   * Create the labels menu from the existing fields within the bundles.
   * In addition to data types, there are various high-level labeling options
   * at the beginning of each list (entity, number of bundles, etc.)
   *
   *@param entity_tag_types types from type-value tags
   */
  private void updateLabelLists(Set<String> entity_tag_types) {
    updateEntityLabelList(entity_tag_types, entity_label_list);
    updateEntityLabelList(entity_tag_types, entity_color_list);
  }

  /**
   * Update the labels specific for entities.  These are more related to entity data
   * types.
   *
   *@param entity_tag_types types from type-value tags
   *@param gui_list         list to update
   */
  private void updateEntityLabelList(Set<String> entity_tag_types, JList<String> gui_list) {
    List<String> selection = Utils.jListGetValuesWrapper(gui_list);
    List<String> items     = new ArrayList<String>();
    // Add the transforms
    // BundlesG globals = getRTParent().getRootBundles().getGlobals();
    String postproc[] = BundlesDT.listAvailablePostProcessors();
    for (int i=0;i<postproc.length;i++) items.add(postproc[i]);
    /*
      String transforms[] = globals.getTransforms();
      for (int i=0;i<transforms.length;i++) items.add(transforms[i]);
    */
    // Add the tag types
    Iterator<String> it = entity_tag_types.iterator(); 
    while (it.hasNext()) items.add(TAG_TYPE_LM + it.next());
    Collections.sort(items, new CaseInsensitiveComparator());
    // Insert the default items
    items.add(0, IP_LABEL_LM);
    items.add(0, DURATION_HOURS_LM);
    items.add(0, DURATION_DAYS_LM);
    items.add(0, TIMEFRAME_MONTHS_LM);
    items.add(0, TIMEFRAME_DAYS_LM);
    items.add(0, TIMEFRAME_LM);
    items.add(0, LASTHEARD_LM);
    items.add(0, FIRSTHEARD_LM);
    items.add(0, TIMELASTGUIDES_LM);
    items.add(0, TIMEBOTHGUIDES_LM);
    items.add(0, TIMEFIRSTGUIDES_LM);
    items.add(0, TAGS_LM);
    items.add(0, BUNDLECOUNT_LM);
    items.add(0, ENTITYTYPE_LM);
    items.add(0, ENTITYCOUNT_LM);
    items.add(0, ENTITY_LM);
    // Convert and update the list
    setListAndResetSelection(gui_list, items, selection);
  }

  /**
   * Wrapper to set the list of items in a JList and then to keep the
   * selected elements.
   *
   *@param list       gui list to modify
   *@param items      all items that should be in the list
   *@param selection  selected items to retain in the list
   */
  private void setListAndResetSelection(JList<String> list, List<String> items, java.util.List<String> selection) {
    Map<String,Integer> str_to_i = new HashMap<String,Integer>();
    String as_str[] = new String[items.size()]; for (int i=0;i<as_str.length;i++) { as_str[i] = items.get(i); str_to_i.put(as_str[i], i); }
    list.setListData(as_str);
    if (selection.size() > 0) {
      List<Integer> ints = new ArrayList<Integer>();      
      // Find the indices for the previous settings
      for (int i=0;i<selection.size();i++)
        if (str_to_i.containsKey(selection.get(i))) 
          ints.add(str_to_i.get(selection.get(i)));
      // Convert back to ints
      int as_ints[] = new int[ints.size()];
      for (int i=0;i<as_ints.length;i++) as_ints[i] = ints.get(i);
      list.setSelectedIndices(as_ints);
    }
  }

  /**
   * Update the entity tag types for the application.  In this specific case, add them as labeling options.
   *
   *@param types all of the types
   */
  @Override
  public void updateEntityTagTypes(Set<String> types) { updateLabelLists(types); }

  /**
   * Encode an array of strings into a comma-delimited, url-encoded string.  Used for the getConfig() routine.
   */
  private String commaDelimited(String strs[]) {
    StringBuffer sb = new StringBuffer();
    if (strs.length > 0) {
      sb.append(Utils.encToURL(strs[0]));
      for (int i=1;i<strs.length;i++) sb.append("," + Utils.encToURL(strs[i]));
    }
    return sb.toString();
  }

  /**
   * Decode a comma-delimited, url-encoded string into an array of strings.  Used for the setConfig() routine.
   */
  private String[] commaDelimited(String str) {
    StringTokenizer st = new StringTokenizer(str,",");
    String strs[] = new String[st.countTokens()];
    for (int i=0;i<strs.length;i++) strs[i] = Utils.decFmURL(st.nextToken());
    return strs;
  }

  /**
   * Helper method to set all the correct strings in a JList.
   */
  private void setJList(JList<String> list, String strs[]) {
    List<Integer> indexes = new ArrayList<Integer>(); ListModel<String> lm = list.getModel();
    for (int i=0;i<strs.length;i++) { for (int j=0;j<lm.getSize();j++) if (strs[i].equals("" + lm.getElementAt(j))) indexes.add(j); }
    int index_array[] = new int[indexes.size()]; for (int i=0;i<index_array.length;i++) index_array[i] = indexes.get(i);
    list.setSelectedIndices(index_array);
  }

  /**
   * Remove any record that is not currently visible.  Remove the associated entities that are empty.
   */
  public void keepOnlyVisibleRecords() {
    saveState(SaveState.CROSS_REFS);

    Set<Bundle> visible_set = getRTParent().getVisibleBundles().bundleSet();
    Set<Bundle> to_remove   = new HashSet<Bundle>();

    // Determine what to remove
    Iterator<Bundle> it = bundle_to_entities.keySet().iterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); if (visible_set.contains(bundle) == false) { to_remove.add(bundle); }
    }

    // Remove them (and the associated entity relationships)
    Set<String> entities_to_remove = new HashSet<String>();
    it = to_remove.iterator(); while (it.hasNext()) {
      Bundle bundle = it.next();
      Iterator<String> it_ent = bundle_to_entities.get(bundle).iterator(); while (it_ent.hasNext()) {
        String entity = it_ent.next();
        entity_to_bundles.get(entity).remove(bundle);
        if (entity_to_bundles.get(entity).size() == 0) entities_to_remove.add(entity);
      }
      bundle_to_entities.remove(bundle);
    }

    // Remove the empty entities
    Iterator<String> it_ent = entities_to_remove.iterator(); while (it_ent.hasNext()) {
      permanentlyRemoveEntityFromVisualization(it_ent.next());
    }

    // Force a render
    getRTComponent().render();
  }

  /**
   * Copy the selected item names to the clipboard.
   */
  public void copySelection() {
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    Clipboard        clipboard = getToolkit().getSystemClipboard();
    StringBuffer     sb        = new StringBuffer();
    Iterator<String> it        = (myrc.filterEntities(getRTParent().getSelectedEntities())).iterator();
    while (it.hasNext()) sb.append(it.next() + "\n");
    StringSelection selection = new StringSelection(sb.toString());
    clipboard.setContents(selection, null);
  }

  /**
   * Select nodes based on the contents of the clipboard.
   */
  public void selectFromClipboard()    { 
    // Render context for visible entities
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Selection operation
    getRTParent().setSelectedEntities(Selection.selectClipboard(myrc.entity_to_sxy.keySet(), Utils.getClipboardText(this)));

  }

  /**
   * Apply the tags to the selected entities.
   *
   *@param tags tags to apply
   */
  public void tagSelectedEntities(String tags) {
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().rc); if (myrc == null) return;
    if (tags != null && tags.equals("") == false) {
      getRTParent().setSelectedEntities(myrc.filterEntities(getRTParent().getSelectedEntities()), false);
      getRTParent().tagSelectedEntities(tags, myrc.bs.ts0(), myrc.bs.ts1());
    }
  }

  /**
   * Clear out the tags for the selected entities.
   */
  public void clearEntityTagsForSelectedEntities() { getRTParent().clearEntityTagsForSelectedEntities(); }

  /**
   * Replace the specified tag with the user supplied one from the selected entities.
   */
  public void replaceEntityTagForSelectedEntities() { getRTParent().replaceEntityTagForSelectedEntities(tag_tf.getText()); }

  /**
   * Remove the user specified tag from the selected entities.
   */
  public void removeEntityTagForSelectedEntities() { getRTParent().removeEntityTagForSelectedEntities(tag_tf.getText()); }

  // Copy above this line from the RTGraphPanel implementation

  /**
   * Set the strict matches flag.  Legacy method...
   *
   *@param b true to select only known types
   */
  public void strictMatches(boolean b) { }

  /**
   * Options for how to sort the entities that are the same
   */
  JRadioButtonMenuItem sort_rbmis[];

  /**
   * Return the sortBy() option in the menus.
   *
   *@return sort by enumeration
   */
  public SortByEnum sortBy() {
    int i = 0; while (i < sort_rbmis.length) { if (sort_rbmis[i].isSelected()) break; i++; }
    if (i == sort_rbmis.length) i = 0;
    for (SortByEnum sort_by : SortByEnum.values()) { if (sort_rbmis[i].getText().equals(sort_by+"")) return sort_by; }
    return SortByEnum.bundles;
  }

  /**
   * Set the sortBy() option in the menus.
   *
   *@str string representation of the sort by enumeration
   */
  public void sortBy(String str) {
    for (int i=0;i<sort_rbmis.length;i++) if (sort_rbmis[i].getText().equals(str)) sort_rbmis[i].setSelected(true);
  }

  /**
   * Sort By Options Enumeration
   */
  enum SortByEnum { bundles, count_by_sum, count_by_min, count_by_max, count_by_median, count_by_average, 
                    time_duration, time_first, time_last, 
                    time_hierarchical_first_last, time_hierarchical_hash_marks, 
                    small_multiples,
                    alpha, natural };

  /**
   * Always show all entities ... required for adding bundles to an entity...
   */
  JCheckBoxMenuItem show_all_cbmi,

  /**
   * For debugging... validates the integrity of the data structures...
   */
                    validate_integrity_cbmi,
  /**
   * When selected, disable the undo feature -- undo feature requires a lot of extra memory.
   */
                    disable_undo_cbmi;

  /**
   * Return the show all option -- show all means that all of the entities will be shown even if the underlying records are filtered out.
   * Useful for adding records to an entity...
   *
   *@return true to show all 
   */
  public boolean showAll() { return show_all_cbmi.isSelected(); }

  /**
   * Set the show all option.
   *
   *@param b true to show all
   */
  public void showAll(boolean b) { show_all_cbmi.setSelected(b); }

  /**
   * Return the manually added flag...
   *
   *@return manually added flag
   */
  public boolean manuallyAdded() { return manually_added; }

  /**
   * Set the manually added flag.
   *
   * This marks the data as dirty... meaning that it can't just be constructed from the
   * "Added Fields" dialog...  the implication is that the file size will be much larger
   * since records need to be saved yet again... or record references.
   *
   *@param b boolean setting
   */
  private void manuallyAdded(boolean b) { manually_added = b; }

  /**
   * Indicates that the data is manually added to the entities...
   */
  private boolean manually_added = false;

  /**
   * Draw Time Markers checkbox menu item
   */
  JCheckBoxMenuItem time_markers_cbmi;

  /**
   * Return if to draw the application-wide time markers.
   *
   *@return true to render time markers
   */
  public boolean timeMarkers() { return time_markers_cbmi.isSelected(); }

  /**
   * Set the option to draw the application-wide time markers.
   *
   *@param b true to render time markers
   */
  public void timeMarkers(boolean b) { time_markers_cbmi.setSelected(b); }

  /**
   * Add the visible bundles to the select entities.  If no entities are selected,
   * provide a dialog to make a new entity name.
   */
  public void addVisibleBundlesToSelection() {
    saveState(SaveState.CROSS_REFS);

    Set<String> entities = getRTParent().getSelectedEntities();

    //
    // Add to selected entities
    //
    entities.retainAll(entity_to_wxy.keySet()); if (entities.size() > 0) {
      Iterator<Bundle> it_bun = getRTParent().getVisibleBundles().bundleIterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); Iterator<String> it_ent = entities.iterator(); while (it_ent.hasNext()) {
          String entity = it_ent.next();

          // Add the mapping from bundle to entity
          if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
          bundle_to_entities.get(bundle).add(entity);

          // Add the mapping from entity to bundle
          if (entity_to_bundles.containsKey(entity) == false) entity_to_bundles.put(entity, new HashSet<Bundle>());
          entity_to_bundles.get(entity).add(bundle);

          updateEntityTimes(entity, bundle);
        }
      }

      manuallyAdded(true); // Mark the mapping as dirty... have to save off all of the mappings :(

    //
    // Show a dialog to make a new entity
    //
    } else {
      String new_entity = 
        (String) JOptionPane.showInputDialog(this,
                                             "New Entity Name", 
                                             "New Entity",            
                                             JOptionPane.PLAIN_MESSAGE,
                                             null,            
                                             null, 
                                             null);
      if (new_entity != null) {

        user_enterred_entities.add(new_entity); // For merges...

        if (entity_to_bundles.containsKey(new_entity) == false) {
          entity_to_bundles.put(new_entity, new HashSet<Bundle>());
          entity_to_wxy.put(new_entity, new Point2D.Double(Math.random(),Math.random()));
        }

        Iterator<Bundle> it_bun = getRTParent().getVisibleBundles().bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();

          // Add the entity to bundle mapping
          entity_to_bundles.get(new_entity).add(bundle);

          // Add the bundle to entity mapping
          if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
          bundle_to_entities.get(bundle).add(new_entity);

          updateEntityTimes(new_entity, bundle);
        }

        manuallyAdded(true); // Mark the mapping as dirty... have to save off all of the mappings :(
      }
    }

    // Force a re-render
    getRTComponent().render();
  }

  /**
   * Update the first heard and last heard for an entity
   *
   *@param entity entity string
   *@param bundle record grouped into the entity -- examined for first and last heard
   */
  protected void updateEntityTimes(String entity, Bundle bundle) {
    // Update time-based information
    if (bundle.hasTime()) {
      if (entity_first_heard.containsKey(entity) == false)    entity_first_heard.put(entity, bundle.ts0());
      else if (entity_first_heard.get(entity) > bundle.ts0()) entity_first_heard.put(entity, bundle.ts0());

      if (entity_last_heard. containsKey(entity) == false)    entity_last_heard. put(entity, bundle.ts1());
      else if (entity_last_heard.get(entity) < bundle.ts1())  entity_last_heard. put(entity, bundle.ts1());
    }
  }

  /**
   * Recalculate the entity times for a specific entity.  Go through all of the associated records
   * and re-calculate the first and last heard overall for the entity
   */
  protected void updateEntityTimes(String entity) {
    entity_first_heard.remove(entity); entity_last_heard.remove(entity);
    Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
      Bundle bundle = itb.next();
      updateEntityTimes(entity,bundle);
    }
  }

  /**
   * Recalculate times for all of the entities... may not be thread safe.
   */
  protected void recalculateAllEntityTimes() {
    entity_first_heard.clear(); entity_last_heard.clear();
    Iterator<String> it_entity = entity_to_bundles.keySet().iterator(); while (it_entity.hasNext()) {
      String entity = it_entity.next(); Iterator<Bundle> it_bundle = entity_to_bundles.get(entity).iterator(); while (it_bundle.hasNext()) {
        updateEntityTimes(entity, it_bundle.next());
      }
    }
  }

  /**
   * Permanently remove an entity from the system.  Maybe should make this a single
   * structure map...  Combined it all together here so that removal was consistent...
   * may still have tears/race conditions.
   *
   *@param entity entity to remove
   */
  private void permanentlyRemoveEntityFromVisualization(String entity) {
    // saveState(SaveState.CROSS_REFS); // Believe this is only called from already saved methods...

    Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next();
      bundle_to_entities.get(bundle).remove(entity);
      if (bundle_to_entities.get(bundle).size() == 0) bundle_to_entities.remove(bundle);
    }

    entity_to_bundles.  remove(entity);
    entity_to_wxy.      remove(entity);
    entity_first_heard. remove(entity);
    entity_last_heard.  remove(entity);
    entity_to_shape.    remove(entity);

    // Not completely necessary... may not even match... but should be updated when entity is removed elsewhere...
    user_enterred_entities.remove(entity);
  }

  /**
   * Remove the visible bundles from the selected entities permanently.  If no entities are selected,
   * do nothing...  If the entity is now empty, remove it from the visualization.
   */
  public void removeVisibleBundlesFromSelection() {
    saveState(SaveState.CROSS_REFS);

    Set<String> entities = getRTParent().getSelectedEntities();
    entities.retainAll(entity_to_wxy.keySet()); if (entities.size() > 0) {
      Iterator<Bundle> it_bun = getRTParent().getVisibleBundles().bundleIterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); Iterator<String> it_ent = entities.iterator(); while (it_ent.hasNext()) {
          String entity = it_ent.next();
          entity_to_bundles.get(entity).remove(bundle);
          bundle_to_entities.get(bundle).remove(entity);

          // Check for empty entities
          if (entity_to_bundles.get(entity).size() == 0) permanentlyRemoveEntityFromVisualization(entity);
        }
      }

      // Re-do all the times...
      recalculateAllEntityTimes();

      manuallyAdded(true); // Mark the mapping as dirty ... have to save off all of the mappings :(
    }

    // Force a re-render
    getRTComponent().render();
  }

  /**
   * Checkbox for the timeline visualization
   */
  JCheckBoxMenuItem timeline_cbmi;

  /**
   * Return if the timeline visualization is enabled.
   *
   *@return timeline visualization flag
   */
  public boolean timelineVisualization() { return timeline_cbmi.isSelected(); }

  /**
   * Set the option for the timeline visualization.
   *
   *@param b timeline visualization setting
   */
  public void timelineVisualization(boolean b) { timeline_cbmi.setSelected(b); }

  /**
   * Flag for the first time timelie setting is used
   */
  boolean first_time_timeline = true;

  /**
   * Place all nodes based on their timestamp values
   */
  public void placeNodesForTimeline() {
    // Go through all entities
    Iterator<String> it_ent = entity_to_wxy.keySet().iterator(); while (it_ent.hasNext()) {
      String entity = it_ent.next(); 

      // Find the min and max time for the entity based on the underlying records
      long ts_min = Long.MAX_VALUE, ts_max = Long.MIN_VALUE;

      // Get the stored first/last heards
      if (entity_first_heard.containsKey(entity)) {
        ts_min = entity_first_heard.get(entity);
        ts_max = entity_last_heard. get(entity);
      }

      // Place the entities
      if (ts_min != Long.MAX_VALUE) {
        Point2D pt = entity_to_wxy.get(entity);
        entity_to_wxy.put(entity, new Point2D.Double((ts_min + ts_max)/2.0, pt.getY()));
      }
    }
    zoomToFit(); getRTComponent().render();
  }

  /**
   * Adjust the node x coordinate for the timeline visualization.  X coordinate is set to
   * the timestamp (or average timestamp from min and max).  If there is no time setting,
   * then don't adjust the coordinate.  Transform method should be called outside of this
   * routine.
   *
   *@param entity entity to adjust
   */
  public void adjustXCoordinateForTimeline(String entity) {
    if (entity_first_heard.containsKey(entity)) {
      long ts_min = entity_first_heard.get(entity), ts_max = entity_last_heard. get(entity);
      Point2D pt = entity_to_wxy.get(entity);
      entity_to_wxy.put(entity, new Point2D.Double((ts_min + ts_max)/2.0, pt.getY()));
    }
  }

  /**
   * Sorts the specified entities by the sortBy() option in the menu.  If bundles are
   * specified, filter the bundle lookups per entity (where possible).
   *
   *@param entities entities to sort
   *@param bs       records to filter by -- can be null
   */
  public List<String> sortBy(Collection<String> entities, Bundles bs) { 
    return sortBy(entities, bs, sortBy(), getRTParent().getControlPanel().getCountBy(), null); 
  } 

  /**
   * Determines if a field is floats and/or integers.
   *
   *@param field field to check
   *
   *@param true if the field contains only integers or floating point values
   */
  private boolean fieldIsFloatsAndOrIntegers(String field) {
    BundlesG globals = getRTParent().getRootBundles().getGlobals();
    Set<BundlesDT.DT> types = globals.getFieldDataTypes(globals.fieldIndex(field));

    if (types != null) {
      if      (types.size() == 1 && types.contains(BundlesDT.DT.INTEGER)) return true;
      else if (types.size() == 1 && types.contains(BundlesDT.DT.FLOAT))   return true;
      else if (types.size() == 2 && types.contains(BundlesDT.DT.FLOAT) && 
                                    types.contains(BundlesDT.DT.INTEGER)) return true;
      else return false;
    } else return false;
  }

  /**
   * Sorts the specified entities by the sortBy() option in the menu.  If bundles are
   * specified, filter the bundle lookups per entity (where possible).
   *
   *@param entities     entities to sort
   *@param bs           filter records -- can be null
   *@param sort_by      sort by enumeration
   *@param count_by_str field for counting by
   *@param values_lu    values from the sorting -- can be null
   *
   *@return sorted list of entities
   */
  public List<String> sortBy(Collection<String> entities, Bundles bs, SortByEnum sort_by, String count_by_str, Map<String,Double> values_lu) {
    BundlesG   globals = getRTParent().getRootBundles().getGlobals();

    // Filter the entities selection by the rendered entities
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().rc); if (myrc == null) return null;
    Set<String> keepers = new HashSet<String>(); Iterator<String> it = entities.iterator(); 
    while (it.hasNext()) { String entity = it.next(); if (myrc.entity_to_sxy.containsKey(entity)) keepers.add(entity); }
    entities = keepers;

    // Short circuit the count_by string under certain conditions
    if (count_by_str.equals(BundlesDT.COUNT_BY_BUNS) &&
         ( sort_by == SortByEnum.count_by_sum || 
           sort_by == SortByEnum.count_by_min ||
           sort_by == SortByEnum.count_by_max ||
           sort_by == SortByEnum.count_by_median ||
           sort_by == SortByEnum.count_by_average ) ) sort_by = SortByEnum.bundles;

    switch (sort_by) {
      case count_by_sum:
      case count_by_min:
      case count_by_max:
      case count_by_median:
      case count_by_average:   if (globals.isScalar(globals.fieldIndex(count_by_str))) return sortByCountScalars(entities, bs, sort_by, count_by_str, values_lu);
                               else if (fieldIsFloatsAndOrIntegers(count_by_str))      return sortByFloats      (entities, bs, sort_by, count_by_str, values_lu);
                               else                                                    return sortByCountSets   (entities, bs, count_by_str, values_lu);

      case time_duration:      return sortByTimeDuration(entities, bs, values_lu); 
      case time_first:         return sortByTimeFirst   (entities, bs, values_lu);
      case time_last:          return sortByTimeLast    (entities, bs, values_lu);

      case time_hierarchical_first_last:  return sortByHierarchicalClusteringFirstLast(entities, bs);

      case time_hierarchical_hash_marks:  return sortByHierarchicalClusteringHashMarks(entities, bs);

      case small_multiples:    return sortBySmallMultipleSimilarity(entities, bs, count_by_str, getRTParent().getColorBy(), entitySize());

      case natural:            return sortNatural(entities, values_lu);

      case alpha:              break;

      case bundles:           
      default:                 return sortByBundles     (entities, bs);
    }

    //
    // Sort Alpha...
    //
    List<String> sort = new ArrayList<String>();
    sort.addAll(entities); Collections.sort(sort);
    return sort;
  }

  /**
   * Sort the entities by their natural ordering.  For example, if they are all integers, then sort
   * numerically.
   *
   *@param entities   entities to sort
   *@param values_lu  store the lookup value per entity (can be null)
   *
   *@return naturally sorted list
   */
  private List<String> sortNatural(Collection<String> entities, Map<String,Double> values_lu) {
    // Get the datatype / datatypes for the entities
    Set<BundlesDT.DT> dts = new HashSet<BundlesDT.DT>();
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      BundlesDT.DT dt = BundlesDT.getEntityDataType(it.next());
      dts.add(dt);
    }

    //
    // Sort by IP Address
    //
    if        (dts.size() == 1 && dts.contains(BundlesDT.DT.IPv4)) {

      List<StrCountSorter> sorter = new ArrayList<StrCountSorter>(); 

      // Convert IP's to integers
      it = entities.iterator(); while (it.hasNext()) {
        String ip = it.next(); int ip_i = Utils.strToIPInt(ip);
        sorter.add(new StrCountSorter(ip, ip_i));
        if (values_lu != null) values_lu.put(ip, (double) ip_i);
      }

      // Sort
      Collections.sort(sorter);

      // Place back into the right structure
      List<String> ret = new ArrayList<String>();
      for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
      return ret;

    //
    // Sort Numerically
    //
    } else if ((dts.size() == 1 && dts.contains(BundlesDT.DT.INTEGER)) ||
               (dts.size() == 1 && dts.contains(BundlesDT.DT.FLOAT))   ||
               (dts.size() == 2 && dts.contains(BundlesDT.DT.INTEGER) && dts.contains(BundlesDT.DT.FLOAT))) {

      List<StrCountSorterD> sorter = new ArrayList<StrCountSorterD>(); 

      // Convert to doubles
      it = entities.iterator(); while (it.hasNext()) {
        String num = it.next(); double num_d = Double.parseDouble(num);
        sorter.add(new StrCountSorterD(num, num_d));
        if (values_lu != null) values_lu.put(num, num_d);
      }

      // Sort
      Collections.sort(sorter);

      // Place back into the right structure
      List<String> ret = new ArrayList<String>();
      for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
      return ret;

    //
    // Sort alphabetically
    //
    } else {
      List<String> sort = new ArrayList<String>();
      sort.addAll(entities); Collections.sort(sort);
      return sort;
    }
  }

  /** 
   * Sort the entities by the number of unique values in the specified field of the related records.
   *
   *@param entities     entities to sort
   *@param bs           bundles for filter (can be null)
   *@param count_by_str specific record field to use
   *@param values_lu    store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   */
  private List<String> sortByCountSets(Collection<String> entities, Bundles bs, String count_by_str, Map<String,Double> values_lu) {
    Map<Tablet,KeyMaker> km_lu       = new HashMap<Tablet,KeyMaker>();   // one keymaker per tablet cache
    List<StrCountSorter> sorter      = new ArrayList<StrCountSorter>();
    Set<String>          unsortables = new HashSet<String>();            // if the entity has no bundles that satisfy count_by_str...
    Set<Bundle>          match_set   = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // For each entity, construct the list of it's values and the perform the specified operation for the entity's value
    Iterator<String> ite = entities.iterator(); while (ite.hasNext()) {
      String entity = ite.next(); Set<String> strings = new HashSet<String>();
      Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        // For every bundle, make sure it's either in the match_set or that there is no match_set
        Bundle bundle = itb.next(); if (match_set == null || match_set.contains(bundle)) {
          Tablet tablet = bundle.getTablet();
          // Check cache... if not in cache, check tablet's applicability to this count_by_str
          if (km_lu.containsKey(tablet) == false) {
            if (KeyMaker.tabletCompletesBlank(tablet, count_by_str)) km_lu.put(tablet, new KeyMaker(tablet, count_by_str));
            else                                                     km_lu.put(tablet, null);
          }
          // If it satisfies, apply the keymaker and capture the values
          if (km_lu.get(tablet) != null) {
            String keys[] = km_lu.get(tablet).stringKeys(bundle);
            for (int i=0;i<keys.length;i++) { strings.add(keys[i]); } //... would this include notsets?
          }
        }
      }

      // Perform the math operation
      if (strings.size() > 0) sorter.add(new StrCountSorter(entity,strings.size()));
      else                    unsortables.add(entity);
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); int sort_min = Integer.MAX_VALUE, sort_max = Integer.MIN_VALUE;
    for (int i=0;i<sorter.size();i++) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), (double) sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = (int) sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = (int) sorter.get(i).count();
    }
    if (sort_min == Integer.MAX_VALUE) { sort_min = 0; sort_max = 100; } // Make it sane

    // Add in the unsortables
    ret.addAll(unsortables);

    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /** 
   * Sort the entities by a specific field in their related records.  Multiple options for the
   * specific mathematical operation to be used.
   *
   *@param entities     entities to sort
   *@param bs           bundles for filter (can be null)
   *@param sort_by      sort by information
   *@param count_by_str specific record field to use
   *@param values_lu    store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   */
  private List<String> sortByCountScalars(Collection<String> entities, Bundles bs, SortByEnum sort_by, String count_by_str, Map<String,Double> values_lu) {
    Map<Tablet,KeyMaker>  km_lu       = new HashMap<Tablet,KeyMaker>();   // one keymaker per tablet cache
    List<StrCountSorterD> sorter      = new ArrayList<StrCountSorterD>();
    Set<String>           unsortables = new HashSet<String>();            // if the entity has no bundles that satisfy count_by_str...
    Set<Bundle>           match_set   = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // For each entity, construct the list of it's values and the perform the specified operation for the entity's value
    Iterator<String> ite = entities.iterator(); while (ite.hasNext()) {
      String entity = ite.next(); List<Integer> values = new ArrayList<Integer>(); long sum = 0L; int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;
      Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        // For every bundle, make sure it's either in the match_set or that there is no match_set
        Bundle bundle = itb.next(); if (match_set == null || match_set.contains(bundle)) {
          Tablet tablet = bundle.getTablet();
          // Check cache... if not in cache, check tablet's applicability to this count_by_str
          if (km_lu.containsKey(tablet) == false) {
            if (KeyMaker.tabletCompletesBlank(tablet, count_by_str)) km_lu.put(tablet, new KeyMaker(tablet, count_by_str));
            else                                                     km_lu.put(tablet, null);
          }
          // If it satisfies, apply the keymaker and capture the values
          if (km_lu.get(tablet) != null) {
            int keys[] = km_lu.get(tablet).intKeys(bundle);
            for (int i=0;i<keys.length;i++) { 
              values.add(keys[i]); sum += keys[i]; 
              if (keys[i] < min)  min =  keys[i];
              if (keys[i] > max)  max =  keys[i];
            }
          }
        }
      }

      // Perform the math operation
      if (values.size() > 0) {
        switch (sort_by) {
          case count_by_median:   QuickStats qs = new QuickStats(values);
                                  sorter.add(new StrCountSorterD(entity, qs.median())); 
                                  break;
          case count_by_min:      sorter.add(new StrCountSorterD(entity, min));                          break;
          case count_by_max:      sorter.add(new StrCountSorterD(entity, max));                          break;
          case count_by_average:  sorter.add(new StrCountSorterD(entity, ((double) sum)/values.size())); break;
          case count_by_sum:
          default:                sorter.add(new StrCountSorterD(entity, sum));                          break;
        }
      } else unsortables.add(entity);
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); double sort_min = Double.MAX_VALUE, sort_max = Double.MIN_VALUE;
    for (int i=0;i<sorter.size();i++) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = sorter.get(i).count();
    }
    if (sort_min == Double.MAX_VALUE) { sort_min = 0.0; sort_max = 1.0; } // make sure they are sane

    // Add in all the unsortable items...
    ret.addAll(unsortables);
    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /** 
   * Sort the entities by a specific field in their related records.  Multiple options for the
   * specific mathematical operation to be used.  Copy of the sortByCountScalars with the addition
   * of float parsing and float calculations.
   *
   *@param entities     entities to sort
   *@param bs           bundles for filter (can be null)
   *@param sort_by      sort by information
   *@param count_by_str specific record field to use
   *@param values_lu    store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   */
  private List<String> sortByFloats(Collection<String> entities, Bundles bs, SortByEnum sort_by, String count_by_str, Map<String,Double> values_lu) {
    Map<Tablet,KeyMaker>  km_lu       = new HashMap<Tablet,KeyMaker>();   // one keymaker per tablet cache
    List<StrCountSorterD> sorter      = new ArrayList<StrCountSorterD>();
    Set<String>           unsortables = new HashSet<String>();            // if the entity has no bundles that satisfy count_by_str...
    Set<Bundle>           match_set   = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // For each entity, construct the list of it's values and the perform the specified operation for the entity's value
    Iterator<String> ite = entities.iterator(); while (ite.hasNext()) {
      String entity = ite.next(); List<Double> values = new ArrayList<Double>(); double sum = 0L; double min = Double.MAX_VALUE, max = Double.MIN_VALUE;
      Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        // For every bundle, make sure it's either in the match_set or that there is no match_set
        Bundle bundle = itb.next(); if (match_set == null || match_set.contains(bundle)) {
          Tablet tablet = bundle.getTablet();
          // Check cache... if not in cache, check tablet's applicability to this count_by_str
          if (km_lu.containsKey(tablet) == false) {
            if (KeyMaker.tabletCompletesBlank(tablet, count_by_str)) km_lu.put(tablet, new KeyMaker(tablet, count_by_str));
            else                                                     km_lu.put(tablet, null);
          }
          // If it satisfies, apply the keymaker and capture the values
          if (km_lu.get(tablet) != null) {
            String strs[] = km_lu.get(tablet).stringKeys(bundle);
            for (int i=0;i<strs.length;i++) { 
              double value = Double.parseDouble(strs[i]);
              values.add(value);  sum += value;
              if (value < min)    min =  value;
              if (value > max)    max =  value;
            }
          }
        }
      }

      // Perform the math operation
      if (values.size() > 0) {
        switch (sort_by) {
          case count_by_median:   QuickStatsDouble qs = new QuickStatsDouble(values);
                                  sorter.add(new StrCountSorterD(entity, qs.median())); 
                                  break;
          case count_by_min:      sorter.add(new StrCountSorterD(entity, min));                          break;
          case count_by_max:      sorter.add(new StrCountSorterD(entity, max));                          break;
          case count_by_average:  sorter.add(new StrCountSorterD(entity, ((double) sum)/values.size())); break;
          case count_by_sum:
          default:                sorter.add(new StrCountSorterD(entity, sum));                          break;
        }
      } else unsortables.add(entity);
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); double sort_min = Double.MAX_VALUE, sort_max = Double.MIN_VALUE;
    for (int i=0;i<sorter.size();i++) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = sorter.get(i).count();
    }
    if (sort_min == Double.MAX_VALUE) { sort_min = 0.0; sort_max = 1.0; } // make sure they are sane

    // Add in all the unsortable items...
    ret.addAll(unsortables);
    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /**
   * Sort the entities using hierarchical clustering of their first and last heard values.
   *
   *@param entities entities to sort
   *@param bs       bundles for filter (can be null)
   *
   *@return sorted list of entities
   *
   */
  private List<String> sortByHierarchicalClusteringFirstLast(Collection<String> entities, Bundles bs) {
    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();
    Set<String> unsortables = new HashSet<String>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Get the first and last for each entity -- make it into a feature vector
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); long min = Long.MAX_VALUE, max = Long.MIN_VALUE; Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if ((match_set == null || match_set.contains(bundle)) && bundle.hasTime()) {
          if (bundle.ts0() < min) min = bundle.ts0();
          if (bundle.ts1() > max) max = bundle.ts1();
        }
      }
      if (min != Long.MAX_VALUE) {
        double vec[] = new double[3]; vec[0] = min; vec[1] = max; vec[2] = max - min;
        feature_vectors.put(entity, vec);
      } else unsortables.add(entity);
    }

    // Apply the hierarchical clustering and get the order
    List<String> order = new ArrayList<String>();
    if (feature_vectors.keySet().size() > 0) {
      HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
      order = hc.dendrogramOrder();
    }

    // Add in the unsortables and return
    order.addAll(unsortables);

    return order;
  }

  /**
   * Sort the entities using hierarchical clustering of their hash marks.
   *
   *@param entities entities to sort
   *@param bs       bundles for filter (can be null)
   *
   *@return sorted list of entities
   *
   */
  private List<String> sortByHierarchicalClusteringHashMarks(Collection<String> entities, Bundles bs) {
    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();
    Set<String> unsortables = new HashSet<String>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Get each entity... then place hashmarks into the vector
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); 
      double vec[]  = new double[100]; int vec_adds = 0;

      // Each bundle -- add the contribution
      Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if ((match_set == null || match_set.contains(bundle)) && bundle.hasTime()) {
          int i0 = (int) (vec.length * (bundle.ts0() - bs.ts0())/(bs.ts1() - bs.ts0())),
              i1 = (int) (vec.length * (bundle.ts1() - bs.ts0())/(bs.ts1() - bs.ts0()));
          if (i0 >= vec.length) i0 = vec.length-1;
          if (i1 >= vec.length) i1 = vec.length-1;
          for (int i=i0;i<=i1;i++) { vec[i]++; vec_adds++; }
        }
      }

      if (vec_adds > 0) { normalizeVector(vec); feature_vectors.put(entity, vec); }  else unsortables.add(entity);
    }

    // Apply the hierarchical clustering and get the order
    List<String> order = new ArrayList<String>();
    if (feature_vectors.keySet().size() > 0) {
      HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
      order = hc.dendrogramOrder();
    }

    // Add in the unsortables and return
    order.addAll(unsortables);

    return order;
  }

  /**
   * Sort the entities by their duration.
   *
   *@param entities entities to sort
   *@param bs       bundles for filter (can be null)
   *@param values_lu store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   *
   */
  private List<String> sortByTimeDuration(Collection<String> entities, Bundles bs, Map<String,Double> values_lu) {
    List<StrCountSorterD> sorter      = new ArrayList<StrCountSorterD>();
    Set<String>           unsortables = new HashSet<String>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Get the last for each entity
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); long min = Long.MAX_VALUE, max = Long.MIN_VALUE; Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if ((match_set == null || match_set.contains(bundle)) && bundle.hasTime()) {
          if (bundle.ts0() < min) min = bundle.ts0();
          if (bundle.ts1() > max) max = bundle.ts1();
        }
      }
      if (min == Long.MAX_VALUE) unsortables.add(entity); else sorter.add(new StrCountSorterD(entity, max-min));
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); long sort_min = Long.MAX_VALUE, sort_max = Long.MIN_VALUE;
    for (int i=0;i<sorter.size();i++) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = (long) sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = (long) sorter.get(i).count();
    }
    if (sort_min == Long.MAX_VALUE) { sort_min = 0L; sort_max = 100L; } // Make sure they are sane

    // Add in the unsortables
    ret.addAll(unsortables);
    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /**
   * Sort the entities by their first heard time.
   *
   *@param entities  entities to sort
   *@param bs        bundles for filter (can be null)
   *@param values_lu store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   */
  private List<String> sortByTimeFirst(Collection<String> entities, Bundles bs, Map<String,Double> values_lu) {
    List<StrCountSorterD> sorter      = new ArrayList<StrCountSorterD>();
    Set<String>           unsortables = new HashSet<String>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Get the last for each entity
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); long min = Long.MAX_VALUE; Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if ((match_set == null || match_set.contains(bundle)) && bundle.hasTime()) {
          if (bundle.ts0() < min) min = bundle.ts0();
        }
      }
      if (min == Long.MAX_VALUE) unsortables.add(entity); else sorter.add(new StrCountSorterD(entity, min));
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); long sort_min = Long.MAX_VALUE, sort_max = Long.MIN_VALUE;
    for (int i=sorter.size()-1;i>=0;i--) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = (long) sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = (long) sorter.get(i).count();
    }
    if (sort_min == Long.MAX_VALUE) { sort_min = 0L; sort_max = 100L; } // Make sure they are sane

    // Add in the unsortables
    ret.addAll(unsortables);
    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /**
   * Sort the entities by their last heard time.
   *
   *@param entities   entities to sort
   *@param bs         bundles for filter (can be null)
   *@param values_lu  store the lookup value per entity (can be null)
   *
   *@return sorted list of entities
   */
  private List<String> sortByTimeLast(Collection<String> entities, Bundles bs, Map<String,Double> values_lu) {
    List<StrCountSorterD> sorter      = new ArrayList<StrCountSorterD>();
    Set<String>           unsortables = new HashSet<String>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Get the last for each entity
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); long max = Long.MIN_VALUE; Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if ((match_set == null || match_set.contains(bundle)) && bundle.hasTime()) {
          if (bundle.ts1() > max) max = bundle.ts1();
        }
      }
      if (max == Long.MIN_VALUE) unsortables.add(entity); else sorter.add(new StrCountSorterD(entity, max));
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>(); long sort_min = Long.MAX_VALUE, sort_max = Long.MIN_VALUE;
    for (int i=sorter.size()-1;i>=0;i--) {
      if (values_lu != null) values_lu.put(sorter.get(i).toString(), sorter.get(i).count());
      ret.add(sorter.get(i).toString());
      if (sort_min > sorter.get(i).count()) sort_min = (long) sorter.get(i).count();
      if (sort_max < sorter.get(i).count()) sort_max = (long) sorter.get(i).count();
    }
    if (sort_min == Long.MAX_VALUE) { sort_min = 0L; sort_max = 100L; } // Make sure they are sane

    // Add in the unsortables
    ret.addAll(unsortables);
    if (values_lu != null) {
      Iterator<String> it_unsorts = unsortables.iterator(); while (it_unsorts.hasNext()) {
        String entity = it_unsorts.next();
        values_lu.put(entity, sort_max + 0.1 * (sort_max - sort_min)); // 10% past the max point
      }
    }

    return ret;
  }

  /**
   * Sort a list of entities by their bundle count.  If bundles are specified, first
   * intersect the bundles with the entity_to_bundles structure.
   *
   *@param entities  entities to sort
   *@param bs        filter bundles... but can be null
   */
  private List<String> sortByBundles(Collection<String> entities, Bundles bs) {
    List<StrCountSorter> sorter = new ArrayList<StrCountSorter>();

    // Get the set to match against... or bundles are null and it doesn't matter
    Set<Bundle> match_set = null; if (bs != null) match_set = bs.bundleSet(); else match_set = null;

    // Count the number of records (after intersecting with the passed bundles if not null)
    Iterator<String> it = entities.iterator(); while (it.hasNext()) {
      String entity = it.next(); int sum = 0; Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
        Bundle bundle = itb.next(); if (match_set == null || match_set.contains(bundle)) sum++;
      }
      sorter.add(new StrCountSorter(entity,sum));
    }

    // Sort
    Collections.sort(sorter);

    // Transfer back to an arraylist of strings and return
    List<String> ret = new ArrayList<String>();
    for (int i=0;i<sorter.size();i++) ret.add(sorter.get(i).toString());
    return ret;
  }

  /**
   * Cluster based on where the timing marks occurs for this entity...
   * ... needs to be refactored with the next one... but i think this one is more complete
   * ... i.e., checking to see if the bundle contributes time...
   */
  public void hierarchicalClusteringTimingMarks() {
    saveState(SaveState.POSITIONS);

    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();

    Bundles     bundles  = getRTParent().getVisibleBundles();
    Set<Bundle> visibles = bundles.bundleSet();

    Set<String> sel = getRTParent().getSelectedEntities();

    // Selected subset
    if (sel != null && sel.size() > 0) {
      double y_min = Double.POSITIVE_INFINITY,
             y_max = Double.NEGATIVE_INFINITY;

      // Each Entity...
      Iterator<String> its = sel.iterator(); while (its.hasNext()) {

        // Figure out bounds and then create the feature vector
        String entity = its.next(); if (entity_to_wxy.containsKey(entity)) {
          Point2D pt = entity_to_wxy.get(entity);
          if (pt.getY() < y_min) y_min = pt.getY();
          if (pt.getY() > y_max) y_max = pt.getY();

          if (entity_first_heard.containsKey(entity)) {
            double vec[] = new double[100];
            Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
              Bundle bundle = itb.next(); if (visibles.contains(bundle) && bundle.hasTime()) {
                int i0 = (int) (vec.length * (bundle.ts0() - bundles.ts0())/(bundles.ts1() - bundles.ts0())),
                    i1 = (int) (vec.length * (bundle.ts1() - bundles.ts0())/(bundles.ts1() - bundles.ts0()));
                if (i0 >= vec.length) i0 = vec.length-1;
                if (i1 >= vec.length) i1 = vec.length-1;
                for (int i=i0;i<=i1;i++) vec[i]++;
              }
            }
            normalizeVector(vec); feature_vectors.put(entity, vec);
          }
        }
      }

      // If there are features, run hierarchical clustering and place nodes within the calculated constraints
      if (feature_vectors.keySet().size() > 0) {
        HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
        List<String> order = hc.dendrogramOrder();
        double y = y_min, y_inc = (y_max - y_min)/order.size();
        its = order.iterator(); while (its.hasNext()) {
          String entity = its.next();
          entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
          y += y_inc;
        }
      }

    //
    // Otherwise, all nodes
    //
    } else {
      // Calculate feature vectors
      Iterator<String> its = entity_first_heard.keySet().iterator(); while (its.hasNext()) {
        String entity = its.next();
        double vec[] = new double[100];
        Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
          Bundle bundle = itb.next(); if (visibles.contains(bundle) && bundle.hasTime()) {
            int i0 = (int) (vec.length * (bundle.ts0() - bundles.ts0())/(bundles.ts1() - bundles.ts0())),
                i1 = (int) (vec.length * (bundle.ts1() - bundles.ts0())/(bundles.ts1() - bundles.ts0()));
            if (i0 >= vec.length) i0 = vec.length-1;
            if (i1 >= vec.length) i1 = vec.length-1;
            for (int i=i0;i<=i1;i++) vec[i]++;
          }
        }
        normalizeVector(vec); feature_vectors.put(entity, vec);
      }

      // If there are features, run hierarchical clustering and place nodes within the calculated constraints
      if (feature_vectors.keySet().size() > 0) {
        HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
        List<String> order = hc.dendrogramOrder();
        double y = 0.0, y_inc = 0.1;
        its = order.iterator(); while (its.hasNext()) {
          String entity = its.next();
          entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
          y += y_inc;
        }
      }
    }

    zoomToFit(); getRTComponent().render();
  }

  /**
   * Ensure that the max value is 1.0...  let's choose linear scaling (versus it has to add to 1.0)
   */
  private void normalizeVector(double v[]) {
    double max = v[0]; for (int i=1;i<v.length;i++) if (v[i] > max) max = v[i];
    if (max <= 0.001) max = 1.0;
    for (int i=0;i<v.length;i++) v[i] = v[i]/max;
  }

  /**
   * Layout the nodes vertically based on how similar their start / ends are in time.
   * ... need to refactor w/ the above method... 90% overlap in code
   *
   * ... appears like this one is less complete -- doesn't check to make sure the bundle is visible...
   */
  public void hierarchicalClusteringFirstLast() {
    saveState(SaveState.POSITIONS);

    Map<String,double[]> feature_vectors = new HashMap<String,double[]>();

    Set<String> sel = getRTParent().getSelectedEntities();

    //
    // Layout the selected nodes locally
    //
    if (sel != null && sel.size() > 0) {
      double y_min = Double.POSITIVE_INFINITY,
             y_max = Double.NEGATIVE_INFINITY;

      // Figure out what's actually shown by this component... should this be currently visible?
      Iterator<String> its = sel.iterator(); while (its.hasNext()) {
        String entity = its.next(); if (entity_to_wxy.containsKey(entity)) {
          Point2D pt = entity_to_wxy.get(entity);
          if (pt.getY() < y_min) y_min = pt.getY();
          if (pt.getY() > y_max) y_max = pt.getY();
          
          if (entity_first_heard.containsKey(entity)) {
            double vec[] = new double[2];
            vec[0] = entity_first_heard.get(entity);
            vec[1] = entity_last_heard.get(entity);
            feature_vectors.put(entity, vec);
          }
        }
      }

      // If there are features, run hierarchical clustering and place nodes within the calculated constraints
      if (feature_vectors.keySet().size() > 0) {
        HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
        List<String> order = hc.dendrogramOrder();
        double y = y_min, y_inc = (y_max - y_min)/order.size();
        its = order.iterator(); while (its.hasNext()) {
          String entity = its.next();
          entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
          y += y_inc;
        }
      }

    //
    // Layout all of the nodes
    //
    } else {
      // Calculate feature vectors
      Iterator<String> its = entity_first_heard.keySet().iterator(); while (its.hasNext()) {
        String entity = its.next();
        double vec[] = new double[2];
        vec[0] = entity_first_heard.get(entity);
        vec[1] = entity_last_heard.get(entity);
        feature_vectors.put(entity, vec);
      }

      // If there are features, run hierarchical clustering and place nodes within the calculated constraints
      if (feature_vectors.keySet().size() > 0) {
        HierarchicalClustering hc = new HierarchicalClustering(feature_vectors, HierarchicalClustering.VectorDistance.euclidean);
        List<String> order = hc.dendrogramOrder();
        double y = 0.0, y_inc = 0.1;
        its = order.iterator(); while (its.hasNext()) {
          String entity = its.next();
          entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
          y += y_inc;
        }
      }
    }

    zoomToFit(); getRTComponent().render();
  }

  /**
   * Lay out the nodes based on their color.  If selected, lay out locally.
   */
  public void nodeColorTreeMapLayout() {
    saveState(SaveState.POSITIONS);

    Set<String> sel = getRTParent().getSelectedEntities();

    // 
    // Layout the selected nodes locally
    //
    if (sel != null && sel.size() > 0) {
      Map<Color,Set<String>> color_to_entities = new HashMap<Color,Set<String>>();
      Map<String,Color>      entity_to_color   = new HashMap<String,Color>();
      double                 x_min             = Double.POSITIVE_INFINITY, // Boundaries for the selection
                             x_max             = Double.NEGATIVE_INFINITY, // ... the layout will be placed within this constraint
                             y_min             = Double.POSITIVE_INFINITY,
                             y_max             = Double.NEGATIVE_INFINITY;

      // Figure out what's actually shown by this component... should this be currently visible?
      Iterator<String> its = sel.iterator(); while (its.hasNext()) {
        String entity = its.next(); if (entity_to_wxy.containsKey(entity)) {
          Color color = calculateEntityColor(entity);
          if (color_to_entities.containsKey(color) == false) color_to_entities.put(color, new HashSet<String>());
          color_to_entities.get(color).add(entity);
          entity_to_color.put(entity, color);

          Point2D pt = entity_to_wxy.get(entity);
          if (pt.getX() < x_min) x_min = pt.getX();
          if (pt.getX() > x_max) x_max = pt.getX();
          if (pt.getY() < y_min) y_min = pt.getY();
          if (pt.getY() > y_max) y_max = pt.getY();
        }
      }

      // Make sure somethings left
      if (color_to_entities.keySet().size() > 0) {
        //
        // Timeline
        //
        if (timelineVisualization()) {
          // Put colors and sizes into a sorter
          List<ColorCountSorter> sorter = new ArrayList<ColorCountSorter>(); int total = 0;
          Iterator<Color> itc = color_to_entities.keySet().iterator(); while (itc.hasNext()) {
            Color color = itc.next(); sorter.add(new ColorCountSorter(color, color_to_entities.get(color).size()));
            total += color_to_entities.get(color).size();
          }
          // Sort
          Collections.sort(sorter);
          //  Layout
          double y = y_min, y_inc = (y_max - y_min)/(total + color_to_entities.keySet().size());

          for (int i=0;i<sorter.size();i++) {
            Color color = sorter.get(i).getColor(); 

            // Perform an inner sort 
            its = sortBy(color_to_entities.get(color), getRTParent().getVisibleBundles()).iterator();

            while (its.hasNext()) {
              String entity = its.next();
              entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
              y += y_inc;
            }
            y += y_inc; // extra space between the colors
          }

        //
        // Regular TreeMap...
        //
        } else {
          TreeMap<Color,String>  treemap = new TreeMap<Color,String>(color_to_entities);
          Map<Color,Rectangle2D> layout  = treemap.squarifiedTileMapping();
          treemap.transformWorldPoints(layout, entity_to_wxy, x_min, y_min, x_max, y_max);
        }

        zoomToFit(); getRTComponent().render();
      }

    //
    // Layout all the nodes
    //
    } else {
      // For all entities...
      Map<Color,Set<String>> color_to_entities = new HashMap<Color,Set<String>>();
      Map<String,Color>      entity_to_color   = new HashMap<String,Color>();

      // Get their color and create the appropriate data structure
      Iterator<String> ite = entity_to_wxy.keySet().iterator(); while (ite.hasNext()) {
        String entity = ite.next(); Color color = calculateEntityColor(entity);
        if (color_to_entities.containsKey(color) == false) color_to_entities.put(color, new HashSet<String>());
        color_to_entities.get(color).add(entity);
        entity_to_color.put(entity, color);
      }

      // Run the treemap algorithm
      if (color_to_entities.keySet().size() > 0) {
        //
        // Timeline version
        //
        if (timelineVisualization()) {
          // Put colors and sizes into a sorter
          List<ColorCountSorter> sorter = new ArrayList<ColorCountSorter>(); int total = 0;
          Iterator<Color> itc = color_to_entities.keySet().iterator(); while (itc.hasNext()) {
            Color color = itc.next(); sorter.add(new ColorCountSorter(color, color_to_entities.get(color).size()));
            total += color_to_entities.get(color).size();
          }
          // Sort
          Collections.sort(sorter);
          //  Layout
          double y = 0.0, y_inc = 1.0;

          for (int i=0;i<sorter.size();i++) {
            Color color = sorter.get(i).getColor();

            // Perform an inner sort 
            Iterator<String> its = sortBy(color_to_entities.get(color), getRTParent().getVisibleBundles()).iterator();

            while (its.hasNext()) {
              String entity = its.next();
              entity_to_wxy.put(entity, new Point2D.Double(entity_to_wxy.get(entity).getX(), y));
              y += y_inc;
            }
            y += y_inc; // extra space between the colors
          }

        //
        // Regular treemap version...
        //
        } else {
          TreeMap<Color,String>  treemap = new TreeMap<Color,String>(color_to_entities);
          Map<Color,Rectangle2D> layout  = treemap.squarifiedTileMapping();
          treemap.transformWorldPoints(layout, entity_to_wxy);
        }

        zoomToFit(); getRTComponent().render();
      }
    }
  }

  /**
   * Display a dialog for manipulating the XY positioning of entities based on sorts.
   */
  public void displayXYDialog() { 
    saveLayoutForUndo(entity_to_wxy, entity_to_wxy.keySet());

    Container container = getParent(); while (container instanceof Frame == false) container = container.getParent();

    XYDialog xydialog = new XYDialog((Frame) container); 
    xydialog.setVisible(true);
  }

  /**
   * Dialog to manipulate the entities interactively on the xy axis.
   */
  class XYDialog extends JDialog {
    /**
     * X axis sort by
     */
    JComboBox<SortByEnum> x_cb,

    /**
     * Y axis sort by
     */
                          y_cb;
    /**
     * X axis count by string
     */
    JComboBox<String>     x_countby_cb,

    /**
     * Y axis count by string
     */
                          y_countby_cb;

    /**
     * X axis equal spacing
     */
    JCheckBox x_eq_cb,

    /**
     * Y axis equal spacing
     */
              y_eq_cb,

    /**
     * Reverse x axis
     */
              x_rev_cb,

    /**
     * Reverse y axis
     */
              y_rev_cb,

    /**
     * Determines if fit should be called prior to render
     */
              refit_cb;

    /**
     * Constructor for the dialog -- add in the components and make it visible.  Assumes
     * that the existing coordinates for entities have already been saved to backups...
     *
     *@param owner parent of dialog
     */
    public XYDialog(Frame owner) {
      super(owner, "XY Dialog", false);
      int gaps = 8;
      setLayout(new BorderLayout(8,8));

      JPanel center  = new JPanel(new BorderLayout());

      JPanel cbs     = new JPanel(new GridLayout(3,1,gaps,gaps));
        cbs.add(x_cb = new JComboBox<SortByEnum>(SortByEnum.values()));
        cbs.add(y_cb = new JComboBox<SortByEnum>(SortByEnum.values()));
        cbs.add(new JLabel("")); // Placeholder

      JPanel count_bys       = new JPanel(new GridLayout(3,1,gaps,gaps));
        count_bys.add(x_countby_cb = new JComboBox<String>(KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), true, true, true, true)));
        count_bys.add(y_countby_cb = new JComboBox<String>(KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), true, true, true, true)));
        count_bys.add(new JLabel("")); // Placeholder

      JPanel equals  = new JPanel(new GridLayout(3,2,gaps,gaps));
        equals.add(x_eq_cb  = new JCheckBox("X Eq", true));
        equals.add(x_rev_cb = new JCheckBox("Rev",  false));
        equals.add(y_eq_cb  = new JCheckBox("Y Eq", true));
        equals.add(y_rev_cb = new JCheckBox("Rev",  false));
        equals.add(refit_cb = new JCheckBox("Fit",  false));
        equals.add(new JLabel(""));

      center.add("Center", cbs);
      center.add("East",   count_bys);
      center.add("West",   equals);

      add("Center", center);

      JButton bt;
      JPanel buttons = new JPanel(new FlowLayout(FlowLayout.LEFT,10,2));
      buttons.add(bt = new JButton("Refresh")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { modifyLayoutAndRender();  } } );
      buttons.add(bt = new JButton("Revert"));  bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { revert(); } } );

      add("South", buttons);

      // Listeners
      ItemListener il = new ItemListener() { public void itemStateChanged(ItemEvent ie) { 
        if (ie.getSource() instanceof JComboBox && ie.getStateChange() == ItemEvent.SELECTED) modifyLayoutAndRender();
        if (ie.getSource() instanceof JCheckBox)                                              modifyLayoutAndRender();
      } };
      x_cb.        addItemListener(il); y_cb.        addItemListener(il);
      x_eq_cb.     addItemListener(il); y_eq_cb.     addItemListener(il);
      x_rev_cb.    addItemListener(il); y_rev_cb.    addItemListener(il);
      x_countby_cb.addItemListener(il); y_countby_cb.addItemListener(il);
      refit_cb.    addItemListener(il);

      // Pack & Display
      pack(); setLocationRelativeTo(owner);
      setVisible(true);

      // WindowAdapter to call the apply() method upon closing
      addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { close(); } } );
    }

    /**
     * Modify the visualizations layout information and force a re-render.
     */
    private void modifyLayoutAndRender() {
      SortByEnum x_sortby = (SortByEnum) x_cb.getSelectedItem(),
                 y_sortby = (SortByEnum) y_cb.getSelectedItem();
      boolean    x_equals = x_eq_cb.isSelected(),
                 y_equals = y_eq_cb.isSelected();

      // Figure out the selection... if showAll() limit to any entities in view... else to only what was rendered
      Set<String> sel = getRTParent().getSelectedEntities(); if (sel != null && sel.size() > 0) {
        if (showAll()) {
          sel.retainAll(entity_to_wxy.keySet());
        } else {
          RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc != null) {
            Iterator<String> it = sel.iterator(); while (it.hasNext()) {
              String entity = it.next(); if (myrc.sxy_counter_context.hasBin(entity) == false) it.remove();
            }
          } else sel.retainAll(entity_to_wxy.keySet()); // No render context?  just use all the entities...
        }
      } else sel = new HashSet<String>();

      // If a selection still exists, get the min and max coordinates ... so that we can adjust locally
      double sel_x0 = Double.MAX_VALUE, sel_y0 = Double.MAX_VALUE, sel_x1 = Double.MIN_VALUE, sel_y1 = Double.MIN_VALUE;
      if (sel.size() > 0) {
        Iterator<String> it = sel.iterator(); while (it.hasNext()) {
          Point2D pt = entity_to_wxy.get(it.next());
          if (sel_x0 > pt.getX()) sel_x0 = pt.getX(); if (sel_y0 > pt.getY()) sel_y0 = pt.getY();
          if (sel_x1 < pt.getX()) sel_x1 = pt.getX(); if (sel_y1 < pt.getY()) sel_y1 = pt.getY();
        }
      }
      if (sel_x0 == Double.MAX_VALUE) { sel_x0 = sel_y0 = 0.0; sel_x1 = sel_y1 = 1.0; } // Make the values sane

      Set<String> to_use = (sel.size() > 0) ? sel : entity_to_wxy.keySet();

      // Perform the sorts / ordering of the entities
      Map<String,Double> x_values_lu = new HashMap<String,Double>(), y_values_lu = new HashMap<String,Double>();
      List<String> x_order = sortBy(to_use, getRTParent().getVisibleBundles(), x_sortby, (String) x_countby_cb.getSelectedItem(), x_values_lu),
                   y_order = sortBy(to_use, getRTParent().getVisibleBundles(), y_sortby, (String) y_countby_cb.getSelectedItem(), y_values_lu);

      // Place the selection...
      if (x_equals || x_values_lu.keySet().size() == 0) { 
        int len = x_order.size()-1; if (len <= 0) len = 1;
        for (int i=0;i<x_order.size();i++) x_values_lu.put(x_order.get(i), i/((double) len)); 
      }
      if (y_equals || y_values_lu.keySet().size() == 0) { 
        int len = y_order.size()-1; if (len <= 0) len = 1;
        for (int i=0;i<y_order.size();i++) y_values_lu.put(y_order.get(i), i/((double) len)); 
      }

      Iterator<String> it = to_use.iterator(); while (it.hasNext()) {
        String entity = it.next(); 
        double x,y;

        // Apply the reverse if necessary
        if (x_rev_cb.isSelected())  x = sel_x0 + (sel_x1 - sel_x0) -  (sel_x1 - sel_x0) * x_values_lu.get(entity); 
        else                        x = sel_x0 +                      (sel_x1 - sel_x0) * x_values_lu.get(entity); 

        if (y_rev_cb.isSelected())  y = sel_y0 + (sel_y1 - sel_y0) -  (sel_y1 - sel_y0) * y_values_lu.get(entity);
        else                        y = sel_y0 +                      (sel_y1 - sel_y0) * y_values_lu.get(entity);

        // Store the new point
        entity_to_wxy.put(entity, new Point2D.Double(x,y));

        // Adjust for timeline if timeline visualization is set
        if (timelineVisualization()) adjustXCoordinateForTimeline(entity); // Honor the timeline requirements...
      }

      // Re-render
      if (refit_cb.isSelected()) zoomToFit();
      getRTComponent().render();
    }

    /**
     * Close the window and disposing of the dialog...
     */
    private void close() { setVisible(false); dispose(); }

    /**
     * Restore the saved layout and then close the window.
     */
    private void revert() { undoLayoutChanges(); setVisible(false); dispose(); getRTComponent().render();  }
  }

  /**
   * Lay out the nodes by their displayed label type.
   */
  public void tagTypeTreeMapLayout() {
    System.err.println("tagTypeTreeMapLayout() not implemented");
  }

  /**
   * Distance metric options
   */
  JRadioButtonMenuItem distance_metrics_rbmi[]; 

  /**
   * Entity size option
   */
  JRadioButtonMenuItem entity_size_rbmis[],

  /**
   * Entity size options for timeline mode
   */
                       timeline_entity_size_rbmis[];

  /**
   * Increment to the next node size.
   */
  public void nextNodeSize() {
    int i = 0; JRadioButtonMenuItem rbmis[];

    if (timelineVisualization()) { rbmis = timeline_entity_size_rbmis; } else { rbmis = entity_size_rbmis; }

    while (i < rbmis.length) {
      if (rbmis[i].isSelected()) {
        i = (i+1)%rbmis.length;
        rbmis[i].setSelected(true);
      }
      i++;
    }
  }

  /**
   * Entity size strings -- almost an exact copy from the graph panel
   */
  public final String ENTITY_SIZE_LARGE_STR              = "Large",
                      ENTITY_SIZE_SMALL_STR              = "Small",
                      ENTITY_SIZE_VARY_STR               = "Vary",

                      ENTITY_SIZE_PIE_SMALL_STR          = "Pie Chart (Sm)",
                      ENTITY_SIZE_PIE_LARGE_STR          = "Pie Chart (Lg)",

                      ENTITY_SIZE_XY_VISIBLE_STR         = "XY (Visible Timeframe)",
                      ENTITY_SIZE_XY_VISIBLE_EQUAL_STR   = "XY (Visible Timeframe, Equal)",
                      ENTITY_SIZE_XY_LOCAL_STR           = "XY (Local Timeframe)",
                      ENTITY_SIZE_XY_LOCAL_EQUAL_STR     = "XY (Local Timeframe, Equal)",

                      ENTITY_SIZE_LINKNODE_STR           = "LinkNode...",     // Requires a popup to define the relationships

                      ENTITY_SIZE_GPS_SMALL_STR          = "GPS Small...",    // Requires a popup to define the gps fields // Longitude, Latitude
                      ENTITY_SIZE_GPS_MEDIUM_STR         = "GPS Medium...",   // Requires a popup to define the gps fields // Longitude, Latitude
                      ENTITY_SIZE_GPS_STR                = "GPS...",          // Requires a popup to define the gps fields // Longitude, Latitude // Legacy String ... map it to Medium
                      ENTITY_SIZE_GPS_LARGE_STR          = "GPS Large...",    // Requires a popup to define the gps fields // Longitude, Latitude
                      ENTITY_SIZE_GPS_LOCAL_STR          = "GPS (Local)...",  // Requires a popup to define the gps fields // Longitude, Latitude

                      ENTITY_SIZE_COUNTRY_SMALL_STR      = "Country (Sm)...", // Requires a popup to define the country field
                      ENTITY_SIZE_COUNTRY_LARGE_STR      = "Country (Lg)...", // Requires a popup to define the country_field

                      TIMELINE_PREFIX_STR                = "Timeline",
                      ENTITY_SIZE_TIMELINE_BLOCKS_STR         = TIMELINE_PREFIX_STR + " Blocks",
                      ENTITY_SIZE_TIMELINE_LARGE_STR          = TIMELINE_PREFIX_STR + " Large",
                      ENTITY_SIZE_TIMELINE_SMALL_STR          = TIMELINE_PREFIX_STR + " Small",
                      ENTITY_SIZE_TIMELINE_SMALL_VARY_STR     = TIMELINE_PREFIX_STR + " Small (Vary Color)",
                      ENTITY_SIZE_TIMELINE_SMALL_OVERLAPS_STR = TIMELINE_PREFIX_STR + " Small (Overlaps)";

  /**
   * Entity size strings as an array -- matches the entity_size_rbmis array
   */
  public final String ENTITY_SIZE_STRS[] = { ENTITY_SIZE_LARGE_STR, ENTITY_SIZE_SMALL_STR, ENTITY_SIZE_VARY_STR,

                                             KeyMaker.BY_MONTH_STR, KeyMaker.BY_DAYOFWEEK_STR, KeyMaker.BY_HOUR_STR, KeyMaker.BY_DAYOFWEEK_HOUR_STR,
                                             KeyMaker.BY_STRAIGHT_STR + " (Sm)", KeyMaker.BY_STRAIGHT_STR + " (Lg)", KeyMaker.BY_AUTOTIME_STR,

                                             ENTITY_SIZE_PIE_SMALL_STR, ENTITY_SIZE_PIE_LARGE_STR,

                                             ENTITY_SIZE_XY_VISIBLE_STR,    ENTITY_SIZE_XY_VISIBLE_EQUAL_STR, ENTITY_SIZE_XY_LOCAL_STR,  ENTITY_SIZE_XY_LOCAL_EQUAL_STR,

                                             ENTITY_SIZE_LINKNODE_STR,

                                             ENTITY_SIZE_GPS_SMALL_STR,     ENTITY_SIZE_GPS_MEDIUM_STR,       ENTITY_SIZE_GPS_LARGE_STR, ENTITY_SIZE_GPS_LOCAL_STR, 

                                             ENTITY_SIZE_COUNTRY_SMALL_STR, ENTITY_SIZE_COUNTRY_LARGE_STR
                                           };
  /**
   * Specific timeline versions of the entity size
   */
  public final String TIMELINE_ENTITY_SIZE_STRS[] = {

                                             ENTITY_SIZE_TIMELINE_BLOCKS_STR,     ENTITY_SIZE_TIMELINE_LARGE_STR, 
                                             ENTITY_SIZE_TIMELINE_SMALL_STR, 
                                             ENTITY_SIZE_TIMELINE_SMALL_VARY_STR, ENTITY_SIZE_TIMELINE_SMALL_OVERLAPS_STR };

  /**
   * Entity size enumeration -- matches strings above
   */
  public enum EntitySize { Large, Small, Vary, 
                           ByMonth, ByDayOfWeek, ByHour, ByDayOfWeekHour, ContinuousSmall, ContinuousLarge, AutoTime,
                           PieSmall, PieLarge, 
                           XYVisible, XYVisibleEqual, XYLocal, XYLocalEqual, 
                           LinkNode,
                           GPSSmall, GPSMedium, GPSLarge, GPSLocal,
                           CountrySmall, CountryLarge,
                           TimelineBlocks, TimelineLarge, TimelineSmall, TimelineSmallVary, TimelineSmallOverlaps };

  /**
   * Return the user-specified entity size.
   *
   *@return entity size
   */
  public EntitySize entitySize() {
    if (timelineVisualization()) {
      String str = ENTITY_SIZE_TIMELINE_BLOCKS_STR;
      for (int i=0;i<timeline_entity_size_rbmis.length;i++) if (timeline_entity_size_rbmis[i].isSelected()) str = timeline_entity_size_rbmis[i].getText();

      if      (str.equals(ENTITY_SIZE_TIMELINE_BLOCKS_STR))         return EntitySize.TimelineBlocks;
      else if (str.equals(ENTITY_SIZE_TIMELINE_LARGE_STR))          return EntitySize.TimelineLarge;
      else if (str.equals(ENTITY_SIZE_TIMELINE_SMALL_STR))          return EntitySize.TimelineSmall;
      else if (str.equals(ENTITY_SIZE_TIMELINE_SMALL_VARY_STR))     return EntitySize.TimelineSmallVary;
      else if (str.equals(ENTITY_SIZE_TIMELINE_SMALL_OVERLAPS_STR)) return EntitySize.TimelineSmallOverlaps;
      else                                                          return EntitySize.TimelineBlocks;

    } else {
      // Find the radio button menu item that is selected and get the string
      String str = ENTITY_SIZE_LARGE_STR;
      for (int i=0;i<entity_size_rbmis.length;i++) if (entity_size_rbmis[i].isSelected()) str = entity_size_rbmis[i].getText();
  
      // Transform it into the String
      if      (str.equals(ENTITY_SIZE_LARGE_STR))               return EntitySize.Large;
      else if (str.equals(ENTITY_SIZE_SMALL_STR))               return EntitySize.Small;
      else if (str.equals(ENTITY_SIZE_VARY_STR))                return EntitySize.Vary;
      else if (str.equals(ENTITY_SIZE_PIE_SMALL_STR))           return EntitySize.PieSmall;
      else if (str.equals(ENTITY_SIZE_PIE_LARGE_STR))           return EntitySize.PieLarge;
      else if (str.equals(ENTITY_SIZE_XY_VISIBLE_STR))          return EntitySize.XYVisible;
      else if (str.equals(ENTITY_SIZE_XY_VISIBLE_EQUAL_STR))    return EntitySize.XYVisibleEqual;
      else if (str.equals(ENTITY_SIZE_XY_LOCAL_STR))            return EntitySize.XYLocal;
      else if (str.equals(ENTITY_SIZE_XY_LOCAL_EQUAL_STR))      return EntitySize.XYLocalEqual;
      else if (str.equals(ENTITY_SIZE_LINKNODE_STR))            return EntitySize.LinkNode;
      else if (str.equals(ENTITY_SIZE_GPS_SMALL_STR))           return EntitySize.GPSSmall;
      else if (str.equals(ENTITY_SIZE_GPS_STR))                 return EntitySize.GPSMedium;        // Legacy Setting
      else if (str.equals(ENTITY_SIZE_GPS_MEDIUM_STR))          return EntitySize.GPSMedium;
      else if (str.equals(ENTITY_SIZE_GPS_LARGE_STR))           return EntitySize.GPSLarge;
      else if (str.equals(ENTITY_SIZE_GPS_LOCAL_STR))           return EntitySize.GPSLocal;
      else if (str.equals(ENTITY_SIZE_COUNTRY_SMALL_STR))       return EntitySize.CountrySmall;
      else if (str.equals(ENTITY_SIZE_COUNTRY_LARGE_STR))       return EntitySize.CountryLarge;
      else if (str.equals(KeyMaker.BY_MONTH_STR))               return EntitySize.ByMonth;
      else if (str.equals(KeyMaker.BY_DAYOFWEEK_STR))           return EntitySize.ByDayOfWeek;
      else if (str.equals(KeyMaker.BY_DAYOFWEEK_HOUR_STR))      return EntitySize.ByDayOfWeekHour;
      else if (str.equals(KeyMaker.BY_HOUR_STR))                return EntitySize.ByHour;
      else if (str.equals(KeyMaker.BY_STRAIGHT_STR + " (Sm)"))  return EntitySize.ContinuousSmall;
      else if (str.equals(KeyMaker.BY_STRAIGHT_STR + " (Lg)"))  return EntitySize.ContinuousLarge;
      else if (str.equals(KeyMaker.BY_AUTOTIME_STR))            return EntitySize.AutoTime;
      else                                                      return EntitySize.Large;
    }
  }

  /**
   * Set the entity size.
   *
   *@param str new entity size setting
   */
  public void entitySize(String str) {
    JRadioButtonMenuItem rbmis[] = null; String gui_str = ENTITY_SIZE_LARGE_STR;

    if (str.equals("" + EntitySize.Large))           { gui_str = ENTITY_SIZE_LARGE_STR;              rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.Small))           { gui_str = ENTITY_SIZE_SMALL_STR;              rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.Vary))            { gui_str = ENTITY_SIZE_VARY_STR;               rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.ByMonth))         { gui_str = KeyMaker.BY_MONTH_STR;              rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.ByDayOfWeek))     { gui_str = KeyMaker.BY_DAYOFWEEK_STR;          rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.ByDayOfWeekHour)) { gui_str = KeyMaker.BY_DAYOFWEEK_HOUR_STR;     rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.ByHour))          { gui_str = KeyMaker.BY_HOUR_STR;               rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.AutoTime))        { gui_str = KeyMaker.BY_AUTOTIME_STR;           rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.PieSmall))        { gui_str = ENTITY_SIZE_PIE_SMALL_STR;          rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.PieLarge))        { gui_str = ENTITY_SIZE_PIE_LARGE_STR;          rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.XYVisible))       { gui_str = ENTITY_SIZE_XY_VISIBLE_STR;         rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.XYVisibleEqual))  { gui_str = ENTITY_SIZE_XY_VISIBLE_EQUAL_STR;   rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.XYLocal))         { gui_str = ENTITY_SIZE_XY_LOCAL_STR;           rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.XYLocalEqual))    { gui_str = ENTITY_SIZE_XY_LOCAL_EQUAL_STR;     rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.LinkNode))        { gui_str = ENTITY_SIZE_LINKNODE_STR;           rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.GPSSmall))        { gui_str = ENTITY_SIZE_GPS_SMALL_STR;          rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.GPSMedium))       { gui_str = ENTITY_SIZE_GPS_MEDIUM_STR;         rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.GPSLarge))        { gui_str = ENTITY_SIZE_GPS_LARGE_STR;          rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.GPSLocal))        { gui_str = ENTITY_SIZE_GPS_LOCAL_STR;          rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.CountrySmall))    { gui_str = ENTITY_SIZE_COUNTRY_SMALL_STR;      rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.CountryLarge))    { gui_str = ENTITY_SIZE_COUNTRY_LARGE_STR;      rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.ContinuousSmall)) { gui_str = KeyMaker.BY_STRAIGHT_STR + " (Sm)"; rbmis = entity_size_rbmis; }
    if (str.equals("" + EntitySize.ContinuousLarge)) { gui_str = KeyMaker.BY_STRAIGHT_STR + " (Lg)"; rbmis = entity_size_rbmis; }

    if (str.equals("" + EntitySize.TimelineBlocks))        { gui_str = ENTITY_SIZE_TIMELINE_BLOCKS_STR;         rbmis = timeline_entity_size_rbmis; }
    if (str.equals("" + EntitySize.TimelineLarge))         { gui_str = ENTITY_SIZE_TIMELINE_LARGE_STR;          rbmis = timeline_entity_size_rbmis; }
    if (str.equals("" + EntitySize.TimelineSmall))         { gui_str = ENTITY_SIZE_TIMELINE_SMALL_STR;          rbmis = timeline_entity_size_rbmis; }
    if (str.equals("" + EntitySize.TimelineSmallVary))     { gui_str = ENTITY_SIZE_TIMELINE_SMALL_VARY_STR;     rbmis = timeline_entity_size_rbmis; }
    if (str.equals("" + EntitySize.TimelineSmallOverlaps)) { gui_str = ENTITY_SIZE_TIMELINE_SMALL_OVERLAPS_STR; rbmis = timeline_entity_size_rbmis; }

    if (rbmis == null) System.err.println("entitySize(str = \"" + str + "\")");

    for (int i=0;i<rbmis.length;i++) {
      if (rbmis[i].getText().equals(gui_str)) 
        rbmis[i].setSelected(true);
    }
  }

  /**
   * Default color option
   */
  JRadioButtonMenuItem entity_color_default_rbmi,

  /**
   * Vary color option
   */
                       entity_color_vary_rbmi,

  /**
   * Vary color based on the entity label
   */
                       entity_color_label_rbmi;

  /**
   * Increment to the next node color.
   */
  public void nextNodeColor() {
    if      (entity_color_default_rbmi. isSelected()) entity_color_vary_rbmi.    setSelected(true);
    else if (entity_color_vary_rbmi.    isSelected()) entity_color_label_rbmi.   setSelected(true);
    else if (entity_color_label_rbmi.   isSelected()) entity_color_default_rbmi. setSelected(true);
  }

  /**
   * Entity color enumeration
   */
  public enum EntityColor { Default, Vary, Label };

  /**
   * Return the entity color.
   *
   *@return entity color
   */
  public EntityColor entityColor() {
    if      (entity_color_default_rbmi.isSelected()) return EntityColor.Default;
    else if (entity_color_vary_rbmi.   isSelected()) return EntityColor.Vary;
    else if (entity_color_label_rbmi.  isSelected()) return EntityColor.Label;
    else                                             return EntityColor.Default;
  }

  /**
   * Set the entity color.
   *
   *@param str entity color (string of the enumerated value)
   */
  public void entityColor(String str) {
    if      (str.equals("" + EntityColor.Default)) entity_color_default_rbmi.setSelected(true);
    else if (str.equals("" + EntityColor.Vary))    entity_color_vary_rbmi.   setSelected(true);
    else if (str.equals("" + EntityColor.Label))   entity_color_label_rbmi.  setSelected(true);
    else                                           entity_color_default_rbmi.setSelected(true);
  }

  /**
   * Truncate to a single line rbmi
   */
  JRadioButtonMenuItem truncate_single_line_rbmi,

  /**
   * Truncate to four lines rbmi
   */
                       truncate_four_line_rbmi,
  /**
   * No truncation
   */
                       no_truncation_rbmi,
  /**
   * 32 character threshold
   */
                       multiline_32_rbmi,
  /**
   * 64 character threshold
   */
                       multiline_64_rbmi,
  /**
   * 96 character threshold
   */
                       multiline_96_rbmi,
  /**
   * 128 character threshold
   */
                       multiline_128_rbmi;

  /**
   * Return the multiline trunction setting.  A value of -1 means that no truncation should occur.
   *
   *@return multiline trunction setting
   */
  public int multilineTruncate() {
    if      (truncate_single_line_rbmi.isSelected()) return  1;
    else if (truncate_four_line_rbmi.  isSelected()) return  4;
    else                                             return -1;
  }

  /**
   * Set the multiline truncation setting.  A value of -1 means that no truncation should occur.
   *
   *@param multiline truncation setting
   */
  public void multilineTruncate(int trunc) {
    if      (trunc ==  1) truncate_single_line_rbmi.setSelected(true);
    else if (trunc ==  4) truncate_four_line_rbmi.  setSelected(true);
    else if (trunc == -1) no_truncation_rbmi.       setSelected(true);
    else throw new RuntimeException("No truncation for " + trunc + " setting");
  }

  /**
   * Return the multiline threshold.  This is the number of characters per line.
   *
   *@return multiline threshold
   */
  public int multilineThreshold() {
    if      (multiline_32_rbmi.isSelected())  return 32;
    else if (multiline_64_rbmi.isSelected())  return 64;
    else if (multiline_96_rbmi.isSelected())  return 96;
    else if (multiline_128_rbmi.isSelected()) return 128;
    else                                      return 32;
  }

  /**
   * Set the multiline threshold.  This is the number of characters per line.
   *
   *@param thresh multiline threshold
   */
  public void multilineThreshold(int thresh) {
    if      (thresh == 32)  multiline_32_rbmi. setSelected(true);
    else if (thresh == 64)  multiline_64_rbmi. setSelected(true);
    else if (thresh == 96)  multiline_96_rbmi. setSelected(true);
    else if (thresh == 128) multiline_128_rbmi.setSelected(true);
    else throw new RuntimeException("No threshold for " + thresh + " setting");
  }

  /**
   * Calculate the color of an entity based on current settings.  If the entity doesn't exist
   * in the coordinate system, return the multiple color set color.
   *
   *@param entity 
   *
   *@return entity color
   */
  public Color calculateEntityColor(String entity) {
    // Base colors on the current render context
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext());
    if (myrc == null) return RTColorManager.getColor("set", "multi");

    // Leverage the context for the color
    if (myrc.entity_to_sxy.containsKey(entity) == false) return RTColorManager.getColor("set", "multi");
    RTEntitiesComponent.RenderContext.ShapeAndColor sac = myrc.node_shaper.shapeAndColor(myrc.entity_to_sxy.get(entity));
    if (sac == null || sac.color == null) return RTColorManager.getColor("set", "multi");

    return sac.color;
  }

  /**
   * Selection textfield
   */
  JTextField select_tf,

  /**
   * Tagging textfield
   */
             tag_tf;

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "entities"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    StringBuffer sb = new StringBuffer(); sb.append("RTEntitiesPanel");

    sb.append(BundlesDT.DELIM + "showall="            + showAll());
    sb.append(BundlesDT.DELIM + "manuallyadded="      + manuallyAdded());
    sb.append(BundlesDT.DELIM + "markers="            + timeMarkers());
    sb.append(BundlesDT.DELIM + "timeline="           + timelineVisualization());
    sb.append(BundlesDT.DELIM + "entitysize="         + Utils.encToURL("" + entitySize()));
    sb.append(BundlesDT.DELIM + "entitycolor="        + Utils.encToURL("" + entityColor()));
    sb.append(BundlesDT.DELIM + "mltruncate="         + multilineTruncate());
    sb.append(BundlesDT.DELIM + "mlthreshold="        + multilineThreshold());
    sb.append(BundlesDT.DELIM + "entitylabels="       + drawEntityLabels());
    sb.append(BundlesDT.DELIM + "nlabels="            + commaDelimited(Utils.asArray(listEntityLabels())));
    sb.append(BundlesDT.DELIM + "clabels="            + commaDelimited(Utils.asArray(listEntityColor())));

    if (gps_fields_selected) {
      sb.append(BundlesDT.DELIM + "latitudefield="     + Utils.encToURL(selected_latitude_field));
      sb.append(BundlesDT.DELIM + "longitudefield="    + Utils.encToURL(selected_longitude_field));
      if (selected_entity_field != null) sb.append(BundlesDT.DELIM + "gpsselectedentity=" + Utils.encToURL(selected_entity_field));
    }

    return sb.toString();
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTEntitiesPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTEntitiesPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";

      if      (type.equals("showall"))            showAll              (value.toLowerCase().equals("true"));
      else if (type.equals("manuallyadded"))      manuallyAdded        (value.toLowerCase().equals("true"));
      else if (type.equals("markers"))            timeMarkers          (value.toLowerCase().equals("true"));
      else if (type.equals("strictmatches"))      strictMatches        (value.toLowerCase().equals("true"));
      else if (type.equals("entitysize"))         entitySize           (Utils.decFmURL(value));
      else if (type.equals("entitycolor"))        entityColor          (Utils.decFmURL(value));
      else if (type.equals("mltruncate"))         multilineTruncate    (Integer.parseInt(value));
      else if (type.equals("mlthreshold"))        multilineThreshold   (Integer.parseInt(value));
      else if (type.equals("entitylabels"))       drawEntityLabels     (value.toLowerCase().equals("true"));
      else if (type.equals("nlabels"))            { if (!value.equals("")) setJList(entity_label_list, commaDelimited(value)); } 
      else if (type.equals("clabels"))            { if (!value.equals("")) setJList(entity_color_list, commaDelimited(value)); }

      else if (type.equals("latitudefield"))      { selected_latitude_field  = Utils.decFmURL(value); if (selected_longitude_field != null) gps_fields_selected = true; }
      else if (type.equals("longitudefield"))     { selected_longitude_field = Utils.decFmURL(value); if (selected_latitude_field  != null) gps_fields_selected = true; }
      else if (type.equals("gpsselectedentity"))  { selected_entity_field    = Utils.decFmURL(value); } 

      else if (type.equals("timeline"))           {
        if (value.toLowerCase().equals("true")) {
          first_time_timeline = false;
          timelineVisualization(value.toLowerCase().equals("true"));
        }
      }

      else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Has additional configuration options...  Placement of entities... possibley
   * the mapping of bundle to entities if manually added...
   */
  @Override
  public boolean hasAdditionalConfig() { return true; }

  /**
   * Add additional configuration lines... includes extents... add entities options... coordinate positions
   * ...entities... and bundle mappings
   *
   *@param list          where to add the lines to
   *@param visible_only  only save information about the visible components / geometries
   */
  @Override
  public void addAdditionalConfig(List<String> list, boolean visible_only) {
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext());

    // View extents
    list.add("#AC extents" + BundlesDT.DELIM + wx_view_min + BundlesDT.DELIM + wy_view_min + BundlesDT.DELIM + wx_view_max + BundlesDT.DELIM + wy_view_max);

    // Active fields
    for (int i=0;i<entities_added_list.size();i++) {
      String fields[] = entities_added_list.get(i).fields;
      String symbol   = entities_added_list.get(i).symbol_str;
      if (fields.length == 1) { list.add("#AC to_add" + BundlesDT.DELIM + Utils.encToURL(fields[0]) +                                               BundlesDT.DELIM + symbol);
      } else                  { list.add("#AC to_add" + BundlesDT.DELIM + Utils.encToURL(fields[0]) + BundlesDT.DELIM + Utils.encToURL(fields[1]) + BundlesDT.DELIM + symbol); }
    }

    // Coordinates of entities
    Iterator<String> it = entity_to_wxy.keySet().iterator(); while (it.hasNext()) {
      String entity = it.next(); Point2D pt = entity_to_wxy.get(entity);

      // Do not include if not shown and the visible_only flag is set
      if (visible_only && showAll() == false && myrc != null && myrc.entity_counter_context.hasBin(entity) == false) continue;

      list.add("#AC wxy" + BundlesDT.DELIM + Utils.encToURL(entity) + BundlesDT.DELIM + pt.getX() + BundlesDT.DELIM + pt.getY());
    }

    // Sticky labels
    RTEntitiesComponent mycomp = (RTEntitiesComponent) getRTComponent();
    if (mycomp.sticky_labels != null && mycomp.sticky_labels.size() > 0) {
      StringBuffer sb = new StringBuffer();
      sb.append("#AC sticky");
      it = mycomp.sticky_labels.iterator(); while (it.hasNext()) { sb.append("|" + Utils.encToURL(it.next())); }
      list.add(sb.toString());
    }

    // Manually added... if so then need to include the lookups for the bundles to entities...
    if (manuallyAdded()) {
      // Construct the visible set if necessary
      Set<Bundle> visible = null;
      if (visible_only) {
        visible = new HashSet<Bundle>();
        Iterator<Bundle> it_bun = getRTParent().getVisibleBundles().bundleIterator(); while (it_bun.hasNext()) visible.add(it_bun.next());
      }

      // Go through each entity and add the entity lookup information
      it = entity_to_bundles.keySet().iterator(); while (it.hasNext()) {
        String entity = it.next();
        StringBuffer sb = new StringBuffer();
        sb.append("#AC e2b" + BundlesDT.DELIM + Utils.encToURL(entity));

        Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next(); if (visible != null && visible.contains(bundle) == false) continue;
          sb.append(BundlesDT.DELIM + Utils.encToURL(bundle.toString()));
        }

        list.add(sb.toString());
      }
    }

    list.add("#AC entitiesend");
  }

  /**
   * Parse the additional configuration for the views visualization parameters.  Includes the extents,
   * the entity to coordinate looks, and the entity to bundles mapping.
   *
   *@param lines  lines to parse
   *@param line_i start index
   *
   *@return index to resume parsing
   */
  public int parseAdditionalConfig(List<String> lines, int line_i) {
    Map<String,Bundle> str_to_bundle = null;

    boolean finished = false; while (!finished) {
      String line = lines.get(line_i);
      if        (line.startsWith("#AC extents")) {
        StringTokenizer st = new StringTokenizer(line, BundlesDT.DELIM);
        st.nextToken();
        wx_view_min = Double.parseDouble(st.nextToken());
        wy_view_min = Double.parseDouble(st.nextToken());
        wx_view_max = Double.parseDouble(st.nextToken());
        wy_view_max = Double.parseDouble(st.nextToken());
        line_i++;
      } else if (line.startsWith("#AC to_add")) {
        StringTokenizer st = new StringTokenizer(line, BundlesDT.DELIM); st.nextToken();

        String fields[];
        if (st.countTokens() == 3) {
          fields    = new String[2];
          fields[0] = Utils.decFmURL(st.nextToken());
          fields[1] = Utils.decFmURL(st.nextToken());
        } else                     {
          fields    = new String[1];
          fields[0] = Utils.decFmURL(st.nextToken());
        }

        String symbol_str = Utils.decFmURL(st.nextToken());
        entities_added_list.add(new EntitiesAddedStruct(fields, symbol_str));

        line_i++;
      } else if (line.startsWith("#AC wxy")) {
        StringTokenizer st = new StringTokenizer(line, BundlesDT.DELIM);
        st.nextToken();
        String entity = Utils.decFmURL(st.nextToken());
        double x      = Double.parseDouble(st.nextToken());
        double y      = Double.parseDouble(st.nextToken());
        entity_to_wxy.put(entity, new Point2D.Double(x,y));
        line_i++;
      } else if (line.startsWith("#AC sticky")) {
        StringTokenizer st = new StringTokenizer(line, BundlesDT.DELIM);
        st.nextToken();
        while (st.hasMoreTokens()) { ((RTEntitiesComponent) getRTComponent()).sticky_labels.add(Utils.decFmURL(st.nextToken())); }
        line_i++;
      } else if (line.startsWith("#AC e2b")) {
        // Need to create the complete mapping of strings to bundles
        if (str_to_bundle == null) {
          str_to_bundle = new HashMap<String,Bundle>();
          Iterator<Bundle> it = getRTParent().getRootBundles().bundleIterator(); while (it.hasNext()) {
            Bundle bundle = it.next(); String str = bundle.toString();
            str_to_bundle.put(str, bundle);
          }
        }

        // Now line up the written strings to the data structures
        StringTokenizer st = new StringTokenizer(line, BundlesDT.DELIM);
        st.nextToken();
        String entity = Utils.decFmURL(st.nextToken());
        while (st.hasMoreTokens()) {
          String bundle_str = Utils.decFmURL(st.nextToken());
          if (str_to_bundle.containsKey(bundle_str)) {
            Bundle bundle = str_to_bundle.get(bundle_str);

            // Make the geometry lookups
            if (entity_to_wxy.containsKey(entity) == false)     entity_to_wxy.    put(entity, new Point2D.Double(Math.random(), Math.random()));

            // Add the entity to bundle mapping
            if (entity_to_bundles.containsKey(entity) == false) entity_to_bundles.put(entity, new HashSet<Bundle>());
            entity_to_bundles.get(entity).add(bundle);

            // Update the entity times
            updateEntityTimes(entity,bundle);

            // Add the bundle to entities mapping
            if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
            bundle_to_entities.get(bundle).add(entity);
          } else System.err.println("No Bundle Found Matching \"" + bundle_str + "\"");
        }
        line_i++;
      } else if (line.startsWith("#AC entitiesend")) { finished =true; line_i++;
      } else System.err.println("Do Not Understand RTEntities Additional Config Line \"" + line + "\" @ line number " + (line_i++));
    }

    newBundlesRoot(getRTParent().getRootBundles());

    return line_i;
  }

  // ==============================================================================================

  /**
   * Set the new bundles root.  Update the data structures based on the new root.  Ensure
   * that the references in the existing data structures still exist.
   *
   *@param new_root new root
   */
  @Override
  public void newBundlesRoot(Bundles new_root) {
    saveState(SaveState.CROSS_REFS);

    // Add the active entities (unless the mapping/structures are dirty)
    if (manuallyAdded() == false) {
      for (int i=0;i<entities_added_list.size();i++) {
        String fields[] = entities_added_list.get(i).fields;
        String symbol   = entities_added_list.get(i).symbol_str;
        addEntities(fields, symbol, false);
      }
    }

    // Determine if we have any bundles that don't exist in the root anymore...
    // - Keep track of the affected entities
    Set<String> affected_entities = new HashSet<String>();

    // - Get the list of existing bundles
    Set<Bundle> bundle_set = new_root.bundleSet();

    // - Keep track of which bundles to remove -- avoid an illegal state exception
    Set<Bundle> bundles_to_remove = new HashSet<Bundle>();

    // - Remove across the visualization structures... and determine affected entities
    Iterator<Bundle> it_bun = bundle_to_entities.keySet().iterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next();
      if (bundle_set.contains(bundle) == false) { // No longer in the application
        bundles_to_remove.add(bundle);
        Iterator<String> it_ent = bundle_to_entities.get(bundle).iterator();
        while (it_ent.hasNext()) {
          String entity = it_ent.next();
          entity_to_bundles.get(entity).remove(bundle);
          if (entity_to_bundles.get(entity).size() == 0) affected_entities.addAll(bundle_to_entities.get(bundle));
        }
      }
    }

    // Determine if any of the entities point to zero bundles/records... remove if so... probabaly
    // don't really need this second check since we checked earlier...
    Iterator<String> it_ent = affected_entities.iterator(); while (it_ent.hasNext()) {
      String entity = it_ent.next();
      if (entity_to_bundles.get(entity).size() == 0) permanentlyRemoveEntityFromVisualization(entity);
    }

    // Remove all the bundle references
    it_bun = bundles_to_remove.iterator(); while (it_bun.hasNext()) {
      Bundle bundle = it_bun.next();
      bundle_to_entities.remove(bundle);
    }

    // Re-render
    getRTComponent().render();
  }

  /**
   * Undo options
   */
  enum SaveState { POSITIONS, CROSS_REFS };

  /**
   * Save the current state of the application based on the to_save specification.
   *
   *@param to_save state to save
   */
  private void saveState(SaveState to_save) {
    if (disable_undo_cbmi.isSelected() == false) {
      SaveStateStruct  save = new SaveStateStruct();
      Iterator<String> it   = null;

      switch (to_save) {
        case CROSS_REFS:   save.b_to_e     = new HashMap<Bundle,Set<String>>();
                           save.e_to_b     = new HashMap<String,Set<Bundle>>();
                           save.e_to_shape = new HashMap<String,String>();
                           it = entity_to_bundles.keySet().iterator(); while (it.hasNext()) {
                             String e = it.next();
                             save.e_to_b.put(e,new HashSet<Bundle>());
                             save.e_to_b.get(e).addAll(entity_to_bundles.get(e));
                             if (entity_to_shape.containsKey(e)) save.e_to_shape.put(e, entity_to_shape.get(e));
                           }
                           Iterator<Bundle> it_b = bundle_to_entities.keySet().iterator(); while (it_b.hasNext()) {
                             Bundle bundle = it_b.next();
                             save.b_to_e.put(bundle, new HashSet<String>());
                             save.b_to_e.get(bundle).addAll(bundle_to_entities.get(bundle));
                           }
        case POSITIONS:    it = entity_to_wxy.keySet().iterator();
                           while (it.hasNext()) {
                             String entity = it.next(); if (entity_to_wxy.containsKey(entity)) {
                               save.e_to_wxy.put(entity, entity_to_wxy.get(entity));
                             }
                           }
      }

      // Save it... could be a list if we wanted multiple undos...
      last_save = save;
    }
  }

  /**
   * Last save state... this will be the undo
   */
  SaveStateStruct last_save = null;

  /**
   * Structure to save the current state -- should mostly match what is listed below...
   * ... by default, the entity_to_wxy will always have elements...
   */
  class SaveStateStruct {
    Map<Bundle,Set<String>> b_to_e     = null;
    Map<String,Set<Bundle>> e_to_b     = null;
    Map<String,Point2D>     e_to_wxy   = new HashMap<String,Point2D>();
    Map<String,String>      e_to_shape = null;
  }

  /**
   * Restore from the last save state.  Swap the restore with the current so that the
   * user can toggle back and forth.
   */
  private void restoreFromSaveState() {
    SaveStateStruct restore_from = last_save; if (restore_from != null) {
      SaveStateStruct swap = new SaveStateStruct();
      swap.b_to_e     = bundle_to_entities;
      swap.e_to_b     = entity_to_bundles;
      swap.e_to_wxy   = entity_to_wxy;
      swap.e_to_shape = entity_to_shape;

      if (restore_from.e_to_b == null) {
        if (restore_from.e_to_wxy.keySet().size() == entity_to_wxy.keySet().size()) entity_to_wxy = restore_from.e_to_wxy; else {
          Iterator<String> it = restore_from.e_to_wxy.keySet().iterator(); while (it.hasNext()) {
            String entity = it.next(); entity_to_wxy.put(entity, restore_from.e_to_wxy.get(entity));
          }
        }
      } else {
        bundle_to_entities = restore_from.b_to_e;
        entity_to_bundles  = restore_from.e_to_b;
        entity_to_wxy      = restore_from.e_to_wxy;
        entity_to_shape    = restore_from.e_to_shape;

        recalculateAllEntityTimes();
      }

      last_save = swap;

      getRTComponent().render();
    }
  }

  // ==============================================================================================
  // BELOW LINE -- COMPLETE STATE OF VISUALIZATION SYSTEM

  /**
   * Bundle to their related entities
   */
  private Map<Bundle,Set<String>> bundle_to_entities = new HashMap<Bundle,Set<String>>();

  /**
   * Entity to their related bundles
   */
  private Map<String,Set<Bundle>> entity_to_bundles = new HashMap<String,Set<Bundle>>();

  /**
   * Entity to first heard
   */
  private Map<String,Long> entity_first_heard = new HashMap<String,Long>(),

  /**
   * Entity to last heard
   */
                           entity_last_heard  = new HashMap<String,Long>();

  /**
   * Entity to their world coordinates
   */
  private Map<String,Point2D> entity_to_wxy = new HashMap<String,Point2D>();

  /**
   * Entity to shape
   */
  private Map<String,String> entity_to_shape = new HashMap<String,String>();

  // ABOVE LINE -- COMPLETE STATE OF VISUALIZATION SYSTEM
  // ==============================================================================================

  /**
   * UI functionality for merging nicely -- i.e., always keep the user enterred names when
   * merging if possible...
   */
  private Set<String> user_enterred_entities = new HashSet<String>();

  /**
   * Either single fields... or two fields (separated by a DELIM)... active within the view...
   * ... if the list is filled in and bundles are added, the will automatically get added to the
   * correct entity.
   */
  private List<String> active_fields = new ArrayList<String>();

  /**
   * Field to shape
   */
  private Map<String,String> field_to_shape = new HashMap<String,String>();

  // ==============================================================================================
  
  /**
   * Backup location for undo operation.
   */
  private Map<String,Point2D> backups = new HashMap<String,Point2D>();

  /**
   * Save the selected set to to a backupp array.  Used for the XY Dialog... not for general purpose usage.
   *
   *@param layout             layout lookups for entities -- usually entity_to_wxy...
   *@param entities_to_save   list of entities that save -- usually entity_to_wxy.keySet()...
   */
  protected void saveLayoutForUndo(Map<String,Point2D> layout, Set<String> entities_to_save) {
    backups.clear();
    Iterator<String> it = entities_to_save.iterator(); while (it.hasNext()) {
      String  entity = it.next(); if (layout.containsKey(entity)) {
        Point2D pt     = layout.get(entity);
        backups.put(entity,pt);
      }
    }  
  }

  /**
   * Apply the backup layout to the visualizations current layout.
   */
  protected void undoLayoutChanges() {
    Iterator<String> it = backups.keySet().iterator(); while (it.hasNext()) {
      String entity = it.next();
      entity_to_wxy.put(entity, backups.get(entity));
    }
  }

  /**
   * Convert a world x coordinate to the screen coordinate
   *
   *@param wx world x-coordinate
   *
   *@return screen x-coordinate
   */
  public int wxToSx(double wx) { return (int) (getRTComponent().getWidth()  * (wx - wx_view_min) / (wx_view_max - wx_view_min)); }

  /**
   * Convert a world y coordinate to the screen coordinate
   *
   *@param wy world y-coordinate
   *
   *@return screen y-coordinate
   */
  public int wyToSy(double wy) { return (int) (getRTComponent().getHeight() * (wy - wy_view_min) / (wy_view_max - wy_view_min)); }

  /**
   *  Convert a screen x coordinate to the world x coordinate
   *
   *@param sx screen x-coordinate
   *
   *@return world x-coordinate
   */
  public double sxToWx(int sx) { return ((sx * (wx_view_max - wx_view_min)) / getRTComponent().getWidth())  + wx_view_min; }

  /**
   * Convert a screen y coordinate to the world y coordinate
   *
   *@param sy screen y-coordinate
   *
   *@return world y-coordinate
   */
  public double syToWy(int sy) { return ((sy * (wy_view_max - wy_view_min)) / getRTComponent().getHeight()) + wy_view_min; }

  /**
   * Find an entity name not currently in use.
   *
   *@param base base name
   *
   *@return deconflicted entity name
   */
  private String deconflictName(String base) {
    String new_name = base;
    while (entity_to_bundles.containsKey(new_name)) {
      new_name = base + "_[" + (((int) (Math.random() * Integer.MAX_VALUE))%100000) + "]";
    }
    return new_name;
  }

  /**
   * Break the selected entities into their corresponding bundles.
   */
  public void breakSelectedEntitiesIntoBundles() {
    saveState(SaveState.CROSS_REFS);

    // Get the selected entities and then limit down to the ones handled by this visualization
    Set<String> sel = getRTParent().getSelectedEntities(); sel.retainAll(entity_to_wxy.keySet());

    // Get the visible records...  let's constrain there (feels correct)
    Bundles visible = getRTParent().getVisibleBundles(); Set<Bundle> visible_bundles = visible.bundleSet();

    Set<String> entities_to_check = new HashSet<String>(); // Entities to check at end to see if they have any records left or not
    if (sel.size() > 0) {
      Iterator<String> ite = sel.iterator(); while (ite.hasNext()) {
        String entity = ite.next(); 

        // Don't split entities that are a single record
        if (entity_to_bundles.get(entity).size() == 1) continue;

        // Rewire
        Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
          Bundle bundle = itb.next(); if (visible_bundles.contains(bundle)) {

            // Remove from existing data structure
            itb.remove(); bundle_to_entities.get(bundle).remove(entity);
            entities_to_check.add(entity);

            // Set the dirty flag
            manuallyAdded(true);

            // Make a new name
            String bundle_str = deconflictName(entity);

            // Create the new mappings
            entity_to_bundles.put(bundle_str, new HashSet<Bundle>()); entity_to_bundles.get(bundle_str).add(bundle);
            if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
            bundle_to_entities.get(bundle).add(bundle_str);
            updateEntityTimes(bundle_str, bundle);

            // New coordinates
            entity_to_wxy.put(bundle_str, new Point2D.Double(entity_to_wxy.get(entity).getX(),entity_to_wxy.get(entity).getY()));
            if (timelineVisualization()) adjustXCoordinateForTimeline(bundle_str);
          }
        }
      }
    }

    // For the entities that were modified, make sure they have records... and recalculate first/last heards
    Iterator<String> ite = entities_to_check.iterator(); while (ite.hasNext()) {
      String entity = ite.next();

      // If there are any empty bundles set, remove them from the view permanently
      if (entity_to_bundles.get(entity).size() == 0) { permanentlyRemoveEntityFromVisualization(entity);

      // Else there are non-empty parts -- recalculate first/last heards
      } else                                         { updateEntityTimes(entity); }
    }

    // Force a transform & render
    getRTComponent().render();
  }

  /**
   * Merge the selected entities.
   */
  public void mergeSelectedEntities() {
    saveState(SaveState.CROSS_REFS);

    // Get the selected entities and then limit down to the ones handled by this visualization
    Set<String> sel = getRTParent().getSelectedEntities(); sel.retainAll(entity_to_wxy.keySet());

    // Get the visible records...  let's constrain there (feels correct)
    Bundles visible = getRTParent().getVisibleBundles(); Set<Bundle> visible_bundles = visible.bundleSet();

    // As long as something is left... calculate what to merge and update the mappings
    Set<Bundle> to_merge = new HashSet<Bundle>(); double wx = 0.0, wy = 0.0;
    Set<String> entities_to_check = new HashSet<String>(), // Entities to check for removal
                user_enters       = new HashSet<String>(); // Any entities that the user enterred manually
    if (sel.size() > 0) {
      Iterator<String> ite = sel.iterator(); while (ite.hasNext()) {
        String entity = ite.next(); if (user_enterred_entities.contains(entity)) user_enters.add(entity);
        Iterator<Bundle> itb = entity_to_bundles.get(entity).iterator(); while (itb.hasNext()) {
          Bundle bundle = itb.next(); if (visible_bundles.contains(bundle)) {
            to_merge.add(bundle); itb.remove(); bundle_to_entities.get(bundle).remove(entity);
            wx = entity_to_wxy.get(entity).getX(); wy = entity_to_wxy.get(entity).getY();
            entities_to_check.add(entity);
          }
        }
      }

      // If there are records to merge, make the new entity
      if (to_merge.size() > 0) {
        manuallyAdded(true);

        // New entity name ... if there was a user enterred name, use that one... if there are multiple
        // user names, choose the one with the most existing records
        String merged_str;
        if        (user_enters.size() == 1) { merged_str = user_enters.iterator().next();
        } else if (user_enters.size() >  1) {
          int max; String max_str;
          Iterator<String> it = user_enters.iterator(); max_str = it.next(); max = entity_to_bundles.get(max_str).size();
          while (it.hasNext()){
            String str = it.next(); if (max < entity_to_bundles.get(str).size()) {
              max_str = str; max = entity_to_bundles.get(max_str).size();
            }
          }
          merged_str = max_str;
        } else {
          int merged_id = 0; while (entity_to_bundles.containsKey("Merged " + merged_id)) merged_id++;
          merged_str = "Merged " + merged_id;
        }

        // Create the new mappings
        if (entity_to_bundles.containsKey(merged_str) == false) entity_to_bundles.put(merged_str, new HashSet<Bundle>()); 
        entity_to_bundles.get(merged_str).addAll(to_merge);
        Iterator<Bundle> itb = to_merge.iterator(); while (itb.hasNext()) {
          Bundle bundle = itb.next(); 
          if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
          bundle_to_entities.get(bundle).add(merged_str);
        }
        updateEntityTimes(merged_str);

        // For the entities that were modified, make sure they have records... and recalculate first/last heards
        ite = entities_to_check.iterator(); while (ite.hasNext()) {
          String entity = ite.next();

          // If there are any empty bundles set, remove them from the view permanently
          if (entity_to_bundles.get(entity).size() == 0) { permanentlyRemoveEntityFromVisualization(entity);

          // Else there are non-empty parts -- recalculate first/last heards
          } else                                         { updateEntityTimes(entity); }
        }

        // New coordinates
        if (entity_to_wxy.containsKey(merged_str) == false) 
          entity_to_wxy.put(merged_str, new Point2D.Double(sxToWx(getRTComponent().mx), syToWy(getRTComponent().my)));
        if (timelineVisualization()) adjustXCoordinateForTimeline(merged_str);

      }
    }

    // Force a transform & render
    getRTComponent().render();
  }

  /**
   * Simple interface for the distance metric
   */
  interface DistanceMetric { 
    public double  distance(String s0, String s1); 
    public boolean longerIsMoreSimilar();
  }

  /**
   * Edit Distance Wrapper
   */
  class EditDistanceMetric implements DistanceMetric {
    Map<String,Map<String,Double>> cache = new HashMap<String,Map<String,Double>>();
    public double distance(String s0, String s1) {
      if (cache.containsKey(s0) && cache.get(s0).containsKey(s1)) return cache.get(s0).get(s1);
      if (cache.containsKey(s1) && cache.get(s1).containsKey(s0)) return cache.get(s1).get(s0);

      double d = (new LevenshteinStateful(s0,s1)).editDistance();

      if (cache.containsKey(s0) == false) cache.put(s0, new HashMap<String,Double>());
      cache.get(s0).put(s1, d);
      return d;
    }
    public boolean longerIsMoreSimilar() { return false; }
  }

  /**
   * LCS Wrapper
   */
  class LongestCommonSubstringDistanceMetric implements DistanceMetric {
    Map<String,Map<String,Double>> cache = new HashMap<String,Map<String,Double>>();
    public double distance(String s0, String s1) {
      if (cache.containsKey(s0) && cache.get(s0).containsKey(s1)) return cache.get(s0).get(s1);
      if (cache.containsKey(s1) && cache.get(s1).containsKey(s0)) return cache.get(s1).get(s0);

      double d = (Utils.longestCommonSubstring(s0,s1)).length();

      if (cache.containsKey(s0) == false) cache.put(s0, new HashMap<String,Double>());
      cache.get(s0).put(s1, d);
      return d;
    }
    public boolean longerIsMoreSimilar() { return true; }
  }

  /**
   * Return a distance metric that represents the user's selected choice.
   *
   *@return distance metric selected by user
   */
  public DistanceMetric distanceMetric() {
    if      (distance_metrics_rbmi[DM_EDIT_DISTANCE].           isSelected()) return new EditDistanceMetric();
    else if (distance_metrics_rbmi[DM_LONGEST_COMMON_SUBSTRING].isSelected()) return new LongestCommonSubstringDistanceMetric();
    else if (distance_metrics_rbmi[DM_CUSTOM].                  isSelected()) throw  new RuntimeException("Cannot Use Custom Distance Metric");
    else                                                                      return new EditDistanceMetric();
  }

  /**
   * Perform the umap on the distance metric.
   */
  public void umapDistanceMetric() {
    // Get the distance metric
    DistanceMetric dm  = distanceMetric();

    // Get what to apply it to
    Set<String>    set = visualizedSelectionOrAll(); List<String> order = new ArrayList<String>(); order.addAll(set);

    // Construct the matrix... find the max and min at the same time
    float dmat_min = Float.POSITIVE_INFINITY, dmat_max = Float.NEGATIVE_INFINITY;
    float dmat[][] = new float[order.size()][order.size()];
    for (int i=0;i<order.size();i++) {
      for (int j=0;j<order.size();j++) {
        dmat[i][j] = (float) dm.distance(order.get(i),order.get(j));
        if (dmat[i][j] < dmat_min) dmat_min = dmat[i][j];
        if (dmat[i][j] > dmat_max) dmat_max = dmat[i][j];
      }
    }

    // Normalize 0...1 ... make sure the min and max are sane
    // ... 0.0 should be very similar strings... 1.0 should be very different strings
    if (Float.isInfinite(dmat_min)) { dmat_min = 0.0f; dmat_max = 1.0f; }
    if (dmat_min == dmat_max) dmat_max = dmat_min + 1.0f;

    for (int i=0;i<dmat.length;i++) { for (int j=0;j<dmat.length;j++) {
      if (dm.longerIsMoreSimilar()) {
        dmat[i][j] = 1.0f - (dmat[i][j] - dmat_min)/(dmat_max - dmat_min);
      } else {
        dmat[i][j] = (dmat[i][j] - dmat_min)/(dmat_max - dmat_min);
    } } }

    // Run umap
    Umap umap = new Umap();
    umap.setNumberComponents(2);
    umap.setNumberNearestNeighbours(15);
    umap.setThreads(8);
    float result[][] = umap.fitTransform(dmat);

    saveState(SaveState.POSITIONS);

    // Apply to the coordinates
    for (int i=0;i<result.length;i++) {
      entity_to_wxy.put(order.get(i), new Point2D.Double(result[i][0], result[i][1]));
      if (timelineVisualization()) adjustXCoordinateForTimeline(order.get(i));
    }

    // Force a render
    zoomToFit(); getRTComponent().render();
  }

  /**
   * Return either the current selection intersected with the visualized entities or all of the entities configured
   * for the view.
   *
   *@return visualized selection or all entities
   */
  protected Set<String> visualizedSelectionOrAll() {
    Set<String> set = new HashSet<String>();
    // Requires a render context...
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) {
      set.addAll(entity_to_wxy.keySet());
    } else {
      // Determine the selection... if no selection, then return all of the entities
      Set<String> sel = getRTParent().getSelectedEntities();
      if (sel != null && sel.size() > 0) {
        if (showAll()) {
          Iterator<String> it = sel.iterator(); while (it.hasNext()) { String ent = it.next(); if (entity_to_wxy.     containsKey(ent)) set.add(ent); }
        } else {
          Iterator<String> it = sel.iterator(); while (it.hasNext()) { String ent = it.next(); if (myrc.entity_to_sxy.containsKey(ent)) set.add(ent); }
        }
      } else { set.addAll(entity_to_wxy.keySet()); }
    }
    return set;
  }

  /**
   * Show a dialog to select which field(s) to use to rename the entities.
   */
  public void renameEntitiesBasedOnFields() { 
    // Determine which to apply to
    Set<String> sel = visualizedSelectionOrAll();

    // Find the parent container
    Container container = getParent(); while (container instanceof Frame == false) container = container.getParent();

    // Create the dialog
    new RenameEntitiesDialog((Frame) container, sel); 
  } 

  /**
   * Provide dialog to rename the entities based on their record fields.
   */
  class RenameEntitiesDialog extends JDialog {
    private static final long serialVersionUID = 262462721392311265L;

    /**
     * Primary field (required)
     */
    JComboBox<String> primary_cb,

    /**
     * Secondary (Optional) field to add
     */
                      secondary_cb,

    /**
     * Shape of the entity
     */
                      shape_cb;

    /**
     * Entities to rename
     */
    Set<String>       selected;

    /**
     * Construct the gui configuration
     *
     *@param visible_only_flag user interface setting for the visible only option
     */
    public RenameEntitiesDialog(Frame owner, Set<String> selected) {
      super(owner, "Rename Entities", true); this.selected = selected;
      JPanel sub_center = new JPanel(new GridLayout(1,2,5,5));
      JPanel pri    = new JPanel(new BorderLayout());
      pri.add("North",   new JLabel("Primary"));
      pri.add("Center",  primary_cb = new JComboBox<String>(RTGraphPanel.nodeBlanks(getRTParent().getRootBundles().getGlobals())));
      sub_center.add(pri);

      JPanel sec    = new JPanel(new BorderLayout());
      sec.add("North",   new JLabel("Secondary (Optional)"));
      sec.add("Center",  secondary_cb = new JComboBox<String>(Utils.prepend(BundlesDT.NOTSET, RTGraphPanel.nodeBlanks(getRTParent().getRootBundles().getGlobals()))));
      sub_center.add(sec);

      JPanel center = new JPanel(new BorderLayout());
      center.add("Center", sub_center);

      JPanel shape = new JPanel(new FlowLayout());
      shape.add(new JLabel("Entity Shape"));
      shape.add(shape_cb = new JComboBox<String>(Utils.SHAPE_STRS));
      
      center.add("South", shape);

      getContentPane().add("Center", center);

      JPanel buttons = new JPanel(new FlowLayout()); JButton bt;
      buttons.add(bt = new JButton("Rename"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          renameEntitiesAction();
        } } );
      buttons.add(bt = new JButton("Cancel"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          closeDialog();
        } } );
      
      getContentPane().add("South", buttons);

      // Pack... set the location... and display
      pack(); setLocationRelativeTo(null); setVisible(true);
    }

    /**
     * Add the entities and then close the dialog
     */
    private void renameEntitiesAction() {
      saveState(SaveState.CROSS_REFS);

      // Figure out the fields to use
      String fields[];

      if (((String) secondary_cb.getSelectedItem()).equals(BundlesDT.NOTSET) == false) { 
        fields = new String[2];
        fields[1] = ((String) secondary_cb.getSelectedItem()); 
      } else fields = new String[1];

      fields[0] = (String) primary_cb.getSelectedItem();

      // Key maker lookup to create possible entity names...
      Map<Tablet,EntitiesKeyMaker> ekm_lu = new HashMap<Tablet,EntitiesKeyMaker>();

      // Iterate over the specified entities
      Iterator<String> it_ent = selected.iterator(); while (it_ent.hasNext()) {
        String entity = it_ent.next();

        // Counter for what occurs within the entity fields...
        Map<String,Integer> key_counts = new HashMap<String,Integer>();

        // Go through the records and apply the fields
        Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          if (ekm_lu.containsKey(bundle.getTablet()) == false) ekm_lu.put(bundle.getTablet(), new EntitiesKeyMaker(bundle.getTablet(), fields));
          if (ekm_lu.get(bundle.getTablet()).isValid()) {
            String keys[] = ekm_lu.get(bundle.getTablet()).keys(bundle); if (keys != null && keys.length > 0) {
              for (int i=0;i<keys.length;i++) {
                if (key_counts.containsKey(keys[i]) == false) key_counts.put(keys[i], 1);
                else                                          key_counts.put(keys[i], key_counts.get(keys[i]) + 1);
              }
            }
          }
        }

        //
        // Now the ambiguous part...
        //
        // Easiest case is just a single name...
        //
        if        (key_counts.keySet().size() >= 1) {
          String new_name;

          // Single entity name is easy... for multiples, choose the one with the highest count
          if (key_counts.keySet().size() == 1) new_name = key_counts.keySet().iterator().next(); else {
            Iterator<String> it = key_counts.keySet().iterator();
            String name = it.next(); int name_count = key_counts.get(name);
            while (it.hasNext()) { String str = it.next(); if (key_counts.get(str) > name_count) { name = str; name_count = key_counts.get(str); } }
            new_name = name;
          }

          // Deconflict the name
          new_name = deconflictName(new_name);

          // Fill in the structure
          entity_to_wxy.    put(new_name, entity_to_wxy.get(entity));
          entity_to_bundles.put(new_name, new HashSet<Bundle>());      
            entity_to_bundles.get(new_name).addAll(entity_to_bundles.get(entity));
          if (entity_first_heard.containsKey(entity)) entity_first_heard.put(new_name, entity_first_heard.get(entity));
          if (entity_last_heard. containsKey(entity)) entity_last_heard. put(new_name, entity_last_heard. get(entity));
          entity_to_shape.put(new_name, (String) shape_cb.getSelectedItem());
          it_bun = entity_to_bundles.get(new_name).iterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next();
            if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
            bundle_to_entities.get(bundle).add(new_name);
          }

          permanentlyRemoveEntityFromVisualization(entity);

        } else { /* do nothing... what would we do?  get rid of the entity?  map to null? */ }
      }

      // Mark everything as dirty
      manuallyAdded(true);

      // Force a re-render
      getRTComponent().render();

      // Close the dialog
      closeDialog();
    }

    /**
     * Close the dialog
     */
    private void closeDialog() { setVisible(false); dispose(); }
  }

  /**
   * Add an entity (possibly two fields...) for the visible records.  Based on previous fields (schemas).  If there are 
   * no previous fields (schemas), should bring up the dialog to show the additional of fields.
   */
  public void addEntitiesPerSchemasForVisibleBundles() {
    //
    // Show Add Entities Dialog...
    //
    if (entities_added_list.size() == 0) {
      new SetEntitiesDialog(true);

    //
    // Otherwise, add based on the current entities_added_list schema...
    //
    } else {
      for (int i=0;i<entities_added_list.size();i++) {
        String fields[] = entities_added_list.get(i).fields;
        String symbol   = entities_added_list.get(i).symbol_str;
        addEntities(fields, symbol, true);
      }
    }
  }

  /**
   * Add an entity (possibly two fields...).... based on the usage of sets, it should be safe to call
   * this multiple times without duplicative entries being made...  Probably will need to be called
   * if there's a new bundle root set with a clear on the current items...
   *
   *@param fields        either one or two element array representing fields to pull entities from
   *@param shape         shape string for the entity
   *@param visible_only  only pull from visible entities
   *
   */
  public void addEntities(String fields[], String shape, boolean visible_only) {
    saveState(SaveState.CROSS_REFS);

    Iterator<Tablet> it_tab = null;

    // Check to see if it's visible only... and if that makes sense ... and mark it dirty
    if (visible_only && getRTParent().getVisibleBundles() != getRTParent().getRootBundles()) {
      it_tab = getRTParent().getVisibleBundles().tabletIterator(); 
      manuallyAdded(true);
    }

    // Just set it to the root bundles if nothing was set
    if (it_tab == null) it_tab = getRTParent().getRootBundles().tabletIterator(); 

    // For each tablet
    while (it_tab.hasNext()) {
      // If tablet is valid...
      Tablet tablet = it_tab.next(); EntitiesKeyMaker maker = new EntitiesKeyMaker(tablet, fields); if (maker.isValid()) {
        // Go through each record;
        Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          String keys[] = maker.keys(bundle); if (keys != null && keys.length > 0) {
            for (int i=0;i<keys.length;i++) {
              String key = keys[i];

              // Bundle to entity correlation
              if (bundle_to_entities.containsKey(bundle) == false) bundle_to_entities.put(bundle, new HashSet<String>());
              bundle_to_entities.get(bundle).add(key);

              // Entity to bundle correlation
              if (entity_to_bundles.containsKey(key) == false) entity_to_bundles.put(key, new HashSet<Bundle>());
              entity_to_bundles.get(key).add(bundle);

              // Update the entity times
              updateEntityTimes(key, bundle);

              // Shape lookup
              entity_to_shape.put(key,shape);

              // Point lookup
              if (entity_to_wxy.containsKey(key) == false) entity_to_wxy.put(key, new Point2D.Double(Math.random(),Math.random()));
            }
          }
        }
      }
    }

    // Enforce timeline positioning if enabled
    if (timelineVisualization()) placeNodesForTimeline();

    // Force a re-render
    zoomToFit();
    getRTComponent().render();
  }

  /**
   * Abstraction to handle converting bundles into their related entities.  Because of the possibility of
   * a concatenated entity, it's easier to have the abstraction implemented to handle the complexity.
   */
  class EntitiesKeyMaker {
    KeyMaker pri_km, 
             sec_km;

    // Constructor
    public EntitiesKeyMaker(Tablet tablet, String fields[]) {
      if (                     KeyMaker.tabletCompletesBlank(tablet, fields[0])) pri_km = new KeyMaker(tablet, fields[0]);
      if (fields.length > 1 && KeyMaker.tabletCompletesBlank(tablet, fields[1])) sec_km = new KeyMaker(tablet, fields[1]);
    }

    // Make the keys for a bundle
    public String[] keys(Bundle bundle) {
      String pris[] = removePipes(pri_km.stringKeys(bundle));

      //
      // As a first cut, let's get rid of empty or notset fields...
      //
      if (pris == null || pris.length == 0 || pris[0] == null || pris[0].equals(BundlesDT.NOTSET)) return new String[0];

      //
      // Handle two paths -- should really be encoding these... we'll do the opposite and remove pipes
      //
      if (sec_km != null) {
        String secs[] = removePipes(sec_km.stringKeys(bundle));
        if (secs.length > 0 && secs[0].equals(BundlesDT.NOTSET) == false) {
          String ret[] = new String[pris.length * secs.length];
          int i = 0;
          for (int j=0;j<pris.length;j++) { for (int k=0;k<secs.length;k++) { ret[i++] = pris[j] + BundlesDT.DELIM + secs[k]; } }
          return ret;
        } else {
          return pris;
        }
      } else {
        return pris;
      }
    }

    // Is the key maker valid?
    public boolean isValid() { return pri_km != null; }

    // Remove any pipes from the keys...
    public String[] removePipes(String strs[]) {
      if (strs        == null) return null; 
      if (strs.length == 0)    return strs;
      String mod[] = new String[strs.length]; 
      for (int i=0;i<mod.length;i++) {
        if (strs[i].indexOf(BundlesDT.DELIM) >= 0) {
          StringBuffer sb = new StringBuffer();
          for (int j=0;j<strs[i].length();j++) {
            char c = strs[i].charAt(j);
            if (c == BundlesDT.DELIM.charAt(0)) sb.append(" "); else sb.append(c);
          }
          mod[i] = sb.toString();
        } else mod[i] = strs[i];
      }
      return mod;
    }
  }

  /**
   * Provide dialog to set the active entities
   */
  class SetEntitiesDialog extends JDialog {
    private static final long serialVersionUID = 262462754332311261L;

    /**
     * Primary field (required)
     */
    JComboBox<String> primary_cb,

    /**
     * Secondary (Optional) field to add
     */
                      secondary_cb,

    /**
     * Shape of the entity
     */
                      shape_cb;

    /**
     * Add just visible records...
     */
    JCheckBox         visible_only_cb;

    /**
     * Construct the gui configuration
     *
     *@param visible_only_flag user interface setting for the visible only option
     */
    public SetEntitiesDialog(boolean visible_only_flag) {
      super(getRTParent(), "Add Entities", true);
      JPanel sub_center = new JPanel(new GridLayout(1,2,5,5));
      JPanel pri    = new JPanel(new BorderLayout());
      pri.add("North",   new JLabel("Primary"));
      pri.add("Center",  primary_cb = new JComboBox<String>(RTGraphPanel.nodeBlanks(getRTParent().getRootBundles().getGlobals())));
      sub_center.add(pri);

      JPanel sec    = new JPanel(new BorderLayout());
      sec.add("North",   new JLabel("Secondary (Optional)"));
      sec.add("Center",  secondary_cb = new JComboBox<String>(Utils.prepend(BundlesDT.NOTSET, RTGraphPanel.nodeBlanks(getRTParent().getRootBundles().getGlobals()))));
      sub_center.add(sec);

      JPanel center = new JPanel(new BorderLayout());
      center.add("Center", sub_center);

      JPanel shape = new JPanel(new FlowLayout());
      shape.add(new JLabel("Entity Shape"));
      shape.add(shape_cb = new JComboBox<String>(Utils.SHAPE_STRS));
      shape.add(visible_only_cb = new JCheckBox("Visible Only", visible_only_flag));
      
      center.add("South", shape);

      getContentPane().add("Center", center);

      JPanel buttons = new JPanel(new FlowLayout()); JButton bt;
      buttons.add(bt = new JButton("Add"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          addEntitiesAction();
        } } );
      buttons.add(bt = new JButton("Cancel"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          closeDialog();
        } } );
      
      getContentPane().add("South", buttons);

      // Pack... set the location... and display
      pack(); setLocationRelativeTo(null); setVisible(true);
    }

    /**
     * Add the entities and then close the dialog
     */
    private void addEntitiesAction() {
      String fields[];

      if (((String) secondary_cb.getSelectedItem()).equals(BundlesDT.NOTSET) == false) { 
        fields = new String[2];
        fields[1] = ((String) secondary_cb.getSelectedItem()); 
      } else fields = new String[1];

      fields[0] = (String) primary_cb.getSelectedItem();

      addEntities(fields, (String) shape_cb.getSelectedItem(), visible_only_cb.isSelected());

      // Save the setting... for new bundles added... and for saves/restores...
      saveEntitiesAddedSetting(fields, (String) shape_cb.getSelectedItem());

      closeDialog();
    }

    /**
     * Close the dialog
     */
    private void closeDialog() { setVisible(false); dispose(); }
  }

  /**
   * Save the entities added setting.  This is needed if any new records are added... and
   * also needed for saving and restoring the configuration.  Note that if records have
   * been manually added... stuff breaks.  Haven't figured out the interactions (yet).
   *
   *@param fields      fields for the schema
   *@param symbol_str  symbol to be used for the newly created entities
   */
  private void saveEntitiesAddedSetting(String fields[], String symbol_str) {
    entities_added_list.add(new EntitiesAddedStruct(fields, symbol_str));
  }

  /**
   * Entities Added Structure
   */
  class EntitiesAddedStruct {
    String fields[];  String symbol_str;
    public EntitiesAddedStruct(String fields[], String symbol_str) {
      this.fields = fields; this.symbol_str = symbol_str;
    }
    public EntitiesAddedStruct(String encoded) {
      StringTokenizer st = new StringTokenizer(encoded, BundlesDT.DELIM);
      fields = new String[2];
      fields[0]  = Utils.decFmURL(st.nextToken());
      fields[1]  = Utils.decFmURL(st.nextToken());
      symbol_str = Utils.decFmURL(st.nextToken());
    }
    public String toString() {
      if (fields.length == 1) {
        return Utils.encToURL(fields[0]) + BundlesDT.DELIM +
               Utils.encToURL(symbol_str);
      } else  {
        return Utils.encToURL(fields[0]) + BundlesDT.DELIM +
               Utils.encToURL(fields[1]) + BundlesDT.DELIM +
               Utils.encToURL(symbol_str);
      }
    }
  }

  /**
   * List of the Entities Added Structures
   */
  private List<EntitiesAddedStruct> entities_added_list = new ArrayList<EntitiesAddedStruct>();

  /**
   * World X Minimum for Viewport
   */
  private double wx_view_min = 0.0,

  /**
   * World X Maximum for Viewport
   */
                 wx_view_max = 1.0,
  /**
   * World Y Minimum for Viewport
   */
                 wy_view_min = 0.0,
  /**
   * World Y Maximum for Viewport
   */
                 wy_view_max = 1.0;

  /**
   * Return the view extents.
   *
   *@return rectangular bounds of extents
   */
  public Rectangle2D getExtents() {
    return new Rectangle2D.Double(wx_view_min, wy_view_min, wx_view_max - wx_view_min, wy_view_max - wy_view_min);
  }

  /**
   * Set the extents and ask for a re-render.
   *
   *@param rect new extents
   */
  public void setExtents(Rectangle2D rect) {
    wx_view_min = rect.getX();
    wy_view_min = rect.getY();
    wx_view_max = rect.getX() + rect.getWidth();
    wy_view_max = rect.getY() + rect.getHeight();
    getRTComponent().render();
  }

  /**
   * Fit the viewport to the data.... should limit it to viewable info...
   */
  public void zoomToFit() {
    RTEntitiesComponent.RenderContext myrc = (RTEntitiesComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Find the mins and maxes...
    double x0 = Double.POSITIVE_INFINITY, y0 = Double.POSITIVE_INFINITY,
           x1 = Double.NEGATIVE_INFINITY, y1 = Double.NEGATIVE_INFINITY;
    Iterator<String> it = entity_to_wxy.keySet().iterator(); while (it.hasNext()) {
      String key = it.next(); 

      if (showAll() == false && myrc.non_empty_entities.contains(key) == false) continue;

      Point2D pt = entity_to_wxy.get(key);
      if (x0 > pt.getX()) x0 = pt.getX(); if (x1 < pt.getX()) x1 = pt.getX();
      if (y0 > pt.getY()) y0 = pt.getY(); if (y1 < pt.getY()) y1 = pt.getY();

      if (timelineVisualization() && entity_first_heard.containsKey(key)) {
        if (x0 > entity_first_heard.get(key)) x0 = entity_first_heard.get(key);
        if (x1 < entity_last_heard. get(key)) x1 = entity_last_heard. get(key);
      }
    }

    // Make sure the values are sane...
    if (Double.isInfinite(x0) || Double.isInfinite(y0)) { x0 = 0.0; y0 = 0.0; x1 = 1.0; y1 = 1.0; }
    if (x0 == x1) { x0 -= 0.5; x1 += 0.5; }
    if (y0 == y1) { y0 -= 0.5; y1 += 0.5; }

    double w = x1 - x0, h = y1 - y0;

    // Assign to the instance variables and re-render ... add 5% on each side...
    wx_view_min = x0 - w/20; wy_view_min = y0 - h/20; 
    wx_view_max = x1 + w/20; wy_view_max = y1 + h/20;

    // Re-render
    getRTComponent().render();
  }

  /**
   * Zoom the view in.
   */
  public void zoomIn() {
    double view_w = wx_view_max - wx_view_min,
           view_h = wy_view_max - wy_view_min;
    if (view_w <= 0.001) view_w = 0.001;
    if (view_h <= 0.001) view_h = 0.001;

    wx_view_min += view_w / 6.0;
    wx_view_max -= view_w / 6.0;

    wy_view_min += view_h / 6.0;
    wy_view_max -= view_h / 6.0;

    // Re-render
    getRTComponent().render();
  }

  /**
   * Zoom in by the desired magnification.
   *
   *@param i magnification
   */
  public void zoomIn(double i)  {
    Rectangle2D r = getExtents();
    double exp = Math.pow(1.5,i); double cx = r.getX() + r.getWidth()/2, cy = r.getY() + r.getHeight()/2;
    setExtents(new Rectangle2D.Double(cx - r.getWidth()/(exp*2), cy - r.getHeight()/(exp*2), r.getWidth()/exp, r.getHeight()/exp));
  }

  /**
   * Zoom in by the desired magnificant leaving the specified coordinate in the same place.
   *
   *@param i      magnification
   *@param ref_cx reference x to keep in the same proportional place
   *@param ref_cy reference y to keep in the same proportional place
   */
  public void zoomIn(double i, double ref_cx, double ref_cy) {
    Rectangle2D r = getExtents();
    if (ref_cx >= r.getMinX() && ref_cx <= r.getMaxX() && ref_cy >= r.getMinY() && ref_cy <= r.getMaxY()) {
      double exp, new_width, new_height;
      if (i > 0.0) { exp = Math.pow(1.5,i);  new_width  = r.getWidth()/exp; new_height = r.getHeight()/exp; }
      else         { exp = Math.pow(1.5,-i); new_width  = r.getWidth()*exp; new_height = r.getHeight()*exp; }
      double x_perc     = (ref_cx - r.getMinX())/(r.getMaxX() - r.getMinX()),
             y_perc     = (ref_cy - r.getMinY())/(r.getMaxY() - r.getMinY());
      double new_xmin   = ref_cx - x_perc*new_width,
             new_ymin   = ref_cy - y_perc*new_height;
      setExtents(new Rectangle2D.Double(new_xmin, new_ymin, new_width, new_height));
    } else zoomIn(i);
  }

  /**
   * Zoom in by the desired magnificant leaving the specified coordinate in the same place.  Does not
   * zoom in on the y-dimension.
   *
   *@param i      magnification
   *@param ref_cx reference x to keep in the same proportional place
   */
  public void zoomIn(double i, double ref_cx) {
    Rectangle2D r = getExtents();
    if (ref_cx >= r.getMinX() && ref_cx <= r.getMaxX()) {
      double exp, new_width, new_height = 1.0;
      if (i > 0.0) { exp = Math.pow(1.5,i);  new_width  = r.getWidth()/exp; }
      else         { exp = Math.pow(1.5,-i); new_width  = r.getWidth()*exp; }
      double x_perc     = (ref_cx - r.getMinX())/(r.getMaxX() - r.getMinX());
      double new_xmin   = ref_cx - x_perc*new_width;
      setExtents(new Rectangle2D.Double(new_xmin, r.getY(), new_width, r.getHeight()));
    } else zoomIn(i);
  }

  /**
   * Zoom the view out.
   */
  public void zoomOut() {
    double view_w = wx_view_max - wx_view_min,
           view_h = wy_view_max - wy_view_min;
    if (view_w <= 0.001) view_w = 0.001;
    if (view_h <= 0.001) view_h = 0.001;

    wx_view_min -= view_w / 6.0;
    wx_view_max += view_w / 6.0;

    wy_view_min -= view_h / 6.0;
    wy_view_max += view_h / 6.0;

    getRTComponent().render();
  }

  /**
   * {@link JComponent} implementing the geospatial hisogram 
   */
  public class RTEntitiesComponent extends RTComponent {
    private static final long serialVersionUID = 222462744334118261L;

    /**
     * Flag to indicate whether help overlay should be drawn
     */
    boolean draw_help = false;

    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      set.addAll(myrc.geom_to_sxy.keySet());
      return set; }
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;

      // Find all entities that these bundles contribute to
      Set<String> entities = new HashSet<String>();
      Iterator<Bundle> it = bundles.iterator(); while (it.hasNext()) {
        Bundle bundle = it.next();
        if (bundle_to_entities.containsKey(bundle)) entities.addAll(bundle_to_entities.get(bundle));
      }

      // Figure out the geometry that they contribute to
      Iterator<String> it_ents = entities.iterator(); while (it_ents.hasNext()) {
        String entity = it_ents.next();
        if (myrc.entity_to_sxy.containsKey(entity) && myrc.sxy_to_geom.containsKey(myrc.entity_to_sxy.get(entity)))
          shapes.add(myrc.sxy_to_geom.get(myrc.entity_to_sxy.get(entity)));
      }

      return shapes; }
    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;

      if (myrc.geom_to_sxy.containsKey(shape) && myrc.sxy_counter_context.hasBin(myrc.geom_to_sxy.get(shape)))
        set.addAll(myrc.sxy_counter_context.getBundles(myrc.geom_to_sxy.get(shape)));

      return set;
    }
    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;

      Iterator<Shape> it = myrc.geom_to_sxy.keySet().iterator(); while (it.hasNext()) {
        Shape to_check = it.next();
        if (Utils.genericIntersects(shape, to_check)) set.add(to_check);
      }

      return set; }

  /**
   * Nodes to always label -- i.e., sticky labels
   */
  protected Set<String> sticky_labels = new HashSet<String>();

  /**
   * Set the nodes for sticky labels.  Sticky labels show for the set of node irregardless of
   * the draw labels flag.  They are useful for maintaining context in the visualization for
   * key nodes.
   */
  public void setStickyLabels() {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    Set<String>   sel  = myrc.filterEntities(getRTParent().getSelectedEntities());
    if (sel != null) {
      if        (last_shft_down && last_ctrl_down) { /* Intersect */ getRTParent().setSelectedEntities(sticky_labels);
      } else if (last_shft_down)                   { /* Subtract  */ sticky_labels.removeAll(sel);
      } else if (last_ctrl_down)                   { /* Add       */ sticky_labels.addAll(sel);
      } else                                       { /* Set/Clear */ sticky_labels.clear(); sticky_labels.addAll(sel); }
    }
    getRTComponent().render(); repaint();
  }

  /**
   * Invert the currently selected entities.
   */
  public void invertSelection() {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    Set<String>   sel     = getRTParent().getSelectedEntities();
    Set<String>   new_sel = new HashSet<String>();
    if (myrc != null && sel != null) {
      Iterator<String> it = myrc.entity_to_sxy.keySet().iterator();
      while (it.hasNext()) {
        String str = it.next();
        if        (sel.contains(str)) { } else {
          //
          // If we are showing all, then use the all possible entities
          //
          if (showAll()) {
            if (myrc.entity_to_sxy.containsKey(str))          new_sel.add(str);

          //
          // If not showing all, then only do what was rendered
          //
          } else {
            if (myrc.non_empty_entities.contains(str)) new_sel.add(str);

          }
        }
      }
    }
    getRTParent().setSelectedEntities(new_sel);
    repaint();
  }

  /**
   * Filter out the selection -- i.e., remove all bundles that are represented by
   * the selected nodes.
   */
  public void filterOutSelection() {
    RenderContext   myrc        = (RenderContext) rc; if (myrc == null) return;
    Set<String>     sel         = myrc.filterEntities(getRTParent().getSelectedEntities());
    if (sel == null || sel.size() == 0) { getRTParent().pop(); repaint(); return; }

    Set<Bundle> new_bundles = new HashSet<Bundle>();
    if (sel != null && sel.size() > 0) {
      String op_str;

      //
      // Only keep selected
      //
      if (last_shft_down) {
        Set<Bundle> visible_bundle_set = myrc.bs.bundleSet();
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) {
          String entity = it.next();
          if (entity_to_bundles.containsKey(entity)) { 
            Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator();
            while (it_bun.hasNext()) {
              Bundle bundle = it_bun.next();
              if (visible_bundle_set.contains(bundle)) new_bundles.add(bundle);
            }
          }
        }
        op_str = "filterInSelection";
        // ... so the usual would be to add in the bundles that aren't rendered in this view...
        // ... but maybe make an exception for this view based on the use cases...

      //
      // Get rid of selected
      //
      } else {
        new_bundles.addAll(myrc.bs.bundleSet());
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) {
          String entity = it.next();
          if (entity_to_bundles.containsKey(entity)) { new_bundles.removeAll(entity_to_bundles.get(entity)); }
        }
        op_str = "filterOutSelection";
      }

      // Construct the new bundle structure...
      Bundles new_bundles_struct = myrc.bs.subset(new_bundles);

      StringBuffer sb = new StringBuffer();
      sb.append("{\"operation\":\"" + op_str + "\",");
      sb.append("\"RTPanel\":\"" + getWinType() + "\",");
      sb.append("\"entities\":[");
      Iterator<String> it = sel.iterator(); while (it.hasNext()) {
        sb.append("\"" + it.next() + "\"");
        if (it.hasNext()) sb.append(",");
      }
      sb.append("]}");

      new_bundles_struct.setSliceDescription(sb.toString());
      getRTParent().push(new_bundles_struct);

      getRTParent().setSelectedEntities(new HashSet<String>());
      repaint();
    }
  }

  //-----------------------------------------------------------------------------------------------------------------------------
  // Below is from RTGraphPanel
  //-----------------------------------------------------------------------------------------------------------------------------

  /**
   *
   */
  public void mousePressed    (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mousePressed(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE || me.getButton() == MouseEvent.BUTTON2) genericMouse(ME_ENUM.PRESSED,  me); }
  public void mouseReleased   (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mouseReleased(me); 
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE) genericMouse(ME_ENUM.RELEASED, me); }
  public void mouseClicked    (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mouseClicked(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE || me.getButton() == MouseEvent.BUTTON2) genericMouse(ME_ENUM.CLICKED,  me); }
  public void mouseMoved      (MouseEvent me) { /* if (mode() == UI_MODE.FILTER) */ super.mouseMoved(me); // Was interfering with the addGanish() method...
                                                if (mode() == UI_MODE.EDIT)   genericMouse(ME_ENUM.MOVED,    me); }
  public void mouseDragged    (MouseEvent me) { if (mode() == UI_MODE.FILTER || mdrag) super.mouseDragged(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE) genericMouse(ME_ENUM.DRAGGED,  me); }
  public void mouseExited     (MouseEvent me) { super.mouseExited(me);  }
  public void mouseEntered    (MouseEvent me) { super.mouseEntered(me); grabFocus(); }
  public void keyPressed      (KeyEvent   ke) { super.keyPressed(ke);   
                                                int mover = 1; if (last_shft_down) mover *= 5; if (last_ctrl_down) mover *= 20;
                                                if      (ke.getKeyCode() == KeyEvent.VK_M)       toggleMode();
                                                else if (ke.getKeyCode() == KeyEvent.VK_U)       restoreFromSaveState();
                                                else if (ke.getKeyCode() == KeyEvent.VK_Q)       invertSelection();
                                                else if (ke.getKeyCode() == KeyEvent.VK_X)       filterOutSelection();
                                                else if (ke.getKeyCode() == KeyEvent.VK_R)      { if (last_shft_down) breakSelectedEntitiesIntoBundles();
                                                                                                  else                mergeSelectedEntities(); }
                                                else if (ke.getKeyCode() == KeyEvent.VK_G)       grid_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_Y)       line_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_A)       pan_mode  = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_E)       selectByFilledStatus();
                                                else if (ke.getKeyCode() == KeyEvent.VK_D)       selectShapeUnderMouse();
                                                else if (ke.getKeyCode() == KeyEvent.VK_Z)       selectColorUnderMouse();
                                                else if (ke.getKeyCode() == KeyEvent.VK_W)       renameSelectedEntities();
                                                else if (ke.getKeyCode() == KeyEvent.VK_F)       zoomToFit();
                                                else if (ke.getKeyCode() == KeyEvent.VK_C && (last_shft_down == false && last_ctrl_down == false))       circ_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_T)       centerSelection();

                                                else if (ke.getKeyCode() == KeyEvent.VK_S)       setStickyLabels();

                                                else if (ke.getKeyCode() == KeyEvent.VK_V && (last_shft_down == false && last_ctrl_down == false))       nextNodeSize();
                                                else if (ke.getKeyCode() == KeyEvent.VK_V && (last_shft_down == false))                                  nextNodeColor();

                                                // else if (ke.getKeyCode() == KeyEvent.VK_L &&
                                                //          last_ctrl_down)                         { node_legend_flag   = !node_legend_flag; render(); }
                                                // else if (ke.getKeyCode() == KeyEvent.VK_L)       toggleLabels();
                                                else if (ke.getKeyCode() == KeyEvent.VK_MINUS && 
                                                                                last_shft_down)  zoomToFit();
                                                else if (ke.getKeyCode() == KeyEvent.VK_MINUS)                             zoomOut();
                                                else if (ke.getKeyCode() == KeyEvent.VK_EQUALS)                            zoomIn();
                                                // else if (ke.getKeyCode() == KeyEvent.VK_UP)      shiftSelection(0,     -mover);
                                                // else if (ke.getKeyCode() == KeyEvent.VK_DOWN)    shiftSelection(0,      mover);
                                                // else if (ke.getKeyCode() == KeyEvent.VK_LEFT)    shiftSelection(-mover, 0);
                                                // else if (ke.getKeyCode() == KeyEvent.VK_RIGHT)   shiftSelection( mover, 0);
                                                else if (ke.getKeyCode() == KeyEvent.VK_H)       { draw_help = !draw_help; repaint(); }
                                              }
  public void keyReleased     (KeyEvent   ke) { super.keyReleased(ke);  
                                                if      (ke.getKeyCode() == KeyEvent.VK_G) grid_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_Y) line_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_C) circ_mode = false;
                                                // else if (ke.getKeyCode() == KeyEvent.VK_Z) zoom_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_A) pan_mode  = false;
                                              }
  public void keyTyped        (KeyEvent   ke) { super.keyTyped(ke);
                                              }
 
  /**
   * Zoom In/Out based on the mouse wheel changes.
   *
   *@param mwe mouse wheel event
   */
  public void mouseWheelMoved (MouseWheelEvent mwe) { 

    if        (timelineVisualization() == true  && last_shft_down == false && last_ctrl_down == false) { // Zoom in on timeline

      zoomIn(-mwe.getWheelRotation(), m_wx);

    } else if (timelineVisualization() == true  && last_shft_down == true  && last_ctrl_down == false) { // Zoom in regularly

      zoomIn(-mwe.getWheelRotation(), m_wx, m_wy);

    } else if (timelineVisualization() == true  && last_shft_down == false && last_ctrl_down == true)  { // Pan on timeline

      double w   = wx_view_max - wx_view_min;
      double inc = w * mwe.getWheelRotation()/10.0;
      wx_view_max += inc; wx_view_min += inc;
      getRTComponent().render();

    } else if (timelineVisualization() == false) { // Zoom in regularly

      zoomIn(-mwe.getWheelRotation(), m_wx, m_wy);

    }

  }

  /**
   * Help strings...
   *
   *Format is:
   *
   * Key | Non-mod | Shift-mod | Ctrl-mod | Shift-Ctrl-mod
   */
  String help_strs[][] = {
    {"M", "Toggle Mode",          "",                    "", "" },

    {"Q", "Invert Selection",     "",                    "", "" },
    {"X", "Filter Out Selection", "Filter In Selection", "", "" },
    {"R", "Merge Selection",      "Break Selection",     "", "" },
    {"E", "Select By Filled",     "",                    "", "" },
    {"D", "Select By Shape",      "",                    "", "" },
    {"Z", "Select By Color",      "",                    "", "" },
    {"W", "Rename Selected",      "",                    "", "" },

    {"T", "Position Selected",    "Horizontal",          "Vertical",   "" },
    {"G", "Grid Layout",          "",                    "", "" },
    {"Y", "Line Layout",          "",                    "", "" },
    {"C", "Circular Layout",      "",                    "", "" },

    {"A", "Pan Mode (Hold)",      "",                    "", "" },
    {"F", "Zoom To Fit",          "To Selected",         "", "" },
    {"S", "Sticky Labels",        "Sub",                 "Add",        "Select Sticky Labels" },
    {"V", "Next Node Size",       "",                    "Node Color", "" }
  };

  /**
   * Method to handle the generic mouse interactions -- by centralizing the interaction, the state of various operations 
   * can be more easily maintained.
   *
   *@param me_enum mouse action
   *@param me      original mouse event
   */
  protected void genericMouse(ME_ENUM me_enum, MouseEvent me) {
    boolean op_in_effect = false;
    if        (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.PRESSED) {
      op_in_effect = true;
      if         (me.getButton() == MouseEvent.BUTTON2 || pan_mode)   { panOrZoom(me_enum, me); 
      } else if  (me.getButton() == MouseEvent.BUTTON1 && grid_mode)  { gridLayout(me_enum, me);
      } else if  (me.getButton() == MouseEvent.BUTTON1 && line_mode)  { lineLayout(me_enum, me);
      } else if  (me.getButton() == MouseEvent.BUTTON1 && circ_mode)  { circleLayout(me_enum, me);
      } else                                                          { selectOrMove(me_enum,me);
      }
    } else if (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.CLICKED && me.getButton() == MouseEvent.BUTTON2) { panOrZoom(me_enum,me);    op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.CLICKED && me.getButton() == MouseEvent.BUTTON1) { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.PANNING)                            { panOrZoom   (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.SELECTING)                          { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.MOVING)                             { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.GRID_LAYOUT)                        { gridLayout  (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.LINE_LAYOUT)                        { lineLayout  (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.CIRCLE_LAYOUT)                      { circleLayout(me_enum,me); op_in_effect = true;
    }
    // Capture mouse positions
    m_x = me.getX(); m_y = me.getY(); m_wx = sxToWx(m_x); m_wy = syToWy(m_y);
    // Inform the application of the entity under the mouse
    getRTParent().setEntitiesUnderMouse(underMouse(me));
    // If no operation is in effect, find the closest node and draw the labels
    RenderContext myrc = (RenderContext) rc;
    if (op_in_effect == false && myrc != null && myrc.draw_entity_labels == false) {
      String new_dyn_labeler = underMouseSimple(me);
      if (dyn_labeler != null) {
        if (dyn_labeler.equals(new_dyn_labeler) == false) { dyn_labeler = new_dyn_labeler; repaint(); }
      } else if (new_dyn_labeler != null) { dyn_labeler = new_dyn_labeler; repaint(); }
    } else if (dyn_labeler != null) { dyn_labeler = null; repaint(); }
  }
  
  /**
   * Dynamic labeler variable holder
   */
  String dyn_labeler = null;

  /**
   * Initialize the variables used for a mouse drag
   *
   *@param me mouse event
   */
  private void initializeDragVars(MouseEvent me) { m_wx0 = m_wx1 = sxToWx(m_x0 = m_x1 = me.getX()); m_wy0 = m_wy1 = syToWy(m_y0 = m_y1 = me.getY()); }

  /**
   * Update the variables used for a mouse drag
   *
   *@param me mouse event
   */
  private void updateDragVars    (MouseEvent me) { updateDragVars(me,false); }

  /**
   * Update the variables used for a mouse drag
   *
   *@param me           mouse event
   *@param ui_constrain if true, restricts movement to just horizontal or just vertical
   */
  private void updateDragVars    (MouseEvent me, boolean ui_constrain) {
    if        (ui_constrain && last_shft_down) {
      m_wx1 = sxToWx(m_x1 = me.getX());
      m_wy1 = m_wy0;
      m_y1  = m_y0;
    } else if (ui_constrain && last_ctrl_down)  {
      m_wx1 = m_wx0;
      m_x1  = m_x0;
      m_wy1 = syToWy(m_y1 = me.getY()); 
    } else {
      m_wx1 = sxToWx(m_x1 = me.getX());
      m_wy1 = syToWy(m_y1 = me.getY()); 
    }
  }

  /**
   * User interface mode
   */
  UI_INTERACTION  ui_inter = UI_INTERACTION.NONE;
  /**
   * X screen coordinate for beginning of mouse drag
   */
  int             m_x0,  
  /**
   * Y screen coordinate for beginning of mouse drag
   */
                  m_y0, 
  /**
   * X screen coordinate for end of mouse drag
   */
                  m_x1, 
  /**
   * Y screen coordinate for end of mouse drag
   */
                  m_y1, 
  /**
   * Current mouse x screen coordinate
   */
                  m_x, 
  /**
   * Current mouse y screen coordinate
   */
                  m_y; 
  /**
   * X world coordinate for beginning of mouse drag
   */
  double          m_wx0, 
  /**
   * Y world coordinate for beginning of mouse drag
   */
                  m_wy0, 
  /**
   * X world coordinate for end of mouse drag
   */
                  m_wx1, 
  /**
   * Y world coordinate for end of mouse drag
   */
                  m_wy1, 
  /**
   * X world coordinate for mouse current position
   */
                  m_wx, 
  /**
   * Y world coordinate for mouse current position
   */
                  m_wy;
  /**
   * Flag to indicate to layout the nodes in a grid
   */
  boolean         grid_mode = false, 
  /**
   * Flag to indicate to layout the nodes in a line
   */
                  line_mode = false, 
  /**
   * Flag to indicate to layout the nodes in a circle
   */
                  circ_mode = false,
  /**
   *
   */
                  zoom_mode = false,
  /**
   * 
   */
                  pan_mode  = false;
  /**
   * Set of nodes that is currently being moved
   */
  Set<String>    moving_set = null;

  /**
   * For the selected entities, assign them to the last location of the mouse.
   * force a transform and re-render.
   */
  public void centerSelection() {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;

    Set<String> sel;
    if (showAll()) sel = getRTParent().getSelectedEntities();
    else           sel = myrc.filterEntities(getRTParent().getSelectedEntities()); 

    if (sel != null && sel.size() > 0) {
      saveState(SaveState.POSITIONS);
      Iterator<String> it = sel.iterator();
      while (it.hasNext()) {
        String ent = it.next(); Point2D point = entity_to_wxy.get(ent);

        if      (last_shft_down) entity_to_wxy.put(ent, new Point2D.Double(point.getX(),m_wy));
        else if (last_ctrl_down) entity_to_wxy.put(ent, new Point2D.Double(m_wx,point.getY()));
        else                     entity_to_wxy.put(ent, new Point2D.Double(m_wx,m_wy));

        if (timelineVisualization()) adjustXCoordinateForTimeline(ent);
      }
      getRTComponent().render();
    }
  }

  /**
   * Add overlays for mouse position/movement
   */
  @Override
  public void addGarnish(Graphics2D g2d, int mx, int my) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    if (myrc.timeline_visualization) {
      // Draw the label showing the exact time
      g2d.setColor(RTColorManager.getColor("label", "major"));
      String str   = Utils.exactDate((long) sxToWx(mx));
      int    txt_h = Utils.txtH(g2d, str);
      int    txt_w = Utils.txtW(g2d, str);
      int    sx    = mx - txt_w/2;
      if (sx < 2)                             sx = 2;
      if (sx > myrc.getRCWidth() - txt_w - 2) sx = myrc.getRCWidth() - txt_w - 2;
      g2d.drawString(str, sx, myrc.getRCHeight() - 2 - 2*txt_h);

      // Draw the line for comparisons...
      g2d.setColor(RTColorManager.getColor("axis", "minor"));
      g2d.drawLine(mx, 0, mx, myrc.getRCHeight());
    }
  }

  /**
   * Handle the interaction for a grid layout with the mouse.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void gridLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me); ui_inter = UI_INTERACTION.GRID_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me);                                            repaint(); break;
      case RELEASED: updateDragVars(me);     ui_inter = UI_INTERACTION.NONE;        
                     Set<String> set; 

                     if (showAll()) set = getRTParent().getSelectedEntities();
                     else           set = myrc.filterEntities(getRTParent().getSelectedEntities()); 

                     // Normalize the coordinates for the grid layout
                     double dx = Math.abs(m_wx1 - m_wx0),
                            dy = Math.abs(m_wy1 - m_wy0);
                     if (m_wx0 > m_wx1) { double tmp = m_wx1; m_wx1 = m_wx0; m_wx0 = tmp; }
                     if (m_wy0 > m_wy1) { double tmp = m_wy1; m_wy1 = m_wy0; m_wy0 = tmp; }

                     // Make sure the set is non-null, non-zero
                     if (set != null && set.size() > 1) {
                       int    sqrt = (int) Math.sqrt(set.size()), max_x_i = 1, max_y_i = 1;
                       if (dx < 0.0001) dx = 0.0001; if (dy < 0.0001) dy = 0.0001;

                       // Figure out the approximate shape
                       if        ((dx/dy) > 1.5 || (dy/dx) > 1.5) { // Rectangular
                         double closest_dist = Double.POSITIVE_INFINITY;
                         for (int i=1;i<=sqrt;i++) {
                           int    other = set.size()/i;
                           double ratio = ((double) other)/((double) i);
                           double dist  = Math.abs(ratio - dx/dy);
                           if (dist < closest_dist) {
                             if (dx/dy > 1.0) {
                               max_x_i = (i > other) ? i : other;
                               max_y_i = (i > other) ? other : i;
                             } else           {
                               max_x_i = (i > other) ? other : i;
                               max_y_i = (i > other) ? i : other;
                             }
                           }
                         }
                       } else if ((dy/dx) > 1.5) { // Rectangular

                       } else                    { // Roughly square
                         max_x_i = max_y_i = sqrt;
                       }
                       int x_i = 0, y_i = 0;

                       // Sort by sort preference and save for undo
                       List<String> sorted = sortBy(set, getRTParent().getVisibleBundles());

                       // Do the  layout
                       saveState(SaveState.POSITIONS);
                       for (int i=0;i<sorted.size();i++) {
                         String entity = sorted.get(i);
                         entity_to_wxy.put(entity,new Point2D.Double(m_wx0 + x_i*(dx/max_x_i),
                                                                     m_wy0 + y_i*(dy/max_y_i)));
                         if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                         x_i++; if (x_i >= max_x_i) { x_i = 0; y_i++; }
                       }
                       getRTComponent().render();
                     } else if (set != null && set.size() == 1) {
                       saveState(SaveState.POSITIONS);
                       String entity = set.iterator().next();
                       entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + dx/2, m_wy0 + dy/2));
                       if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                       getRTComponent().render();
                     }
                     repaint(); break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a line layout.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void lineLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me);   ui_inter = UI_INTERACTION.LINE_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me, true);                                        repaint(); break;
      case RELEASED: updateDragVars(me, true); ui_inter = UI_INTERACTION.NONE;

                     Set<String> set;

                     if (showAll()) set = getRTParent().getSelectedEntities();
                     else           set = myrc.filterEntities(getRTParent().getSelectedEntities()); 

                     if (set != null && set.size() > 0) {
                       // Calculate the line equation
                       double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                       double t  = 0.0, inc = 1.0 / (set.size() - 1);

                       // Sort by sort preference and save for undo
                       List<String> sorted = sortBy(set, getRTParent().getVisibleBundles());

                       // Do the  layout
                       saveState(SaveState.POSITIONS);
                       for (int i=0;i<sorted.size();i++) {
                         String entity = sorted.get(i);
                         entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + t*dx, m_wy0 + t*dy));
                         if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                         t += inc;
                       }
                       getRTComponent().render();
                     }
                     repaint(); break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interations for a circle layout.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void circleLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me); ui_inter = UI_INTERACTION.CIRCLE_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me);                                              repaint(); break;
      case RELEASED: updateDragVars(me);     ui_inter = UI_INTERACTION.NONE;          repaint();
                     double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0, radius = Math.sqrt(dx*dx+dy*dy);

                     Set<String> set;
                     if (showAll()) set = getRTParent().getSelectedEntities();
                     else           set = myrc.filterEntities(getRTParent().getSelectedEntities()); 

                     saveState(SaveState.POSITIONS);
                     if        (set != null && set.size() == 1) {
                       String entity = set.iterator().next();
                       entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + dx/2, m_wy0 + dy/2));
                       if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                       getRTComponent().render();
                     } else if (set != null && set.size() >  0) {
                       double angle = 0.0, angle_inc = 2.0 * Math.PI / set.size();
                       // Sort the elements
                       List<StrCountSorter> sorter = new ArrayList<StrCountSorter>();
                       Iterator<String> it = set.iterator();
                       while (it.hasNext()) {
                         String entity = it.next();
                         int    total  = entity_to_bundles.get(entity).size(); 
                         sorter.add(new StrCountSorter(entity,total));
                       }
                       Collections.sort(sorter);
                       // Do the  layout
                       for (int i=0;i<sorter.size();i++) {
                         String entity = sorter.get(i).toString();
                         entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + Math.cos(angle)*radius,
                                                                      m_wy0 + Math.sin(angle)*radius));
                         if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                         angle += angle_inc;
                       }
                       getRTComponent().render();
                     }
                     break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a pan/zoom operations.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void panOrZoom(ME_ENUM me_enum, MouseEvent me) {
    if (me.getButton() != MouseEvent.BUTTON2 && ui_inter != UI_INTERACTION.PANNING && pan_mode == false) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me);  ui_inter = UI_INTERACTION.PANNING; repaint(); break;
      case DRAGGED:  updateDragVars(me,true);                                    repaint(); break;
      case RELEASED: updateDragVars(me,true); ui_inter = UI_INTERACTION.NONE; 
                     double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                     if (dx != 0.0 || dy != 0.0) {
                       Rectangle2D r = getExtents();
                       setExtents(new Rectangle2D.Double(r.getX() - dx, r.getY() - dy, r.getWidth(), r.getHeight()));
                     }
                     break;
      case CLICKED:  zoomToFit(); break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a select entities, move entities operation.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void selectOrMove(ME_ENUM me_enum, MouseEvent me) {
    if (me.getButton() != MouseEvent.BUTTON1 && ui_inter != UI_INTERACTION.SELECTING && ui_inter != UI_INTERACTION.MOVING) return;
    RenderContext myrc = (RenderContext) rc;
    if (myrc != null) {
      switch (me_enum) {
        case PRESSED:  Set<String>  sel;

                       if (showAll()) { sel = getRTParent().getSelectedEntities();
                                        sel.retainAll(entity_to_wxy.keySet());
                       } else           sel = myrc.filterEntities(getRTParent().getSelectedEntities());

                       // Figure what's under the mouse
                       Set<String>  under_mouse = underMouse(me);

                       // If what's under the mouse is already selected, then cause it to be moving
                       if      (Utils.overlap(sel,under_mouse))     { ui_inter = UI_INTERACTION.MOVING; 
                                                                      moving_set = sel; }
                       else if (under_mouse.size() > 0 && 
                                (last_shft_down || last_ctrl_down)) { setOperation(under_mouse); }
                       else if (under_mouse.size() > 0)             { ui_inter = UI_INTERACTION.MOVING; 
                                                                      getRTParent().setSelectedEntities(under_mouse);
                                                                       moving_set = under_mouse; }
                       else                                         { ui_inter = UI_INTERACTION.SELECTING; }

                       initializeDragVars(me);

                       repaint();
                       break;
        case DRAGGED:  updateDragVars(me, ui_inter == UI_INTERACTION.MOVING); repaint(); break;
        case RELEASED: updateDragVars(me, ui_inter == UI_INTERACTION.MOVING);
                       boolean no_move = false;
                       if        (ui_inter == UI_INTERACTION.MOVING)    {
                         double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                         if (dx != 0 || dy != 0) {
                           saveState(SaveState.POSITIONS);
                           Iterator<String> it = moving_set.iterator();
                           while (it.hasNext()) {
                             String  entity  = it.next();
                             Point2D pt      = entity_to_wxy.get(entity);
                             entity_to_wxy.put(entity, new Point2D.Double(pt.getX() + dx, pt.getY() + dy));
                             if (timelineVisualization()) adjustXCoordinateForTimeline(entity);
                           }
                         getRTComponent().render();
                         } else no_move = true;
                       } 
                       if (ui_inter == UI_INTERACTION.SELECTING || no_move) {
                         int x0 = m_x0 < m_x1 ? m_x0 : m_x1,   y0 = m_y0 < m_y1 ? m_y0 : m_y1,
                             dx = (int) Math.abs(m_x1 - m_x0), dy = (int) Math.abs(m_y1 - m_y0);
                         if (dx == 0) dx = 1; if (dy == 0) dy = 1;
                         Rectangle2D rect = new Rectangle2D.Double(x0,y0,dx,dy);

                         Set<String> new_sel = new HashSet<String>();
                         Iterator<String> it = myrc.sxy_to_geom.keySet().iterator();
                         while (it.hasNext()) {
                           String node = it.next(); Shape shape = myrc.sxy_to_geom.get(node);
                           if (rect.intersects(shape.getBounds())) new_sel.addAll(myrc.sxy_to_entities.get(node));
                         }
                         setOperation(new_sel);
                       }
                       ui_inter = UI_INTERACTION.NONE;
                       repaint(); break;
        case CLICKED:  setOperation(underMouse(me));
                       repaint(); break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
      }
    }
  }

  /**
   * Based on the keys pressed (ctrl, shft), perform the correct set operation for selected entities.
   *
   *@param sel selection set
   */
  public void setOperation(Set<String> sel) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    Set<String> new_sel = new HashSet<String>(), old_sel;
    if        (last_shft_down && last_ctrl_down) { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.retainAll(sel);
                                                   new_sel = old_sel;
    } else if (last_shft_down)                   { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.removeAll(sel);
                                                   new_sel = old_sel;
    } else if (                  last_ctrl_down) { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.addAll(sel);
                                                   new_sel = old_sel;
    } else new_sel = sel;
    getRTParent().setSelectedEntities(new_sel);
  }

  /**
   * Find the entities under the mouse.  Useful for selecting and moving operation.
   *
   *@param  me mouse event
   *
   *@return all entities under the mouse as a set
   */
  Set<String> underMouse(MouseEvent me) { return underMouse(me.getX(), me.getY()); }

  /**
   * Find the entities under the mouse.  Useful for selecting and moving operation.
   *
   *@param x x-coordinate of mouse
   *@param y y-coordinate of mouse
   *
   *@return all entities under the mouse as a set
   */
  Set<String> underMouse(int x, int y) {
    RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return new HashSet<String>();
    Rectangle2D      mouse_rect  = new Rectangle2D.Double(x-1,y-1,3,3);
    Set<String>      under_mouse = new HashSet<String>();
    Iterator<String> it          = myrc.sxy_to_entities.keySet().iterator();
    while (it.hasNext()) {
      String node_coord = it.next(); 
      if (myrc.sxy_to_geom.containsKey(node_coord) && mouse_rect.intersects(myrc.sxy_to_geom.get(node_coord).getBounds())) {
        under_mouse.addAll(myrc.sxy_to_entities.get(node_coord));
      }
    }
    if (under_mouse.size() == 0) { // If nothing, check the links
    }
    return under_mouse;
  }

  /**
   * Find the entities under the mouse.  For this version, only nodes are considered and no accumulateion occurs.
   *
   *@param me mouse event
   *
   *@return node_coord for the object under the mouse
   */
  String underMouseSimple(MouseEvent me) {
    RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return null;
    Rectangle2D      mouse_rect  = new Rectangle2D.Double(me.getX()-1,me.getY()-1,3,3);
    Iterator<String> it          = myrc.sxy_to_entities.keySet().iterator();
    while (it.hasNext()) {
      String node_coord = it.next(); 
      if (myrc.sxy_to_geom.containsKey(node_coord) && 
          mouse_rect.intersects(myrc.sxy_to_geom.get(node_coord).getBounds())) return node_coord;
    }
    return null;
  }

  /**
   * Draw the current interactions as the mouse is dragged.
   *
   *@param g2d  graphics primitive
   *@param myrc current render context
   */
  private void drawInteractions(Graphics2D g2d, RenderContext myrc) {
      Composite orig_comp = g2d.getComposite();
      // Draw the interaction
      switch (ui_inter) {
        case PANNING:       g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,getWidth(),getHeight());
                            g2d.drawImage(myrc.getBase(), m_x1 - m_x0, m_y1 - m_y0, null); 
                            g2d.setColor(RTColorManager.getColor("annotate", "cursor")); int cx = getWidth()/2, cy = getHeight()/2; g2d.drawLine(cx-12,cy,cx+12,cy); g2d.drawLine(cx,cy-12,cx,cy+12);
                            break;
        case GRID_LAYOUT:
        case CIRCLE_LAYOUT:
        case SELECTING:     int x0 = m_x0 < m_x1 ? m_x0 : m_x1,   y0 = m_y0 < m_y1 ? m_y0 : m_y1,
                                dx = (int) Math.abs(m_x1 - m_x0), dy = (int) Math.abs(m_y1 - m_y0);
                            if (dx == 0) dx = 1; if (dy == 0) dy = 1;
                            g2d.setColor(RTColorManager.getColor("select", "region"));
                            Shape shape;
                            if (ui_inter == UI_INTERACTION.SELECTING || ui_inter == UI_INTERACTION.GRID_LAYOUT) {
                              shape = new Rectangle2D.Double(x0,y0,dx,dy);
                            } else {
                              double radius = Math.sqrt(dx*dx+dy*dy);
                              shape = new Ellipse2D.Double(m_x0-radius,m_y0-radius,2*radius,2*radius);
                            }
                            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
                            g2d.fill(shape);
                            g2d.setComposite(orig_comp);
                            if (ui_inter == UI_INTERACTION.SELECTING) {
                              String str = "Selecting..."; if      (last_shft_down && last_ctrl_down) str += " (Intersect)";
                                                           else if (last_shft_down                  ) str += " (Remove From)";
                                                           else if (                  last_ctrl_down) str += " (Add To)";
                              g2d.drawString(str, x0, y0); 
                            } else g2d.drawString("Layout...", x0, y0);
                            g2d.draw(shape);
                            break;
        case MOVING:        g2d.drawString("Moving...", 5, getHeight()-5); 
                            Set<String> moving_set_dup = moving_set;
                            if (moving_set_dup != null && moving_set_dup.size() > 0) {
                              AffineTransform orig = g2d.getTransform(); g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));
                              g2d.translate(m_x1 - m_x0, m_y1 - m_y0);
                              Iterator<String> it = moving_set_dup.iterator();
                              while (it.hasNext()) {
                                String str = it.next(); shape = myrc.sxy_to_geom.get(myrc.entity_to_sxy.get(str));
                                if (shape != null) { g2d.draw(shape); }
                              }
                              g2d.setTransform(orig);
                              }
                            break;
        case LINE_LAYOUT:   g2d.setColor(RTColorManager.getColor("linknode", "layout"));
                            g2d.drawLine(m_x0, m_y0, m_x1, m_y1);
                            break;
        case NONE:
                break;
        default:
                break;
      }
  }

  /**
   * Draw the number of selected entities.  Draw the current mode in text.
   *
   *@param g2d    graphics primitive
   *@param myrc   current render context
   *@param sel    currently selected entities
   *@param sticks sticky label set
   *@param txt_h  text height for current font
   */
  private void drawSelectedEntities(Graphics2D g2d, RenderContext myrc, Set<String> sel, Set<String> sticks, int txt_h) {
        Stroke orig_stroke = g2d.getStroke(); g2d.setStroke(new BasicStroke(3.0f));

        // Draw the selected entities - outline the shape with red and draw labels if appropriate
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) { 
          String node  = it.next();  String node_coord = myrc.entity_to_sxy.get(node);
          Shape  shape = myrc.sxy_to_geom.get(node_coord);
          if (shape != null) {
            g2d.setColor(RTColorManager.getColor("linknode", "movenodes")); g2d.draw(shape);
            if (myrc.node_lm != null && sel.size() < 100 && sticks.contains(node) == false) { 
              // Bound it by 100...  otherwise, it's going to be unreasonable
              myrc.node_lm.draw(g2d, node_coord, (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getMaxY(), true); // label render
            }
          }
        }

        // Draw the sticky labels
        it = sticks.iterator();
        while (it.hasNext()) {
          String node  = it.next();  String node_coord = myrc.entity_to_sxy.get(node);
          Shape  shape = myrc.sxy_to_geom.get(node_coord);
          if (shape != null && myrc.node_lm != null) {
            myrc.node_lm.draw(g2d, node_coord, (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getMaxY(), true); // label render
          }
        }
        g2d.setStroke(orig_stroke);

        // Provide information on what's selected
        g2d.setColor(RTColorManager.getColor("label", "default"));
        g2d.drawString("" + sel.size() + " Selected", 5, txt_h);
  }

  //-----------------------------------------------------------------------------------------------------------------------------
  // Above is from RTGraphPanel
  //-----------------------------------------------------------------------------------------------------------------------------

    /**
     * Select by the filled status of the shape under the mouse.  Filled status is whether the records within
     * that shape are currently rendered.
     */
    public void selectByFilledStatus() {
      RenderContext    myrc                 = (RenderContext) rc; if (myrc == null) return;
      Set<String>      matching_entities    = new HashSet<String>();

      // What's under the mouse?
      Set<String>      entities_under_mouse = underMouse(m_x, m_y);

      // Determine if what's under the mouse is filled or not filled (or both)...
      boolean select_filled = false, select_empty = false;
      if (entities_under_mouse != null && entities_under_mouse.size() > 0) {
        Iterator<String> it = entities_under_mouse.iterator(); while (it.hasNext()) {
          String entity = it.next();
          if (myrc.entity_counter_context.hasBin(entity)) select_filled = true;
          else                                            select_empty  = true;
        }
      }

      // For everything, add to the matching if it matches the criteria (empty, filled, or both)
      Iterator<String> it = entity_to_wxy.keySet().iterator(); while (it.hasNext()) {
        String entity = it.next();
        if (select_filled && myrc.entity_counter_context.hasBin(entity) == true)  matching_entities.add(entity);
        if (select_empty  && myrc.entity_counter_context.hasBin(entity) == false) matching_entities.add(entity);
      }

      // Set the selection
      setOperation(matching_entities); // abc -- doesn't work for all cases when using setOperation()... probably because counter context is empty...
    }

    /**
     * Based on the shape(s) under the mouse, select all entities with that shape.
     */
    public void selectShapeUnderMouse() {
      RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return;

      // Figure out the shapes under the mouse
      Set<String> entities_under_mouse = underMouse(m_x, m_y);
      Set<String> shapes_under_mouse = new HashSet<String>();
      Iterator<String> it = entities_under_mouse.iterator(); while (it.hasNext()) {
        shapes_under_mouse.add(entity_to_shape.get(it.next()));
      }

      // Find all the other enties with that shape
      Set<String> matching_entities = new HashSet<String>();
      it = entity_to_shape.keySet().iterator(); while (it.hasNext()) {
        String entity = it.next();
        String shape  = entity_to_shape.get(entity);
        if (shapes_under_mouse.contains(shape)) {
          if      (showAll())                                  matching_entities.add(entity);
          else if (myrc.entity_counter_context.hasBin(entity)) matching_entities.add(entity);
        }
      }

      // Set the selection
      setOperation(matching_entities);
    }

    /**
     * Base on the color under the mouse, select all entities with that color.
     */
    public void selectColorUnderMouse() {
      boolean first_notification = true;
      RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return;

      // Figure out the shapes under the mouse
      Set<String> entities_under_mouse = underMouse(m_x, m_y);
      Set<Color> colors_under_mouse = new HashSet<Color>();
      Iterator<String> it = entities_under_mouse.iterator(); while (it.hasNext()) {
        String entity = it.next();
        if (entityColor() == EntityColor.Label && myrc.label_color_lm != null) colors_under_mouse.add(myrc.label_color_lm.getColor(myrc.entity_to_sxy.get(entity)));
        else                                                                   colors_under_mouse.add(myrc.entity_counter_context.binColor(entity));
      }

      // Match entities with that color
      Set<String> matching_entities = new HashSet<String>();
      it = myrc.entity_to_sxy.keySet().iterator(); while (it.hasNext()) {
        String entity = it.next();
        if (showAll()) {
          // ... so ... non rendered entities don't have a color

          if (myrc.entity_counter_context.hasBin(entity)) {
            Color color;

            if (entityColor() == EntityColor.Label && myrc.label_color_lm != null) color = myrc.label_color_lm.getColor(myrc.entity_to_sxy.get(entity));
            else                                                                   color = myrc.entity_counter_context.binColor(entity);

            if (colors_under_mouse.contains(color)) matching_entities.add(entity);
          } else {
            if (first_notification) {
              System.err.println("selectColorUnderMouse() - don't know color of unrendered items...");
              first_notification = false;
            }
          }

        } else if (myrc.entity_counter_context.hasBin(entity)) {
          Color color = myrc.entity_counter_context.binColor(entity);
          if (colors_under_mouse.contains(color)) matching_entities.add(entity);
        }
      }

      // Set the selection
      setOperation(matching_entities);
    }

    /**
     * Provide a popup box for the user to rename the selected entities.
     */
    public void renameSelectedEntities() {
      saveState(SaveState.CROSS_REFS);

      // Get the selected entities and filter them down to the current view
      RenderContext myrc = (RenderContext) rc;
      Set<String> entities = getRTParent().getSelectedEntities();
      if (showAll() || myrc == null) entities.retainAll(entity_to_wxy.keySet());
      else                           entities.retainAll(myrc.entity_to_sxy.keySet());

      // If any exist, popup a dialog for renaming them
      if (entities.size() > 0) {
        String existing_name = entities.iterator().next();
        String description = (String) JOptionPane.showInputDialog(this, "New Name", "Rename Entity(s)", JOptionPane.PLAIN_MESSAGE, null, null, existing_name);
        if (description != null) {

          user_enterred_entities.add(description); // maybe only description?  or maybe all the incremented versions?

          Set<String> new_names = new HashSet<String>();

          int num = 1;
          Iterator<String> it = entities.iterator(); while (it.hasNext()) {
            String old_name = it.next();
            String new_name = description; while (entity_to_wxy.containsKey(new_name)) new_name = description + " " + (num++);
            new_names.add(new_name);

            entity_to_wxy.      put(new_name, entity_to_wxy.      get(old_name));
            entity_to_bundles.  put(new_name, entity_to_bundles.  get(old_name));
            entity_first_heard. put(new_name, entity_first_heard. get(old_name));
            entity_last_heard.  put(new_name, entity_last_heard.  get(old_name));
            entity_to_shape.    put(new_name, entity_to_shape.    get(old_name));

            Iterator<Bundle> it_bun = entity_to_bundles.get(new_name).iterator(); while (it_bun.hasNext()) {
              Bundle bundle = it_bun.next();
              bundle_to_entities.get(bundle).remove(old_name);
              bundle_to_entities.get(bundle).add(new_name);
            }

            permanentlyRemoveEntityFromVisualization(old_name);
          }

          // Set the selected
          getRTParent().setSelectedEntities(new_names);

          // Render
          getRTComponent().render();
        }
      }
    }

    /**
     * Go to the next entity size and re-render.
     */
    private void nextEntitySize() {
      int sel_i = 0;
      for (int i=0;i<entity_size_rbmis.length;i++) {
        if (entity_size_rbmis[i].isSelected()) sel_i = i;
      }
      sel_i = (sel_i + 1) % entity_size_rbmis.length;
      entity_size_rbmis[sel_i].setSelected(true);
    }

    @Override
    public void paintComponent(Graphics g) {
      // Setup
      Graphics2D g2d = (Graphics2D) g; int txt_h = Utils.txtH(g2d, "0");
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      // Do the default stuff
      super.paintComponent(g);

      // Draw the mode
      g2d.setColor(RTColorManager.getColor("label", "default"));
      if (mode() == UI_MODE.EDIT) { g2d.drawString("Edit",   getWidth() - 2 - Utils.txtW(g2d, "Edit"),    txt_h + 2); } 
      else                        { g2d.drawString("Filter", getWidth() - 2 - Utils.txtW(g2d, "Filter"),  txt_h + 2); }

      // Draw the other features
      RenderContext myrc = (RenderContext) rc; if (myrc != null) {
        drawInteractions(g2d,myrc);

        // Draw the selection
        Set<String> sel = getRTParent().getSelectedEntities(); if (sel == null) sel = new HashSet<String>();
        if (sel.size() > 0 || sticky_labels.size() > 0) { 
          drawSelectedEntities(g2d, myrc, sel, sticky_labels, txt_h);
        }

        // Draw the dynamic label if set
        String dyn_labeler_copy = dyn_labeler; if (dyn_labeler_copy != null) drawDynamicLabels(g2d, myrc, dyn_labeler_copy);
      }

      if (draw_help) drawHelp(g2d, txt_h);
    }

    /**
     * Draw the dynamic label.
     *
     *@param g2d              graphics primitive
     *@param myrc             render context
     *@param dyn_labeler_copy copy of the dynamic label to render
     */
    public void drawDynamicLabels(Graphics2D g2d, RenderContext myrc, String dyn_labeler_copy) {
      // Check for existence
      if (myrc.sxy_to_entities.containsKey(dyn_labeler_copy)) {

        // Get all the labels
        List<String> labels = new ArrayList<String>();
        labels.addAll(myrc.sxy_to_entities.get(dyn_labeler_copy));

        // Get the location
        int base_sx = myrc.sxy_to_sx.get(dyn_labeler_copy), 
            base_sy = myrc.sxy_to_sy.get(dyn_labeler_copy);

        // Adjust for timeline visualization for partially visible entities ... copy of the adjustXForTimelineVisualization()...
        if (myrc.timeline_visualization && (base_sx < 0 || base_sx > myrc.rc_w)) {
          int x0 = (int) myrc.sxy_to_geom.get(dyn_labeler_copy).getBounds().getMinX(), 
              x1 = (int) myrc.sxy_to_geom.get(dyn_labeler_copy).getBounds().getMaxX();
          if      (x0 > 0 && x0 < myrc.rc_w) base_sx = (x0 + myrc.rc_w)/2;
          else if (x1 > 0 && x1 < myrc.rc_w) base_sx = (x1 + 0)/2;
          else                               base_sx = myrc.rc_w/2;

          if (base_sx <             20) base_sx =             20;
          if (base_sx > myrc.rc_w - 20) base_sx = myrc.rc_w - 20;
        }

        int beneath_y = 0;

        // Render all of the labels... first is in the normal place... the rest need to be placed
        // to not overlap...
        for (int i=0;i<labels.size();i++) {
          // Break into multilines
          String strs[] = new String[1]; strs[0] = labels.get(i);
          if (strs[0].length() >= (myrc.ml_threshold-2) && Utils.containsWhiteSpace(strs[0])) { strs = Utils.breakIntoMultiLine(strs[0], myrc.ml_threshold); }

          // Figure out the dimensions
          int txt_h     = Utils.txtH(g2d, strs[0]), 
              max_txt_w = Utils.txtW(g2d, strs[0]); 
          int txt_w     = max_txt_w;
          for (int j=0;j<strs.length;j++) { txt_w = Utils.txtW(g2d, strs[j]); if (txt_w > max_txt_w) max_txt_w = txt_w; }

          // Handle upto four labels...
          int sx = -1, sy = -1;
          if        (labels.size() == 1) {
            sx = base_sx; sy = base_sy;

          } else if (labels.size() == 2) {
            if (i == 0) { sx = base_sx - max_txt_w/2 - 5; sy = base_sy; }
            else        { sx = base_sx + max_txt_w/2 + 5; sy = base_sy; }

          } else if (labels.size() == 3) {
            if      (i == 0) { sx = base_sx - max_txt_w/2 - 5; sy = base_sy + 5; }
            else if (i == 1) { sx = base_sx + max_txt_w/2 + 5; sy = base_sy + 5; }
            else             { sx = base_sx;                   sy = base_sy - 5 - txt_h * strs.length; }

          } else if (labels.size() >= 4) {
            if      (i == 0) { sx = base_sx - max_txt_w/2 - 5; sy = base_sy + 5; if (base_sy+5+txt_h*strs.length > beneath_y) beneath_y = base_sy+5+txt_h*strs.length; }
            else if (i == 1) { sx = base_sx + max_txt_w/2 + 5; sy = base_sy + 5; if (base_sy+5+txt_h*strs.length > beneath_y) beneath_y = base_sy+5+txt_h*strs.length; }
            else if (i == 2) { sx = base_sx - max_txt_w/2 - 5; sy = base_sy - 5 - txt_h * strs.length; }
            else if (i == 3) { sx = base_sx + max_txt_w/2 + 5; sy = base_sy - 5 - txt_h * strs.length; }
          }

          // Actually render...
          if (sx != -1 && sy != -1) {
            for (int j=0;j<strs.length;j++) {
              txt_w = Utils.txtW(g2d,strs[j]);
              clearStr(g2d, strs[j], (int) (sx - txt_w/2), (int) (sy + 2*txt_h + j*txt_h),
                       RTColorManager.getColor("label", "defaultfg"), RTColorManager.getColor("label", "defaultbg"));
            }

          } else {
            String too_many_labels_str = "Too Many Labels... [" + labels.size() + "]";
                   txt_w               = Utils.txtW(g2d, too_many_labels_str);
            clearStr(g2d, too_many_labels_str, base_sx - txt_w/2, beneath_y + 2*txt_h,
                     RTColorManager.getColor("label", "errorfg"), RTColorManager.getColor("label", "errorbg"));
          }
        }
      }
    }

    /**
     * Render the help information.
     *
     *@param g2d graphics primitive
     */
    public void drawHelp(Graphics2D g2d, int txt_h) {
      // Draw the help
      if (draw_help) {
        txt_h += 2;

        // Width of string
        int max_w = 0; for (int i=0;i<help_strs.length;i++) {
          for (int j=0;j<help_strs[i].length;j++) {
            int w = Utils.txtW(g2d, help_strs[i][j]);
            if (w > max_w) max_w = w;
        } }
        max_w += 30;

        // Render the help
        int y = txt_h + 2;
        g2d.setColor(RTColorManager.getColor("label", "default"));
        g2d.drawString("Key",        5,            y);
        g2d.drawString("Normal",     50 + 0*max_w, y);
        g2d.drawString("Shift",      50 + 1*max_w, y);
        g2d.drawString("Ctrl",       50 + 2*max_w, y);
        g2d.drawString("Shift-Ctrl", 50 + 3*max_w, y);
        
        for (int i=0;i<help_strs.length;i++) {
          y += txt_h;
          g2d.setColor(RTColorManager.getColor("label", "default")); 
          g2d.drawString(help_strs[i][0], 5, y);
          g2d.setColor(RTColorManager.getColor("label", "major"));
          for (int j=1;j<help_strs[i].length;j++) { 
            g2d.drawString(help_strs[i][j], 50 + (j-1)*max_w, y); 
          }
        }
      }
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy();
      RenderContext myrc = null;
      myrc = new RenderContext(id, bs, count_by, color_by, 
                               timelineVisualization(), timeMarkers(),
                               entitySize(), entityColor(), showAll(),
                               multilineTruncate(), multilineThreshold(),
                               drawEntityLabels(), listEntityLabels(), listEntityColor(),
                               validate_integrity_cbmi.isSelected(),
                               getWidth(), getHeight());
      return myrc;
    }
    
    /**
     * RenderContext implementation
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by; 


      /**
       * Timeline visualization setting
       */
      boolean               timeline_visualization;

      /**
       * Flag to draw the time markers
       */
      boolean               draw_markers;

      /**
       * Entity size and shape
       */
      EntitySize            entity_size;

      /**
       * Entity color
       */
      EntityColor           entity_color;

      /**
       * Entities that have visible bundles
       */
      Set<String>           non_empty_entities;

      /**
       * Multiline truncation setting
       */
      int                   ml_truncate,

      /**
       * Multiline threshold setting
       */
                            ml_threshold;

      /**
       * Always show all entities regardless of filtering
       */
      boolean              show_all         = false,

      /**
       * Draw the entity labels
       */
                           draw_entity_labels = false;

      /**
       * Labels for the entities
       */
      List<String>         entity_labels = null;

      /**
       * Color (based on a label) for the entities
       */
      String               label_entity_color = null;

      /**
       * Label maker
       */
      LabelMaker           node_lm = null;

      /**
       * Entity to their screen coordinates as a string -- x|y
       */
      public Map<String,String>  entity_to_sxy = new HashMap<String,String>();

      /**
       * Screen coordinate as a string to the x screen coordinate
       */
      public Map<String,Integer> sxy_to_sx = new HashMap<String,Integer>(),

      /**
       * Screen coordinate as a string to the y screen coordinate
       */
                                 sxy_to_sy = new HashMap<String,Integer>();

      /**
       * Validate the integrity of the data structures
       */
      boolean validate_integrity = false;

      /**
       * Render time metrics
       */
      long rtime_ts0, rtime_ts1;

      /**
       * Construct the rendering context for the geospatial histogram
       * with the specified settings.
       *
       *@param id                      render id
       *@param bs                      bundles to render
       *@param count_by                how to count the record contribution to each country
       *@param color_by                color option based on global settings
       *@param timeline_visualization  timeline visualization mode
       *@param draw_makers             flag to draw time markers
       *@param entity_size             entity size of nodes
       *@param entity_color            color scheme to apply to the nodes
       *@param show_all                show all entities even if the underlying records are hidden
       *@param multiline_truncate      multiline truncation setting -- -1 means no truncation
       *@param multiline_threshold     multiline threshold -- number of characters per line
       *@param draw_entity_labels      draw the entity labels
       *@param entity_labels           entity labels to draw
       *@param list_entity_color       entity color to use if color setting is 'by label'
       *@param validate_integrity      run check on internal data structure and provide an overlay on the results (debugging)
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by,
                           boolean timeline_visualization, boolean draw_markers,
                           EntitySize entity_size, EntityColor entity_color,
                           boolean show_all, int multiline_truncate, int multiline_threshold,
                           boolean draw_entity_labels, List<String> entity_labels, List<String> list_entity_color,
                           boolean validate_integrity,
                           int w, int h) {

        rtime_ts0 = System.currentTimeMillis();

        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
        this.count_by               = count_by;
        this.color_by               = color_by;

        this.timeline_visualization = timeline_visualization;
        this.draw_markers           = draw_markers;

        this.entity_size            = entity_size;
        this.entity_color           = entity_color;
        this.show_all               = show_all;

        this.ml_truncate            = multiline_truncate;
        this.ml_threshold           = multiline_threshold;

        this.draw_entity_labels     = draw_entity_labels;
        this.entity_labels          = entity_labels;

        this.validate_integrity     = validate_integrity;
      
        if (list_entity_color.size() > 0) label_entity_color = list_entity_color.get(0);

        // Zeroize the no mapping set
        clearNoMappingSet();

        // Figure out which entities could be rendered...
        non_empty_entities = new HashSet<String>(); Set<Bundle> possible_bundles = new HashSet<Bundle>();
        Iterator<Bundle> it_bun = bs.bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          if (bundle_to_entities.containsKey(bundle)) { 
            possible_bundles.add(bundle);
            non_empty_entities.addAll(bundle_to_entities.get(bundle)); 
          } else addToNoMappingSet(bundle);
        }

        // Transform all the coordinates ... may be able to get away with just those rendered if the
        // always-show-all isn't selected... but that could still cause issues
        Iterator<String> it;
        if (show_all) it = entity_to_wxy.keySet().iterator();
        else          it = non_empty_entities.iterator();
        while (it.hasNext() && currentRenderID() == getRenderID()) {
          String entity = it.next(); Point2D pt = entity_to_wxy.get(entity);
          int    sx     = wxToSx(pt.getX()),
                 sy     = wyToSy(pt.getY());
          String sxy    = sx + "|" + sy;
          entity_to_sxy.put(entity, sxy);
          sxy_to_sx.put(sxy,sx);
          sxy_to_sy.put(sxy,sy);
        }

        // Create the counter contexts...
        sxy_counter_context    = new BundlesCounterContext(bs, count_by, color_by);
        entity_counter_context = new BundlesCounterContext(bs, count_by, color_by);

        // Map those entities to screen coordinates... discard ones that aren't within the viewport
        // ... start counting up bundles at the screen coordinate level
        if (show_all) it = entity_to_wxy.keySet().iterator();
        else          it = non_empty_entities.iterator(); 

        while (it.hasNext() && currentRenderID() == getRenderID()) {
          String entity = it.next(); String sxy = entity_to_sxy.get(entity);
          int sx = sxy_to_sx.get(sxy), sy = sxy_to_sy.get(sxy);

          boolean easy_in       = (sx >= -10 && sx <= getRCWidth()  + 10 && sy >= -10 && sy <= getRCHeight() + 10);
          boolean timeline_in   = false;

          if (timeline_visualization && entity_first_heard.containsKey(entity)) {
            int sx0 = wxToSx(entity_first_heard.get(entity)),
                sx1 = wxToSx(entity_last_heard. get(entity));
            timeline_in = (timeline_visualization && sy >= -10 && sy <= getRCHeight() + 10) &&
                          (sx1 >= 0 && sx0 <= getRCWidth());
          }

          // Within viewport...
          if ((easy_in || timeline_in) && entity_to_bundles.containsKey(entity)) {

            it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
              Bundle bundle = it_bun.next(); if (possible_bundles.contains(bundle)) {
                sxy_counter_context.count(bundle, sxy);
                entity_counter_context.count(bundle, entity);
              }
            }

            if (sxy_to_entities.containsKey(sxy) == false) sxy_to_entities.put(sxy, new HashSet<String>());
            sxy_to_entities.get(sxy).add(entity);
          } 
        }
      }

      /**
       * Filter the supplied set to only the visible ones in the view.
       * ... seems like this is problematic w.r.t. showAll() setting
       *
       *@param superset entities to filter
       *
       *@return filtered set by what is visible
       */
      public Set<String> filterEntities(Set<String> superset) {
        Set<String> return_set = new HashSet<String>(); 
        Iterator<String> it = superset.iterator(); while (it.hasNext()) {
          String str = it.next();
          if (non_empty_entities.contains(str)) return_set.add(str);
        }
        return return_set;
      }

      /**
       * Maps the geometry to the sxy keys
       */
      Map<Shape,String> geom_to_sxy = new HashMap<Shape,String>();

      /**
       * Maps the sxy key to the geometry
       */
      Map<String,Shape> sxy_to_geom = new HashMap<String,Shape>();

      /**
       * Screen coordinate pairs the entities at exactly that point
       */
      Map<String,Set<String>> sxy_to_entities = new HashMap<String,Set<String>>();

      /**
       * BundlesCounterContext for the entities
       */
      BundlesCounterContext entity_counter_context,

      /**
       * BundlesCounterContext for the screen xy keys
       */
                            sxy_counter_context;

      /**
       * Color bin ordering for small multiples that require it
       */
      List<String>          cbins = null;

      /**
       * Node Shaper (and Colorer)
       */
      NodeShaper            node_shaper;

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() {
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);
          int     txt_h   = Utils.txtH(g2d,"0");

          // Calculate the color bins if necessary ... global color bins are necessary to get the small multiples
          // to render cohesively -- i.e., same order of colors from the top to the bottom of the bar charts...
          if (color_by != null && ((entity_size == EntitySize.ByMonth)         ||
                                   (entity_size == EntitySize.ByDayOfWeek)     ||
                                   (entity_size == EntitySize.ByDayOfWeekHour) ||
                                   (entity_size == EntitySize.ByHour)          ||
                                   (entity_size == EntitySize.ContinuousSmall) ||
                                   (entity_size == EntitySize.ContinuousLarge) ||
                                   (entity_size == EntitySize.AutoTime)        || 
                                   (entity_size == EntitySize.PieSmall)        ||
                                   (entity_size == EntitySize.PieLarge))) {
            BundlesCounterContext color_bcc = new BundlesCounterContext(bs, count_by, color_by, color_by);
            cbins = color_bcc.getColorBinsSortedByCount();
          }

          // Draw the background for the timeline visualization
          if (timeline_visualization) { renderTimelineVisualizationBackground(g2d); }

          // Node labels
          node_lm = new LabelMaker(entity_labels, sxy_counter_context);

          // Create the node shaper
          node_shaper = nodeShaper();
          Iterator<String> it = sxy_to_entities.keySet().iterator(); while (it.hasNext()) {
            String sxy = it.next(); 
            ShapeAndColor sac = node_shaper.shapeAndColor(sxy);

            // For labeling
            int shape_x_center = sxy_to_sx.get(sxy), 
                shape_y_bottom = sxy_to_sy.get(sxy);

            // Small multiples version
            if        (sac.sm_bi != null) {
              Rectangle2D r = (Rectangle2D) sac.shape;
              g2d.drawImage(sac.sm_bi, (int) r.getMinX(), (int) r.getMinY(), null);

            // Custom renderer version
            } else if (node_shaper.customRenderer()) {
              g2d.setColor(sac.color);
              node_shaper.customRender(g2d, sac, sxy);

            // User selected shape version
            } else if (sac.shape != null) {
              g2d.setColor(sac.color);
              if (sxy_counter_context.hasBin(sxy)) {
                g2d.fill(sac.shape);
                shape_y_bottom += sac.shape.getBounds().getHeight()/2;
              } else               {
                Composite orig_comp = g2d.getComposite();
                g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
                g2d.draw(sac.shape);
                g2d.setComposite(orig_comp);
                shape_y_bottom = Integer.MIN_VALUE; // Don't draw labels for unfilled entities
              }
            }

            // Draw the node labels // label render
            if (node_lm != null && draw_entity_labels && shape_y_bottom != Integer.MIN_VALUE) { 
              node_lm.draw(g2d, sxy, shape_x_center, (int) sac.shape.getBounds().getMaxY(), true);
            }

            // Save off the geometry to sxy lookup -- for filtering and selection ops
            if (sac.shape != null) { geom_to_sxy.put(sac.shape, sxy); sxy_to_geom.put(sxy, sac.shape); }
          }

          // Draw some indicators of the timeline
          if (timeline_visualization) {
            long t_left  = (long) sxToWx(0),
                 t_right = (long) sxToWx(getRCWidth());

            String ts0_str = Utils.exactDate(t_left),
                   ts1_str = Utils.exactDate(t_right);

            g2d.setColor(RTColorManager.getColor("label", "major"));
            g2d.drawString(ts0_str, 2,                                           getRCHeight() - 2);
            g2d.drawString(ts1_str, getRCWidth() - 2 - Utils.txtW(g2d, ts1_str), getRCHeight() - 2);
            
            String human_dur_str = Utils.humanReadableDuration(t_right - t_left);
            g2d.drawString(human_dur_str, getRCWidth()/2 - Utils.txtW(g2d,human_dur_str)/2, getRCHeight() - 2);

            if (draw_markers) drawMarkersFG(g2d);
          }

          // Validate integrity
          if (validate_integrity) { validateIntegrityCalculationAndInfo(g2d); }

          // Render time metrics
          rtime_ts1 = System.currentTimeMillis();
          g2d.setColor(RTColorManager.getColor("label", "minor"));
          String str = "" + (rtime_ts1 - rtime_ts0) + " ms";
          g2d.drawString(str, getRCWidth() - Utils.txtW(g2d, str) - 2, txt_h * 2 + 2);

         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }

      //
      // vvv -- Here Down Is Copy from RTTimePanel ...
      //

      /**
       * Draw the background for the time markers.
       *
       *@param g2d graphics drawing primitive
       */
      private void drawMarkersBG(Graphics2D g2d) { drawMarkersGeneric(g2d, true); }

      /**
       * Draw the foreground for markers.
       *
       *@param g2d graphics drawing primitive
       */
      public void drawMarkersFG(Graphics2D g2d) { drawMarkersGeneric(g2d, false); }

      /**
       * Draw time markers generically
       *
       *@param g2d graphics drawing primitive
       *@param bg  indicates the background draw so that it doesn't obscure the foreground
       */
      private void drawMarkersGeneric(Graphics2D g2d, boolean bg) {
        int graph_y_ins = 5, graph_h = getRCHeight() - 2*graph_y_ins, w = getRCWidth();

        Set<TimeMarker> markers = getRTParent().getTimeMarkers((long) sxToWx(0),(long) sxToWx(getRCWidth()));

        if (markers != null) {
          Iterator<TimeMarker> it    = markers.iterator();
          // int                  count = 0;
          while (it.hasNext()) {
            TimeMarker tm = it.next();
            g2d.setColor(RTColorManager.getColor(tm.getDescription()));
            if (tm.isTimeStamp()) {
              int sx  = wxToSx(tm.ts0());
              if (bg) {
                g2d.drawLine(sx-5,graph_y_ins  ,sx,graph_y_ins+5);
                g2d.drawLine(sx+5,graph_y_ins  ,sx,graph_y_ins+5);
               g2d.drawLine(sx,  graph_y_ins+5,sx,graph_y_ins+graph_h);
              } else {
                String str = Utils.fitTxt(g2d, tm.getDescription(), graph_h);
                Utils.drawRotatedString(g2d, str, sx, graph_y_ins+graph_h/2+Utils.txtW(g2d,str)/2);
              }
            } else                {
              int sy = (graph_y_ins+graph_h/2) - (tm.getDescription().hashCode() & 0x00ffffff)%(graph_y_ins+graph_h/2) + 10;
              int sx0 = wxToSx(tm.ts0()), sx1 = wxToSx(tm.ts1());
              if (bg) {
                g2d.drawLine(sx0, sy-5, sx0,   sy+5); g2d.drawLine(sx1, sy-5, sx1,   sy+5);
                g2d.drawLine(sx0, sy,   sx1,   sy);
                g2d.drawLine(sx0, sy,   sx0+5, sy-5); g2d.drawLine(sx0, sy,   sx0+5, sy+5);
                g2d.drawLine(sx1, sy,   sx1-5, sy-5); g2d.drawLine(sx1, sy,   sx1-5, sy+5);
                      Composite composite = g2d.getComposite();
                      g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f));
                      g2d.fillRect(sx0, graph_y_ins, sx1 - sx0, graph_h);
                      g2d.setComposite(composite);
              } else {
                if (sx0 < 0) sx0 = 0; if (sx1 > w) sx1 = w;
                String str = Utils.fitTxt(g2d, tm.getDescription(), sx1-sx0);
                g2d.drawString(str, (sx0+sx1)/2 - Utils.txtW(g2d,str)/2, sy);
              }
            }
          }
        }
      }

      //
      // ^^^ -- Here Up Is Copy from RTTimePanel ...
      //

      /**
       * Render the background for the timeline visualization.  Challenge is to figure out what the rendering
       * width can support... the marker distances are straightforward to calculate.
       */
      protected void renderTimelineVisualizationBackground(Graphics2D g2d) {
        long     ts0 = (long) sxToWx(0), 
                 ts1 = (long) sxToWx(getRCWidth()), 
                 dt  = ts1 - ts0;
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT")); 
                 cal.setTimeInMillis(ts0);
                 cal.set(Calendar.MILLISECOND, 0);

        if (draw_markers) drawMarkersBG(g2d);

        // Figure out the start and increment
        int add_field, add_amount;

        // ... by constructing the granularity based on the the timeframe shown
        // ... also have to manipulate the incremental calendar so that the less signficiant fields are all zeroized...
        SimpleDateFormat label_sdf;
        if        (dt <                  10L*60L*1000L) { /* 10 mins */ add_field = Calendar.SECOND;       add_amount = 30;
                                                                        label_sdf   = new SimpleDateFormat("mm:ss");

        } else if (dt <                  60L*60L*1000L) { /* 1 hour  */ add_field = Calendar.MINUTE;       add_amount =  5;
                                                                        label_sdf   = new SimpleDateFormat("hh:mm");
                                                                        cal.set(Calendar.SECOND, 0);
        } else if (dt <              12L*60L*60L*1000L) { /* 1/2 day */ add_field = Calendar.HOUR;         add_amount =  1;
                                                                        label_sdf   = new SimpleDateFormat("hh");
                                                                        cal.set(Calendar.SECOND, 0);
                                                                        cal.set(Calendar.MINUTE, 0);
                                                                        cal.set(Calendar.HOUR,   0);

        } else if (dt <              24L*60L*60L*1000L) { /* 1 day   */ add_field = Calendar.HOUR;         add_amount =  2;
                                                                        label_sdf   = new SimpleDateFormat("hh");
                                                                        cal.set(Calendar.SECOND, 0);
                                                                        cal.set(Calendar.MINUTE, 0);
                                                                        cal.set(Calendar.HOUR,   0);

        } else if (dt <           8L*24L*60L*60L*1000L) { /* 8 days  */ add_field = Calendar.DAY_OF_MONTH; add_amount =  1;
                                                                        label_sdf   = new SimpleDateFormat("MM-dd");
                                                                        cal.set(Calendar.SECOND, 0);
                                                                        cal.set(Calendar.MINUTE, 0);
                                                                        cal.set(Calendar.HOUR,   0);

        } else if (dt <          16L*24L*60L*60L*1000L) { /* 16 days */ add_field = Calendar.DAY_OF_MONTH; add_amount =  2;
                                                                        label_sdf   = new SimpleDateFormat("MM-dd");
                                                                        cal.set(Calendar.SECOND, 0);
                                                                        cal.set(Calendar.MINUTE, 0);
                                                                        cal.set(Calendar.HOUR,   0);

        } else if (dt <          30L*24L*60L*60L*1000L) { /* 1 month */ add_field = Calendar.DAY_OF_MONTH; add_amount = 10;
                                                                        label_sdf   = new SimpleDateFormat("MM-dd");
                                                                        cal.set(Calendar.SECOND,       0);
                                                                        cal.set(Calendar.MINUTE,       0);
                                                                        cal.set(Calendar.HOUR,         0);
                                                                        cal.set(Calendar.DAY_OF_MONTH, 0);

        } else if (dt <   3L*12L*30L*24L*60L*60L*1000L) { /* 3 years */ add_field = Calendar.MONTH;        add_amount =  3;
                                                                        label_sdf   = new SimpleDateFormat("yyyy-MM");
                                                                        cal.set(Calendar.SECOND,       0);
                                                                        cal.set(Calendar.MINUTE,       0);
                                                                        cal.set(Calendar.HOUR,         0);
                                                                        cal.set(Calendar.DAY_OF_MONTH, 1);

        } else if (dt <  20L*12L*30L*24L*60L*60L*1000L) { /* 20 years*/ add_field = Calendar.MONTH;        add_amount =  6;
                                                                        label_sdf   = new SimpleDateFormat("yyyy-MM");
                                                                        cal.set(Calendar.SECOND,       0);
                                                                        cal.set(Calendar.MINUTE,       0);
                                                                        cal.set(Calendar.HOUR,         0);
                                                                        cal.set(Calendar.DAY_OF_MONTH, 1);
                                                                        cal.set(Calendar.MONTH,        0);

        } else                                          { /* lots    */ add_field = Calendar.YEAR;         add_amount = 10; 
                                                                        label_sdf   = new SimpleDateFormat("yyyy");
                                                                        cal.set(Calendar.SECOND,       0);
                                                                        cal.set(Calendar.MINUTE,       0);
                                                                        cal.set(Calendar.HOUR,         0);
                                                                        cal.set(Calendar.DAY_OF_MONTH, 1);
                                                                        cal.set(Calendar.MONTH,        0);
                                                        }

        label_sdf.  setTimeZone(TimeZone.getTimeZone("GMT"));

        int txt_h = Utils.txtH(g2d, "0");

        // Render the lines
        while (wxToSx(cal.getTimeInMillis()) < getRCWidth()) {
          long ts = cal.getTimeInMillis();

          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          int sx = wxToSx(ts);
          g2d.drawLine(sx, 0, sx, getRCHeight());

          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          String str = label_sdf.format(new Date(ts));
          g2d.drawString(str, sx + 2, getRCHeight() - 2 - txt_h);

          cal.add(add_field, add_amount);
        }
      }

      /**
       * Return a node renderer instance based on the user settings.
       *
       *@return node shaper to perform rendering
       */
      public NodeShaper nodeShaper() {
        // Override the other options if the timeline visualization is active
        if (timeline_visualization) return new TimelineNodeShaper();

        // Find the appropriate node shaper
        switch (entity_size) {
          case Large:           return new FixedNodeShaper(10.0f);
          case Small:           return new FixedNodeShaper(4.0f);
          case Vary:            return new VaryNodeShaper();

          case ByMonth: 
          case ByDayOfWeek: 
          case ByHour:      
          case ByDayOfWeekHour: 

          case ContinuousSmall:
          case ContinuousLarge:
          case AutoTime:

          case PieSmall:
          case PieLarge:

          case XYVisible:
          case XYVisibleEqual:
          case XYLocal:
          case XYLocalEqual:    return new SmallMultipleNodeShaper();

          case LinkNode:        // Not implemented yet // Falls through to the SmallMultiplesNodeShaper

          case GPSSmall:
          case GPSMedium:
          case GPSLarge:
          case GPSLocal:

          case CountrySmall:
          case CountryLarge:    return new SmallMultipleNodeShaper();

          default:              return new FixedNodeShaper(10.0f);
        }
      }

      class ShapeAndColor { Shape shape; Color color; BufferedImage sm_bi; }

      /**
       *  NodeShaper abstraction
       */
      abstract class NodeShaper { 
        public abstract ShapeAndColor shapeAndColor(String sxy); 
        public boolean  customRenderer()             { return false; }
        public void     customRender(Graphics2D g2d, ShapeAndColor sac, String sxy) { }
      }

      /**
       * Node shaper for the timeline view
       */
      class TimelineNodeShaper extends NodeShaper {
        /**
         * For the specified coordinate lookup string, return the shape and color of the nodes.
         *
         *@param sxy x and y coordinate as a string
         *
         *@return shape and color of the node
         */
        public ShapeAndColor shapeAndColor(String sxy) {
          ShapeAndColor sac;

          // Iterator over the entities within the pixel
          long ts0 = Long.MAX_VALUE, ts1 = Long.MIN_VALUE;
          Iterator<String> it_entities = sxy_to_entities.get(sxy).iterator(); while (it_entities.hasNext()) {
            String entity = it_entities.next(); if (entity_first_heard.containsKey(entity)) {
              if (entity_first_heard.get(entity) < ts0) ts0 = entity_first_heard.get(entity);
              if (entity_last_heard. get(entity) > ts1) ts1 = entity_last_heard. get(entity);
            }
          }

          //
          // If we matched at least one, then make the geometry fit the timeline ... else default to the fixed node shaper
          //
          if (ts0 != Long.MAX_VALUE) {
            sac = new ShapeAndColor();

            // Adjust the height based on the user setting
            int sm_h = 3; 
            if      (entity_size == EntitySize.TimelineLarge)         sm_h = 10; 
            else if (entity_size == EntitySize.TimelineSmall ||
                     entity_size == EntitySize.TimelineSmallVary ||
                     entity_size == EntitySize.TimelineSmallOverlaps) sm_h = 5;
            else if (entity_size == EntitySize.TimelineBlocks)        sm_h = 3;

            // Geometry is a thin rectangle from first to last heard
            int sx0 = wxToSx(ts0), sx1 = wxToSx(ts1); 
            int sy  = sxy_to_sy.get(sxy);
            if (sx1 == sx0) sx1 = sx0 + 2;
            sac.shape = new Rectangle2D.Double(sx0, sy - sm_h/2, sx1 - sx0, sm_h);

            // Color is per the user's selection
            switch (entity_color) {
              case Vary:    if (sxy_counter_context.hasBin(sxy)) sac.color = sxy_counter_context.binColor(sxy);
                            else                                 sac.color = RTColorManager.getColor("data", "default");
                            break;
              case Label:   if      (label_entity_color == null) sac.color = RTColorManager.getColor("data", "default");
                            else {
                              if (label_color_lm == null) {
                                List<String> as_list = new ArrayList<String>(); as_list.add(label_entity_color);
                                label_color_lm = new LabelMaker(as_list, sxy_counter_context);
                              }
                              sac.color = label_color_lm.getColor(sxy);
                            }
                            break;
              case Default:
              default:      sac.color = RTColorManager.getColor("data", "default");
                            break;
            }

          //
          // For no timestamps, just provide a fixed node shaper
          //
          } else sac = (new FixedNodeShaper(5.0f)).shapeAndColor(sxy);

          return sac;
        }

        @Override
        public boolean  customRenderer()             { return entity_size != EntitySize.TimelineBlocks; }

        @Override
        public void     customRender(Graphics2D g2d, ShapeAndColor sac, String sxy) {
          Rectangle2D bounds    = sac.shape.getBounds();

          // Timeline middle
          // Composite   comp_orig = g2d.getComposite(); g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
          Color color_orig = g2d.getColor(); g2d.setColor(RTColorManager.getColor("axis","minor"));
          g2d.drawLine((int) bounds.getMinX(), (int) bounds.getCenterY(), (int) bounds.getMaxX(), (int) bounds.getCenterY());
          g2d.setColor(color_orig);
          // g2d.setComposite(comp_orig);

          // End points
          g2d.drawLine((int) bounds.getMinX(), (int) bounds.getMinY(), (int) bounds.getMinX(), (int) bounds.getMaxY()-1);
          g2d.drawLine((int) bounds.getMaxX(), (int) bounds.getMinY(), (int) bounds.getMaxX(), (int) bounds.getMaxY()-1);

          // Tablet overlap view option
          Map<Integer,Set<Bundle>> x_to_bundle_set = new HashMap<Integer,Set<Bundle>>();

          // Points in the middle
          if (sxy_counter_context.hasBin(sxy)) { Iterator<Bundle> it = sxy_counter_context.getBundles(sxy).iterator(); while (it.hasNext()) {
            Bundle bundle = it.next(); if (bundle.hasTime()) {
              long t0    = bundle.ts0(), t1    = bundle.ts1();
              int  t0_sx = wxToSx(t0),   t1_sx = wxToSx(t1);

              g2d.fillRect(t0_sx, (int) bounds.getMinY(), t1_sx - t0_sx + 1, (int) bounds.getHeight());

              // Track where different tablets intersect ... if overlaps or color varies
              if (entity_size == EntitySize.TimelineSmallOverlaps || entity_size == EntitySize.TimelineSmallVary) {
                for (int x=t0_sx;x<=t1_sx;x++) { if (x_to_bundle_set.containsKey(x) == false) x_to_bundle_set.put(x, new HashSet<Bundle>()); x_to_bundle_set.get(x).add(bundle); }
              }
            }
          } }

          // Draw indicators where the overlap count exceeds one... or where theres vary colors
          // ... overlap is pixel specific... and is limited to each separate entity -- i.e., cross entity doesn't work even if same pixel
          if (entity_size == EntitySize.TimelineSmallOverlaps || entity_size == EntitySize.TimelineSmallVary) {
            int sy = (int) ((bounds.getMinY() + bounds.getMaxY())/2.0);
            Iterator<Integer> it_x = x_to_bundle_set.keySet().iterator(); while (it_x.hasNext()) {
              int sx = it_x.next(); String sx_str = "" + sx; if (x_to_bundle_set.get(sx).size() > 0) {
                BundlesCounterContext sx_bcc = new BundlesCounterContext(bs, count_by, color_by);
                Iterator<Bundle> it_bun = x_to_bundle_set.get(sx).iterator(); while (it_bun.hasNext()) { sx_bcc.count(it_bun.next(),sx_str); }

                // Vary color
                if (entity_size == EntitySize.TimelineSmallVary) {
                  g2d.setColor(sx_bcc.binColor(sx_str));
                  g2d.fillRect(sx, sy - 2, 1, 5);

                // Draw overlap
                } else if (sx_bcc.total(sx_str) > 1) {
                  g2d.setColor(RTColorManager.getColor("data","mean"));
                  g2d.drawLine(sx,     sy,     sx - 8, sy - 8);
                  g2d.drawLine(sx,     sy,     sx + 8, sy - 8);
                  g2d.drawLine(sx - 8, sy - 8, sx + 8, sy - 8);
                } 
              }
            }
          }
        }
      }

      /**
       * Label maker for color based on label
       */
      LabelMaker label_color_lm = null;

      /**
       * Default node shaper
       */
      class FixedNodeShaper extends NodeShaper {
        float size;
        public FixedNodeShaper(float size) { this.size = size; }
        public ShapeAndColor shapeAndColor(String sxy) {
          ShapeAndColor sac = new ShapeAndColor();
          if (sxy_to_entities.get(sxy).size() > 1) {
            sac.shape = Utils.createClover(sxy_to_sx.get(sxy), sxy_to_sy.get(sxy), size, size);
          } else {
            String entity = sxy_to_entities.get(sxy).iterator().next();
            sac.shape = Utils.shape(Utils.parseSymbol(entity_to_shape.get(entity)),sxy_to_sx.get(sxy) - size/2.0f,sxy_to_sy.get(sxy) - size/2.0f, size);
          }

          switch (entity_color) {
            case Vary:     if (sxy_counter_context.hasBin(sxy)) sac.color = sxy_counter_context.binColor(sxy);
                           else                                 sac.color = RTColorManager.getColor("data", "default");
                           break;
            case Label:    if      (label_entity_color == null) sac.color = RTColorManager.getColor("data", "default");
                           else {
                             if (label_color_lm == null) {
                               List<String> as_list = new ArrayList<String>(); as_list.add(label_entity_color);
                               label_color_lm = new LabelMaker(as_list, sxy_counter_context);
                             }
                             sac.color = label_color_lm.getColor(sxy);
                           }
                           break;
            case Default:
            default:       sac.color = RTColorManager.getColor("data", "default");
                           break;
          }
         
          return sac;
        }
      }

      /**
       * Vary node shaper
       */
      class VaryNodeShaper extends NodeShaper {
        public ShapeAndColor shapeAndColor(String sxy) {
          return (new FixedNodeShaper(5.0f)).shapeAndColor(sxy); // place holder
          /* abc
          ShapeAndColor sac = new ShapeAndColor();
          if (sxy_to_entities.get(sxy).size() > 1) {

          } else {

          }
          return sac;
          */
        }
      }

      /**
       *
       */
      class SmallMultipleNodeShaper extends NodeShaper {
        public ShapeAndColor shapeAndColor(String sxy) {
          ShapeAndColor sac = new ShapeAndColor();

          Set<Bundle> set = sxy_counter_context.getBundles(sxy);

          switch (entity_size) {
            case ByMonth:         sac.sm_bi = SmallMultiples.renderDiscrete(bs, count_by, color_by, set, cbins, KeyMaker.BY_MONTH_STR);          break;
            case ByDayOfWeek:     sac.sm_bi = SmallMultiples.renderDiscrete(bs, count_by, color_by, set, cbins, KeyMaker.BY_DAYOFWEEK_STR);      break;
            case ByHour:          sac.sm_bi = SmallMultiples.renderDiscrete(bs, count_by, color_by, set, cbins, KeyMaker.BY_HOUR_STR);           break;
            case ByDayOfWeekHour: sac.sm_bi = SmallMultiples.renderDiscrete(bs, count_by, color_by, set, cbins, KeyMaker.BY_DAYOFWEEK_HOUR_STR); break;

            case AutoTime:        sac.sm_bi = SmallMultiples.renderAutoTime(bs, count_by, color_by, set, cbins, 64, 30); break;

            case ContinuousSmall: sac.sm_bi = SmallMultiples.renderContinuous(bs, count_by, color_by, set, cbins, 34, 18); break;
            case ContinuousLarge: sac.sm_bi = SmallMultiples.renderContinuous(bs, count_by, color_by, set, cbins, 66, 26); break;

            case PieSmall:        sac.sm_bi = SmallMultiples.renderPie(bs, count_by, color_by, set, cbins, 32); break;
            case PieLarge:        sac.sm_bi = SmallMultiples.renderPie(bs, count_by, color_by, set, cbins, 64); break;

            case XYVisible:       sac.sm_bi = XYRenderer.renderTimeInX(set, 64, 32, bs, true,  noCountByBundles(count_by), false, color_by); break;
            case XYVisibleEqual:  sac.sm_bi = XYRenderer.renderTimeInX(set, 64, 32, bs, true,  noCountByBundles(count_by), true,  color_by); break;
            case XYLocal:         sac.sm_bi = XYRenderer.renderTimeInX(set, 64, 32, bs, false, noCountByBundles(count_by), false, color_by); break;
            case XYLocalEqual:    sac.sm_bi = XYRenderer.renderTimeInX(set, 64, 32, bs, false, noCountByBundles(count_by), true,  color_by); break;

            case GPSSmall:        sac.sm_bi = SmallMultiples.renderGPS    (bs, count_by, color_by, set, getLongitudeField(), getLatitudeField(), getEntityField(), false, 48);  break;
            case GPSMedium:       sac.sm_bi = SmallMultiples.renderGPS    (bs, count_by, color_by, set, getLongitudeField(), getLatitudeField(), getEntityField(), false, 96);  break;
            case GPSLarge:        sac.sm_bi = SmallMultiples.renderGPS    (bs, count_by, color_by, set, getLongitudeField(), getLatitudeField(), getEntityField(), false, 256); break;
            case GPSLocal:        sac.sm_bi = SmallMultiples.renderGPS    (bs, count_by, color_by, set, getLongitudeField(), getLatitudeField(), getEntityField(), true,  96);  break;

            case CountrySmall:    sac.sm_bi = SmallMultiples.renderCountry(bs, count_by, color_by, set, getCountryField(), 96);  break;
            case CountryLarge:    sac.sm_bi = SmallMultiples.renderCountry(bs, count_by, color_by, set, getCountryField(), 192); break;

            // case LinkNode:        // Not implemented yet
          }

          if (sac.sm_bi != null) {
            double w = sac.sm_bi.getWidth(), h = sac.sm_bi.getHeight();
            sac.shape = new Rectangle2D.Double(sxy_to_sx.get(sxy) - w/2, sxy_to_sy.get(sxy) - h/2, w, h);
          } else sac = (new FixedNodeShaper(5.0f)).shapeAndColor(sxy);

          return sac;
        }

        /**
         * Really stupid... there's no easy way to fix how the framework processes the "count by bundles" option
         * as a field... the framework would take more work to figure it out and may make other parts of the application
         * unstable.  This method makes sure the that the "count by buns" can't be passed as a y-axis option for the
         * XY scatterplot small multiples.
         */
        private String noCountByBundles(String str) {
          if (str.equals(BundlesDT.COUNT_BY_BUNS)) return KeyMaker.TABLET_SEP_STR;
          else                                     return str;
        }
      }

      /**
       * Class and subclasses responsible for interpreting the label strings and defining the resulting label.
       */
      class LabelMaker {
        /**
         * List of labels to show
         */
        protected java.util.List<String> labels;

        /**
         * Return the list of labels to make as a string array.
         *
         *@param string array representing labels to make
         */
        public String[] labelsArray() {
          String strs[] = new String[labels.size()];
          for (int i=0;i<strs.length;i++) strs[i] = labels.get(i);
          return strs;
        }

        /**
         * Actual label maker subclasses
         */
        protected LM                     makers[];

        /**
         * Counter context for accumulating information for labeling
         */
        protected BundlesCounterContext  cc;

        /**
         * Construct a new label maker with the list of labels and the pre-calculated
         * counter context.
         *
         *@param labels labels to render
         *@param cc     counter context for nodes
         */
        public LabelMaker(java.util.List<String> labels, BundlesCounterContext cc) {
          this.labels = labels; this.cc = cc; makers = new LM[labels.size()];
          for (int i=0;i<labels.size();i++) makers[i] = createLM(labels.get(i), labels.size() == 1);
        }

        /**
         * Return the color of a specific bin (node coord string probably).
         *
         *@param bin node coordinate
         *
         *@return color for label (?)
         */
        public Color getColor(String bin) {
          Color color = RTColorManager.getColor("set", "multi");
          if (makers.length > 0) {
            color = makers[0].draw(null, bin, 0, 0);
            if (color == null) color = Color.darkGray;
          }
          return color;
        }

        /**
         * For a specific node coordinate (called a bin), render the set of labels from the construction stage.
         *
         *@param g2d            graphics primitive
         *@param bin            node coordinate
         *@param x              x coordinate for rendering labels
         *@param y              y coordinate for rendering labels
         *@param top_justified  determines how the vertical justification will occur
         *
         *@return the shape of the first label
         */
        public Shape draw(Graphics2D g2d, String bin, int x, int y, boolean top_justified) {
          // get txt height
          int txt_h = Utils.txtH(g2d, "0"); 
          // Figure you where to start
          if (top_justified) y += txt_h; 
          // else               y -= (int) (txt_h * makers.length/2 + (txt_h/2) * (makers.length%2));
          else {
            if      (makers.length   == 1) y += txt_h/2;
            else if (makers.length%2 == 0) y -= (makers.length/2 - 1)*txt_h;
            else {
              y += txt_h/2;
              y -= (makers.length/2)*txt_h;
            }
          }

          // adjust for the timeline x coordinate -- it could be off the screen...
          if (timeline_visualization && sxy_to_geom.containsKey(bin)) x = adjustXForTimelineVisualization(x,bin);

          // draw the labels
          Shape shape = null;
          for (int i=0;i<makers.length;i++) if (makers[i] != null) { 
            if (makers[i].draw(g2d, bin, x, y) != null) { // label render
              y += txt_h * makers[i].renderedLines(); 
              if (i == 0) shape = makers[i].getShape(); 
            }
          }
          return shape;
        }

        /**
         * The timeline visualization may have entities only partially on the screen -- for these
         * entities, the labels won't be rendered within the view.  This method adjust the 
         * rendering for the partially visible entities.
         *
         *@param x    x coordinate to adjust
         *@param bin  bin to be labeled
         *
         *@return adjusted x coordinate
         */
        public int adjustXForTimelineVisualization(int x, String bin) {
          if (timeline_visualization && (x < 0 || x > rc_w)) {
            int x0 = (int) sxy_to_geom.get(bin).getBounds().getMinX(), 
                x1 = (int) sxy_to_geom.get(bin).getBounds().getMaxX();
            if      (x0 > 0 && x0 < rc_w) x = (x0 + rc_w)/2;
            else if (x1 > 0 && x1 < rc_w) x = (x1 + 0)/2;
            else                          x = rc_w/2;

            if (x <        20) x =        20;
            if (x > rc_w - 20) x = rc_w - 20;
          }
          return x;
        }

        /**
         * Return the strings for each label maker.
         *
         *@param bin node coordinate to use for the bin
         *
         *@return array of strings for each label maker result
         */
        public String[] toStrings(String bin) {
          String strs[] = new String[makers.length];
          for (int i=0;i<strs.length;i++) strs[i] = makers[i].toString(bin);
          return strs;
        }

        /**
         * Abstract class for making a label.
         */
        abstract class LM { 
          /**
           * Shape of resulting label
           */
          Shape shape = null;

          /**
           * Number of lines rendered... for multiline labels...
           */
          int rendered_lines = 1;

          /**
           * Return the number of lines rendered
           */
          public int renderedLines() { return rendered_lines; }

          /**
           * Render a label to the screen.
           *
           *@param g2d  graphics primitive
           *@param bin  node coordinate for label
           *@param x    x coordinate for label
           *@param y    y coordinate for label
           *
           *@return color of label (hack to get the "color-by-label" to work)
           */
          public abstract Color draw(Graphics2D g2d, String bin, int x, int y); 

          /**
           * Return the string that will be rendered for the specified bin (node coordinate).
           *
           *@param bin node coordinate for label
           *
           *@return actual string to be rendered
           */
          public abstract String toString(String bin);

          /**
           * Return the actual shape of the label.
           *
           *@return label shape
           */
          public Shape getShape() { return shape; }
        }

        /**
         * Label for showing duration information
         */
        protected class DurationLM extends LM {
          static final int DAYS=0,HOURS=1; int granularity=0; public DurationLM(int granularity) { this.granularity = granularity; }

          /**
           *
           */
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            if (cc.hasBin(bin) == false) return RTColorManager.getColor("label", "default"); // Protect against null pointer
            long   delta = timeDelta(bin);
            String str   = makeString(delta);
            Color  color = colorByDelta(delta/makeDiv());
            if (g2d != null) { x -= Utils.txtW(g2d, str)/2; g2d.setColor(color); g2d.drawString(str, x, y); }
            return color;
          }

          BrewerColorScale bcs = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 7, 1);

          private Color colorByDelta(long ts_diff) {
            if      (ts_diff <  0L)    return RTColorManager.getColor("label", "default");
            else if (ts_diff == 0L)    return bcs.atIndex(0);
            else if (ts_diff == 1L)    return bcs.atIndex(1);
            else if (ts_diff <  10L)   return bcs.atIndex(2);
            else if (ts_diff <  50L)   return bcs.atIndex(3);
            else if (ts_diff <  100L)  return bcs.atIndex(4);
            else if (ts_diff <  500L)  return bcs.atIndex(5);
            else                       return bcs.atIndex(6);
          }

          private long makeDiv() {
            long div = 1L;
            if      (granularity == DAYS)  { div = 24L * 60L * 60L * 1000L; }
            else if (granularity == HOURS) { div =       60L * 60L * 1000L; }
            return div;
          }

          private String makeString(long ts_diff) {
            if (ts_diff < 0) return "No Times";
            String desc = "Not Set...";
            if      (granularity == DAYS)  { desc = "Day(s)";  }
            else if (granularity == HOURS) { desc = "Hour(s)"; }
            String str = (ts_diff/makeDiv()) + " " + desc;
            return str;
          }

          private long timeDelta(String bin) {
            if (cc.hasBin(bin) == false) return -1L;
            long ts0 = Long.MAX_VALUE, 
                 ts1 = 0L;
            Iterator<Bundle> it = cc.getBundles(bin).iterator(); while (it.hasNext()) {
              Bundle bundle = it.next(); if (bundle.hasTime()) {
                if (bundle.ts0() < ts0) ts0 = bundle.ts0();
                if (bundle.ts1() > ts1) ts1 = bundle.ts1();
              }
            }
            if (ts0 == Long.MAX_VALUE) return -1L;
            else                       return ts1 - ts0;
          }

          public String toString(String bin) { return makeString(timeDelta(bin)); }
        }

        /**
         * Label for showing time information
         */
        protected class TimeLM extends LM {
          static final int BOTH=0, FIRST=1, LAST=2; int type, prec; public TimeLM(int type, int prec) { this.type = type; this.prec = prec; }
          static final int EXACT=0, MONTHS=1, DAYS=2, GUIDES=3;
          public Color draw(Graphics2D g2d, String bin, int x, int y) { 

            if (cc.hasBin(bin) == false) return RTColorManager.getColor("label", "default"); // Protect against null pointer

            long ts0 = Long.MAX_VALUE, 
                 ts1 = 0L;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                Bundle bundle = it.next(); if (bundle.hasTime()) {
                  if (bundle.ts0() < ts0) ts0 = bundle.ts0();
                  if (bundle.ts1() > ts1) ts1 = bundle.ts1();
                }
              }
            }
            if (ts0 == Long.MAX_VALUE) return null; // No time bundles found
            String ts0_str, ts1_str; Color ts0_color, ts1_color;

            double ts0_fraction = ((double) ts0 - bs.ts0())/(bs.ts1() - bs.ts0()), ts1_fraction = ((double) ts1 - bs.ts0())/(bs.ts1() - bs.ts0());
            if (ts0_fraction < 0.0) ts0_fraction = 0.0; if (ts0_fraction > 1.0) ts0_fraction = 1.0;
            if (ts1_fraction < 0.0) ts1_fraction = 0.0; if (ts1_fraction > 1.0) ts1_fraction = 1.0;

            switch (type) {
              case BOTH: ts0_str = precision(ts0); // Utils.humanReadableDate(ts0);
                         ts1_str = precision(ts1); // Utils.humanReadableDate(ts1); 

                         ts0_color = RTColorManager.getColor("label","default");
                         ts1_color = RTColorManager.getColor("label","default");

                         // Combine the strings together if they are equal...  for the days or months settings
                         if (g2d != null) {
                           if (prec == GUIDES) {
                             if (timelineVisualization()) {
                              Stroke orig_stroke = g2d.getStroke(); 
                              float dashes[] = new float[2]; dashes[0] = 5.0f; dashes[1] = 2.0f;
                              g2d.setStroke(new BasicStroke(0.3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));

                              x = wxToSx(ts0); g2d.drawLine(x, 0, x, getRCHeight());
                              int txt_w = Utils.txtW(g2d,ts0_str); y += txt_w + 5; if ((y + txt_w) >= getRCHeight()-10) y = getRCHeight() - 10 - txt_w;
                              Utils.drawRotatedString(g2d, ts0_str, x, y);

                              x = wxToSx(ts1); g2d.drawLine(x, 0, x, getRCHeight());
                              Utils.drawRotatedString(g2d, ts1_str, x, y);

                              g2d.setStroke(orig_stroke);
                             }
                           } else {
                             if (ts0_str.equals(ts1_str)) {
                               x -= Utils.txtW(g2d, ts0_str)/2;
                               g2d.setColor(RTColorManager.getColor("label", "minor"));
                               g2d.drawString(ts0_str, x, y);
                             } else {
                               x -= Utils.txtW(g2d, ts0_str + " - " + ts1_str)/2;
                               g2d.setColor(ts0_color);   g2d.drawString(ts0_str, x, y); x += Utils.txtW(g2d, ts0_str);
                               g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y); x += Utils.txtW(g2d, " - ");
                               g2d.setColor(ts1_color);   g2d.drawString(ts1_str, x, y);
                             }
                           }
                         }

                         return ts0_color;

              case FIRST: ts0_str = precision(ts0); // Utils.humanReadableDate(ts0);
                          if (g2d != null) { x -= Utils.txtW(g2d, ts0_str + " - ")/2; }
                          ts0_color = RTColorManager.getColor("label", "default");

                          if (g2d != null && prec == GUIDES) {
                            if (timelineVisualization()) {
                              Stroke orig_stroke = g2d.getStroke();
                              float dashes[] = new float[2]; dashes[0] = 5.0f; dashes[1] = 2.0f;
                              g2d.setStroke(new BasicStroke(0.3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));

                              x = wxToSx(ts0); g2d.drawLine(x, 0, x, getRCHeight());
                              int txt_w = Utils.txtW(g2d,ts0_str); y += txt_w + 5; if ((y + txt_w) >= getRCHeight()-10) y = getRCHeight() - 10 - txt_w;
                              Utils.drawRotatedString(g2d, ts0_str, x, y);
                              g2d.setStroke(orig_stroke);
                            }
                          } else if (g2d != null) {
                            g2d.setColor(ts0_color);   g2d.drawString(ts0_str, x, y); x += Utils.txtW(g2d, ts0_str);
                            g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y);
                          }

                          return ts0_color;

              case LAST:  ts1_str = precision(ts1); // Utils.humanReadableDate(ts1);
                          if (g2d != null) { x -= Utils.txtW(g2d, " - " + ts1_str)/2; }
                          ts1_color = RTColorManager.getColor("label", "default");

                          if (g2d != null && prec == GUIDES) {
                            if (timelineVisualization()) {
                              Stroke orig_stroke = g2d.getStroke();
                              float dashes[] = new float[2]; dashes[0] = 5.0f; dashes[1] = 2.0f;
                              g2d.setStroke(new BasicStroke(0.3f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));

                              x = wxToSx(ts1); g2d.drawLine(x, 0, x, getRCHeight());
                              int txt_w = Utils.txtW(g2d,ts1_str); y += txt_w + 5; if ((y + txt_w) >= getRCHeight()-10) y = getRCHeight() - 10 - txt_w;
                              Utils.drawRotatedString(g2d, ts1_str, x, y);
                              g2d.setStroke(orig_stroke);
                            }
                          } else if (g2d != null) {
                            g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y); x += Utils.txtW(g2d, " - ");
                            g2d.setColor(ts1_color);   g2d.drawString(ts1_str, x, y);
                          }

                          return ts1_color;
            }
            return null;
          }

          /**
           * Return the timestamp formatted at the specified precision
           */
          private String precision(long ts) {
            switch (prec) {
              case DAYS:   return Utils.dayDateStr(ts);
              case MONTHS: return Utils.monthDateStr(ts);
              case GUIDES: return adaptiveDateString(ts);
              case EXACT: 
              default:     return Utils.humanReadableDate(ts);
            }
          }

          /**
           * Provide an adaptive string for the caller based on how much of the timeline is visible
           */
          private String adaptiveDateString(long ts) {
            long delta = (long) (sxToWx(getRCWidth()) - sxToWx(0));
            if        (delta < 1000L*60L*60L       ) { return          hhmmss_sdf.format(new Date(ts));
            } else if (delta < 1000L*60L*60L*12L)    { return          hhmm_sdf.  format(new Date(ts));
            } else if (delta < 1000L*60L*60L*24L*7L) { return       dd_hhmm_sdf.  format(new Date(ts));
            } else                                   { return yyyymmdd_sdf.       format(new Date(ts));
            }
          }

          /**
           * Return the rendered string.
           */
          public String toString(String bin) { 
            // Find the min and max time for this vertex
            // Only works for the node version... see the render code to modify it to work for links
            long ts0 = Long.MAX_VALUE, ts1 = 0L; 
            Iterator<Bundle> it = cc.getBundles(bin).iterator();
            while (it.hasNext()) {
              Bundle bundle = it.next(); if (bundle.hasTime()) {
                if (bundle.ts0() < ts0) ts0 = bundle.ts0();
                if (bundle.ts1() > ts1) ts1 = bundle.ts1();
              }
            }

            // Choose the right format
            if (ts0 != Long.MAX_VALUE && ts1 != 0L) {
              switch (type) {
                case FIRST: return precision(ts0);
                case LAST:  return precision(ts1);
                case BOTH:
                default:    return precision(ts0) + " - " + precision(ts1);
              }
            }

            return "No Timestamps";
          }
        }

        /**
         * Label for showing underlying bundle (record) counts
         */
        protected class BundleCountLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            if (cc.hasBin(bin) == false) return RTColorManager.getColor("label", "default"); // Protect against null pointers

            String str; int total;
            str = "" + (total = cc.getBundles(bin).size()) + " Recs";
            Color color = RTColorManager.getLogColor(total);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }

          /**
           *
           */
          public String toString(String bin) { 
            int total;
            total = cc.getBundles(bin).size();
            return "" + total + " Recs";
          }
        }

        /**
         * Label for counting entities within aggregated nodes.
         */
        protected class EntityCountLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            int total;
            String str = "" + (total = sxy_to_entities.get(bin).size()) + " Ents";
            Color color = RTColorManager.getLogColor(total);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }

          /**
           *
           */
          public String toString(String bin) { return "" + sxy_to_entities.get(bin).size() + " Ents"; }
        }

        /**
         * Label for the entity type
         */
        protected class EntityTypeLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str = toString(bin);
            Color  color = RTColorManager.getColor(str);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }
          public String toString(String bin) {
            Map<BundlesDT.DT,Integer> map = typeCounts(bin); StringBuffer sb = new StringBuffer(); Iterator<BundlesDT.DT> it;
            if        (map.keySet().size() == 1) { it = map.keySet().iterator(); sb.append("" + it.next());
            } else if (map.keySet().size() == 2 ||
                       map.keySet().size() == 3) { it = map.keySet().iterator(); sb.append("" + it.next());
                                                   while (it.hasNext()) sb.append("|" + it.next());
            } else sb.append("Multiple Data Types");
            return sb.toString();
          }
          protected Map<BundlesDT.DT,Integer> typeCounts(String bin) {
            Map<BundlesDT.DT,Integer> map = new HashMap<BundlesDT.DT,Integer>();
            Iterator<String> it = sxy_to_entities.get(bin).iterator(); while (it.hasNext()) {
              String       str = it.next();
              BundlesDT.DT dt  = BundlesDT.getEntityDataType(str);
              if (dt != null) { if (map.containsKey(dt) == false) map.put(dt, 1); else map.put(dt, map.get(dt) + 1); }
            }
            return map;
          }
        }

        /**
         * Label for showing just the entity name.
         */
        protected class EntityLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str = toString(bin);
            Color color = RTColorManager.getColor(str);
            if (g2d != null) {
              // Short string ... or can't be broken into separate parts
              if (str.length() < (ml_threshold-2) || Utils.containsWhiteSpace(str) == false) {
                shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                rendered_lines = 1;

              // Break it into separate parts... but no more than say three lines
              } else {
                Area area = new Area();
                String strs[] = Utils.breakIntoMultiLine(str, ml_threshold);

                // Determine if there should be truncation...
                if (ml_truncate == -1 || strs.length <= ml_truncate) { rendered_lines = strs.length; } else { rendered_lines = ml_truncate; }

                for (int i=0;i<rendered_lines;i++) {
                  Shape line_shape = clearStr(g2d, strs[i], x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                  area.add(new Area(line_shape));
                  y += Utils.txtH(g2d, strs[i]);
                }

                // Draw some dots to indicate not everything was rendered...
                if (rendered_lines != strs.length) {
                  Shape line_shape = clearStr(g2d, "...", x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                  area.add(new Area(line_shape));
                  y += Utils.txtH(g2d, "...");
                }
                shape = area;
              }
            }
            return color;
          }
          public String toString(String bin) { 
            if (sxy_to_entities.get(bin).size() == 1) return sxy_to_entities.get(bin).iterator().next();
            else                                     return Utils.calculateCIDR(sxy_to_entities.get(bin));
          }
        }

        /**
         * Label for IP Ranges ... private vs public... some other random classes based
         * on the wikipedia page:  https://en.wikipedia.org/wiki/IP_address
         */
        protected class IPLabelLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str;
            if (sxy_to_entities.get(bin).size() == 1) {
              str = Utils.removeUniquifier(sxy_to_entities.get(bin).iterator().next());
            } else str = Utils.calculateCIDR(sxy_to_entities.get(bin));

            String label = ipLabel(str);

            Color color = RTColorManager.getColor(label);
            if (g2d != null) shape = clearStr(g2d, label, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
            
          }

          public String toString(String bin) {
            String str;
            if (sxy_to_entities.get(bin).size() == 1) { 
              str = Utils.removeUniquifier(sxy_to_entities.get(bin).iterator().next());
            } else str = Utils.calculateCIDR(sxy_to_entities.get(bin));
            return ipLabel(str);
          }

          protected String ipLabel(String str) {
            if        (Utils.isIPv4(str))       { return Utils.ipSpaceLabel(str); // IPv4 address
            } else if (str.indexOf(" - ") >= 0) { // might be an IP range ... // returned by the Utils.calculateCIDR() method...
              int i = str.indexOf(" - "); 
              String s1 = str.substring(0,i), 
                     s2 = str.substring(i+3,str.length());
              if (Utils.isIPv4(s1) && Utils.isIPv4(s2)) return "IP Range";
              else                                      return "Not An IP Address";
            } else if (Utils.isIPv6(str))       { return Utils.ipSpaceLabel(str); // IPv6 address
            } else return "Not An IP Address";
          }
        }

        /**
         * Generic caching label maker for handling sets of labels.
         */
        protected abstract class CacheLM extends LM {
          String fld; int fld_i; public CacheLM(String fld) { this.fld = fld; fld_i = getRTParent().getRootBundles().getGlobals().fieldIndex(fld); }
          Map<Tablet,KeyMaker> km_lu    = new HashMap<Tablet,KeyMaker>();
          Set<Tablet>          ignore   = new HashSet<Tablet>();
          //
          public String[] stringKeys(Bundle bundle) {
            if (ignore.contains(bundle.getTablet())) return null;
            if (km_lu.containsKey(bundle.getTablet()) == false) {
              // System.err.println("CacheLM.stringKeys(): fld = \"" + fld + "\"");
              if (KeyMaker.tabletCompletesBlank(bundle.getTablet(), fld)) {
                km_lu.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), fld));
              } else { ignore.add(bundle.getTablet()); return null; }
            }
            return km_lu.get(bundle.getTablet()).stringKeys(bundle);
            }
          //
          public int[]    intKeys(Bundle bundle) {
          if (ignore.contains(bundle.getTablet())) return null;
          if (km_lu.containsKey(bundle.getTablet()) == false) {
            if (KeyMaker.tabletCompletesBlank(bundle.getTablet(), fld)) {
              km_lu.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), fld));
            } else { ignore.add(bundle.getTablet()); return null; }
          }
          return km_lu.get(bundle.getTablet()).intKeys(bundle);
          }
        }

        /**
         * Simple statistic labels (min/max/sum).
         */
        protected class SimpleStatLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public SimpleStatLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE; long sum = 0L;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                int ints[] = intKeys(it.next()); if (ints == null) continue;
                for (int i=0;i<ints.length;i++) {
                  if (min > ints[i]) min = ints[i];
                  if (max < ints[i]) max = ints[i];
                  sum += ints[i];
                }
              }
            }
            String str = (single ? "" : fld + " ") + min + "/" + max + "/" + sum;
            if (g2d != null) {
              x -= Utils.txtW(g2d, str)/2;
              if (single == false) {
                g2d.setColor(RTColorManager.getColor("label", "default")); g2d.drawString(fld + " ", x, y); x += Utils.txtW(g2d, fld + " ");
              }
              g2d.setColor(RTColorManager.getColor("data",  "min"));     g2d.drawString("" + min,  x, y); x += Utils.txtW(g2d, "" + min);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",       x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "max"));     g2d.drawString("" + max,  x, y); x += Utils.txtW(g2d, "" + max);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",       x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "sum"));     g2d.drawString("" + sum,  x, y);
            }
            return RTColorManager.getLogColor(sum);
          }
          public String toString(String bin) { return "Not Implemented (SimpleStatsLM)"; }
        }

        /**
         * Complex stats label maker (mean/avg/stdev).
         */
        protected class ComplexStatLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public ComplexStatLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            List<Integer> al = new ArrayList<Integer>(); double sum = 0.0;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                int ints[] = intKeys(it.next()); if (ints == null) continue;
                for (int i=0;i<ints.length;i++) { al.add(ints[i]); sum += ints[i]; }
              }
            }
            if (al.size() == 0) return null;
            double avg   = sum/al.size();
            double stdev = Utils.calculateStandardDeviation(al,avg);
            Collections.sort(al);
            int   median   = al.get(al.size()/2);

            String median_str = "" + median,
                   avg_str    = Utils.humanReadableDouble(avg),
                   stdev_str  = Utils.humanReadableDouble(stdev);

            String str = (single ? "" : fld + " ") + median_str + "/" + avg_str + "/" + stdev_str;
            if (g2d != null) {
              x -= Utils.txtW(g2d, str)/2;
              if (single == false) {
                g2d.setColor(RTColorManager.getColor("label", "default")); g2d.drawString(fld + " ",        x, y); x += Utils.txtW(g2d, fld + " | ");
              }
              g2d.setColor(RTColorManager.getColor("data",  "median"));  g2d.drawString("" + median_str,  x, y); x += Utils.txtW(g2d, median_str);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",              x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "mean"));    g2d.drawString("" + avg_str,     x, y); x += Utils.txtW(g2d, avg_str);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",            x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("label", "stdev"));   g2d.drawString("" + stdev_str, x, y);
            }
            return RTColorManager.getLogColor(avg);
          }
          public String toString(String bin) { return "Not Implemented (ComplexStatsLM)"; }
        }

        /**
         * Label maker for handling sets of labels.
         */
        protected class ItemsLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public ItemsLM(String fld, boolean single) { super(fld); this.single = single;}
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Map<String,Integer> counter = new HashMap<String,Integer>();
            // Count elements
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { 
                  for (int i=0;i<strs.length;i++) {
                    if (counter.containsKey(strs[i]) == false) counter.put(strs[i], 1);
                    else                                       counter.put(strs[i], counter.get(strs[i]) + 1);
                  }
                }
              }
            }
            // Place them in an array and sort
            StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
            Iterator<String> it_str = counter.keySet().iterator();
            for (int i=0;i<sorter.length;i++) {
              String str = it_str.next(); sorter[i] = new StrCountSorter(str, counter.get(str));
            }
            Arrays.sort(sorter);
            // Put the three most common
            StringBuffer sb = new StringBuffer(); if (!single) sb.append(fld + " | ");
            // for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
            for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " ");
            if (sorter.length > 2) sb.append("... [" + sorter.length + " To]");
            // Draw the string
            if (g2d != null && sb.toString().equals(BundlesDT.NOTSET + " ") == false) shape = clearStr(g2d, sb.toString(), x, y, RTColorManager.getColor(fld), RTColorManager.getColor("label", "defaultbg"), true);
            return (sorter.length == 1 ? RTColorManager.getColor(sorter[0].toString()) : RTColorManager.getColor("set", "multi"));
          }
          public String toString(String bin) { return "Not Implemented (ItemsLM)"; }
        }

        /**
         * Label maker for counting the size of sets.
         */
        protected class CountLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public CountLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Set<String> set = new HashSet<String>();
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { for (int i=0;i<strs.length;i++) set.add(strs[i]); }
              }
            }
            if (g2d != null) shape = clearStr(g2d, (single ? "" : fld + " | ") + set.size() + " Els", 
                                              x, y, RTColorManager.getColor(fld), RTColorManager.getColor("label", "defaultbg"), true);
            return RTColorManager.getLogColor(set.size());
          }
          public String toString(String bin) { return "Not Implemented (CountLM)"; }
        }

        /**
         * Label maker for tag fields.
         */
        protected class TagsLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            // Initialize variables
            StringBuffer sb = new StringBuffer(); Iterator<String> it, it_tags; ColStr colstr = new ColStr();
            long ts0 = bs.ts0(), ts1 = bs.ts1();

            // Determine if one entity or many
            if (sxy_to_entities.get(bin).size() == 1) {
              Set<String> tags = getRTParent().getEntityTags(Utils.removeUniquifier(sxy_to_entities.get(bin).iterator().next()),ts0,ts1);
              it_tags = tags.iterator();
              while (it_tags.hasNext()) { if (sb.length() > 0) sb.append(BundlesDT.DELIM); sb.append(it_tags.next()); }
              colstr.col = RTColorManager.getColor(sb.toString());
            } else                                   {
              // Go through the entities, accumulating the tags
              int entity_count = sxy_to_entities.get(bin).size();
              it = sxy_to_entities.get(bin).iterator();  Map<String,Integer> map = new HashMap<String,Integer>();
              while (it.hasNext()) {
                String      entity = Utils.removeUniquifier(it.next());
                Set<String> tags   = getRTParent().getEntityTags(entity, ts0, ts1);
                // Count the tags if they exist
                if (tags != null && tags.size() > 0) {
                  it_tags = tags.iterator();
                  while (it_tags.hasNext()) {
                    String tag = it_tags.next();
                    if (map.containsKey(tag) == false) map.put(tag, 1); else map.put(tag, map.get(tag)+1);
                  }
                }
              }
              // put the string buffer together
              if        (map.keySet().size() == 1) {
                it = map.keySet().iterator(); String tag = it.next(); int count = map.get(tag);
                if (count == entity_count) sb.append(tag); else sb.append(tag + " (" + count + ")");
                colstr.col = RTColorManager.getColor(tag);
              } else if (map.keySet().size() >= 2) {
                // Sort the counts
                StrCountSorter sorter[] = new StrCountSorter[map.keySet().size()];
                it = map.keySet().iterator();
                for (int i=0;i<sorter.length;i++) {
                  String tag = it.next();
                  sorter[i] = new StrCountSorter(tag, map.get(tag));
                }
                Arrays.sort(sorter);
                // Add the first two to the string
                sb.append(sorter[0].toString()); if (sorter[0].count() != entity_count) sb.append(" (" + sorter[0].count() + ")");
                sb.append(" " + BundlesDT.DELIM + " ");
                sb.append(sorter[1].toString()); if (sorter[1].count() != entity_count) sb.append(" (" + sorter[1].count() + ")");
                if (sorter.length > 2) sb.append(" ... [" + sorter.length + " To]");
                colstr.col = RTColorManager.getColor("set", "multi");
              }
            }
            if (sb.length() > 0) {
              colstr.str = sb.toString();
              if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, RTColorManager.getColor("label", "default"), RTColorManager.getColor("label", "defaultbg"), true);
            }
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label maker for a specific tag type in type-value pairs.
         */
        protected class TagTypesLM extends LM {
          String tag_type;
          public TagTypesLM(String tag_type) { this.tag_type = tag_type; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            // Initialize variables
            StringBuffer sb = new StringBuffer(); Iterator<String> it, it_tags; ColStr colstr = new ColStr();
            long ts0 = bs.ts0(), ts1 = bs.ts1();

            // Determine if one entity or many
            if (sxy_to_entities.get(bin).size() == 1) {
              int count = 0; // Keep track of the number of tags for the color
              // Get the tags for this entity and iterate over them
              Set<String> tags = getRTParent().getEntityTags(Utils.removeUniquifier(sxy_to_entities.get(bin).iterator().next()),ts0,ts1);
              // For each tag, determine if it matches the specified type -- if so, accumulate them into a string buffer
              it_tags = tags.iterator();
              while (it_tags.hasNext()) { 
                String tag = it_tags.next();
                if (Utils.tagIsTypeValue(tag)) {
                  String sep[] = Utils.separateTypeValueTag(tag);
                  if (sep[0].equals(tag_type)) {
                    if (sb.length() > 0) sb.append(BundlesDT.DELIM); 
                    sb.append(sep[1]); count++;
                  }
                }
              }
              // Pick the color based on the number of types assigned to this entity
              if      (count == 0) colstr.col = RTColorManager.getColor("label", "minor"); // Need to also change the color in the force directed color layout (i think...)
              else                 colstr.col = RTColorManager.getColor(sb.toString());

              // Assign the string
              colstr.str = sb.toString();
            } else                                   {
              // Go through the entities, accumulating the tags
              int entity_count = sxy_to_entities.get(bin).size();
              it = sxy_to_entities.get(bin).iterator();  Map<String,Integer> map = new HashMap<String,Integer>();
              while (it.hasNext()) {
                String      entity = Utils.removeUniquifier(it.next());
                Set<String> tags   = getRTParent().getEntityTags(entity, ts0, ts1);
                // Count the tags if they exist
                if (tags != null && tags.size() > 0) {
                  it_tags = tags.iterator();
                  while (it_tags.hasNext()) {
                    String tag = it_tags.next();
                    if (Utils.tagIsTypeValue(tag)) {
                      String sep[] = Utils.separateTypeValueTag(tag);
                      if (sep[0].equals(tag_type)) {
                        if (map.containsKey(sep[1]) == false) map.put(sep[1], 1); else map.put(sep[1], map.get(sep[1])+1);
                      }
                    }
                  }
                }
              }
              // put the string buffer together
              if        (map.keySet().size() == 1) {
                it = map.keySet().iterator(); String tag = it.next(); int count = map.get(tag);
                if (count == entity_count) sb.append(tag); else sb.append(tag + " (" + count + ")");
                colstr.col = RTColorManager.getColor(tag);
              } else if (map.keySet().size() >= 2) {
                // Sort the counts
                StrCountSorter sorter[] = new StrCountSorter[map.keySet().size()];
                it = map.keySet().iterator();
                for (int i=0;i<sorter.length;i++) {
                  String tag = it.next();
                  sorter[i] = new StrCountSorter(tag, map.get(tag));
                }
                Arrays.sort(sorter);
                // Add the first two to the string
                sb.append(sorter[0].toString()); if (sorter[0].count() != entity_count) sb.append(" (" + sorter[0].count() + ")");
                sb.append(" " + BundlesDT.DELIM + " ");
                sb.append(sorter[1].toString()); if (sorter[1].count() != entity_count) sb.append(" (" + sorter[1].count() + ")");
                if (sorter.length > 2) sb.append(" ... [" + sorter.length + " To]");
                colstr.col = RTColorManager.getColor("set", "multi");
              }
              colstr.str = sb.toString();
            }
            if (sb.length() > 0) {
              colstr.str = sb.toString();
              if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            }
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label maker for post processed information.
         */
        protected class LinkPostLM extends CacheLM {
          PostProc proc; boolean single;
          public LinkPostLM(String field, String postproc, BundlesG globals, boolean single) { 
            super(field); proc = BundlesDT.createPostProcessor(postproc, globals); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Map<String,Integer> counter = new HashMap<String,Integer>(); int no_match = 0;
            // Count elements
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { 
                  for (int i=0;i<strs.length;i++) {
                    if (proc.type().contains(BundlesDT.getEntityDataType(strs[i])) == false) { no_match++; }
                    String post[] = proc.postProcess(strs[i]);
                    for (int j=0;j<post.length;j++) {
                      if (counter.containsKey(post[j]) == false) counter.put(post[j], 1);
                      else                                       counter.put(post[j], counter.get(post[j]) + 1);
                    }
                  }
                }
              }
            }
            // Place them in an array and sort
            StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
            Iterator<String> it_str = counter.keySet().iterator();
            for (int i=0;i<sorter.length;i++) {
              String str = it_str.next(); sorter[i] = new StrCountSorter(str, counter.get(str));
            }
            Arrays.sort(sorter);
            // Put the three most common
            StringBuffer sb = new StringBuffer(); if (single == false) sb.append(fld + " | ");
            // for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
            for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " ");
            if (sorter.length > 2) sb.append("... [" + sorter.length + " To]");
            if (no_match > 0) sb.append(" [" + no_match + " NoM]");
            // Draw the string
            Color color = (sorter.length == 1 ? RTColorManager.getColor(sorter[0].toString()) : RTColorManager.getColor("set", "multi"));
            if (g2d != null) shape = clearStr(g2d, sb.toString(), x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }
          public String toString(String bin) { return "Not Implemented (LinkPostLM)"; }
        }

        /**
         * Label maker for post processed node information.
         */
        protected class NodePostLM extends LM {
          PostProc proc;
          public NodePostLM(String postproc, BundlesG globals) { proc = BundlesDT.createPostProcessor(postproc, globals); }
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            ColStr colstr = new ColStr();
            if (sxy_to_entities.get(bin).size() == 1) {
              colstr.str = Utils.removeUniquifier(sxy_to_entities.get(bin).iterator().next());
              if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                String       post[] = proc.postProcess(colstr.str);
                if (post == null || post.length == 0 || (post.length == 1 && post[0].equals(BundlesDT.NOTSET))) return colstr;
                StringBuffer sb = new StringBuffer();
                sb.append(post[0]); for (int i=1;i<post.length;i++) sb.append(BundlesDT.DELIM + post[i]);
                colstr.str = sb.toString(); colstr.col = RTColorManager.getColor(colstr.str);
              }  else return colstr;
            } else                                   {
              Map<String,Integer> counter = new HashMap<String,Integer>(); int no_match = 0;
              Iterator<String> it = sxy_to_entities.get(bin).iterator();
              while (it.hasNext()) {
                colstr.str = Utils.removeUniquifier(it.next());
                if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                  String post[] = proc.postProcess(colstr.str);
                  for (int i=0;i<post.length;i++) {
                    if (counter.containsKey(post[i])) counter.put(post[i],counter.get(post[i])+1); else counter.put(post[i],1);
                  }
                } else no_match++;
              }
              // Place them in an array and sort
              StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
              Iterator<String> it_str = counter.keySet().iterator();
              for (int i=0;i<sorter.length;i++) {
                colstr.str = it_str.next(); sorter[i] = new StrCountSorter(colstr.str, counter.get(colstr.str));
              }
              Arrays.sort(sorter);
              // Put the three most common
              StringBuffer sb = new StringBuffer();
              for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
              if (sorter.length > 2) sb.append("... [" + sorter.length + "]");
              if (no_match > 0 && sb.length() > 0) sb.append(" + " + no_match + "NoM"); // Only add no-match when there's a string...
              colstr.str = sb.toString();
              if (sorter.length == 1) colstr.col = Utils.strColor(sorter[0].toString()); else colstr.col = RTColorManager.getColor("set", "multi");
            }
            if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label maker for post processed node information.
         */
        protected class NodePostLMBeforeJustIP extends LM {
          PostProc proc;
          public NodePostLMBeforeJustIP(String postproc, BundlesG globals) { proc = BundlesDT.createPostProcessor(postproc, globals); }
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            ColStr colstr = new ColStr();
            if (sxy_to_entities.get(bin).size() == 1) {
              colstr.str = sxy_to_entities.get(bin).iterator().next();
              if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                String       post[] = proc.postProcess(colstr.str);
                if (post == null || post.length == 0 || (post.length == 1 && post[0].equals(BundlesDT.NOTSET))) return colstr;
                StringBuffer sb = new StringBuffer();
                sb.append(post[0]); for (int i=1;i<post.length;i++) sb.append(BundlesDT.DELIM + post[i]);
                colstr.str = sb.toString(); colstr.col = RTColorManager.getColor(colstr.str);
              }  else return colstr;
            } else                                   {
              Map<String,Integer> counter = new HashMap<String,Integer>(); int no_match = 0;
              Iterator<String> it = sxy_to_entities.get(bin).iterator();
              while (it.hasNext()) {
                colstr.str = it.next();
                if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                  String post[] = proc.postProcess(colstr.str);
                  for (int i=0;i<post.length;i++) {
                    if (counter.containsKey(post[i])) counter.put(post[i],counter.get(post[i])+1); else counter.put(post[i],1);
                  }
                } else no_match++;
              }
              // Place them in an array and sort
              StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
              Iterator<String> it_str = counter.keySet().iterator();
              for (int i=0;i<sorter.length;i++) {
                colstr.str = it_str.next(); sorter[i] = new StrCountSorter(colstr.str, counter.get(colstr.str));
              }
              Arrays.sort(sorter);
              // Put the three most common
              StringBuffer sb = new StringBuffer();
              for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
              if (sorter.length > 2) sb.append("... [" + sorter.length + "]");
              if (no_match > 0 && sb.length() > 0) sb.append(" + " + no_match + "NoM"); // Only add no-match when there's a string...
              colstr.str = sb.toString();
              if (sorter.length == 1) colstr.col = Utils.strColor(sorter[0].toString()); else colstr.col = RTColorManager.getColor("set", "multi");
            }
            if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Simple structure to hold both a string and a color.
         */
        private class ColStr { public String str; public Color col; public ColStr() { str = "0"; col = RTColorManager.getColor("label", "minor"); } }

        /**
         * From a label string, return the appropriate label maker.
         *
         *@param label labeling option
         *
         *@return label maker
         */
        protected LM createLM(String label, boolean single) {
          BundlesG globals = getRTParent().getRootBundles().getGlobals();
          if      (label.equals(TIMEFRAME_LM))        return new TimeLM(TimeLM.BOTH,  TimeLM.EXACT);
          else if (label.equals(FIRSTHEARD_LM))       return new TimeLM(TimeLM.FIRST, TimeLM.EXACT);
          else if (label.equals(LASTHEARD_LM))        return new TimeLM(TimeLM.LAST,  TimeLM.EXACT);

          else if (label.equals(TIMEFRAME_MONTHS_LM)) return new TimeLM(TimeLM.BOTH,  TimeLM.MONTHS);
          else if (label.equals(TIMEFRAME_DAYS_LM))   return new TimeLM(TimeLM.BOTH,  TimeLM.DAYS);

          else if (label.equals(TIMELASTGUIDES_LM))   return new TimeLM(TimeLM.LAST,  TimeLM.GUIDES);
          else if (label.equals(TIMEBOTHGUIDES_LM))   return new TimeLM(TimeLM.BOTH,  TimeLM.GUIDES);
          else if (label.equals(TIMEFIRSTGUIDES_LM))  return new TimeLM(TimeLM.FIRST, TimeLM.GUIDES);

          else if (label.equals(DURATION_DAYS_LM))    return new DurationLM(DurationLM.DAYS);
          else if (label.equals(DURATION_HOURS_LM))   return new DurationLM(DurationLM.HOURS);

          else if (label.equals(BUNDLECOUNT_LM))   return new BundleCountLM();
          else if (label.equals(TAGS_LM))          return new TagsLM();
          else if (label.startsWith(TAG_TYPE_LM))  return new TagTypesLM(label.substring(TAG_TYPE_LM.length(),label.length()));

          else if (label.equals(ENTITYTYPE_LM))    return new EntityTypeLM();
          else if (label.equals(ENTITYCOUNT_LM))   return new EntityCountLM();
          else if (label.equals(ENTITY_LM))        return new EntityLM();

          else if (label.equals(IP_LABEL_LM))      return new IPLabelLM();

          else if (label.endsWith(SIMPLESTAT_LM))  return new SimpleStatLM (label.substring(0,label.indexOf(BundlesDT.DELIM)),     single);
          else if (label.endsWith(COMPLEXSTAT_LM)) return new ComplexStatLM(label.substring(0,label.indexOf(BundlesDT.DELIM)),     single);
          else if (label.endsWith(ITEMS_LM))       return new ItemsLM      (label.substring(0,label.lastIndexOf(BundlesDT.DELIM)), single);
          else if (label.endsWith(COUNT_LM))       return new CountLM      (label.substring(0,label.lastIndexOf(BundlesDT.DELIM)), single);
          else if (label.indexOf(BundlesDT.DELIM) >= 0) {
            String   start   = label.substring(0,label.indexOf(BundlesDT.DELIM)),
                     rest    = label.substring(label.indexOf(BundlesDT.DELIM)+1,label.length());
            if (globals.fieldIndex(start) != -1) return new LinkPostLM(start,rest,globals,single);
            else                                 return new NodePostLM(label,globals);
          } else {
            return new NodePostLM(label,globals);
          }
        }
      }

      /**
       * Validates the integrity of the data structures in the visualization.
       *
       *@param g2d graphics primitive
       */
      private void validateIntegrityCalculationAndInfo(Graphics2D g2d) {
        int txt_h             = Utils.txtH(g2d, "0");
        int s1_missing_bundle = 0,
            s1_missing_entity = 0,
            s2_missing_entity = 0,
            s2_missing_bundle = 0,
            missing_wxy       = 0,
            missing_shape     = 0;

        Iterator<String> it_str = entity_to_bundles.keySet().iterator(); while (it_str.hasNext()) {
          String entity = it_str.next();
          Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next();
            if      (bundle_to_entities.containsKey(bundle)          == false) s1_missing_bundle++;
            else if (bundle_to_entities.get(bundle).contains(entity) == false) s1_missing_entity++;
          }

          if (entity_to_wxy.containsKey(entity)   == false) missing_wxy++;
          if (entity_to_shape.containsKey(entity) == false) missing_shape++;
        }

        Iterator<Bundle> it_bun = bundle_to_entities.keySet().iterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          it_str = bundle_to_entities.get(bundle).iterator(); while (it_str.hasNext()) {
            String entity = it_str.next();
            if      (entity_to_bundles.containsKey(entity)          == false) s2_missing_entity++;
            else if (entity_to_bundles.get(entity).contains(bundle) == false) s2_missing_bundle++;
          }
        }
        
        int y = txt_h * 4;
        if (s1_missing_bundle > 0) g2d.setColor(Color.red); else g2d.setColor(Color.green);
        g2d.drawString("" + s1_missing_bundle + " S1 Missing Bundle(s)", 5, y); y+= txt_h;
        if (s1_missing_entity > 0) g2d.setColor(Color.red); else g2d.setColor(Color.green);
        g2d.drawString("" + s1_missing_entity + " S2 Missing Entity(s)", 5, y); y+= txt_h;
        if (s2_missing_bundle > 0) g2d.setColor(Color.red); else g2d.setColor(Color.green);
        g2d.drawString("" + s2_missing_bundle + " S2 Missing Bundle(s)", 5, y); y+= txt_h;
        if (s2_missing_entity > 0) g2d.setColor(Color.red); else g2d.setColor(Color.green);
        g2d.drawString("" + s2_missing_entity + " S2 Missing Entity(s)", 5, y); y+= txt_h;

        y += txt_h;
        
        if ((entity_to_bundles.keySet().size()) != (entity_to_wxy.keySet().size())) g2d.setColor(Color.red); else g2d.setColor(Color.green);
        g2d.drawString("" + (entity_to_bundles.keySet().size()) + " ?= " + (entity_to_wxy.keySet().size()), 5, y); y += txt_h;

        g2d.setColor(Color.lightGray);
        g2d.drawString("" + user_enterred_entities.size() + " User Enterred",  5, y); y += txt_h;
        g2d.drawString("" + manuallyAdded()               + " Manually Added", 5, y); y += txt_h;

        y += txt_h;
        g2d.setColor(Color.white);
        g2d.drawString("" + (entities_added_list.size()) + " Schema(s) Active", 5, y); y += txt_h;
        g2d.setColor(Color.lightGray);
        for (int i=0;i<entities_added_list.size();i++) { g2d.drawString("" + entities_added_list.get(i), 5, y); y+= txt_h; }
      }
    }
  }

  /**
   * Strings for the variety of non-application specific labeling options.
   */
  public final static String TIMEFRAME_LM            = BundlesDT.DELIM + "Time Frame"   + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             TIMEFRAME_MONTHS_LM     = BundlesDT.DELIM + "TF Months"    + BundlesDT.DELIM, // 
                             TIMEFRAME_DAYS_LM       = BundlesDT.DELIM + "TF Days"      + BundlesDT.DELIM, // 
                             LASTHEARD_LM            = BundlesDT.DELIM + "Last Heard"   + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             FIRSTHEARD_LM           = BundlesDT.DELIM + "First Heard"  + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             TIMELASTGUIDES_LM       = BundlesDT.DELIM + "Last Tm Guides"  + BundlesDT.DELIM,
                             TIMEBOTHGUIDES_LM       = BundlesDT.DELIM + "Both Tm Guides"  + BundlesDT.DELIM,
                             TIMEFIRSTGUIDES_LM      = BundlesDT.DELIM + "First Tm Guides" + BundlesDT.DELIM,
                             DURATION_HOURS_LM       = BundlesDT.DELIM + "Dur (Hours)"  + BundlesDT.DELIM,
                             DURATION_DAYS_LM        = BundlesDT.DELIM + "Dur (Days)"   + BundlesDT.DELIM,
                             BUNDLECOUNT_LM          = BundlesDT.DELIM + "Bundle Count" + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             TAGS_LM                 = BundlesDT.DELIM + "All Tags"     + BundlesDT.DELIM,
                             TAG_TYPE_LM             = BundlesDT.DELIM + "Tag Type"     + BundlesDT.DELIM,
                             ENTITYTYPE_LM           = BundlesDT.DELIM + "Entity Type"  + BundlesDT.DELIM, // Node String (bin)
                             ENTITYCOUNT_LM          = BundlesDT.DELIM + "Entity Count" + BundlesDT.DELIM, // Node String (bin)
                             ENTITY_LM               = BundlesDT.DELIM + "Entity"       + BundlesDT.DELIM, // Node String (bin)
                             SIMPLESTAT_LM           = BundlesDT.DELIM + "Min/Max/Sum",                    // Bundle Set  <= CounterContext <= Bin
                             COMPLEXSTAT_LM          = BundlesDT.DELIM + "Med/Avg/StDev",                  // Bundle Set  <= CounterContext <= Bin
                             ITEMS_LM                = BundlesDT.DELIM + "Items",                          // Bundle Set  <= CounterContext <= Bin
                             COUNT_LM                = BundlesDT.DELIM + "Count",                          // Bundle Set  <= CounterContext <= Bin
                             IP_LABEL_LM             = BundlesDT.DELIM + "IP Label"     + BundlesDT.DELIM; // IP Labeler

  /**
   * Date formats for various levels of precision
   */
  static SimpleDateFormat          hhmmss_sdf,
                                     mmss_sdf,
                                   hhmm_sdf,
                                dd_hhmm_sdf,
                          yyyymmdd_sdf;

  static {
             hhmmss_sdf = new SimpleDateFormat("HH:mm:ss");            hhmmss_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
               mmss_sdf = new SimpleDateFormat("mm:ss");                 mmss_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
             hhmm_sdf   = new SimpleDateFormat("HH:mm");               hhmm_sdf.  setTimeZone(TimeZone.getTimeZone("GMT"));
          dd_hhmm_sdf   = new SimpleDateFormat("dd HH:mm");         dd_hhmm_sdf.  setTimeZone(TimeZone.getTimeZone("GMT"));
    yyyymmdd_sdf        = new SimpleDateFormat("yyyy-MM-dd"); yyyymmdd_sdf.       setTimeZone(TimeZone.getTimeZone("GMT"));
  }

  /**
   * Sort by small multiple similarity.  Only applies to the shape of the small multiple.  This is very similar to the
   * smallMultiplesUMAPLayout() implementation... should be refactored if I could figure out how to make it more general.
   *
   *@param entities     entities to sort
   *@param bs           bundles for filter (can be null)
   *@param count_by     how to count
   *@param color_by     how to color // only used for the pie chart version
   *
   */
  private List<String> sortBySmallMultipleSimilarity(Collection<String> entities, 
                                                     Bundles            bs, 
                                                     String             count_by,
                                                     String             color_by,
                                                     EntitySize         entity_size) {

    // ALMOST EXACT COPY OF NEXT METHOD vvvvvvvvvv
    //
    // Deterime if this is going to work... map into the SmallMultiples feature vector enum...  copied from the SmallMuliples switch statement
    // .. commented out are ones that don't match / aren't implemented here
    SmallMultiples.FeatureVector feature_vector_setting = null;
    switch (entity_size) {
      case ByDayOfWeek:      feature_vector_setting = SmallMultiples.FeatureVector.ByDayOfWeek;          break;
      case ByDayOfWeekHour:  feature_vector_setting = SmallMultiples.FeatureVector.ByDayOfWeekHour;      break;
      // case BySecond:         feature_vector_setting = SmallMultiples.FeatureVector.BySecond;          break;
      // case ByMinute:         feature_vector_setting = SmallMultiples.FeatureVector.ByMinute;          break;
      // case ByMinuteSecond:   feature_vector_setting = SmallMultiples.FeatureVector.ByMinuteSecond;    break;
      case ByHour:           feature_vector_setting = SmallMultiples.FeatureVector.ByHour;               break;
      // case ByHourMinute:     feature_vector_setting = SmallMultiples.FeatureVector.ByHourMinute;      break;
      case ByMonth:          feature_vector_setting = SmallMultiples.FeatureVector.ByMonth;              break;
      // case ByYear:           feature_vector_setting = SmallMultiples.FeatureVector.ByYear;            break;
      // case ByYearMonth:      feature_vector_setting = SmallMultiples.FeatureVector.ByYearMonth;       break;
      // case ByYearMonthDay:   feature_vector_setting = SmallMultiples.FeatureVector.ByYearMonthDay;    break;

      // case Pie:              feature_vector_setting = SmallMultiples.FeatureVector.Pie;
      case PieSmall:
      case PieLarge:         feature_vector_setting = SmallMultiples.FeatureVector.Pie;                  break;

      // case Continuous:       feature_vector_setting = SmallMultiples.FeatureVector.Continuous;        break;
      case ContinuousSmall:
      case ContinuousLarge:  feature_vector_setting = SmallMultiples.FeatureVector.Continuous;           break;
    }
    if (feature_vector_setting == null) return sortByBundles(entities, bs); // If it's not a good setting, just sort by records

    // For what is visible, construct the underlying sets of the records ... and keep the lookup to the entity
    // ... note that this is a change from what was originally implemented which was pixel precise
    Set<Set<Bundle>>        sets        = new HashSet<Set<Bundle>>();
    Map<Set<Bundle>,String> set_lu      = new HashMap<Set<Bundle>,String>();

    Set<Bundle>             all_bs      = bs.bundleSet();

    Iterator<String> it_ent  = entities.iterator(); while (it_ent.hasNext()) {
      String entity = it_ent.next(); Set<Bundle> set = new HashSet<Bundle>();
      Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); if (all_bs.contains(bundle)) set.add(bundle);
      }
      if (set.size() > 0) { sets.add(set); set_lu.put(set, entity); }
    }
    //
    // TO ABOUT HERE ^^^^^^^^^^^^^^^^^^^

    // Assuming anything is left, run the hierarchical clustering algorithm
    if (sets.size() > 0) {
      List<Set<Bundle>> ordered_sets     = SmallMultiples.sortByHierarchicalClustering(bs, count_by, color_by, sets, feature_vector_setting);
      List<String>      ordered_entities = new ArrayList<String>();
      for (int i=0;i<ordered_sets.size();i++) ordered_entities.add(set_lu.get(ordered_sets.get(i)));
      return ordered_entities;
    } else return sortByBundles(entities, bs);
  }

  /**
   * Return the feature vector for the entity size small multiples setting.  If null, then
   * the entity size set was not supported.
   *
   *@return feature vector enum from the SmallMultiples class
   */
  private SmallMultiples.FeatureVector getFeatureVectorForSmallMultiples() {
    SmallMultiples.FeatureVector feature_vector_setting = null;

    // Deterime if this is going to work... map into the SmallMultiples feature vector enum...  copied from the SmallMuliples switch statement
    // .. commented out are ones that don't match / aren't implemented here
    switch (entitySize()) {
      case ByDayOfWeek:      feature_vector_setting = SmallMultiples.FeatureVector.ByDayOfWeek;         break;
      case ByDayOfWeekHour:  feature_vector_setting = SmallMultiples.FeatureVector.ByDayOfWeekHour;     break;
      // case BySecond:         feature_vector_setting = SmallMultiples.FeatureVector.BySecond;         break;
      // case ByMinute:         feature_vector_setting = SmallMultiples.FeatureVector.ByMinute;         break;
      // case ByMinuteSecond:   feature_vector_setting = SmallMultiples.FeatureVector.ByMinuteSecond;   break;
      case ByHour:           feature_vector_setting = SmallMultiples.FeatureVector.ByHour;              break;
      // case ByHourMinute:     feature_vector_setting = SmallMultiples.FeatureVector.ByHourMinute;     break;
      case ByMonth:          feature_vector_setting = SmallMultiples.FeatureVector.ByMonth;             break;
      // case ByYear:           feature_vector_setting = SmallMultiples.FeatureVector.ByYear;           break;
      // case ByYearMonth:      feature_vector_setting = SmallMultiples.FeatureVector.ByYearMonth;      break;
      // case ByYearMonthDay:   feature_vector_setting = SmallMultiples.FeatureVector.ByYearMonthDay;   break;

      // case Pie:              feature_vector_setting = SmallMultiples.FeatureVector.Pie;              break;
      case PieSmall:
      case PieLarge:         feature_vector_setting = SmallMultiples.FeatureVector.Pie;                 break;

      // case Continuous:       feature_vector_setting = SmallMultiples.FeatureVector.Continuous;       break;
      case ContinuousSmall:
      case ContinuousLarge:  feature_vector_setting = SmallMultiples.FeatureVector.Continuous;          break;

      case CountrySmall:
      case CountryLarge:     feature_vector_setting = SmallMultiples.FeatureVector.Country;             break;
    }
    return feature_vector_setting;
  }

  /**
   * Display the K-Means Small Multiples Dialog.
   */
  private void kMeansSmallMultiplesDialog() { 
    Container container = getParent(); while (container instanceof Frame == false) container = container.getParent();

    // Get the feature vector and make sure it's valid...
    SmallMultiples.FeatureVector feature_vector_setting = getFeatureVectorForSmallMultiples();
    if (feature_vector_setting == null) {
      JOptionPane.showMessageDialog((Frame) container, "Node Size \"" + entitySize() + "\" Unsupported", "Node Size Unsupported", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // Save the layout (in case of reset)
    saveState(SaveState.POSITIONS);

    // Display the dialog
    new KMeansSmallMultiplesDialog((Frame) container, feature_vector_setting); 
  }

  /**
   * K-Means Dialog for Small Multiples -- will enable to specify k and then preview the clustering results.
   */
  class KMeansSmallMultiplesDialog extends JDialog {
    // Feature vector to use
    SmallMultiples.FeatureVector feature_vector_setting;

    // K Slider // number of clusters
    JSlider    k_sl,        // K for K-Means Slider
               iters_sl,    // Iterations for K-Means Slider
               kth_nbor_sl, // Mutual Reachability Param for HDBSCAN
               min_sz_sl;   // Minimum Cluster Size Param for HDBSCAN
    JTextField k_tf, 
               iters_tf,
               kth_nbor_tf,
               min_sz_tf;

    // Layout options
    JRadioButton grid_rb, expanded_grid_rb, umap_rb;
    JSlider      umap_nbor_sl, umap_dist_sl;
    JTextField   umap_nbor_tf, umap_dist_tf;

    // Graph of k's to their sum squared
    KGraphComponent kgc;

    // Tabbed Pane for Clustering Algorithms
    JTabbedPane clustering_algo_tabs;

    // Constructor
    public KMeansSmallMultiplesDialog(Frame owner, SmallMultiples.FeatureVector feature_vector_setting) {
      super(owner, "K-Means Small Multiples", false);
      this.feature_vector_setting = feature_vector_setting;

      JButton bt;

      // Tabs (for K-Means and for HDBSCAN)
      clustering_algo_tabs = new JTabbedPane();

      // K-Means Section
      JPanel k_panel = new JPanel(new BorderLayout(5,5)); k_panel.setBorder(BorderFactory.createTitledBorder("K-Means Clustering"));
        JPanel k_panel_labels = new JPanel(new GridLayout(2,1));
        k_panel_labels.add(new JLabel("K"));
        k_panel_labels.add(new JLabel("Iters"));
        k_panel.add("West", k_panel_labels);

        JPanel k_panel_controls = new JPanel(new GridLayout(2,1));
        k_panel_controls.add(k_sl     = new JSlider(SwingConstants.HORIZONTAL, 2,   60, 10));
          k_sl.   addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { k_tf.setText("" + k_sl.getValue()); kgc.repaint(); } } );
        k_panel_controls.add(iters_sl = new JSlider(SwingConstants.HORIZONTAL, 40, 100, 40));
          iters_sl.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { iters_tf.setText("" + iters_sl.getValue()); } } );
        k_panel.add("Center", k_panel_controls);

        JPanel k_panel_info = new JPanel(new GridLayout(2,1));
        k_panel_info.add(k_tf     = new JTextField(4)); k_tf.    setEditable(false);
        k_panel_info.add(iters_tf = new JTextField(4)); iters_tf.setEditable(false);
        k_panel.add("East", k_panel_info);

      JPanel k_graph = new JPanel(new BorderLayout()); k_graph.setBorder(BorderFactory.createTitledBorder("WSS & Silhouette Graphs"));
        k_graph.add("Center", kgc = new KGraphComponent());
        k_graph.add("East",   bt = new JButton("Calc")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { kgc.calc(); } } );

      JPanel k_panel_border = new JPanel(new BorderLayout());
        k_panel_border.add("North",  k_panel);
        k_panel_border.add("Center", k_graph);
      
      clustering_algo_tabs.add("K-Means", k_panel_border);

      // HDBSCAN Section
      JPanel hdbscan_panel = new JPanel(new BorderLayout(5,5)); hdbscan_panel.setBorder(BorderFactory.createTitledBorder("HDBSCAN Clustering"));
        JPanel hdbscan_panel_labels = new JPanel(new GridLayout(2,1));
        hdbscan_panel_labels.add(new JLabel("Kth Nbor"));
        hdbscan_panel_labels.add(new JLabel("Min Size"));
        hdbscan_panel.add("West", hdbscan_panel_labels);

        JPanel hdbscan_panel_controls = new JPanel(new GridLayout(2,1));
        hdbscan_panel_controls.add(kth_nbor_sl = new JSlider(SwingConstants.HORIZONTAL, 2, 20, 5));
          kth_nbor_sl.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { kth_nbor_tf.setText("" + kth_nbor_sl.getValue()); } } );
        hdbscan_panel_controls.add(min_sz_sl   = new JSlider(SwingConstants.HORIZONTAL, 3, 50, 6));
          min_sz_sl.  addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { min_sz_tf.  setText("" + min_sz_sl.getValue()); } } );
        hdbscan_panel.add("Center", hdbscan_panel_controls);

        JPanel hdbscan_panel_info = new JPanel(new GridLayout(2,1));
        hdbscan_panel_info.add(kth_nbor_tf = new JTextField(4)); kth_nbor_tf.setEditable(false);
        hdbscan_panel_info.add(min_sz_tf   = new JTextField(4)); min_sz_tf.  setEditable(false);
        hdbscan_panel.add("East", hdbscan_panel_info);

      clustering_algo_tabs.add("HDBSCAN", hdbscan_panel);

      add("North", clustering_algo_tabs);

      // Layout options
      JPanel layout_panel = new JPanel(new BorderLayout()); layout_panel.setBorder(BorderFactory.createTitledBorder("Layout Options"));
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT)); ButtonGroup bg = new ButtonGroup();
          grid_rb          = new JRadioButton("Grid", true); bg.add(grid_rb);          panel.add(grid_rb);
          expanded_grid_rb = new JRadioButton("Grid Exp");   bg.add(expanded_grid_rb); panel.add(expanded_grid_rb);
          umap_rb          = new JRadioButton("UMAP");       bg.add(umap_rb);          panel.add(umap_rb);
        layout_panel.add("North", panel);

        panel = new JPanel(new BorderLayout());
          JPanel panel2 = new JPanel(new GridLayout(2,1));
            panel2.add(new JLabel("UMAP Nbors"));
            panel2.add(new JLabel("UMAP Dist"));
          panel.add("West", panel2);

          panel2 = new JPanel(new GridLayout(2,1));
            panel2.add(umap_nbor_sl = new JSlider(5, 50, 15)); // per https://github.com/tag-bio/umap-java
              umap_nbor_sl.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { umap_nbor_tf.setText("" + umap_nbor_sl.getValue()); } } );
            panel2.add(umap_dist_sl  = new JSlider(1,50,10));   // per https://github.com/tag-bio/umap-java ... divide it by 100.0 ... so 0.01 to 0.50
              umap_dist_sl.addChangeListener(new ChangeListener() { public void stateChanged(ChangeEvent ce) { umap_dist_tf.setText("" + (umap_dist_sl.getValue()/100.0)); } } );
          panel.add("Center", panel2);

          panel2 = new JPanel(new GridLayout(2,1));
            panel2.add(umap_nbor_tf = new JTextField(4)); umap_nbor_tf.setEditable(false);
            panel2.add(umap_dist_tf = new JTextField(4)); umap_dist_tf.setEditable(false);
          panel.add("East", panel2);

        layout_panel.add("Center", panel);
      k_panel_border.add("South", layout_panel);

      add("Center", layout_panel);

      // Buttons
      JPanel button_panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
      button_panel.add(bt = new JButton("Cluster"));      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clusterAndApply(); } } );
      button_panel.add(bt = new JButton("Undo & Close")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { restoreFromSaveState(); setVisible(false); dispose(); } } );
      button_panel.add(bt = new JButton("Close"));        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {                         setVisible(false); dispose(); } } );
      add("South", button_panel);

      pack(); setVisible(true);
    }

    /**
     * Displays a simple graph of the within-cluster sum of squares calculation for all possible k's...
     */
    class KGraphComponent extends JComponent {
      /**
       * Results of the WSS
       */
      Map<Integer,Double> k_to_sum_squared_map = new HashMap<Integer,Double>();

      /**
       *
       */
      Map<Integer,Double> k_to_silhouette_map = new HashMap<Integer,Double>();

      /**
       *
       */
      public KGraphComponent() { Dimension dim = new Dimension(384,160); setPreferredSize(dim); setMinimumSize(dim); }

      /**
       * Calculate the WSS for all k values within the slider bar.
       */
      public void calc() {
        Map<Integer,Double> copy_wss = new HashMap<Integer,Double>();
        Map<Integer,Double> copy_sil = new HashMap<Integer,Double>();

        // Approximate copy of the clusterAndApply() setup...
        Set<Set<Bundle>>        sets        = new HashSet<Set<Bundle>>();
        Bundles                 visible     = getRTParent().getVisibleBundles(); 
        Set<Bundle>             all_visible = visible.bundleSet();
        Iterator<String> it_ent  = entity_to_bundles.keySet().iterator(); while (it_ent.hasNext()) {
          String entity = it_ent.next(); Set<Bundle> set = new HashSet<Bundle>();
          Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next(); if (all_visible.contains(bundle)) set.add(bundle);
          }
          if (set.size() > 0) { sets.add(set); }
        }

        // Run through all the k's possible
        System.err.print("Calculating KMeans WSS: ");
        String country_field = null; if (feature_vector_setting == SmallMultiples.FeatureVector.Country) country_field = getCountryField();
        for (int k=k_sl.getMinimum();k<=k_sl.getMaximum();k++) {
          Set<Set<Set<Bundle>>> clusters = SmallMultiples.kMeans(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field,
                                                                 sets, feature_vector_setting, k, iters_sl.getValue());
          double sum_squared = SmallMultiples.kMeansWSS(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field,
                                                        sets, feature_vector_setting, clusters);
          copy_wss.put(k,sum_squared);

          double silhouette  = SmallMultiples.kMeansSilhouette(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field,
                                                               sets, feature_vector_setting, clusters);
          copy_sil.put(k,silhouette);
          if ((k%10) == 0) System.err.print("k="+k); else System.err.print(".");
        }
        System.err.println();

        k_to_sum_squared_map = copy_wss;
        k_to_silhouette_map  = copy_sil; 
        repaint();
      }

      /**
       * Insets in x and y
       */
      final int x_ins = 3, y_ins = 3;

      /**
       *
       */
      public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g; g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Copy for the results ... to prevent tears
        Map<Integer,Double> copy_wss = k_to_sum_squared_map;
        Map<Integer,Double> copy_sil = k_to_silhouette_map;

        // Clear the background & check for data
        g2d.setColor(Color.black); g2d.fillRect(0,0,getWidth(),getHeight());
        if (copy_wss == null || copy_wss.keySet().size() == 0) { g2d.setColor(Color.white); g2d.drawString("No Data -- Press Calc", 5, getHeight() - 5); return; }

        // Figure out the mins and maxes...
        int    k_min           = Integer.MAX_VALUE,        k_max           = Integer.MIN_VALUE;
        double sum_squared_min = Double.POSITIVE_INFINITY, sum_squared_max = Double.NEGATIVE_INFINITY;
        double sil_min         = Double.POSITIVE_INFINITY, sil_max         = Double.NEGATIVE_INFINITY;

        Iterator<Integer> it = copy_wss.keySet().iterator(); while (it.hasNext()) {
          int k = it.next(); double sum_squared = copy_wss.get(k);
          if (k           < k_min)           k_min           = k;           if (k           > k_max)           k_max           = k;
          if (sum_squared < sum_squared_min) sum_squared_min = sum_squared; if (sum_squared > sum_squared_max) sum_squared_max = sum_squared;
        }
        it = copy_sil.keySet().iterator(); while (it.hasNext()) {
          int k = it.next(); double sil = copy_sil.get(k);
          if (sil < sil_min) sil_min = sil; if (sil > sil_max) sil_max = sil;
        }

        // Geometry
        int graph_w = getWidth()  - 2*x_ins,
            graph_h = getHeight() - 2*y_ins;
        if (graph_w < 10) graph_w = 10;
        if (graph_h < 10) graph_h = 10;

        // Draw a line at every 10...
        for (int k=5;k<k_max;k+=5) {
          int x = (int) ((graph_w * (k - k_min))/(k_max - k_min) + x_ins);
          if ((k%5) == 0 && (k%10) != 0) g2d.setColor(Color.darkGray); else g2d.setColor(Color.lightGray);
          g2d.drawLine(x,0,x,getHeight());
        }

        // Draw it out
        it = copy_wss.keySet().iterator(); while (it.hasNext()) {
          //
          // WSS First
          //
          int k  = it.next();
          int x  = (int) ((graph_w * (k     - k_min))/(k_max - k_min) + x_ins);
          int x1 = (int) ((graph_w * ((k+1) - k_min))/(k_max - k_min) + x_ins);

          double sum_squared = copy_wss.get(k);
          int y = (int) (y_ins + graph_h - (graph_h * (sum_squared - sum_squared_min))/(sum_squared_max - sum_squared_min));

          if (copy_wss.containsKey(k+1)) {
            double sum_squared_1 = copy_wss.get(k+1);
            int y1 = (int) (y_ins + graph_h - (graph_h * (sum_squared_1 - sum_squared_min))/(sum_squared_max - sum_squared_min));
            g2d.setColor(Color.darkGray); g2d.drawLine(x,y,x1,y1);
            g2d.setColor(Color.lightGray); g2d.fillOval(x -1,y -1,2,2); if (k     == k_sl.getValue()) g2d.drawOval(x -4,y -4,8,8);
            g2d.setColor(Color.lightGray); g2d.fillOval(x1-1,y1-1,2,2); if ((k+1) == k_sl.getValue()) g2d.drawOval(x1-4,y1-4,8,8);
          } else { 
            g2d.setColor(Color.lightGray); g2d.fillOval(x-1,y-1,2,2); if (k == k_sl.getValue()) g2d.drawOval(x-4,y-4,8,8);
          }

          //
          // Silhouette next
          //
          if (copy_sil.containsKey(k)) {
            double sil = copy_sil.get(k);
            y = (int) (y_ins + graph_h - (graph_h * (sil - sil_min))/(sil_max - sil_min));
            if (copy_sil.containsKey(k+1)) {
              double sil_1 = copy_sil.get(k+1);
              int y1 = (int) (y_ins + graph_h - (graph_h * (sil_1 - sil_min))/(sil_max - sil_min));
              g2d.setColor(Color.darkGray); g2d.drawLine(x,y,x1,y1);
              g2d.setColor(Color.lightGray); g2d.fillOval(x -1,y -1,2,2); if (k     == k_sl.getValue()) g2d.drawOval(x -4,y -4,8,8);
              g2d.setColor(Color.lightGray); g2d.fillOval(x1-1,y1-1,2,2); if ((k+1) == k_sl.getValue()) g2d.drawOval(x1-4,y1-4,8,8);
            } else {
              g2d.setColor(Color.lightGray); g2d.fillOval(x-1,y-1,2,2); if (k == k_sl.getValue()) g2d.drawOval(x-4,y-4,8,8);
            }
          }
        }
      }
    }

    /**
     * Cluster the data and apply the layout.
     */
    private void clusterAndApply() {
      // Copy of the UMAP Layout code (below)
      // For what is visible, construct the underlying sets of the records ... and keep the lookup to the entity
      // ... note that this is a change from what was originally implemented which was pixel precise
      Set<Set<Bundle>>        sets        = new HashSet<Set<Bundle>>();
      Map<Set<Bundle>,String> set_lu      = new HashMap<Set<Bundle>,String>();

      Bundles                 visible     = getRTParent().getVisibleBundles(); 
      Set<Bundle>             all_visible = visible.bundleSet();

      Iterator<String> it_ent  = entity_to_bundles.keySet().iterator(); while (it_ent.hasNext()) {
        String entity = it_ent.next(); Set<Bundle> set = new HashSet<Bundle>();
        Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next(); if (all_visible.contains(bundle)) set.add(bundle);
        }
        if (set.size() > 0) { sets.add(set); set_lu.put(set, entity); }
      }

      // Run the clustering algorithm...
      Set<Set<Set<Bundle>>> clusters = null; 
      int k = 10; // k == the number of clusters ... setting for k-means... interpreted for hdbscan
      String country_field = null; if (feature_vector_setting == SmallMultiples.FeatureVector.Country) country_field = getCountryField();

      // - KMeans
      if (clustering_algo_tabs.getTitleAt(clustering_algo_tabs.getSelectedIndex()).equals("K-Means")) {
        k = k_sl.getValue();
        long t0 = System.currentTimeMillis();
        clusters = SmallMultiples.kMeans(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field,
                                         sets, feature_vector_setting, k, iters_sl.getValue());
        long t1 = System.currentTimeMillis();
        System.err.println("K-Means Cluster Time: " + (t1 - t0) + " ms | k = " + k + " | iters = " + iters_sl.getValue());

      // - HDBSCAN
      } else {
        long t0 = System.currentTimeMillis();
        Map<Set<Bundle>,Map<String,Double>> fvec_map = SmallMultiples.createFeatureVectors(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field, 
                                                                                           sets, feature_vector_setting);
        long t1 = System.currentTimeMillis();
        clusters = HDBSCAN.cluster(fvec_map, kth_nbor_sl.getValue(), min_sz_sl.getValue());
        long t2 = System.currentTimeMillis();
        System.err.println("HDBSCAN Cluster Time: " + (t2-t1) + " ms | FeatureVector Setup = " + (t1-t0) + " ms");
        k = clusters.size();
      }

      //
      // Put down as a grid
      //
      if (grid_rb.isSelected() || expanded_grid_rb.isSelected()) {
        int edge = (int) Math.sqrt(k); edge++; int x = 0, y = 0;
        Iterator<Set<Set<Bundle>>> it_cluster = clusters.iterator(); while (it_cluster.hasNext()) {
          Set<Set<Bundle>> cluster = it_cluster.next(); 
          double cur = 0.0, inc = 1.0 / (cluster.size() + 4); if (inc > 0.1) inc = 0.1;
          Iterator<Set<Bundle>> it_set = cluster.iterator(); while (it_set.hasNext()) {
            Set<Bundle> set = it_set.next(); String entity = set_lu.get(set);
            if (expanded_grid_rb.isSelected()) {
              entity_to_wxy.put(entity, new Point2D.Double(x + cur, y + cur));
              cur += inc;
            } else {
              entity_to_wxy.put(entity, new Point2D.Double(x, y));
            }
          }
          x++; if (x > edge) { x = 0; y++; }
        }

      //
      // UMAP the clusters
      //
      } else if (umap_rb.isSelected()) {
        // Need new feature vectors for the clusters & to keep track of the mappings
        // ... this will be a set that's found with entities here to the complet set of everything in a cluster
        Map<Set<Bundle>,Set<Bundle>> entity_set_to_cluster_set_map = new HashMap<Set<Bundle>,Set<Bundle>>(); 
        List<Set<Bundle>>            cluster_set_list              = new ArrayList<Set<Bundle>>();
        Set<Set<Bundle>>             cluster_sets                  = new HashSet<Set<Bundle>>();

        Iterator<Set<Set<Bundle>>> it_cluster = clusters.iterator(); while (it_cluster.hasNext()) {
          Set<Set<Bundle>> cluster = it_cluster.next(); Set<Bundle> all_within_cluster = new HashSet<Bundle>();
          Iterator<Set<Bundle>> it_set = cluster.iterator(); while (it_set.hasNext()) {
            Set<Bundle> set = it_set.next(); all_within_cluster.addAll(set); entity_set_to_cluster_set_map.put(set, all_within_cluster);
          }
          cluster_set_list.add(all_within_cluster);
          cluster_sets.add(all_within_cluster);
        }

        // Make feature vectors
        Map<Set<Bundle>,Map<String,Double>> cluster_set_to_feature_vector_map = 
          SmallMultiples.createFeatureVectors(visible, getRTParent().getCountBy(), getRTParent().getColorBy(), country_field,
                                              cluster_sets, feature_vector_setting);
       
        // Determine the distances between them...
        float dmat[][] = new float[cluster_set_list.size()][cluster_set_list.size()];
        for (int i=0;i<dmat.length;i++) { for (int j=0;j<dmat.length;j++) {
          dmat[i][j] = (float) SmallMultiples.distanceBetweenFeatureVectors(cluster_set_to_feature_vector_map.get(cluster_set_list.get(i)),
                                                                            cluster_set_to_feature_vector_map.get(cluster_set_list.get(j)));
        } }

        // Run umap
        Umap umap = new Umap();
        umap.setNumberComponents(2);
        umap.setNumberNearestNeighbours(umap_nbor_sl.getValue());
        umap.setMinDist((float) (umap_dist_sl.getValue()/100.0));
        umap.setThreads(8);
        float results[][] = umap.fitTransform(dmat);

        // Translate the results back into the placement of the cluster centers & underlying small multiple
        Map<Set<Bundle>,Point2D> cluster_set_to_point2d_map = new HashMap<Set<Bundle>,Point2D>();
        for (int i=0;i<cluster_set_list.size();i++) {
          Set<Bundle> all_within_cluster = cluster_set_list.get(i);
          cluster_set_to_point2d_map.put(all_within_cluster, new Point2D.Double(results[i][0], results[i][1]));
        }
        Iterator<Set<Bundle>> it_entity_set = entity_set_to_cluster_set_map.keySet().iterator(); while (it_entity_set.hasNext()) {
          Set<Bundle> entity_set         = it_entity_set.next();
          Set<Bundle> all_within_cluster = entity_set_to_cluster_set_map.get(entity_set);
          Point2D     point2d            = cluster_set_to_point2d_map.get(all_within_cluster);
          String      entity             = set_lu.get(entity_set);
          entity_to_wxy.put(entity,point2d);
        }
      } else throw new RuntimeException("Uknown K-Means Layout...");

      // Force a repaint
      zoomToFit(); 
      repaint();
    }
  }

  /**
   * Reimplementation of the UMAP Layout for small multiples by using the SmallMultiples class in the visualization package.
   */
  public void smallMultiplesUMAPLayout() {
    // Get the global settings...
    String      count_by    = getRTParent().getCountBy(),
                color_by    = getRTParent().getColorBy();

    // Get the feature vector and make sure it's valid...
    SmallMultiples.FeatureVector feature_vector_setting = getFeatureVectorForSmallMultiples();
    if (feature_vector_setting == null) {
      JOptionPane.showMessageDialog(this, "Node Size \"" + entitySize() + "\" Unsupported", "Node Size Unsupported", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // Copied to other places ... should be refactored...
    // For what is visible, construct the underlying sets of the records ... and keep the lookup to the entity
    // ... note that this is a change from what was originally implemented which was pixel precise
    Set<Set<Bundle>>        sets        = new HashSet<Set<Bundle>>();
    Map<Set<Bundle>,String> set_lu      = new HashMap<Set<Bundle>,String>();

    Bundles                 visible     = getRTParent().getVisibleBundles(); 
    Set<Bundle>             all_visible = visible.bundleSet();

    Iterator<String> it_ent  = entity_to_bundles.keySet().iterator(); while (it_ent.hasNext()) {
      String entity = it_ent.next(); Set<Bundle> set = new HashSet<Bundle>();
      Iterator<Bundle> it_bun = entity_to_bundles.get(entity).iterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); if (all_visible.contains(bundle)) set.add(bundle);
      }
      if (set.size() > 0) { sets.add(set); set_lu.put(set, entity); }
    }

    // Assuming anything is left, run the umap algorithm on the results
    if (sets.size() > 1) {
      // Save the current positions
      saveState(SaveState.POSITIONS);

      // Run the layout algorithm
      String country_field = null; if (feature_vector_setting == SmallMultiples.FeatureVector.Country) country_field = getCountryField();
      Map<Set<Bundle>,Point2D> results = SmallMultiples.umapSmallMultiplesLayout(visible, count_by, color_by, country_field, sets, feature_vector_setting);

      // Copy to the world map coordinates
      Iterator<Set<Bundle>> it_set = results.keySet().iterator(); while (it_set.hasNext()) {
        Set<Bundle> set    = it_set.next();
        String      entity = set_lu.get(set);
        Point2D     pt     = results.get(set);
        entity_to_wxy.put(entity, pt);
      }

      // Force a repaint
      zoomToFit(); 
      repaint();
    }
  }

  // BELOW ... mostly copy of the version in the rt graph panel -- modified for the exact variables

  /**
   * Based on the small multiple type, create feature vectors for each node (may be grouped together nodes).
   * Apply the UMAP embedding based on the distances between the feature vectors.  Probably isn't going to work
   * well on a large number of items.  Modifies the entity_to_wxy with the results.
   */
  public void smallMultiplesUMAPLayoutXXX() {
    // Only keep bundles that are visible
    Bundles    visible   = getRTParent().getVisibleBundles();
    EntitySize node_size = entitySize();

    // Only allow certain types of small multiples at first...
    if ((node_size != EntitySize.ByMonth)         && (node_size != EntitySize.ByDayOfWeek)       && 
        (node_size != EntitySize.ByHour)          && 
        (node_size != EntitySize.ContinuousSmall) && (node_size != EntitySize.ContinuousLarge)   &&
        (node_size != EntitySize.XYVisible)       && (node_size != EntitySize.XYVisibleEqual)    && 
        (node_size != EntitySize.XYLocal)         && (node_size != EntitySize.XYLocalEqual)) {
      JOptionPane.showMessageDialog(this, "Node Size \"" + node_size + "\" Unsupported", "Node Size Unsupported", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // First step is to collapse all of the entities into their common/overlapping coordinates
    // and then grab all of the application records (bundles) associated with that point.
    Map<Point2D,String>      pt_key_lu      = new HashMap<Point2D,String>();
    Map<String,Set<Bundle>>  pt_key_bundles = new HashMap<String,Set<Bundle>>();
    Map<String,Set<String>>  pt_key_to_ents = new HashMap<String,Set<String>>();

    Iterator<String> it_ent = entity_to_wxy.keySet().iterator(); while (it_ent.hasNext()) {
      String   entity   = it_ent.next(); 

      // Track the mapping from the Point2D to a String version
      Point2D  pt       = entity_to_wxy.get(entity); 
      String   pt_key   = pt.getX() + "," + pt.getY();
      pt_key_lu.put(pt, pt_key);
      if (pt_key_bundles.containsKey(pt_key) == false) pt_key_bundles.put(pt_key, new HashSet<Bundle>());
      if (pt_key_to_ents.containsKey(pt_key) == false) pt_key_to_ents.put(pt_key, new HashSet<String>());
      pt_key_to_ents.get(pt_key).add(entity);

      // First approximation is everything.. but should really be filtered by visible // abc
      pt_key_bundles.get(pt_key).addAll(entity_to_bundles.get(entity));
    }

    // Second step is to calculate the distances between the bundle set... this creates
    // an array of features.
    Map<String,float[]> feature_map = new HashMap<String,float[]>();
    Iterator<String> it = pt_key_bundles.keySet().iterator(); while (it.hasNext()) {
      String pt_key = it.next(); Set<Bundle> bundle_set = pt_key_bundles.get(pt_key); if (bundle_set.size() == 0) continue;
      feature_map.put(pt_key, bundleSetFeatureVector(bundle_set, node_size));
    }
    List<String> sorted = new ArrayList<String>(); sorted.addAll(feature_map.keySet()); Collections.sort(sorted);
    float d[][] = new float[sorted.size()][sorted.size()]; float max_rms = 0.0f;
    for (int i=0;i<sorted.size();i++) for (int j=0;j<=i;j++) {
      d[i][j] = vectorDistance(feature_map.get(sorted.get(i)), feature_map.get(sorted.get(j)));
      d[j][i] = d[i][j]; // mirror it... make the matrix symmetric
      if (max_rms < d[j][i]) max_rms = d[j][i];
    }
    if (max_rms == 0.0) max_rms = 1.0f;
    for (int i=0;i<d.length;i++) for (int j=0;j<d[i].length;j++) d[i][j] = d[i][j] / max_rms; // Normalize to 1.0

    // Third step is to apply the UMAP transformation
    // - Derived from code at https://github.com/tag-bio/umap-java
    Umap umap = new Umap();
    umap.setNumberComponents(2);          // Number of dimensions in result
    umap.setNumberNearestNeighbours(15);  // Maybe this should be 15 or the max number of things? (says 10 to 15 is sensible at url)
    umap.setThreads(8);                   // Parallelism
    float results[][] = umap.fitTransform(d);

    // Fourth step is to copy the locations back into the world xy coordinates and then force a transform... and re-render
    // System.err.println("Results = " + results.length + " x " + results[0].length + " ... sorted.size() = " + sorted.size());
    for (int i=0;i<results.length;i++) { 
      String  pt_key = sorted.get(i);
      Point2D pt     = new Point2D.Double(results[i][0], results[i][1]);
      Iterator<String> it_ents = pt_key_to_ents.get(pt_key).iterator(); while (it_ents.hasNext()) {
        String entity = it_ents.next();
        entity_to_wxy.put(entity, pt);
      }
    }

    zoomToFit(); 
    repaint();
  }

  /**
   * Calculate the distance between two vectors ... rms error...  probably
   * should be something else...
   *
   *@param v0 first vector
   *@param v1 second vector
   *
   *@return root mean square error between the two vectors
   */
  protected float vectorDistance(float v0[], float v1[]) {
    double sum = 0.0; for (int i=0;i<v0.length;i++) sum += (v0[i] - v1[i])*(v0[i] - v1[i]);
    return (float) Math.sqrt(sum/v0.length);
  }

  //
  // Ordering... for vector creation and small multiples rendering
  //
  String sm_months[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" },
         sm_dows[]   = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
         sm_hours[]  = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                         "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };

  /**
   * Create a feature vector for a set of bundles.  The type of feature vector depends
   * on the small multiples node size setting ... but should be the same size for the 
   * same type of node_size.
   *
   *@param set        set of records
   *@param node_size  the type of small multiples
   *
   *@return feature vector of the records
   */
  protected float[] bundleSetFeatureVector(Set<Bundle> set, EntitySize node_size) {
    float vec[] = null; 
    //
    // Discrete Options ----------------------------------------------------------------------------------
    //
    if (node_size == EntitySize.ByMonth || node_size == EntitySize.ByDayOfWeek || node_size == EntitySize.ByHour) {
      BundlesCounterContext bcc_lite = null; String order[] = null;
      switch (node_size) {
        case ByMonth:           order = sm_months;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_MONTH_STR);
                                break;

        case ByDayOfWeek:       order = sm_dows;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_DAYOFWEEK_STR);
                                break;

        case ByHour:            order = sm_hours;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_HOUR_STR);
                                break;

        case Large:
        case Small:
        case Vary:
        case ContinuousSmall:
        case ContinuousLarge:
        case PieSmall:
        case PieLarge: 
        case XYVisible:
        case XYVisibleEqual:
        case XYLocal:
        case XYLocalEqual: 
        case LinkNode:
        case TimelineBlocks:
        case TimelineLarge:
        case TimelineSmall:
        case TimelineSmallVary:
        case TimelineSmallOverlaps:
        default:                System.err.println("Unreachable Case Statements in bundleSetFeatureVector()");
      }

      vec = new float[order.length];
      Map<String,Integer> to_i = new HashMap<String,Integer>(); 
      for (int i=0;i<order.length;i++) to_i.put(order[i], i);
      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) { 
        String bin = it_bin.next(); vec[to_i.get(bin)] = (float) bcc_lite.totalNormalized(bin); 
      }

    //
    // Continuous Options ----------------------------------------------------------------------------------
    //
    } else if (node_size == EntitySize.ContinuousLarge || node_size == EntitySize.ContinuousSmall) {
      // Use the same values as the rendering for the vector size
      if (node_size == EntitySize.ContinuousLarge) vec = new float[64]; else vec = new float[32];

      // Keep track of strings to integers...
      Map<String,Integer> x_map = new HashMap<String,Integer>();

      Bundles visible = getRTParent().getVisibleBundles();

      // Determine which tablets can count
      Set<String> tablet_counts = new HashSet<String>();
      Iterator<Tablet> it_tab = visible.tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next();
        if (KeyMaker.tabletCompletesBlank(tablet, getRTParent().getControlPanel().getCountBy())) tablet_counts.add(tablet.fileHeader());
      }

      // Run through the records and place them on the x axis
      BundlesCounterContext bcc_lite = new BundlesCounterContext(visible, getRTParent().getControlPanel().getCountBy(), null);
      Iterator<Bundle> it = set.iterator(); while (it.hasNext()) {
        Bundle bundle = it.next(); if (bundle.hasTime() && tablet_counts.contains(bundle.getTablet().fileHeader())) {
          int    x     = (int) (((vec.length-1) * (bundle.ts0() - visible.ts0())) / (visible.ts1() - visible.ts0()));
          String x_str = "" + x; x_map.put(x_str, x);
          bcc_lite.count(bundle, x_str);
        }
      }

      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); vec[x_map.get(bin)] = (float) bcc_lite.totalNormalized(bin);
      }

    //
    // XY Options ----------------------------------------------------------------------------------
    //
    } else if (node_size == EntitySize.XYVisible || node_size == EntitySize.XYVisibleEqual ||
               node_size == EntitySize.XYLocal   || node_size == EntitySize.XYLocalEqual) {

      Bundles visible = getRTParent().getVisibleBundles();
      int     total_w = 64,
              total_h = 32;

      boolean global_time;
      if (node_size == EntitySize.XYVisible || 
          node_size == EntitySize.XYVisibleEqual) global_time = true; 
      else                                             global_time = false;

      boolean y_equal_spacing;
      if (node_size == EntitySize.XYVisibleEqual ||
          node_size == EntitySize.XYLocalEqual)   y_equal_spacing = true;
      else                                             y_equal_spacing = false;

      vec = XYRenderer.vectorTimeInX(set, total_w, total_h, visible, global_time, noCountByBundles(getRTParent().getControlPanel().getCountBy()), y_equal_spacing);
    }

    return vec;
  }

  /**
   * Really stupid... there's no easy way to fix how the framework processes the "count by bundles" option
   * as a field... the framework would take more work to figure it out and may make other parts of the application
   * unstable.  This method makes sure the that the "count by buns" can't be passed as a y-axis option for the
   * XY scatterplot small multiples.
   */
  private String noCountByBundles(String str) {
    if (str.equals(BundlesDT.COUNT_BY_BUNS)) return KeyMaker.TABLET_SEP_STR;
    else                                     return str;
  }

  // ABOVE ... mostly copy of the version in the rt graph panel -- modified for the exact variables

}

