/* 

Copyright 2018 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import racetrack.framework.Bundles;
import racetrack.framework.BundlesG;
import racetrack.framework.BundlesDT;
import racetrack.framework.RFC4180Importer;
import racetrack.util.CSVTokenConsumer;
import racetrack.util.RFC4180CSVReader;
import racetrack.util.Utils;

/**
 * Provide a dialog for the user to enumerate timestamps and scalar fields.
 */
public class RFC4180ImportDialog extends JDialog implements CSVTokenConsumer {
  private static final long serialVersionUID = -3990124544318124608L;

  /**
   * File to load
   */
  protected File file; 

  /**
   * Reference to the parent of this dialog (needed for constructor)
   */
  protected RTControlFrame parent;

  /**
   * The first so many lines of the file.
   */
  List<String[]> lines = new ArrayList<String[]>();

  /**
   * Key for RTStore procedure
   */
  String storage_key = null;

  /**
   * Constructor
   *
   *@param parent  parent frame
   *@param file    file to read
   */
  public RFC4180ImportDialog(RTControlFrame parent, File file) {
    super(parent, "Import Dialog...", true); this.parent = parent; this.file = file;
    // Get some of the file for examinination
    try {
      /* RFC4180CSVReader reader = */ new RFC4180CSVReader(file,this);
      if (lines.size() <= 2) {
        JOptionPane.showMessageDialog(this, "Not Enough Lines In The File For Parsing", "File Error", JOptionPane.ERROR_MESSAGE);
      } else {
        // Figure out which lines had a consistent number of tokens
        Map<Integer,Integer> token_histo = new HashMap<Integer,Integer>();
        for (int i=0;i<lines.size();i++) {
          if (token_histo.containsKey(lines.get(i).length) == false) token_histo.put(lines.get(i).length, 0);
          token_histo.put(lines.get(i).length, 1 + token_histo.get(lines.get(i).length));
        }
        // Find the maximum
        int max = -1, tokens = -1;
        Iterator<Integer> it = token_histo.keySet().iterator();
        while (it.hasNext()) {
          int num_of_tokens = it.next();
          if (token_histo.get(num_of_tokens) > max) { max = token_histo.get(num_of_tokens); tokens = num_of_tokens; }
        }
        // Check to see if the tokens are consistent with at least half of the lines
        if (max < lines.size()/2) {
          JOptionPane.showMessageDialog(this, "No Definitive Token Counts In File", "File Error", JOptionPane.ERROR_MESSAGE);
        } else {
          // Find the first line, assume it's the header
          first_line = -1; for (int i=0;i<lines.size();i++) if ((lines.get(i)).length == tokens && first_line == -1) first_line = i;
          hdr = lines.get(first_line);
          // Check for a stored configuration of the header
          String copy[] = new String[hdr.length]; for (int i=0;i<copy.length;i++) copy[i] = hdr[i];
          Arrays.sort(copy); StringBuffer sb = new StringBuffer(); sb.append(copy[0].toLowerCase());
          for (int i=1;i<copy.length;i++) sb.append("_" + Utils.encToURL(copy[i].toLowerCase()));
          storage_key = "IMPORT_" + sb.toString();
          String prefs = RTPrefs.retrieveString(storage_key);
          // For the remaining lines, figure out some data types
          Map<String,Set<BundlesDT.DT>> datatypes = new HashMap<String,Set<BundlesDT.DT>>();
          for (int i=0;i<hdr.length;i++) datatypes.put(hdr[i], new HashSet<BundlesDT.DT>());
          for (int i=first_line+1;i<lines.size();i++) {
            if (lines.get(i).length == tokens) {
              for (int j=0;j<tokens;j++)
                datatypes.get(hdr[j]).add(BundlesDT.getEntityDataType((lines.get(i))[j]));;
            }
          }

          // Create the gui so that the user can configure the data
          timestamps   = new JComboBox[tokens];
          scalars      = new JCheckBox[tokens];
          labels       = new JTextField[tokens];
          to_lowercase = new JCheckBox[tokens];
          ignore       = new JCheckBox[tokens];

          // Use a cascading borderlayout to get strips that are variable sized
          JPanel hdr_panel = new JPanel(new BorderLayout(5,0)); JPanel panel = hdr_panel, tmp_panel; JPanel strip = new JPanel(new GridLayout(hdr.length+1,1));
          panel.add("West", strip); strip.add(new JLabel("Orig"));       for (int i=0;i<hdr.length;i++) { strip.add(new JLabel(hdr[i])); }

          panel.add("East", tmp_panel=new JPanel(new BorderLayout(5,0))); strip     = new JPanel(new GridLayout(hdr.length+1,1));  tmp_panel.add("West",strip); panel = tmp_panel;
          strip.add(new JLabel("Field Name")); for (int i=0;i<hdr.length;i++) { strip.add(labels[i] = new JFocusTextField(restorePrefLabel(hdr[i], prefs, datatypes))); }

          panel.add("East", tmp_panel=new JPanel(new BorderLayout(5,0))); strip=new JPanel(new GridLayout(hdr.length+1,1)); tmp_panel.add("West",strip); panel = tmp_panel;
          strip.add(new JLabel("Scalar")); for (int i=0;i<hdr.length;i++) {
            if (datatypes.get(hdr[i]).size() == 1 && datatypes.get(hdr[i]).iterator().next() == BundlesDT.DT.INTEGER) {
              strip.add(scalars[i] = new JCheckBox("",restorePrefScalar(hdr[i], prefs))); 
              scalars[i].addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent ce) {
                  for (int i=0;i<scalars.length;i++) {
                    if (scalars[i] == ce.getSource()) {
                      if (scalars[i].isSelected()) labels[i].setText(labels[i].getText().toUpperCase());
                      else                         labels[i].setText(labels[i].getText().toLowerCase());
                    }
                  }
                }
              } );
            } else strip.add(new JLabel(""));
          }

          panel.add("East", tmp_panel=new JPanel(new BorderLayout(5,0))); strip=new JPanel(new GridLayout(hdr.length+1,1)); tmp_panel.add("West",strip); panel = tmp_panel;
          strip.add(new JLabel("Lower")); for (int i=0;i<hdr.length;i++) { strip.add(to_lowercase[i] = new JCheckBox("",restorePrefLowerCase(hdr[i],prefs))); }

          panel.add("East", tmp_panel=new JPanel(new BorderLayout(5,0))); strip=new JPanel(new GridLayout(hdr.length+1,1)); tmp_panel.add("West",strip); panel = tmp_panel;
          strip.add(new JLabel("Ignore")); for (int i=0;i<hdr.length;i++) { 
            strip.add(ignore[i] = new JCheckBox("",restorePrefIgnore(hdr[i],prefs))); 
            ignore[i].addChangeListener(new ChangeListener() {
              public void stateChanged(ChangeEvent e) {
                for (int i=0;i<ignore.length;i++) {
                  if (e.getSource() == ignore[i]) {
                    boolean state;
                    if (ignore[i].isSelected()) { state = false; } else { state = true; }
                    labels[i].setEditable(state);
                    to_lowercase[i].setEnabled(state);
                    if (scalars[i]    != null) scalars[i].setEnabled(state);
                    if (timestamps[i] != null) timestamps[i].setEnabled(state);
                    return;
                  } } } } ); }

          panel.add("East", tmp_panel=new JPanel(new BorderLayout(5,0))); strip=new JPanel(new GridLayout(hdr.length+1,1)); tmp_panel.add("West",strip); panel = tmp_panel;
          strip.add(new JLabel("Time")); for (int i=0;i<hdr.length;i++) {
            if (datatypes.get(hdr[i]).size() == 1 && datatypes.get(hdr[i]).iterator().next() == BundlesDT.DT.TIMESTAMP) {
              strip.add(timestamps[i] = new JComboBox<String>(time_options));
              timestamps[i].setSelectedIndex(restorePrefTime(hdr[i],prefs));
            } else strip.add(new JLabel(""));
          }

          // Add a checkbox to indicate that the data has a header
          panel = new JPanel(new GridLayout(3,1));
          panel.add(data_has_header_cb = new JCheckBox("First Data Row Is A Header",    true));
          panel.add(strip_spaces_cb    = new JCheckBox("Strip Before and After Spaces", restorePrefSpaces(prefs)));
          panel.add(url_decode_cb      = new JCheckBox("URL Decode If Possible",        restorePrefURLDecode(prefs)));
            JPanel encode_panel = new JPanel(new BorderLayout()); encode_panel.add("Center", new JLabel("Encoding"));
                                                                  encode_panel.add("East",   encoding_cb = new JComboBox<String>(encodings));
          panel.add(encode_panel);
          getContentPane().add("North", panel);

          getContentPane().add("Center", new JScrollPane(hdr_panel));

          JPanel but_panel = new JPanel(new FlowLayout()); JButton bt;
          but_panel.add(bt = new JButton("Import File")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { importFile(); } } );
          but_panel.add(bt = new JButton("Cancel"));      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setVisible(false); } } );

          getContentPane().add("South", but_panel);

          pack(); setVisible(true);
        }
      }
    } catch (IOException ioe) {
      JOptionPane.showMessageDialog(this, "Error Retrieving Header Information", "File Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Simple class to select all the text when the component gains focus.  Simulates the
   * select all feature as a user tabs through the components.
   */
  class JFocusTextField extends JTextField implements FocusListener {
    private static final long serialVersionUID = -4010129342318124608L;

    public JFocusTextField(String str) { super(str); addFocusListener(this); }
    public void focusGained(FocusEvent fe) { selectAll(); }
    public void focusLost(FocusEvent fe) { }
  }

  /**
   * Return the preferred label based on the original header string.  If the prefs variable contains the preference,
   * restore that value.  Otherwise look to see if the datatypes are equivalent to integers.
   *
   *@param orig_hdr  original header from the file
   *@param prefs     preference settings for this header
   *@param datatypes parsed datatypes for the headers in the file
   *
   *@return preferred header
   */
  protected String restorePrefLabel(String orig_hdr, String prefs, Map<String,Set<BundlesDT.DT>> datatypes) {
    if (prefs != null) {
      String val = getPrefVal(prefs, Utils.encToURL(orig_hdr) + ".label");
      if (val != null) return val;
    }
    if (restorePrefScalar(orig_hdr, prefs)) {
      if (datatypes.get(orig_hdr).size() == 1 && datatypes.get(orig_hdr).iterator().next() == BundlesDT.DT.INTEGER) return orig_hdr.toUpperCase();
      else return orig_hdr.toLowerCase();
    } else return orig_hdr.toLowerCase();
  }

  /**
   * Generic method to get the specific key from the preference string.
   *
   *@param prefs preference string
   *@param key   setting to return
   *
   *@return value for the key/value pair if it exists, null otherwise
   */
  protected String getPrefVal(String prefs, String key) {
    key = "|" + key + "=";
    int    i   = prefs.indexOf(key);
    if (i >= 0) {
      String val = prefs.substring(i + key.length(), prefs.indexOf("|",i+1));
      return Utils.decFmURL(val);
    } else return null;
  }

  /**
   * Restore the preferred scalar setting for the original header string.
   *
   *@param orig_header header from the file
   *@param prefs       preference string
   *
   *@return preferred value setting, true otherwise
   */
  protected boolean restorePrefScalar(String orig_hdr, String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, Utils.encToURL(orig_hdr) + ".scalar");
      if (val != null) return val.equals("" + true);
    }
    return true;
  }

  /**
   * Restore the preferred lowercase setting for the original header string.
   *
   *@param orig_header header from the file
   *@param prefs       preference string
   *
   *@return preferred value setting, true otherwise
   */
  protected boolean restorePrefLowerCase(String orig_hdr, String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, Utils.encToURL(orig_hdr) + ".lower");
      if (val != null) return val.equals("" + true);
    } 
    return true;
  }

  /**
   * Restore the preferred ignore setting for the original header string.
   *
   *@param orig_header header from the file
   *@param prefs       preference string
   *
   *@return preferred value setting, false otherwise
   */
  protected boolean restorePrefIgnore(String orig_hdr, String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, Utils.encToURL(orig_hdr) + ".ignore");
      if (val != null) return val.equals("" + true);
    } 
    return false;
  }

  /**
   * Restore the preferred time setting for the original header string.
   *
   *@param orig_header header from the file
   *@param prefs       preference string
   *
   *@return preferred value setting
   */
  protected int restorePrefTime(String orig_hdr, String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, Utils.encToURL(orig_hdr) + ".time");
      if (val != null) return Integer.parseInt(val);
    } 
    return 0;
  }

  /**
   * Return the preference for the strip spaces setting.
   *
   *@param prefs       preference string
   *
   *@return the preference for the strip spaces setting
   */
  protected boolean restorePrefSpaces(String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, "-spaces-");
      if (val != null) return val.equals("" + true);
    } 
    return true;
  }

  /**
   * Return the preference for the URL-decode setting.
   *
   *@param prefs       preference string
   *
   *@return the preference for the URL-decode setting
   */
  protected boolean restorePrefURLDecode(String prefs) {
    if (prefs != null) {
      String val = getPrefVal(prefs, "-urlencode-");
      if (val != null) return val.equals("" + true);
    } 
    return false;
  }

  /**
   * Dropdown options for the timestamp setting combo box.
   */
  private String time_options[] = { "None", "Timestamp", "Timestamp End" }; 

  /**
   * The header from the filename
   */
  protected String     hdr[];

  /**
   * Number of the first line to begin parsing
   */
  protected int        first_line;

  /**
   * Comboboxes for the timestamps
   */
  protected JComboBox<String>  timestamps[];

  /**
   * Checkbox for the scalar setting
   */
  protected JCheckBox  scalars[];

  /**
   * Checkbox for the lowercase setting
   */
  protected JCheckBox  to_lowercase[];

  /**
   * Checkbox for the ignore setting
   */
  protected JCheckBox  ignore[];

  /**
   * Checkbox for the strip spaces
   */
  protected JCheckBox  strip_spaces_cb;

  /**
   * Checkbox for the data header option
   */
  protected JCheckBox  data_has_header_cb;

  /**
   * Checkbox for the URL decode option
   */
  protected JCheckBox  url_decode_cb;

  /**
   * Combobox for the encoding options
   */
  protected JComboBox<String>  encoding_cb;

  /**
   * List of supported encodings (probably a better way to get this -- i pulled this from the java docs)
   */
  protected String     encodings[] = { "None",
                                       "US-ASCII",
                                       "ISO-8859-1",
                                       "UTF-8",
                                       "UTF-16BE",
                                       "UTF-16LE",
                                       "UTF-16" };

  /**
   * Textfield for the user to modify the label
   */
  protected JTextField labels[];

  /**
   * Check the user settings (providing feedback as necessary) and then parse the file based
   * on the setting if everything is okay.
   */
  protected void importFile() {
    // Check requirements
    // - Are all fields ignored?
    boolean all_ignored = true;
    for (int i=0;i<ignore.length;i++) {
      if (ignore[i].isSelected() == false) all_ignored = false;
    }
    if (all_ignored) {
      JOptionPane.showMessageDialog(this, "All Fields Ignored, Try 'Cancel' Instead", "Labeling Error", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // - Are any fields duplicated?
    Set<String> dupe_test = new HashSet<String>();
    for (int i=0;i<labels.length;i++) {
      if (ignore[i].isSelected()) continue;
      if (dupe_test.contains(labels[i].getText())) {
        JOptionPane.showMessageDialog(this, "Label " + i + " Is A Duplicate (\"" + labels[i].getText() + "\")", "Labeling Error", JOptionPane.ERROR_MESSAGE);
        return;
      } else dupe_test.add(labels[i].getText());
    }

    // - Are any fields blank?
    for (int i=0;i<labels.length;i++) {
      if (ignore[i].isSelected()) continue;
      if (Utils.stripSpaces(labels[i].getText()).equals("")) {
        JOptionPane.showMessageDialog(this, "Label " + i + " Is Blank", "Labeling Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    for (int i=0;i<labels.length;i++) {
      if (ignore[i].isSelected()) continue;
      if (scalars[i] != null && scalars[i].isSelected() && Utils.isAllUpper(labels[i].getText()) == false) {
        JOptionPane.showMessageDialog(this, "Change Label \"" + labels[i].getText() + "\" To All Uppercase (Scalar)", "Labeling Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
      if ((scalars[i] == null || scalars[i].isSelected() == false) && Utils.isAllUpper(labels[i].getText()) == true)  {
        JOptionPane.showMessageDialog(this, "Change Label \"" + labels[i].getText() + "\" To At Least One Lowercase (Non-Scalar)", "Labeling Error", JOptionPane.ERROR_MESSAGE);
        return;
      }
    }
    int timestamp = 0, timestamp_end = 0; int timestamp_i = -1, timestamp_end_i = -1;
    for (int i=0;i<timestamps.length;i++) {
      if (ignore[i].isSelected()) continue;
      if (timestamps[i] != null) {
        if (timestamps[i].getSelectedIndex() == 1) { timestamp_i     = i; timestamp++;     }
        if (timestamps[i].getSelectedIndex() == 2) { timestamp_end_i = i; timestamp_end++; }
      }
    }
    if (timestamp == 0 && timestamp_end == 0) { } else if (timestamp == 1 && timestamp_end == 0) { } else if (timestamp == 1 && timestamp_end == 1) { } else {
        JOptionPane.showMessageDialog(this, "Timestamp Settings Incorrect - Must Be Unique", "Settings Error", JOptionPane.ERROR_MESSAGE);
        return;
    }
    
    // Create the prefs string
    StringBuffer prefs = new StringBuffer();
    for (int i=0;i<hdr.length;i++) {
      prefs.append("|" + Utils.encToURL(hdr[i]) + ".label=" + Utils.encToURL(labels[i].getText()) + "|");
      prefs.append("|" + Utils.encToURL(hdr[i]) + ".lower=" + to_lowercase[i].isSelected()        + "|");
      if (scalars[i]    != null) prefs.append("|" + Utils.encToURL(hdr[i]) + ".scalar=" + scalars[i].isSelected() + "|");
      prefs.append("|" + Utils.encToURL(hdr[i]) + ".ignore=" + ignore[i].isSelected() + "|");
      if (timestamps[i] != null) prefs.append("|" + Utils.encToURL(hdr[i]) + ".time=" + timestamps[i].getSelectedIndex() + "|");
    }
    prefs.append("|-urlencode-=" + url_decode_cb.isSelected()   +"|");
    prefs.append("|-spaces-="    + strip_spaces_cb.isSelected() +"|");

    RTPrefs.store(storage_key, prefs.toString());

    // Otherwise, we can parse the file
    try {
      // Copy the settings into arrays
      String  label_strs[]   = new String[labels.length];        for (int i=0;i<label_strs.length;i++)   label_strs[i]   = labels[i].getText();
      boolean scalar_flags[] = new boolean[scalars.length];      for (int i=0;i<scalar_flags.length;i++) if (scalars[i] == null) scalar_flags[i] = false; else scalar_flags[i] = scalars[i].isSelected();
      boolean ignore_flags[] = new boolean[ignore.length];       for (int i=0;i<ignore_flags.length;i++) ignore_flags[i] = ignore[i].isSelected();
      boolean lower_flags[]  = new boolean[to_lowercase.length]; for (int i=0;i<lower_flags.length;i++)  lower_flags[i]  = to_lowercase[i].isSelected();

      String  encoding = (String) encoding_cb.getSelectedItem(); if (encoding.equals("None")) encoding = null;

      // Call the parser
      Bundles bundles = parent.getRTParent().getRootBundles(); BundlesG globals = bundles.getGlobals();

      RFC4180Importer importer = new RFC4180Importer(file, 
                                                     data_has_header_cb.isSelected(), 
                                                     bundles, timestamp_i, timestamp_end_i, 
                                                     label_strs, scalar_flags, ignore_flags, lower_flags,
                                                     url_decode_cb.isSelected(),
                                                     strip_spaces_cb.isSelected(),
                                                     encoding);

      // Reset the transforms to force the lookups to be created
      System.err.println("**\n** Probably Need To Include Cached Bundles...\n**");
      imported_bundles_set.add(bundles);
      globals.cleanse(imported_bundles_set);

      // Update the GUI with new field information
      parent.getRTParent().updatePanelsForNewBundles(importer.getBundlesAdded());
    } catch (IOException ioe) {
      JOptionPane.showMessageDialog(this, "Error Parsing File: " + ioe, "File Error", JOptionPane.ERROR_MESSAGE);
      System.err.println("IOException: " + ioe);
      ioe.printStackTrace(System.err);
    }
    setVisible(false);
  }

  /**
   * Set of the imported bundles
   */
  protected Set<Bundles> imported_bundles_set = new HashSet<Bundles>();

  /**
   * Return the set of imported bundles.
   *
   *@return imported bundles
   */
  public Set<Bundles> getImportedBundles() { return imported_bundles_set; }

  /**
   * Consume a line of CSV input.  This provides the dialog with information about the headers
   * and fields.
   *
   *@param tokens     tokens from the line
   *@param line       complete line from the file
   *@param line_no    line number
   *
   *@return true to keep parsing
   */
  public boolean consume(String tokens[], String line, int line_no) {
    lines.add(tokens); 
    return lines.size() < 10;
  }

  /**
   * Handle (or rather don't handle) comment lines.
   *
   *@param line
   */
  public void    commentLine(String line) { }
}

