/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.Tablet;

import racetrack.kb.KBProgressListener;
import racetrack.kb.KBSource;
import racetrack.kb.KBSources;

import racetrack.util.Utils;

/**
 * Class that implements a dynamically update reports views.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class KBQueryDialog extends JDialog implements Runnable {
  private static final long serialVersionUID = -117124942718121108L;

  /**
   * Instances of sources available
   */
  KBSource sources[];

  /**
   * All parameters that the sources require
   */
  Set<String> all_params = new HashSet<String>();

  /**
   * Column specific parameters
   */
  List<String> column_params = new ArrayList<String>();

  /**
   * Grid Layout Indentations
   */
  public static final int GLI = 4;

  /**
   * Determines if the source is enabled
   */
  JToggleButton source_enabled[];

  /**
   * Progress bar for each source
   */
  JProgressBar progress_bars[];

  /**
   * Cancel buttons per source
   */
  JButton cancel_bts[];

  /**
   * Parent of this dialog
   */
  RTControlFrame rt_control_frame;

  /**
   * Strings to query
   */
  Set<String> query_strs;

  /**
   * Keep track of which options are enabled per source
   */
  Map<String,Map<String,JCheckBox>> param_enabled = new HashMap<String,Map<String,JCheckBox>>();

  /**
   * Options for the selectables
   */
  JButton selectables_bts[];

  /**
   * Construct the dialog based on the number of sources available.
   *
   *@param rt_control_frame rt control frame
   *@param query_strs       strings to query
   */
  public KBQueryDialog(RTControlFrame rt_control_frame0, Set<String> query_strs0) {
    super(rt_control_frame0, "KB Source Query", true);

    this.rt_control_frame = rt_control_frame0;
    this.query_strs       = query_strs0;
    
    // Option to the related sources needing them (for a tooltip)
    Map<String,Set<String>> option_to_sources = new HashMap<String,Set<String>>();

    // Get the sources and determine the possible query params
    sources = KBSources.getSources(); 
    for (int i=0;i<sources.length;i++) {
      // Overall set of all of the options needed
      all_params.addAll(sources[i].queryParams());

      // Keep a map of the options to their sources (for a tooltip)
      Iterator<String> it = sources[i].queryParams().iterator(); while (it.hasNext()) {
        String option = it.next();
        if (option_to_sources.containsKey(option) == false) option_to_sources.put(option, new HashSet<String>());
        option_to_sources.get(option).add(sources[i].kbSourceName());
      }
    }

    // Determine which ones are in columns ... only checkable items
    if (all_params.contains(KBSource.REMOVE_NONPUBLIC_IPS_STR)) column_params.add(KBSource.REMOVE_NONPUBLIC_IPS_STR);
    if (all_params.contains(KBSource.PRIVATE_IPS_ONLY_STR))     column_params.add(KBSource.PRIVATE_IPS_ONLY_STR);

    // Allocate the panels (toggle button ... selectables ... var params ... types progress cancel)
    JPanel panels[] = new JPanel[2 + column_params.size() + 1 + 1 + 1]; JButton bt;

      // First level panel is toggle button per source
      panels[0] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));
      panels[0].add(new JLabel("KB Source Name"));
      source_enabled = new JToggleButton[sources.length];
      for (int i=0;i<sources.length;i++) { 
        String src_name = sources[i].kbSourceName();
        panels[0].add(source_enabled[i] = new JToggleButton(src_name)); 
        source_enabled[i].setHorizontalAlignment(SwingConstants.LEFT);
        recallSetting(src_name, "enabled", source_enabled[i]);
        source_enabled[i].addItemListener(new PersistOptionsItemListener(src_name, "enabled"));
      }

      // Second level is selectable options
      selectables_bts = new JButton[sources.length];
      panels[1] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));
      panels[1].add(new JLabel());
      for (int i=0;i<sources.length;i++) {
        List<String> selectables = sources[i].selectableOptions();
        if (selectables != null) {
         panels[1].add(selectables_bts[i] = new JButton(selectablesButtonText(i)));
         selectables_bts[i].setToolTipText(makeToolTipText(selectablesSet(i)));
         selectables_bts[i].addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { selectablesDialog(ae); } } );
        } else panels[1].add(new JLabel(""));
      }

      // Next Levels are configurable...
      // Create configurable panels for the columns
      int panel_i = 2;
      for (int i=0;i<column_params.size();i++) {
        panels[panel_i] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));

        // - Make the name shorter... but descriptive
        String abbr_name = column_params.get(panel_i-2);
        if (abbr_name.equals(KBSource.REMOVE_NONPUBLIC_IPS_STR)) abbr_name = "|= !NonPubs =|";
        if (abbr_name.equals(KBSource.PRIVATE_IPS_ONLY_STR))     abbr_name = "|= PrvtIPs =|";

        panels[panel_i].add(new JLabel(abbr_name));
        for (int j=0;j<sources.length;j++) {
          if (sources[j].queryParams().contains(column_params.get(i))) {
            JCheckBox cb; String src_name = sources[j].kbSourceName();
            panels[panel_i].add(cb = new JCheckBox(""));
            recallSetting(src_name, column_params.get(i), cb);
            cb.addItemListener(new PersistOptionsItemListener(src_name, column_params.get(i)));

            // Track the source and option
            if (param_enabled.containsKey(sources[j].kbSourceName()) == false) param_enabled.put(sources[j].kbSourceName(), new HashMap<String,JCheckBox>());
            param_enabled.get(sources[j].kbSourceName()).put(column_params.get(i), cb);
          } else { panels[panel_i].add(new JLabel("")); } // Doesn't apply to this source
        }

        // Increment for the next panel
        panel_i++;
      }

      // Types panel
      panels[panel_i] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));
      panels[panel_i].add(new JLabel("|===== Types =====|"));
      for (int i=0;i<sources.length;i++) panels[panel_i].add(new JLabel("" + sources[i].handlesDataTypes()));
      panel_i++;

      // Progress panel
      progress_bars = new JProgressBar[sources.length];
      panels[panel_i] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));
      panels[panel_i].add(new JLabel("|= Progress..."));
      for (int i=0;i<sources.length;i++) panels[panel_i].add(progress_bars[i] = new JProgressBar());
      panel_i++;

      // Cancel panel
      cancel_bts = new JButton[sources.length];
      panels[panel_i] = new JPanel(new GridLayout(sources.length+1,1,GLI,GLI));
      panels[panel_i].add(new JLabel(""));
      for (int i=0;i<sources.length;i++) panels[panel_i].add(cancel_bts[i] = new JButton("Cancel"));
      panel_i++;

    // Create the layers of the tables
    JPanel center = new JPanel(new BorderLayout());
    center.add("Center", panels[0]);  JPanel next = center;

    for (int i=1;i<panels.length;i++) {
      JPanel panel = new JPanel(new BorderLayout());
      panel.add("Center", panels[i]);
      next.add("East", panel);
      next = panel;
    }

    getContentPane().add("Center", center);

    // Make the additional options panel (if the options are set)
    if (all_params.contains(KBSource.MAX_RECS_STR) ||
        all_params.contains(KBSource.TIMEFRAME_STR)) {
      JPanel add_opts = new JPanel(new BorderLayout(10,10));
      add_opts.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Global Options"));

      JPanel labels = new JPanel(new GridLayout(0,1));
      JPanel combos = new JPanel(new GridLayout(0,1));
      JLabel jlabel;

      if (all_params.contains(KBSource.MAX_RECS_STR)) {
        labels.add(jlabel = new JLabel("Max Records"));  jlabel.setToolTipText(makeToolTipText(option_to_sources.get(KBSource.MAX_RECS_STR)));
        combos.add(max_recs_cb = new JComboBox<String>(max_recs_strs));
        recallSetting("global", KBSource.MAX_RECS_STR, max_recs_cb);
        max_recs_cb.addItemListener(new PersistOptionsItemListener("global", KBSource.MAX_RECS_STR));
      }

      if (all_params.contains(KBSource.TIMEFRAME_STR)) {
        labels.add(jlabel = new JLabel("Timeframe"));   jlabel.setToolTipText(makeToolTipText(option_to_sources.get(KBSource.TIMEFRAME_STR)));
        combos.add(timeframe_cb = new JComboBox<String>(timeframe_strs));
        recallSetting("global", KBSource.TIMEFRAME_STR, timeframe_cb);
        timeframe_cb.addItemListener(new PersistOptionsItemListener("global", KBSource.TIMEFRAME_STR));
      }

      add_opts.add("West",   labels);
      add_opts.add("Center", combos);

      getContentPane().add("North", add_opts);
    }

    // Make the southern panel
    JPanel south = new JPanel(new FlowLayout(FlowLayout.CENTER));
    south.add(query_bt = new JButton("Query"));  query_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      query_bt.setEnabled(false); query(); // Only let it get pressed once...
    } } );
    south.add(bt       = new JButton("Cancel")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      cancelAllSourceQueries(); setVisible(false); dispose();
    } } );
    getContentPane().add("South", south);

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent we) {
        cancelAllSourceQueries(); setVisible(false); dispose();
      }
    } );

    // For disabled sources, disable the options on that line (the toggle button enable/disable isn't all that obvious :(
    for (int i=0;i<sources.length;i++) {
      if (source_enabled[i].isSelected() == false) {
        String src_name = sources[i].kbSourceName();
        progress_bars[i].setEnabled(false);
        cancel_bts[i].   setEnabled(false);
        if (selectables_bts[i] != null) selectables_bts[i].setEnabled(false);
        if (param_enabled != null && param_enabled.containsKey(src_name)) {
          Iterator<String> it = param_enabled.get(src_name).keySet().iterator();
          while (it.hasNext()) { String opt = it.next(); param_enabled.get(src_name).get(opt).setEnabled(false); }
        }
      }
    }

    pack();
    setLocationRelativeTo(null);
    setVisible(true);
  }

  /**
   * Make a tooltip out of a set of strings.  Used to create tool tips for the global
   * options labels.
   */
  private String makeToolTipText(Set<String> set) {
    List<String> as_list = new ArrayList<String>();
    as_list.addAll(set);
    Collections.sort(as_list);

    StringBuffer sb = new StringBuffer(); 

    sb.append("<html>");
    for (int i=0;i<as_list.size();i++) { sb.append(as_list.get(i) + "<br>"); }
    sb.append("</html>");

    return sb.toString();
  }

  /**
   * Simple dialog with simple selectable options that persist.
   */
  protected class SelectablesDialog extends JDialog {
    private static final long serialVersionUID = -120174911312224600L;

    /**
     * Create and display the selectables dialog.
     */
    public SelectablesDialog(JDialog parent, String source_name, List<String> sels) {
      super(parent, "Selectables - " + source_name, true);

      // Checkbox Panel
      cbs = new JCheckBox[sels.size()];
      JPanel panel = new JPanel(new GridLayout(sels.size(),1));
      for (int i=0;i<sels.size();i++) {
        panel.add(cbs[i] = new JCheckBox(sels.get(i)));
        recallSetting(source_name, sels.get(i), cbs[i]);
        cbs[i].addItemListener(new PersistOptionsItemListener(source_name, sels.get(i)));
      }
      getContentPane().add("Center", panel);

      // Helper buttons for all/none
      panel = new JPanel(new FlowLayout()); JButton bt;

      // -- All button
      panel.add(bt = new JButton("All"));  
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        for (int i=0;i<cbs.length;i++) cbs[i].setSelected(true);
      } } );
      
      // -- None button
      panel.add(bt = new JButton("None"));
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        for (int i=0;i<cbs.length;i++) cbs[i].setSelected(false);
      } } );
      getContentPane().add("South", panel);

      pack(); 
      setLocationRelativeTo(null);
      setVisible(true);
    }

    /**
     * Checkbox items
     */
    JCheckBox cbs[];
  }

  /**
   * Return the button text for the option buttons -- label with
   * an indication of how many are set.
   */
  private String selectablesButtonText(int src_i) {
    List<String> selectables = sources[src_i].selectableOptions();
    int options_true = 0;
    for (int i=0;i<selectables.size();i++) {
      String str = RTPrefs.retrieveString("kbq." + sources[src_i].kbSourceName() + "." + selectables.get(i));
      if (str == null || str.toLowerCase().equals("true")) options_true++;
    }

    return "" + options_true + " / " + selectables.size();
  }

  /**
   * Return just the selectables that are set for a source... Used to make a button tooltip...
   */
  private Set<String> selectablesSet(int src_i) {
    List<String> selectables = sources[src_i].selectableOptions();

    Set<String> set = new HashSet<String>();

    for (int i=0;i<selectables.size();i++) {
      String str = RTPrefs.retrieveString("kbq." + sources[src_i].kbSourceName() + "." + selectables.get(i));
      if (str == null || str.toLowerCase().equals("true")) set.add(selectables.get(i));
    }

    return set;
  }

  /**
   * Display a dialog for configuring the selectables.
   *
   *@param event source event for the selectables
   */
  private void selectablesDialog(ActionEvent event) {
    for (int i=0;i<selectables_bts.length;i++) {
      if (event.getSource() == selectables_bts[i]) {
        List<String> selectables = sources[i].selectableOptions();
        new SelectablesDialog(this, sources[i].kbSourceName(), selectables);
        selectables_bts[i].setText(selectablesButtonText(i));
        selectables_bts[i].setToolTipText(makeToolTipText(selectablesSet(i)));
        break;
      }
    }
  }

  /**
   * Combobox for the max records constraints
   */
  JComboBox<String> max_recs_cb;

  /**
   * Max Record Strings
   */
  static final String MAX_100_RECS    = "100 Records",
                      MAX_1K_RECS     = "1K Records",
                      MAX_10K_RECS    = "10K Records",
                      MAX_100K_RECS   = "100K Records",
                      MAX_1M_RECS     = "1M Records";
  static final String max_recs_strs[] = { MAX_100_RECS, MAX_1K_RECS, MAX_10K_RECS, MAX_100K_RECS, MAX_1M_RECS };

  public String maxRecords() {
    if      (max_recs_cb                   == null)          return "100";
    else if (max_recs_cb.getSelectedItem() == MAX_100_RECS)  return "100";
    else if (max_recs_cb.getSelectedItem() == MAX_1K_RECS)   return "1000";
    else if (max_recs_cb.getSelectedItem() == MAX_10K_RECS)  return "10000";
    else if (max_recs_cb.getSelectedItem() == MAX_100K_RECS) return "100000";
    else if (max_recs_cb.getSelectedItem() == MAX_1M_RECS)   return "1000000";
    else                                                     return "100";
  }

  /**
   * Combobox for the timeframe constraints
   */
  JComboBox<String> timeframe_cb;

  /**
   * Timeframe Strings
   */
  static final String TIMEFRAME_LAST_30_DAYS = "Last 30 Days",
                      TIMEFRAME_LAST_90_DAYS = "Last 90 Days",
                      TIMEFRAME_LAST_YEAR    = "Last Year",
                      TIMEFRAME_DATA         = "Loaded Data",
                      TIMEFRAME_VISIBLE_DATA = "Visible Data";
  static final String timeframe_strs[] = { TIMEFRAME_LAST_30_DAYS, TIMEFRAME_LAST_90_DAYS, TIMEFRAME_LAST_YEAR, TIMEFRAME_DATA, TIMEFRAME_VISIBLE_DATA };

  /**
   * Return the start timeframe for the query
   */
  public String timeframeStart() {
    if        (timeframe_cb == null)                                     { return Utils.exactDate(System.currentTimeMillis() - 30L  * 24L * 60L * 60L * 1000L);
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_30_DAYS) { return Utils.exactDate(System.currentTimeMillis() - 30L  * 24L * 60L * 60L * 1000L);
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_90_DAYS) { return Utils.exactDate(System.currentTimeMillis() - 90L  * 24L * 60L * 60L * 1000L);
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_YEAR)    { return Utils.exactDate(System.currentTimeMillis() - 365L * 24L * 60L * 60L * 1000L);
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_DATA)         { return Utils.exactDate(rt_control_frame.getRTParent().getRootBundles().ts0());
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_VISIBLE_DATA) { return Utils.exactDate(rt_control_frame.getRTParent().getVisibleBundles().ts1());
    }
    return Utils.exactDate(System.currentTimeMillis() - 30L * 24L * 60L * 60L * 1000L);
  }

  /**
   * Return the ending timeframe for the query
   */
  public String timeframeEnd() {
    if        (timeframe_cb == null)                                       { return Utils.exactDate(System.currentTimeMillis());
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_30_DAYS ||
               timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_90_DAYS ||
               timeframe_cb.getSelectedItem() == TIMEFRAME_LAST_YEAR)     { return Utils.exactDate(System.currentTimeMillis()); 
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_DATA)          { return Utils.exactDate(rt_control_frame.getRTParent().getRootBundles().ts1());
    } else if (timeframe_cb.getSelectedItem() == TIMEFRAME_VISIBLE_DATA)  { return Utils.exactDate(rt_control_frame.getRTParent().getVisibleBundles().ts1());
    }
    return Utils.exactDate(System.currentTimeMillis());
  }

  /**
   * Store settings that user chooses so that they will be persistent.
   */
  class PersistOptionsItemListener implements ItemListener {
    String src, opt;
    public PersistOptionsItemListener(String src0, String opt0) { this.src = src0; this.opt = opt0; }
    public void itemStateChanged(ItemEvent ie) {
      // First part is to disable / enable the entire row for just the enabled events
      if (opt.equals("enabled")) {
        boolean enabled = ((JToggleButton) ie.getSource()).isSelected();

        // Find the source name and source index
        String src_name = src; int src_i = -1; for (int i=0;i<sources.length;i++) if (sources[i].kbSourceName().equals(src_name)) src_i = i;
        if (src_i >= 0) {
          progress_bars[src_i].setEnabled(enabled);
          cancel_bts[src_i].   setEnabled(enabled);
          if (selectables_bts[src_i] != null) selectables_bts[src_i].setEnabled(enabled);
          if (param_enabled != null && param_enabled.containsKey(src_name)) {
            Iterator<String> it = param_enabled.get(src_name).keySet().iterator();
            while (it.hasNext()) { String opt = it.next(); param_enabled.get(src_name).get(opt).setEnabled(enabled); }
          }
        }
      }

      // Second part is to store for persistence
      if        (ie.getSource() instanceof JToggleButton)     { 
        RTPrefs.store("kbq." + src + "." + opt, "" + ((JToggleButton)     ie.getSource()).isSelected());
      } else if (ie.getSource() instanceof JCheckBox)         { 
        RTPrefs.store("kbq." + src + "." + opt, "" + ((JCheckBox)         ie.getSource()).isSelected());
      } else if (ie.getSource() instanceof JComboBox) {
        JComboBox<String> jcb_str = (JComboBox<String>) ie.getSource();
        RTPrefs.store("kbq." + src + "." + opt, "" + jcb_str.getSelectedItem());
      } else { throw new RuntimeException("Unknown Source \"" + ie.getSource() + "\" for PersistOptions [1]"); }
    }
  }

  /**
   * Recall a setting based on the source name and the ui option.
   */
  private void recallSetting(String src, String opt, Object object) {
    if        (object instanceof JToggleButton) {  String str = RTPrefs.retrieveString("kbq." + src + "." + opt); if (str == null) str = "true";
                                                   ((JToggleButton) object).setSelected(str.toLowerCase().equals("true"));
    } else if (object instanceof JCheckBox)     {  String str = RTPrefs.retrieveString("kbq." + src + "." + opt); if (str == null) str = "true";
                                                   ((JCheckBox)     object).setSelected(str.toLowerCase().equals("true"));
    } else if (object instanceof JComboBox)     {  String str = RTPrefs.retrieveString("kbq." + src + "." + opt); if (str == null) str = "";
                                                   JComboBox<String> jcb_str = (JComboBox<String>) object;
                                                   jcb_str.setSelectedItem(str);
    } else { throw new RuntimeException("Uknown object \"" + object + "\" for PersistOptions [2]"); }
  }

  /**
   * Recall a specific boolean setting 
   */
  private boolean recallBooleanSetting(String src, String opt) {
    String setting = RTPrefs.retrieveString("kbq." + src + "." + opt);
    if (setting == null) setting = "true";
    return setting.toLowerCase().equals("true");
  }

  /**
   * Query button.. so it can be disabled after press...
   */
  JButton query_bt;

  /**
   * Threads to perform the query
   */
  WorkerThread threads[] = null;

  /**
   * Execute the query based on the dialog settings.  Make it a thread so that the dialog doesn't freeze -- 
   * i.e., cancel buttons still work...
   */
  protected void query() { Thread thread = new Thread(this); thread.start(); }

  /**
   * Make it a thread... so that the dialog doesn't block
   */
  public void run() {
    threads = new WorkerThread[sources.length];
  
    // Create the threads
    for (int i=0;i<sources.length;i++) {
      if (source_enabled[i].isSelected()) {
        threads[i] = new WorkerThread(i, this, progress_bars[i], cancel_bts[i]);
        threads[i].start();
      }
    }

    // Wait for the threads to finish
    for (int i=0;i<threads.length;i++) {
      if (threads[i] != null) { // for disabled sources, the thread will be null...
        try { threads[i].join(); } catch (InterruptedException ie) { }
      }
    }

    // Close dialog...dispose of it...
    setVisible(false); dispose();
  }

  /**
   * Cancel all source queries.
   */
  protected void cancelAllSourceQueries() {
  }

  /**
   * Thread for each query source
   */
  class WorkerThread extends Thread implements KBProgressListener {
    // Minimal information for the thread since the parent class should be accessible
    int source_i; Object mutex; JProgressBar progress_bar; JButton cancel_bt;

    /**
     * Controls lower level threads -- used with the cancel button
     */
    boolean keep_going = true;

    /**
     * Constructor
     */
    public WorkerThread(int source_i0, Object mutex0, JProgressBar progress_bar0, JButton cancel_bt0) {
      this.source_i = source_i0; this.mutex = mutex0; this.progress_bar = progress_bar0; this.cancel_bt = cancel_bt0;
      cancel_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { keep_going = false; } } );
    }

    /**
     * Where the work gets done
     */
    public void run() {
      // First filter is to check datatypes
      Set<String> filtered = new HashSet<String>(); filtered.addAll(query_strs);
      Iterator<String> it = filtered.iterator(); while (it.hasNext()) {
        String str = it.next();  
        if (sources[source_i].handlesDataTypes().contains(BundlesDT.getEntityDataType(str)) == false) it.remove();
      }

      // Now check the other types of restrictions
      // - Non Public IP Filter
      if (sources[source_i].queryParams().contains(KBSource.REMOVE_NONPUBLIC_IPS_STR) &&
          param_enabled.get(sources[source_i].kbSourceName()).get(KBSource.REMOVE_NONPUBLIC_IPS_STR).isSelected()) {
        it = filtered.iterator(); while (it.hasNext()) {
          String str = it.next();
          if (Utils.isIPv4(str)) {
            String label = Utils.ipSpaceLabel(str);
            if (label.startsWith("loopback") || label.startsWith("private") || label.indexOf("broadcast") >= 0) it.remove();
          }
        }
      }

      if (sources[source_i].queryParams().contains(KBSource.PRIVATE_IPS_ONLY_STR) &&
          param_enabled.get(sources[source_i].kbSourceName()).get(KBSource.PRIVATE_IPS_ONLY_STR).isSelected()) {
        it = filtered.iterator(); while (it.hasNext()) {
          String str = it.next();
          if (Utils.isIPv4(str)) {
            String label = Utils.ipSpaceLabel(str);
            if (label.startsWith("private")) { } else { it.remove(); }
          }
        }
      }

      //
      // Add More Here As Necessary....
      //

      // Construct the query params
      Map<String,String> params = new HashMap<String,String>();

      if (sources[source_i].queryParams().contains(KBSource.MAX_RECS_STR))  { params.put(KBSource.MAX_RECS_STR, maxRecords());      }
      if (sources[source_i].queryParams().contains(KBSource.TIMEFRAME_STR)) { params.put("timestamp",           timeframeStart());
                                                                              params.put("timestamp_end",       timeframeEnd());    }   

      // Determine if there are selectable options
      List<String> selectables = sources[source_i].selectableOptions();
      if (selectables != null) {
        for (int i=0;i<selectables.size();i++) {
          boolean b = recallBooleanSetting(sources[source_i].kbSourceName(), selectables.get(i));
          params.put(selectables.get(i), "" + b);
        }
      }

      // Construct succeeded and failed sets
      Set<String> succeeded = new HashSet<String>(),
                  failed    = new HashSet<String>();

      // Execute the query
      List<Map<String,String>> results = sources[source_i].query(filtered, params, succeeded, failed, this);

      // Update the tablets
      synchronized (mutex) {
        Set<String> found         = new HashSet<String>(); // Keep track of what was found
        Set<Bundle> bundles_added = new HashSet<Bundle>(); // Bundles added

        // Make new records for each result
        for (int i=0;i<results.size();i++) {
         try {
          Map<String,String> attr_map = results.get(i);

          // Keep track of which ones were found
          if (attr_map.containsKey("query")) found.add(attr_map.get("query"));

          // Find the tablet
          String hdrs[] = new String[attr_map.keySet().size()]; it = attr_map.keySet().iterator();
          for (int j=0;j<hdrs.length;j++) hdrs[j] = it.next();
          Tablet tablet = rt_control_frame.getRTParent().getRootBundles().findOrCreateTablet(hdrs);

          // Separate out timestamps
          String ts0_str = null, ts1_str = null;
          if        (attr_map.containsKey("timestamp") && attr_map.containsKey("timestamp_end")) {
            ts0_str = attr_map.get("timestamp"); ts1_str = attr_map.get("timestamp_end");
            attr_map.remove("timestamp"); attr_map.remove("timestamp_end");
          } else if (attr_map.containsKey("timestamp")) {
            ts0_str = attr_map.get("timestamp");
            attr_map.remove("timestamp");
          } 

          // Create the bundle
          Bundle bundle = null;
          if      (ts1_str != null) bundle = tablet.addBundle(attr_map, ts0_str, ts1_str);
          else if (ts0_str != null) bundle = tablet.addBundle(attr_map, ts0_str);
          else                      bundle = tablet.addBundle(attr_map);

          bundles_added.add(bundle);
         } catch (Throwable throwable) {
          System.err.println("KBQueryDialog: addBundle() Failed: " + throwable);
          throwable.printStackTrace(System.err);
         }
        }

        // Force the state change to propagate
        Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(rt_control_frame.getRTParent().getRootBundles());
        rt_control_frame.getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
        rt_control_frame.getRTParent().updatePanelsForNewBundles(bundles_added);
      }
    }

    /**
     * Notification on the percentage complete to the user interface.  Includes the current entity as well as a message.
     *
     *@param percent_complete percentage complete
     *@param entity           current query entity
     *@param message          additional information on the status of the service
     *
     *@return true to indicate that the service should continue API calls
     */
    public boolean percentComplete(double percent_complete, String entity, String message) {
      updateProgressBar(percent_complete);
      return keep_going;
    }

    /**
     * Notification on the percentage complete to the user interface.
     *
     *@param percent_complete
     *
     *@return true  to indicate that the service should continue API calls
     */
    public boolean percentComplete(double percent_complete) {
      updateProgressBar(percent_complete);
      return keep_going;
    }

    /**
     * Update the progress bar
     */
    private void updateProgressBar(double perc) {
      if (progress_bar != null) {
        progress_bar.setMaximum(1000);
        progress_bar.setValue((int) (1000 * perc));
        progress_bar.setIndeterminate(false);
      }
    }

    /**
     * Exception thrown by the API...
     */
    public void exceptionThrown(Throwable throwable, String query) { }
  }
}

