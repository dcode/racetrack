/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Area;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tagbio.umap.Umap;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesG;
import racetrack.framework.BundlesUtils;
import racetrack.framework.KeyMaker;
import racetrack.framework.PostProc;
import racetrack.framework.Tablet;

import racetrack.graph.BiConnectedComponents;
import racetrack.graph.Conductance;
import racetrack.graph.DijkstraSingleSourceShortestPath;
import racetrack.graph.GraphTransform;
import racetrack.graph.EdgeCrossingsPlus;
import racetrack.graph.GraphFactory;
import racetrack.graph.GraphLayouts;
import racetrack.graph.GraphLayoutProgress;
import racetrack.graph.GraphUtils;
import racetrack.graph.LandmarkSelection;
import racetrack.graph.MyGraph;
import racetrack.graph.SimpleMyGraph;
import racetrack.graph.UniGraph;
import racetrack.graph.UniTwoPlusDegreeGraph;
import racetrack.graph.WorldToScreenTransform;

import racetrack.kb.EntityTag;

import racetrack.transform.CCShapeRec;
import racetrack.transform.GeoData;

import racetrack.util.CaseInsensitiveComparator;
import racetrack.util.Entity;
import racetrack.util.EntityExtractor;
import racetrack.util.JTextFieldHistory;
import racetrack.util.LineCountSorter;
import racetrack.util.LineIntersection;
import racetrack.util.Selection;
import racetrack.util.StrCountSorter;
import racetrack.util.SubText;
import racetrack.util.Utils;

import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.ColorScale;
import racetrack.visualization.RTColorManager;
import racetrack.visualization.ShapeFile;
import racetrack.visualization.TreeMap;
import racetrack.visualization.XYRenderer;


/**
 * Visualization component for handling link-node graphs.  By far the most
 * complex visualization component due to the unique characteristics of
 * link-node analysis -- predominantly related to laying out nodes and
 * manipulating their locations.
 *
 * Version 2.0:  Added new interactive dialog for defining edges and symbols in a graph
 *
 *@author  D. Trimm
 *@version 2.0
 */
public class RTGraphPanel extends RTPanel implements WorldToScreenTransform {
  /**
   *
   */
  private static final long serialVersionUID = -6648601887937566057L;

  /**
   * Enumeration of mouse events
   */
  enum ME_ENUM        { PRESSED, RELEASED, CLICKED, MOVED, DRAGGED, WHEEL };

  /**
   * Enumeration of different UI interaction events
   */
  enum UI_INTERACTION { NONE, PANNING, SELECTING, MOVING, GRID_LAYOUT, LINE_LAYOUT, CIRCLE_LAYOUT };

  /**
   * Enumeration of the modes for the UI
   */
  enum UI_MODE        { EDIT, FILTER, EDGELENS, TIMELINE };

  /**
   *  Color parts approach
   */
  enum ColorPartsApproach { THIRDS, THIRDS2, THIRDS3 };

  /**
   * Radio choices for the node size
   */
  JRadioButtonMenuItem         node_sizes[], 
  /**
   * Radio choices for the node color
   */
                               node_colors[], 
  /**
   * Radio choices for the link size
   */
                               link_sizes[], 
  /**
   * Radio choices for the link color
   */
                               link_colors[], 
  /**
   * Radio choice for the edit graph mode
   */
                               edit_rbmi, 
  /**
   * Radio choice for the filter graph mode - this mode is most similar to all of the other
   * visualization components
   */
                               filter_rbmi, 
  /**
   * Radio choice for the edgelens mode which provides a means to understand congested areas
   */
                               edgelens_rbmi,
  /**
   * Radio choice for the timeline mode which provides a way to combine spatial with temporal
   */
                               timeline_rbmi,
  /**
   * Radio choice for no background
   */
                               nobg_rbmi, 
  /**
   * Radio choice for geo outline background
   */
                               geo_out_rbmi, 
  /**
   * Radio choice for geo filled backgroun
   */
                               geo_fill_rbmi, 
  /**
   * Radio choice for geo filled on only those countries containing a node
   */
                               geo_touch_rbmi,
  /**
   * Radio choice for kcore background
   */
                               kcores_rbmi;
  /**
   * Enable link transparency
   */
  JCheckBoxMenuItem            link_trans_cbmi, 
  /**
   * Enable link curves (for directed graph analysis)
   */
                               link_curves_cbmi,
  /**
   * Enable link color parts (for directed graph analysis)
   */
                               link_color_parts_cbmi,
  /**
   * Enable link arrows (for directed graph analysis)
   */
                               arrows_cbmi, 
  /**
   * Enable link timing marks (to enable time analysis across edges)
   */
                               timing_cbmi, 
  /**
   * Recalculate the boundaries upon redraw
   */
                               recalc_bounds_cbmi,
  /**
   * Enable dynamic lables that occur under the mouse and with the nearest neighbors
   */
                               dyn_label_cbmi,
  /**
   * Vertex placement heatmap option (experimental)
   */
                               vertex_placement_heatmap_cbmi,
  /**
   * Calculate crossings (experimental)
   */
                               calculate_crossings_cbmi,
  /**
   * If the node diagram is about report titles, enabling will allow selection of the contained reports as report titles are selected
   */
                               report_select_entities_cbmi;

  /**
   * Enable node labels
   */
  JCheckBox                    node_labels_cb,
  /**
   *
   */
                               link_labels_cb;
  /**
   * List for selecting node labels
   */
  JList<String>                entity_label_list, 
  /**
   * List for selecting node color when based on a label
   */
                               entity_color_list,
  /**
   * List for selecting edge labels
   */
                               bundle_label_list;
  /**
   * Textfield for selecting entities (nodes)
   */
  JTextField                   select_tf, 
  /**
   * Textfield for tagging selected entities (nodes)
   */
                               tag_tf;
  /**
   * Menu item to add a relationship (i.e., edges) to the graph
   */
  JMenuItem                    add_relationship_mi, 
  /**
   * Menu item to display the interactive edges dialog
   */
                               interactive_edges_mi,
  /**
   * Menu item to delete a relationship (i.e., edges) in the graph
   */
                               delete_relationship_mi,
  /**
   * Menu item to add header relationships (tablet headers) as a graph
   */
                               add_header_relationships_mi, 
  /**
   * Menu item for adding header relationships (with links to their data types)
   */
                               add_header_relationships_types_mi,
  /**
   * Menu item to add header star relationships (tablet headers) as a graph
   */
                               add_header_relationships_stars_mi, 
  /**
   * Menu item for adding header star relationships (with links to their data types)
   */
                               add_header_relationships_types_stars_mi;

  /**
   * String for no nodes
   */
  final static String NODE_SZ_INVISIBLE = "Hidden",     
  /**
   * String for small nodes
   */
                      NODE_SZ_SMALL     = "Small",
  /**
   * String for large nodes - most useful for manipulating the graph
   */
                      NODE_SZ_LARGE     = "Large",      
  /**
   * String for varying the node sizes
   */
                      NODE_SZ_VARY      = "Vary",
  /**
   * String for varying the node sizes (logarithmic)
   */
                      NODE_SZ_VARY_LOG  = "Vary (Log)", 
  /**
   * String for making the nodes into glyphs for their data types
   */
                      NODE_SZ_TYPE      = "Type",
  /**
   * String for showing graph characteristics
   */
                      NODE_SZ_GRAPHINFO = "Graph Info",
  /**
   * String for cluster coefficient sizing
   */
                      NODE_SZ_CLUSTERCO = "Cluster Coefficient",
  /**
   * String to make the node equal to the label
   */
                      NODE_SZ_LABEL     = "Label",
  /**
   * Small Multiple - Pie Chart Small
   */
                      NODE_SZ_SM_PIE_SMALL = "Pie Chart (Sm)",
  /**
   * Small Multiple - Pie Chart Large
   */
                      NODE_SZ_SM_PIE_LARGE = "Pie Chart (Lg)",
  /**
   * Small Multiple - XY ... uses visible timeframe for x, linear scaling of the count in y
   */
                      NODE_SZ_XY_VISIBLE          = "XY (Visible Timeframe)",
  /**
   * Small Multiple - XY ... uses visible timeframe for x, equal scaling of the count in y
   */
                      NODE_SZ_XY_VISIBLE_EQUAL   = "XY (Visible Timeframe, Equal)",
  /**
   * Small Multiple - XY ... uses local timeframe for x, linear scaling of the count in y
   */
                      NODE_SZ_XY_LOCAL           = "XY (Local Timeframe)",
  /**
   * Small Multiple - XY ... uses local timeframe for x, equal scaling of the count in y
   */
                      NODE_SZ_XY_LOCAL_EQUAL     = "XY (Local Timeframe, Equal)",
  /**
   * String for coloring the nodes white
   */
                      NODE_CO_WHITE     = "Default",      
  /**
   * String for varying the color of nodes based on the global color option
   */
                      NODE_CO_VARY      = "Vary",   
  /**
   * String for varying the node color based on a label option
   */
                      NODE_CO_LABEL     = "Label",
  /**
   * String for varying color by the cluster coefficient
   */
                      NODE_CO_CLUSTERCO = "Cluster Coefficient",

  /**
   * String for varying color by the data type of the entity
   */
                      NODE_CO_DATATYPE  = "Data Type",

  /**
   * String for making the link (edge) size normal (constant)
   */
                      LINK_SZ_NORMAL    = "Normal",     
  /**
   * String for making the link (edge) size thin (constant)
   */
                      LINK_SZ_THIN      = "Thin",
  /**
   * String for making the link (edge) size thick (constant)
   */
                      LINK_SZ_THICK     = "Thick",      
  /**
   * String for varying the link (edge) size linearly
   */
                      LINK_SZ_VARY      = "Vary",
  /**
   * String for hiding the links (edges)
   */
                      LINK_SZ_INVISIBLE = "Hidden",
  /**
   * String for link size by conductance
   */
                      LINK_SZ_CONDUCT   = "Conductance",
  /**
   * String for link size of cluster probability
   */
                      LINK_SZ_CLUSTERP = "Cluster Prob",
  /**
   * String for making the link (edge) color gray (constant)
   */
                      LINK_CO_GRAY      = "Gray",   
  /**
   * String for making the link (edge) color gray (constant)
   */
                      LINK_CO_WHITE     = "White",   
  /**
   * String for making the link (edge) color gray (constant)
   */
                      LINK_CO_DARKGRAY  = "Dark Gray",   

  /**
   * String for making the link (edge) color a reflection of the node types on each side of that edge
   */
                      LINK_CO_NODE_TYPES = "Node Types",

  /**
   * String for varying the link (edge) color based on the bundles
   */
                      LINK_CO_VARY      = "Vary";
  /**
   * Array for holding the node size strings
   */
  final static String NODE_SZ_STRS[]    = { NODE_SZ_GRAPHINFO, NODE_SZ_LARGE, NODE_SZ_SMALL, NODE_SZ_VARY,  NODE_SZ_VARY_LOG, NODE_SZ_TYPE,  
                                            NODE_SZ_INVISIBLE, NODE_SZ_CLUSTERCO, NODE_SZ_LABEL,
                                            KeyMaker.BY_MONTH_STR, KeyMaker.BY_DAYOFWEEK_STR, KeyMaker.BY_HOUR_STR,
                                            KeyMaker.BY_STRAIGHT_STR + " (Sm)", KeyMaker.BY_STRAIGHT_STR + " (Lg)",
                                            NODE_SZ_SM_PIE_SMALL, NODE_SZ_SM_PIE_LARGE,
                                            NODE_SZ_XY_VISIBLE, NODE_SZ_XY_VISIBLE_EQUAL, NODE_SZ_XY_LOCAL, NODE_SZ_XY_LOCAL_EQUAL },
  /**
   * Array for holding the node color strings
   */
                      NODE_CO_STRS[]    = { NODE_CO_VARY, NODE_CO_WHITE, NODE_CO_LABEL, NODE_CO_CLUSTERCO, NODE_CO_DATATYPE },
  /**
   * Array for holding the link (edge) size strings
   */
                      LINK_SZ_STRS[]    = { LINK_SZ_THIN, LINK_SZ_NORMAL, LINK_SZ_THICK, LINK_SZ_VARY, LINK_SZ_INVISIBLE, LINK_SZ_CONDUCT, LINK_SZ_CLUSTERP },
  /**
   * Array for holding the link (edge) color strings
   */
                      LINK_CO_STRS[]    = { LINK_CO_GRAY, LINK_CO_WHITE, LINK_CO_DARKGRAY, LINK_CO_NODE_TYPES, LINK_CO_VARY };
  /**
   * String for the filtering mode - this mode is most like the other visualization component interactions
   */
  final static String MODE_FILTER       = "Filter",
  /**
   * String for the edit mode - this mode is used to manipulate the location of nodes
   */
                      MODE_EDIT         = "Edit";
  /**
   * Array for holding the UI mode strings
   */
  final static String MODE_STRS[]       = { MODE_FILTER, MODE_EDIT };

  /**
   * Timeline Resolution Options
   */
  JRadioButtonMenuItem timeline_res_minutes_rbmi,
                       timeline_res_15minutes_rbmi,
                       timeline_res_hours_rbmi,
                       timeline_res_6hours_rbmi,
                       timeline_res_days_rbmi,
                       timeline_res_3days_rbmi;

  /**
   * For the Timeline Mode, return the resolution for matching timeline edges.
   *
   *@return resolution in milliseconds
   */
  public long timeLineResolution() {
    if      (timeline_res_minutes_rbmi.   isSelected()) return                  60L * 1000L;
    else if (timeline_res_15minutes_rbmi. isSelected()) return            15L * 60L * 1000L;
    else if (timeline_res_hours_rbmi.     isSelected()) return            60L * 60L * 1000L;
    else if (timeline_res_6hours_rbmi.    isSelected()) return       6L * 60L * 60L * 1000L;
    else if (timeline_res_days_rbmi.      isSelected()) return      24L * 60L * 60L * 1000L;
    else if (timeline_res_3days_rbmi.     isSelected()) return 3L * 24L * 60L * 60L * 1000L;
    else                                                return            60L * 60L * 1000L;
  }

  /**
   * Construct the panel by placing the GUI panel, setting up the popup menu, and adding
   * listeners for callback events.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt       application reference
   */
  public RTGraphPanel              (RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      {
    super(win_type, win_pos, win_uniq, rt); JMenuItem mi;
    JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, false, component = new RTGraphComponent(), createLabelsPanel(getRTParent().getEntityTagTypes()));
    split.setOneTouchExpandable(true); split.setResizeWeight(1.0);
    add("Center", split);

    // - Relationships...
    getRTPopupMenu().add(add_relationship_mi               = new JMenuItem("Add Edge Relationship..."));
    getRTPopupMenu().add(delete_relationship_mi            = new JMenuItem("Delete Edge Relationship..."));
    common_relationships_menu = new JMenu("Common Relationships"); getRTPopupMenu().add(common_relationships_menu); 
      fillCommonRelationshipsMenu();

    // - Copy and paste
    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(mi = new JMenuItem("Copy Selected Entities"));  mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { copySelection();          } } );
    getRTPopupMenu().add(mi = new JMenuItem("Select From Clipboard"));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { selectFromClipboard();    } } );
    getRTPopupMenu().add(mi = new JMenuItem("Copy Labels As Table"));    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { copyLabelsAsTable();      } } );

    // - Layout Stuff
    getRTPopupMenu().addSeparator(); getRTPopupMenu().addSeparator();
    JMenu layouts_menu = new JMenu("Layout");
    getRTPopupMenu().add(layouts_menu);

    // Make common layouts more accessible
    String layouts[] = GraphLayouts.getLayoutAlgorithms();

    // - Layout I/O
    layouts_menu.add(mi = new JMenuItem("Save Layout..."));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveLayout(false, null); } } );
    layouts_menu.add(mi = new JMenuItem("Save Layout (Selected Entities)..."));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveLayout(true,  null);  } } );
    layouts_menu.add(mi = new JMenuItem("Load Layout..."));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadLayout(false); } } );
    layouts_menu.add(mi = new JMenuItem("Load Layout (Apply To Selected)..."));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadLayout(true);  } } );

    // - Layout helpers
    layouts_menu.addSeparator();
    layouts_menu.add(mi = new JMenuItem("Subset One Bundle Per Edge (Fast UI)"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { subsetOneBundlePerEdge(); } } );
    layouts_menu.add(mi = new JMenuItem("IP Logical Octet Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { ipLogicalOctetLayout(); } } );
    layouts_menu.add(mi = new JMenuItem("Temporal Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { temporalLayout(); } } );
    layouts_menu.add(mi = new JMenuItem("Collapse Blocks"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { collapseBlocks(); } } );
    layouts_menu.addSeparator();
    JMenu keyword_treemap_menu = new JMenu("Keyword Treemaps"); layouts_menu.add(keyword_treemap_menu);
    keyword_treemap_menu.add(mi = new JMenuItem("First Match/Node Only..."));    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { keywordTreeMapLayout(true,  true);  } } );
    keyword_treemap_menu.add(mi = new JMenuItem("First Match/Assoc Fields...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { keywordTreeMapLayout(false, true);  } } );
    keyword_treemap_menu.add(mi = new JMenuItem("All Matches/Node Only..."));    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { keywordTreeMapLayout(true,  false); } } );
    keyword_treemap_menu.add(mi = new JMenuItem("All Matches/Assoc Fields...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { keywordTreeMapLayout(false, false); } } );
    keyword_treemap_menu.addSeparator();
    keyword_treemap_menu.add(mi = new JMenuItem("CIDR/ 8")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cidrTreeMapLayout(8);  } } );
    keyword_treemap_menu.add(mi = new JMenuItem("CIDR/16")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cidrTreeMapLayout(16); } } );
    keyword_treemap_menu.add(mi = new JMenuItem("CIDR/24")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cidrTreeMapLayout(24); } } );

    layouts_menu.addSeparator();
    layouts_menu.add(mi = new JMenuItem("Node Color TreeMap Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { nodeColorTreeMapLayout(); } } );
    layouts_menu.add(mi = new JMenuItem("Force Directed Color Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { nodeColorCompositeFDLayout(); } } );

    layouts_menu.addSeparator();
    layouts_menu.add(mi = new JMenuItem("Tag Type TreeMap Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { tagTypeTreeMapLayout(); } } );

    layouts_menu.addSeparator();
    layouts_menu.add(mi = new JMenuItem("UMAP Small Multiples Layout"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { smallMultiplesUMAPLayout(); } } );

    // - Specialized layouts
    layouts_menu.addSeparator();
    layouts_menu.add(mi = new JMenuItem("Geospatial"));      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { geospatialLayout(false); } } );
    // layouts_menu.add(mi = new JMenuItem("Geospatial (cc)")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { geospatialLayout(true);  } } );

    // - Layouts
    layouts_menu.addSeparator(); Map<String,JMenu> layout_cats = new HashMap<String,JMenu>(); Map<String,String> last_added = new HashMap<String,String>();
    for (int i=0;i<layouts.length;i++) {

      // Categorize
      String category = GraphLayouts.getLayoutCategory(layouts[i]);
      if (layout_cats.containsKey(category) == false) {
        JMenu jmenu;
        layouts_menu.add(jmenu = new JMenu(category));
        layout_cats.put(category, jmenu);
      }

      // Add a separator if the next menu item is different from the last one
      // if (last_added.containsKey(category) && Utils.levenshteinDistance(last_added.get(category), layouts[i]) >= 10) layout_cats.get(category).addSeparator();

      // Add the item
      layout_cats.get(category).add(mi = new JMenuItem(layouts[i]));
      last_added.put(category, layouts[i]);

      // Add the action listener for each layout
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        String algorithm = ((JMenuItem) ae.getSource()).getText();
        (new LayoutThread(algorithm)).start();
      } } );
    }

    // - Mode
    getRTPopupMenu().addSeparator();
    JMenu menu; 

    // - Sizes, Colors
    getRTPopupMenu().addSeparator();
    menu = new JMenu("Node Size");  getRTPopupMenu().add(menu); ButtonGroup bg = new ButtonGroup();
    node_sizes  = new JRadioButtonMenuItem[NODE_SZ_STRS.length]; 
    for (int i=0;i<NODE_SZ_STRS.length;i++) { 
      if (NODE_SZ_STRS[i].equals(KeyMaker.BY_MONTH_STR)) menu.addSeparator();
      if (NODE_SZ_STRS[i].equals(NODE_SZ_SM_PIE_SMALL))  menu.addSeparator();
      if (NODE_SZ_STRS[i].equals(NODE_SZ_XY_VISIBLE))    menu.addSeparator();
      menu.add(node_sizes[i]  = new JRadioButtonMenuItem(NODE_SZ_STRS[i],i==0)); 
      bg.add(node_sizes[i]);  
      defaultListener(node_sizes[i]);  
    }
    menu = new JMenu("Node Color"); getRTPopupMenu().add(menu); bg = new ButtonGroup();
    node_colors = new JRadioButtonMenuItem[NODE_CO_STRS.length]; for (int i=0;i<NODE_CO_STRS.length;i++) { menu.add(node_colors[i] = new JRadioButtonMenuItem(NODE_CO_STRS[i],i==0)); bg.add(node_colors[i]); defaultListener(node_colors[i]); }
    menu = new JMenu("Link Size");  getRTPopupMenu().add(menu); bg = new ButtonGroup();
    link_sizes  = new JRadioButtonMenuItem[LINK_SZ_STRS.length]; for (int i=0;i<LINK_SZ_STRS.length;i++) { menu.add(link_sizes[i]  = new JRadioButtonMenuItem(LINK_SZ_STRS[i],i==0)); bg.add(link_sizes[i]);  defaultListener(link_sizes[i]);  }
      menu.addSeparator();
      menu.add(link_trans_cbmi       = new JCheckBoxMenuItem("Enable Link Transpency", false));
      menu.add(link_curves_cbmi      = new JCheckBoxMenuItem("Use Curves"));
      menu.add(link_color_parts_cbmi = new JCheckBoxMenuItem("Use Color Parts (Beta)"));
      menu.add(arrows_cbmi           = new JCheckBoxMenuItem("Draw Arrows"));
      menu.add(timing_cbmi           = new JCheckBoxMenuItem("Draw Timing Marks"));
    menu = new JMenu("Link Color"); getRTPopupMenu().add(menu); bg = new ButtonGroup();
    link_colors = new JRadioButtonMenuItem[LINK_CO_STRS.length]; for (int i=0;i<LINK_CO_STRS.length;i++) { menu.add(link_colors[i] = new JRadioButtonMenuItem(LINK_CO_STRS[i],i==0)); bg.add(link_colors[i]); defaultListener(link_colors[i]); }

    // Label multi-line thresholds
    JMenu multiline_menu = new JMenu("Multilines"); bg = new ButtonGroup();
      multiline_menu.add(truncate_single_line_rbmi = new JRadioButtonMenuItem("Truncate to single line")); bg.add(truncate_single_line_rbmi);
      multiline_menu.add(truncate_four_line_rbmi   = new JRadioButtonMenuItem("Truncate to four lines"));  bg.add(truncate_four_line_rbmi);
      multiline_menu.add(no_truncation_rbmi        = new JRadioButtonMenuItem("No Truncation", true));     bg.add(no_truncation_rbmi);

      multiline_menu.addSeparator(); bg = new ButtonGroup();
      multiline_menu.add(multiline_32_rbmi         = new JRadioButtonMenuItem("32 Characters", true));     bg.add(multiline_32_rbmi);
      multiline_menu.add(multiline_64_rbmi         = new JRadioButtonMenuItem("64 Characters"));           bg.add(multiline_64_rbmi);
      multiline_menu.add(multiline_96_rbmi         = new JRadioButtonMenuItem("96 Characters"));           bg.add(multiline_96_rbmi);
      multiline_menu.add(multiline_128_rbmi        = new JRadioButtonMenuItem("128 Characters"));          bg.add(multiline_128_rbmi);

      defaultListener(truncate_single_line_rbmi);
      defaultListener(truncate_four_line_rbmi);
      defaultListener(no_truncation_rbmi);
      defaultListener(multiline_32_rbmi);
      defaultListener(multiline_64_rbmi);
      defaultListener(multiline_96_rbmi);
      defaultListener(multiline_128_rbmi);
    getRTPopupMenu().add(multiline_menu);

    // - Other options
    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(report_select_entities_cbmi   = new JCheckBoxMenuItem("For Reports, Add Entities On Selection",  false));

    // - Selection
    menu = new JMenu("Select By"); getRTPopupMenu().add(menu);
    Iterator<Utils.Symbol> it_sym = EnumSet.allOf(Utils.Symbol.class).iterator();
    while (it_sym.hasNext()) { Utils.Symbol symbol = it_sym.next(); menu.add(mi = new JMenuItem(""+symbol)); mi.addActionListener(new SymbolSelector(symbol)); }

    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(mi = new JMenuItem("Retain Visible Nodes Only (Destructive, Locally)"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { retainOnlyVisibleNodes(); } } );
    getRTPopupMenu().add(mi = new JMenuItem("Add Selection To Retained"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addSelectionToRetained(); } } );
    getRTPopupMenu().add(mi = new JMenuItem("Clear Retained Set"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clearRetained(); } } );

    getRTPopupMenu().addSeparator();

    JMenu rare_menu = new JMenu("Rare Options");

    menu = new JMenu("Mode"); rare_menu.add(menu); bg = new ButtonGroup();
    menu.add(edit_rbmi     = new JRadioButtonMenuItem("Edit",true)); bg.add(edit_rbmi);
    menu.add(filter_rbmi   = new JRadioButtonMenuItem("Filter"));    bg.add(filter_rbmi);
    menu.add(edgelens_rbmi = new JRadioButtonMenuItem("EdgeLens"));  bg.add(edgelens_rbmi);
    menu.add(timeline_rbmi = new JRadioButtonMenuItem("Timeline"));  bg.add(timeline_rbmi);

    menu = new JMenu("Timeline Res"); rare_menu.add(menu); bg = new ButtonGroup();
    menu.add(timeline_res_minutes_rbmi   = new JRadioButtonMenuItem("Minutes"));    bg.add(timeline_res_minutes_rbmi);
    menu.add(timeline_res_15minutes_rbmi = new JRadioButtonMenuItem("15 Minutes")); bg.add(timeline_res_15minutes_rbmi);
    menu.add(timeline_res_hours_rbmi     = new JRadioButtonMenuItem("Hours"));      bg.add(timeline_res_hours_rbmi);     timeline_res_hours_rbmi.setSelected(true);
    menu.add(timeline_res_6hours_rbmi    = new JRadioButtonMenuItem("6 Hours"));    bg.add(timeline_res_6hours_rbmi);
    menu.add(timeline_res_days_rbmi      = new JRadioButtonMenuItem("Days"));       bg.add(timeline_res_days_rbmi);
    menu.add(timeline_res_3days_rbmi     = new JRadioButtonMenuItem("3 Days"));     bg.add(timeline_res_3days_rbmi);

    rare_menu.addSeparator();

    rare_menu.add(interactive_edges_mi              = new JMenuItem("Interactive Edges..."));

    rare_menu.addSeparator();

    JMenu datascience_menu = new JMenu("Data Science"); rare_menu.add(datascience_menu);
      datascience_menu.add(add_header_relationships_mi             = new JMenuItem("Add Header Relationships"));
      datascience_menu.add(add_header_relationships_types_mi       = new JMenuItem("Add Header Relationships (Types)"));
      datascience_menu.add(add_header_relationships_stars_mi       = new JMenuItem("Add Header Star Relationships"));
      datascience_menu.add(add_header_relationships_types_stars_mi = new JMenuItem("Add Header Star Relationships (Types)"));

    rare_menu.addSeparator();

    rare_menu.add(mi = new JMenuItem("Layout Small Multiples Dialog (Beta)..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new LayoutSmallMultiplesDialog(); } } );

    rare_menu.addSeparator();

    rare_menu.add(mi = new JMenuItem("Export Graph \"As Is\"..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { exportGraphAsIs(); } } );

    // - Add labeling options
    menu = new JMenu("Tagging"); rare_menu.addSeparator(); rare_menu.add(menu);
      menu.add(mi = new JMenuItem("Tag Connected Components (Visible)")); 
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { tagConnectedComponents(); } } );
      menu.add(mi = new JMenuItem("Extend Tag From Node Label Color (Visible) (Undirected)"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { extendNodeLabelColorTagToNeighbors(false); } } );
      menu.add(mi = new JMenuItem("Extend Tag From Node Label Color (Visible) (Directed)"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { extendNodeLabelColorTagToNeighbors(true); } } );

    // - Background Stuff
    JMenu background_menu = new JMenu("Background"); rare_menu.addSeparator(); rare_menu.add(background_menu);
      bg = new ButtonGroup();
        nobg_rbmi      = new JRadioButtonMenuItem("None", true);   bg.add(nobg_rbmi);      background_menu.add(nobg_rbmi);      defaultListener(nobg_rbmi);
        background_menu.addSeparator();
        geo_out_rbmi   = new JRadioButtonMenuItem("Geo Outline");  bg.add(geo_out_rbmi);   /* background_menu.add(geo_out_rbmi);   */ defaultListener(geo_out_rbmi);
        geo_fill_rbmi  = new JRadioButtonMenuItem("Geo Fill");     bg.add(geo_fill_rbmi);  /* background_menu.add(geo_fill_rbmi);  */ defaultListener(geo_fill_rbmi);
        geo_touch_rbmi = new JRadioButtonMenuItem("Geo Touch");    bg.add(geo_touch_rbmi); /* background_menu.add(geo_touch_rbmi); */ defaultListener(geo_touch_rbmi);
        background_menu.addSeparator();
        kcores_rbmi    = new JRadioButtonMenuItem("KCores");       bg.add(kcores_rbmi);    background_menu.add(kcores_rbmi);    defaultListener(kcores_rbmi);


    rare_menu.addSeparator();
    rare_menu.add(dyn_label_cbmi                = new JCheckBoxMenuItem("Dynamic Labels",                          true));
    rare_menu.add(recalc_bounds_cbmi            = new JCheckBoxMenuItem("Recalculate Bounds On Re-Draw",           false));
    rare_menu.add(vertex_placement_heatmap_cbmi = new JCheckBoxMenuItem("Vertex Placement Heatmap (Experimental)", false));
    rare_menu.add(calculate_crossings_cbmi      = new JCheckBoxMenuItem("Calculate Crossings (Experimental)",      false));

    // - Add growth options
    menu = new JMenu("Expansion/Filter"); rare_menu.addSeparator(); rare_menu.add(menu);
      menu.add(mi = new JMenuItem("Add All Bundles On Visible Links"));         mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addAllOnVisibleLinks();    } } );
      menu.add(mi = new JMenuItem("Make 1-Hop Links Visible"));                 mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { makeOneHopsVisible(false); } } );
      menu.add(mi = new JMenuItem("Make 1-Hop Links Visible (Directional)"));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { makeOneHopsVisible(true);  } } );
      menu.addSeparator();
      menu.add(mi = new JMenuItem("Only Keep Bidirectional Links"));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { filterToBidirectionalLinks(); } } );

    // - Test graphs
    // -- Create the listener
    ActionListener graphfactory_al = new ActionListener() { public void actionPerformed(ActionEvent ae) {
      if (ae.getSource() instanceof JMenuItem) { 
        // Get the type from the label and instantiate the graph
        String type_str = ((JMenuItem) ae.getSource()).getText();
        GraphFactory.Type type = GraphFactory.toType(type_str);
        MyGraph mygraph = GraphFactory.createInstance(type, null);

        // Create the specialized tablet
        String header[] = { "from", "to", "source" };
        Tablet tablet = getRTParent().getRootBundles().findOrCreateTablet(header);

        // Go through the nodes and add them to the tablet
        for (int i=0;i<mygraph.getNumberOfEntities();i++) {
          String node = mygraph.getEntityDescription(i);
          for (int j=0;j<mygraph.getNumberOfNeighbors(i);j++) {
            String nbor = mygraph.getEntityDescription(mygraph.getNeighbor(i,j));

            // Add the record
            Map<String,String> attr = new HashMap<String,String>();
            attr.put("from",   node);
            attr.put("to",     nbor);
            attr.put("source", "GraphFactory");

            tablet.addBundle(attr);
        } } 
        // Force the change
        Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(getRTParent().getRootBundles());
        getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
        // Update the dropdowns
        getRTParent().updateBys();
    } } };

    // -- Fill out the menu for the test graphs
    menu = new JMenu("Test Graphs"); rare_menu.addSeparator(); rare_menu.add(menu);
    Iterator<GraphFactory.Type> it_gt = GraphFactory.graphTypeIterator();
    while (it_gt.hasNext()) {
      mi = new JMenuItem(GraphFactory.toString(it_gt.next()));
      menu.add(mi);
      mi.addActionListener(graphfactory_al);
    }

    getRTPopupMenu().add(rare_menu);

    // High resolution dialog
    /*
    getRTPopupMenu().add(mi = new JMenuItem("Create Hi Res Image..."));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { showHighResolutionExportDialog(); } } );
    */
    
    // Create the southern panel
    JPanel panel = new JPanel(new FlowLayout()); JButton clear_tags_bt, replace_tag_bt, remove_tag_bt;

    // -- shortcut for common layouts
    JButton bt;
    panel.add(bt = new JButton("H"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout(GraphLayouts.HYPERTREE_PLUS_STR);            } } );
      bt.setToolTipText(GraphLayouts.HYPERTREE_PLUS_STR);
    // panel.add(bt = new JButton("F"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout(GraphLayouts.MDS_ITERATIVE_DIRECT_STR);    } } );
    //   bt.setToolTipText(GraphLayouts.MDS_ITERATIVE_DIRECT_STR);
    // panel.add(bt = new JButton("F"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout(GraphLayouts.MDS_ITERATIVE_PROP_STR);      } } );
    //   bt.setToolTipText(GraphLayouts.MDS_ITERATIVE_PROP_STR);
    panel.add(bt = new JButton("Y"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout(GraphLayouts.YET_ANOTHER_SPRING_LAYOUT_NO_BARY_STR); } } );
      bt.setToolTipText(GraphLayouts.YET_ANOTHER_SPRING_LAYOUT_NO_BARY_STR);
    panel.add(bt = new JButton("F2"));  bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout(GraphLayouts.MDS_ITERATIVE_PROP_DEG_STR);    } } );
      bt.setToolTipText(GraphLayouts.MDS_ITERATIVE_PROP_DEG_STR);
    panel.add(bt = new JButton("C"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { buttonLayout("Node Color TreeMap Layout");                } } );
      bt.setToolTipText("Node Color TreeMap Layout");

    // -- selection
    panel.add(new JLabel("Sel"));
    panel.add(select_tf     = new JTextField(16)); 
    select_tf.setToolTipText(Selection.getToolTipHelp());
    new JTextFieldHistory(select_tf);

    // -- kb query
    panel.add(bt = new JButton("KB")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      if (getRTParent().getSelectedEntities().size() > 0) getRTParent().getControlPanel().kbQueryForSelectedDialog();
    } } );  bt.setToolTipText("Show the KB Source Query Dialog");

    // -- tagging
    panel.add(new JLabel("Tag"));
    panel.add(tag_tf        = new JTextField(16)); tag_tf.setToolTipText(Utils.getTagToolTip());       new JTextFieldHistory(tag_tf);

    panel.add(clear_tags_bt  = new JButton("Clr")); clear_tags_bt.  setToolTipText("Clear All Tags");
    panel.add(replace_tag_bt = new JButton("Rpl")); replace_tag_bt. setToolTipText("Replace Type Value Tag(s) w/ New Value");
    panel.add(remove_tag_bt  = new JButton("Rm"));  remove_tag_bt.  setToolTipText("Remove Tag");

    add("South", panel);

    // Listeners
    add_relationship_mi.addActionListener                     (new ActionListener() { public void actionPerformed(ActionEvent ae) { addRelationshipDialog();            } } );
    interactive_edges_mi.addActionListener                    (new ActionListener() { public void actionPerformed(ActionEvent ae) { interactiveEdgesDialog();           } } );
    delete_relationship_mi.addActionListener                  (new ActionListener() { public void actionPerformed(ActionEvent ae) { deleteRelationshipDialog();         } } );
    add_header_relationships_mi.addActionListener             (new ActionListener() { public void actionPerformed(ActionEvent ae) { addHeaderRelationships(false);      } } );
    add_header_relationships_types_mi.addActionListener       (new ActionListener() { public void actionPerformed(ActionEvent ae) { addHeaderRelationships(true);       } } );
    add_header_relationships_stars_mi.addActionListener       (new ActionListener() { public void actionPerformed(ActionEvent ae) { addHeaderRelationshipsStars(false); } } );
    add_header_relationships_types_stars_mi.addActionListener (new ActionListener() { public void actionPerformed(ActionEvent ae) { addHeaderRelationshipsStars(true);  } } );
    defaultListener(link_curves_cbmi);
    defaultListener(link_color_parts_cbmi);
    defaultListener(link_trans_cbmi);
    defaultListener(arrows_cbmi);
    defaultListener(timing_cbmi);

    select_tf.     addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { select(select_tf.getText());            } } );
    tag_tf.        addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { tagSelectedEntities(tag_tf.getText());  } } );
    clear_tags_bt. addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clearEntityTagsForSelectedEntities();   } } );
    replace_tag_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { replaceEntityTagForSelectedEntities();  } } );
    remove_tag_bt. addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeEntityTagForSelectedEntities();   } } );

  }

  /**
   * Execute a layout (from a shortcut button press)
   *
   *@param layout_str layout option
   */
  protected void buttonLayout(String layout_str) {
    if (layout_str.equals("Node Color TreeMap Layout")) { nodeColorTreeMapLayout(); }  else { (new LayoutThread(layout_str)).start(); }
  }

  /**
   * Export the graph as it is currently represented on the screen.  Does not export the underlying records... just the graph edges.
   */
  protected void exportGraphAsIs() {
    // Show a file chooser
    JFileChooser file_chooser = new JFileChooser(".");
    if (file_chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      // Determine the save files -- one for the edges, one for the layout
      File edge_file   = file_chooser.getSelectedFile();
      File layout_file = new File(edge_file.getParentFile(), edge_file.getName() + ".layout.csv");

      // Make the header map
      Map<String,Set<String>> hdr_map = new HashMap<String,Set<String>>();
      Set<String> nodes_saved = new HashSet<String>(); 

      // Go through all the nodes
      for (int node_i=0;node_i<digraph.getNumberOfEntities();node_i++) {
        String       node       = digraph.getEntityDescription(node_i); nodes_saved.add(node);
        Utils.Symbol node_shape = entity_to_shape.get(node);

        for (int i=0;i<digraph.getNumberOfNeighbors(node_i);i++) {
          int          nbor_i     = digraph.getNeighbor(node_i,i);
          String       nbor       = digraph.getEntityDescription(nbor_i); nodes_saved.add(nbor);
          Utils.Symbol nbor_shape = entity_to_shape.get(nbor);

          // Create the header... make sure the map entry exist... and add the line
          String hdr = "from_"+ node_shape + ",to_" + nbor_shape;
          if (hdr_map.containsKey(hdr) == false) hdr_map.put(hdr, new HashSet<String>());
          hdr_map.get(hdr).add(Utils.encToURL(node) + "," + Utils.encToURL(nbor));
        }
      }

      try {
        PrintStream out;

        // Save to file -- edges first
        out = new PrintStream(new FileOutputStream(edge_file));
        Iterator<String> it = hdr_map.keySet().iterator(); while (it.hasNext()) {
          String hdr = it.next(); out.println(hdr); Iterator<String> it_lines = hdr_map.get(hdr).iterator(); while (it_lines.hasNext()) {
            out.println(it_lines.next());
          }
          out.println();
        }
        out.close();

        // Save to file -- node positions
        out = new PrintStream(new FileOutputStream(layout_file));
        it = nodes_saved.iterator(); while (it.hasNext()) {
          String entity = it.next();
          out.println(Utils.encToURL(entity) + "," + Utils.encToURL(""+entity_to_wxy.get(entity).getX()) + "," + Utils.encToURL(""+entity_to_wxy.get(entity).getY()));
        }
        out.close();

        // Let user know it worked..
        JOptionPane.showMessageDialog(file_chooser, "Files Saved", "Graph Save \"As Is\" Success", JOptionPane.PLAIN_MESSAGE);

      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(file_chooser, "IOException: " + ioe, "Graph Save \"As Is\" Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Create a representation of the currently visible graph...
   *
   *@param directed make a directed graph // else make an undirected graph
   */
  protected MyGraph createVisibleGraph(boolean directed) {
    // Create the graph as it is currently seen
    SimpleMyGraph visible_graph = new SimpleMyGraph();

    SimpleMyGraph<Bundle> g; if (directed) g = digraph; else g = graph;

    // Visible records
    Set<Bundle> visibles = getRTParent().getVisibleBundles().bundleSet();
    // For all of the nodes...
    for (int node_i=0;node_i<g.getNumberOfEntities();node_i++) { String node = g.getEntityDescription(node_i);
      // Get their neighbors...
      for (int i=0;i<g.getNumberOfNeighbors(node_i);i++) {
        int nbor_i = g.getNeighbor(node_i, i); String nbor = g.getEntityDescription(nbor_i);

        // Iterate over the edge records -- if one exists, then mark that edge as added
        boolean edge_visible = false;
        Iterator<Bundle> it = g.linkRefIterator(g.linkRef(node_i,nbor_i));
        while (edge_visible == false && it.hasNext()) if (visibles.contains(it.next())) edge_visible = true;

        // Add the edge to the visible graph ... add both directions if it's an undirected graph
        if (edge_visible) { 
                                 visible_graph.addNeighbor(node, nbor);
          if (directed == false) visible_graph.addNeighbor(nbor, node);
        } 
      }
    }

    return visible_graph;
  }

  /**
   * For the visible nodes and their relationships, extend the specific node label color (really the tag itself)
   * to the neighbors.  If there's a selection, only expand from the selection...  Otherwise, expand from all visible
   * along visible edges.
   *
   *@param use_directed use a directed graph
   */
  public void extendNodeLabelColorTagToNeighbors(boolean use_directed) {
    // Is there a label color?  Don't move forward without it...  give something descriptive because i'll never remember how this works after this week...
    java.util.List<String> as_list = listEntityColor();
    if (as_list == null || as_list.size() == 0) { System.err.println("Node Color Label is empty");              return; }
    if (as_list.size()  >  1)                   { System.err.println("Node Color Label has too many entries");  return; }
    String label = as_list.get(0);
    if (label.startsWith(TAG_TYPE_LM) == false) { System.err.println("Entry \"" + label + "\" not a tag type"); return; }
    String tag_type = label.substring(TAG_TYPE_LM.length(),label.length());

    // Get the selection... make sure it's not null
    Set<String> sel = getRTParent().getSelectedEntities(); if (sel == null) sel = new HashSet<String>();

    // Create the visible graph representation
    MyGraph g = createVisibleGraph(use_directed);

    // For every node, determine if it has that tag... and then if so, should it be conveyed to its neighbor?
    Map<String,Set<String>> to_apply = new HashMap<String,Set<String>>(); // what to apply...
    for (int node_i=0;node_i<g.getNumberOfEntities();node_i++) {
      String node = g.getEntityDescription(node_i); 
      if (sel.size() > 0 && sel.contains(node) == false) continue; // If there's a selection, make sure this node is in it

      // Get the values for this type...
      Set<String> tag_type_values = getTagValuesOfType(node,tag_type);
      if (tag_type_values == null || tag_type_values.size() == 0) continue;

      // Go through the neighbors...
      for (int j=0;j<g.getNumberOfNeighbors(node_i);j++) {
        int nbor_i = g.getNeighbor(node_i, j); String nbor = g.getEntityDescription(nbor_i);

        Set<String> nbor_tag_type_values = getTagValuesOfType(nbor,tag_type);

        Iterator<String> it = tag_type_values.iterator(); while (it.hasNext()) {
          String value = it.next(); if (nbor_tag_type_values.contains(value) == false) {
            if (to_apply.containsKey(nbor) == false) to_apply.put(nbor, new HashSet<String>());
            to_apply.get(nbor).add(value);
          }
        }
      }
    }

    // Apply the new values
    Iterator<String> it = to_apply.keySet().iterator(); while (it.hasNext()) {
      String node = it.next(); 
      Set<String> new_values = to_apply.get(node); 
      Iterator<String> it_value = new_values.iterator(); while (it_value.hasNext()) {
        String value = it_value.next();
        getRTParent().tagEntity(node, tag_type + "=" + value);
      }
    }
  }
 
  /**
   * Helper method to get the specific typed tags for an entity.
   *
   *@param entity   entity to retrieve tags for
   *@param tag_type tag type to return
   *
   *@return set of the values for that tag type
   */
  protected Set<String> getTagValuesOfType(String entity, String tag_type) {
    Set<String> set  = new HashSet<String>();
    Set<String> tags = getRTParent().getEntityTags(entity, getRTParent().getRootBundles().ts0(), getRTParent().getRootBundles().ts1());
    Iterator<String> it = tags.iterator(); while (it.hasNext()) {
      String tag = it.next();
      if (Utils.tagIsTypeValue(tag)) { String strs[] = Utils.separateTypeValueTag(tag); if (strs[0].equals(tag_type)) set.add(strs[1]); }
    }
    return set;
  }

  /**
   * For the visible connected components, label the nodes with the tag "component" and an incremented value
   * of the component number (arbitrarily assigned).
   */
  public void tagConnectedComponents() {
    MyGraph visible_graph = createVisibleGraph(false);

    // Run the connected components algorithm
    Set<Set<String>> components = GraphUtils.connectedComponents(new UniGraph(visible_graph));

    // Label them with the "component"
    int component_i = 0; Iterator<Set<String>> it_comp = components.iterator(); while (it_comp.hasNext()) {
      Set<String> component = it_comp.next(); Iterator<String> it_node = component.iterator(); while (it_node.hasNext()) {
        String node = it_node.next();
        getRTParent().tagEntity(node, "component=" + component_i);
      }
      component_i++;
    }
  }

  /**
   * Class to contain a layout thread - required so that the gui worker thread can get its own time
   */
  class LayoutThread extends Thread {
    String layout_algorithm = null; public LayoutThread(String layout_algorithm) { this.layout_algorithm = layout_algorithm; }
    public void run() {
      // Get and validate the render context
      RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

      // Create a map of the node to colors and vice versa ... assumes coloring is set to label and the layout method has the word color in it (kindof a hack...)
      Map<Color,Set<String>> color_to_nodes = null;
      Map<String,Color>      node_to_color  = null;
      NodeColor node_color = getNodeColor(); if (node_color != NodeColor.LABEL || layout_algorithm.toLowerCase().indexOf("color") < 0) { node_color = null; } else {
        color_to_nodes = new HashMap<Color,Set<String>>();
        node_to_color  = new HashMap<String,Color>();
        myrc.createNodeColorMaps(node_color, color_to_nodes, node_to_color, false);
      }

      // Execute the layout
      LayoutProgressDialog layout_progress_dialog = new LayoutProgressDialog();
      try { 
        saveLayoutForUndo(entity_to_wxy, null);
        (new GraphLayouts()).executeLayoutAlgorithm(layout_algorithm, 
                                                    graph, 
                                                    myrc.filterEntities(getRTParent().getSelectedEntities()), 
                                                    entity_to_wxy, 
                                                    layout_progress_dialog,
                                                    color_to_nodes,
                                                    node_to_color);
      } finally {
        if (layout_progress_dialog != null) {
          layout_progress_dialog.setVisible(false);
          layout_progress_dialog.dispose();
        }
      }
        
      // Rebound and redraw
      zoomToFit(myrc); getRTComponent().render();
  } }

  /**
   * Dialog to show the progress of the layout algorithm.
   */
  class LayoutProgressDialog extends JDialog implements GraphLayoutProgress {
    private static final long serialVersionUID = -226947715925234838L;
    /**
     * Cancel the layout
     */
    boolean cancel_layout = false;

    /**
     * Progress bar
     */
    JProgressBar progress_bar;

    /**
     * Simple graph component
     */
    Grapher      grapher;

    /**
     * Create the dialog and display it.
     */
    public LayoutProgressDialog() {
      super(getRTParent(), "Layout Progress", false);
      getContentPane().setLayout(new BorderLayout(10,10));

      // Progress bar
      getContentPane().add("North", progress_bar = new JProgressBar());

      // Grapher
      getContentPane().add("Center", grapher = new Grapher());

      // Cancel button
      JPanel buttons = new JPanel(new FlowLayout()); JButton button;
      buttons.add(button = new JButton("Cancel"));
      button.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cancel_layout = true; } } );
      getContentPane().add("South", buttons);

      pack(); setSize(640,384); setVisible(true);
    }

    /**
     * Receive updates from the layout algorithm.
     */
    public boolean update(double vel, int i, int max) { 
      // make sure the max is at least one
      if (max < i) max = i + 1;

      // Set min, value, and max
      progress_bar.setMinimum(0); progress_bar.setValue(i); progress_bar.setMaximum(max);
      grapher.add(vel);

      // Return flag to indicate if layout should be canceled
      return cancel_layout; 
    }

    /**
     * Return true if the canceled button has been pressed.
     *
     *@return true to cancel layout operation
     */
    public boolean canceled() { return cancel_layout; }

    /**
     * Set an image that shows what the graph looks like so far.
     *
     *@param image graph image
     *
     *@return true to cancel layout operation
     */
    public boolean setGraphImage(BufferedImage image) {
      grapher.setGraphImage(image);
      return cancel_layout;
    }

    /**
     * Simple scrolling graph
     */
    class Grapher extends JComponent {
      private static final long serialVersionUID = -4869479122125234838L;
      public Grapher() { Dimension dim = new Dimension(512,256); setPreferredSize(dim); setMinimumSize(dim); }
      double max_seen = Double.NEGATIVE_INFINITY, min_seen = 0.0;
      List<Double> vels = new ArrayList<Double>();
      public void add(double v) { 
        if (Double.isNaN(v) || Double.isInfinite(v)) v = 10000.0;
        if (v > 10000.0) v = 10000.0; // cap it at 10k
        if (v < 0.0)     v = 0.0;     // make sure it isn't negative
        vels.add(v);
        if (v > max_seen) max_seen = v; 
        if (v < min_seen) min_seen = v;
        while (vels.size() > getWidth()) vels.remove(0); 
        repaint(); 
      }
      BufferedImage image = null;
      public void setGraphImage(BufferedImage image0) {
        this.image = image0;
      }
      public void paintComponent(Graphics g) {
        g.setColor(RTColorManager.getColor("background","default")); g.fillRect(0,0,getWidth(),getHeight());
        if (vels.size() > 0) {
          int h = getHeight();
          int txt_h = Utils.txtH((Graphics2D) g, "0");

          // Min and max labels
          g.setColor(RTColorManager.getColor("label", "default"));
          g.drawString(Utils.humanReadableDouble(max_seen), 5, txt_h + 5);
          g.drawString(Utils.humanReadableDouble(min_seen), 5, getHeight() - 5);

          // Lines @ 10s
          g.setColor(RTColorManager.getColor("axis", "minor"));
          double d = 1.0; while (d < max_seen) {
            int    y = (int) (h - h * ((Math.log(d + 1.0) - Math.log(min_seen + 1.0)) / (Math.log(max_seen + 1.0) - Math.log(min_seen + 1.0))));
            g.drawLine(0, y, getWidth(), y);
            g.drawString(""+d, getWidth() - 5 - Utils.txtW((Graphics2D) g, ""+d), y - 2);
            d *= 10.0;
          }

          // Data
          for (int x=0;x<vels.size();x++) {
            d = vels.get(x);
            int    y = (int) (h - h * ((Math.log(d + 1.0) - Math.log(min_seen + 1.0)) / (Math.log(max_seen + 1.0) - Math.log(min_seen + 1.0))));
            g.setColor(RTColorManager.getColor("data", "default"));
            g.fillRect(x,y,1,1);
            if (x == vels.size()-1) {
              String s = Utils.humanReadableDouble(d);
              g.setColor(RTColorManager.getColor("label", "default"));
              int label_y = y + txt_h/2; if (label_y > (getHeight() - 5)) label_y = getHeight() - 5;
              g.drawString(s, getWidth() - 5 - Utils.txtW(s), label_y);
            }
          }
        }

        if (image != null) {
          ((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.6f));
          g.drawImage(image, getWidth() - image.getWidth(), 0, null);
        }
      }
    }
  }

  /**
   * Show the dialog to create a high resolution version of the currently
   * rendered graph.
   */
  protected void showHighResolutionExportDialog() {
    HighResolutionExportDialog dialog = new HighResolutionExportDialog();
    dialog.setVisible(true);
  }

  /**
   * Create a dialog to export a higher resolution picture of the
   * graph.  This is useful for seeing labels in the graph and
   * other details that can't be fully seen on a single screen.
   */
  class HighResolutionExportDialog extends JDialog {
    private static final long serialVersionUID = -1165469122525234838L;
    /**
     * base filename textfield
     */
    JTextField fn_tf,

    /**
     * textfield that shows the scale (not editable)
     */
               scale_tf;
    /**
     * Scale slider -- divide by 10.
     */
    JSlider    scale_sl;

    /**
     * Construct the dialog.
     */
    public HighResolutionExportDialog() {
      super(getRTParent(), "Hi Res Export...", true);

      setLayout(new BorderLayout(5,5));

      JPanel middle = new JPanel(new BorderLayout());
      JPanel labels = new JPanel(new GridLayout(2,1,5,5));
        labels.add(new JLabel("Scale"));
        labels.add(new JLabel(""));
        labels.add(new JLabel("Filename"));
      middle.add("West", labels);

      JPanel input = new JPanel(new GridLayout(2,1,5,5));
        input.add(scale_sl  = new JSlider(20,40,20));
        input.add(fn_tf     = new JTextField("graph"));
      middle.add("Center", input);

      JPanel adds = new JPanel(new GridLayout(2,1,5,5));
        adds.add(scale_tf  = new JTextField("2.0 x")); scale_tf.setEditable(false);
        adds.add(new JLabel(""));
      middle.add("East", adds);

      scale_sl.addChangeListener(new ChangeListener() {
        public void stateChanged(ChangeEvent ce) {
        float scale = scale_sl.getValue() / 10.0f;
        scale_tf.setText("" + scale + " x");
      } } );

      add("Center", middle);
      JPanel south = new JPanel(new FlowLayout());
      JButton bt;
      south.add(bt = new JButton("Create")); bt.addActionListener(new ActionListener() {
                                               public void actionPerformed(ActionEvent ae) {
                                                 ((RTGraphComponent) getRTComponent()).
                                                   createHighResolutionExport(scale_sl.getValue()/10.0f, fn_tf.getText());
                                                 setVisible(false);
                                               } } );
      south.add(bt = new JButton("Cancel")); bt.addActionListener(new ActionListener() {
                                               public void actionPerformed(ActionEvent ae) {
                                                 setVisible(false);
                                               } } );
      add("South", south); pack();
    }
  }

  /**
   * Simple class to select by symbol
   */
  class SymbolSelector implements ActionListener {
    Utils.Symbol symbol; public SymbolSelector(Utils.Symbol symbol) { this.symbol = symbol; }
    public void actionPerformed(ActionEvent ae) {
      RTGraphComponent comp = ((RTGraphComponent) getRTComponent()); Set<String> sel = new HashSet<String>();
      Iterator<String> it = entity_to_shape.keySet().iterator();
      while (it.hasNext()) {
        String entity = it.next();
        if (entity_to_shape.get(entity).equals(symbol)) sel.add(entity);
      }
      comp.setOperation(sel);
    }
  }

  /**
   * Truncate to a single line rbmi
   */
  JRadioButtonMenuItem truncate_single_line_rbmi,

  /**
   * Truncate to four lines rbmi
   */
                       truncate_four_line_rbmi,
  /**
   * No truncation
   */
                       no_truncation_rbmi,
  /**
   * 32 character threshold
   */
                       multiline_32_rbmi,
  /**
   * 64 character threshold
   */
                       multiline_64_rbmi,
  /**
   * 96 character threshold
   */
                       multiline_96_rbmi,
  /**
   * 128 character threshold
   */
                       multiline_128_rbmi;

  /**
   * Return the multiline trunction setting.  A value of -1 means that no truncation should occur.
   *
   *@return multiline trunction setting
   */
  public int multilineTruncate() {
    if      (truncate_single_line_rbmi.isSelected()) return  1;
    else if (truncate_four_line_rbmi.  isSelected()) return  4;
    else                                             return -1;
  }

  /**
   * Set the multiline truncation setting.  A value of -1 means that no truncation should occur.
   *
   *@param multiline truncation setting
   */
  public void multilineTruncate(int trunc) {
    if      (trunc ==  1) truncate_single_line_rbmi.setSelected(true);
    else if (trunc ==  4) truncate_four_line_rbmi.  setSelected(true);
    else if (trunc == -1) no_truncation_rbmi.       setSelected(true);
    else throw new RuntimeException("No truncation for " + trunc + " setting");
  }

  /**
   * Return the multiline threshold.  This is the number of characters per line.
   *
   *@return multiline threshold
   */
  public int multilineThreshold() {
    if      (multiline_32_rbmi.isSelected())  return 32;
    else if (multiline_64_rbmi.isSelected())  return 64;
    else if (multiline_96_rbmi.isSelected())  return 96;
    else if (multiline_128_rbmi.isSelected()) return 128;
    else                                      return 32;
  }

  /**
   * Set the multiline threshold.  This is the number of characters per line.
   *
   *@param thresh multiline threshold
   */
  public void multilineThreshold(int thresh) {
    if      (thresh == 32)  multiline_32_rbmi. setSelected(true);
    else if (thresh == 64)  multiline_64_rbmi. setSelected(true);
    else if (thresh == 96)  multiline_96_rbmi. setSelected(true);
    else if (thresh == 128) multiline_128_rbmi.setSelected(true);
    else throw new RuntimeException("No threshold for " + thresh + " setting");
  }

  /**
   * Based on the small multiple type, create feature vectors for each node (may be grouped together nodes).
   * Apply the UMAP embedding based on the distances between the feature vectors.  Probably isn't going to work
   * well on a large number of items.  Modifies the entity_to_wxy with the results.
   */
  public void smallMultiplesUMAPLayout() {
    // Only keep bundles that are visible
    Bundles   visible   = getRTParent().getVisibleBundles();
    NodeSize  node_size = getNodeSize();

    // Only allow certain types of small multiples at first...
    if ((node_size != NodeSize.SM_MONTH)          && (node_size != NodeSize.SM_DOW)              && (node_size != NodeSize.SM_HOUR)           &&
        (node_size != NodeSize.SM_STRAIGHT_SMALL) && (node_size != NodeSize.SM_STRAIGHT_LARGE)   &&
        (node_size != NodeSize.SM_XY_VISIBLE)     && (node_size != NodeSize.SM_XY_VISIBLE_EQUAL) && 
        (node_size != NodeSize.SM_XY_LOCAL)       && (node_size != NodeSize.SM_XY_LOCAL_EQUAL)) {
      JOptionPane.showMessageDialog(file_chooser, "Node Size \"" + node_size + "\" Unsupported", "Node Size Unsupported", JOptionPane.ERROR_MESSAGE);
      return;
    }

    // First step is to collapse all of the entities into their common/overlapping coordinates
    // and then grab all of the application records (bundles) associated with that point.
    Map<Point2D,String>      pt_key_lu      = new HashMap<Point2D,String>();
    Map<String,Set<Bundle>>  pt_key_bundles = new HashMap<String,Set<Bundle>>();
    Map<String,Set<String>>  pt_key_to_ents = new HashMap<String,Set<String>>();

    Iterator<String> it_ent = entity_to_wxy.keySet().iterator(); while (it_ent.hasNext()) {
      String   entity   = it_ent.next(); 

      // Track the mapping from the Point2D to a String version
      Point2D  pt       = entity_to_wxy.get(entity); 
      String   pt_key   = pt.getX() + "," + pt.getY();
      pt_key_lu.put(pt, pt_key);
      if (pt_key_bundles.containsKey(pt_key) == false) pt_key_bundles.put(pt_key, new HashSet<Bundle>());
      if (pt_key_to_ents.containsKey(pt_key) == false) pt_key_to_ents.put(pt_key, new HashSet<String>());
      pt_key_to_ents.get(pt_key).add(entity);

      // Get all of the records associated with any entities that are on this same point
      int      entity_i = graph.getEntityIndex(entity);
      for (int i=0;i<graph.getNumberOfNeighbors(entity_i);i++) {
        int    nbor_i   = graph.getNeighbor(entity_i, i);
        String link_ref = graph.getLinkRef(entity_i, nbor_i);
        Iterator<Bundle> it_bun = graph.linkRefIterator(link_ref); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next(); 
          if (visible.bundleSet().contains(bundle) == false) continue; // Only keep bundles that are in the visible (not necessarily visible in this transform)
          pt_key_bundles.get(pt_key).add(bundle);
        }
      }
    }

    // Second step is to calculate the distances between the bundle set... this creates
    // an array of features.
    Map<String,float[]> feature_map = new HashMap<String,float[]>();
    Iterator<String> it = pt_key_bundles.keySet().iterator(); while (it.hasNext()) {
      String pt_key = it.next(); Set<Bundle> bundle_set = pt_key_bundles.get(pt_key); if (bundle_set.size() == 0) continue;
      feature_map.put(pt_key, bundleSetFeatureVector(bundle_set, node_size));
    }
    List<String> sorted = new ArrayList<String>(); sorted.addAll(feature_map.keySet()); Collections.sort(sorted);
    float d[][] = new float[sorted.size()][sorted.size()]; float max_rms = 0.0f;
    for (int i=0;i<sorted.size();i++) for (int j=0;j<=i;j++) {
      d[i][j] = vectorDistance(feature_map.get(sorted.get(i)), feature_map.get(sorted.get(j)));
      d[j][i] = d[i][j]; // mirror it... make the matrix symmetric
      if (max_rms < d[j][i]) max_rms = d[j][i];
    }
    if (max_rms == 0.0) max_rms = 1.0f;
    for (int i=0;i<d.length;i++) for (int j=0;j<d[i].length;j++) d[i][j] = d[i][j] / max_rms; // Normalize to 1.0

    // Third step is to apply the UMAP transformation
    // - Derived from code at https://github.com/tag-bio/umap-java
    Umap umap = new Umap();
    umap.setNumberComponents(2);          // Number of dimensions in result
    umap.setNumberNearestNeighbours(15);  // Maybe this should be 15 or the max number of things? (says 10 to 15 is sensible at url)
    umap.setThreads(8);                   // Parallelism
    float results[][] = umap.fitTransform(d);

    // Fourth step is to copy the locations back into the world xy coordinates and then force a transform... and re-render
    // System.err.println("Results = " + results.length + " x " + results[0].length + " ... sorted.size() = " + sorted.size());
    for (int i=0;i<results.length;i++) { 
      String  pt_key = sorted.get(i);
      Point2D pt     = new Point2D.Double(results[i][0], results[i][1]);
      Iterator<String> it_ents = pt_key_to_ents.get(pt_key).iterator(); while (it_ents.hasNext()) {
        String entity = it_ents.next();
        entity_to_wxy.put(entity, pt);
      }
    }
    transform();
    zoomToFit(); 
    repaint();
  }

  /**
   * Calculate the distance between two vectors ... rms error...  probably
   * should be something else...
   *
   *@param v0 first vector
   *@param v1 second vector
   *
   *@return root mean square error between the two vectors
   */
  protected float vectorDistance(float v0[], float v1[]) {
    double sum = 0.0; for (int i=0;i<v0.length;i++) sum += (v0[i] - v1[i])*(v0[i] - v1[i]);
    return (float) Math.sqrt(sum/v0.length);
  }

  //
  // Ordering... for vector creation and small multiples rendering
  //
  String sm_months[] = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" },
         sm_dows[]   = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
         sm_hours[]  = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                         "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23" };

  /**
   * Create a feature vector for a set of bundles.  The type of feature vector depends
   * on the small multiples node size setting ... but should be the same size for the 
   * same type of node_size.
   *
   *@param set        set of records
   *@param node_size  the type of small multiples
   *
   *@return feature vector of the records
   */
  protected float[] bundleSetFeatureVector(Set<Bundle> set, NodeSize node_size) {
    float vec[] = null; 
    //
    // Discrete Options ----------------------------------------------------------------------------------
    //
    if (node_size == NodeSize.SM_MONTH || node_size == NodeSize.SM_DOW || node_size == NodeSize.SM_HOUR) {
      BundlesCounterContext bcc_lite = null; String order[] = null;
      switch (node_size) {
        case SM_MONTH:          order = sm_months;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_MONTH_STR);
                                break;

        case SM_DOW:            order = sm_dows;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_DAYOFWEEK_STR);
                                break;

        case SM_HOUR:           order = sm_hours;
                                bcc_lite = new BundlesCounterContext(getRTParent().getVisibleBundles(),
                                                                     getRTParent().getControlPanel().getCountBy(),
                                                                     null, set, KeyMaker.BY_HOUR_STR);
                                break;
        case LARGE:
        case GRAPHINFO:
        case SM_XY_VISIBLE:
        case SM_XY_VISIBLE_EQUAL:
        case SM_XY_LOCAL:
        case SM_XY_LOCAL_EQUAL:
        case SM_STRAIGHT_SMALL:
        case SM_STRAIGHT_LARGE:
        case SM_PIE_SMALL:
        case SM_PIE_LARGE:
        case SMALL:
        case LABEL:
        case INVISIBLE:
        case TYPE:
        case CLUSTERCO:
        case VARY:
        case VARY_LOG:   System.err.println("Unreachable Case Statements in bundleSetFeatureVector()");
      }

      vec = new float[order.length];
      Map<String,Integer> to_i = new HashMap<String,Integer>(); 
      for (int i=0;i<order.length;i++) to_i.put(order[i], i);
      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) { 
        String bin = it_bin.next(); vec[to_i.get(bin)] = (float) bcc_lite.totalNormalized(bin); 
      }

    //
    // Continuous Options ----------------------------------------------------------------------------------
    //
    } else if (node_size == NodeSize.SM_STRAIGHT_LARGE || node_size == NodeSize.SM_STRAIGHT_SMALL) {
      // Use the same values as the rendering for the vector size
      if (node_size == NodeSize.SM_STRAIGHT_LARGE) vec = new float[64]; else vec = new float[32];

      // Keep track of strings to integers...
      Map<String,Integer> x_map = new HashMap<String,Integer>();

      Bundles visible = getRTParent().getVisibleBundles();

      // Determine which tablets can count
      Set<String> tablet_counts = new HashSet<String>();
      Iterator<Tablet> it_tab = visible.tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next();
        if (KeyMaker.tabletCompletesBlank(tablet, getRTParent().getControlPanel().getCountBy())) tablet_counts.add(tablet.fileHeader());
      }

      // Run through the records and place them on the x axis
      BundlesCounterContext bcc_lite = new BundlesCounterContext(visible, getRTParent().getControlPanel().getCountBy(), null);
      Iterator<Bundle> it = set.iterator(); while (it.hasNext()) {
        Bundle bundle = it.next(); if (bundle.hasTime() && tablet_counts.contains(bundle.getTablet().fileHeader())) {
          int    x     = (int) (((vec.length-1) * (bundle.ts0() - visible.ts0())) / (visible.ts1() - visible.ts0()));
          String x_str = "" + x; x_map.put(x_str, x);
          bcc_lite.count(bundle, x_str);
        }
      }

      Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
        String bin = it_bin.next(); vec[x_map.get(bin)] = (float) bcc_lite.totalNormalized(bin);
      }

    //
    // XY Options ----------------------------------------------------------------------------------
    //
    } else if (node_size == NodeSize.SM_XY_VISIBLE || node_size == NodeSize.SM_XY_VISIBLE_EQUAL ||
               node_size == NodeSize.SM_XY_LOCAL   || node_size == NodeSize.SM_XY_LOCAL_EQUAL) {

      Bundles visible = getRTParent().getVisibleBundles();
      int     total_w = 64,
              total_h = 32;

      boolean global_time;
      if (node_size == NodeSize.SM_XY_VISIBLE || 
          node_size == NodeSize.SM_XY_VISIBLE_EQUAL) global_time = true; 
      else                                           global_time = false;

      boolean y_equal_spacing;
      if (node_size == NodeSize.SM_XY_VISIBLE_EQUAL ||
          node_size == NodeSize.SM_XY_LOCAL_EQUAL)   y_equal_spacing = true;
      else                                           y_equal_spacing = false;

      vec = XYRenderer.vectorTimeInX(set, total_w, total_h, visible, global_time, noCountByBundles(getRTParent().getControlPanel().getCountBy()), y_equal_spacing);
    }

    return vec;
  }

  /**
   * Really stupid... there's no easy way to fix how the framework processes the "count by bundles" option
   * as a field... the framework would take more work to figure it out and may make other parts of the application
   * unstable.  This method makes sure the that the "count by buns" can't be passed as a y-axis option for the
   * XY scatterplot small multiples.
   */
  private String noCountByBundles(String str) {
    if (str.equals(BundlesDT.COUNT_BY_BUNS)) return KeyMaker.TABLET_SEP_STR;
    else                                     return str;
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "linknode"; }

  /**
   * Simple accessor to get the transformative version of this class.
   *
   *@return this
   */
  public WorldToScreenTransform getWorldToScreenTransform() { return this; }

  /**
   * Return the configuration of this panel as a string.  Originally intended
   * for bookmarking and recalling views.
   *
   *@return configuration as a string
   */
  public String       getConfig       ()           { 
    StringBuffer ar_sb = new StringBuffer();
    if (active_relationships.size() > 0) {
      ar_sb.append(Utils.encToURL(active_relationships.get(0)));
      for (int i=1;i<active_relationships.size();i++) ar_sb.append("," + Utils.encToURL(active_relationships.get(i)));
    }

    return "RTGraphPanel"    +                                           BundlesDT.DELIM +
           "nodesize="       + Utils.encToURL(nodeSize())              + BundlesDT.DELIM +
           "nodecolor="      + Utils.encToURL(nodeColor())             + BundlesDT.DELIM +
           "linksize="       + Utils.encToURL(linkSize())              + BundlesDT.DELIM +
           "linkcolor="      + Utils.encToURL(linkColor())             + BundlesDT.DELIM +
           "mltruncate="     + multilineTruncate()                     + BundlesDT.DELIM +
           "mlthreshold="    + multilineThreshold()                    + BundlesDT.DELIM +
           "linkcurves="     + Utils.encToURL("" + linkCurves())       + BundlesDT.DELIM +
           "linkcolorparts=" + Utils.encToURL("" + linkColorParts())   + BundlesDT.DELIM +
           "linktrans="      + Utils.encToURL("" + linksTransparent()) + BundlesDT.DELIM +
           "arrows="         + Utils.encToURL("" + drawArrows())       + BundlesDT.DELIM +
           "timing="         + Utils.encToURL("" + drawTiming())       + BundlesDT.DELIM +
           "dynlabels="      + Utils.encToURL("" + dynamicLabels())    + BundlesDT.DELIM +
           "nodelabels="     + Utils.encToURL("" + nodeLabels())       + BundlesDT.DELIM +
           "linklabels="     + Utils.encToURL("" + linkLabels())       + BundlesDT.DELIM +
           "edgetemplates="  + ((RTGraphComponent) getRTComponent()).drawEdgeTemplates() + BundlesDT.DELIM +
           "nodelegend="     + ((RTGraphComponent) getRTComponent()).drawNodeLegend()    + BundlesDT.DELIM +
           "nlabels="        + commaDelimited(nodeLabelsArray())       + BundlesDT.DELIM +
           "clabels="        + commaDelimited(colorLabelsArray())      + BundlesDT.DELIM +
           "llabels="        + commaDelimited(linkLabelsArray())       +
           (ar_sb.length() > 0 ? BundlesDT.DELIM + "relates=" + ar_sb.toString() : "");
  }

  /**
   * Encode an array of strings into a comma-delimited, url-encoded string.  Used for the getConfig() routine.
   */
  private String commaDelimited(String strs[]) {
    StringBuffer sb = new StringBuffer();
    if (strs.length > 0) {
      sb.append(Utils.encToURL(strs[0]));
      for (int i=1;i<strs.length;i++) sb.append("," + Utils.encToURL(strs[i]));
    }
    return sb.toString();
  }

  /**
   * Decode a comma-delimited, url-encoded string into an array of strings.  Used for the setConfig() routine.
   */
  private String[] commaDelimited(String str) {
    StringTokenizer st = new StringTokenizer(str,",");
    String strs[] = new String[st.countTokens()];
    for (int i=0;i<strs.length;i++) strs[i] = Utils.decFmURL(st.nextToken());
    return strs;
  }

  /**
   * Helper method to set all the correct strings in a JList.
   */
  private void setJList(JList<String> list, String strs[]) {
    List<Integer> indexes = new ArrayList<Integer>(); ListModel<String> lm = list.getModel();
    for (int i=0;i<strs.length;i++) { for (int j=0;j<lm.getSize();j++) if (strs[i].equals("" + lm.getElementAt(j))) indexes.add(j); }
    int index_array[] = new int[indexes.size()]; for (int i=0;i<index_array.length;i++) index_array[i] = indexes.get(i);
    list.setSelectedIndices(index_array);
  }

  /**
   * Set the configuration of this panel based on a previously returned configuration string.
   * Not implemented.
   *
   *@param str configuration string
   */
  public void         setConfig       (String str) { 
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTGraphPanel") == false) throw new RuntimeException("setConfig(" + str + ") - not a RTGraphPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";

      if      (type.equals("nodesize"))        nodeSize(Utils.decFmURL(value));
      else if (type.equals("nodecolor"))       nodeColor(Utils.decFmURL(value));
      else if (type.equals("linksize"))        linkSize(Utils.decFmURL(value));
      else if (type.equals("linkcolor"))       linkColor(Utils.decFmURL(value));
      else if (type.equals("mltruncate"))      multilineTruncate(Integer.parseInt(value));
      else if (type.equals("mlthreshold"))     multilineThreshold(Integer.parseInt(value));
      else if (type.equals("linkcurves"))      linkCurves(value.toLowerCase().equals("true"));
      else if (type.equals("linkcolorparts"))  linkColorParts(value.toLowerCase().equals("true"));
      else if (type.equals("linktrans"))       linksTransparent(value.toLowerCase().equals("true"));
      else if (type.equals("arrows"))          drawArrows(value.toLowerCase().equals("true"));
      else if (type.equals("timing"))          drawTiming(value.toLowerCase().equals("true"));
      else if (type.equals("strict"))          strictMatches(value.toLowerCase().equals("true"));
      else if (type.equals("dynlabels"))       dynamicLabels(value.toLowerCase().equals("true"));
      else if (type.equals("nodelabels"))      nodeLabels(value.toLowerCase().equals("true"));
      else if (type.equals("linklabels"))      linkLabels(value.toLowerCase().equals("true"));
      else if (type.equals("edgetemplates"))   ((RTGraphComponent) getRTComponent()).drawEdgeTemplates(value.toLowerCase().equals("true"));
      else if (type.equals("nodelegend"))      ((RTGraphComponent) getRTComponent()).drawNodeLegend   (value.toLowerCase().equals("true"));
      else if (type.equals("nlabels")) { if (!value.equals("")) setJList(entity_label_list, commaDelimited(value)); }
      else if (type.equals("clabels")) { if (!value.equals("")) setJList(entity_color_list, commaDelimited(value)); }
      else if (type.equals("llabels")) { if (!value.equals("")) setJList(bundle_label_list, commaDelimited(value)); }
      else if (type.equals("relates")) {
        if (!value.equals("")) {
          st2 = new StringTokenizer(value, ",");
          while (st2.hasMoreTokens()) active_relationships.add(Utils.decFmURL(st2.nextToken()));
        }
      } else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }

    // Apply the active_relationships
    newBundlesRoot(getRTParent().getRootBundles());
  }

  /**
   * Override to indicate that this component needs to save additional information.
   *
   *@return true
   */
  @Override
  public boolean hasAdditionalConfig() { return true; }

  /**
   * Add the additional configuration for this component so that the state can be restored.
   *
   *@param list         Additional configuration strings added here
   *@param visible_only only save information about the visible components -- important because of the need to
   *                    save the world positions of all the nodes
   */
  @Override
  public void addAdditionalConfig(List<String> list, boolean visible_only) {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; RTGraphComponent mycomp = (RTGraphComponent) getRTComponent();
    list.add("#AC extents|" + extents.getX() + "|" + extents.getY() + "|" + extents.getWidth() + "|" + extents.getHeight());
    // Save the retained nodes
    if (retained_nodes.size() > 0) {
      StringBuffer     sb = new StringBuffer();
      Iterator<String> it = retained_nodes.iterator();
      sb.append("#AC retain"); while (it.hasNext()) sb.append("|" + Utils.encToURL(it.next()));
      list.add(sb.toString());
    }
    // Save the sticky labels
    if (mycomp.sticky_labels != null && mycomp.sticky_labels.size() > 0) {
      StringBuffer sb = new StringBuffer(); 
      Iterator<String> it = mycomp.sticky_labels.iterator();
      sb.append("#AC sticky"); while (it.hasNext()) sb.append("|" + Utils.encToURL(it.next()));
      list.add(sb.toString());
    }
    // Pick out the right iterator -- if the visible one is available, use that
    Iterator<String> it;
    if (visible_only && myrc != null) it = myrc.entity_counter_context.binIterator();
    else                              it = entity_to_wxy.keySet().iterator();
    while (it.hasNext()) {
      String  entity = it.next(); Point2D point  = entity_to_wxy.get(entity);
      list.add("#AC wxy|" + Utils.encToURL(entity) + "|" + point.getX() + "|" + point.getY());
    }
    list.add("#AC graphend");
  }

  /**
   * Parse additional configuration information.
   */
  public int parseAdditionalConfig(List<String> lines, int line_i) {
    RTGraphComponent mycomp = (RTGraphComponent) getRTComponent();
    // Parse the extents
    if (lines.get(line_i).startsWith("#AC extents|")) {
      StringTokenizer st = new StringTokenizer(lines.get(line_i++),"|");
      st.nextToken();
      extents = new Rectangle2D.Double(Double.parseDouble(st.nextToken()),
                                       Double.parseDouble(st.nextToken()),
                                       Double.parseDouble(st.nextToken()),
                                       Double.parseDouble(st.nextToken()));
    }
    // Parse the retained nodes
    if (lines.get(line_i).startsWith("#AC retain|"))  {
      StringTokenizer st = new StringTokenizer(lines.get(line_i++),"|"); st.nextToken();
      Set<String> new_retained_nodes = new HashSet<String>();
      while (st.hasMoreTokens()) new_retained_nodes.add(Utils.decFmURL(st.nextToken()));
      if (retained_nodes != null) {
        retained_nodes.clear();
        retained_nodes.addAll(new_retained_nodes);
      } else retained_nodes = new_retained_nodes;
    }
    // Parse the sticky labels
    if (lines.get(line_i).startsWith("#AC sticky|")) {
      StringTokenizer st = new StringTokenizer(lines.get(line_i++),"|"); st.nextToken();
      Set<String> new_sticky_labels = new HashSet<String>();
      while (st.hasMoreTokens()) new_sticky_labels.add(Utils.decFmURL(st.nextToken()));
      if (mycomp.sticky_labels != null) {
        mycomp.sticky_labels.clear();
        mycomp.sticky_labels.addAll(new_sticky_labels);
      } else mycomp.sticky_labels = new_sticky_labels;
    }
    // Parse the coordinates
    while (lines.get(line_i).startsWith("#AC wxy|")) {
      StringTokenizer st = new StringTokenizer(lines.get(line_i++),"|"); 
      if (st.countTokens() == 4) {
        st.nextToken();
        String entity = Utils.decFmURL(st.nextToken());
        double x      = Double.parseDouble(st.nextToken()),
               y      = Double.parseDouble(st.nextToken());
        entity_to_wxy.put(entity, new Point2D.Double(x,y));
      } else System.err.println("Missing Token In LinkNode Config.  Line \"" + lines.get(line_i-1) + "\"");
    }
    if (lines.get(line_i).equals("#AC graphend")) line_i++;
    else throw new RuntimeException("Incorrect Ending For LinkNode Parser \"" + lines.get(line_i) + "\"");

    // Apply the settings
    newBundlesRoot(getRTParent().getRootBundles());
    transform();
    return line_i;
  }

  /**
   * Perform a geospatial layout of the nodes.
   *
   *@param cc_level if true, keep the nodes at a single position for each country (not implemented)
   */
  protected void geospatialLayout(boolean cc_level) {
    boolean one_trans = false, transform_available = true;
    saveLayoutForUndo(entity_to_wxy, null);
    Iterator<String> it = entity_to_wxy.keySet().iterator();
    while (it.hasNext()) {
      String       orig_entity = it.next(), entity = orig_entity;
      // Check for a concat
      if (entity.indexOf(BundlesDT.DELIM) >= 0) entity = entity.substring(entity.lastIndexOf(BundlesDT.DELIM)+1, entity.length());
      // Get the data type
      BundlesDT.DT datatype = BundlesDT.getEntityDataType(entity);
      // Attempt the transforms
      String lats[] = null, lons[] = null;
      try {
        if (transform_available) {
          lats = getRTParent().getRootBundles().getGlobals().transform(datatype, "latitude",  entity);
          lons = getRTParent().getRootBundles().getGlobals().transform(datatype, "longitude", entity);
        } else lats = lons = null;
      } catch (NullPointerException npe) { transform_available = false; lats = lons = null; }
      // Check for validity
      if (lats != null && lats.length > 0 && lats[0].equals(BundlesDT.NOTSET) == false &&
          lons != null && lons.length > 0 && lons[0].equals(BundlesDT.NOTSET) == false) {
        // System.err.println("Putting \"" + orig_entity + "\" (\"" + entity + "\") @ " + lats[0] + "," + lons[0]);
        entity_to_wxy.put(orig_entity, new Point2D.Double(-Double.parseDouble(lons[0]), -Double.parseDouble(lats[0])));
        transform(orig_entity); one_trans = true;
      } else if (GeoData.getInstance().geoDataAvailable(datatype)) {
        Point2D pt = GeoData.getInstance().geoLocate(datatype, entity);
        if (pt != null) {
          entity_to_wxy.put(orig_entity, new Point2D.Double(pt.getX(), -pt.getY()));
          transform(orig_entity); one_trans = true;
        }
      }
    }
    if (one_trans) getRTComponent().render();
  }

  /**
   * Submenu for the common edge relationships.
   */
  JMenu common_relationships_menu;

  /**
   * Adds common relationship types to the common_relationships_menu including recently used relationships.
   * These relationships describe the links (or edges) in the link-node graph.
   */
  protected void fillCommonRelationshipsMenu() {
    common_relationships_menu.removeAll();
    // -- Common relationships
    JMenuItem mi;
    BundlesG  globals = getRTParent().getRootBundles().getGlobals();
    //
    //
    //
    if (globals.fieldIndex("sip") != -1 && globals.fieldIndex("dip") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("sip => dip"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("sip",  Utils.SQUARE_STR,   false, "dip",  Utils.SQUARE_STR,   false, STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("srcip") != -1 && globals.fieldIndex("dstip") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("srcip => dstip"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("srcip",  Utils.SQUARE_STR,   false, "dstip",  Utils.SQUARE_STR,   false, STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("sip") != -1 && globals.fieldIndex("dip") != -1 && globals.fieldIndex("dpt") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("sip => dpt => dip"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("sip",  Utils.SQUARE_STR, false, "dpt",  Utils.CIRCLE_STR, true,  STYLE_SOLID_STR, true, true);
        addRelationship("dpt",  Utils.CIRCLE_STR, true,  "dip",  Utils.SQUARE_STR, false, STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("sip") != -1 && globals.fieldIndex("dip") != -1 && globals.fieldIndex("dpt") != -1 && globals.fieldIndex("spt") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("sip => spt => dpt => dip"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("sip",  Utils.SQUARE_STR, false, "spt",  Utils.CIRCLE_STR,   true,  STYLE_SOLID_STR, true, true);
        addRelationship("spt",  Utils.CIRCLE_STR, true,  "dpt",  Utils.CIRCLE_STR,   true,  STYLE_SOLID_STR, true, true);
        addRelationship("dpt",  Utils.CIRCLE_STR, true,  "dip",  Utils.SQUARE_STR,   false, STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("DBYT") != -1 && globals.fieldIndex("SBYT") != -1 && globals.fieldIndex("dpt") != -1 && globals.fieldIndex("spt") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("DBYT => dpt => SBYT"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("DBYT", Utils.DIAMOND_STR, true, "dpt",  Utils.CIRCLE_STR,  true,  STYLE_SOLID_STR, true, true);
        addRelationship("dpt",  Utils.CIRCLE_STR,  true, "SBYT", Utils.DIAMOND_STR, true,  STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("DOCT") != -1 && globals.fieldIndex("SOCT") != -1 && globals.fieldIndex("dpt") != -1 && globals.fieldIndex("spt") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("DOCT => dpt => SOCT"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("DOCT", Utils.DIAMOND_STR, true, "dpt",  Utils.CIRCLE_STR,  true,  STYLE_SOLID_STR, true, true);
        addRelationship("dpt",  Utils.CIRCLE_STR,  true, "SOCT", Utils.DIAMOND_STR, true,  STYLE_SOLID_STR, true, true); } } );
    }
    //
    //
    //
    if (globals.fieldIndex("domain") != -1 && globals.fieldIndex("ip") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("domain => ip"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("domain", Utils.TRIANGLE_STR, false,  "ip",  Utils.SQUARE_STR, false,  STYLE_SOLID_STR, true, true); } } );
    }

    //
    // JSON Tree Utility
    //
    if (globals.fieldIndex("parent") != -1 && globals.fieldIndex("child") != -1) {
      if (globals.fieldIndex("arrayelement") != -1) {
        common_relationships_menu.add(mi = new JMenuItem("json tree file"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          addRelationship("parent", Utils.CIRCLE_STR, false,  "arrayelement",  Utils.SQUARE_STR, false,  STYLE_SOLID_STR, true, true);
          addRelationship("parent", Utils.CIRCLE_STR, false,  "child",         Utils.CIRCLE_STR, false,  STYLE_SOLID_STR, true, true); } } );
      } else {
        common_relationships_menu.add(mi = new JMenuItem("json tree file"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          addRelationship("parent", Utils.CIRCLE_STR, false,  "child", Utils.CIRCLE_STR, false,  STYLE_SOLID_STR, true, true); } } );
      }
    }

    //
    // JSON Tree Utility (Local Names)
    //
    if (globals.fieldIndex("parent_local") != -1 && globals.fieldIndex("child_local") != -1) {
      common_relationships_menu.add(mi = new JMenuItem("json tree file (local)"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        addRelationship("parent_local", Utils.CIRCLE_STR, false,  "child_local", Utils.CIRCLE_STR, false,  STYLE_SOLID_STR, true, true); } } );
    }

    // -- Recent Relationships
    String recents[] = RTPrefs.retrieveStrings(RECENT_RELATIONSHIPS_PREF_STR);
    if (recents != null && recents.length > 0) {
      // Prepare the lookup...
      recent_relationships_lu = new HashMap<String,String>();
      // and the menu...
      common_relationships_menu.addSeparator();
      // and the blanks list...
      Set<String> blanks_set = new HashSet<String>();
      String blanks[] = nodeBlanks(getRTParent().getRootBundles().getGlobals());
      for (int i=0;i<blanks.length;i++) blanks_set.add(blanks[i]);
      // Go through the recents...
      for (int i=0;i<recents.length;i++) {
        // Extract the relationships
        StringTokenizer st = new StringTokenizer(recents[i], BundlesDT.DELIM);
        String delimited        = Utils.decFmURL(st.nextToken());
        // Get the header elements -- make sure they exist in the current data set
        String fm_hdr  = relationshipFromHeader(delimited),
               to_hdr  = relationshipToHeader(delimited);
        if (blanks_set.contains(fm_hdr) == false || blanks_set.contains(to_hdr) == false) continue; 
        // Change them to a human representation and add them to the lookups
        String recents_in_human = fm_hdr + " => " + to_hdr;
        recent_relationships_lu.put(recents_in_human, delimited);
        common_relationships_menu.add(mi = new JMenuItem(recents_in_human));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          String  cmd      = ae.getActionCommand(); cmd = recent_relationships_lu.get(cmd);
          // System.err.println("Command = \"" + cmd + "\"");
          String  fm_hdr   = relationshipFromHeader(cmd), fm_ico   = relationshipFromIcon(cmd); boolean fm_typed  = relationshipFromTyped(cmd);
          String  to_hdr   = relationshipToHeader(cmd),   to_ico   = relationshipToIcon(cmd);   boolean to_typed  = relationshipToTyped(cmd);
          String  style    = relationshipStyle(cmd);                                            boolean ignore_ns = relationshipIgnoreNotSet(cmd);
          addRelationship(fm_hdr, fm_ico, fm_typed, to_hdr, to_ico, to_typed, style, ignore_ns, false);
        } } );
      }
      // Add an option to clear out the recent relationships
      common_relationships_menu.addSeparator();
      common_relationships_menu.add(mi = new JMenuItem("Clear Relationships"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        RTPrefs.store(RECENT_RELATIONSHIPS_PREF_STR, new String[0]);
      } } );
    }
  }

  /**
   * Lookup table for converting the menu strings into the edge relationship strings
   */
  Map<String,String> recent_relationships_lu = new HashMap<String,String>();

  /**
   * Enumeration for the background options
   */
  enum GraphBG { NONE, GEO_OUT, GEO_FILL, GEO_TOUCH, KCORES };

  /**
   * Determine which radio button is selected and return the appropriate enum.
   *
   *@return background enumeration
   */
  public GraphBG getGraphBG() {
    if      (nobg_rbmi.isSelected())      return GraphBG.NONE;
    else if (geo_out_rbmi.isSelected())   return GraphBG.GEO_OUT;
    else if (geo_fill_rbmi.isSelected())  return GraphBG.GEO_FILL;
    else if (geo_touch_rbmi.isSelected()) return GraphBG.GEO_TOUCH;
    else if (kcores_rbmi.isSelected())    return GraphBG.KCORES;
    else                                  return GraphBG.NONE;
  }

  /**
   * For an array of radio buttons, determine which is selected and return that as a string.
   *
   *@param  items radio button list
   *
   *@return selected radio button label
   */
  public String findSelected(JRadioButtonMenuItem items[]) {
    for (int i=0;i<items.length;i++) if (items[i].isSelected()) return items[i].getText();
    return items[0].getText();
  }

  /**
   * For an array of radio buttons, determine which matches the specified string and set it as selected.
   *
   *@param items radio button list
   *@param str   text of button to set as selected
   */
  public void setSelected(JRadioButtonMenuItem items[], String str) {
    for (int i=0;i<items.length;i++) if (items[i].getText().equals(str)) items[i].setSelected(true);
  }

  /**
   * Enumeration for the node sizes
   * 
   */
  enum NodeSize { INVISIBLE, SMALL, LARGE, VARY, VARY_LOG, TYPE, GRAPHINFO, CLUSTERCO, LABEL, 
                  SM_MONTH, SM_DOW, SM_HOUR, SM_STRAIGHT_SMALL, SM_STRAIGHT_LARGE,
                  SM_PIE_SMALL, SM_PIE_LARGE, 
                  SM_XY_VISIBLE, SM_XY_VISIBLE_EQUAL, SM_XY_LOCAL, SM_XY_LOCAL_EQUAL };

  /**
   * Return the enumeration of the node size based on the selected radio
   * button.
   *
   *@return node size enumeration
   */
  public NodeSize  getNodeSize() {
    String str = findSelected(node_sizes);
     if       (str.equals(NODE_SZ_INVISIBLE))                  { return NodeSize.INVISIBLE;
    } else if (str.equals(NODE_SZ_SMALL))                      { return NodeSize.SMALL;
    } else if (str.equals(NODE_SZ_LARGE))                      { return NodeSize.LARGE;
    } else if (str.equals(NODE_SZ_VARY))                       { return NodeSize.VARY; 
    } else if (str.equals(NODE_SZ_VARY_LOG))                   { return NodeSize.VARY_LOG;
    } else if (str.equals(NODE_SZ_TYPE))                       { return NodeSize.TYPE;
    } else if (str.equals(NODE_SZ_GRAPHINFO))                  { return NodeSize.GRAPHINFO;
    } else if (str.equals(NODE_SZ_CLUSTERCO))                  { return NodeSize.CLUSTERCO;
    } else if (str.equals(NODE_SZ_LABEL))                      { return NodeSize.LABEL;
    } else if (str.equals(KeyMaker.BY_MONTH_STR))              { return NodeSize.SM_MONTH;
    } else if (str.equals(KeyMaker.BY_DAYOFWEEK_STR))          { return NodeSize.SM_DOW;
    } else if (str.equals(KeyMaker.BY_HOUR_STR))               { return NodeSize.SM_HOUR;
    } else if (str.equals(KeyMaker.BY_STRAIGHT_STR + " (Sm)")) { return NodeSize.SM_STRAIGHT_SMALL;
    } else if (str.equals(KeyMaker.BY_STRAIGHT_STR + " (Lg)")) { return NodeSize.SM_STRAIGHT_LARGE;
    } else if (str.equals(NODE_SZ_SM_PIE_SMALL))               { return NodeSize.SM_PIE_SMALL;
    } else if (str.equals(NODE_SZ_SM_PIE_LARGE))               { return NodeSize.SM_PIE_LARGE;
    } else if (str.equals(NODE_SZ_XY_VISIBLE))                 { return NodeSize.SM_XY_VISIBLE;
    } else if (str.equals(NODE_SZ_XY_VISIBLE_EQUAL))           { return NodeSize.SM_XY_VISIBLE_EQUAL;
    } else if (str.equals(NODE_SZ_XY_LOCAL))                   { return NodeSize.SM_XY_LOCAL;
    } else if (str.equals(NODE_SZ_XY_LOCAL_EQUAL))             { return NodeSize.SM_XY_LOCAL_EQUAL;
    } else                                                       return NodeSize.LARGE;
  }

  /**
   * Return the node size setting.
   *
   *@return node size setting as a string
   */
  public String nodeSize()           { return findSelected(node_sizes); }

  /**
   * Set the node size setting.
   *
   *@param str node size setting as a string
   */
  public void   nodeSize(String str) { setSelected(node_sizes, str); }

  /**
   * Enumeration for the node color
   */
  enum NodeColor { WHITE, VARY, LABEL, CLUSTERCO, DATATYPE };

  /**
   * Return the node color enumeration based on the radio button
   * status for the node color options.
   *
   *@return node color enumeration
   */
  public NodeColor getNodeColor() {
    String str = findSelected(node_colors);
    if        (str.equals(NODE_CO_WHITE))          { return NodeColor.WHITE;
    } else if (str.equals(NODE_CO_VARY))           { return NodeColor.VARY;
    } else if (str.equals(NODE_CO_LABEL))          { return NodeColor.LABEL;
    } else if (str.equals(NODE_CO_CLUSTERCO))      { return NodeColor.CLUSTERCO;
    } else if (str.equals(NODE_CO_DATATYPE))       { return NodeColor.DATATYPE;
    } else return NodeColor.WHITE;
  }

  /**
   * Return the node color setting as a string.
   *
   *@return node color as a string
   */
  public String nodeColor() { return findSelected(node_colors); }

  /**
   * Set the node color setting.
   *
   *@param str node color setting as a string
   */
  public void nodeColor(String str) { setSelected(node_colors, str); }

  /**
   * Return the flag indicating if node labels should be drawn.
   *
   *@return node labels flag
   */
  public boolean nodeLabels() { return node_labels_cb.isSelected(); }

  /**
   * Set the flag indicating that node labels should / should not be drawn.
   *
   *@param b new node label flag setting
   */
  public void nodeLabels(boolean b) { node_labels_cb.setSelected(b); }

  /**
   * Return the selected entries in the node labels list.
   *
   *@return array of strings that are selected
   */
  public String[] nodeLabelsArray() {
    List<String> list   = Utils.jListGetValuesWrapper(entity_label_list);
    String       strs[] = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i);
    return strs;
  }

  /**
   * Return the selected entries in the node colors list.
   *
   *@return array of strings that are selected
   */
  public String[] colorLabelsArray() {
    List<String> list   = Utils.jListGetValuesWrapper(entity_color_list);
    String       strs[] = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i);
    return strs;
  }

  /**
   * Return the selected entries in the link labels list.
   *
   *@return array of strings that are selected
   */
  public String[] linkLabelsArray() {
    List<String> list   = Utils.jListGetValuesWrapper(bundle_label_list);
    String       strs[] = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i);
    return strs;
  }

  /**
   * Link color enumeration
   */
  enum LinkColor {  GRAY, VARY, WHITE, DARKGRAY, NODE_TYPES };

  /**
   * Return the link color enumeration based on the link color
   * radio button.
   *
   *@return link color enumeration
   */
  public LinkColor getLinkColor() {
    String str = findSelected(link_colors);
    if        (str.equals(LINK_CO_GRAY))       { return LinkColor.GRAY;
    } else if (str.equals(LINK_CO_WHITE))      { return LinkColor.WHITE;
    } else if (str.equals(LINK_CO_DARKGRAY))   { return LinkColor.DARKGRAY;
    } else if (str.equals(LINK_CO_VARY))       { return LinkColor.VARY;
    } else if (str.equals(LINK_CO_NODE_TYPES)) { return LinkColor.NODE_TYPES;
    } else return LinkColor.GRAY;
  }

  /**
   * Return the link color setting as a string.
   *
   *@return link color as a string
   */
  public String linkColor() { return findSelected(link_colors); }

  /**
   * Set the link color setting.
   *
   *@param str link color setting as a string
   */
  public void linkColor(String str) { setSelected(link_colors, str); }

  /**
   * Enumeration for the link sizes
   */
  enum LinkSize { INVISIBLE, THIN, NORMAL, THICK, VARY, CONDUCT, CLUSTERP };

  /**
   * Return the link size enumeration based on the link size radio buttons
   *
   *@return link size enumeration
   */
  public LinkSize getLinkSize() {
    String str = findSelected(link_sizes);
    if        (str.equals(LINK_SZ_INVISIBLE)) { return LinkSize.INVISIBLE;
    } else if (str.equals(LINK_SZ_THIN))      { return LinkSize.THIN;
    } else if (str.equals(LINK_SZ_NORMAL))    { return LinkSize.NORMAL;
    } else if (str.equals(LINK_SZ_THICK))     { return LinkSize.THICK;
    } else if (str.equals(LINK_SZ_VARY))      { return LinkSize.VARY;
    } else if (str.equals(LINK_SZ_CONDUCT))   { return LinkSize.CONDUCT;
    } else if (str.equals(LINK_SZ_CLUSTERP))  { return LinkSize.CLUSTERP;
    } else return LinkSize.NORMAL;
  }

  /**
   * Return the link size as a string.
   *
   *@return link size as string
   */
  public String linkSize() { return findSelected(link_sizes); }

  /**
   * Set the link size setting.
   *
   *@param str link size setting as string
   */
  public void   linkSize(String str) { setSelected(link_sizes, str); }

  /**
   * Return the flag indicating if link labels should be drawn.
   *
   *@return link labels flag
   */
  public boolean linkLabels() { return link_labels_cb.isSelected(); }

  /**
   * Set the flag to indicate link labels should be drawn.
   *
   *@param b new setting for drawing link labels
   */
  public void linkLabels(boolean b) { link_labels_cb.setSelected(b); }

  /**
   * Return true if link curves is enabled.
   *
   *@return true for link curves
   */
  public boolean           linkCurves() { return link_curves_cbmi.isSelected(); }

  /**
   * Set the link curves option.
   *
   *@param b new link curves setting
   */
  public void              linkCurves(boolean b) { link_curves_cbmi.setSelected(b); }

  /**
   *  Return true if the link color parts checkbox is selected.
   *
   *@return true if selected
   */
  public boolean linkColorParts() { return link_color_parts_cbmi.isSelected(); }

  /**
   * Set the link color parts selected checkbox.
   *
   *@param b value to set it to
   */
  public void linkColorParts(boolean b) { link_color_parts_cbmi.setSelected(b); }

  /**
   * Return true if link transparency is enabled.
   *
   *@return true for link transparency
   */
  public boolean           linksTransparent() { return link_trans_cbmi.isSelected();  }

  /**
   * Set the link transparency option.
   *
   *@param b new link transparency setting
   */
  public void              linksTransparent(boolean b) { link_trans_cbmi.setSelected(b); }

  /**
   * Return true if link arrows are enabled.
   *
   *@return true for link arrows
   */
  public boolean           drawArrows()       { return arrows_cbmi.isSelected();      }

  /**
   * Set the draw arrows option.
   *
   *@param b new draw arrows flag
   */
  public void              drawArrows(boolean b) { arrows_cbmi.setSelected(b); }

  /**
   * Return true if crossings should be calculated.
   */
  public boolean           calculateCrossings() { return calculate_crossings_cbmi.isSelected(); }

  /**
   * Return the flag indicating if dynamic labeling is enabled.  Dynamic labeling
   * shows the node under the mouse as well as the neighbors of that node (if they
   * are visible).
   *
   *@return true if dynamic labels should be shown under the mouse
   */
  public boolean           dynamicLabels()    { return dyn_label_cbmi.isSelected(); }

  /**
   * Set the dynamic labeling option.
   *
   *@param b new dynamic labeling setting
   */
  public void              dynamicLabels(boolean b) { dyn_label_cbmi.setSelected(b); }

  /**
   * Return true if timing marks are enabled.
   *
   *@return true for timing marks
   */
  public boolean           drawTiming()       { return timing_cbmi.isSelected();      }

  /**
   * Set the option to draw timing marks on the links.
   *
   *@param b new timing marks setting
   */
  public void              drawTiming(boolean b) { timing_cbmi.setSelected(b); }

  /**
   * Return true if node label rendering is enabled
   *
   *@return true for node labels
   */
  public boolean           drawNodeLabels()       { return node_labels_cb.isSelected();        }

  /**
   * Return true if link labeling is enabled
   *
   *@return true for link labels
   */
  public boolean           drawLinkLabels()       { return link_labels_cb.isSelected();        }

  /**
   * Toggle labeling drawing.
   */
  public void              toggleLabels()     { node_labels_cb.setSelected(!node_labels_cb.isSelected()); }

  /**
   * Return a list of the labels that are selected for viewing related to the bundles (links).
   *
   *@return list of labels for the links
   */
  public java.util.List<String> listBundleLabels() { return Utils.jListGetValuesWrapper(bundle_label_list); }

  /** 
   * Return a list of the labels that are selected for viewing related to entities (nodes).
   *
   *@return list of labels for nodes
   */
  public java.util.List<String> listEntityLabels() { return Utils.jListGetValuesWrapper(entity_label_list); }

  /** 
   * Return a list of the labels for coloring the nodes.
   *
   *@return list of labels for node coloring
   */
  public java.util.List<String> listEntityColor() { return Utils.jListGetValuesWrapper(entity_color_list); }

  /**
   * Update the labels menu based on the various fields in application.
   */
  @Override
  public void updateBys() { updateLabelLists(getRTParent().getEntityTagTypes()); fillCommonRelationshipsMenu(); }

  /**
   * Update the tag types for the labeling menu so that individual tag types can be used
   * for labeling and coloring.
   */
  @Override
  public void updateEntityTagTypes(Set<String> types) { updateLabelLists(types); fillCommonRelationshipsMenu(); }

  /**
   * Upon new bundles (records) being added to the application, ensure that those
   * links (edges) are in the graph and have the corresponding bundle-to-edge mapping.
   *
   *@param set new bundles
   */
  @Override
  public void newBundlesAdded(Set<Bundle> set) {
// System.err.println("newBundlesAdded(set.size = " + set.size() + ")");
    Bundles as_bundles = getRTParent().getRootBundles().subset(set);
    for (int i=0;i<active_relationships.size();i++) {
// System.err.println("  newBundlesAdded():  Adding In Relationship " + i + " \"" + active_relationships.get(i) + "\"...  bundles.size() = " + as_bundles.size());
      addRelationship(active_relationships.get(i), false, as_bundles);
    }
  }

  /**
   * Set a new root bundle set.  This is used at the application level to
   * permanently remove unwanted data so that relevant/verified data can
   * be better examinined.  This method is complicated because the state
   * of the component has to be managed (most other components are stateless).
   *
   *@param new_root new root bundle set
   */
  @Override
  public void newBundlesRoot(Bundles new_root) {
    // Adjust state of super
    super.newBundlesRoot(new_root);
    // Save some state
    Map<String,Point2D> wxy_copy = entity_to_wxy;
    // Reset stateful variables for this class
    digraph = new SimpleMyGraph<Bundle>();
    graph   = new SimpleMyGraph<Bundle>();
    entity_to_shape = new HashMap<String,Utils.Symbol>();
    entity_to_wxy   = new HashMap<String,Point2D>();
    entity_to_sxy   = new HashMap<String,String>();
    entity_to_sx    = new HashMap<String,Integer>();
    entity_to_sy    = new HashMap<String,Integer>();
    // Reapply all the relationships
    for (int i=0;i<active_relationships.size();i++) {
      // System.err.println("newBundlesRoot():  Adding In Relationship \"" + active_relationships.get(i) + "\"...  bundles.size() = " + new_root.size());
      addRelationship(active_relationships.get(i), false, new_root);
    }
    // Reapply the world coordinates
    Iterator<String> it = entity_to_wxy.keySet().iterator();
    while (it.hasNext()) {
      String entity = it.next();
      if (wxy_copy.containsKey(entity)) entity_to_wxy.put(entity, wxy_copy.get(entity));
    }
    transform();
  }

  /**
   * Indicate that entities exist in the view and can be selected for comments.
   *
   *@return true
   */
  public boolean supportsEntityComments() { return true; }

  /**
   *
   */
  Set<String> retained_nodes = new HashSet<String>();

  /**
   * Reform the graph to only have the visible nodes present.  Destructive to the graph... (experimental)
   */
  public void retainOnlyVisibleNodes() {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;
    Set<String> new_retained_nodes = new HashSet<String>();
    Iterator<String> it = myrc.entity_counter_context.binIterator();
    while (it.hasNext()) new_retained_nodes.add(it.next());
    retained_nodes = new_retained_nodes;
    newBundlesRoot(getRTParent().getRootBundles());
  }

  /**
   * Add the application selection (usually the same linknode window) to the retained nodes set.  Re-create the graph
   * with the new settings.
   */
  public void addSelectionToRetained() {
    retained_nodes.addAll(getRTParent().getSelectedEntities());
    newBundlesRoot(getRTParent().getRootBundles());
  }

  /**
   * Clear the retained node set, basically re-adding all the nodes to the graph.
   */
  public void clearRetained() {
    retained_nodes.clear();
    newBundlesRoot(getRTParent().getRootBundles());
  }

  /**
   * For the edges that are visible, add all of the bundles (records) that relate to those edges.
   */
  public void addAllOnVisibleLinks() {
    // Check for a valid render context and create the new set
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;
    Set<Bundle> set = new HashSet<Bundle>();
    // Go through visible links and find the related bundles
    Iterator<String> it = myrc.graphedgeref_to_link.keySet().iterator();
    while (it.hasNext()) {
      String graphedgeref = it.next();
      Iterator<Bundle> itb = digraph.linkRefIterator(graphedgeref);
      while (itb.hasNext()) set.add(itb.next());
    }
    // Add the no mapping set and push it to the RTParent
    set.addAll(getRTComponent().getNoMappingSet()); 
    Bundles new_bundles = getRTParent().getRootBundles().subset(set);
    new_bundles.setSliceDescription("{\"operation\":\"addAllOnVisibleLink\",\"RTPanel\":\"" + getWinType() + "\"}");
    getRTParent().push(new_bundles);
  }

  /**
   * Filter the visible records to only those links/edges that have both nodes as sources.
   */
  public void filterToBidirectionalLinks() {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;

    // Get the visible links, extract the direction, create the reverse direction -- if that exists, add those records to keepers
    Iterator<String> it = myrc.graphedgeref_to_link.keySet().iterator(); Set<Bundle> to_keep = new HashSet<Bundle>();
    while (it.hasNext()) {
      String graph_edge_ref = it.next(); String line_ref = myrc.graphedgeref_to_link.get(graph_edge_ref);
      int fm_i = digraph.linkRefFm(graph_edge_ref);
      int to_i = digraph.linkRefTo(graph_edge_ref);
      String other_dir = digraph.getLinkRef(to_i,fm_i);
      if (myrc.graphedgeref_to_link.containsKey(other_dir)) { to_keep.addAll(myrc.link_counter_context.getBundles(line_ref)); }
    }

    // Add the no mapping set and push it to the RTParent
    to_keep.addAll(getRTComponent().getNoMappingSet()); 
    Bundles new_bundles = getRTParent().getRootBundles().subset(to_keep);
    new_bundles.setSliceDescription("{\"operation\":\"filterToBidirectionalLinks\",\"RTPanel\":\"" + getWinType() + "\"}");
    getRTParent().push(new_bundles);
  }

  /**
   * Expand the graph (non-time specific) by one hop.  If the digraph option is set, only
   * expand using the directional graph.
   *
   *@param use_digraph use directed edges only if true
   */
  public void makeOneHopsVisible(boolean use_digraph) {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;

    SimpleMyGraph<Bundle> g = use_digraph ? digraph : graph; // which graph to use

    Set<Bundle> set = new HashSet<Bundle>();                 // resulting bundles to keep
    set.addAll(getRTParent().getVisibleBundles().bundleSet());   // add all of the current ones

    // Get the visible links, extract the nodes
    Iterator<String> it = myrc.graphedgeref_to_link.keySet().iterator(); Set<Integer> nodes = new HashSet<Integer>();
    while (it.hasNext()) {
      String linkref = it.next();
      nodes.add(g.linkRefFm(linkref));
      nodes.add(g.linkRefTo(linkref));
    }
    Set<Integer> already_visible = new HashSet<Integer>(); already_visible.addAll(nodes);

    // If there's a selection, limit to the selected nodes
    Set<String> sel = getRTParent().getSelectedEntities(); if (sel != null && sel.size() > 0) {
      Set<Integer> sel_as_ints = new HashSet<Integer>();
      it = sel.iterator(); while (it.hasNext()) {
        String node = it.next(); int node_i = g.getEntityIndex(node);
        sel_as_ints.add(node_i);
      }
      nodes.retainAll(sel_as_ints);
    }
    if (nodes.size() == 0) return;

    // Go through the nodes, find all their edges, and re-add the bundles to the set
    Iterator<Integer> iti = nodes.iterator(); Set<String> new_nbors = new HashSet<String>();
    while (iti.hasNext()) {
      int node_i = iti.next();
      for (int i=0;i<g.getNumberOfNeighbors(node_i);i++) {
        int    nbor_i  = g.getNeighbor(node_i,i);
        String nbor    = g.getEntityDescription(nbor_i); 
        String linkref = g.getLinkRef(node_i,nbor_i);

        // When expanding from a selection, add the newly visible nodes to the selection
        if (sel != null && sel.size() > 0 && already_visible.contains(nbor_i) == false) new_nbors.add(nbor);

        // Add the associated bundles
        Iterator<Bundle> itb = g.linkRefIterator(linkref);
        while (itb.hasNext()) set.add(itb.next());
      }
    }

    // If there was a selection, add the expanded nodes to the selection
    if (sel != null && sel.size() > 0 && new_nbors.size() > 0) { sel.addAll(new_nbors); getRTParent().setSelectedEntities(sel); }

    // Add the no mapping set and push it to the RTParent
    set.addAll(getRTComponent().getNoMappingSet()); 
    Bundles new_bundles = getRTParent().getRootBundles().subset(set);
    new_bundles.setSliceDescription("{\"operation\":\"makeOneHopsVisible\",\"RTPanel\":\"" + getWinType() + "\",\"use_digraph\":\"" + use_digraph + "\"}");
    getRTParent().push(new_bundles);
  }

  /**
   * Return true if the selected entities should also apply to report titles (and the corresponding extraction
   * of entities from the report text).  True in the logical sense means that the cbmi is selected and the
   * graph actually has a report title as a relationship.
   *
   *@return true means to select entities from selected reports
   */
  public boolean reportEntities() {
    return report_select_entities_cbmi.isSelected() && relationshipsIncludeReportTitles();
  }

  /**
   * Return true if report title is in the current relationships.
   *
   *@return true if nodes can be report titles
   */
  public boolean relationshipsIncludeReportTitles() {
    for (int i=0;i<active_relationships.size();i++) {
      if (active_relationships.get(i).indexOf(BundlesUtils.REPORT_TITLE_STR) >= 0) return true;
    }
    return false;
  }

  /**
   * Apply the tags to the selected entities.
   *
   *@param tags tags to apply
   */
  public void tagSelectedEntities(String tags) {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().rc); if (myrc == null) return;
    if (tags != null && tags.equals("") == false) {
      getRTParent().setSelectedEntities(myrc.filterEntities(getRTParent().getSelectedEntities()), reportEntities());
      getRTParent().tagSelectedEntities(tags, myrc.bs.ts0(), myrc.bs.ts1());
    }
  }

  /**
   * Clear out the tags for the selected entities.
   */
  public void clearEntityTagsForSelectedEntities() { getRTParent().clearEntityTagsForSelectedEntities(); }

  /**
   * Replace the specified tag with the user supplied one from the selected entities.
   */
  public void replaceEntityTagForSelectedEntities() { getRTParent().replaceEntityTagForSelectedEntities(tag_tf.getText()); }

  /**
   * Remove the user specified tag from the selected entities.
   */
  public void removeEntityTagForSelectedEntities() { getRTParent().removeEntityTagForSelectedEntities(tag_tf.getText()); }

  /**
   * Refactored into the Utils.selection() class...
   *
   *@param selection pattern for string selection
   */
  public void select(String selection) {
    // Get the render context... contains info about what nodes haven't been filtered out
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc; if (myrc == null) return;

    // Get the no operation selection ... have to do direct matches first for the three parts... then apply the set operation...
    String noop_selection = selection;
    if (Selection.selectionContainsSetOperation(selection)) noop_selection = Selection.removeSetOperation(selection);

    // Apply selection to visible (visible means it had at least one visible record in the current stack)
    Set<String> direct_set = new HashSet<String>();;
    if (Selection.isTagRelated(noop_selection) == false) direct_set = Selection.select(myrc.visible_entities, noop_selection);

    // Re-run with other labels ... not perfect do to grouped entities... those will all have the same labeling
    Set<String> label_set  = new HashSet<String>();
    if (myrc.node_lm != null && Selection.isTagRelated(noop_selection) == false) {
      // Need a lookup from the node key back to the entities within that node (filtered by visible nodes)
      Map<String,Set<String>> node_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it_entity = entity_to_sxy.keySet().iterator(); while (it_entity.hasNext()) {
        String entity = it_entity.next(); if (myrc.entity_counter_context.hasBin(entity)) {
          String node = entity_to_sxy.get(entity);
          if (node_to_entities.containsKey(node) == false) node_to_entities.put(node, new HashSet<String>());
          node_to_entities.get(node).add(entity);
        }
      }

      // Now construct the labels shown
      Map<String,Set<String>> label_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it = myrc.node_counter_context.binIterator(); while (it.hasNext()) {
        String node = it.next();
        String values[] = myrc.node_lm.toStrings(node); if (values != null) {
          for (int i=0;i<values.length;i++) { 
            if (label_to_entities.containsKey(values[i]) == false) label_to_entities.put(values[i], new HashSet<String>());
            label_to_entities.get(values[i]).addAll(node_to_entities.get(node));
          }
        }
      }
      Set<String> label_set_matches = Selection.select(label_to_entities.keySet(), noop_selection);
      it = label_set_matches.iterator(); while (it.hasNext()) label_set.addAll(label_to_entities.get(it.next()));
    }

    // Re-run with tags ... if the selection is tag related
    Set<String> tag_set = new HashSet<String>();
    if (Selection.isTagRelated(noop_selection)) {
      Map<String,Set<String>> tag_to_entities = new HashMap<String,Set<String>>();
      Iterator<String> it = myrc.entity_counter_context.binIterator(); while (it.hasNext()) { String entity = it.next();
        Set<String> tags = getRTParent().getEntityTags(entity, getRTParent().getRootBundles().ts0(), getRTParent().getRootBundles().ts1());
        Iterator<String> it_tag = tags.iterator(); while (it_tag.hasNext()) {
          String tag = it_tag.next();
          if (tag_to_entities.containsKey(tag) == false) tag_to_entities.put(tag, new HashSet<String>());
          tag_to_entities.get(tag).add(entity);
        }
      }
      Set<String> tag_set_matches = Selection.select(tag_to_entities.keySet(), noop_selection);
      it = tag_set_matches.iterator(); while (it.hasNext()) tag_set.addAll(tag_to_entities.get(it.next()));
    }

    // Union of the three
    Set<String> union = new HashSet<String>(); union.addAll(direct_set); union.addAll(label_set); union.addAll(tag_set);

    // Get the already selected entities for set-based operations
    Set<String> already_selected_entities = myrc.filterEntities(getRTParent().getSelectedEntities()); // Filter selection to what's visible in this view
    Set<String> finalized_set;
    if (noop_selection.equals(selection) == false) {
      finalized_set = Selection.applySetOperation(selection, union, already_selected_entities, myrc.visible_entities);
    } else { finalized_set = union; }

    // Apply the selection methodology and set application-wide
    getRTParent().setSelectedEntities(finalized_set); 
  }

  /**
   * Create panel for handling labeling.
   *
   *@param  entity_tag_types types from type-value tags
   *
   *@return gui panel for labeling
   */
  private JPanel createLabelsPanel(Set<String> entity_tag_types) {
    JPanel panel  = new JPanel(new BorderLayout());
     JPanel labels_panel = new JPanel(new GridLayout(1,3,4,4));
      labels_panel.add(new JLabel("Labels"));
      labels_panel.add(node_labels_cb = new JCheckBox("Node", false)); defaultListener(node_labels_cb);
      labels_panel.add(link_labels_cb = new JCheckBox("Link", false)); defaultListener(link_labels_cb);
      panel.add("North", labels_panel);
    JPanel center = new JPanel(new GridLayout(3,1)); JScrollPane scroll;
      // Node Labels
      center.add(scroll = new JScrollPane(entity_label_list = new JList<String>())); defaultListener(entity_label_list);
      scroll.setBorder(BorderFactory.createTitledBorder("Node Label"));
      // Node Color
      center.add(scroll = new JScrollPane(entity_color_list = new JList<String>())); defaultListener(entity_color_list);
      scroll.setBorder(BorderFactory.createTitledBorder("Node Color"));
      entity_color_list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      // Link Labels
      center.add(scroll = new JScrollPane(bundle_label_list = new JList<String>())); defaultListener(bundle_label_list);
      scroll.setBorder(BorderFactory.createTitledBorder("Link Label"));
      panel.add("Center", center);
    updateLabelLists(entity_tag_types);
    return panel;
  }

  /**
   * Create the labels menu from the existing fields within the bundles.
   * In addition to data types, there are various high-level labeling options
   * at the beginning of each list (entity, number of bundles, etc.)
   *
   *@param entity_tag_types types from type-value tags
   */
  private void updateLabelLists(Set<String> entity_tag_types) {
    updateEntityLabelList(entity_tag_types, entity_label_list);
    updateEntityLabelList(entity_tag_types, entity_color_list);
    updateBundleLabelList();
  }

  /**
   * Update the labels specific for entities.  These are more related to entity data
   * types.
   *
   *@param entity_tag_types types from type-value tags
   *@param gui_list         list to update
   */
  private void updateEntityLabelList(Set<String> entity_tag_types, JList<String> gui_list) {
    List<String> selection = Utils.jListGetValuesWrapper(gui_list);
    List<String> items     = new ArrayList<String>();
    // Add the transforms
    // BundlesG globals = getRTParent().getRootBundles().getGlobals();
    String postproc[] = BundlesDT.listAvailablePostProcessors();
    for (int i=0;i<postproc.length;i++) items.add(postproc[i]);
    /*
      String transforms[] = globals.getTransforms();
      for (int i=0;i<transforms.length;i++) items.add(transforms[i]);
    */
    // Add the tag types
    Iterator<String> it = entity_tag_types.iterator(); 
    while (it.hasNext()) items.add(TAG_TYPE_LM + it.next());
    Collections.sort(items, new CaseInsensitiveComparator());
    // Insert the default items
    items.add(0, IP_LABEL_LM);
    items.add(0, TIMEFRAME_MONTHS_LM);
    items.add(0, TIMEFRAME_DAYS_LM);
    items.add(0, TIMEFRAME_LM);
    items.add(0, LASTHEARD_LM);
    items.add(0, FIRSTHEARD_LM);
    items.add(0, DEGREE_LM);
    items.add(0, TAGS_LM);
    items.add(0, BUNDLECOUNT_LM);
    items.add(0, ENTITYTYPE_LM);
    items.add(0, ENTITYCOUNT_LM);
    items.add(0, ENTITY_LM);
    // Convert and update the list
    setListAndResetSelection(gui_list, items, selection);
  }

  /**
   * Update the edge (link) labeling list.
   *
   *@param entity_tag_types types from type-value tags
   */
  private void updateBundleLabelList() {
    List<String> selection  = Utils.jListGetValuesWrapper(bundle_label_list);
    List<String> items      = new ArrayList<String>();
    // Go through all of the header fields and add them with their variations
    boolean tags_present = false;
    BundlesG globals = getRTParent().getRootBundles().getGlobals();
    Iterator<String> it = globals.fieldIterator();
    while (it.hasNext()) {
      String fld = it.next(); int fld_i = globals.fieldIndex(fld);
      if (globals.isScalar(fld_i))   {
        items.add(fld + SIMPLESTAT_LM);
        items.add(fld + COMPLEXSTAT_LM);
      } else if (fld.equals("tags")) {
        tags_present = true;
      } else                         {
        items.add(fld + ITEMS_LM);
        items.add(fld + COUNT_LM);
      }
      BundlesDT.DT datatype = globals.getFieldDataType(fld_i);
      if (globals.isScalar(fld_i) == false && datatype == BundlesDT.DT.INTEGER) datatype = null;
      if (datatype != null) {
        String appends[] = BundlesDT.dataTypeVariations(datatype, globals);
        for (int i=0;i<appends.length;i++) items.add(fld + BundlesDT.DELIM + appends[i]);
      }
    }
    Collections.sort(items, new CaseInsensitiveComparator());
    // Add the tag options at the end
    if (tags_present) {
      items.add("tags" + ITEMS_LM);
      items.add("tags" + BundlesDT.MULTI + ITEMS_LM);
      items.add("tags" + BundlesDT.MULTI + COUNT_LM);
      Iterator<String> it2 = globals.tagTypeIterator();
      while (it2.hasNext()) {
        String tag_type = it2.next();
        items.add("tags" + BundlesDT.MULTI + BundlesDT.DELIM + tag_type + ITEMS_LM);
        items.add("tags" + BundlesDT.MULTI + BundlesDT.DELIM + tag_type + COUNT_LM);
      }
    }
    // Insert the default items
    items.add(0, TIMEFRAME_MONTHS_LM);
    items.add(0, TIMEFRAME_DAYS_LM);
    items.add(0, TIMEFRAME_LM);
    items.add(0, LASTHEARD_LM);
    items.add(0, FIRSTHEARD_LM);
    items.add(0, BUNDLECOUNT_LM);
    // Convert and update the list
    setListAndResetSelection(bundle_label_list, items, selection);
  }

  /**
   * Wrapper to set the list of items in a JList and then to keep the
   * selected elements.
   *
   *@param list       gui list to modify
   *@param items      all items that should be in the list
   *@param selection  selected items to retain in the list
   */
  private void setListAndResetSelection(JList<String> list, List<String> items, java.util.List<String> selection) {
    Map<String,Integer> str_to_i = new HashMap<String,Integer>();
    String as_str[] = new String[items.size()]; for (int i=0;i<as_str.length;i++) { as_str[i] = items.get(i); str_to_i.put(as_str[i], i); }
    list.setListData(as_str);
    if (selection.size() > 0) {
      List<Integer> ints = new ArrayList<Integer>();      
      // Find the indices for the previous settings
      for (int i=0;i<selection.size();i++)
        if (str_to_i.containsKey(selection.get(i))) 
          ints.add(str_to_i.get(selection.get(i)));
      // Convert back to ints
      int as_ints[] = new int[ints.size()];
      for (int i=0;i<as_ints.length;i++) as_ints[i] = ints.get(i);
      list.setSelectedIndices(as_ints);
    }
  }

  /**
   * Graph instance (directed)
   */
  SimpleMyGraph<Bundle> digraph = new SimpleMyGraph<Bundle>(),

  /**
   * Graph instance (undirected)
   */
                        graph   = new SimpleMyGraph<Bundle>();
  /**
   * Bi-connected component analysis of the graph
   */
  BiConnectedComponents graph_bcc, 

  /**
   * Bi-connected component analysis of the graph after single node neighbors are removed.
   * This provides non-trivial bi-connected components.
   */
                        graph2p_bcc;

  /**
   * Cluster Coefficients
   */
  Map<String,Double>    cluster_cos;

  /**
   *
   */
  Conductance           conductance;

  /** 
   * Create a subset of the data that only leaves one record per edge in the graph.  This
   * is used to make the graph render faster for interactive layout.
   */
  public void subsetOneBundlePerEdge() {
    Set<Bundle> set = new HashSet<Bundle>();
    for (int ent_i=0;ent_i<digraph.getNumberOfEntities();ent_i++) {
      for (int i=0;i<digraph.getNumberOfNeighbors(ent_i);i++) {
        int nbor_i = digraph.getNeighbor(ent_i,i);
        Iterator<Bundle> it = digraph.linkRefIterator(digraph.linkRef(ent_i,nbor_i));
        set.add(it.next());
      }
    }
    Bundles new_bundles = getRTParent().getRootBundles().subset(set);
    new_bundles.setSliceDescription("{\"operation\":\"subsetOneBundlePerEdge\",\"RTPanel\":\"" + getWinType() + "\"}");
    getRTParent().push(new_bundles);
  }

  /**
   * Layout IP address nodes by sub-dividing the octets recursively.
   */
  public void ipLogicalOctetLayout() {
    // Choose the appropriate set
    Set<String> sel = getRTParent().getSelectedEntities(); Iterator<String> it;
    saveLayoutForUndo(entity_to_wxy, sel);
    if (sel != null && sel.size() > 0) it = sel.iterator(); else it = entity_to_wxy.keySet().iterator();

    // Go through the set and layout the IP addresses
    double base_x = -32.0, base_y = -32.0;
    while (it.hasNext()) {
      String entity = it.next();
      if (Utils.isIPv4(entity)) {
        StringTokenizer st = new StringTokenizer(entity,"."); 
        int ip0 = Integer.parseInt(st.nextToken()), mx0 = ip0/16, my0 = ip0%16,
            ip1 = Integer.parseInt(st.nextToken()), mx1 = ip1/16, my1 = ip1%16,
            ip2 = Integer.parseInt(st.nextToken()), mx2 = ip2/16, my2 = ip2%16,
            ip3 = Integer.parseInt(st.nextToken()), mx3 = ip3/16, my3 = ip3%16;
        entity_to_wxy.put(entity, new Point2D.Double(base_x + mx0 + mx1/16.0 + mx2/256.0 + mx3/8192.0,
                                                     base_y + my0 + my1/16.0 + my2/256.0 + my3/8192.0));
        transform(entity);
      }
    }
    zoomToFit(); repaint();
  }

  /**
   * For a specific node, check all of the associated fields for the keyword.
   *
   *@param node_str node string
   *@param keyword  keyword
   *
   *@return true if one of the associated fields has the keyword
   */
  protected boolean checkAllAssociatedFields(String node_str, String keyword) {
    // create a lookup for the tablet to all entities keymaker
    Map<Tablet,KeyMaker> tablet_lu = new HashMap<Tablet,KeyMaker>();
    // get the node index
    int node_i = graph.getEntityIndex(node_str); 
    // go through the node neighbors
    for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) {
      // get the neighbor index
      int nbor_i = graph.getNeighbor(node_i, i);
      // get an iterator for any bundle related to this node
      Iterator<Bundle> it_bun = graph.linkRefIterator(graph.getLinkRef(node_i, nbor_i)); 
      // go through each bundle in the iterator
      while (it_bun.hasNext()) {
        // get the bundle and then check every field for the keyword
        Bundle bundle = it_bun.next();
        // get the tablet and either get/create the all entities maker for this tablet
        Tablet tablet = bundle.getTablet(); if (tablet_lu.containsKey(tablet) == false) tablet_lu.put(tablet, new KeyMaker(tablet, KeyMaker.ALL_ENTITIES_STR));
        KeyMaker km = tablet_lu.get(tablet);
        // turn the bundle into all the strings
        String keys[] = km.stringKeys(bundle);
        // check each string -- if any match, return true
        for (int j=0;j<keys.length;j++) if (keys[j] != null && keys[j].toLowerCase().indexOf(keyword) >= 0) return true;
      }
    }
    // if we get this far, the keyword wasn't found
    return false;
  }

  /**
   * FileChooser for the keyword treemap layout option.
   */
  JFileChooser keyword_file_chooser;

  /**
   * Load a keyword textfile that is then used to layout the reports via treemap.
   *
   *@param just_nodes_str    only examine the node name... not any of the associated fields
   *@param first_match_only  only the first matching keyword mattern -- later keyword matches will not be counted for the categorization
   */
  public void keywordTreeMapLayout(boolean just_nodes_str, boolean first_match_only) {
    // Allocate the file chooser -- keep this alive in the class so it can be re-used easily
    if (keyword_file_chooser == null) keyword_file_chooser = new JFileChooser(".");

    // Show the dialog and move forward if approved
    if (keyword_file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      File file = keyword_file_chooser.getSelectedFile(); if (file != null && file.exists()) {
        try {
          // Allocate a list for the keywords
          Map<String,String> keywords = new HashMap<String,String>(); List<String> keywords_list = new ArrayList<String>();

          // Read them in line by line -- force them to lower case to make it easier
          BufferedReader in = new BufferedReader(new FileReader(file));
          String line; while ((line = in.readLine()) != null) {
            int comma = line.indexOf(","); String keyword, equivalence;
            if (comma >= 0) { 
              keyword     = line.substring(0,comma).toLowerCase();
              equivalence = line.substring(comma+1,line.length()).toLowerCase();
            } else keyword = equivalence = line.toLowerCase();
            keywords.put(keyword,equivalence); keywords_list.add(keyword);
          }
          in.close();

          // Prepare the structure for the treemap call
          Map<String,Set<String>> keyword_lu = new HashMap<String,Set<String>>();

          // For each report, look for each keyword and place into the correct keyword lookup entry
          Iterator<String>    it_nodes = entity_to_wxy.keySet().iterator(); while (it_nodes.hasNext()) {
            StringBuffer key_acc  = new StringBuffer();
            String       node_str = it_nodes.next();
            Iterator<String> it_key = keywords_list.iterator();

            // Iterate over the keywords
            boolean keep_going = it_key.hasNext();
            while (keep_going) {
              String keyword = it_key.next(); keep_going = it_key.hasNext();

              // Check title
              boolean found = node_str.toLowerCase().indexOf(keyword) >= 0;

              // Check any associated fields with the underlying records of this node
              if (found == false && just_nodes_str == false) { found = checkAllAssociatedFields(node_str, keyword); }

              // If found, appent to the accumulated keywords for this report
              if (found) { if (key_acc.length() >= 0) key_acc.append("|"); 
                           key_acc.append(keywords.get(keyword)); 
                           if (first_match_only) keep_going = false; }
            }

            // Use the accumulated key for the treemap structure
            String key; if (key_acc.length() == 0) key = "|none|"; else key = key_acc.toString();
            if (keyword_lu.containsKey(key) == false) keyword_lu.put(key, new HashSet<String>());
            keyword_lu.get(key).add(node_str);
          }

          // Run the treemap layout and then place the results
          saveLayoutForUndo(entity_to_wxy, null);
          TreeMap<String,String>  treemap  = new TreeMap<String,String>(keyword_lu);
          Map<String,Rectangle2D> regions  = treemap.squarifiedTileMapping();
          treemap.transformWorldPoints(regions, entity_to_wxy);

          // Ask for a render
          transform(); zoomToFit(); getRTComponent().render();
        } catch (IOException ioe) { JOptionPane.showMessageDialog(file_chooser, "IOException: " + ioe, "File Load Error", JOptionPane.ERROR_MESSAGE); }
      }
    }
  }

  /**
   * Partition IP addresses based on a CIDR block.
   *
   *@param cidr_mask needs to be either 8, 16, or 24
   */
  public void cidrTreeMapLayout(int cidr_mask) {
    if (cidr_mask != 8 && cidr_mask != 16 && cidr_mask != 24) return;

    // Prepare the structure for the treemap call
    Map<String,Set<String>> keyword_lu = new HashMap<String,Set<String>>();
    
    // Go through the nodes
    saveLayoutForUndo(entity_to_wxy, null);
    Iterator<String> it_nodes = entity_to_wxy.keySet().iterator(); while (it_nodes.hasNext()) {
      String node_str = it_nodes.next(); String key = "not_ip";
      if (Utils.isIPv4(node_str)) {
        switch (cidr_mask)  { case 8:  key = Utils.ipv4CIDR08(node_str); break;
                              case 16: key = Utils.ipv4CIDR16(node_str); break;
                              default: key = Utils.ipv4CIDR24(node_str); break; };
      }
      if (keyword_lu.containsKey(key) == false) keyword_lu.put(key, new HashSet<String>());
      keyword_lu.get(key).add(node_str);
    }

    // Run the treemap layout and then place the results
    TreeMap<String,String>  treemap  = new TreeMap<String,String>(keyword_lu);
    Map<String,Rectangle2D> regions  = treemap.squarifiedTileMapping();
    treemap.transformWorldPoints(regions, entity_to_wxy);

    // Ask for a render
    transform(); zoomToFit(); getRTComponent().render();
  }

  /**
   * For a graph contructed with the tag types nodes, group the nodes by their type portion
   * of the string.
   */
  public void tagTypeTreeMapLayout() {
    // Make the structure for the groupings
    Map<String,Set<String>> groups = new HashMap<String,Set<String>>();
    groups.put("ee", new HashSet<String>()); // everything else

    // Group the nodes by their type part... or if no type part, then put them with everything else
    Iterator<String> it_nodes = entity_to_wxy.keySet().iterator(); while (it_nodes.hasNext()) {
      String node_str = it_nodes.next();
      if (node_str.indexOf("=") >= 0) {
        String group = node_str.substring(0, node_str.indexOf("="));
        if (groups.containsKey(group) == false) groups.put(group, new HashSet<String>());
        groups.get(group).add(node_str);
      } else { groups.get("ee").add(node_str); }
    }

    // Run the tree map algorithm
    TreeMap<String,String> treemap = new TreeMap<String,String>(groups);
    Map<String,Rectangle2D> regions  = treemap.squarifiedTileMapping();
    treemap.transformWorldPoints(regions, entity_to_wxy);

    // Ask for a render
    transform(); zoomToFit(); getRTComponent().render();
  }

  /**
   * Layout the nodes by their color using a treemap.  Assumes that the color option is not trivial.
   */
  public void nodeColorTreeMapLayout() {
    // Get a valid render context
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Check for a valid color setting
    NodeColor node_color = getNodeColor();
    if (node_color == NodeColor.VARY || node_color == NodeColor.LABEL || node_color == NodeColor.DATATYPE) {
      myrc.nodeColorTreeMapLayout(node_color, getRTParent().getSelectedEntities());
    } else System.err.println("RTGraphPanel.nodeColorTreeMapLayout() - Only works with VARY or LABEL");
  }

  /**
   * Layout the nodes by using their colors to form composite nodes and then applying a force directed layout.
   */
  public void nodeColorCompositeFDLayout() {
    // Get a valid render context
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Check for a valid color setting
    NodeColor node_color = getNodeColor();
    if (node_color == NodeColor.VARY || node_color == NodeColor.LABEL) { 
      (new Thread(myrc.nodeColorCompositeFDLayoutRunnable(node_color))).start();
    } else System.err.println("RTGraphPanel.nodeColorCompositeFDLayout() - Only works with VARY or LABEL");
  }

  /**
   * Collapse blocks of the graph (based on the biconnected components) into single aggregate
   * nodes.
   */
  public void collapseBlocks() {
    saveLayoutForUndo(entity_to_wxy, null);
    // Create it if it's null
    if (graph_bcc == null) {
      // Create graph parametrics (Only add linear time algorithms here...)
      graph_bcc     = new BiConnectedComponents(graph);
      graph2p_bcc   = new BiConnectedComponents(new UniTwoPlusDegreeGraph(graph));
    }
    BiConnectedComponents bcc = graph_bcc, bcc_2p = graph2p_bcc; if (bcc != null && bcc_2p != null) {
      // Get the vertex to block lookup
      Map<String,Set<MyGraph>> v_to_b = bcc.getVertexToBlockMap();
      // Go through the entities and accumulate the positions
      Map<MyGraph,Double> x_sum = new HashMap<MyGraph,Double>(), y_sum = new HashMap<MyGraph,Double>();
      Iterator<String> it_e = entity_to_wxy.keySet().iterator();
      while (it_e.hasNext()) {
        String entity = it_e.next(); Point2D pt = entity_to_wxy.get(entity);
        if (v_to_b.containsKey(entity)) {
          Iterator<MyGraph> it_mg = v_to_b.get(entity).iterator();
          while (it_mg.hasNext()) {
            MyGraph mg = it_mg.next();
            if (x_sum.containsKey(mg) == false) { x_sum.put(mg,0.0); y_sum.put(mg,0.0); }
            x_sum.put(mg,x_sum.get(mg)+pt.getX()); y_sum.put(mg,y_sum.get(mg)+pt.getY());
          }
        } else System.err.println("Vertex To Block Lookup missing \"" + entity + "\"");
      }
      // Now position those entities that aren't cut vertices at the center of the graph
      it_e = entity_to_wxy.keySet().iterator();
      while (it_e.hasNext()) {
        String entity = it_e.next(); // Point2D pt = entity_to_wxy.get(entity);
        if (v_to_b.containsKey(entity) && bcc.getCutVertices().contains(entity) == false) {
          MyGraph mg = v_to_b.get(entity).iterator().next();
          entity_to_wxy.put(entity, new Point2D.Double(x_sum.get(mg)/mg.getNumberOfEntities(),y_sum.get(mg)/mg.getNumberOfEntities()));
          transform(entity);
        }
      }
      // Re-render
      zoomToFit(); repaint();
    }
  }

  /**
   * Layout the nodes in the x-axis based on their temporal occurence within the data set.
   * The y-position is calculated based on the corresponding integer values for the entities.
   */
  public void temporalLayout() {
    // Choose the appropriate set
    Set<String> sel = getRTParent().getSelectedEntities(); Iterator<String> it;
    saveLayoutForUndo(entity_to_wxy, sel);
    if (sel != null && sel.size() > 0) it = sel.iterator(); else it = entity_to_wxy.keySet().iterator();
    // Get globals for string to int conversion
    Bundles  root    = getRTParent().getRootBundles();
    BundlesG globals = root.getGlobals();
    // Go through the selection and place the nodes
    while (it.hasNext()) {
      String entity     = it.next(); int entity_i = graph.getEntityIndex(entity);
      int    entity_int = globals.toInt(entity); 
      long   ts0 = root.ts1(), ts1 = root.ts0();
      // Go through the neighbors and iterate through the underlying bundles
      for (int i=0;i<graph.getNumberOfNeighbors(entity_i);i++) {
        int              nbor_i   = graph.getNeighbor(entity_i, i);
        String           linkref  = graph.getLinkRef(entity_i, nbor_i);
        Iterator<Bundle> it_b     = graph.linkRefIterator(linkref);
        while (it_b.hasNext()) {
          Bundle bundle = it_b.next();
          if (bundle.hasTime()) {
            if (ts0 > bundle.ts0()) ts0 = bundle.ts0();
            if (ts1 < bundle.ts0()) ts1 = bundle.ts0();
          }
        }
      }
      // Place the node
      Point2D point = new Point2D.Double((((double) ts0)/(root.ts1() - root.ts0())),((double) entity_int)/Integer.MAX_VALUE);
      entity_to_wxy.put(entity, point);
      // Do the transformation
      transform(entity);
    }
    zoomToFit(); repaint();
  }

  /**
   * Edge (link) style strings
   */
  final static String STYLE_SOLID_STR     = "Style - Solid",
                      STYLE_LONG_DASH_STR = "Style - Long Dash",
                      STYLE_DOTTED_STR    = "Style - Dotted",
                      STYLE_ALTERNATE_STR = "Style - Long/Short Alt";

  /**
   * Array of edge style strings
   */
  final static String STYLE_STRS[] = { STYLE_SOLID_STR,
                                       STYLE_LONG_DASH_STR,
                                       STYLE_DOTTED_STR,
                                       STYLE_ALTERNATE_STR };

  /**
   * Enumeration for edge styles
   */
  enum LineStyle { SOLID, LONG_DASH, DOTTED, ALTERNATE };

  /**
   * Map to convert link style strings to the corresponding enumeration.
   */
  static Map<String,LineStyle> str_to_linsty;
  static {
    str_to_linsty = new HashMap<String,LineStyle>();
    str_to_linsty.put(STYLE_SOLID_STR, LineStyle.SOLID);
    str_to_linsty.put(STYLE_LONG_DASH_STR, LineStyle.LONG_DASH);
    str_to_linsty.put(STYLE_DOTTED_STR, LineStyle.DOTTED);
    str_to_linsty.put(STYLE_ALTERNATE_STR, LineStyle.ALTERNATE);
  }

  /**
   * Select nodes by their symbol.
   *
   *@param symbol symbol to select by
   */
  public void selectBySymbol(Utils.Symbol symbol) {
    Set<String> sel = new HashSet<String>();
    Iterator<String> it = entity_to_shape.keySet().iterator();
    while (it.hasNext()) { String entity = it.next(); if (entity_to_shape.get(entity) == symbol) sel.add(entity); }
    ((RTGraphComponent) getRTComponent()).setOperation(sel);
  }

  /**
   * Method to convert strings to line styles.
   *
   *@param  str edge/line/link style string
   *
   *@return enumeration
   */
  public LineStyle parseStyle(String str) { return str_to_linsty.get(str); }

  /**
   * Entity_to_shape - the shape of the entity when drawn
   */
  Map<String,Utils.Symbol>       entity_to_shape = new HashMap<String,Utils.Symbol>();

  /**
   * Entity_to_wxy - the entities world x, y coordinates
   */
  Map<String,Point2D>      entity_to_wxy   = new HashMap<String,Point2D>();

  /**
   * Entity_to_sxy - the entities screen x, y coordinates (transformed from the world coordinates)
   * - the sxy is stored as a string for mapping in other contexts
   */
  Map<String,String>       entity_to_sxy   = new HashMap<String,String>();

  /**
   * entity_to_sx, _to_sy - entity to screen x and screen y separately -- much faster for conversion
   */
  Map<String,Integer>      entity_to_sx    = new HashMap<String,Integer>(),
                           entity_to_sy    = new HashMap<String,Integer>();

  /**
   * Current set of active relationships.
   */
  List<String>       active_relationships   = new ArrayList<String>();

  /**
   * Mapping of the active relationship string to the edge/link/line style.
   */
  Map<String,String>  relationships_to_style = new HashMap<String,String>();

  /**
   * Static string for storing application parameters related to recent relationships.
   */
  static final String RECENT_RELATIONSHIPS_PREF_STR = "RTGraphPanel.recentRelationships";

  /**
   * Keep recently used relationships in the common relationships menu.
   * Write them to preferences, limit to 20
   *
   *@param encoded encoded version of the relationship string (maybe)
   */
  public void updateRecentRelationships(String encoded) {
    String strs[]     = RTPrefs.retrieveStrings(RECENT_RELATIONSHIPS_PREF_STR);
    String new_strs[];
    if (strs == null || strs.length == 0) {
      // Make a default one
      new_strs = new String[1]; new_strs[0] = Utils.encToURL(encoded) + BundlesDT.DELIM + System.currentTimeMillis();
    } else {
      // Transfer existing to a map for recency/set update
      Map<String,Long> map = new HashMap<String,Long>();
      long earliest = 0L;
      for (int i=0;i<strs.length;i++) { StringTokenizer st = new StringTokenizer(strs[i], BundlesDT.DELIM);
                                        String str = st.nextToken(); long ts = Long.parseLong(st.nextToken());
                                        if (earliest == 0L) earliest = ts; else if (ts < earliest) earliest = ts;
                                        map.put(str,ts); }
      // Add the new one
      map.put(Utils.encToURL(encoded),System.currentTimeMillis());
      // Only keep 20 or less based on recency
      if (map.keySet().size() > 20) {
        Iterator<String> it = map.keySet().iterator();
        while (it.hasNext()) {
          String str = it.next(); if (earliest == map.get(str)) it.remove();
        }
      }
      // Transfer to a set of strings
      new_strs = new String[map.keySet().size()];
      Iterator<String> it = map.keySet().iterator();
      for (int i=0;i<new_strs.length;i++) {
        String str = it.next(); long ts = map.get(str);
        new_strs[i] = str + BundlesDT.DELIM + ts;
      }
    }
    // Store it off and update the menu // Note that this won't go across other RTGraphPanel windows -- will need a restart
    RTPrefs.store(RECENT_RELATIONSHIPS_PREF_STR, new_strs);
    fillCommonRelationshipsMenu();
  }

  class RelationshipStruct {
    String  fm_hdr, 
            fm_ico; 
    boolean fm_typed;
    String  to_hdr, 
            to_ico; 
    boolean to_typed;
    String  style;
    boolean ignore_ns; 

    public RelationshipStruct(String rel_str) {
      StringTokenizer st = new StringTokenizer(rel_str, BundlesDT.DELIM);
      fm_hdr = Utils.decFmURL(st.nextToken()); 
      fm_ico = Utils.decFmURL(st.nextToken()); 
      fm_typed = st.nextToken().toLowerCase().equals("true");
      to_hdr = Utils.decFmURL(st.nextToken()); 
      to_ico = Utils.decFmURL(st.nextToken()); 
      to_typed = st.nextToken().toLowerCase().equals("true");
      style  = Utils.decFmURL(st.nextToken());
      ignore_ns = st.nextToken().toLowerCase().equals("true");
    }
  }

  /**
   * Extract the "from typed" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "from typed" flag
   */
  public boolean relationshipFromTyped(String rel_str) { return (new RelationshipStruct(rel_str)).fm_typed; }

  /**
   * Extract the "to typed" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "to typed" flag
   */
  public boolean relationshipToTyped(String rel_str) { return (new RelationshipStruct(rel_str)).to_typed; }

  /**
   * Extract the "from header" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "from header" flag
   */
  public String relationshipFromHeader(String rel_str) { return (new RelationshipStruct(rel_str)).fm_hdr; }

  /**
   * Extract the "to header" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "to header" flag
   */
  public String relationshipToHeader(String rel_str) { return (new RelationshipStruct(rel_str)).to_hdr; }

  /**
   * Extract the "from icon" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "from icon" flag
   */
  public String  relationshipFromIcon(String rel_str) { return (new RelationshipStruct(rel_str)).fm_ico; }

  /**
   * Extract the "to icon" field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "to icon" flag
   */
  public String  relationshipToIcon(String rel_str) { return (new RelationshipStruct(rel_str)).to_ico; }

  /**
   * Extract the edge style field from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return edge style
   */
  public String  relationshipStyle(String rel_str) { return (new RelationshipStruct(rel_str)).style; }

  /**
   * Extract the "ignore not sets" flag from an encoded relationship string.
   *
   *@param  rel_str encoded relationship string
   *@return "ignore not sets" flag
   */
  public boolean relationshipIgnoreNotSet(String rel_str) { return (new RelationshipStruct(rel_str)).ignore_ns; }
 
  /**
   * Add nodes that show how headers connect with one another.
   *
   *@param include_types if true, include the header types as part of the graph
   */
  public void addHeaderRelationshipsStars(boolean include_types) { 
    Bundles bs = getRTParent().getRootBundles();
    Iterator<Tablet> it_tab = bs.tabletIterator();
    while (it_tab.hasNext()) {
      Tablet tablet   = it_tab.next();
      int    fields[] = tablet.getFields();

      // Add the tablet node
      String tablet_str = (new KeyMaker(tablet, KeyMaker.TABLET_SEP_STR).stringKeys(tablet.bundleIterator().next()))[0];
      if (entity_to_shape.containsKey(tablet_str) == false) entity_to_shape.put(tablet_str, Utils.Symbol.SQUARE); 
      if (entity_to_wxy.containsKey(tablet_str) == false) { entity_to_wxy.put(tablet_str, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); transform(tablet_str); }
      graph.addNode(tablet_str); digraph.addNode(tablet_str);

      // Only keep the non-null fields
      List<Integer> al = new ArrayList<Integer>(); for (int i=0;i<fields.length;i++) if (fields[i] != -1) al.add(i);
      fields = new int[al.size()]; for (int i=0;i<fields.length;i++) fields[i] = al.get(i);

      // Go through the valid fields now
      for (int i=0;i<fields.length;i++) {
        int    hdr_i     = fields[i]; String hdr_i_str = bs.getGlobals().fieldHeader(hdr_i);

        // Put the symbol
        if (entity_to_shape.containsKey(hdr_i_str) == false) entity_to_shape.put(hdr_i_str, Utils.Symbol.CIRCLE); 

        // Make the world location
        if (entity_to_wxy.containsKey(hdr_i_str) == false) { entity_to_wxy.put(hdr_i_str, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); transform(hdr_i_str); }

        // Figure out the weights
        graph.addNode(hdr_i_str); digraph.addNode(hdr_i_str);
        double g_w  =   graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex(tablet_str)),
               dg_w = digraph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex(tablet_str));
        if (Double.isInfinite(g_w)) g_w = 0.0; if (Double.isInfinite(dg_w)) dg_w = 0.0;
        // Add the edges
          graph.addNeighbor(hdr_i_str,  tablet_str, g_w  + tablet.size());
          graph.addNeighbor(tablet_str, hdr_i_str,  g_w  + tablet.size());
        digraph.addNeighbor(hdr_i_str,  tablet_str, dg_w + tablet.size());
        // Associate the bundles with the edge
        Iterator<Bundle> it_bun = tablet.bundleIterator();
        while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
            graph.addLinkReference(  graph.getEntityIndex(hdr_i_str),   graph.getEntityIndex(tablet_str), bundle);
            graph.addLinkReference(  graph.getEntityIndex(tablet_str),  graph.getEntityIndex(hdr_i_str),  bundle);
          digraph.addLinkReference(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex(tablet_str), bundle);
          // Include the data types if specified
          if (include_types) {
            BundlesDT.DT datatype = BundlesDT.getEntityDataType(bundle.toString(hdr_i));
            // System.err.println("Including Type For \"" + bundle.toString(hdr_i) + "\" ==> " + datatype);
            if (datatype != null) {
                graph.addNode("" + datatype);  entity_to_shape.put("" + datatype, Utils.Symbol.TRIANGLE);
              digraph.addNode("" + datatype);  
              if (entity_to_wxy.containsKey("" + datatype) == false) {
                entity_to_wxy.put("" + datatype, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); 
                transform("" + datatype);
              }
              g_w  = graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex("" + datatype));
              dg_w = graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex("" + datatype));
              if (Double.isInfinite(g_w)) g_w = 0.0; if (Double.isInfinite(dg_w)) dg_w = 0.0;
                graph.addNeighbor("" + datatype, hdr_i_str,      g_w + 1);
                graph.addNeighbor(hdr_i_str,     "" + datatype,  g_w + 1);
              digraph.addNeighbor(hdr_i_str,     "" + datatype, dg_w + 1);
                graph.addLinkReference(  graph.getEntityIndex(hdr_i_str),      graph.getEntityIndex("" + datatype), bundle);
                graph.addLinkReference(  graph.getEntityIndex("" + datatype),  graph.getEntityIndex(hdr_i_str),     bundle);
              digraph.addLinkReference(digraph.getEntityIndex(hdr_i_str),    digraph.getEntityIndex("" + datatype), bundle);
              digraph.addLinkStyle(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex("" + datatype), STYLE_DOTTED_STR);
            }
          }
        }
        // Add the style
        digraph.addLinkStyle(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex(tablet_str), STYLE_SOLID_STR);
      }
    }
    getRTComponent().render();
  }

  /**
   * Add nodes that show how headers connect with one another.
   *
   *@param include_types if true, include the header types as part of the graph
   */
  public void addHeaderRelationships(boolean include_types) { 
    Bundles bs = getRTParent().getRootBundles();
    Iterator<Tablet> it_tab = bs.tabletIterator();
    while (it_tab.hasNext()) {
      Tablet tablet   = it_tab.next();
      // System.err.println("Tablet \"" + tablet + "\"");
      int    fields[] = tablet.getFields();
      // Only keep the non-null fields
      List<Integer> al = new ArrayList<Integer>(); for (int i=0;i<fields.length;i++) if (fields[i] != -1) al.add(i);
      fields = new int[al.size()]; for (int i=0;i<fields.length;i++) fields[i] = al.get(i);
      // Go through the valid fields now
      for (int i=0;i<fields.length;i++) {
        int    hdr_i     = fields[i];
        int    hdr_j     = fields[(i+1)%fields.length];
        String hdr_i_str = bs.getGlobals().fieldHeader(hdr_i),
               hdr_j_str = bs.getGlobals().fieldHeader(hdr_j);
        // System.err.println("  \"" + hdr_i_str + "\" => \"" + hdr_j_str + "\"");
        // Put the symbol
        if (entity_to_shape.containsKey(hdr_i_str) == false) entity_to_shape.put(hdr_i_str, Utils.Symbol.CIRCLE); 
        if (entity_to_shape.containsKey(hdr_j_str) == false) entity_to_shape.put(hdr_j_str, Utils.Symbol.CIRCLE); 
        // Make the world location
        if (entity_to_wxy.containsKey(hdr_i_str) == false) { entity_to_wxy.put(hdr_i_str, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); transform(hdr_i_str); }
        if (entity_to_wxy.containsKey(hdr_j_str) == false) { entity_to_wxy.put(hdr_j_str, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); transform(hdr_j_str); }
        // Figure out the weights
        graph.addNode(hdr_i_str); graph.addNode(hdr_j_str); digraph.addNode(hdr_i_str); digraph.addNode(hdr_j_str);
        double g_w  =   graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex(hdr_j_str)),
               dg_w = digraph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex(hdr_j_str));
        if (Double.isInfinite(g_w)) g_w = 0.0; if (Double.isInfinite(dg_w)) dg_w = 0.0;
        // Add the edges
          graph.addNeighbor(hdr_i_str, hdr_j_str, g_w  + tablet.size());
          graph.addNeighbor(hdr_j_str, hdr_i_str, g_w  + tablet.size());
        digraph.addNeighbor(hdr_i_str, hdr_j_str, dg_w + tablet.size());
        // Associate the bundles with the edge
        Iterator<Bundle> it_bun = tablet.bundleIterator();
        while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
            graph.addLinkReference(  graph.getEntityIndex(hdr_i_str),   graph.getEntityIndex(hdr_j_str), bundle);
            graph.addLinkReference(  graph.getEntityIndex(hdr_j_str),   graph.getEntityIndex(hdr_i_str), bundle);
          digraph.addLinkReference(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex(hdr_j_str), bundle);
          // Include the data types if specified
          if (include_types) {
            BundlesDT.DT datatype = BundlesDT.getEntityDataType(bundle.toString(hdr_i));
            // System.err.println("Including Type For \"" + bundle.toString(hdr_i) + "\" ==> " + datatype);
            if (datatype != null) {
                graph.addNode("" + datatype);  entity_to_shape.put("" + datatype, Utils.Symbol.TRIANGLE);
              digraph.addNode("" + datatype);  
              if (entity_to_wxy.containsKey("" + datatype) == false) {
                entity_to_wxy.put("" + datatype, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1)); 
                transform("" + datatype);
              }
              g_w  = graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex("" + datatype));
              dg_w = graph.getConnectionWeight(graph.getEntityIndex(hdr_i_str), graph.getEntityIndex("" + datatype));
              if (Double.isInfinite(g_w)) g_w = 0.0; if (Double.isInfinite(dg_w)) dg_w = 0.0;
                graph.addNeighbor("" + datatype, hdr_i_str,      g_w + 1);
                graph.addNeighbor(hdr_i_str,     "" + datatype,  g_w + 1);
              digraph.addNeighbor(hdr_i_str,     "" + datatype, dg_w + 1);
                graph.addLinkReference(  graph.getEntityIndex(hdr_i_str),      graph.getEntityIndex("" + datatype), bundle);
                graph.addLinkReference(  graph.getEntityIndex("" + datatype),  graph.getEntityIndex(hdr_i_str),     bundle);
              digraph.addLinkReference(digraph.getEntityIndex(hdr_i_str),    digraph.getEntityIndex("" + datatype), bundle);
              digraph.addLinkStyle(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex("" + datatype), STYLE_DOTTED_STR);
            }
          }
        }
        // Add the style
        digraph.addLinkStyle(digraph.getEntityIndex(hdr_i_str), digraph.getEntityIndex(hdr_j_str), STYLE_SOLID_STR);
      }
    }
    getRTComponent().render();
  }

  /**
   * Determine which blanks can be used as node types.
   *
   *@return blanks for node types
   */
  public static String[] nodeBlanks(BundlesG globals) { 
    String base_blanks[] = KeyMaker.blanks(globals);

    String blanks[]      = new String[base_blanks.length + 8];
    System.arraycopy(base_blanks, 0, blanks, 0, base_blanks.length);

    blanks[base_blanks.length+0] = KeyMaker.BY_YEAR_STR;
    blanks[base_blanks.length+1] = KeyMaker.BY_YEAR_MONTH_STR;
    blanks[base_blanks.length+2] = KeyMaker.BY_YEAR_MONTH_DAY_STR;

    blanks[base_blanks.length+3] = KeyMaker.BY_MONTH_STR;
    blanks[base_blanks.length+4] = KeyMaker.BY_MONTH_DAY_STR;

    blanks[base_blanks.length+5] = KeyMaker.BY_DAYOFWEEK_STR;
    blanks[base_blanks.length+6] = KeyMaker.BY_DAYOFWEEK_HOUR_STR;
    blanks[base_blanks.length+7] = KeyMaker.BY_HOUR_STR;
   
    return blanks;
  }

  /**
   * Go through the bundles and create both a unidirectional and a directional graph.
   * - For the graphs, keep the relationship of the edges to the bundles.
   *
   *@param encoded_str relationship string (encoded)
   *@param built_in    is the relationship builtin?
   *@param to_add      bundles (records) to add to the graph
   */
  public void addRelationship(String encoded_str, boolean built_in, Bundles to_add) {
    StringTokenizer st = new StringTokenizer(encoded_str, BundlesDT.DELIM);
    String  fm_hdr    = Utils.decFmURL(st.nextToken()),
            fm_ico    = Utils.decFmURL(st.nextToken());
    boolean fm_typed  = st.nextToken().toLowerCase().equals("true");
    String  to_hdr    = Utils.decFmURL(st.nextToken()),
            to_ico    = Utils.decFmURL(st.nextToken());
    boolean to_typed  = st.nextToken().toLowerCase().equals("true");
    String  style     = Utils.decFmURL(st.nextToken());
    boolean ignore_ns = st.nextToken().toLowerCase().equals("true");
    // Determine if relationship is possible with the current data set...  if not return
    String blanks[] = nodeBlanks(getRTParent().getRootBundles().getGlobals());
    boolean to_found = false, fm_found = false;
    for (int i=0;i<blanks.length;i++) {
      if (blanks[i].equals(fm_hdr)) fm_found = true;
      if (blanks[i].equals(to_hdr)) to_found = true;
    }
    if (fm_found && to_found) addRelationship(fm_hdr, fm_ico, fm_typed, to_hdr, to_ico, to_typed, style, ignore_ns, false, to_add);
  }

  /**
   * Add an edge relationship to the graph.
   *
   *@param fm_field       from node field
   *@param fm_symbol_str  symbol for the from nodes
   *@param fm_typed       add type of label on the from nodes
   *@param to_field       to node field
   *@param to_symbol_str  symbol for the to nodes
   *@param to_typed       add type of label on the to nodes
   *@param style_str      style of edge
   *@param ignore_ns      ignore not sets in fields (i.e., don't include them in the graph)
   *@param built_in       relationship description is built-in to the application
   */
  public void addRelationship(String fm_field,  String  fm_symbol_str, boolean fm_typed,
                              String to_field,  String  to_symbol_str, boolean to_typed, 
                              String style_str, boolean ignore_ns,     boolean built_in) {
    addRelationship(fm_field, fm_symbol_str, fm_typed, to_field, to_symbol_str, to_typed, style_str, ignore_ns, built_in, null); }

  /**
   * Add an edge relationship to the graph.
   *
   *@param fm_field       from node field
   *@param fm_symbol_str  symbol for the from nodes
   *@param fm_typed       add type of label on the from nodes
   *@param to_field       to node field
   *@param to_symbol_str  symbol for the to nodes
   *@param to_typed       add type of label on the to nodes
   *@param style_str      style of edge
   *@param ignore_ns      ignore not sets in fields (i.e., don't include them in the graph)
   *@param built_in       relationship description is built-in to the application
   *@param to_add         records (bundles) to add to the graph
   */
  public void addRelationship(String fm_field,  String  fm_symbol_str, boolean fm_typed,
                              String to_field,  String  to_symbol_str, boolean to_typed, 
                              String style_str, boolean ignore_ns,     boolean built_in, Bundles to_add) {
    String  fm_pre  = "", to_pre = "";

    if (fm_typed) fm_pre = fm_field + BundlesDT.DELIM;
    if (to_typed) to_pre = to_field + BundlesDT.DELIM;

    Utils.Symbol fm_symbol = Utils.parseSymbol(fm_symbol_str),
                 to_symbol = Utils.parseSymbol(to_symbol_str);

      // Keep track of existing relationships, update the longer term ones
      String encoded_relationship_str = Utils.encToURL(fm_field)  + BundlesDT.DELIM + Utils.encToURL(fm_symbol_str) + BundlesDT.DELIM + Utils.encToURL("" + fm_typed) + BundlesDT.DELIM +
                                        Utils.encToURL(to_field)  + BundlesDT.DELIM + Utils.encToURL(to_symbol_str) + BundlesDT.DELIM + Utils.encToURL("" + to_typed) + BundlesDT.DELIM +
                                        Utils.encToURL(style_str) + BundlesDT.DELIM + Utils.encToURL("" + ignore_ns);
      if (active_relationships.contains(encoded_relationship_str) == false) active_relationships.add(encoded_relationship_str);
      if (built_in == false) updateRecentRelationships(encoded_relationship_str);
      // Is this an addition or from scratch?
      Bundles bundles; if (to_add == null) bundles = getRTParent().getRootBundles(); else bundles = to_add;
      // BundlesG globals = bundles.getGlobals();
      // Go through the tablets
      Iterator<Tablet> it_tablet = bundles.tabletIterator();
      while (it_tablet.hasNext()) {
        Tablet tablet = it_tablet.next();
        // Check to see if this table will complete both blanks, if so, go through the bundles adding the edges to the graphs
        if (KeyMaker.tabletCompletesBlank(tablet,fm_field) && KeyMaker.tabletCompletesBlank(tablet,to_field)) {
          // Create the key makers
          KeyMaker fm_km = new KeyMaker(tablet,fm_field), to_km = new KeyMaker(tablet,to_field);
          // Go through the bundles
          Iterator<Bundle> it_bundle = tablet.bundleIterator();
          while (it_bundle.hasNext()) {
            Bundle bundle    = it_bundle.next();
            // Create the combinator for the from and to keys
            String fm_keys[], to_keys[];
            // Transform the bundle to keys
            fm_keys = fm_km.stringKeys(bundle);
            to_keys = to_km.stringKeys(bundle);
            // Make the relationships
            if (fm_keys != null && fm_keys.length > 0 && to_keys != null && to_keys.length > 0) {
              for (int i=0;i<fm_keys.length;i++) for (int j=0;j<to_keys.length;j++) {
                // Check for not sets if the flag is specified
                if (ignore_ns && (fm_keys[i].equals(BundlesDT.NOTSET) || to_keys[j].equals(BundlesDT.NOTSET))) continue;
                // The key will be a combination of the header and the entity
                String fm_fin = fm_pre + fm_keys[i], to_fin = to_pre + to_keys[j];
                // If we're in retain mode only, make sure both nodes exist in the set
                if (retained_nodes != null && retained_nodes.size() > 0 && (retained_nodes.contains(fm_fin) == false || retained_nodes.contains(to_fin) == false)) continue;
                // Set the shape
                if (entity_to_shape.containsKey(fm_fin) == false) entity_to_shape.put(fm_fin, fm_symbol);
                if (entity_to_shape.containsKey(to_fin) == false) entity_to_shape.put(to_fin, to_symbol);
                // Create the initial world coordinate and transform as appropriate 
                if (entity_to_wxy.containsKey(fm_fin) == false) { 
                  entity_to_wxy.put(fm_fin, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1));
                  transform(fm_fin); }
                if (entity_to_wxy.containsKey(to_fin) == false) { 
                  entity_to_wxy.put(to_fin, new Point2D.Double(Math.random()*2 - 1, Math.random()*2 - 1));
                  transform(to_fin); }
                // Add the reference back to this object
                graph.addNode(fm_fin);   graph.addNode(to_fin);
                digraph.addNode(fm_fin); digraph.addNode(to_fin);
                // Set the weights equal to the number of bundles on the edge
                double    previous_weight =   graph.getConnectionWeight(  graph.getEntityIndex(fm_fin),   graph.getEntityIndex(to_fin)),
                       di_previous_weight = digraph.getConnectionWeight(digraph.getEntityIndex(fm_fin), digraph.getEntityIndex(to_fin));
                // Check for infinite because the graph class returns infinite if two nodes are not connected
                if (Double.isInfinite(   previous_weight))    previous_weight = 0.0;
                if (Double.isInfinite(di_previous_weight)) di_previous_weight = 0.0;
                // Finally, add them to both forms of the graphs
                  graph.addNeighbor(fm_fin, to_fin,    previous_weight + 1.0);
                  graph.addNeighbor(to_fin, fm_fin,    previous_weight + 1.0);
                digraph.addNeighbor(fm_fin, to_fin, di_previous_weight + 1.0);
                // System.err.println("RTGraphPanel.addRelationship() : \"" + fm_fin + "\" => \"" + to_fin + "\": w=" + (previous_weight+1.0) + " | di_w=" + (di_previous_weight+1.0));
                  graph.addLinkReference(  graph.getEntityIndex(fm_fin),   graph.getEntityIndex(to_fin), bundle);
                  graph.addLinkReference(  graph.getEntityIndex(to_fin),   graph.getEntityIndex(fm_fin), bundle);
                digraph.addLinkReference(digraph.getEntityIndex(fm_fin), digraph.getEntityIndex(to_fin), bundle);
                // Keep track of the link style
                digraph.addLinkStyle(digraph.getEntityIndex(fm_fin), digraph.getEntityIndex(to_fin), style_str);
              }
            }
          }
        }
      }
    // Nullify the biconnected components
    graph_bcc   = null;
    graph2p_bcc = null;
    cluster_cos = null;
    conductance = null;
    // Re-render
    getRTComponent().render();
  }

  /**
   * Show a dialog to delete a realtionship in the graph
   */
  public void deleteRelationshipDialog() { new DeleteRelationshipDialog(); }

  /**
   * Dialog to allow a user to specify a relationship to remove from the current graph.
   */
  class DeleteRelationshipDialog extends JDialog {
    private static final long serialVersionUID = -3318668787937566057L;
    /**
     * Translates the abbreviated relationship string into the encoded string.
     */
    Map<String,String> relationship_lu = new HashMap<String,String>();

    /**
     * Choice for relationship to remove
     */
    JComboBox<String> relationship_cb;

    /**
     * Construct the dialog and instantiate the listeners.
     */
    public DeleteRelationshipDialog() {
      super(getRTParent(), "Add Relationship...", true);
      getContentPane().setLayout(new BorderLayout(5,5));

      // Make the dropdown box
      Iterator<String> it = active_relationships.iterator(); while (it.hasNext()) {
        String encoded_str     = it.next();
        String abbreviated_str = relationshipFromHeader(encoded_str) + " => " + relationshipToHeader(encoded_str);
        relationship_lu.put(abbreviated_str, encoded_str);
      }
      String strs[] = new String[relationship_lu.keySet().size()];
      it = relationship_lu.keySet().iterator(); for (int i=0;i<strs.length;i++) strs[i] = it.next();

      getContentPane().add("Center", relationship_cb = new JComboBox<String>(strs));

      // Add the buttons
      JPanel  buttons = new JPanel(new FlowLayout());
      JButton bt;
      buttons.add(bt = new JButton("Delete Relationship"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          String abbr = (String) relationship_cb.getSelectedItem();
          // Translate the abbreviate string into the encoded string and remove it from active relationships
          active_relationships.remove(relationship_lu.get(abbr));
          // Apply the active_relationships
          newBundlesRoot(getRTParent().getRootBundles());
          // Close the dialog and dispose
          setVisible(false); dispose();
        } } );
      buttons.add(bt = new JButton("Cancel"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
          // Close the dialog and dispose
          setVisible(false); dispose();
        } } );
      getContentPane().add("South", buttons);

      pack(); setVisible(true);
    }
  }

  /**
   * Show a dialog for adding new relationships to the graph.
   */
  public void addRelationshipDialog() { new RelationshipDialog(); }

  /**
   * Dialog for allowing user to specfic a graph edge relationship
   */
  class RelationshipDialog extends JDialog {
    /**
     * 
     */
    private static final long serialVersionUID = 1584278233303146912L;

    /**
     * Choice for the from field
     */
    JComboBox<String> from_cb, 
    /**
     * Choice for the to field
     */
                      to_cb, 
    /**
     * Choice for the symbol for the from node
     */
                      from_symbol_cb, 
    /**
     * Choice for the symbol for the to node
     */
                      to_symbol_cb,
    /**
     * Choice for the style of the node
     */
                      style_cb;
    /**
     * Checkbox to "type" the from nodes
     */
    JCheckBox from_typed_cb, 

    /**
     * Checkbox to "type" the to nodes
     */
              to_typed_cb,

    /**
     * Checkbox to ignore "notset" values in a bundle
     */
              ignore_ns_cb;

    /**
     * Construct the dialog by laying out the components and attaching listeners.
     */
    public RelationshipDialog() {
      super(getRTParent(), "Add Relationship...", true);
      getContentPane().setLayout(new BorderLayout(5,5));
      JPanel center = new JPanel(new GridLayout(5,2,5,5));
      center.add(new JLabel("From"));    
      center.add(new JLabel("To"));

      String blanks[] = nodeBlanks(getRTParent().getRootBundles().getGlobals());

      center.add(from_cb = new JComboBox<String>(blanks));
      center.add(to_cb   = new JComboBox<String>(blanks));

      center.add(from_symbol_cb = new JComboBox<String>(Utils.SHAPE_STRS));
      center.add(to_symbol_cb   = new JComboBox<String>(Utils.SHAPE_STRS));
      center.add(from_typed_cb  = new JCheckBox("Field Typed",     false));
      center.add(to_typed_cb    = new JCheckBox("Field Typed",     false));
      center.add(ignore_ns_cb   = new JCheckBox("Ignore Not Sets", true));
      getContentPane().add("Center", center);

      getContentPane().add("North",  style_cb = new JComboBox<String>(STYLE_STRS));

      JPanel bottom = new JPanel(new FlowLayout());
      JButton add_bt, cancel_bt;
      bottom.add(add_bt    = new JButton("Add"));
      bottom.add(cancel_bt = new JButton("Cancel"));
      getContentPane().add("South", bottom);

      // Add listeners
      cancel_bt.addActionListener(new ActionListener() { 
        public void actionPerformed(ActionEvent ae) { setVisible(false); dispose(); } } );
      add_bt.addActionListener(new ActionListener() { 
        public void actionPerformed(ActionEvent ae) {
          setVisible(false); dispose();
          addRelationship((String) from_cb.getSelectedItem(), 
                          (String) from_symbol_cb.getSelectedItem(),
                            from_typed_cb.isSelected(),
                          (String) to_cb.getSelectedItem(), 
                          (String) to_symbol_cb.getSelectedItem(),
                            to_typed_cb.isSelected(),
                          (String) style_cb.getSelectedItem(), 
                          ignore_ns_cb.isSelected(), 
                          false);
      } } );
      pack(); setVisible(true);
    }
  }

  /**
   * Undo save map
   */
  Map<String,Point2D> undo_map = new HashMap<String,Point2D>();

  /**
   * Save the layout for the undo operations.
   * 
   *@param map transform for nodes to points
   *@param set set to save -- if it's null or empty, save all the points
   */
  public void saveLayoutForUndo(Map<String,Point2D> map, Set<String> set) {
    // Determine what to save
    if (set == null || set.size() == 0) set = map.keySet();

    // Clear out the current settings
    undo_map.clear();

    // Save them to the undo_map
    Iterator<String> it = set.iterator(); while (it.hasNext()) {
      String node = it.next(); Point2D point = map.get(node);
      undo_map.put(node, point);
    }
  }

  /**
   * Restore the points saved in the undo buffer.
   */
  public void restoreFromUndo() {
    // Iterate over the undo map keys
    Iterator<String> it = undo_map.keySet().iterator();
    while (it.hasNext()) { 
      // Get the key and the value to replace
      String node = it.next(); Point2D point = entity_to_wxy.get(node);
     
      // Restore from the undo
      entity_to_wxy.put(node, undo_map.get(node)); 

      // But put the original back into the undo map
      undo_map.put(node, point);
    }

    // Transform and render
    transform(); getRTComponent().render();
  }

  /**
   * File chooser for saving and loading layouts.
   */
  JFileChooser file_chooser = new JFileChooser(".");

  /**
   * Save the layout of the graph to a file.
   *
   *@param selected_only only save positions for selected nodes
   *@param file          null to show popup, otherwise, use the specified file
   */
  public void saveLayout(boolean selected_only, File save_file) {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    if (save_file != null || file_chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        Set<String> sel = myrc.filterEntities(getRTParent().getSelectedEntities());
        if (selected_only && (sel == null || sel.size() == 0)) {
        } else {
          int entities_written = 0;
          System.err.println("Saving Layout (Selected Only = " + selected_only + ")...");
          if (save_file == null) save_file = file_chooser.getSelectedFile();
          PrintStream out = new PrintStream(new FileOutputStream(save_file));
          Iterator<String> it = (selected_only) ? (sel.iterator()) : (entity_to_wxy.keySet().iterator());
          while (it.hasNext()) {
            String entity = it.next();
            out.println(Utils.encToURL(entity) + "," + Utils.encToURL(""+entity_to_wxy.get(entity).getX()) + "," + Utils.encToURL(""+entity_to_wxy.get(entity).getY()));
            entities_written++;
          }
          out.close();
          System.err.println("  Done Saving Layout! (" + entities_written + " Entities Written)");
        }
      } catch (IOException ioe) {
        JOptionPane.showInternalMessageDialog(this, "Save Layout Error", "IOException : " + ioe, JOptionPane.ERROR_MESSAGE);
        System.err.println("IOException : " + ioe); ioe.printStackTrace(System.err);
      }
    }
  }

  /**
   * Load the graph layout from a file.
   *
   *@param selected_only only apply new positions to the selected entities
   */
  public void loadLayout(boolean selected_only) {
    // Get the render context
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Show the open dialog
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        // Handle the selection
        int entities_read = 0, entities_applied = 0;
        Set<String> sel = myrc.filterEntities(getRTParent().getSelectedEntities());
        if (selected_only) saveLayoutForUndo(entity_to_wxy, sel); else saveLayoutForUndo(entity_to_wxy, null);
        System.err.println("Loading Layout (Selected Only = " + selected_only + ")...");

        // Read the file
        BufferedReader in = new BufferedReader(new FileReader(file_chooser.getSelectedFile()));
        String line = in.readLine();
        while (line != null) {
          // Parse the line
          StringTokenizer st     = new StringTokenizer(line,",");
          String          entity = Utils.decFmURL(st.nextToken());
          double          wx     = Double.parseDouble(Utils.decFmURL(st.nextToken())),
                          wy     = Double.parseDouble(Utils.decFmURL(st.nextToken()));
          // If it matches an entity (and a selection is set), then set it
          if (entity_to_wxy.containsKey(entity)) {
            if (selected_only == false || sel.contains(entity)) {
              entity_to_wxy.put(entity, new Point2D.Double(wx,wy)); transform(entity);
              entities_applied++;
            }
          }
          line = in.readLine(); entities_read++;
        }
        in.close();
        System.err.println("  Done Loading Layout! (" + entities_read + " Entities Read, " + entities_applied + " Entities Applied)");
        zoomToFit(); repaint(); 
      } catch (IOException ioe) {
        JOptionPane.showInternalMessageDialog(this, "Save Layout Error", "IOException : " + ioe, JOptionPane.ERROR_MESSAGE);
        System.err.println("IOException : " + ioe); ioe.printStackTrace(System.err);
      }
    }
  }

  /**
   * Toggle the mode for the panel interactions.
   * - 2015-11-06:  only two modes are really ever used - just toggle between those...  the other modes are still in the dropdown
   */
  public void toggleMode() {
    if      (edit_rbmi.isSelected())     filter_rbmi.setSelected(true);
    // else if (filter_rbmi.isSelected())   edgelens_rbmi.setSelected(true);
    // else if (edgelens_rbmi.isSelected()) timeline_rbmi.setSelected(true);
    else                                 edit_rbmi.setSelected(true);
  }

  /**
   * Return the mode for the panel interactions.
   *
   *@return user interaction mode
   */
  public UI_MODE mode() { 
    if      (filter_rbmi.isSelected())   return UI_MODE.FILTER;
    else if (edgelens_rbmi.isSelected()) return UI_MODE.EDGELENS;
    else if (timeline_rbmi.isSelected()) return UI_MODE.TIMELINE;
    else                                 return UI_MODE.EDIT;
  }

  /**
   * Set the strict substring matches setting. Legacy method to avoid breakage with older file formats.
   *
   *@param b new setting
   */
  public void strictMatches(boolean b) { } 

  /**
   * The current world viewport for the rendering as extents.
   */
  Rectangle2D extents      = new Rectangle2D.Double(-180,-90,360,180); // For geospatial testing

  /**
   * Incremental id to keep the rendering synced with the transformations
   */
  long        transform_id = 1L;

  /**
   * Return the current width of the rendering.
   *
   *@return width
   */
  public int     getMyWidth()            { RTComponent.RTRenderContext myrc = getRTComponent().rc; if (myrc != null) return myrc.getRCWidth();  else return getRTComponent().getWidth();  }

  /**
   * Return the current height of the rendering.
   *
   *@return height
   */
  public int     getMyHeight()           { RTComponent.RTRenderContext myrc = getRTComponent().rc; if (myrc != null) return myrc.getRCHeight(); else return getRTComponent().getHeight(); }

  /**
   * Convert a world x coordinate into a screen x coordiante.
   *
   *@param wx world x coordinate
   *
   *@return screen x coordinate
   */
  public int     wxToSx  (double wx)     { return (int) (getMyWidth()  * (wx - extents.getMinX()) / extents.getWidth());  }

  /**
   * Convert a world y coordinate into a screen y coordiante.
   *
   *@param wy world y coordinate
   *
   *@return screen y coordinate
   */
  public int     wyToSy  (double wy)     { return (int) (getMyHeight() * (wy - extents.getMinY()) / extents.getHeight()); }

  /**
   * Convert a screen x coordinate into a world x coordiante.
   *
   *@param sx screen x coordinate
   *
   *@return world x coordinate
   */
  public double  sxToWx  (int    sx)     { return ((sx * extents.getWidth()) /getMyWidth())  + extents.getMinX(); }

  /**
   * Convert a screen y coordinate into a world y coordiante.
   *
   *@param sy screen y coordinate
   *
   *@return world y coordinate
   */
  public double  syToWy  (int    sy)     { return ((sy * extents.getHeight())/getMyHeight()) + extents.getMinY(); }

  /**
   * Return the correct world increment value for slowly panning the viewport.
   *
   *@return world x increment value
   */
  public double  getWxInc()              { return extents.getWidth()  /getMyWidth();  }

  /**
   * Return the correct world increment value for slowly panning the viewport.
   *
   *@return world y increment value
   */
  public double  getWyInc()              { return extents.getHeight() /getMyHeight(); }

  /**
   * Convert the world coordinates to screen coordinates for the current view for
   * a specific entity (useful when only that entity has moved).
   *
   *@param entity for this specific entity
   */
  public void    transform(String entity) {
    double wx = entity_to_wxy.get(entity).getX(),
           wy = entity_to_wxy.get(entity).getY();
    int    sx, sy;
    entity_to_sxy.put(entity, (sx = wxToSx(wx)) + BundlesDT.DELIM + (sy = wyToSy(wy)));
    entity_to_sx.put(entity, sx); entity_to_sy.put(entity, sy);
    transform_id++;
  }

  /**
   * Calculate edge crossings (actual calculation).
   */
  public EdgeCrossingsPlus calcEdgeCrossingsPlus() {
    if (ecp != null && ecp_id == transform_id) return ecp; // Already calculated for this transform

    // Find the total extents
    double x_max = Double.NEGATIVE_INFINITY, y_max = Double.NEGATIVE_INFINITY,
           x_min = Double.POSITIVE_INFINITY, y_min = Double.POSITIVE_INFINITY;
    Iterator<String> it = entity_to_wxy.keySet().iterator(); while (it.hasNext()) {
      String   node = it.next();
      Point2D  pt   = entity_to_wxy.get(node);
      if (pt.getX() > x_max) x_max = pt.getX();
      if (pt.getY() > y_max) y_max = pt.getY();
      if (pt.getX() < x_min) x_min = pt.getX();
      if (pt.getY() < y_min) y_min = pt.getY();
    }

    // Create a transform
    GraphTransform dupe_trans = new GraphTransform(512,512,new Rectangle2D.Double(x_min,y_min,x_max-x_min,y_max-y_min));

    // Calculate the edge crossings & 
    ecp = new EdgeCrossingsPlus(new UniGraph(graph), entity_to_wxy, dupe_trans);
    ecp_id = transform_id;

    return ecp;
  }

  /**
   * Edge crossings calculation
   */
  EdgeCrossingsPlus ecp;

  /**
   * Transform id associated with the edge crossings calculation
   */
  long              ecp_id;

  /**
   * Convert the world coordinates to screen coordinates for the current view for
   * all entities.
   */
  public synchronized void transform() {
    Iterator<String> it = entity_to_wxy.keySet().iterator();
    while (it.hasNext()) {
      String entity = it.next();
      double wx = entity_to_wxy.get(entity).getX(),
             wy = entity_to_wxy.get(entity).getY();
      int    sx, sy;
      entity_to_sxy.put(entity, (sx = wxToSx(wx)) + BundlesDT.DELIM + (sy = wyToSy(wy)));
      entity_to_sx.put(entity, sx); entity_to_sy.put(entity, sy);
    }
    transform_id++;
  }

  /**
   * Set the new viewport extents, force a transform across all the nodes.
   *
   *@param new_extents new viewport extents
   */
  public void   setExtents(Rectangle2D new_extents) {
    this.extents = new_extents; transform(); getRTComponent().render();
  }

  /**
   * Return the current viewport extents.
   *
   *@return current viewport extents
   */
  public Rectangle2D getExtents() { return this.extents; }

  /**
   * Zoom in by half on the current view.
   */
  public void zoomIn() { zoomIn(1); }   

  /**
   * Zoom in by the desired magnification.
   *
   *@param i magnification
   */
  public void zoomIn(double i)  {
    Rectangle2D r = getExtents();
    double exp = Math.pow(1.5,i); double cx = r.getX() + r.getWidth()/2, cy = r.getY() + r.getHeight()/2;
    setExtents(new Rectangle2D.Double(cx - r.getWidth()/(exp*2), cy - r.getHeight()/(exp*2), r.getWidth()/exp, r.getHeight()/exp));
  }

  /**
   * Zoom in by the desired magnificant leaving the specified coordinate in the same place.
   *
   *@param i      magnification
   *@param ref_cx reference x to keep in the same proportional place
   *@param ref_cy reference y to keep in the same proportional place
   */
  public void zoomIn(double i, double ref_cx, double ref_cy) {
    Rectangle2D r = getExtents();
    if (ref_cx >= r.getMinX() && ref_cx <= r.getMaxX() && ref_cy >= r.getMinY() && ref_cy <= r.getMaxY()) {
      double exp, new_width, new_height;
      if (i > 0.0) { exp = Math.pow(1.5,i);  new_width  = r.getWidth()/exp; new_height = r.getHeight()/exp; }
      else         { exp = Math.pow(1.5,-i); new_width  = r.getWidth()*exp; new_height = r.getHeight()*exp; }
      double x_perc     = (ref_cx - r.getMinX())/(r.getMaxX() - r.getMinX()),
             y_perc     = (ref_cy - r.getMinY())/(r.getMaxY() - r.getMinY());
      double new_xmin   = ref_cx - x_perc*new_width,
             new_ymin   = ref_cy - y_perc*new_height;
      setExtents(new Rectangle2D.Double(new_xmin, new_ymin, new_width, new_height));
    } else zoomIn(i);
  }

  /**
   * Zoom out by half of the current view.
   */
  public void zoomOut() { zoomOut(1); } public void zoomOut(int i) {
    Rectangle2D r = getExtents();
    double exp = Math.pow(1.5,i); double cx = r.getX() + r.getWidth()/2, cy = r.getY() + r.getHeight()/2;
    setExtents(new Rectangle2D.Double(cx - r.getWidth()*exp/2, cy - r.getHeight()*exp/2, r.getWidth()*exp, r.getHeight()*exp));
  }

  /**
   * Zoom to fit all the nodes.
   */
  public void zoomToFit() { zoomToFit(null); }

  /**
   * Zoom to fit all the nodes.
   *
   *@param myrc render context to use for the calculation
   */
  public void zoomToFit(RTGraphComponent.RenderContext myrc) {
    RTGraphComponent.RenderContext original_myrc = myrc;
    // Get the render context
    if (myrc == null) myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    // Go through the entities...  may be faster to iterate over visible entities...
    Iterator<String> it = entity_to_wxy.keySet().iterator(); if (it.hasNext() == false) return;
    double x0 = Double.POSITIVE_INFINITY, y0 = Double.POSITIVE_INFINITY,
           x1 = Double.NEGATIVE_INFINITY, y1 = Double.NEGATIVE_INFINITY;
    // Check bounds for each one and adjust mins/maxes as appropriate
    while (it.hasNext()) { 
      String str = it.next(); if (myrc.visible_entities.contains(str) == false) continue;
      if (x0 > entity_to_wxy.get(str).getX()) x0 = entity_to_wxy.get(str).getX();
      if (y0 > entity_to_wxy.get(str).getY()) y0 = entity_to_wxy.get(str).getY();
      if (x1 < entity_to_wxy.get(str).getX()) x1 = entity_to_wxy.get(str).getX();
      if (y1 < entity_to_wxy.get(str).getY()) y1 = entity_to_wxy.get(str).getY();
    }
    // Validate the output
    if (Double.isInfinite(x0) || Double.isInfinite(y0)) return;
    // Give it a border
    if (x1 == x0) x1 = x0 + 0.5; if (y1 == y0) y1 = y0 + 0.5;
    double xp = (x1 - x0)*0.05, yp = (y1 - y0)*0.05;
    // Transform and redraw
    if (getGraphBG() == GraphBG.GEO_OUT || getGraphBG() == GraphBG.GEO_FILL || getGraphBG() == GraphBG.GEO_TOUCH) {
      if (x0 > -180) { x0 = -180; xp = 0.0; }
      if (y0 > -90)  { y0 = -90;  yp = 0.0; }
      if (x1 <  180) { x1 =  180; xp = 0.0; }
      if (y1 <  90)  { y1 =  90;  yp = 0.0; }
    }
    extents = new Rectangle2D.Double(x0-xp,y0-yp,x1-x0+2*xp,y1-y0+2*yp);
    transform(); if (original_myrc == null) getRTComponent().render();
  }

  /**
   * Shift the current selection nodes by the specified amount.  Force a transform on those nodes.
   *
   *@param d_sx amount to shift in x direction
   *@param d_sy amount to shift in y direction
   */
  public void shiftSelection(int d_sx, int d_sy) {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    Set<String> sel = myrc.filterEntities(getRTParent().getSelectedEntities());
    if (sel != null && sel.size() > 0) {
      saveLayoutForUndo(entity_to_wxy, sel);
      Iterator<String> it = sel.iterator();
      double x_inc = getWxInc(),
             y_inc = getWyInc();
      while (it.hasNext()) {
        String entity = it.next();
        if (entity_to_wxy.containsKey(entity)) {
          Point2D point = entity_to_wxy.get(entity);
          entity_to_wxy.put(entity, new Point2D.Double(point.getX() + d_sx * x_inc,
                                                       point.getY() + d_sy * y_inc));
          transform(entity);
        }
      }
      getRTComponent().render();
    }
  }

  /**
   * Copy the selected item names to the clipboard.
   */
  public void copySelection() {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    Clipboard        clipboard = getToolkit().getSystemClipboard();
    StringBuffer     sb        = new StringBuffer();
    Iterator<String> it        = (myrc.filterEntities(getRTParent().getSelectedEntities())).iterator();
    while (it.hasNext()) sb.append(it.next() + "\n");
    StringSelection selection = new StringSelection(sb.toString());
    clipboard.setContents(selection, null);
  }

  /**
   * Select nodes based on the contents of the clipboard.
   */
  public void selectFromClipboard() { 
    // Render context for visible entities
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;

    // Selection operation
    getRTParent().setSelectedEntities(Selection.selectClipboard(myrc.visible_entities, Utils.getClipboardText(this)));
  }

  /**
   * Copy the labels as a table.  Put them onto the clipboard in a format that can be pasted into a spreadsheet.
   */
  public void copyLabelsAsTable() {
    RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
    if (myrc.node_lm != null) {
      StringBuffer sb = new StringBuffer();

      String header[] = myrc.node_lm.labelsArray();
      for (int i=0;i<header.length;i++) { if (i > 0) { sb.append(","); } sb.append(Utils.makeRFC4180SafeString(header[i])); } sb.append("\n");

      int entity_i = -1; for (int i=0;i<header.length;i++) if (header[i].equals(ENTITY_LM)) entity_i = i;

      Iterator<String> it_bin = myrc.node_counter_context.binIterator(); while (it_bin.hasNext()) {
        String bin      = it_bin.next();
        String values[] = myrc.node_lm.toStrings(bin);

        String entity   = null; if (entity_i >= 0) entity = values[entity_i];

        if        ((entity != null && getRTParent().getSelectedEntities().contains(entity)) ||  // There's an entity and it's in the selected set
                   (entity == null)                                                         ||  // There's not an entity (label not selected?)
                   (                  getRTParent().getSelectedEntities().size() == 0)) {       // There aren't any selected entities... copy them all
          for (int i=0;i<values.length;i++) { if (i > 0) { sb.append(","); } sb.append(Utils.makeRFC4180SafeString(values[i])); } sb.append("\n");
        }
      }

      Clipboard clipboard = getToolkit().getSystemClipboard();
      StringSelection selection = new StringSelection(sb.toString());
      clipboard.setContents(selection, null);
    }
  }

  /**
   * Component to handle the painting of the rendering and interaction with the
   * user to manipulate nodes/views.
   */
  public class RTGraphComponent extends RTComponent {
    /**
     * 
     */
    private static final long serialVersionUID = -2088687525263801720L;

    /**
     * Entities that should always display node labels
     */
    Set<String> sticky_labels = new HashSet<String>();

    /**
     * Flag to overlay help on the screen for keyboard shortcuts
     */
    boolean         draw_help     = false;

    /**
     * Copy the current screen rendering to the clipboard as an image.
     * Does not seem to work across platforms.
     *
     *@param shft shift-key down
     *@param alt  alternate-key down
     */
    @Override
    public void copyToClipboard    (boolean shft, boolean alt) {
      RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) getRTComponent().rc;
      if      (shft == false && alt  == false) copySelection();
      else if (shft == true  && myrc != null)  Utils.copyToClipboard(myrc.getBase());
    }

    /**
     * Select the nodes from the clipboard that match.
     *
     *@param shft shift key down
     *@param alt  alternate key down
     */
    @Override
    public void pasteFromClipboard (boolean shft, boolean alt) {
      if (shft == false && alt == false) selectFromClipboard();
    }

    /**
     * Describe the data in the shape.
     *
     *@param shape shape to intersect with rendered geometries
     *
     *@return json-formatted string describing what is in the specified shape
     */
    @Override
    public String describeDataInShape(Shape shape) { 
      StringBuffer nodes_sb = null,
                   edges_sb = null;

      RenderContext myrc = (RenderContext) rc; if (myrc != null) {
        Rectangle bounds = shape.getBounds();
        /*
        int       x0     = (int) bounds.getX(), x1     = (int) (bounds.getX() + bounds.getWidth()),
                  y0     = (int) bounds.getY(), y1     = (int) (bounds.getY() + bounds.getHeight());
        */
        // Go through the nodes first... those are easier
        Set<String> nodes = new HashSet<String>();

        Iterator<String> it          = myrc.node_coord_set.keySet().iterator();
        while (it.hasNext()) {
          String node_coord = it.next(); 
          if (myrc.node_to_geom.containsKey(node_coord) && bounds.intersects(myrc.node_to_geom.get(node_coord).getBounds())) {
            nodes.addAll(myrc.node_coord_set.get(node_coord));
          }
        }

        nodes_sb = new StringBuffer();
        nodes_sb.append("\"nodes\":[");
        it = nodes.iterator(); boolean first = true; 
        while (it.hasNext()) { if (!first) nodes_sb.append(","); nodes_sb.append("\"" + it.next() + "\""); first = false; }
        nodes_sb.append("]");

        // Next edges...
        Set<String> edges = new HashSet<String>();
        it = myrc.link_counter_context.binIterator(); while (it.hasNext()) {
          String link = it.next(); Line2D line = myrc.link_to_line.get(link); if (line.intersects(bounds)) {
            Set<String> graphedgerefs = myrc.link_to_graphedgerefs.get(link);
            Iterator<String> it_ger = graphedgerefs.iterator(); while (it_ger.hasNext()) {
              String graph_edge_ref = it_ger.next();
              int fm_i = digraph.linkRefFm(graph_edge_ref);  String fm = digraph.getEntityDescription(fm_i);
              int to_i = digraph.linkRefTo(graph_edge_ref);  String to = digraph.getEntityDescription(to_i);
              if (nodes.contains(fm) == false && nodes.contains(to) == false) { // don't bother with edges that had a corresponding node that was covered
                edges.add(fm + " | " + to);
              }
            }
          }
        }

        edges_sb = new StringBuffer();
        edges_sb.append("\"edges\":[");
        it = edges.iterator(); first = true;
        while (it.hasNext()) { if (!first) edges_sb.append(","); edges_sb.append("\"" + it.next() + "\""); first = false; }
        edges_sb.append("]");
      }

      StringBuffer sb = new StringBuffer();
        sb.append("{");
          // Add nodes if they were set
          if (nodes_sb != null) sb.append(nodes_sb.toString());
          // Add the edges
          if (edges_sb != null) {
            if (sb.length() != 1) sb.append(",");
            sb.append("," + edges_sb.toString());
          } else {
            if (sb.length() != 1) sb.append(",");
            sb.append(",\"edges\":\"not_implemented\"");
          }
        sb.append("}");
      return sb.toString();
    }

    /**
     * Create a high resolution version of the currently rendered graph and
     * save it to a file.
     *
     *@param scale         scale from the current screen resolution
     *@param base_filename base filename
     */
    protected void createHighResolutionExport(float scale, String base_filename) {
      int screen_w = getWidth(),               screen_h = getHeight();
      int hires_w  = (int) (scale * screen_w), hires_h  = (int) (scale * screen_h);

      Rectangle2D.Double render_extents = new Rectangle2D.Double(extents.getX(), extents.getY(), extents.getWidth()*scale, extents.getHeight()*scale);
      // Rectangle2D.Double render_extents = new Rectangle2D.Double(Math.random(), Math.random(), 10.0*Math.random(), 10.0*Math.random());

      Bundles bs = getRenderBundles();
      String count_by = getRTParent().getCountBy(); // , color_by = getRTParent().getColorBy();
      if (bs != null && count_by != null) {
        RenderContext myrc = new RenderContext((short) -1, bs, render_extents, getRTParent().getColorBy(), getRTParent().getCountBy(), 
                                               getNodeColor(), getNodeSize(), getLinkColor(), getLinkSize(), multilineTruncate(), multilineThreshold(),
                                               drawArrows(), linksTransparent(), linkCurves(), linkColorParts(),
                                               drawTiming(), drawNodeLabels(), drawLinkLabels(), listEntityLabels(), listEntityColor(), listBundleLabels(), getGraphBG(),
                                               drawEdgeTemplates(), drawNodeLegend(), hires_w, hires_h, (RenderContext) rc);
        BufferedImage scale_bi = myrc.getBase();
        try { ImageIO.write(scale_bi, "PNG", new FileOutputStream(new File(base_filename + "_" + System.currentTimeMillis() + ".png")));
        } catch (FileNotFoundException fnfe) {
          JOptionPane.showMessageDialog(this, "FileNotFoundException: " + fnfe, "File Not Found", JOptionPane.ERROR_MESSAGE);
        } catch (IOException           ioe) {
          JOptionPane.showMessageDialog(this, "IOException: " + ioe, "IO Exception", JOptionPane.ERROR_MESSAGE);
        }
      }
    }

    /**
     * Return the set of all shapes on the screen.
     *
     *@return set of all rendered shapes
     */
    public Set<Shape>      allShapes()                     { 
      RenderContext myrc = (RenderContext) rc;
      if (myrc == null) return new HashSet<Shape>();
      else              return myrc.all_shapes;            
    }

    /**
     * For the set of bundles, return the corresponding set of shapes.
     *
     *@param bundles records to correlate
     *
     *@return set of corresponding shapes
     */
    public Set<Shape>  shapes(Set<Bundle> bundles) { 
      RenderContext  myrc = (RenderContext) rc;
      Set<Shape> set  = new HashSet<Shape>();
      if (myrc == null) return set;
      Iterator<Bundle> it  = bundles.iterator();
      while (it.hasNext()) {
        Bundle bundle = it.next();
        if (myrc.bundle_to_shapes.containsKey(bundle)) set.addAll(myrc.bundle_to_shapes.get(bundle));
      }
      return set;
    }

    /**
     * Calculate the highlighted bundles based on the current position of the mouse.  Needs to be
     * improved to handle neighboring edges for first and second order derivatives.  FEATURE
     *
     *@param me            mouse event
     *@param highlights    records directly under the mouse
     *@param highlights_p  records near the mouse
     *@param highlights_pp records further from the mouse (but still close)
     */
    public void calculateHighlights(MouseEvent me, Set<Bundle> highlights, Set<Bundle> highlights_p, Set<Bundle> highlights_pp) {
      // The problem with this approach is that the screen rep is not equivalent to the graph rep
      // - for example, when multiple nodes are combined into a single node...
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
      if (getRTParent().highlight())            { 
        boolean first_order  = getRTParent().highlightFirstOrder(),
                second_order = getRTParent().highlightSecondOrder();
        Shape shape = graphGeometryAt(me.getX(),me.getY());
        if        (shape == null)           {
        } else if (shape instanceof Line2D) { 
          String          link          = myrc.line_to_link.get(shape);
          if (link          == null) throw new RuntimeException("Link Null - link_to_link.get()");
          highlights.addAll(myrc.link_counter_context.getBundles(link));

          if (first_order) {
            System.err.println("RTGraph:  1st Order HL - Not Impl");
            Set<String> graphedgerefs = myrc.link_to_graphedgerefs.get(link);
            if (graphedgerefs == null) throw new RuntimeException("GraphEdgeRefs Null");
            Iterator<String> it = graphedgerefs.iterator();
            while (it.hasNext()) {
              /* String graphedgeref = */ it.next();
              // linkrefs.add(graphedgeref_to_link.get(graphedgeref));
            }
          }
          if (second_order) { System.err.println("RTGraph:  2nd Order HL - Not Impl");
          }
        } else {
          highlights.addAll(myrc.geom_to_bundles.get(shape));
          if (first_order)  { System.err.println("RTGraph:  1st Order HL - Not Impl"); }
          if (second_order) { System.err.println("RTGraph:  2nd Order HL - Not Impl"); }
        }
      }
    }

    /**
     * Find the graph geometry at the specified screen location
     * - Give priority to the nodes
     * - If a node doesn't match, check the lines for a closest match.
     *
     *@param  x x coordinate
     *@param  y y coordinate
     *
     *@return most appropriate shape under the mouse position
     */
    public Shape graphGeometryAt(int x, int y) {
      RenderContext myrc = (RenderContext) rc;
      if (myrc == null) return null;
      Iterator<Shape> it_shape = myrc.geom_to_bundles.keySet().iterator();
      while (it_shape.hasNext()) { Shape shape = it_shape.next();
                                   if (shape.contains(x,y)) return shape; }
      if (myrc.line_to_bundles.keySet().size() == 0) return null;
      Iterator<Line2D> it_line = myrc.line_to_bundles.keySet().iterator();
      Line2D closest_line = it_line.next(); double closest_distance = LineIntersection.distanceFromPointToLineSegment(x,y, closest_line);
      while (it_line.hasNext()) {
        Line2D line =it_line.next();
        double dist = LineIntersection.distanceFromPointToLineSegment(x,y, line);
        if (dist < closest_distance) {
          closest_line = line; closest_distance = dist;
        }
      }
      if (closest_distance < 10.0) return closest_line; else return null;
    }

    /**
     * For a specific shape, return the corresponding bundles.
     *
     *@param  shape specified  shape
     *
     *@return set of records corresponding to the specified shape
     */
    public Set<Bundle> shapeBundles(Shape shape)       { 
      RenderContext myrc = (RenderContext) rc;
      if (myrc == null) return new HashSet<Bundle>();
      if      (myrc.line_to_bundles.containsKey(shape)) return myrc.line_to_bundles.get(shape);
      else if (myrc.geom_to_bundles.containsKey(shape)) return myrc.geom_to_bundles.get(shape);
      else return new HashSet<Bundle>(); }

    /**
     * Determine which shapes overlap with the specified shape.  The specified shape
     * can be generic (i.e., it doesn't have to be created as part of the rendering process).
     *
     *@param shape shape to check for overlaps
     *
     *@return set of overlapping, rendered shapes
     */
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      RenderContext myrc = (RenderContext) rc;
      if (myrc == null) return new HashSet<Shape>();
      Set<Shape> set = new HashSet<Shape>();  
      Iterator<Shape> its = myrc.geom_to_bundles.keySet().iterator();
      while (its.hasNext()) {
        Shape its_shape = its.next();
        if (Utils.genericIntersects(shape, its_shape)) set.add(its_shape);
      }
      Iterator<Line2D> itl = myrc.line_to_bundles.keySet().iterator();
      while (itl.hasNext()) {
         Line2D line = itl.next(); 
         if (line.contains(shape.getBounds())) set.add(line);
      }
      return set;
    }

    /**
     * Flag for rendering edge templates
     */
    boolean edge_template_flag = true;

    /**
     * Return true to draw the edge templates.
     *
     *@return true for edge templates
     */
    public boolean drawEdgeTemplates() { return edge_template_flag; }

    /**
     * Set the draw edge templates legend.
     *
     *@param b true to draw the edge templates legend
     */
    public void drawEdgeTemplates(boolean b) { edge_template_flag = b; }

    /**
     * Flag for rendering the node legend
     */
    boolean node_legend_flag = true;

    /**
     * Return true if node legend should be rendered.
     *
     *@return true for node legend
     */
    public boolean drawNodeLegend() { return node_legend_flag; }

    /**
     * Set the draw node legend flag.
     *
     *@param b true to draw the node leged
     */
    public void drawNodeLegend(boolean b) { node_legend_flag = b; }

    /**
     * Render the current configurations/bundle set.  Use a render id to abort unnecessary renderings.
     *
     *@param id render id
     */
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Don't draw if not visible...
      if (isVisible() == false) { repaint(); return null; }
      // Get the parameters
      Bundles bs = getRenderBundles();
      String count_by = getRTParent().getCountBy(); // , color_by = getRTParent().getColorBy();
      if (bs != null && count_by != null) {
        RenderContext myrc = new RenderContext(id, bs, extents, getRTParent().getColorBy(), getRTParent().getCountBy(), 
                                               getNodeColor(), getNodeSize(), getLinkColor(), getLinkSize(),  multilineTruncate(), multilineThreshold(),
                                               drawArrows(), linksTransparent(), linkCurves(), linkColorParts(),
                                               drawTiming(), drawNodeLabels(), drawLinkLabels(), listEntityLabels(), listEntityColor(), listBundleLabels(), getGraphBG(),
                                               drawEdgeTemplates(), drawNodeLegend(), getWidth(), getHeight(), (RenderContext) rc);
        // Recalculate the bounds if checked
        if (recalc_bounds_cbmi.isSelected()) { 
          zoomToFit(myrc); 
          myrc = new RenderContext(id, bs, extents, getRTParent().getColorBy(), getRTParent().getCountBy(), 
                                   getNodeColor(), getNodeSize(), getLinkColor(), getLinkSize(), multilineTruncate(), multilineThreshold(),
                                   drawArrows(), linksTransparent(), linkCurves(), linkColorParts(),
                                   drawTiming(), drawNodeLabels(), drawLinkLabels(), listEntityLabels(), listEntityColor(), listBundleLabels(), getGraphBG(),
                                   drawEdgeTemplates(), drawNodeLegend(), getWidth(), getHeight(), (RenderContext) rc);
        }
        return myrc;
      }
      return null;
    }

    /**
     * Expand the currently selected node.
     *
     * Modifiers:
     * - Nothing   -- expand one edge
     * - Shft      -- expand using the directed graph
     * - Ctrl      -- select intersection of the selected nodes
     * - Shft-Ctrl -- select intersection of the selected nodes / exclusive
     *
     *@param shift shift key pressed
     *@param ctrl  control key pressed
     */
    public void expandSelection(boolean shift, boolean ctrl) {
      RenderContext   myrc    = (RenderContext) rc; if (myrc == null) return;
      Set<String>     sel     = myrc.filterEntities(getRTParent().getSelectedEntities()),
                      new_sel = new HashSet<String>();

      //
      // Expansion...
      //
      if ((shift == false && ctrl == false) || (shift == true && ctrl == false)) {
        MyGraph         to_use  = (shift) ? digraph : graph;
        if (sel != null && sel.size() > 0) {
          Iterator<String> it = sel.iterator();
          while (it.hasNext()) {
            String entity = it.next();
            new_sel.add(entity);
            int entity_i = to_use.getEntityIndex(entity);
            for (int i=0;i<to_use.getNumberOfNeighbors(entity_i);i++) {
              String nbor = to_use.getEntityDescription(to_use.getNeighbor(entity_i, i));
              if (myrc.visible_entities.contains(nbor)) new_sel.add(nbor);
            }
          }
        }

      //
      // Intersection...
      //
      } else if (sel != null && sel.size() > 1) {
        Set<String> set = new HashSet<String>();
        Iterator<String> it = sel.iterator(); String node = it.next(); int node_i = graph.getEntityIndex(node);
        for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) set.add(graph.getEntityDescription(graph.getNeighbor(node_i,i)));

        Set<String> next_set;
        while (it.hasNext()) {
          node = it.next(); node_i = graph.getEntityIndex(node); next_set = new HashSet<String>();
          for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) next_set.add(graph.getEntityDescription(graph.getNeighbor(node_i,i)));
          set.retainAll(next_set);
        }

        // Exclusive?
        if (shift == true && ctrl == true) {
          Set<String> to_keep = new HashSet<String>();
          it = set.iterator(); while (it.hasNext()) {
            node = it.next(); node_i = graph.getEntityIndex(node); boolean keep = true;
            if (graph.getNumberOfNeighbors(node_i) == sel.size()) {
              for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) 
                if (sel.contains(graph.getEntityDescription(graph.getNeighbor(node_i,i))) == false) 
                  keep = false;
            } else keep = false;
            if (keep) to_keep.add(node);
          }
          set = to_keep;
        }

        new_sel = set;
      }

      getRTParent().setSelectedEntities(new_sel, reportEntities());
      repaint();
    }

    /**
     * Select the opposite set of nodes from the currently visible nodes.
     */
    public void invertSelection() {
      RenderContext myrc    = (RenderContext) rc; if (myrc == null) return;
      Set<String>   sel     = myrc.filterEntities(getRTParent().getSelectedEntities());
      Set<String>   new_sel = new HashSet<String>();
      if (myrc != null && sel != null) {
        Iterator<String> it = myrc.visible_entities.iterator();
        while (it.hasNext()) {
          String str = it.next();
          if (sel.contains(str)) { } else new_sel.add(str);
        }
      }
      getRTParent().setSelectedEntities(new_sel, reportEntities());
      repaint();
    }

    /**
     * Select all of the entities with the same shape as the one under the mouse.  Shift, Ctrl, and Shift-Ctrl
     * variations apply.
     */
    public void selectShapeUnderMouse() {
      RenderContext myrc    = (RenderContext) rc; if (myrc == null) return;

      // Figure what's under the mouse
      Set<String>  under_mouse = underMouse(m_x, m_y); if (under_mouse == null || under_mouse.size() == 0) return;

      // Determine those shapes
      Set<Utils.Symbol> symbols = new HashSet<Utils.Symbol>();
      Iterator<String> it = under_mouse.iterator(); while (it.hasNext()) {
        String entity = it.next(); 
        Utils.Symbol symbol = entity_to_shape.get(entity);
        symbols.add(symbol);
      }

      // Figure out the all the other entities that have those shapes
      Set<String> new_sel = new HashSet<String>();
      for (int i=0;i<graph.getNumberOfEntities();i++) {
        String entity = graph.getEntityDescription(i);
        if (symbols.contains(entity_to_shape.get(entity))) new_sel.add(entity);
      }

      // Only use visible
      new_sel = myrc.filterEntities(new_sel);

      // Execute the set operation
      setOperation(new_sel);
    }

    /**
     * Select all of the entities with the same color as the one under the mouse.  Shift, Ctrl, and Shift-Ctrl
     * variations apply.
     */
    public void selectColorUnderMouse() {
      RenderContext myrc    = (RenderContext) rc; if (myrc == null) return;
      NodeColor node_color = getNodeColor();
      if (node_color == NodeColor.VARY || node_color == NodeColor.LABEL) {
        myrc.selectColorUnderMouse(node_color, m_x, m_y);
      } else System.err.println("selectColorUnderMouse():  Only works with LABEL or VARY");
    }

    /**
     * Select nodes with a specific degree.  Note that this method uses the underlying graph
     * for the selection -- not the currently displayed number of neighbors.
     *
     *@param degree degree to select
     */
    public void selectNodesByDegree(int degree) {
      Set<String> new_sel = new HashSet<String>();

      for (int i=0;i<graph.getNumberOfEntities();i++) {
        if (graph.getNumberOfNeighbors(i) == degree) 
          new_sel.add(graph.getEntityDescription(i));
      }

      // Attempt to filter by visible
      RenderContext myrc    = (RenderContext) rc; 
      if (myrc == null) setOperation(new_sel);
      else              setOperation(myrc.filterEntities(new_sel));
    }

    /**
     * Run localized clean-up scripts.
     */
    public void localizedCleanUp(boolean shft, boolean alt, boolean ctrl) {
      if (shft == false) {
        if (alt  == false) {
          if (ctrl == false) { cleanUpOneDegrees();
          } else             { 

            saveLayoutForUndo(entity_to_wxy, null);

            RenderContext  myrc = (RenderContext) rc; if (myrc == null) return;
            Set<String>    sel  = myrc.filterEntities(getRTParent().getSelectedEntities());
            (new GraphLayouts()).fixTwoColumns(new UniGraph(graph), sel, entity_to_wxy, true);

            transform(); getRTComponent().render();

          }
        } else             {
          nodeColorTreeMapLayout();
        }
      } else             {

        saveLayoutForUndo(entity_to_wxy, null);

        RenderContext  myrc = (RenderContext) rc; if (myrc == null) return;
        Set<String>    sel  = myrc.filterEntities(getRTParent().getSelectedEntities());
        (new GraphLayouts()).fixTwoColumns(new UniGraph(graph), sel, entity_to_wxy, false);

        transform(); getRTComponent().render();

      }
    }

    /**
     * Clean up the selected nodes.
     */
    public void cleanUpOneDegrees() {
      // Copy the selection
      RenderContext myrc    = (RenderContext) rc; if (myrc == null) return;
      Set<String>     sel         = myrc.filterEntities(getRTParent().getSelectedEntities());

      // If something's selected, run the cleanup and re-render
      if (sel.size() > 0) {
        saveLayoutForUndo(entity_to_wxy, null);
        (new GraphLayouts()).layoutOneDegreeNeighbors(new UniGraph(graph), sel, entity_to_wxy);
        transform(); getRTComponent().render();
      }
    }

    /**
     * Select non-trivial cut vertices.  Useful for arranging the graph by blocks.
     */
    public void selectCutVertices() {
      BiConnectedComponents bcc_2p = graph2p_bcc;
      if (bcc_2p != null) {
        Set<String> new_sel = new HashSet<String>();
        new_sel.addAll(bcc_2p.getCutVertices());
        setOperation(new_sel);
      }
    }

    /**
     * Select nodes with a specific degree or higher.
     *
     *@param degree minmum degree necessary
     */
    public void selectNodesWithDegreeAtLeast(int degree) {
      Set<String> new_sel = new HashSet<String>();
      for (int i=0;i<graph.getNumberOfEntities();i++) {
        if (graph.getNumberOfNeighbors(i) >= degree) new_sel.add(graph.getEntityDescription(i));
      }
      setOperation(new_sel);
    }

    /**
     * Remove the selected nodes (and their associated bundles) from the visible set.
     */
    public void filterOutSelection() {
      RenderContext   myrc        = (RenderContext) rc; if (myrc == null) return;
      Set<String>     sel         = myrc.filterEntities(getRTParent().getSelectedEntities());
      if (sel == null || sel.size() == 0) { getRTParent().pop(); repaint(); return; }
      Set<Bundle> new_bundles = new HashSet<Bundle>();
      if (sel != null && sel.size() > 0) {
        new_bundles.addAll(myrc.bs.bundleSet());
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) new_bundles.removeAll(myrc.entity_counter_context.getBundles(it.next()));

        // Construct the new bundle structure...
        Bundles new_bundles_struct = myrc.bs.subset(new_bundles);

        StringBuffer sb = new StringBuffer();
        sb.append("{\"operation\":\"filterOutSelection\",");
         sb.append("\"RTPanel\":\"" + getWinType() + "\",");
         sb.append("\"entities\":[");
         it = sel.iterator(); while (it.hasNext()) {
           sb.append("\"" + it.next() + "\"");
           if (it.hasNext()) sb.append(",");
         }
        sb.append("]}"); 

        new_bundles_struct.setSliceDescription(sb.toString());
        getRTParent().push(new_bundles_struct);

        getRTParent().setSelectedEntities(new HashSet<String>());
        repaint();
      }
    }

    /**
     * Scale the current selection by the specified multiple value.  Values greater than one
     * expand the positions while those less than one contract the nodes.  The mouse point is
     * used as the center for the transformation.
     *
     *@param mult multiple value to use for expansion or contraction
     */
    public void scaleSelection(double mult) {
      RenderContext   myrc        = (RenderContext) rc; if (myrc == null) return;
      Set<String>     sel         = myrc.filterEntities(getRTParent().getSelectedEntities());
      if (sel != null && sel.size() > 0) {
        saveLayoutForUndo(entity_to_wxy, sel);
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) { 
          String ent = it.next(); Point2D point = entity_to_wxy.get(ent);
          double px    = point.getX(), py    = point.getY(),
                 dx    = px - m_wx,    dy    = py - m_wy;
          double dist  = Math.sqrt((px - m_wx)*(px - m_wx) + (py - m_wy)*(py - m_wy));
          if (dist > 0.0001) {
            double ndx = dx/dist, ndy = dy/dist;
            double nx  = px + mult * ndx,
                   ny  = py + mult * ndy;
            entity_to_wxy.put(ent, new Point2D.Double(nx,ny));
            transform(ent); 
          }
        }
        getRTComponent().render();
      }
    }

    /**
     * Rotate the current selection by the specified angle value.  The mouse point is
     * used as the center for the transformation.
     *
     *@param degrees degrees to rotate the nodes
     */
    public void rotateSelection(double degrees) {
      RenderContext myrc    = (RenderContext) rc; if (myrc == null) return;
      double        radians = (2*Math.PI)/(360/degrees);
      Set<String>   sel     = myrc.filterEntities(getRTParent().getSelectedEntities());
      if (sel != null && sel.size() > 0) {
        saveLayoutForUndo(entity_to_wxy, sel);
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) { 
          String ent = it.next(); Point2D point = entity_to_wxy.get(ent);
          double px    = point.getX(), py    = point.getY();
          double dist  = Math.sqrt((px - m_wx)*(px - m_wx) + (py - m_wy)*(py - m_wy));
          double angle = Utils.direction(px - m_wx, py - m_wy);
          double nx    = m_wx + dist * Math.cos(angle + radians),
                 ny    = m_wy + dist * Math.sin(angle + radians);
          entity_to_wxy.put(ent, new Point2D.Double(nx,ny));
          transform(ent); 
        }
        getRTComponent().render();
      }
    }

    /**
     * For the selected entities, assign them to the last location of the mouse.
     * force a transform and re-render.
     */
    public void centerSelection() {
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
      Set<String>   sel  = myrc.filterEntities(getRTParent().getSelectedEntities());
      if (sel != null && sel.size() > 0) {
        saveLayoutForUndo(entity_to_wxy, sel);
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) { 
          String ent = it.next(); Point2D point = entity_to_wxy.get(ent);
          if      (last_shft_down) entity_to_wxy.put(ent, new Point2D.Double(point.getX(),m_wy)); 
          else if (last_ctrl_down) entity_to_wxy.put(ent, new Point2D.Double(m_wx,point.getY()));
          else                     entity_to_wxy.put(ent, new Point2D.Double(m_wx,m_wy));
          transform(ent); 
        }
        getRTComponent().render();
      }
    }

    /**
     * Function key saves for the coordinates
     */
    Map<Integer,Map<String,Point2D>> function_key_saves = new HashMap<Integer,Map<String,Point2D>>();

    /**
     * Save the layout based on a function key press.  Shift and control will be used as follows:
     * - shift | control | results
     * - no    | no      | save the layout
     * - yes   | *       | restore the layout
     * - no    | yes     | delete layout copy
     *
     *@param key   key to save/restore the layout from
     *@param shft  indicates layout should be restored
     *@param ctrl  indicates layout should be erased
     */
    protected void saveLayoutByFunctionKey(int fn_key, boolean shft, boolean ctrl) {
      if        (shft && function_key_saves.containsKey(fn_key)) {
        //
        // Restore the save
        //
        saveLayoutForUndo(entity_to_wxy, null);
        System.err.println("RTGraphPanel: Restoring Layout From KeyCode " + fn_key);
        Iterator<String> it = function_key_saves.get(fn_key).keySet().iterator();
        while (it.hasNext()) {
          String k = it.next();
          if (entity_to_wxy.keySet().contains(k)) entity_to_wxy.put(k, function_key_saves.get(fn_key).get(k));
        }
        RenderContext myrc = (RenderContext) rc;
        transform(); if (myrc != null) getRTComponent().render();

      } else if (ctrl && function_key_saves.containsKey(fn_key)) { 
        //
        // Remove the previously saved coordinates
        //
        System.err.println("RTGraphPanel: Deleting Layout From KeyCode " + fn_key);
        function_key_saves.remove(fn_key);

      } else                                                  {
        //
        // Save off the coordinates
        //
        System.err.println("RTGraphPanel:  Saving Layout To KeyCode " + fn_key);
        function_key_saves.put(fn_key, new HashMap<String,Point2D>());
        Iterator<String> it = entity_to_wxy.keySet().iterator(); 
        while (it.hasNext()) {
          String k = it.next();
          function_key_saves.get(fn_key).put(k, entity_to_wxy.get(k));
        }
      }
    }

  /**
   *
   */
  public void mousePressed    (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mousePressed(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE || me.getButton() == MouseEvent.BUTTON2) genericMouse(ME_ENUM.PRESSED,  me); }
  public void mouseReleased   (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mouseReleased(me); 
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE) genericMouse(ME_ENUM.RELEASED, me); }
  public void mouseClicked    (MouseEvent me) { if (mode() == UI_MODE.FILTER || me.getButton() == MouseEvent.BUTTON3) super.mouseClicked(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE || me.getButton() == MouseEvent.BUTTON2) genericMouse(ME_ENUM.CLICKED,  me); }
  public void mouseMoved      (MouseEvent me) { if (mode() == UI_MODE.FILTER || mode() == UI_MODE.EDGELENS || mode() == UI_MODE.TIMELINE) super.mouseMoved(me);    
                                                if (mode() == UI_MODE.EDIT)   genericMouse(ME_ENUM.MOVED,    me); }
  public void mouseDragged    (MouseEvent me) { if (mode() == UI_MODE.FILTER || mdrag) super.mouseDragged(me);  
                                                if (mode() == UI_MODE.EDIT   || ui_inter != UI_INTERACTION.NONE) genericMouse(ME_ENUM.DRAGGED,  me); }
  public void mouseExited     (MouseEvent me) { super.mouseExited(me);  }
  public void mouseEntered    (MouseEvent me) { super.mouseEntered(me); grabFocus(); }
  public void keyPressed      (KeyEvent   ke) { super.keyPressed(ke);   
                                                int mover = 1; if (last_shft_down) mover *= 5; if (last_ctrl_down) mover *= 20;
                                                if      (ke.getKeyCode() == KeyEvent.VK_M)       toggleMode();
                                                else if (ke.getKeyCode() == KeyEvent.VK_E)       expandSelection(last_shft_down, last_ctrl_down);
                                                else if (ke.getKeyCode() == KeyEvent.VK_Q)       invertSelection();
                                                else if (ke.getKeyCode() == KeyEvent.VK_X)       filterOutSelection();
                                                else if (ke.getKeyCode() == KeyEvent.VK_G)       grid_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_Y)       line_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_A)       pan_mode  = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_D)       selectShapeUnderMouse();
                                                else if (ke.getKeyCode() == KeyEvent.VK_Z)       selectColorUnderMouse();
                                                else if (ke.getKeyCode() == KeyEvent.VK_F)       zoomToFit();
                                                else if (ke.getKeyCode() == KeyEvent.VK_C && (last_shft_down == false && last_ctrl_down == false))       circ_mode = true;
                                                else if (ke.getKeyCode() == KeyEvent.VK_T)             centerSelection();
                                                else if (ke.getKeyCode() == KeyEvent.VK_OPEN_BRACKET)  scaleSelection(last_shft_down ? -2.0 : (last_ctrl_down ? -4 : -1));
                                                else if (ke.getKeyCode() == KeyEvent.VK_CLOSE_BRACKET) scaleSelection(last_shft_down ?  2.0 : (last_ctrl_down ?  4 :  1));
                                                else if (ke.getKeyCode() == KeyEvent.VK_COMMA)         rotateSelection(last_shft_down ? -15 : (last_ctrl_down ? -90 : -1));
                                                else if (ke.getKeyCode() == KeyEvent.VK_PERIOD)        rotateSelection(last_shft_down ?  15 : (last_ctrl_down ?  90 :  1));

                                                else if (ke.getKeyCode() == KeyEvent.VK_1)       selectNodesByDegree(1);
                                                else if (ke.getKeyCode() == KeyEvent.VK_2)       selectNodesByDegree(2);
                                                else if (ke.getKeyCode() == KeyEvent.VK_3)       selectNodesByDegree(3);
                                                else if (ke.getKeyCode() == KeyEvent.VK_4)       selectNodesByDegree(4);
                                                else if (ke.getKeyCode() == KeyEvent.VK_5)       selectNodesByDegree(5);
                                                else if (ke.getKeyCode() == KeyEvent.VK_6)       selectNodesByDegree(6);
                                                else if (ke.getKeyCode() == KeyEvent.VK_7)       selectNodesByDegree(7);
                                                else if (ke.getKeyCode() == KeyEvent.VK_8)       selectNodesByDegree(8);
                                                else if (ke.getKeyCode() == KeyEvent.VK_9)       selectNodesByDegree(9);
                                                else if (ke.getKeyCode() == KeyEvent.VK_0)       selectNodesWithDegreeAtLeast(10);

                                                else if (ke.getKeyCode() == KeyEvent.VK_R)       localizedCleanUp(last_shft_down, last_alt_down, last_ctrl_down);

                                                else if (ke.getKeyCode() == KeyEvent.VK_S)       setStickyLabels();

                                                else if (ke.getKeyCode() == KeyEvent.VK_V && (last_shft_down == false && last_ctrl_down == false))       nextNodeSize();
                                                else if (ke.getKeyCode() == KeyEvent.VK_V && (last_shft_down == false))                                  nextNodeColor();
                                                else if (ke.getKeyCode() == KeyEvent.VK_V && (                           last_ctrl_down == false))       nextLinkSize();
                                                else if (ke.getKeyCode() == KeyEvent.VK_V)                                                               nextLinkColor();

                                                else if (ke.getKeyCode() == KeyEvent.VK_L &&
                                                         last_shft_down)                         { edge_template_flag = !edge_template_flag; render(); }
                                                else if (ke.getKeyCode() == KeyEvent.VK_L &&
                                                         last_ctrl_down)                         { node_legend_flag   = !node_legend_flag; render(); }
                                                else if (ke.getKeyCode() == KeyEvent.VK_L)       toggleLabels();
                                                else if (ke.getKeyCode() == KeyEvent.VK_MINUS && 
                                                                                last_shft_down)  zoomToFit();
                                                else if (ke.getKeyCode() == KeyEvent.VK_MINUS)   zoomOut();
                                                else if (ke.getKeyCode() == KeyEvent.VK_EQUALS)  zoomIn();
                                                else if (ke.getKeyCode() == KeyEvent.VK_UP)      shiftSelection(0,     -mover);
                                                else if (ke.getKeyCode() == KeyEvent.VK_DOWN)    shiftSelection(0,      mover);
                                                else if (ke.getKeyCode() == KeyEvent.VK_LEFT)    shiftSelection(-mover, 0);
                                                else if (ke.getKeyCode() == KeyEvent.VK_RIGHT)   shiftSelection( mover, 0);
                                                else if (ke.getKeyCode() == KeyEvent.VK_W)       makeOneHopsVisible(last_shft_down);
                                                else if (ke.getKeyCode() == KeyEvent.VK_U)       restoreFromUndo();
                                                else if (ke.getKeyCode() == KeyEvent.VK_B)       getRTParent().filterReportsWithoutSelectedEntities();
                                                else if (ke.getKeyCode() == KeyEvent.VK_K)       setOperation((new LandmarkSelection(graph, true)).landmarks());
                                                else if (ke.getKeyCode() == KeyEvent.VK_H)       { draw_help = !draw_help; repaint(); }
                                                else if (ke.getKeyCode() == KeyEvent.VK_F2 ||
                                                         ke.getKeyCode() == KeyEvent.VK_F3 ||
                                                         ke.getKeyCode() == KeyEvent.VK_F4 ||
                                                         ke.getKeyCode() == KeyEvent.VK_F5)      { saveLayoutByFunctionKey(ke.getKeyCode(), last_shft_down, last_ctrl_down); }
                                                else if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
                                                  if        (last_shft_down && last_ctrl_down) { addAllOnVisibleLinks();
                                                  } else if (last_shft_down)                   { makeOneHopsVisible(true);
                                                  } else if (last_ctrl_down)                   {
                                                  } else                                       { makeOneHopsVisible(false);
                                                  }
                                                }
                                              }
  public void keyReleased     (KeyEvent   ke) { super.keyReleased(ke);  
                                                if      (ke.getKeyCode() == KeyEvent.VK_G) grid_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_Y) line_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_C) circ_mode = false;
                                                // else if (ke.getKeyCode() == KeyEvent.VK_Z) zoom_mode = false;
                                                else if (ke.getKeyCode() == KeyEvent.VK_A) pan_mode  = false;
                                              }
  public void keyTyped        (KeyEvent   ke) { super.keyTyped(ke);
                                              }
 
  public void mouseWheelMoved (MouseWheelEvent mwe) { 
    zoomIn(-mwe.getWheelRotation(), m_wx, m_wy);
    // int inc = (int) Math.abs(mwe.getWheelRotation());
    // if (mwe.getWheelRotation() < 0) zoomIn(inc*0.2, m_wx, m_wy); else zoomOut(inc);
  }

  /**
   * Method to handle the generic mouse interactions -- by centralizing the interaction, the state of various operations 
   * can be more easily maintained.
   *
   *@param me_enum mouse action
   *@param me      original mouse event
   */
  protected void genericMouse(ME_ENUM me_enum, MouseEvent me) {
// System.err.println("" + me_enum);
    boolean op_in_effect = false;
    if        (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.PRESSED) {
      op_in_effect = true;
      if         (me.getButton() == MouseEvent.BUTTON2 || pan_mode)   { panOrZoom(me_enum, me); 
      } else if  (me.getButton() == MouseEvent.BUTTON1 && grid_mode)  { gridLayout(me_enum, me);
      } else if  (me.getButton() == MouseEvent.BUTTON1 && line_mode)  { lineLayout(me_enum, me);
      } else if  (me.getButton() == MouseEvent.BUTTON1 && circ_mode)  { circleLayout(me_enum, me);
      } else                                                          { selectOrMove(me_enum,me);
      }
    } else if (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.CLICKED && me.getButton() == MouseEvent.BUTTON2) { panOrZoom(me_enum,me);    op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.NONE && me_enum == ME_ENUM.CLICKED && me.getButton() == MouseEvent.BUTTON1) { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.PANNING)                            { panOrZoom   (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.SELECTING)                          { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.MOVING)                             { selectOrMove(me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.GRID_LAYOUT)                        { gridLayout  (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.LINE_LAYOUT)                        { lineLayout  (me_enum,me); op_in_effect = true;
    } else if (ui_inter == UI_INTERACTION.CIRCLE_LAYOUT)                      { circleLayout(me_enum,me); op_in_effect = true;
    }
    // Capture mouse positions
    m_x = me.getX(); m_y = me.getY(); m_wx = sxToWx(m_x); m_wy = syToWy(m_y);
    // Inform the application of the entity under the mouse
    getRTParent().setEntitiesUnderMouse(underMouse(me));
    // If no operation is in effect, find the closest node and draw the labels
    RenderContext myrc = (RenderContext) rc;
    if (op_in_effect == false && myrc != null && myrc.draw_node_labels == false) {
      String new_dyn_labeler = underMouseSimple(me);
      if (dyn_labeler != null) {
        if (dyn_labeler.equals(new_dyn_labeler) == false) { dyn_labeler = new_dyn_labeler; repaint(); }
      } else if (new_dyn_labeler != null) { dyn_labeler = new_dyn_labeler; repaint(); }
    } else if (dyn_labeler != null) { dyn_labeler = null; repaint(); }
  }
  
  /**
   * Dynamic labeler variable holder
   */
  String dyn_labeler = null;

  /**
   * Initialize the variables used for a mouse drag
   *
   *@param me mouse event
   */
  private void initializeDragVars(MouseEvent me) { m_wx0 = m_wx1 = sxToWx(m_x0 = m_x1 = me.getX()); m_wy0 = m_wy1 = syToWy(m_y0 = m_y1 = me.getY()); }

  /**
   * Update the variables used for a mouse drag
   *
   *@param me mouse event
   */
  private void updateDragVars    (MouseEvent me) { updateDragVars(me,false); }

  /**
   * Update the variables used for a mouse drag
   *
   *@param me           mouse event
   *@param ui_constrain if true, restricts movement to just horizontal or just vertical
   */
  private void updateDragVars    (MouseEvent me, boolean ui_constrain) {
    if        (ui_constrain && last_shft_down) {
      m_wx1 = sxToWx(       m_x1 = me.getX());         
      m_wy1 = m_wy0;
      m_y1  = m_y0;
    } else if (ui_constrain && last_ctrl_down)  {
      m_wx1 = m_wx0;
      m_x1  = m_x0;
      m_wy1 = syToWy(       m_y1 = me.getY()); 
    } else {
      m_wx1 = sxToWx(       m_x1 = me.getX());         
      m_wy1 = syToWy(       m_y1 = me.getY()); 
    }
  }

  /**
   * User interface mode
   */
  UI_INTERACTION  ui_inter = UI_INTERACTION.NONE;
  /**
   * X screen coordinate for beginning of mouse drag
   */
  int             m_x0,  
  /**
   * Y screen coordinate for beginning of mouse drag
   */
                  m_y0, 
  /**
   * X screen coordinate for end of mouse drag
   */
                  m_x1, 
  /**
   * Y screen coordinate for end of mouse drag
   */
                  m_y1, 
  /**
   * Current mouse x screen coordinate
   */
                  m_x, 
  /**
   * Current mouse y screen coordinate
   */
                  m_y; 
  /**
   * X world coordinate for beginning of mouse drag
   */
  double          m_wx0, 
  /**
   * Y world coordinate for beginning of mouse drag
   */
                  m_wy0, 
  /**
   * X world coordinate for end of mouse drag
   */
                  m_wx1, 
  /**
   * Y world coordinate for end of mouse drag
   */
                  m_wy1, 
  /**
   * X world coordinate for mouse current position
   */
                  m_wx, 
  /**
   * Y world coordinate for mouse current position
   */
                  m_wy;
  /**
   * Flag to indicate to layout the nodes in a grid
   */
  boolean         grid_mode = false, 
  /**
   * Flag to indicate to layout the nodes in a line
   */
                  line_mode = false, 
  /**
   * Flag to indicate to layout the nodes in a circle
   */
                  circ_mode = false,
  /**
   *
   */
                  zoom_mode = false,
  /**
   * 
   */
                  pan_mode  = false;
  /**
   * Set of nodes that is currently being moved
   */
  Set<String>    moving_set = null;

  /**
   * Handle the interaction for a grid layout with the mouse.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void gridLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me); ui_inter = UI_INTERACTION.GRID_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me);                                    repaint(); break;
      case RELEASED: updateDragVars(me);     ui_inter = UI_INTERACTION.NONE;        
                     Set<String> set = myrc.filterEntities(getRTParent().getSelectedEntities());

                     // Normalize the coordinates for the grid layout
                     double dx = Math.abs(m_wx1 - m_wx0),
                            dy = Math.abs(m_wy1 - m_wy0);
                     if (m_wx0 > m_wx1) { double tmp = m_wx1; m_wx1 = m_wx0; m_wx0 = tmp; }
                     if (m_wy0 > m_wy1) { double tmp = m_wy1; m_wy1 = m_wy0; m_wy0 = tmp; }

                     // Make sure the set is non-null, non-zero
                     if (set != null && set.size() > 1) {
                       int    sqrt = (int) Math.sqrt(set.size()), max_x_i = 1, max_y_i = 1;
                       if (dx < 0.0001) dx = 0.0001; if (dy < 0.0001) dy = 0.0001;

                       // Figure out the approximate shape
                       if        ((dx/dy) > 1.5 || (dy/dx) > 1.5) { // Rectangular
                         double closest_dist = Double.POSITIVE_INFINITY;
                         for (int i=1;i<=sqrt;i++) {
                           int    other = set.size()/i;
                           double ratio = ((double) other)/((double) i);
                           double dist  = Math.abs(ratio - dx/dy);
                           if (dist < closest_dist) {
                             if (dx/dy > 1.0) {
                               max_x_i = (i > other) ? i : other;
                               max_y_i = (i > other) ? other : i;
                             } else           {
                               max_x_i = (i > other) ? other : i;
                               max_y_i = (i > other) ? i : other;
                             }
                           }
                         }
                       } else if ((dy/dx) > 1.5) { // Rectangular

                       } else                    { // Roughly square
                         max_x_i = max_y_i = sqrt;
                       }
                       int x_i = 0, y_i = 0;
                       // Sort the elements
                       List<StrCountSorter> sorter = new ArrayList<StrCountSorter>();
                       Iterator<String> it = set.iterator();
                       while (it.hasNext()) {
                         String entity = it.next();
                         int    total  = (int) myrc.entity_counter_context.total(entity); 
                         sorter.add(new StrCountSorter(entity,total));
                       }
                       Collections.sort(sorter);
                       // Do the  layout
                       saveLayoutForUndo(entity_to_wxy, set);
                       for (int i=0;i<sorter.size();i++) {
                         String entity = sorter.get(i).toString();
                         entity_to_wxy.put(entity,new Point2D.Double(m_wx0 + x_i*(dx/max_x_i),
                                                                     m_wy0 + y_i*(dy/max_y_i)));
                         transform(entity);
                         x_i++; if (x_i >= max_x_i) { x_i = 0; y_i++; }
                       }
                       getRTComponent().render();
                     } else if (set != null && set.size() == 1) {
                       saveLayoutForUndo(entity_to_wxy, null);
                       entity_to_wxy.put(set.iterator().next(), new Point2D.Double(m_wx0 + dx/2, m_wy0 + dy/2));
                       transform(set.iterator().next());
                       getRTComponent().render();
                     }
                     repaint(); break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a line layout.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void lineLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me);   ui_inter = UI_INTERACTION.LINE_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me, true);                                        repaint(); break;
      case RELEASED: updateDragVars(me, true); ui_inter = UI_INTERACTION.NONE;
                     Set<String> set = myrc.filterEntities(getRTParent().getSelectedEntities());
                     if (set != null && set.size() > 0) {
                       // Calculate the line equation
                       double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                       double t  = 0.0, inc = 1.0 / (set.size() - 1);
                       // Sort the elements
                       List<StrCountSorter> sorter = new ArrayList<StrCountSorter>();
                       Iterator<String> it = set.iterator();
                       while (it.hasNext()) {
                         String entity = it.next();
                         int    total  = (int) myrc.entity_counter_context.total(entity); 
                         sorter.add(new StrCountSorter(entity,total));
                       }
                       Collections.sort(sorter);
                       // Do the  layout
                       saveLayoutForUndo(entity_to_wxy, set);
                       for (int i=0;i<sorter.size();i++) {
                         String entity = sorter.get(i).toString();
                         entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + t*dx, m_wy0 + t*dy));
                         transform(entity);
                         t += inc;
                       }
                       getRTComponent().render();
                     }
                     repaint(); break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interations for a circle layout.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void circleLayout(ME_ENUM me_enum, MouseEvent me) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me); ui_inter = UI_INTERACTION.CIRCLE_LAYOUT; repaint(); break;
      case DRAGGED:  updateDragVars(me);                                              repaint(); break;
      case RELEASED: updateDragVars(me);     ui_inter = UI_INTERACTION.NONE;          repaint();
                     double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0, radius = Math.sqrt(dx*dx+dy*dy);
                     Set<String> set = myrc.filterEntities(getRTParent().getSelectedEntities());
                     saveLayoutForUndo(entity_to_wxy, set);
                     if        (set != null && set.size() == 1) {
                       entity_to_wxy.put(set.iterator().next(), new Point2D.Double(m_wx0 + dx/2, m_wy0 + dy/2));
                       transform(set.iterator().next());
                       getRTComponent().render();
                     } else if (set != null && set.size() >  0) {
                       double angle = 0.0, angle_inc = 2.0 * Math.PI / set.size();
                       // Sort the elements
                       List<StrCountSorter> sorter = new ArrayList<StrCountSorter>();
                       Iterator<String> it = set.iterator();
                       while (it.hasNext()) {
                         String entity = it.next();
                         int    total  = (int) myrc.entity_counter_context.total(entity); 
                         sorter.add(new StrCountSorter(entity,total));
                       }
                       Collections.sort(sorter);
                       // Do the  layout
                       for (int i=0;i<sorter.size();i++) {
                         String entity = sorter.get(i).toString();
                         entity_to_wxy.put(entity, new Point2D.Double(m_wx0 + Math.cos(angle)*radius,
                                                                      m_wy0 + Math.sin(angle)*radius));
                         transform(entity);
                         angle += angle_inc;
                       }
                       getRTComponent().render();
                     }
                     break;
        case CLICKED:
                break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a pan/zoom operations.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void panOrZoom(ME_ENUM me_enum, MouseEvent me) {
    if (me.getButton() != MouseEvent.BUTTON2 && ui_inter != UI_INTERACTION.PANNING && pan_mode == false) return;
    switch (me_enum) {
      case PRESSED:  initializeDragVars(me);  ui_inter = UI_INTERACTION.PANNING; repaint(); break;
      case DRAGGED:  updateDragVars(me,true);                                    repaint(); break;
      case RELEASED: updateDragVars(me,true); ui_inter = UI_INTERACTION.NONE; 
                     double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                     if (dx != 0.0 || dy != 0.0) {
                       Rectangle2D r = getExtents();
                       setExtents(new Rectangle2D.Double(r.getX() - dx, r.getY() - dy, r.getWidth(), r.getHeight()));
                     }
                     break;
      case CLICKED:  zoomToFit(); break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
    }
  }

  /**
   * Handle the interactions for a select entities, move entities operation.
   *
   *@param me_enum mouse action
   *@param me      mouse event
   */
  public void selectOrMove(ME_ENUM me_enum, MouseEvent me) {
    if (me.getButton() != MouseEvent.BUTTON1 && ui_inter != UI_INTERACTION.SELECTING && ui_inter != UI_INTERACTION.MOVING) return;
    RenderContext myrc = (RenderContext) rc;
    if (myrc != null) {
      switch (me_enum) {
        case PRESSED:  Set<String>  sel = myrc.filterEntities(getRTParent().getSelectedEntities());

                       // Figure what's under the mouse
                       Set<String>  under_mouse = underMouse(me);

                       // If what's under the mouse is already selected, then cause it to be moving
                       if      (Utils.overlap(sel,under_mouse))     { ui_inter = UI_INTERACTION.MOVING; 
                                                                      moving_set = sel; }
                       else if (under_mouse.size() > 0 && 
                                (last_shft_down || last_ctrl_down)) { setOperation(under_mouse); }
                       else if (under_mouse.size() > 0)             { ui_inter = UI_INTERACTION.MOVING; 
                                                                      getRTParent().setSelectedEntities(under_mouse, reportEntities());
                                                                       moving_set = under_mouse; }
                       else                                         { ui_inter = UI_INTERACTION.SELECTING; }

                       initializeDragVars(me);

                       repaint();
                       break;
        case DRAGGED:  updateDragVars(me, ui_inter == UI_INTERACTION.MOVING); repaint(); break;
        case RELEASED: updateDragVars(me, ui_inter == UI_INTERACTION.MOVING);
                       boolean no_move = false;
                       if        (ui_inter == UI_INTERACTION.MOVING)    {
                         double dx = m_wx1 - m_wx0, dy = m_wy1 - m_wy0;
                         if (dx != 0 || dy != 0) {
                           saveLayoutForUndo(entity_to_wxy, moving_set);
                           Iterator<String> it = moving_set.iterator();
                           while (it.hasNext()) {
                             String  entity  = it.next();
                             Point2D pt      = entity_to_wxy.get(entity);
                             entity_to_wxy.put(entity, new Point2D.Double(pt.getX() + dx, pt.getY() + dy));
                             transform(entity);
                           }
                         getRTComponent().render();
                         } else no_move = true;
                       } 
                       if (ui_inter == UI_INTERACTION.SELECTING || no_move) {
                         int x0 = m_x0 < m_x1 ? m_x0 : m_x1,   y0 = m_y0 < m_y1 ? m_y0 : m_y1,
                             dx = (int) Math.abs(m_x1 - m_x0), dy = (int) Math.abs(m_y1 - m_y0);
                         if (dx == 0) dx = 1; if (dy == 0) dy = 1;
                         Rectangle2D rect = new Rectangle2D.Double(x0,y0,dx,dy);

                         Set<String> new_sel = new HashSet<String>();
                         Iterator<String> it = myrc.node_to_geom.keySet().iterator();
                         while (it.hasNext()) {
                           String node = it.next(); Shape shape = myrc.node_to_geom.get(node);
                           if (rect.intersects(shape.getBounds())) new_sel.addAll(myrc.node_coord_set.get(node));
                         }
                         setOperation(new_sel);
                       }
                       ui_inter = UI_INTERACTION.NONE;
                       repaint(); break;
        case CLICKED:  setOperation(underMouse(me));
                       repaint(); break;
        case MOVED:
                break;
        case WHEEL:
                break;
        default:
                break;
      }
    }
  }

  /**
   * Set the nodes for sticky labels.  Sticky labels show for the set of node irregardless of
   * the draw labels flag.  They are useful for maintaining context in the graph for the key nodes.
   */
  public void setStickyLabels() {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    Set<String>   sel  = myrc.filterEntities(getRTParent().getSelectedEntities());
    if (sel != null) {
      if        (last_shft_down && last_ctrl_down) { /* Select    */ getRTParent().setSelectedEntities(sticky_labels);
      } else if (last_shft_down)                   { /* Subtract  */ sticky_labels.removeAll(sel);
      } else if (last_ctrl_down)                   { /* Add       */ sticky_labels.addAll(sel);
      } else                                       { /* Set/Clear */ sticky_labels.clear(); sticky_labels.addAll(sel); }
    }
    getRTComponent().render(); repaint();
  }

  /**
   * Based on the keys pressed (ctrl, shft), perform the correct set operation for selected entities.
   *
   *@param sel selection set
   */
  public void setOperation(Set<String> sel) {
    RenderContext myrc = (RenderContext) rc; if (myrc == null) return;
    Set<String> new_sel = new HashSet<String>(), old_sel;
    if        (last_shft_down && last_ctrl_down) { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.retainAll(sel);
                                                   new_sel = old_sel;
    } else if (last_shft_down)                   { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.removeAll(sel);
                                                   new_sel = old_sel;
    } else if (                  last_ctrl_down) { old_sel = myrc.filterEntities(getRTParent().getSelectedEntities());
                                                   old_sel.addAll(sel);
                                                   new_sel = old_sel;
    } else new_sel = sel;
    getRTParent().setSelectedEntities(new_sel, reportEntities());
  }

  /**
   * Find the entities under the mouse.  Useful for selecting and moving operation.
   *
   *@param  me mouse event
   *
   *@return all entities under the mouse as a set
   */
  Set<String> underMouse(MouseEvent me) { return underMouse(me.getX(), me.getY()); }

  /**
   * Find the entities under the mouse.  Useful for selecting and moving operation.
   *
   *@param x x coordinate of mouse
   *@param y y coordinate of mouse
   *
   *@return all entities under the mouse as a set
   */
  Set<String> underMouse(int x, int y) {
    RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return new HashSet<String>();
    Rectangle2D      mouse_rect  = new Rectangle2D.Double(x-1,y-1,3,3);
    Set<String>      under_mouse = new HashSet<String>();
    Iterator<String> it          = myrc.node_coord_set.keySet().iterator();
    while (it.hasNext()) {
      String node_coord = it.next(); 
      if (myrc.node_to_geom.containsKey(node_coord) && mouse_rect.intersects(myrc.node_to_geom.get(node_coord).getBounds())) {
        under_mouse.addAll(myrc.node_coord_set.get(node_coord));
      }
    }
    if (under_mouse.size() == 0) { // If nothing, check the links
    }
    return under_mouse;
  }

  /**
   * Find the entities under the mouse.  For this version, only nodes are considered and no accumulateion occurs.
   *
   *@param me mouse event
   *
   *@return node_coord for the object under the mouse
   */
  String underMouseSimple(MouseEvent me) {
    RenderContext    myrc        = (RenderContext) rc; if (myrc == null) return null;
    Rectangle2D      mouse_rect  = new Rectangle2D.Double(me.getX()-1,me.getY()-1,3,3);
    Iterator<String> it          = myrc.node_coord_set.keySet().iterator();
    while (it.hasNext()) {
      String node_coord = it.next(); 
      if (myrc.node_to_geom.containsKey(node_coord) && 
          mouse_rect.intersects(myrc.node_to_geom.get(node_coord).getBounds())) return node_coord;
    }
    return null;
  }

  /**
   * Set the color that indicates the current UI mode.
   *
   *@param g2d graphics primitive object
   */
  public void modeColor(Graphics2D g2d) { g2d.setColor(RTColorManager.getColor("label", "default")); }

  /**
   * Paint the component through the super class.  Overlay the current interactions on the screen.
   * Draw dynamic labels, selected entities, highlighted entities, help as specified by user.
   *
   *@param g graphics primitive
   */
  public void paintComponent(Graphics g) {
    // Setup
    Graphics2D g2d = (Graphics2D) g; int txt_h = Utils.txtH(g2d, "0");
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

    // Paint the super
    super.paintComponent(g2d); 

    // Get the render context and draw if it's valid
    RenderContext myrc = (RenderContext) rc;
    if (myrc != null) {
      // Draw the interactions
      drawInteractions(g2d, myrc);

      // Draw the edge lens
      if      (mode() == UI_MODE.EDGELENS) drawEdgeLens(g2d, myrc);
      else if (mode() == UI_MODE.TIMELINE) drawTimeLine(g2d, myrc);

      // Draw the selection
      Set<String> sel = getRTParent().getSelectedEntities(); if (sel == null) sel = new HashSet<String>();
      if (sel.size() > 0 || sticky_labels.size() > 0) { drawSelectedEntities(g2d, myrc, sel, sticky_labels, txt_h);
      } else { String str = "" + mode(); modeColor(g2d); g2d.drawString("" + str, 5, txt_h); }

      // Draw the dynamic labels -- under the mouse for context
      String dyn_labeler_copy = dyn_labeler;
      if (dyn_label_cbmi.isSelected() && 
          dyn_labeler_copy != null    && 
          myrc.node_coord_set.containsKey(dyn_labeler_copy)) drawDynamicLabels(g2d, myrc, dyn_labeler_copy);

      // If graph info mode, provide additional information
      if (myrc.provideGraphInfo()) { drawGraphInfo(g2d, myrc, sel); }

      // Draw the vertex placement heatmap (experimental)
      if (vertex_placement_heatmap_cbmi.isSelected()) {
        // Make sure it's a single selection (and that it's also a vertex);
        if (sel != null && sel.size() == 1) {
          String sel_str = sel.iterator().next(); if (entity_to_wxy.containsKey(sel_str)) {
            int hm_w = myrc.getRCWidth()/4, hm_h = myrc.getRCHeight()/4;
            // Determine if the heatmap will be large enough to provide value
            if (hm_w >= 50 && hm_h >= 50) {
              // Determine if the heatmap needs to be recalculated (doesn't consider if the size is still correct... or if the node shifted...
              if (hm_sel == null || hm_sel.equals(sel_str) == false || hm_bi == null) {
                hm_bi  = GraphUtils.vertexPlacementHeatmap(new UniGraph(graph), sel_str, entity_to_wxy, hm_w, hm_h);
                hm_sel = sel_str;
              }
              Composite orig_comp = g2d.getComposite(); g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f)); // Partially transparent
              g2d.drawImage(hm_bi, myrc.getRCWidth() - hm_bi.getWidth(null), myrc.getRCHeight() - hm_bi.getHeight(null), null);
              g2d.setComposite(orig_comp);
            }
          }
        }
      }

      // Show the edge crossings
      if (calculateCrossings()) {
        long ecp_t0 = System.currentTimeMillis();
        EdgeCrossingsPlus my_ecp = calcEdgeCrossingsPlus();
        int crossings = my_ecp.totalCrossings();
        long ecp_t1 = System.currentTimeMillis();
        String s = "X'ings: " + crossings;

        if ((ecp_t1 - ecp_t0) > 1L) System.err.println("Edge Crossings Calc Time: " + (ecp_t1 - ecp_t0) + " ms");

        g2d.setColor(RTColorManager.getColor("label", "default"));
        g2d.drawString(s, getWidth() - 5 - Utils.txtW(g2d, s), 5 + 2*Utils.txtH(g2d, s));
      }

      // Draw the highlighted entities
      highlightEntities(g2d, myrc);

      // Draw the excerpts
      drawExcerpts(g2d, myrc);
    }

    // Draw the help chart
    if (draw_help) drawHelp(g2d);
  }

  /**
   * State for the heatmap (so that it doesn't need to be recalculated on every redraw)
   */
  private String        hm_sel = null;
  private BufferedImage hm_bi  = null;

  /**
   * Experimental feature to draw the excerpts from reports within the window.
   *
   *@param g2d  graphics device
   *@param myrc render context -- assumes already checked for null
   */
  private void drawExcerpts(Graphics2D g2d, RenderContext myrc) {
    Rectangle2D bounds     = new Rectangle2D.Double(0,0,getWidth(),getHeight());
    Area        fill_state = new Area();

    Map<String,Set<SubText>> excerpt_map = getRTParent().getExcerptMap();
    Iterator<String> it = excerpt_map.keySet().iterator();
    while (it.hasNext()) {
      String entity     = it.next();
      String node_coord = entity_to_sxy.get(entity);
      Shape  shape      = myrc.node_to_geom.get(node_coord);
      if (shape != null) {
        SubText.renderContextHints(g2d, excerpt_map.get(entity), (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getCenterY(), 
                                   bounds, fill_state);
      }
    }
  }

  /**
   * Experimental feature to draw a timeline with the line (and corresponding records) under
   * the mouse.  Timeline is drawn at the bottom of the view with matching edges stacked ontop
   * of the original edge.
   *
   * Probably need to throttle / bail out if taking too long...
   *
   *@param g2d  graphics device
   *@param myrc render context
   */
  private void drawTimeLine(Graphics2D g2d, RenderContext myrc) {
    if (mouseIn()) {
      double mxc = mx, myc = my; // Save the mouse coords so they are static

      // Find the closest line segment
      Line2D closest_line = null; double closest_line_d = Double.MAX_VALUE;
      Iterator<Line2D> it = myrc.line_to_bundles.keySet().iterator();
      while (it.hasNext()) {
        Line2D line = it.next();
        double d    = line.ptSegDist(mxc,myc);
        if (d < closest_line_d) { closest_line = line; closest_line_d = d; }
      }

      // If we found an edge, create the timeline
      if (closest_line != null && closest_line_d < 20.0) {
        // Darken background
        Composite orig_comp = g2d.getComposite(); 
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
        g2d.setColor(RTColorManager.getColor("brush", "dim"));
        g2d.fillRect(0,0,myrc.getRCWidth(),myrc.getRCHeight());
        g2d.setComposite(orig_comp);

        // Create the composites
        Composite trans = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f), full  = g2d.getComposite();

        // Determine the timeline geometry
        int txt_h = Utils.txtH(g2d, "0");
        int tl_x0 = 10, tl_x1 = myrc.w - 10,        tl_w = tl_x1 - tl_x0;
        int             tl_y0 = myrc.h - txt_h - 4, tl_h = 6;

        // Get the time bounds
        long ts0 = getRenderBundles().ts0(), ts1 = getRenderBundles().ts1dur();

        // Give it labels
        String str = Utils.humanReadableDate(ts0); Color fg = RTColorManager.getColor("label", "defaultfg"), bg = RTColorManager.getColor("label", "defaultbg");
        g2d.setColor(RTColorManager.getColor("axis","default")); g2d.drawLine(tl_x0, tl_y0+1, tl_x1, tl_y0+1);
        clearStr(g2d, str, tl_x0, tl_y0 + txt_h+2, fg, bg);
        str = Utils.humanReadableDate(ts1);
        clearStr(g2d, str, tl_x1 - Utils.txtW(g2d,str), tl_y0 + txt_h+2, fg, bg);

        // Highlight the line itself
        g2d.setColor(RTColorManager.getColor("annotate","cursor"));
        drawTimeLineForLine(g2d, myrc, closest_line, full, trans, tl_x0, tl_x1, tl_w, tl_y0, tl_h, ts0, ts1);

        // Create a bitvector representing locations in the timeline...  get the resolution from the UI... bound it somewhere...
        long resolution      = timeLineResolution();
        int  resolution_bins = (int) ((ts1 - ts0)/resolution);
        if (resolution_bins <= 0) resolution_bins = 1;

        if (resolution_bins > 0 && resolution_bins < 5*365*24*60) {
          boolean mask[] = new boolean[resolution_bins+1]; // Give it a little extra...
          fillTimeLineMask(myrc,closest_line,mask,ts0,resolution);

          // Count matches across the rest of the lines...
          List<LineCountSorter> sorter = new ArrayList<LineCountSorter>();
          it = myrc.line_to_bundles.keySet().iterator();
          while (it.hasNext()) {
            Line2D line = it.next();
            if (line != closest_line) {
              int matches = countTimeLineMatches(myrc, line, mask, ts0, resolution);
              if (matches > 0) { sorter.add(new LineCountSorter(line, matches)); }
            }
          }

          // If we have matches, sort them
          Collections.sort(sorter);

          // Display the top twenty or so...
          for (int i=0;i<(sorter.size() > 20 ? 20 : sorter.size());i++) {
            tl_y0 -= tl_h; g2d.setColor(RTColorManager.getColor(sorter.get(i).getLine().toString()));
            drawTimeLineForLine(g2d, myrc, sorter.get(i).getLine(), full, trans, tl_x0, tl_x1, tl_w, tl_y0, tl_h, ts0, ts1);
          }

          if (sorter.size() > 20) { // Let the user know that the display is abridged...
            tl_y0 -= tl_h;
            fg = RTColorManager.getColor("label", "errorfg"); bg = RTColorManager.getColor("label", "errorbg");
            clearStr(g2d, "Edges Truncated", tl_x0, tl_y0, fg, bg);
          }
        }
      }
    }
  }

  /**
   * Fill the provided bit-vector mask with the times of the records (bundles) in the specified
   * line.
   *
   *@param myrc       render context
   *@param line       line to reference records for filling bitvector
   *@param mask       pre-allocated bitvector
   *@param ts0        initial timestamp of records
   *@param resolution divisor for time different for calculating bitvector index
   */
  private void fillTimeLineMask(RenderContext myrc, Line2D line, boolean mask[], long ts0, long resolution) {
    Iterator<Bundle> it = myrc.line_to_bundles.get(line).iterator();
    while (it.hasNext()) {
      Bundle bundle = it.next();
      if (bundle.hasTime()) {
        int i0 = (int) ((bundle.ts0() - ts0)/resolution) - 1; if (i0 <  0)           i0 = 0;
        int i1 = (int) ((bundle.ts1() - ts0)/resolution) + 1; if (i1 >= mask.length) i1 = mask.length-1;
        for (int i=i0;i<=i1;i++) mask[i] = true;

        // Do +/- one as well
        if ((i0-1) >= 0)            mask[i0-1] = true;
        if ((i1+1) <  mask.length)  mask[i1+1] = true;
      }
    }
  }

  /**
   * Count the number of overlapping bitvector indices with the specified line's records.
   *
   *@param myrc       render context
   *@param line       line to reference records for matching bitvector
   *@param mask       pre-allocated bitvector
   *@param ts0        initial timestamp of records
   *@param resolution divisor for time different for calculating bitvector index
   *
   *@return number of bit positions matching specific line records
   */
  private int countTimeLineMatches(RenderContext myrc, Line2D line, boolean mask[], long ts0, long resolution) {
    int matches = 0;
    Iterator<Bundle> it = myrc.line_to_bundles.get(line).iterator();
    while (it.hasNext()) {
      Bundle bundle = it.next();
      if (bundle.hasTime()) { 
        int i0 = (int) ((bundle.ts0() - ts0)/resolution), i1 = (int) ((bundle.ts1() - ts0)/resolution);
        for (int i=i0;i<=i1;i++) if (mask[i]) matches++;
      }
    }
    return matches;
  }

  /**
   * Render a line (and its corresponding records) on the timeline.
   *
   *@param g2d graphics device
   *@param myrc render context
   *@param line line to render (and corresponding records)
   *@param full full composite (no transparency)
   *@param trans transparent composite
   *@param tl_x0 min x of timeline
   *@param tl_x1 max x of timeline
   *@param tl_w  width of timeline
   *@param tl_y0 base y of timeline
   *@param ts0   minimum timestamp for visible records
   *@param ts1   maximum timestamp for visible records
   */
  private void drawTimeLineForLine(Graphics2D g2d, RenderContext myrc, Line2D line, 
                                   Composite full, Composite trans,
                                   int tl_x0, int tl_x1, int tl_w, int tl_y0, int tl_h, 
                                   long ts0, long ts1) {
        g2d.setComposite(full); 
        // Parametric formula for line
        double dx  = line.getX2() - line.getX1(), dy  = line.getY2() - line.getY1();
        double len = Utils.length(dx,dy);
        if (len < 0.001) len = 0.001; dx = dx/len; dy = dy/len; double pdx = dy, pdy = -dx;
        if (pdy < 0.0)   { pdy = -pdy; pdx = -pdx; } // Always point down
        double gather_x = line.getX1() + dx*len/2 + pdx*40, gather_y = line.getY1() + dy*len/2 + pdy*40;

        // Find the bundles, for this with timestamps, construct the timeline
        Set<Bundle> set = myrc.line_to_bundles.get(line);
        Iterator<Bundle> itb = set.iterator(); double x_sum = 0.0; int x_samples = 0;
        while (itb.hasNext()) {
          Bundle bundle = itb.next();
          if (bundle.hasTime()) { x_sum += (int) (tl_x0 + (tl_w*(bundle.ts0() - ts0))/(ts1 - ts0)); x_samples++; }
        }
        if (x_samples == 0) return;
        double x_avg = x_sum/x_samples;
        ColorScale timing_marks_cs = RTColorManager.getTemporalColorScale();
        itb = set.iterator();
        while (itb.hasNext()) { 
          Bundle bundle = itb.next();

          if (bundle.hasTime()) {
            double ratio = ((double) (bundle.ts0() - ts0))/((double) (ts1 - ts0));
            g2d.setColor(timing_marks_cs.at((float) ratio));

            int xa = (int) (tl_x0 + (tl_w*(bundle.ts0() - ts0))/(ts1 - ts0));
            // g2d.setComposite(full); 
            g2d.draw(line);
            if        (bundle.hasDuration()) {
              int xb = (int) (tl_x0 + (tl_w*(bundle.ts1() - ts0))/(ts1 - ts0)); 
              double r = (tl_h-2)/2;
              g2d.fill(new Ellipse2D.Double(xa-r,tl_y0-tl_h/2-r,2*r,2*r));
              g2d.fill(new Ellipse2D.Double(xb-r,tl_y0-tl_h/2-r,2*r,2*r));
              if (xa != xb) g2d.fill(new Rectangle2D.Double(xa, tl_y0-tl_h/2-r, xb-xa, 2*r));
              if (xa != xb) { g2d.drawLine(xa,tl_y0-tl_h,xb,tl_y0); g2d.drawLine(xa,tl_y0,     xb,tl_y0); }
            }
            g2d.drawLine(xa,tl_y0-tl_h,xa,tl_y0); // Make it slightly higher at the start
            double x0 = line.getX1() + dx * len * 0.1 + dx * len * 0.8 * ratio,
                   y0 = line.getY1() + dy * len * 0.1 + dy * len * 0.8 * ratio;
            g2d.draw(new CubicCurve2D.Double(x0, y0, 
                                               x0       + pdx*10, y0       + pdy*10,
                                               gather_x - pdx*10, gather_y - pdy*10,
                                             gather_x, gather_y));
            g2d.draw(new CubicCurve2D.Double(gather_x, gather_y, 
                                               gather_x + pdx*40, gather_y + pdy*40,
                                               x_avg,             tl_y0 - 10*tl_h, 
                                             x_avg, tl_y0 - 8*tl_h));
            g2d.draw(new CubicCurve2D.Double(x_avg,         tl_y0 - 8*tl_h,
                                               (x_avg+xa)/2,  tl_y0 - 6*tl_h,
                                               xa,            tl_y0 - 2*tl_h,
                                             xa,            tl_y0 - tl_h/2));
          }
        }
      }
  
  /**
   * Draw the edge lens interactive technique.  See the following for a description:
   * http://innovis.cpsc.ucalgary.ca/Research/EdgeLens
   *
   *@param g2d  graphics primitive
   *@param myrc current render context
   */
  private void drawEdgeLens(Graphics2D g2d, RenderContext myrc) {
    if (mouseIn()) {
      double           edgelens_dist = 50.0, mxc = mx, myc = my;

      // Figure out which edges to bend
      Set<Line2D>  to_bend       = new HashSet<Line2D>();
      Iterator<Line2D> it            = myrc.line_to_bundles.keySet().iterator();
      while (it.hasNext()) {
        Line2D line = it.next();
        double dist = line.ptSegDist(mxc,myc), dist_v0 = line.getP1().distance(mxc,myc), dist_v1 = line.getP2().distance(mxc,myc);
        if (dist < edgelens_dist && dist < dist_v0 && dist < dist_v1) to_bend.add(line);
      }

      // If there are edges to bend, darken the background and bend the edges
      if (to_bend.size() > 0) {
        // Darken background
        Composite orig_comp = g2d.getComposite(); 
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f));
        g2d.setColor(RTColorManager.getColor("brush", "dim"));
        g2d.fillRect(0,0,myrc.getRCWidth(),myrc.getRCHeight());
        g2d.setComposite(orig_comp);

        // Draw the bended edges
        // - Approximates "EdgeLens:  An Interactive Methd for Managing Edge Congestion in Graphs", Wong, Caprendale, and Greenberg.  2003.
        g2d.setColor(RTColorManager.getColor("linknode", "edgelens"));
        it = to_bend.iterator();
        while (it.hasNext()) {
          Line2D line = it.next();
          double dist = line.ptSegDist(mxc,myc);
          double dx   = line.getX2() - line.getX1(),
                 dy   = line.getY2() - line.getY1(),
                 mdx  = mxc          - line.getX1(),
                 mdy  = myc          - line.getY1(),
                 len  = Math.sqrt(dx*dx + dy*dy);
          if (len > 0.001) {
            dx /= len; dy /= len; 
            
            double pdx, pdy;
            if (Utils.shortestAngle(dx,dy,mdx,mdy) < 0.0) { pdx = -dy; pdy = dx; } else { pdx = dy; pdy = -dx; }

            double sc_x       = mxc + pdx*dist,   sc_y      = myc + pdy*dist;   // Uses notation from "EdgeLens" paper
            double disp_sc_x  = mxc + pdx*dist*3, disp_sc_y = myc + pdy*dist*3; // Unsure of this one...  paper cites another paper... math :(
            double n2_dist    = Math.sqrt((sc_x - line.getX1()) * (sc_x - line.getX1()) + (sc_y - line.getY1()) * (sc_y - line.getY1())),
                   n1_dist    = Math.sqrt((sc_x - line.getX2()) * (sc_x - line.getX2()) + (sc_y - line.getY2()) * (sc_y - line.getY2()));
            double c1_x       = disp_sc_x - dx * 0.4 * n1_dist, c1_y       = disp_sc_y - dy * 0.4 * n1_dist,
                   c2_x       = disp_sc_x + dx * 0.4 * n2_dist, c2_y       = disp_sc_y + dy * 0.4 * n2_dist;

            g2d.draw(new CubicCurve2D.Double(line.getX1(), line.getY1(), c1_x, c1_y, c2_x, c2_y, line.getX2(), line.getY2()));
          }
        }
      }
    }
  }

  /**
   * Draw the current interactions as the mouse is dragged.
   *
   *@param g2d  graphics primitive
   *@param myrc current render context
   */
  private void drawInteractions(Graphics2D g2d, RenderContext myrc) {
      Composite orig_comp = g2d.getComposite();
      // Draw the interaction
      switch (ui_inter) {
        case PANNING:       g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,getWidth(),getHeight());
                            g2d.drawImage(myrc.getBase(), m_x1 - m_x0, m_y1 - m_y0, null); 
                            g2d.setColor(RTColorManager.getColor("annotate", "cursor")); int cx = getWidth()/2, cy = getHeight()/2; g2d.drawLine(cx-12,cy,cx+12,cy); g2d.drawLine(cx,cy-12,cx,cy+12);
                            break;
        case GRID_LAYOUT:
        case CIRCLE_LAYOUT:
        case SELECTING:     int x0 = m_x0 < m_x1 ? m_x0 : m_x1,   y0 = m_y0 < m_y1 ? m_y0 : m_y1,
                                dx = (int) Math.abs(m_x1 - m_x0), dy = (int) Math.abs(m_y1 - m_y0);
                            if (dx == 0) dx = 1; if (dy == 0) dy = 1;
                            g2d.setColor(RTColorManager.getColor("select", "region"));
                            Shape shape;
                            if (ui_inter == UI_INTERACTION.SELECTING || ui_inter == UI_INTERACTION.GRID_LAYOUT) {
                              shape = new Rectangle2D.Double(x0,y0,dx,dy);
                            } else {
                              double radius = Math.sqrt(dx*dx+dy*dy);
                              shape = new Ellipse2D.Double(m_x0-radius,m_y0-radius,2*radius,2*radius);
                            }
                            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
                            g2d.fill(shape);
                            g2d.setComposite(orig_comp);
                            if (ui_inter == UI_INTERACTION.SELECTING) {
                              String str = "Selecting..."; if      (last_shft_down && last_ctrl_down) str += " (Intersect)";
                                                           else if (last_shft_down                  ) str += " (Remove From)";
                                                           else if (                  last_ctrl_down) str += " (Add To)";
                              g2d.drawString(str, x0, y0); 
                            } else g2d.drawString("Layout...", x0, y0);
                            g2d.draw(shape);
                            break;
        case MOVING:        g2d.drawString("Moving...", 5, getHeight()-5); 
                            Set<String> moving_set_dup = moving_set;
                            if (moving_set_dup != null && moving_set_dup.size() > 0) {
                              AffineTransform orig = g2d.getTransform(); g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));
                              g2d.translate(m_x1 - m_x0, m_y1 - m_y0);
                              Iterator<String> it = moving_set_dup.iterator();
                              while (it.hasNext()) {
                                String str = it.next(); shape = myrc.node_to_geom.get(entity_to_sxy.get(str));
                                if (shape != null) { g2d.draw(shape); }
                              }
                              g2d.setTransform(orig);
                              }
                            break;
        case LINE_LAYOUT:   g2d.setColor(RTColorManager.getColor("linknode", "layout"));
                            g2d.drawLine(m_x0, m_y0, m_x1, m_y1);
                            break;
        case NONE:
                break;
        default:
                break;
      }
  }

  /**
   * Draw the number of selected entities.  Draw the current mode in text.
   *
   *@param g2d    graphics primitive
   *@param myrc   current render context
   *@param sel    currently selected entities
   *@param sticks sticky label set
   *@param txt_h  text height for current font
   */
  private void drawSelectedEntities(Graphics2D g2d, RenderContext myrc, Set<String> sel, Set<String> sticks, int txt_h) {
        Stroke orig_stroke = g2d.getStroke(); g2d.setStroke(new BasicStroke(3.0f));

        // Draw the selected entities - outline the shape with red and draw labels if appropriate
        Iterator<String> it = sel.iterator();
        while (it.hasNext()) { 
          String node  = it.next();  String node_coord = entity_to_sxy.get(node);
          Shape  shape = myrc.node_to_geom.get(node_coord);
          if (shape != null) {
            g2d.setColor(RTColorManager.getColor("linknode", "movenodes")); g2d.draw(shape);
            if (myrc.node_lm != null && sel.size() < 100 && sticks.contains(node) == false) { 
              // Bound it by 100...  otherwise, it's going to be unreasonable
              myrc.node_lm.draw(g2d, node_coord, (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getMaxY(), true);
            }
          }
        }

        // Draw the sticky labels
        it = sticks.iterator();
        while (it.hasNext()) {
          String node  = it.next();  String node_coord = entity_to_sxy.get(node);
          Shape  shape = myrc.node_to_geom.get(node_coord);
          if (shape != null && myrc.node_lm != null) {
            myrc.node_lm.draw(g2d, node_coord, (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getMaxY(), true);
          }
        }
        g2d.setStroke(orig_stroke);

        // Provide information on what's selected
        g2d.setColor(RTColorManager.getColor("label", "default"));
        g2d.drawString("" + sel.size() + " Selected", 5, txt_h);

        int x_off = Utils.txtW(g2d, "" + sel.size() + " Selected");

        // Print the mode string...  doesn't really belong here...
        String str = "| " + mode();
        modeColor(g2d);
        g2d.drawString("" + str, 10 + x_off, txt_h);
  }

  /**
   * Results of dijkstra's single source shortest path algorithm.  Kept for caching purposes.
   */
  DijkstraSingleSourceShortestPath last_ssp; 

  /**
   * Last source for the single source shortest path algorithm.
   */
  String                           last_ssp_source;

  /**
   * Draw information on the nodes related to graph algorithm.  Currently, the shortest path
   * between two selected nodes is shown as well as the nearest three neighbors from the selected
   * node.  Additionally, cut vertices are also depicted.
   *
   *@param g2d   graphics primitive
   *@param myrc  current render context
   *@param sel   current selection set
   */
  private void drawGraphInfo(Graphics2D g2d, RenderContext myrc, Set<String> sel) {
    if        (sel.size() == 1) { // Draw distances to... probably less than 4 to be useful
      // Make sure the selection equals a node in the graph
      String node = sel.iterator().next(); if (entity_to_wxy.containsKey(node) == false) return;

      // Run the algorithm
      DijkstraSingleSourceShortestPath ssp;
      if (last_ssp != null && node.equals(last_ssp_source)) ssp = last_ssp;
      else ssp = new DijkstraSingleSourceShortestPath(graph, graph.getEntityIndex(node));

      // Draw a number next to those numbers that are within 4 edges (arbitrary number...)
      for (int i=0;i<graph.getNumberOfEntities();i++) {
        int dist = (int) ssp.getDistanceTo(i);
        if (dist > 0 && dist <= 4) {
          Color color;
          switch (dist) {
            case 1:  color = RTColorManager.getColor("linknode", "nbor");
            case 2:  color = RTColorManager.getColor("linknode", "nbor+");
            case 3:  color = RTColorManager.getColor("linknode", "nbor++");
            case 4:  
            default: color = RTColorManager.getColor("linknode", "nbor+++");
          }
          String entity = graph.getEntityDescription(i);
          int    x      = entity_to_sx.get(entity),
                 y      = entity_to_sy.get(entity);
          String str    = "" + dist;
          clearStr(g2d, str, x - Utils.txtW(g2d,str)/2, y + Utils.txtH(g2d,str)/2, color, RTColorManager.getColor("label", "defaultbg"));
        }
      }

      // Cache the results so that repaints are faster...  need to worry about tears
      last_ssp = ssp; last_ssp_source = node;
    } else if (sel.size() == 2) { // Draw shortest path between two nodes
      // Make sure the selection equals a node in the graph
      String node0,node1;
      Iterator<String> it = sel.iterator(); node0 = it.next(); node1 = it.next();
      if (entity_to_wxy.containsKey(node0) == false || entity_to_wxy.containsKey(node1) == false) return;

      // Run the algorithm
      DijkstraSingleSourceShortestPath ssp;
      if      (last_ssp != null && last_ssp_source.equals(node0)) { ssp = last_ssp; }
      else if (last_ssp != null && last_ssp_source.equals(node1)) { ssp = last_ssp; String tmp = node0; node0 = node1; node1 = tmp; }
      else ssp = new DijkstraSingleSourceShortestPath(graph, graph.getEntityIndex(node0));

      // Get the path
      int path[] = ssp.getPathTo(graph.getEntityIndex(node1));
      if (path != null && path.length > 1) {
        Stroke orig_stroke = g2d.getStroke(); g2d.setColor(RTColorManager.getColor("annotate", "cursor")); g2d.setStroke(new BasicStroke(3.0f));
        for (int i=0;i<path.length-1;i++) {
          String graph_linkref  = graph.getLinkRef(path[i],path[i+1]),
                 graph_linkref2 = graph.getLinkRef(path[i+1],path[i]);
          String gui_linkref    = myrc.graphedgeref_to_link.get(graph_linkref),
                 gui_linkref2   = myrc.graphedgeref_to_link.get(graph_linkref2);
          Line2D line           = myrc.link_to_line.get(gui_linkref),
                 line2          = myrc.link_to_line.get(gui_linkref2);
          if      (line  != null) g2d.draw(line); 
          else if (line2 != null) g2d.draw(line2);
          else System.err.println("No Line Between \"" + graph.getEntityDescription(path[i]) + "\" and \"" + graph.getEntityDescription(path[i+1]) + "\"");
        }
        g2d.setStroke(orig_stroke);
      }

      // Cache the results so that repaints are faster...  need to worry about tears
      last_ssp = ssp; last_ssp_source = node0;
    } else if (sel.size() >= 3) { // Draw common neighbors
    }
  }

  /**
   * Draw dynamic labels for the node under the mouse as well as its neighbors.
   * - 2013-01-14 - added timers to prevent complicated graphs from taking minutes to redraw.
   *
   *@param g2d               graphics primitive
   *@param myrc              current render context
   *@param dyn_labeler_copy  node coordinate information to find geometry
   */
  private void drawDynamicLabels(Graphics2D g2d, RenderContext myrc, String dyn_labeler_copy) {
        long start_time = System.currentTimeMillis();
        // Get the node information for the one directly under the mouse
        String strs[] = new String[1]; String str;                   // need to keep the original node name...

        Set<String> primary_set = myrc.node_coord_set.get(dyn_labeler_copy); if (primary_set == null) return;

        if (primary_set.size() == 1) { // It's a single node -- just get the first string
          str = strs[0] = primary_set.iterator().next();
          // Figure out if it's multiline
          if (strs[0].length() >= (multilineThreshold()-2) && Utils.containsWhiteSpace(strs[0])) {
            strs = Utils.breakIntoMultiLine(strs[0], multilineThreshold()); 
          }
        } else {  // It's a group node -- summarize appropriately
          str = strs[0] = Utils.calculateCIDR(primary_set);
        }

        Point2D pt   = myrc.node_coord_lu.get(dyn_labeler_copy); if (pt    == null) return;
        Shape  shape = myrc.node_to_geom.get(dyn_labeler_copy);  if (shape == null) return;

        int txt_h = Utils.txtH(g2d, strs[0]), max_txt_w = Utils.txtW(g2d, strs[0]); int txt_w = max_txt_w;
        for (int i=0;i<strs.length;i++) { if (strs[i] == null) continue;
          txt_w = Utils.txtW(g2d,strs[i]); if (txt_w > max_txt_w) max_txt_w = txt_w;
          clearStr(g2d, strs[i], (int) (pt.getX() - txt_w/2), (int) (shape.getBounds().getMaxY() + (i+1)*txt_h), 
                   RTColorManager.getColor("label", "defaultfg"), RTColorManager.getColor("label", "defaultbg"));
        }
        Area already_taken = new Area(new Rectangle2D.Double(pt.getX()-max_txt_w/2,pt.getY()+txt_h,max_txt_w,txt_h*strs.length));

        // Draw the neighbors
        String      sxy = entity_to_sxy.get(str); if (sxy == null) return;
        Set<String> set = myrc.node_coord_set.get(sxy);
        if (set != null) {
          // First iteration is to make sure the actual geometrical shapes are in the "already_taken" area
          Iterator<String> it  = set.iterator();
          while (it.hasNext() && (System.currentTimeMillis() - start_time < 1000L)) {
            String node   = it.next();
            int    node_i = graph.getEntityIndex(node);
            for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) {
              int    nbor_i = graph.getNeighbor(node_i, i);
              String nbor   = graph.getEntityDescription(nbor_i);
                     sxy    = entity_to_sxy.get(nbor);
              if (sxy != null) {
                shape = myrc.node_to_geom.get(sxy);
                if (shape != null) {
                  g2d.setColor(RTColorManager.getColor("label", "major")); g2d.draw(shape);
                  Rectangle2D  bounds = shape.getBounds2D(); already_taken.add(new Area(bounds));
                }
              }
            }
          }
          // Second iteration actually places them
          it  = set.iterator();
          while (it.hasNext() && (System.currentTimeMillis() - start_time < 1000L)) {
            String node   = it.next();
            int    node_i = graph.getEntityIndex(node);
            for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) {
              int    nbor_i = graph.getNeighbor(node_i, i);
              String nbor   = graph.getEntityDescription(nbor_i);
                     sxy    = entity_to_sxy.get(nbor);
              if (sxy != null) {
                shape = myrc.node_to_geom.get(sxy);
                if (shape != null) {
                  g2d.setColor(RTColorManager.getColor("label", "major")); g2d.draw(shape);
                  Rectangle2D  bounds = shape.getBounds2D();

                  // Figure out the multiline issue
                  Rectangle2D nbor_rect = null; int nbor_rect_w, nbor_rect_h;
                  if (nbor.length() >= (multilineThreshold()-2) && Utils.containsWhiteSpace(nbor)) {
                    strs = Utils.breakIntoMultiLine(nbor, multilineThreshold()); if (strs.length > 4) { String four[] = new String[4]; for (int j=0;j<four.length;j++) four[j] = strs[j]; strs = four; }
                    max_txt_w = Utils.txtW(g2d,strs[0]); txt_w = Utils.txtW(g2d,strs[0]);
                    for (int j=0;j<strs.length;j++) { txt_w = Utils.txtW(g2d,strs[j]); if (txt_w > max_txt_w) max_txt_w = txt_w; }
                    nbor_rect = new Rectangle2D.Double(bounds.getCenterX() - max_txt_w/2, bounds.getMaxY() + 0.1*txt_h, max_txt_w, strs.length * txt_h);
                    nbor_rect_w = max_txt_w; nbor_rect_h = strs.length * txt_h;
                  } else {
                    txt_w     = Utils.txtW(g2d,nbor); 
                    txt_h     = Utils.txtH(g2d,nbor);
                    nbor_rect = new Rectangle2D.Double(bounds.getCenterX() - txt_w/2, bounds.getMaxY() + 0.1*txt_h, txt_w, txt_h);
                    nbor_rect_w = txt_w; nbor_rect_h = txt_h;
                  }

                  // Find a good location for the text rectangle -- needs to check for overlap with other rectangles
                  int    iteration = 0; boolean original = true; 
                  double dx        = bounds.getCenterX() - pt.getX(), dy = bounds.getCenterY() - pt.getY(); double len = Math.sqrt(dx*dx+dy*dy);
                  if (len < 0.001) { len = 1.0; dx = 0.0; dy = 1.0; } dx /= len; dy /= len; // Normalize the vector
                  double pdx = -dy, pdy = dx; // Perpendicular vectors
                  while (iteration < 20 && already_taken.intersects(nbor_rect)) { // search for a better location
                    double distance = 15 * iteration / 3, perp_distance = 10 * ((iteration % 3) - 1);
                    nbor_rect = new Rectangle2D.Double(bounds.getCenterX() -     txt_w/2 + dx*distance + pdx*perp_distance, 
                                                       bounds.getMaxY()    + 0.1*txt_h   + dy*distance + pdy*perp_distance, 
                                                       nbor_rect_w, nbor_rect_h);
                    iteration++; original = false;
                  }
                  if (original == false) g2d.drawLine((int) nbor_rect.getCenterX(), (int) nbor_rect.getCenterY(), (int) bounds.getCenterX(), (int) bounds.getCenterY());

                  if (nbor.length() >= (multilineThreshold()-2) && Utils.containsWhiteSpace(nbor)) {
                    strs = Utils.breakIntoMultiLine(nbor, multilineThreshold()); if (strs.length > 4) { String four[] = new String[4]; for (int j=0;j<four.length;j++) four[j] = strs[j]; strs = four; }
                    max_txt_w = Utils.txtW(g2d, strs[0]); txt_w = max_txt_w;
                    for (int j=0;j<strs.length;j++) {
                      txt_w = Utils.txtW(g2d,strs[j]); if (txt_w > max_txt_w) max_txt_w = txt_w;
                      clearStr(g2d, strs[j], (int) (nbor_rect.getCenterX() - txt_w/2), (int) (nbor_rect.getMinY() + txt_h + j*txt_h), 
                               RTColorManager.getColor("label", "defaultfg"), RTColorManager.getColor("label", "defaultbg"));
                    }
                  } else {
                    clearStr(g2d, nbor, (int) nbor_rect.getMinX(), (int) nbor_rect.getMaxY(), RTColorManager.getColor("label", "defaultfg"), RTColorManager.getColor("label", "defaultbg"));
                  }

                  already_taken.add(new Area(nbor_rect));
                }
              }
            }
          }
        }
      }

  /**
   * Render a cheat sheet of all of the shortcut commands and their variations.
   *
   *@param g2d graphics primitive
   */
  private void drawHelp(Graphics2D g2d) {
    Composite orig_comp = g2d.getComposite();
    g2d.setColor(RTColorManager.getColor("brush", "dim")); g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.8f)); g2d.fillRect(0,0,getWidth(),getHeight()); g2d.setComposite(orig_comp);
    int base_x = 5, base_y = Utils.txtH(g2d,"0");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Key",         "Normal",                  "Shift",                  "Ctrl",             "Ctrl-Shift");
    base_y = drawKeyCombo(g2d, base_x, base_y, "E",           "Expand Selection",        "Use Directed Graph",     "Common Nbors",     "Common Nbors Excl");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Space",       "Make One Hops Visible",   "Use Directed Graph",     "",                 "Add All Bundles On Visible Links");
    base_y = drawKeyCombo(g2d, base_x, base_y, "1 - 9",       "Select Degree",           "Subtract",               "Add",              "Intersect");
    base_y = drawKeyCombo(g2d, base_x, base_y, "0",           "Select Degree > 10",      "Subtract",               "Add",              "Intersect");
    base_y = drawKeyCombo(g2d, base_x, base_y, "R",           "Clean-up 1-Degrees",      "Fix Two Rows",           "Fix Two Cols",     "Color Layout");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Q",           "Invert Selection");
    base_y = drawKeyCombo(g2d, base_x, base_y, "X",           "Hide Selection");
    base_y = drawKeyCombo(g2d, base_x, base_y, "D",           "Sel Shape Under Mouse",   "Subtract",               "Add",              "Intersect");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Z",           "Sel Color Under Mouse",   "Subtract",               "Add",              "Intersect");
    base_y = drawKeyCombo(g2d, base_x, base_y, "M",           "Toggle Mode");
    base_y = drawKeyCombo(g2d, base_x, base_y, "G",           "Grid Layout Mode");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Y",           "Line Layout Mode");
    base_y = drawKeyCombo(g2d, base_x, base_y, "C",           "Circle Layout Mode");
    base_y = drawKeyCombo(g2d, base_x, base_y, "T",           "Group Nodes",             "Align Horizontally",    "Align Vertically",  "");
    base_y = drawKeyCombo(g2d, base_x, base_y, "S",           "Set/Clear Sticky Labels", "Subtract From",         "Add To",            "Select Sticky Labels");
    base_y = drawKeyCombo(g2d, base_x, base_y, "V",           "Toggle Node Size",        "Toggle Link Size",      "Toggle Node Color", "Toggle Link Color");
    base_y = drawKeyCombo(g2d, base_x, base_y, "L",           "Toggle Labels",           "Edge Templates",        "Node Legend",       "");
    base_y = drawKeyCombo(g2d, base_x, base_y, "W",           "Make One Hops Visible",   "Use Directed Graph");
    base_y = drawKeyCombo(g2d, base_x, base_y, "N",           "Add Note");
    base_y = drawKeyCombo(g2d, base_x, base_y, "A",           "Pan Mode");
    base_y = drawKeyCombo(g2d, base_x, base_y, "F",           "Fit");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Minus (-)",   "Zoom Out",                "Fit");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Plus (+)",    "Zoom In");
    base_y = drawKeyCombo(g2d, base_x, base_y, "Cursor Keys", "Shift Selected Nodes");
    base_y = drawKeyCombo(g2d, base_x, base_y, "< >",         "Rotate Selected Nodes", "15 Degs", "90 Degs",  "");
    base_y = drawKeyCombo(g2d, base_x, base_y, "{ }",         "Scale Selected Nodes",  "More",    "Even More", "");
    base_y = drawKeyCombo(g2d, base_x, base_y, "F[2-5]",      "Save Layout To Fx",     "Restore", "Delete",    "Restore");
    base_y = drawKeyCombo(g2d, base_x, base_y, "K",           "Select Landmarks");
    base_y = drawKeyCombo(g2d, base_x, base_y, "B",           "Filter Reports w/o Ents");
  }

  /**
   * For the help cheat sheet, draw the variations of commands (shift, alt, control) in different colors.
   *
   *@param g2d       graphics primitive
   *@param base_x    x position for string
   *@param base_y    y position for string
   *@param key       normal action
   *@param shft      shift action
   *@param ctrl      control action
   *@param ctrl_shft control-shift action
   *
   *@return new base y position
   */
  private int drawKeyCombo(Graphics2D g2d, int base_x, int base_y, String key, String norm, String shft, String ctrl, String ctrl_shft) {
    if (key       != null) { g2d.setColor(RTColorManager.getColor("label", "default")); g2d.drawString(key,       base_x, base_y); base_x += 100; /* Utils.txtW(g2d,key); */       }
    if (norm      != null) { g2d.setColor(RTColorManager.getColor("label", "major"));   g2d.drawString(norm,      base_x, base_y); base_x += 200; /* Utils.txtW(g2d,norm); */      }
    if (shft      != null) { g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString(shft,      base_x, base_y); base_x += 150; /* Utils.txtW(g2d,shft); */      }
    if (ctrl      != null) { g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString(ctrl,      base_x, base_y); base_x += 150; /* Utils.txtW(g2d,ctrl); */      }
    if (ctrl_shft != null) { g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString(ctrl_shft, base_x, base_y); base_x += 150; /* Utils.txtW(g2d,ctrl_shft); */ }
    return base_y + Utils.txtH(g2d, "0");
  }
  private int drawKeyCombo(Graphics2D g2d, int base_x, int base_y, String key, String norm) {
    return drawKeyCombo(g2d, base_x, base_y, key, norm, null, null, null); }
  private int drawKeyCombo(Graphics2D g2d, int base_x, int base_y, String key, String norm, String shft) {
    return drawKeyCombo(g2d, base_x, base_y, key, norm, shft, null, null); }

  /**
   * Choose the next node size for rendering.
   */
  public void nextNodeSize() { nextRBMI(node_sizes, new HashSet<JRadioButtonMenuItem>()); }

  /**
   * Choose the next node color for rending.
   */
  public void nextNodeColor() { nextRBMI(node_colors, new HashSet<JRadioButtonMenuItem>()); }

  /**
   * Choose the next link size for rendering.
   */
  public void nextLinkSize() { 
    Set<JRadioButtonMenuItem> excepts = new HashSet<JRadioButtonMenuItem>();
    for (int i=0;i<LINK_SZ_STRS.length;i++) {
      if      (link_sizes[i].getText().equals(LINK_SZ_CONDUCT))  excepts.add(link_sizes[i]);
      else if (link_sizes[i].getText().equals(LINK_SZ_CLUSTERP)) excepts.add(link_sizes[i]);
    }
    nextRBMI(link_sizes, excepts); 
  }

  /**
   * Choose the next link color for rendering.
   */
  public void nextLinkColor() { nextRBMI(link_colors, new HashSet<JRadioButtonMenuItem>()); }

  /**
   * Generic method to move to the next RBMI.
   */
  protected void nextRBMI(JRadioButtonMenuItem rbmis[], Set<JRadioButtonMenuItem> excepts) {
    String str = findSelected(rbmis);
    int i = 0; while (i < rbmis.length && rbmis[i].getText().equals(str) == false) i++;
    i = (i+1)%rbmis.length;
    while (excepts.contains(rbmis[i])) i = (i+1)%rbmis.length;
    rbmis[i].setSelected(true);
  }

  /**
   * Render contexts produce the visualization and maintain the state for the current rendering.
   * They are generated when the visualization changes and are used to correlate bundles (records)
   * with rendered geometry.
   */
    public class RenderContext extends RTRenderContext {
      /**
       * Data set to render
       */
      Bundles      bs; 
      /**
       * Width of rendering (in pixels)
       */
      int          w, 
      /**
       * Height of rendering (in pixels)
       */
                   h; 
      /**
       * Current view port extents
       */
      Rectangle2D  ext; 
      /**
       * Global header for coloring the visualization
       */
      String       color_by, 
      /**
       * Global header for counting objects within the visualization
       */
                   count_by; 
      /**
       * Node color setting
       */
      NodeColor    node_color; 
      /**
       * Node size setting
       */
      NodeSize     node_size; 
      /**
       * Link color setting
       */
      LinkColor    link_color; 
      /**
       * Link size setting
       */
      LinkSize     link_size;
      /**
       * Multiline truncation setting
       */
      int          ml_truncate,
      /**
       * Multiline threshold setting
       */
                   ml_threshold;
      /**
       * Graph background image option
       */
      GraphBG      graph_bg;
      /**
       * Flag to draw link directions
       */
      boolean      arrows, 
      /**
       * Flag to draw curves
       */
                   curves,
      /**
       * Flag for color parts
       */
                   color_parts,
      /**
       * Flag to enable link transparency
       */
                   link_trans, 
      /**
       * Flag to enable timing marks
       */
                   timing, 
      /**
       * Flag to enable node labels
       */
                   draw_node_labels,
      /**
       * Flag to enable link labels
       */
                   draw_link_labels;
      /**
       * Node labels to render
       */
      java.util.List<String> entity_labels, 
      /**
       * For labeling by color, list of which strings to use.  Only uses the first one.
       */
                             entity_color, 
      /**
       * Edge labels to render
       */
                             bundle_labels;

      /**
       * Method to accumulate values for links.
       * - link_counter_context: link=string_based rep "x0|y0|x1|y1"
       */
      BundlesCounterContext link_counter_context, 
      /**
       * Method to accumulate values for nodes.
       * - node_counter_context:   node=string_based rep "x|y"
       */
                            node_counter_context,
      /**
       * Method to accumulate values for entities.
       * - entity_counter_context: entity=string_based rep
       */
                            entity_counter_context;

      /**
       * Lookup from screen coordinates to the underlying entities.
       * - node_coord_set:         node="x|y" to the entity set ("ip", "ip", ...)
       */
      Map<String,Set<String>> node_coord_set = new HashMap<String,Set<String>>();

      /**
       * Lookup from the screen version of coordinates to the double version.
       * - node_coord_lu:          node="x|y" to the point on the screen
       */
      Map<String,Point2D>         node_coord_lu  = new HashMap<String,Point2D>();

      /**
       * Set of all rendered shapes.
       */
      Set<Shape>                   all_shapes            = new HashSet<Shape>();

      /**
       * Set of all visible entities within the current rendering.
       */
      Set<String>                  visible_entities      = new HashSet<String>();

      /**
       * Map from the line shape to the underlying bundles.
       */
      Map<Line2D, Set<Bundle>> line_to_bundles       = new HashMap<Line2D, Set<Bundle>>();

      /**
       * Map from the node shape to the underyling bundles.
       */
      Map<Shape,  Set<Bundle>> geom_to_bundles       = new HashMap<Shape,  Set<Bundle>>();

      /**
       * Map from the bundles (application records) to the geometrical shapes.
       */
      Map<Bundle, Set<Shape>>  bundle_to_shapes      = new HashMap<Bundle, Set<Shape>>();

      /**
       * Map from the string version of the screen coordinates to the geometry.
       * - node_to_geom:           node="x|y" to the geometry on the screen
       */
      Map<String, Shape>           node_to_geom          = new HashMap<String, Shape>();

      /**
       * Map from the link (string rep of a line) to the geometrical line.
       * - link_to_line:           link "x0|y0|x1|y1" to the actual screen line geometry
       */
      Map<String, Line2D>          link_to_line          = new HashMap<String, Line2D>();

      /**
       * Map from the geometrical line back to the string rep of the line (link)
       */
      Map<Line2D, String>          line_to_link          = new HashMap<Line2D, String>();

      /**
       * Map from the string based line (link) to the {@link MyGraph} edge references.
       * - link_to_graphedgerefs   link "x0|y0|x1|y1" to the graph edges references
       */
      Map<String, Set<String>> link_to_graphedgerefs = new HashMap<String, Set<String>>();

      /**
       * Map from the {@link MyGraph} edge references back to the string based line (link)
       * - graphedgerefs_to_link   graph edge references to the link "x0|y0|x1|y1"
       */
      Map<String, String>          graphedgeref_to_link  = new HashMap<String, String>();

      /**
       * Map from the string based link to the edge styles
       */
      Map<String, Set<String>> line_ref_to_styles    = new HashMap<String, Set<String>>();

      /**
       * Flag to render the active edge templates
       */
      boolean draw_edge_templates = false;

      /**
       * Flag to render the node legend
       */
      boolean draw_node_legend    = false;

      /**
       * Construct the render context based on the specified parameters.
       *
       *@param bs            Data set to render
       *@param w             Width of rendering (in pixels)
       *@param h             Height of rendering (in pixels)
       *@param ext           Current view port extents
       *@param color_by      Global header for coloring the visualization
       *@param count_by      Global header for counting objects within the visualization
       *@param node_color    Node color setting
       *@param node_size     Node size setting
       *@param link_color    Link color setting
       *@param link_size     Link size setting
       *@param ml_truncate   Multiline truncation setting
       *@param ml_threshold  Multiline threshold setting
       *@param graph_bg      Graph background image option
       *@param arrows        Flag to draw link directions
       *@param link_trans    Flag to enable link transparency
       *@param curves        Flag to enable link curves
       *@param color_parts   Flag to enable coloring the link in color parts
       *@param timing        Flag to enable timing marks
       *@param draw_node_labels   Flag to enable node labels
       *@param draw_link_labels   Flag to enable link labels
       *@param entity_labels Node labels to render
       *@param entity_color  For labeling by color, list of which strings to use.  Only uses the first one.
       *@param bundles_label Edge labels to render
       */
      public RenderContext(short id, Bundles bs, Rectangle2D ext, String color_by, String count_by,
                           NodeColor node_color, NodeSize node_size, LinkColor link_color, LinkSize link_size,
                           int ml_truncate, int ml_threshold,
                           boolean arrows, boolean link_trans, boolean curves, boolean color_parts, boolean timing, 
                           boolean draw_node_labels, boolean draw_link_labels, 
                           java.util.List<String> entity_labels, java.util.List<String> entity_color, java.util.List<String> bundle_labels, GraphBG graph_bg,
                           boolean draw_edge_templates, boolean draw_node_legend,
                           int w, int h, RenderContext last_rc) {
        timer_a = System.currentTimeMillis();
        render_id = id; this.bs = bs; this.w = w; this.h = h; this.ext = ext;
        this.node_color = node_color; this.node_size = node_size; this.link_color = link_color; this.link_size = link_size;
        this.ml_truncate = ml_truncate; this.ml_threshold = ml_threshold;
        this.arrows = arrows; this.link_trans = link_trans; this.curves = curves; this.color_parts = color_parts; this.timing = timing;
        this.draw_node_labels = draw_node_labels; this.draw_link_labels = draw_link_labels;
        this.entity_labels = entity_labels; this.entity_color = entity_color; this.bundle_labels = bundle_labels; this.graph_bg = graph_bg;
        this.color_by = color_by; this.count_by = count_by;
        this.draw_edge_templates = draw_edge_templates; this.draw_node_legend = draw_node_legend;
        // Create an initial no map set with all of the bundles
        Set<Bundle> no_maps = new HashSet<Bundle>(); no_maps.addAll(bs.bundleSet());
/*
    System.err.println("Extents:\tx=" + ext.getX() + "\ty=" + ext.getY() + 
                       "\tcx=" + ext.getCenterX() + "\tcy=" + ext.getCenterY() +
                       "\tw=" + ext.getWidth() + "\th=" + ext.getHeight());
*/
        // Create the counter contexts
        link_counter_context   = new BundlesCounterContext(bs, count_by, color_by);
        node_counter_context   = new BundlesCounterContext(bs, count_by, color_by);
        entity_counter_context = new BundlesCounterContext(bs, count_by, color_by);
        if (digraph.getNumberOfEntities() == 0) { return; }
        // Force a transform
        if (last_rc == null || last_rc.getRCWidth() != w || last_rc.getRCHeight() != h) transform();
        // transform();
        // Figure out which edges are active
        Iterator<Tablet> it_t = bs.tabletIterator();
        while (it_t.hasNext() && (currentRenderID() == getRenderID() || getRenderID() == (short) -1)) { // RenderID used to abort early
          Tablet tablet = it_t.next();
          // Determine if the tablet can be used here
          boolean fills_relationship = (graph.getNumberOfEntities() > 0) && (active_relationships.size() == 0);
          if (fills_relationship == false) for (int i=0;i<active_relationships.size();i++) {
            StringTokenizer st = new StringTokenizer(active_relationships.get(i), BundlesDT.DELIM);
            String fm_e, to_e;
            fm_e = Utils.decFmURL(st.nextToken()); st.nextToken() /* icon */ ; st.nextToken(); /* typed */
            to_e = Utils.decFmURL(st.nextToken());
            if (KeyMaker.tabletCompletesBlank(tablet, fm_e) && KeyMaker.tabletCompletesBlank(tablet, to_e)) fills_relationship = true;
          }
          boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          // If it fills the relationship, add to the edges
          if (fills_relationship) {
            Iterator<Bundle> it_b = tablet.bundleIterator(); no_maps.removeAll(tablet.bundleSet());
            while (it_b.hasNext() && (currentRenderID() == getRenderID() || getRenderID() == (short) -1)) { // RenderID used to abort early
              Bundle bundle = it_b.next();
              Iterator<String> it_l = digraph.linkUnRefIterator(bundle); if (it_l.hasNext() == false) no_maps.add(bundle);
              while (it_l.hasNext()) {
                String graph_edge_ref  = it_l.next();
                // Node counts
                String fm_entity  = digraph.getEntityDescription(digraph.linkRefFm(graph_edge_ref)), 
                       to_entity  = digraph.getEntityDescription(digraph.linkRefTo(graph_edge_ref));
                visible_entities.add(fm_entity); visible_entities.add(to_entity);
                String node_coord;
                // - fm entity
                node_coord = entity_to_sxy.get(fm_entity); 
                if (node_coord_set.containsKey(node_coord) == false) { 
                  node_coord_set.put(node_coord,new HashSet<String>());
                  node_coord_lu.put(node_coord,new Point2D.Float(entity_to_sx.get(fm_entity),entity_to_sy.get(fm_entity)));
                }
                node_coord_set.get(node_coord).add(fm_entity);
                if (tablet_can_count) {
                  node_counter_context.count(bundle, node_coord);
                  entity_counter_context.count(bundle, fm_entity);
                }
                // - to entity
                node_coord = entity_to_sxy.get(to_entity); 
                if (node_coord_set.containsKey(node_coord) == false) {
                  node_coord_set.put(node_coord,new HashSet<String>());
                  node_coord_lu.put(node_coord,new Point2D.Float(entity_to_sx.get(to_entity),entity_to_sy.get(to_entity)));
                }
                node_coord_set.get(node_coord).add(to_entity);
                if (tablet_can_count) {
                  node_counter_context.count(bundle, node_coord);
                  entity_counter_context.count(bundle, to_entity);
                }
                // Link counts
                String line_ref  = entity_to_sxy.get(fm_entity) + BundlesDT.DELIM + entity_to_sxy.get(to_entity);
                Line2D line      = new Line2D.Float(entity_to_sx.get(fm_entity),entity_to_sy.get(fm_entity),entity_to_sx.get(to_entity),entity_to_sy.get(to_entity));
                if (link_to_line.keySet().contains(line_ref) == false) { link_to_line.put(line_ref, line); line_to_link.put(line, line_ref); }

                if (link_to_graphedgerefs.containsKey(line_ref) == false) link_to_graphedgerefs.put(line_ref,new HashSet<String>());
                link_to_graphedgerefs.get(line_ref).add(graph_edge_ref);
                graphedgeref_to_link.put(graph_edge_ref,line_ref);

                if (tablet_can_count) link_counter_context.count(bundle, line_ref);
                if (line_ref_to_styles.containsKey(line_ref) == false) line_ref_to_styles.put(line_ref, new HashSet<String>());
                line_ref_to_styles.get(line_ref).addAll(digraph.getLinkStyles(graph_edge_ref));
              }
            }
          }
        }
        // Add all to the no mapping set
        // System.err.println("RTGraph.no_maps().size = " + no_maps.size());
        addToNoMappingSet(no_maps);
        // Stop the timer
        timer_b = System.currentTimeMillis();
      }

      /**
       * Return if graph information is being drawn.
       *
       *@return true for rendering graph information
       */
      public boolean provideGraphInfo() { return node_size == NodeSize.GRAPHINFO; }

      /**
       * Tests to see if a specific string is a link in the current context.
       *
       *@return true if string is a link (edge)
       */
      public boolean isLink(String str) { return link_to_line.keySet().contains(str); }

      /**
       * Reverses a link so that it can be used to test for the other direction.
       *
       *@param str link
       *
       *@return reversed link
       */
      public String  reverseLink(String str) {
        int pip = str.indexOf(BundlesDT.DELIM, str.indexOf(BundlesDT.DELIM)+1);
        return str.substring(pip+1,str.length()) + BundlesDT.DELIM + str.substring(0,pip);
      }

      @Override
      public int             getRCWidth()      { return w; }
      @Override
      public int             getRCHeight()     { return h; }

      /**
       * Returns that this view represents entities directly.
       *
       *@return true
       */
      @Override
      public boolean         hasEntityShapes() { return true; }

      /**
       * Returns the entity shapes associated with the substrings extracted from a text.
       *
       *@param subtexts extracted entities to match
       *
       *@return shapes for the matching entities
       */
      @Override
      public Set<Shape>  entityShapes(Set<SubText> subtexts) {
        Set<String> straights = new HashSet<String>();
        // Go through the subtexts -- separate them into CIDRs and straights
        Iterator<SubText> it_sub = subtexts.iterator();
        while (it_sub.hasNext()) {
          SubText subtext = it_sub.next();
          // Only match entity subtexts
          if (subtext instanceof Entity) {
            // Entity entity = (Entity) subtext;
            straights.add(subtext.toString());
          }
        }
        return entityShapesInternal(straights);
      }

      /**
       * Return the entity shapes associated with a set of strings.
       *
       *@param superset strings to match
       *
       *@return shapes for the matching entities
       */
      public Set<Shape> entityShapesInternal(Set<String> superset) {
        Set<String> filtered = filterEntities(superset); Set<Shape> shapes = new HashSet<Shape>();
        Iterator<String> it = filtered.iterator();
        while (it.hasNext()) {
          String node_coord = entity_to_sxy.get(it.next());
          if (node_coord != null) {
            Shape shape = node_to_geom.get(node_coord);
            if (shape != null) shapes.add(shape);
          }
        }
        return shapes;
      }

      /**
       * Return a set of just the entities within this link-node view.
       *
       *@param superset set to check against
       *
       *@return intersecting set
       */
      public Set<String> filterEntities(Set<String> superset) {
        Set<String> set = new HashSet<String>(); set.addAll(superset);
        // Check to see if any of the relationships are typed...  if so, we'll have to do the typed conversions...
        for (int i=0;i<active_relationships.size();i++) {
          String relationship = active_relationships.get(i);
          if (relationshipFromTyped(relationship)) addTypedStrings(set, relationshipFromHeader(relationship));
          if (relationshipToTyped(relationship))   addTypedStrings(set, relationshipToHeader(relationship));
        } 
        // Retain all...
        set.retainAll(visible_entities); // Filter to what's visible in this view
        return set;
      }

      /**
       * For all of the strings in the set, add the prefix hdr for easier set matching.  This is a fix for the
       * occurence of the field header as part of the node name.
       *
       *@param set set of strings
       *@param hdr prepend the hdr for typing
       */
      private void addTypedStrings(Set<String> set, String hdr) {
        BundlesG globals = getRTParent().getRootBundles().getGlobals();  Set<String> to_add = new HashSet<String>();
        Set<BundlesDT.DT> datatypes = globals.getFieldDataTypes(globals.fieldIndex(hdr));

        // In transformed datatypes (e.g., dstip|ORG), the datatypes lookup fails and a null is returned.  I'm
        // unsure if the following will further break the selection/filtered data views -- the underlying assumption
        // in the original implementation was that all of the entities were strongly typed...  however, for IP Org
        // lookups, this is clearly not the case.
        if (datatypes == null) return;

        Iterator<BundlesDT.DT> it_dt = datatypes.iterator();
        while (it_dt.hasNext()) {
          BundlesDT.DT dt = it_dt.next();
          Iterator<String> it = set.iterator();
          while (it.hasNext()) {
            String str = it.next();
            if (BundlesDT.stringIsType(str, dt)) to_add.add(hdr + BundlesDT.DELIM + str);
          }
        }
        set.addAll(to_add);
      }

      /**
       * Draw the K-Core analysis for the the current graph.
       *
       *@param g2d graphics primitive
       *@param bi  buffered image for the rendering
       */
      protected void drawKCores(Graphics2D g2d, BufferedImage bi) {
        // Map<String,Integer> kcore_lu = GraphUtils.kCore(graph);
        // RTGraphPanel rt_graph_panel = (RTGraphPanel) getRTPanel();
        GraphUtils.renderKCores(g2d, bi, entity_to_wxy, new UniGraph(graph), getWorldToScreenTransform(), visible_entities);
      }

      /**
       * Draw the specified background on the view prior to rendering the link node graph.
       *
       *@param g2d  graphics primitive
       *@param type type of background to draw
       */
      protected void drawGeoBackground(Graphics2D g2d, GraphBG type) {
        // Get the shapes, make sure we have them
        ShapeFile sf = GeoData.getInstance().getShapeFile(); if (sf == null) return;
        // Save original graphics context
        AffineTransform orig_trans  = g2d.getTransform();
        Stroke          orig_stroke = g2d.getStroke();
        // Set the transform 
        g2d.scale(getRCWidth()/ext.getWidth(),-getRCHeight()/ext.getHeight());
        g2d.translate(-ext.getX(), ext.getY());
        // Based on the type, draw the geo background
        if (type == GraphBG.GEO_FILL) {
          // Draw the oceans
          g2d.setColor(RTColorManager.getColor("linknode", "ocean"));
          g2d.fillRect(-180,-90,360,180);
          // Fill the continents
          g2d.setColor(RTColorManager.getColor("background", "default"));
          sf.fill(g2d);
          // Color in the lines
          g2d.setColor(RTColorManager.getColor("linknode", "ocean"));
          g2d.setStroke(new BasicStroke((float) (0.0004*ext.getWidth())));
          sf.draw(g2d);
        } else if (type == GraphBG.GEO_TOUCH) {
          g2d.setStroke(new BasicStroke((float) (0.0008*ext.getWidth())));
          // Get the countries overlapping with the points
          Set<CCShapeRec>  recs = GeoData.getInstance().containingCountries(entity_to_wxy); 
          // Draw only them...
          Iterator<CCShapeRec> it   = recs.iterator();
          while (it.hasNext()) {
            CCShapeRec rec = it.next();
            Color color        = RTColorManager.getColor(rec.getName());
            g2d.setColor(color);
            rec.getShapeRec().fill(g2d);
          }
        } else {
          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          g2d.setStroke(new BasicStroke((float) (0.0008*ext.getWidth())));
          sf.draw(g2d);
        }
        // Reset the transform
        g2d.setStroke(orig_stroke);
        g2d.setTransform(orig_trans);
      }

      /**
       * Draw the background.
       *
       *@param g2d graphics primitive
       */
      protected void drawBackground(Graphics2D g2d, BufferedImage bi) {
        switch (graph_bg) {
          /* ========================================================= */
          case GEO_FILL:
          case GEO_OUT:
          case GEO_TOUCH: drawGeoBackground(g2d, graph_bg); break;
          /* ========================================================= */
          case KCORES:    drawKCores(g2d, bi); break;
          default:        /* Draw nothing */
        }
      }

      /**
       * Cached copy of the rendered image
       */
      BufferedImage base_bi;

      /**
       * Timers for monitoring the performance of the renderer
       */
      long timer_a, timer_b, timer_c, timer_d;

      /**
       * Render the image.
       */
      @Override
      public BufferedImage getBase() {
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          timer_c = System.currentTimeMillis();
          base_bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
          g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);
          // Draw the background
          if (graph_bg != GraphBG.NONE) { drawBackground(g2d, base_bi); }
          // Draw the links
          if (link_size != LinkSize.INVISIBLE) drawLinks(g2d);
          // Draw the nodes
          if (node_size != NodeSize.INVISIBLE) drawNodes(g2d);
          // Draw the legend
          if (draw_edge_templates) drawEdgeTemplates(g2d);
          if (draw_node_legend)    drawNodeLegend(g2d);
          // Provide stats on render time
          timer_d = System.currentTimeMillis();
          g2d.setColor(RTColorManager.getColor("label", "default")); 
          String  str = "RTime : " + ((timer_b - timer_a) + (timer_d - timer_c)); 
          g2d.drawString(str,w - Utils.txtW(g2d,str),Utils.txtH(g2d,"0"));
          // Let user know if the graph is limited
          if (retained_nodes != null && retained_nodes.size() > 0) {
            clearStr(g2d, "Limited", w - Utils.txtW(g2d,str), 2*Utils.txtH(g2d,"0"), RTColorManager.getColor("label", "errorfg"), RTColorManager.getColor("label", "errorbg"));
          }
          // Clean up
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }

      /**
       * Render the edge headers for the displayed graph.  This is useful for keeping
       * track of which headers are already being mapped to edges.
       *
       *@param g2d graphics primitive
       */
      public void drawEdgeTemplates(Graphics2D g2d) {
        Iterator<String> it = active_relationships.iterator();
        int              y  = getRCHeight() - 2 * Utils.txtH(g2d, "0");
        while (it.hasNext()) {
          String str    = it.next();
          String fm_hdr = relationshipFromHeader(str),
                 to_hdr = relationshipToHeader(str);
          g2d.setColor(RTColorManager.getColor("label", "default"));
          g2d.drawString(fm_hdr + " => " + to_hdr, 5, y);
          y -= Utils.txtH(g2d, "0");
        }
      }

      /**
       * Render the legend for the node shapes.
       *
       *@param g2d graphics primitive;
       */
      public void drawNodeLegend(Graphics2D g2d) {
        Iterator<String> it    = active_relationships.iterator();
        int              y     = getRCHeight() - 2 * Utils.txtH(g2d, "0");
        int              max_w = 0;
        while (it.hasNext()) {
          String str      = it.next();
          int    fm_hdr_w = Utils.txtW(g2d, relationshipFromHeader(str)),
                 to_hdr_w = Utils.txtW(g2d, relationshipToHeader(str));
          if (fm_hdr_w > max_w) max_w = fm_hdr_w;
          if (to_hdr_w > max_w) max_w = to_hdr_w;
        }
                         it    = active_relationships.iterator();
        int              x     = getRCWidth() - max_w - 5;
        Set<String>      drawn = new HashSet<String>();
        while (it.hasNext()) {
          String str    = it.next();
          String fm_hdr  = relationshipFromHeader(str),
                 fm_icon = relationshipFromIcon(str),
                 to_hdr  = relationshipToHeader(str),
                 to_icon = relationshipToIcon(str);
          g2d.setColor(RTColorManager.getColor("label", "default"));
          Shape  shape; String key;
          key = fm_hdr + BundlesDT.DELIM + fm_icon;
          if (drawn.contains(key) == false) {
            shape = Utils.shape(Utils.parseSymbol(fm_icon), x - 10, y - 6, 6); g2d.draw(shape); g2d.drawString(fm_hdr, x, y); y -= Utils.txtH(g2d, "0");
            drawn.add(key);
          }
          key = to_hdr + BundlesDT.DELIM + to_icon;
          if (drawn.contains(key) == false) {
            shape = Utils.shape(Utils.parseSymbol(to_icon), x - 10, y - 6, 6); g2d.draw(shape); g2d.drawString(to_hdr, x, y); y -= Utils.txtH(g2d, "0");
            drawn.add(key);
          }
        }
      }

      /**
       * Abstract class to color links.
       */
      abstract class LinkColorer { 
        /**
         * Based on the link string, return the appropriate color of that link (edge/line).
         *
         *@param link string version of edge
         *
         *@return color for rendering the edge on the visualization
         */
        abstract Color linkColor(String link); 
      }

      /**
       * Fixed class for coloring the links in a constant color.
       */
      class FixedLinkColorer extends LinkColorer { Color color;
                                                   public FixedLinkColorer(Color color) { this.color = color; }
                                                   public Color linkColor(String link) { return color; } };

      /**
       * Variable class for coloring the links based on the counter context results.
       */
      class VaryLinkColorer extends LinkColorer {  public Color linkColor(String link) { return link_counter_context.binColor(link); } }

      /**
       * Colors the link according to the entity types of the two nodes
       */
      class NodeTypesLinkColorer extends LinkColorer {
        public Color linkColor(String link) {
          String ref   = link_to_graphedgerefs.get(link).iterator().next();

          String fm    = graph.getEntityDescription(graph.linkRefFm(ref));
          String to    = graph.getEntityDescription(graph.linkRefTo(ref));

          String fm_et = BundlesDT.getEntityDataType(fm).toString(),
                 to_et = BundlesDT.getEntityDataType(to).toString();

          // System.err.println(link + " | " + fm + " (" + fm_et + ") => " + to + " (" + to_et + ")");

          return RTColorManager.getColor(fm_et + " => " + to_et);
        }
      }

      /**
       * Method to draw the pre-calculated links onto the screen.  Handles coloring, style, etc.
       *
       *@param g2d graphics primitive
       */
      protected void drawLinks(Graphics2D g2d) {
        Rectangle2D screen = new Rectangle2D.Float(0,0,w,h);
        // Figure out the stroke, color, transparency
        Stroke orig_stroke = g2d.getStroke(); Composite orig_composite = g2d.getComposite();
        LinkSizer sizer = null; LinkColorer colorer = null; float default_size = 1.0f;
        switch (link_size) {
          case INVISIBLE: return;
          case THIN:      g2d.setStroke(new BasicStroke(default_size = 0.4f)); break;
          case THICK:     g2d.setStroke(new BasicStroke(default_size = 2.5f)); break;
          case NORMAL:    g2d.setStroke(new BasicStroke(default_size = 1.0f)); break;
          case VARY:      sizer = new VaryLinkSizer();          break;
          case CONDUCT:   sizer = new ConductanceLinkSizer();   break;
          case CLUSTERP:  sizer = new ClusterProbSizer();       break;
          default:        throw new RuntimeException("Unknown Link Size " + link_size);
        }
        switch (link_color) {
          case GRAY:       colorer = new FixedLinkColorer(RTColorManager.getColor("linknode", "edge"));       break;
          case WHITE:      colorer = new FixedLinkColorer(RTColorManager.getColor("linknode", "edge-white")); break;
          case DARKGRAY:   colorer = new FixedLinkColorer(RTColorManager.getColor("linknode", "edge-dark"));  break;
          case VARY:       colorer = new VaryLinkColorer();                                                   break;
          case NODE_TYPES: colorer = new NodeTypesLinkColorer();                                              break;
          default:         throw new RuntimeException("Unknown Link Color " + link_color);
        }

        // Enable transparency if requested
        if (link_trans) { g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.4f)); }
        else            { g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,0.8f)); }

        // Make the label maker
        LabelMaker lm = null;
        if (draw_link_labels) lm = new LabelMaker(bundle_labels, link_counter_context);

        // - Create the dashes
        float dashes[]    = null;
        float long_ds[]   = new float[2],
              dotted_ds[] = new float[2],
              alter_ds[]  = new float[4];
        long_ds[0]   = 10.0f; long_ds[1]   = 5.0f;
        dotted_ds[0] = 1.0f;  dotted_ds[1] = 5.0f;
        alter_ds[0]  = 10.0f; alter_ds[1]  = 5.0f; alter_ds[2] = 1.0f; alter_ds[3] = 5.0f;
        // Go through the links and draw them
        Iterator<String> it = link_counter_context.binIterator();
        while (it.hasNext()) {
          String link  = it.next(); if (link.equals(reverseLink(link))) continue;
          Line2D line  = link_to_line.get(link);
          if (line.intersects(screen)) {
            // Figure out the rendering...
            Set<String> styles = line_ref_to_styles.get(link);
            if (styles != null && styles.size() == 1) {
              String str = styles.iterator().next();
              if      (str.equals(STYLE_SOLID_STR))     dashes = null;
              else if (str.equals(STYLE_LONG_DASH_STR)) dashes = long_ds;
              else if (str.equals(STYLE_DOTTED_STR))    dashes = dotted_ds;
              else if (str.equals(STYLE_ALTERNATE_STR)) dashes = alter_ds;
              else System.err.println("Do Not Understand Line Style \"" + str + "\"");
            } else dashes = null;
            if      (sizer != null && dashes != null) g2d.setStroke(new BasicStroke(sizer.linkSize(link), BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));
            else if (sizer != null                  ) g2d.setStroke(new BasicStroke(sizer.linkSize(link)));
            else if (                 dashes != null) g2d.setStroke(new BasicStroke(default_size, BasicStroke.CAP_ROUND, BasicStroke.JOIN_BEVEL, 1.0f, dashes, 0.0f));
            else                                      g2d.setStroke(new BasicStroke(default_size));
            g2d.setColor(colorer.linkColor(link));

            // Draw curved arcs to help differentiate direction
            if (curves) {
              //
              // Calculate the arc
              //
              double dx     =  line.getX2() - line.getX1(),
                     dy     =  line.getY2() - line.getY1();
              double l      = Math.sqrt(dx*dx+dy*dy); if (l < 0.0001) l = 0.0001;
                     dx     = dx/l;
                     dy     = dy/l;
              double cx     = (line.getX2() + line.getX1())/2,
                     cy     = (line.getY2() + line.getY1())/2;
              double ctrl_x = cx - (l/8)*dy,
                     ctrl_y = cy + (l/8)*dx;
              g2d.draw(new QuadCurve2D.Double(line.getX1(), line.getY1(), ctrl_x, ctrl_y, line.getX2(), line.getY2()));

              //
              // Add a half arrow to aid with directionality
              //
                     ctrl_x = cx - (l/4)*dy;
                     ctrl_y = cy + (l/4)*dx;
                     dx     = ctrl_x - line.getX2();
                     dy     = ctrl_y - line.getY2();
                     l      = Math.sqrt(dx*dx+dy*dy); if (l < 0.0001) l = 0.0001;
                     dx     = dx/l;
                     dy     = dy/l;
              g2d.draw(new Line2D.Double(line.getX2(), line.getY2(), line.getX2() + dx*16.0, line.getY2() + dy*16.0));

            } else if (color_parts) {
              // 
              // -- code that shades the lines in three parts based on source and destination...
              //
              drawColorPartsLine(g2d, line);
            } else {
              //
              // Draw a straight line - where the actual drawing occurs
              //
              g2d.draw(line);
            }

            // Draw arrows if applicable
            if (arrows || timing) {
              double dx  = line.getX2() - line.getX1(),
                     dy  = line.getY2() - line.getY1();
              double len = Utils.length(dx,dy);
              if (len < 0.001) len = 0.001; dx = dx/len; dy = dy/len; double pdx = dy, pdy = -dx;
              if (arrows) {
                GeneralPath gp = new GeneralPath();
                // Triangle (best?... worst for performance)
                // - From "A User Study on Visualizing Directed Edges in Graphs", Danny Holten, Jarke J. van Wijk.  2009.
                //
                gp.moveTo(line.getX2(),         line.getY2());
                gp.lineTo(line.getX1() - 5*pdx, line.getY1() - 5*pdy);
                gp.lineTo(line.getX1() + 5*pdx, line.getY1() + 5*pdy);
                gp.closePath();
                g2d.fill(gp);
/*              // Mid Arrows (better)
                double midx = (line.getX1() + line.getX2())/2.0, midy = (line.getY1() + line.getY2())/2.0;
                g2d.draw(new Line2D.Double(midx - 5*dx, midy - 5*dy, midx - 10*dx + 5*pdx, midy - 10*dy + 5*pdy));
                g2d.draw(new Line2D.Double(midx - 5*dx, midy - 5*dy, midx - 10*dx - 5*pdx, midy - 10*dy - 5*pdy));
*/
/*              // End Arrows (okay)
                g2d.draw(new Line2D.Double(line.getX2() - 5*dx, line.getY2() - 5*dy, line.getX2() - 10*dx + 5*pdx, line.getY2() - 10*dy + 5*pdy));
                g2d.draw(new Line2D.Double(line.getX2() - 5*dx, line.getY2() - 5*dy, line.getX2() - 10*dx - 5*pdx, line.getY2() - 10*dy - 5*pdy));
*/
              }
              if (timing) {
                Iterator<Bundle> itb = link_counter_context.getBundles(link).iterator();

                Stroke pre_timing_stroke = g2d.getStroke();
                g2d.setStroke(new BasicStroke(1.0f));

                while (itb.hasNext()) {
                  Bundle bundle = itb.next();
                  if (bundle.hasTime()) {
                    double ratio = ((double) (bundle.ts0() - bs.ts0()))/((double) (bs.ts1dur() - bs.ts0()));
                    g2d.setColor(timing_marks_cs.at((float) ratio));
                    double x0 = line.getX1() + dx * len * 0.1 + dx * len * 0.8 * ratio,
                           y0 = line.getY1() + dy * len * 0.1 + dy * len * 0.8 * ratio;
                    // g2d.draw(new Line2D.Double(x0 - 4*pdx, y0 - 4*pdy, x0 + 4*pdx, y0 + 4*pdy));
                    g2d.draw(new Line2D.Double(x0, y0, x0 + 4*pdx, y0 + 4*pdy));
                    
                    // Draw the duration if appropriate
                    double ratio2 = ((double) (bundle.ts1() - bs.ts0()))/((double) (bs.ts1dur() - bs.ts0()));
                    if (len * (ratio2 - ratio) > 4) {
                      double x1 = line.getX1() + dx * len * 0.1 + dx * len * 0.8 * ratio2,
                             y1 = line.getY1() + dy * len * 0.1 + dy * len * 0.8 * ratio2,
                             xa = line.getX1() + dx * len * 0.1 + dx * (len-0.05*len) * 0.8 * ratio2,
                             ya = line.getY1() + dy * len * 0.1 + dy * (len-0.05*len) * 0.8 * ratio2;
                      g2d.draw(new Line2D.Double(x0 + 4*pdx, y0 + 4*pdy, x1 + 4*pdx, y1 + 4*pdy));
                      g2d.draw(new Line2D.Double(xa + 8*pdx, ya + 8*pdy, x1 + 4*pdx, y1 + 4*pdy));
                      // g2d.draw(new Line2D.Double(x1 + 4*pdx, y1 + 4*pdy, x1 + 2*pdx + 2*pdy, y1 + 2*pdy + 2*pdx));
                    }
                  }
                }

                g2d.setStroke(pre_timing_stroke);
              }
            }
            // Draw labels if applicable
            if (lm != null) lm.draw(g2d, link, (int) line.getBounds().getCenterX(), 
                                               (int) line.getBounds().getCenterY(), false);

            // Accounting for interactivity
            all_shapes.add(line);
            // line_to_link.put(line, link); // Doesn't seem to be needed -- replaced with another function...
            line_to_bundles.put(line, link_counter_context.getBundles(link));

            Iterator<Bundle> itb = link_counter_context.getBundles(link).iterator();
            while (itb.hasNext()) {
              Bundle bundle = itb.next();
              if (bundle_to_shapes.containsKey(bundle) == false) bundle_to_shapes.put(bundle, new HashSet<Shape>());
              bundle_to_shapes.get(bundle).add(line);
            }
          }
        }
        g2d.setComposite(orig_composite); g2d.setStroke(orig_stroke);
      }

      /**
       *
       */
      ColorScale timing_marks_cs = RTColorManager.getTemporalColorScale();


      /**
       * Render a line using color parts
       *
       *@param g2d    graphics 2d primitive
       *@param line   line to render
       */
      protected void drawColorPartsLine(Graphics2D g2d, Line2D line) {

        ColorPartsApproach approach = ColorPartsApproach.THIRDS3;

        double x0 = line.getX1(), x1 = line.getX2(),
               y0 = line.getY1(), y1 = line.getY2();

        Color color_a = null, color_b = null, color_c = null;

        color_a = new Color(0.1f, 0.1f, 0.6f);
        color_b = new Color(0.4f, 0.4f, 0.6f);
        color_c = new Color(0.9f, 0.9f, 0.9f);

        BrewerColorScale bcs = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 3, 0); // 0 or 1 only
        color_a = bcs.atIndex(0); color_b = bcs.atIndex(1); color_c = bcs.atIndex(2);

        switch (approach) {
          // Straight thirds
          case THIRDS:    { double xma = (x1 - x0) * 1.0 / 3.0 + x0, yma = (y1 - y0) * 1.0 / 3.0 + y0,
                                   xmb = (x1 - x0) * 2.0 / 3.0 + x0, ymb = (y1 - y0) * 2.0 / 3.0 + y0;
                            g2d.setColor(color_a); g2d.draw(new Line2D.Double(x0, y0, xma, yma));
                            g2d.setColor(color_b); g2d.draw(new Line2D.Double(xma, yma, xmb, ymb));
                            g2d.setColor(color_c); g2d.draw(new Line2D.Double(xmb, ymb, x1, y1)); } break;

          // 50-25-25
          case THIRDS2:   { double xma = (x1 - x0) * 1.0 / 2.0 + x0, yma = (y1 - y0) * 1.0 / 2.0 + y0,
                                   xmb = (x1 - x0) * 3.0 / 4.0 + x0, ymb = (y1 - y0) * 3.0 / 4.0 + y0;
                            g2d.setColor(color_a); g2d.draw(new Line2D.Double(x0, y0, xma, yma));
                            g2d.setColor(color_b); g2d.draw(new Line2D.Double(xma, yma, xmb, ymb));
                            g2d.setColor(color_c); g2d.draw(new Line2D.Double(xmb, ymb, x1, y1)); } break;

          // 25-50-25
          case THIRDS3:   { double xma = (x1 - x0) * 1.0 / 4.0 + x0, yma = (y1 - y0) * 1.0 / 4.0 + y0,
                                   xmb = (x1 - x0) * 3.0 / 4.0 + x0, ymb = (y1 - y0) * 3.0 / 4.0 + y0;
                            g2d.setColor(color_a); g2d.draw(new Line2D.Double(x0, y0, xma, yma));
                            g2d.setColor(color_b); g2d.draw(new Line2D.Double(xma, yma, xmb, ymb));
                            g2d.setColor(color_c); g2d.draw(new Line2D.Double(xmb, ymb, x1, y1)); } break;
        }
      }              

      /**
       * Abstract class to handle the render size of links (edges/lines).
       */
      abstract class LinkSizer { 
        /**
         * Based on the link, return the appropriate size for the line in the rendering.
         *
         *@param link link (string version of edge)
         *
         *@return line width for rendering
         */
        abstract float linkSize(String link); 
      }

      /**
       * Link size based on conductance on the edge - won't work with aggregates.
       */
      class ConductanceLinkSizer extends LinkSizer {
        double min, max;
        public ConductanceLinkSizer() { 
          if (conductance == null) conductance = new Conductance(graph, 100, 0.2); 
          min = conductance.getMin();
          max = conductance.getMax();
        }
        public float linkSize(String link) {
          Set<String> nodes         = new HashSet<String>();
          Set<String> graphedgerefs = link_to_graphedgerefs.get(link);
          Iterator<String> it = graphedgerefs.iterator(); while (it.hasNext()) {
            String ref = it.next();
            nodes.add(graph.getEntityDescription(graph.linkRefFm(ref)));
            nodes.add(graph.getEntityDescription(graph.linkRefTo(ref)));
          }
          if (nodes.size() != 2) return 0.5f;
          it = nodes.iterator(); String n0 = it.next(), n1 = it.next();
          double val = conductance.getResult(n0, n1);
          return (float) (0.5 + 5.0*(val - min)/(max - min));
        }
      }

      /**
       * Link size based on cluster probability between two nodes - won't work with aggregates.
       */
      class ClusterProbSizer extends LinkSizer {
        public ClusterProbSizer() { if (conductance == null) conductance = new Conductance(graph, 100, 0.2); }
        public float linkSize(String link) {
          Set<String> nodes         = new HashSet<String>();
          Set<String> graphedgerefs = link_to_graphedgerefs.get(link);
          Iterator<String> it = graphedgerefs.iterator(); while (it.hasNext()) {
            String ref = it.next();
            nodes.add(graph.getEntityDescription(graph.linkRefFm(ref)));
            nodes.add(graph.getEntityDescription(graph.linkRefTo(ref)));
          }
          if (nodes.size() != 2) return 0.5f;
          it = nodes.iterator(); String n0 = it.next(), n1 = it.next();
          double val = conductance.getClusterProbability(n0, n1);
          return (float) (0.5 + 5.0*val);
        }
      }

      /**
       * Use a variable link sizer that queries the counter context to determine with
       * width of the edge/line.
       */
      class VaryLinkSizer extends LinkSizer { public float linkSize(String link) { 
        return (float) (link_counter_context.totalNormalized(link) * 5.0 + 0.5);
      } }

      /**
       * Abstraction to handle the color of nodes.
       */
      abstract class NodeColorer { 
        /**
         * Return the color for a specific entity.
         *
         *@return color of entity
         */
        Color entityColor(String entity) { return Color.white; }

        /**
         * For the specified node, return the appropriate color for rendering that node.
         *
         *@param node node in graph (or collection of nodes)
         *
         *@return color for rendering the node
         */
        abstract Color nodeColor(String node); }

      /**
       * Use a fixed node color class to render the nodes
       */
      class FixedNodeColorer  extends NodeColorer    { Color color;
                                                       public FixedNodeColorer(Color color) { this.color = color; } 
                                                       public Color nodeColor(String node)  { return color; } }
      /**
       * Use a variable node color that draws from the render context for the node color.
       */
      class VaryNodeColorer   extends NodeColorer    {
        @Override
        public Color entityColor(String entity) {
          if (entity_counter_context.hasBin(entity)) return entity_counter_context.binColor(entity);
          else                                       return Color.white;
        }

        @Override
        public Color nodeColor(String node)  { return node_counter_context.binColor(node); } 
      }
       

      /**
       * Use a variable node color that uses the label results for the node color.
       */
      class LabelNodeColorer  extends NodeColorer    { 
        LabelMaker lm;
        public LabelNodeColorer()                 { lm = new LabelMaker(entity_color, node_counter_context); }

        @Override
        public Color entityColor(String entity)   {
          if (entity_to_sxy.containsKey(entity)) return lm.getColor(entity_to_sxy.get(entity)); 
          else                                   return RTColorManager.getColor("set", "multi");
        }

        @Override
        public Color      nodeColor(String node)  { return lm.getColor(node); } 
      }

      /**
       * Colorer that adjusts to the cluster coefficient for a node.
       */
      class ClusterCoefficientColorer extends NodeColorer {
        public ClusterCoefficientColorer()   { if (cluster_cos == null) cluster_cos = GraphUtils.clusterCoefficients(graph); }

        @Override
        public Color nodeColor(String node) {
          if (node_coord_set.get(node).size() > 1) return RTColorManager.getColor("set", "multi");
          else {
            String  n0         = node_coord_set.get(node).iterator().next();
            double coefficient = cluster_cos.get(n0);
            return RTColorManager.getLogColor(Math.pow(10,10*coefficient));
          }
        }
      }

      /**
       * Colorer that adjusts for the datatype of the node.
       */
      class DataTypeNodeColorer extends NodeColorer {
        @Override
        public Color entityColor(String entity) {
          String dt_str = "" + BundlesDT.getEntityDataType(entity);
          return RTColorManager.getColor(dt_str);
        }

        @Override
        public Color nodeColor(String node) { 
          Set<String> set = node_coord_set.get(node);
          if (set.size() == 0 || set.size() > 1) {
            return RTColorManager.getColor("set", "multi");
          } else {
            String entity = set.iterator().next();
            String dt_str = "" + BundlesDT.getEntityDataType(entity);
            return RTColorManager.getColor(dt_str);
          }
        }
      }

      /**
       * Lookup the geometrical representation of the node string version "x|y".
       *
       *@param node_coord string version of coordiante
       *
       *@return screen coordinate of node
       */
      public Point2D nodeToPoint(String node_coord) { return node_coord_lu.get(node_coord); }

      /**
       * Class to abstract the node shape information away.
       */
      abstract class NodeShaper  { 
        /**
         * For a specific node coordinate string, return the proper shape for rendering the node.
         *
         *@param node node coordinate string
         *@param g2d  graphics primitive
         *
         *@return shape of node(s)
         */
        abstract Shape nodeShape(String node, Graphics2D g2d); }

      /**
       * Fixed node shaper that renders the shape with a constant width/height.
       */
      class FixedNodeShaper extends NodeShaper {
        float size;
        public FixedNodeShaper(float size) { this.size = size; }
        public Shape nodeShape(String node, Graphics2D g2d) {
          if (node_coord_set.get(node).size() > 1) return cloverShape(node, size);
          else {
            Point2D point = nodeToPoint(node);
            Utils.Symbol symbol = entity_to_shape.get(node_coord_set.get(node).iterator().next());
            float x0 = (float) (point.getX() - size/2),
                  y0 = (float) (point.getY() - size/2);
            return Utils.shape(symbol,x0,y0,size); } } } 

      /**
       * Node shaper for small multiples
       */
      class SmallMultNodeShaper extends NodeShaper {
        NodeSize   node_sz;  // node size enum
        String     km_str;   // keymaker string

        int        total_bars,
                   bar_w,
                   bar_gap,
                   max_bar_h;

        final int  x_ins = 2, 
                   y_ins = 2;

        int        total_w = 10,
                   total_h = 10;

        String order[];

        public SmallMultNodeShaper(NodeSize node_sz) { 
          this.node_sz = node_sz;

          if (node_sz == NodeSize.SM_MONTH || node_sz == NodeSize.SM_DOW || node_sz == NodeSize.SM_HOUR) {
            if        (node_sz == NodeSize.SM_MONTH) { 
              total_bars = 12; bar_w = 2; bar_gap = 1; max_bar_h = 16; km_str = KeyMaker.BY_MONTH_STR;     order = sm_months;
            } else if (node_sz == NodeSize.SM_DOW)   { 
              total_bars =  7; bar_w = 3; bar_gap = 1; max_bar_h = 14; km_str = KeyMaker.BY_DAYOFWEEK_STR; order = sm_dows;
            } else                                   { 
              total_bars = 24; bar_w = 2; bar_gap = 0; max_bar_h = 20; km_str = KeyMaker.BY_HOUR_STR;      order = sm_hours;   node_sz = NodeSize.SM_HOUR;
            }
            total_w = x_ins + total_bars * bar_w + bar_gap * (bar_w - 1) + x_ins;
            total_h = y_ins + max_bar_h + y_ins;
          } else if (node_size == NodeSize.SM_STRAIGHT_SMALL) {
            total_w = x_ins + 32 + x_ins;
            total_h = y_ins + 16 + y_ins;
          } else if (node_size == NodeSize.SM_STRAIGHT_LARGE) {
            total_w = x_ins + 64 + x_ins;
            total_h = y_ins + 24 + y_ins;
          } else if (node_size == NodeSize.SM_PIE_SMALL) { 
            if (color_by == null) total_w = total_h = 12;
            else                  total_w = total_h = 32;
          } else if (node_size == NodeSize.SM_PIE_LARGE) { 
            if (color_by == null) total_w = total_h = 12;
            else                  total_w = total_h = 64;
          } else if (node_size == NodeSize.SM_XY_VISIBLE ||
                     node_size == NodeSize.SM_XY_VISIBLE_EQUAL ||
                     node_size == NodeSize.SM_XY_LOCAL ||
                     node_size == NodeSize.SM_XY_LOCAL_EQUAL) {
            total_w = 64;
            total_h = 32;
          }

          // If there's color, need to determine the global coloring ordering...
          if (color_by != null) { color_bcc = new BundlesCounterContext(bs, count_by, color_by, color_by); }
        }

        // Counter context for coloring -- specifically, the color ordering to make each small multiple consistent
        BundlesCounterContext color_bcc = null;

        /**
         *
         */
        public Shape nodeShape(String node, Graphics2D g2d) {
          Point2D point = nodeToPoint(node);
          return new Rectangle2D.Double(point.getX() - total_w/2, point.getY() - total_h/2, total_w, total_h);
        }

        /**
         * Render the small multiple node... choose the correct rendering path.
         */
        public BufferedImage render(Set<Bundle> set) {
          switch (node_size) {
            case SM_PIE_SMALL:
            case SM_PIE_LARGE:          return renderPie(set);

            case SM_XY_VISIBLE:         return XYRenderer.renderTimeInX(set, total_w, total_h, bs, true,  noCountByBundles(count_by), false, color_by);
            case SM_XY_VISIBLE_EQUAL:   return XYRenderer.renderTimeInX(set, total_w, total_h, bs, true,  noCountByBundles(count_by), true,  color_by);
            case SM_XY_LOCAL:           return XYRenderer.renderTimeInX(set, total_w, total_h, bs, false, noCountByBundles(count_by), false, color_by);
            case SM_XY_LOCAL_EQUAL:     return XYRenderer.renderTimeInX(set, total_w, total_h, bs, false, noCountByBundles(count_by), true,  color_by);

            case SM_STRAIGHT_SMALL:
            case SM_STRAIGHT_LARGE:     return renderContinuous(set);

            default:                    return renderDiscrete(set);
          }
        }

        /**
         * For the piechart mode.
         */
        public BufferedImage renderPie(Set<Bundle> set) {
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_ARGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          // Geometry
          double circle_x = 2, circle_y = 2, circle_d = total_w - 4;

          // If no color_by, then just make it a small dot
          if (color_by == null) { 
            g2d.setColor(RTColorManager.getColor("label","default")); g2d.fill(new Ellipse2D.Double(circle_x, circle_y, circle_d, circle_d));

          // Else, calculate the slices based on the the set
          } else {
            // Run through the records and place them into the pie slices
            BundlesCounterContext bcc = new BundlesCounterContext(bs, count_by, color_by, set, color_by);

            // Color bin ordering ... from global ordering (to make consistent)
            List<String> cbins = color_bcc.getColorBinsSortedByCount();

            double total      = bcc.totalCounts(),
                   deg_start  = 90.0,
                   from_zero  = 0.0;

            // Draw the color bins from greatest size to smallest (overall to make consistent)...
            // ... not every color bin may be present in the local counter context...
            // ... only draw up to 345 degrees... then make the rest just gray...
            int cbin_i = cbins.size()-1;
            while (cbin_i >= 0 && from_zero < 345.0) {
              String cbin = cbins.get(cbin_i);

              // Get the total for this bin... if it contains a count, render the slice
              if (bcc.hasBin(cbin)) { double total_bin = bcc.total(cbin); if (total_bin > 0.0) {
                // Incremental degrees
                double deg_inc = (total_bin / total) * 360.0;

                // Render
                g2d.setColor(RTColorManager.getColor(cbin));
                g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, deg_start, deg_inc, Arc2D.PIE));
                deg_start += deg_inc; from_zero += deg_inc;
              }
            } cbin_i--; }

            // Draw the left overs
            if (from_zero < 359.0) {
              g2d.setColor(RTColorManager.getColor("set", "multi"));
              g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, deg_start, 360.0 - from_zero, Arc2D.PIE));
            }
          }

          } finally { if (g2d != null) g2d.dispose(); }

          return bi;
        }

        /**
         * For the two continuous modes
         */
        public BufferedImage renderContinuous(Set<Bundle> set) {
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_RGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();

          // Draw background and a frame
          g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,total_w,  total_h);
          g2d.setColor(RTColorManager.getColor("axis",       "minor"));   g2d.drawRect(0,0,total_w-1,total_h-1);

          // Dimensions
          int graph_w = total_w - x_ins*2,
              graph_h = total_h - y_ins*2;

          // Determine which tablets can count
          Set<String> tablet_counts = new HashSet<String>();
          Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
            Tablet tablet = it_tab.next();
            if (KeyMaker.tabletCompletesBlank(tablet, count_by)) tablet_counts.add(tablet.fileHeader());
          }

          Map<String,Integer> x_map = new HashMap<String,Integer>();

          // Run through the records and place them on the x axis
          BundlesCounterContext bcc_lite = new BundlesCounterContext(bs, count_by, color_by);
          Iterator<Bundle> it = set.iterator(); while (it.hasNext()) {
            Bundle bundle = it.next(); if (bundle.hasTime() && tablet_counts.contains(bundle.getTablet().fileHeader())) {
              int    x     = (int) (((graph_w) * (bundle.ts0() - bs.ts0())) / (bs.ts1() - bs.ts0())) + x_ins;
              String x_str = "" + x; x_map.put(x_str, x);
              bcc_lite.count(bundle, x_str);
            }
          }
          
          // Color bin ordering
          List<String> cbins = null;
          if (color_by != null) cbins = color_bcc.getColorBinsSortedByCount();

          // Render
          Iterator<String> it_bin = bcc_lite.binIterator(); while (it_bin.hasNext()) {
            String bin = it_bin.next(); int h = (int) (bcc_lite.totalNormalized(bin) * graph_h); if (h == 0) h = 1;

            // No Color Version
            if (color_by == null) {
              g2d.setColor(RTColorManager.getColor("data", "default"));
              // g2d.fillRect(x_map.get(bin), graph_h - y_ins - h, 1, h); // no space at top?
              g2d.fillRect(x_map.get(bin), graph_h - h + y_ins, 1, h);
            } else {
              int y_inc = graph_h - y_ins - h; // seems to leave no space at the top

              for (int i=0;i<cbins.size();i++) {
                String cbin   = cbins.get(i);
                double ctotal = bcc_lite.total(bin,cbin);
                if (ctotal > 0.0) {
                  int sub_h = (int) ((ctotal * h)/bcc_lite.binColorTotal(bin));
                  if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x_map.get(bin), y_inc + 2*y_ins, 1, sub_h);
                    y_inc += sub_h;
                  }
                }
              }

              // Draw the left overs
              if (y_inc != graph_h - y_ins) {
                g2d.setColor(RTColorManager.getColor("set", "multi"));
                g2d.fillRect(x_map.get(bin), y_inc + 2*y_ins, 1, graph_h - y_ins - y_inc);
              }
            }
          }

          } finally { if (g2d != null) g2d.dispose(); }

          return bi;
        }

        /**
         * For discrete versions of the keymaker
         */
        public BufferedImage renderDiscrete(Set<Bundle> set) {
          BufferedImage bi  = new BufferedImage(total_w, total_h, BufferedImage.TYPE_INT_RGB);
          Graphics2D    g2d = null;

          try {

          g2d = (Graphics2D) bi.getGraphics();

          // Draw background and a frame
          g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fillRect(0,0,total_w,  total_h);
          g2d.setColor(RTColorManager.getColor("axis",       "minor"));   g2d.drawRect(0,0,total_w-1,total_h-1);


          // Map records into the bins for the small multiples
          BundlesCounterContext bcc_lite = new BundlesCounterContext(bs, count_by, color_by, set, km_str);

          // Calculate x offsets for the bins
          Map<String,Integer> x_map = new HashMap<String,Integer>();
          int x = x_ins; for (int i=0;i<order.length;i++) { x_map.put(order[i], x); x += bar_w + bar_gap; }

          // Color bin ordering
          List<String> cbins = null;
          if (color_by != null) cbins = color_bcc.getColorBinsSortedByCount();

          // Go through bins and render
          Iterator<String> it = bcc_lite.binIterator(); while (it.hasNext()) {
            // Determine the total height
            String bin = it.next(); int h   = (int) (bcc_lite.totalNormalized(bin) * max_bar_h); if (h == 0) h = 1;

            // Determine coloring options
            // -- No Color
            if (color_by == null) {
              g2d.setColor(RTColorManager.getColor("data", "default"));
              g2d.fillRect(x_map.get(bin), total_h - y_ins - h, bar_w, h);

            // - Color
            } else {
              int y_inc = total_h - y_ins - h;
              for (int i=0;i<cbins.size();i++) {
                String cbin   = cbins.get(i);
                double ctotal = bcc_lite.total(bin,cbin);
                if (ctotal > 0.0) {
                  int sub_h = (int) ((ctotal * h)/bcc_lite.binColorTotal(bin));
                  if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x_map.get(bin), y_inc, bar_w, sub_h);
                    y_inc += sub_h;
                  }
                }
              }

              // Draw the left overs
              if (y_inc != total_h - y_ins) {
                g2d.setColor(RTColorManager.getColor("set", "multi"));
                g2d.fillRect(x_map.get(bin), y_inc, bar_w, total_h - y_ins - y_inc);
              }
            }
          }

        } finally { if (g2d != null) g2d.dispose(); }

        return bi;
      } 

      }

      /**
       * Special shaper for when the label becomes the node.
       */
      class LabelNodeShaper extends NodeShaper {
        LabelMaker node_lm; int txt_h = -1; FixedNodeShaper fixed = new FixedNodeShaper(10.0f);
        public LabelNodeShaper(LabelMaker node_lm) { this.node_lm = node_lm; }
        public Shape nodeShape(String node, Graphics2D g2d) {
          // Get the coordinate
          Point2D point = nodeToPoint(node);
          int x = (int) point.getX(), y = (int) point.getY(); 

          // Get the string
          String strs[] = node_lm.toStrings(node); String str = "Not Set"; if (strs.length > 0) str = strs[0];

          // Calculate the text dimensions
          if (txt_h == -1) txt_h = Utils.txtH(g2d, "0"); 
          int txt_w = Utils.txtW(g2d, str);

          // Make the area -- a rectangle with rounded begins and ends
          Area area = new Area(); 
          area.add(new Area(new Rectangle2D.Float(x-txt_w/2,y-txt_h,txt_w,txt_h)));
          area.add(new Area(new Ellipse2D.Float(x-txt_w/2 - txt_h/2,y-txt_h,txt_h,txt_h)));
          area.add(new Area(new Ellipse2D.Float(x+txt_w/2-txt_h/2,y-txt_h,txt_h,txt_h)));

          return area;
        }
      }

      /**
       * Create a shape basd on the counter context for the node.
       */
      class VaryNodeShaper extends NodeShaper {
        public Shape nodeShape(String node, Graphics2D g2d) {
          float size  = (float) (1.0 + node_counter_context.totalNormalized(node) * 15.0);
          if (node_coord_set.get(node).size() > 1) return cloverShape(node, size);
          else {
            Point2D point = nodeToPoint(node);
            Utils.Symbol symbol = entity_to_shape.get(node_coord_set.get(node).iterator().next());
            float x0 = (float) (point.getX() - size/2),
                  y0 = (float) (point.getY() - size/2);
            return Utils.shape(symbol,x0,y0,size); } } } 

      /**
       * Create a shape based on the log of the counter context for the node
       */
      class VaryLogNodeShaper extends NodeShaper {
        public Shape nodeShape(String node, Graphics2D g2d) {
          double  total   = node_counter_context.total(node), 
                  maximum = node_counter_context.totalMaximum();
          float   size = 1.0f; if (total > 0.0) size = (float) (1.0 + (Math.log(total) / Math.log(maximum)) * 15.0);
          if (node_coord_set.get(node).size() > 1) return cloverShape(node, size);
          else {
            Point2D point   = nodeToPoint(node);
            Utils.Symbol symbol = entity_to_shape.get(node_coord_set.get(node).iterator().next());
            float x0 = (float) (point.getX() - size/2),
                  y0 = (float) (point.getY() - size/2);
            return Utils.shape(symbol,x0,y0,size); } } } 

      /**
       * Shaper that adjusts to the cluster coefficient for a node.
       */
      class ClusterCoefficientShaper extends NodeShaper {
        public ClusterCoefficientShaper()   { if (cluster_cos == null) cluster_cos = GraphUtils.clusterCoefficients(graph); }
        public Shape nodeShape(String node, Graphics2D g2d) {
          if (node_coord_set.get(node).size() > 1) return cloverShape(node, 5.0f);
          else {
            String  n0  = node_coord_set.get(node).iterator().next();
            Point2D point = nodeToPoint(node);
            Utils.Symbol symbol = entity_to_shape.get(node_coord_set.get(node).iterator().next());
            double coefficient = cluster_cos.get(n0); float size = (float) (1.0 + coefficient * 10.0);
            float x0 = (float) (point.getX() - size/2),
                  y0 = (float) (point.getY() - size/2);
            return Utils.shape(symbol,x0,y0,size); 
          }
        }
      }

      /**
       * Create a multiple node shape of the appropriate size.  These shapes are used to 
       * represent multiple nodes within the same pixel.
       *
       *@param node_coord string version of node coordinate
       *@param size       desired size of clover
       *
       *@return appropriate shape
       */
      public Shape cloverShape(String node_coord, float size) {
        Point2D point = nodeToPoint(node_coord);
        return Utils.createClover((float) point.getX(), (float) point.getY(), size, size);
      }

      /**
       * Render the graph information about the node.  Varies with other rendering
       * method since the shape is more complex and may need multiple primitive operations.
       *
       *@param g2d        graphics primitive
       *@param node_coord node to render
       *@param shape      recommended shape
       *
       *@return actual shape of rendered node
       */
      private Shape drawNodeGraphInfo(Graphics2D g2d, String node_coord, Shape shape) {
        if (graph_bcc == null) {
          // Create graph parametrics (Only add linear time algorithms here...)
          graph_bcc     = new BiConnectedComponents(graph);
          graph2p_bcc   = new BiConnectedComponents(new UniTwoPlusDegreeGraph(graph));
        }
        BiConnectedComponents bcc = graph_bcc, bcc_2p = graph2p_bcc; if (bcc != null && bcc_2p != null) {
          // Get the graph info sources
          Set<String>              cuts    = bcc.getCutVertices(),
                                   cuts_2p = bcc_2p.getCutVertices();
          Map<String,Set<MyGraph>> v_to_b  = bcc.getVertexToBlockMap();

          // Determine if we have a set of nodes or a single node
          Set<String> set = node_coord_set.get(node_coord);
          if (set.size() == 1) {
            String node = set.iterator().next();
            if (cuts.contains(node)) {
              g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fill(shape); g2d.setColor(RTColorManager.getColor("background", "reverse")); g2d.draw(shape);
              if (cuts_2p.contains(node)) {
                g2d.setColor(RTColorManager.getColor("annotate", "cursor"));
                double cx = shape.getBounds().getCenterX(),
                       cy = shape.getBounds().getCenterY(),
                       dx = shape.getBounds().getMaxX() - cx;
                g2d.draw(new Ellipse2D.Double(cx-1.8*dx,cy-1.8*dx,3.6*dx,3.6*dx));
              }
            } else {
              MyGraph mg = v_to_b.get(node).iterator().next();
              if (mg.getNumberOfEntities() <= 2) g2d.setColor(RTColorManager.getColor("background", "nearbg"));
              else                               g2d.setColor(RTColorManager.getColor(mg.toString())); 
              g2d.fill(shape);
            }
          } else {
            boolean lu_miss = false;
            Set<MyGraph> graphs = new HashSet<MyGraph>();
            Iterator<String> it = set.iterator();
            while (it.hasNext()) {
              String node = it.next();
              if (v_to_b.containsKey(node)) graphs.addAll(v_to_b.get(node));
              else { System.err.println("No V-to-B Lookup For Node \"" + node + "\""); lu_miss = true; }
            }
            if (graphs.size() == 1) {
              MyGraph mg = graphs.iterator().next();
              g2d.setColor(RTColorManager.getColor(mg.toString()));
            } else {
              g2d.setColor(RTColorManager.getColor("set", "multi"));
            }
            g2d.fill(shape);
            if (lu_miss) {
              g2d.setColor(RTColorManager.getColor("label", "errorfg"));
              Rectangle2D rect = shape.getBounds();
              g2d.drawLine((int) rect.getMinX() - 5, (int) rect.getMinY() - 5, 
                           (int) rect.getMaxX() + 5, (int) rect.getMaxY() + 5);
              g2d.drawLine((int) rect.getMaxX() + 5, (int) rect.getMinY() - 5, 
                           (int) rect.getMinX() - 5, (int) rect.getMaxY() + 5);
            }
          }
        }
        return shape;
      }

      /**
       * Make the node shape and color depict information about the entity.  Typically
       * this means drawing a specific color/glyph based on the entity type
       *
       *@param g2d         graphics primitive
       *@param node_coord  coordinate of node to render
       *
       *@return shape of rendered node
       */
      private Shape drawNodeType(Graphics2D g2d, String node_coord) {
        Point2D         point = nodeToPoint(node_coord); int x = (int) point.getX(), y = (int) point.getY();
        Set<String> set   = node_coord_set.get(node_coord);
        Shape           shape = null;
        if (set.size() == 1) {
          String       str      = set.iterator().next();
          BundlesDT.DT datatype = BundlesDT.getEntityDataType(str);
          if (datatype != null) { // If the data type is valid, draw the data type specific shape/color
            switch (datatype) {
              case IPv4:     shape = drawNodeTypeIPv4(g2d,point,str);   break;
              case DOMAIN:   shape = drawNodeTypeDomain(g2d,point,str); break;
              case EMAIL:    shape = drawNodeTypeEmail(g2d,point,str);  break;
              case MD5:      shape = drawNodeTypeMD5(g2d,point,str);    break;
              default:       shape = new Rectangle2D.Double(x - 5, y - 5, 10, 10); 
                             g2d.setColor(RTColorManager.getColor("background", "reverse"));
                             g2d.fill(shape);
                             break;
            }
          } else { // Draw the string in red/type color 
            if (str.indexOf(BundlesDT.DELIM) > 0) g2d.setColor(RTColorManager.getColor(str.substring(0,str.indexOf(BundlesDT.DELIM)))); else g2d.setColor(RTColorManager.getColor("default", "major"));
            g2d.drawString(str, (int) (x - Utils.txtW(g2d,str)/2), y);
            shape = new Rectangle2D.Double(x - Utils.txtW(g2d,str)/2, y - Utils.txtH(g2d,str), Utils.txtW(g2d,str), Utils.txtH(g2d,str));
          }
        } else               { // Draw the default clover shape
          g2d.setColor(RTColorManager.getColor("background", "reverse")); 
          Iterator<String> it = set.iterator(); boolean first_ip = true; int min_ip = 0, max_ip = 0;
          while (it.hasNext()) {
            String       entity   = it.next();
            BundlesDT.DT datatype = BundlesDT.getEntityDataType(entity);
            if (datatype != null) {
              switch (datatype) {
                case IPv4:  int ip = Utils.ipAddrToInt(entity); 
                            if (first_ip) { min_ip = max_ip = ip; first_ip = false; } else {
                              if (min_ip > ip) min_ip = ip; 
                              if (max_ip < ip) max_ip = ip;
                            }
                            break;
                default: break;
              }
            }
          }
          g2d.draw(shape = cloverShape(node_coord, 10.0f));
          if (first_ip == false) {
            drawNodeTypeIPv4(g2d, new Point2D.Double(point.getX() - 4, point.getY() - 4), min_ip, max_ip);
          }
        }
        return shape;
      }
      
      /**
       * Draw the glyph for a domain name and return the shape.
       *
       *@param g2d    graphics primitive
       *@param point  location for node
       *@param domain entity string
       *
       *@return shape of rendered domain
       */
      private Shape drawNodeTypeDomain(Graphics2D g2d, Point2D point, String domain) {
        int x = (int) point.getX(), y = (int) point.getY(), sw = 8, h2 = 8; boolean four_plus = false;
        StringTokenizer st = new StringTokenizer(domain,".");
        while (st.countTokens() > 3) { st.nextToken(); four_plus = true; }
        if (st.countTokens() == 3) { g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillRect(x-sw,y-h2,sw,  h2); }
        if (st.countTokens() == 2) { g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillRect(x,   y-h2,sw,  h2); }
        if (st.countTokens() == 1) { g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillRect(x-sw,y   ,2*sw,h2); }
        if (four_plus) { g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawLine(x-sw,y-h2,x-sw-4,y-h2-4); g2d.drawLine(x-sw,y-h2-4,x-sw-4,y-h2); }
        return new Rectangle2D.Double(x-sw,y-h2,2*sw,2*h2);
      }

      /**
       * Draw the glyph for an IPv4 address and return the shape.
       *
       *@param g2d    graphics primitive
       *@param point  location for node
       *@param ipv4   IP address
       *
       *@return shape of rendered address
       */
      private Shape drawNodeTypeIPv4(Graphics2D g2d, Point2D point, String ipv4) {
        StringTokenizer st = new StringTokenizer(ipv4,".");
        int x = (int) point.getX(), y = (int) point.getY(), w = 8, h = 8;
        g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillOval(x-w,   y-h,   2*w,   2*h);
        g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillOval(x-w+3, y-h+3, 2*w-6, 2*h-6);
        g2d.setColor(RTColorManager.getColor(st.nextToken())); g2d.fillOval(x-w+6, y-h+6, 2*w-12,2*h-12);
        return new Ellipse2D.Double(x - 8, y - 8, 2*w, 2*h);
      }

      /**
       * Draw the glyph for an email address and return the shape.
       *
       *@param g2d    graphics primitive
       *@param point  location for node
       *@param ipv4   IP address
       *
       *@return shape of rendered address
       */
      private Shape drawNodeTypeEmail(Graphics2D g2d, Point2D point, String email) {
        Shape shape = Utils.createEnvelope((float) point.getX() - 4, (float) point.getY() - 4, 8, 8);
        g2d.setColor(RTColorManager.getColor(Utils.emailDomain(email)));
        g2d.fill(shape);
        return shape;
      }

      /**
       * Draw the glyph for an md5 and return the shape.
       *
       *@param g2d    graphics primitive
       *@param point  location for node
       *@param ipv4   IP address
       *
       *@return shape of rendered address
       */
      private Shape drawNodeTypeMD5(Graphics2D g2d, Point2D point, String email) {
        Shape shape = Utils.createDocumentShape((float) point.getX() - 4, (float) point.getY() - 4, 12, 12);
        g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fill(shape);
        g2d.setColor(RTColorManager.getColor("data",       "default")); g2d.draw(shape);
        return shape;
      }

      /**
       * Draw the glyph for a range of IPv4 addresses.  Those that share octets
       * will have a uniform color.  If the octet varies, the glyph portion will be black.
       *
       *@param g2d    graphics primitive
       *@param point  location for node
       *@param min_ip lowest IP address
       *@param max_ip greatest IP address
       *
       *@return shape of rendered addresses
       */
      private Shape drawNodeTypeIPv4(Graphics2D g2d, Point2D point, int min_ip, int max_ip) {
        int x = (int) point.getX(), y = (int) point.getY(), w = 8, h = 8;
        if ((min_ip & 0xff000000) == (max_ip & 0xff000000)) g2d.setColor(RTColorManager.getColor("" + ((min_ip >> 24)&0x00ff))); else g2d.setColor(RTColorManager.getColor("background", "default"));
        g2d.fillOval(x-w,   y-h,   2*w,   2*h);
        if ((min_ip & 0x00ff0000) == (max_ip & 0x00ff0000)) g2d.setColor(RTColorManager.getColor("" + ((min_ip >> 16)&0x00ff))); else g2d.setColor(RTColorManager.getColor("background", "default"));
        g2d.fillOval(x-w+3, y-h+3, 2*w-6, 2*h-6);
        if ((min_ip & 0x0000ff00) == (max_ip & 0x0000ff00)) g2d.setColor(RTColorManager.getColor("" + ((min_ip >>  8)&0x00ff))); else g2d.setColor(RTColorManager.getColor("background", "default"));
        g2d.fillOval(x-w+6, y-h+6, 2*w-12,2*h-12);
        return new Ellipse2D.Double(x - 8, y - 8, 2*w, 2*h);
      }

      /**
       * Select all entities that are the same color as the one(s) under the mouse.
       *
       *@param node_color node color setting
       *@param m_x        mouse x coordinate
       *@param m_y        mouse y coordinate
       */
      public void selectColorUnderMouse(NodeColor node_color, int m_x, int m_y) {
        Set<String> under_mouse = underMouse(m_x, m_y); if (under_mouse == null || under_mouse.size() == 0) return;

        // Determine the correct colorer to use
        NodeColorer colorer = null;
        switch (node_color) {
          case VARY:      colorer = new VaryNodeColorer();     break;
          case LABEL:     colorer = new LabelNodeColorer();    break;
          case DATATYPE:  colorer = new DataTypeNodeColorer(); break;
          default:        System.err.println("selectColorUnderMouse() - Only works for DATATYPE, LABEL, or VARY");
        }

        // If the colorer isn't null, continue
        if (colorer != null) {
          Set<Color> colors = new HashSet<Color>();

          // Find the colors for the entities under the mouse
          Iterator<String> it = under_mouse.iterator(); while (it.hasNext()) {
            String entity         = it.next();
            String node_coord_str = entity_to_sx.get(entity) + "|" + entity_to_sy.get(entity);
            Color color = colorer.nodeColor(node_coord_str);
            if (color != null) colors.add(color);
          }

          // Find all other entities with the same color
          Set<String> new_sel = new HashSet<String>(); it = node_counter_context.binIterator();
          while (it.hasNext()) {
            String node_coord_str = it.next();
            Color  color          = colorer.nodeColor(node_coord_str);
            if (colors.contains(color)) { new_sel.addAll(node_coord_set.get(node_coord_str)); }
          }

          // Only allow visible
          new_sel = filterEntities(new_sel);

          // Execute the set operation
          setOperation(new_sel);
        }
      }

      /**
       * Fill the mappings for color to nodes and vice versa.  If a node doesn't have a color, leave it blank in the maps.
       * 
       *@param node_color_for_layout  node color to use -- needs to be label
       *@param color_to_nodes         map to go from a color to the set of nodes that match - modified by this method
       *@param node_to_color          map to go from the node string to the assigned color - modified by this method
       *@param keep_no_colors         keep nodes that have the no_label_color setting
       */
      public void createNodeColorMaps(NodeColor node_color_for_layout, Map<Color,Set<String>> color_to_nodes, Map<String,Color> node_to_color, boolean keep_no_colors) {
        // Clear out the destination
        color_to_nodes.clear(); node_to_color.clear();

        // Create the colorer
        NodeColorer colorer = null;
        switch (node_color_for_layout) {
          case LABEL:    colorer = new LabelNodeColorer();    break;
          case VARY:     colorer = new VaryNodeColorer();     break;
          case DATATYPE: colorer = new DataTypeNodeColorer(); break;
          default:       System.err.println("createNodeColorMaps() - Only works for DATATYPE, LABEL, or VARY");
        }

        // Assuming we have a colorer, proceed with the method
        if (colorer != null) {
          // Iterator over the coordinate mapping
          Iterator<String> it_coords = node_counter_context.binIterator(); Color no_label_color = RTColorManager.getColor("label", "minor");

          // While more nodes (or rather, node coordinates)
          while (it_coords.hasNext()) {

            // Get the node coordinate string and the associated color ... bail out if no color net
            String node_coord_str = it_coords.next(); Color color = colorer.nodeColor(node_coord_str);
            if (keep_no_colors == false && color.equals(no_label_color)) continue;

            // Break out the nodes at that coordinate
            Iterator<String> it_nodes = node_coord_set.get(node_coord_str).iterator(); while (it_nodes.hasNext()) {
              String node = it_nodes.next();

              // Update the data structures
              if (color_to_nodes.containsKey(color) == false) color_to_nodes.put(color, new HashSet<String>());
              color_to_nodes.get(color).add(node);
              node_to_color.put(node,color);
            }
          }
        }
      }

      /**
       * Use the node colors to form composite that are then run through a force directed (FD) layout.
       * Nodes that don't have a color (or are multi-colored) will be kept as their original nodes.  One
       * limitation of this implementation is that the nodes need to be visually separated (i.e., not composite)
       * in the view... they may also need to be on the screen... unsure.
       *
       *@param node_color_for_layout node color for layout
       */
      public Runnable nodeColorCompositeFDLayoutRunnable(NodeColor node_color_for_layout) {
        return new NodeColorCompositeFDLayoutRunnable(node_color_for_layout);
      }

      /**
       * Runnable implementing the node color composite force directed layout.  Requries a thread so that the
       * GUI can continue to update / show progress.
       */
      class NodeColorCompositeFDLayoutRunnable implements Runnable {
       NodeColor node_color_for_layout;
       public NodeColorCompositeFDLayoutRunnable(NodeColor node_color_for_layout0) {
         this.node_color_for_layout = node_color_for_layout0;
       }
       public void run() {
        // Determine the correct colorer to use
        NodeColorer colorer = null;
        switch (node_color_for_layout) {
          case LABEL:     colorer = new LabelNodeColorer();    break;
          case VARY:      colorer = new VaryNodeColorer();     break;
          case DATATYPE:  colorer = new DataTypeNodeColorer(); break;
          default:        System.err.println("nodeColorCompositeFDLayout() - Only works for DATATYPE, LABEL, or VARY");
        }

        if (colorer != null) {
          // Map for color to nodes sets
          Map<Color,Set<String>> color_to_nodes = new HashMap<Color,Set<String>>();
          Map<String,Color>      node_to_color  = new HashMap<String,Color>();
          Map<String,Color>      res_map        = new HashMap<String,Color>();
          Map<Color,String>      res_rmap       = new HashMap<Color,String>();

          // Iterate over the nodes to put them into the correct color bins
          Iterator<String> it_coords = node_counter_context.binIterator(); Color no_label_color = RTColorManager.getColor("label", "minor");
          while (it_coords.hasNext()) {
            String node_coord_str = it_coords.next();
            Color  color          = colorer.nodeColor(node_coord_str);

            if (color.equals(no_label_color) || color.equals(RTColorManager.getColor("set","multi"))) continue; // No label for this node... keep the node distinct

            // Keep a conversion from color to string and vice versa
            String color_str = "color : " + color.toString(); res_map.put(color_str, color); res_rmap.put(color, color_str);

            // Apply the color to the node and vice versa in the maps
            if (color_to_nodes.containsKey(color) == false) color_to_nodes.put(color, new HashSet<String>());
            Iterator<String> it_nodes = node_coord_set.get(node_coord_str).iterator(); while (it_nodes.hasNext()) {
              String node = it_nodes.next();
              color_to_nodes.get(color).add(node);
              node_to_color.put(node, color);
            }
          }

          // Take the original graph and form the composite graph based on color aggregation
          UniGraph composite_g = new UniGraph(); 

          for (int node_i=0;node_i<graph.getNumberOfEntities();node_i++) {
            String node     = graph.getEntityDescription(node_i);
            String node_res = node;

            if (node_to_color.containsKey(node)) node_res = res_rmap.get(node_to_color.get(node));

            for (int i=0;i<graph.getNumberOfNeighbors(node_i);i++) {
              int    nbor_i   = graph.getNeighbor(node_i, i);
              String nbor     = graph.getEntityDescription(nbor_i);
              String nbor_res = nbor;

              if (node_to_color.containsKey(nbor)) nbor_res = res_rmap.get(node_to_color.get(nbor));

              composite_g.addNeighbor(node_res, nbor_res);
            }
          }

          // Create a wxy shell class
          Map<String,Point2D> composite_wxy = new HashMap<String,Point2D>();
          for (int node_i=0;node_i<composite_g.getNumberOfEntities();node_i++) {
            String node = composite_g.getEntityDescription(node_i);
            composite_wxy.put(node, new Point2D.Double(Math.random(), Math.random()));
          }

          // Run the force directed layout
          LayoutProgressDialog layout_progress_dialog = new LayoutProgressDialog();
          try { (new GraphLayouts()).executeLayoutAlgorithm(GraphLayouts.MDS_ITERATIVE_PROP_STR, composite_g, null, composite_wxy, layout_progress_dialog, null, null); } finally {
            if (layout_progress_dialog != null) {
              layout_progress_dialog.setVisible(false);
              layout_progress_dialog.dispose();
            }
          }

          // Save the layout for undo
          saveLayoutForUndo(entity_to_wxy, null);

          // Apply the layout via the color aggregation
          for (int node_i=0;node_i<graph.getNumberOfEntities();node_i++) {
            String node = graph.getEntityDescription(node_i);
            if (node_to_color.containsKey(node)) entity_to_wxy.put(node, composite_wxy.get(res_rmap.get(node_to_color.get(node))));
            else                                 entity_to_wxy.put(node, composite_wxy.get(node));
          }

          // Request a re-render
          transform(); zoomToFit(); getRTComponent().render();
        }
       }
      }
      
      /**
       * Layout the nodes based on their colors using a treemap.
       *
       *@param node_color_for_layout node color for layout
       */
      public void nodeColorTreeMapLayout(NodeColor node_color_for_layout, Set<String> selected) {
        // Determine the correct colorer to use
        NodeColorer colorer = null;
        switch (node_color_for_layout) {
          case VARY:      colorer = new VaryNodeColorer();     break;
          case LABEL:     colorer = new LabelNodeColorer();    break;
          case DATATYPE:  colorer = new DataTypeNodeColorer(); break;
          default:        System.err.println("nodeColorTreeMapLayout() - Only works for DATATYPE, LABEL, or VARY");
        }

        // If we have a valid colorer, collate nodes by the color into a map
        if (colorer != null) {
          saveLayoutForUndo(entity_to_wxy, null);

          // Map for color to nodes sets
          Map<Color,Set<String>> map = new HashMap<Color,Set<String>>();

          // Iterate over the nodes to put them into the correct color bins
          //
          // Apply to the selected entities
          //
          if (selected != null && selected.size() > 0) {
            double min_x = Double.POSITIVE_INFINITY, min_y = Double.POSITIVE_INFINITY,
                   max_x = Double.NEGATIVE_INFINITY, max_y = Double.NEGATIVE_INFINITY;

            // Prune the selected to the entities in the graph... map them into color
            Iterator<String> it = selected.iterator(); while (it.hasNext()) {
              String entity = it.next(); if (entity_to_wxy.containsKey(entity) && entity_counter_context.hasBin(entity)) {
                // Determine extents of selected -- it has to fit back into this
                Point2D pt = entity_to_wxy.get(entity);
                if (pt.getX() < min_x) min_x = pt.getX();
                if (pt.getY() < min_y) min_y = pt.getY();
                if (pt.getX() > max_x) max_x = pt.getX();
                if (pt.getY() > max_y) max_y = pt.getY();

                Color color = colorer.entityColor(entity);
                if (map.containsKey(color) == false) map.put(color, new HashSet<String>());
                map.get(color).add(entity);
              }
            }

            // Make sure the extents make sense
            if (min_x == max_x) { min_x -= 0.5; max_x += 0.5; }
            if (min_y == max_y) { min_y -= 0.5; max_y += 0.5; }

            // Run the treemap algorithm
            TreeMap<Color,String> treemap = new TreeMap<Color,String>(map);
            Map<Color,Rectangle2D> layout  = treemap.squarifiedTileMapping(); 
            treemap.transformWorldPoints(layout, entity_to_wxy, min_x, min_y, max_x, max_y);

            // Transform and re-render
            transform(); getRTComponent().render();

          //
          // Apply to all the nodes
          //
          } else {
            // Create the node colors lookup
            Map<Color,Set<String>> color_to_nodes = new HashMap<Color,Set<String>>();
            Map<String,Color>      node_to_color  = new HashMap<String,Color>();
            createNodeColorMaps(node_color, color_to_nodes, node_to_color, true);

            // Standard treemap code ... assuming that the previous method worked (i.e., nodes had colors)
            if (color_to_nodes.keySet().size() > 0) {
              TreeMap<Color,String> treemap = new TreeMap<Color,String>(color_to_nodes);
              Map<Color,Rectangle2D> layout  = treemap.squarifiedTileMapping();
              treemap.transformWorldPoints(layout,entity_to_wxy);

              transform(); zoomToFit(); getRTComponent().render();
            }
          }
        }
      }

      /**
       * Label maker for nodes
       */
      LabelMaker node_lm = null, special_lm =  null;

      /**
       * Take the precalculated information and render it on the screen in the right colors and shapes.
       *
       *@param g2d graphics primitive
       */
      protected void drawNodes(Graphics2D g2d) {
        Rectangle2D screen = new Rectangle2D.Float(0,0,w,h);
        NodeShaper  shaper = null; NodeColorer colorer = null;
        NodeShaper  bkup_shaper = null; // Backup shaper for the label shaper if sticky labels are enabled

        // Prepare the label maker (if node size equates to node label... prepare that as well)
        if (node_size == NodeSize.LABEL) {
          List<String> list = new ArrayList<String>();
          if (entity_labels.size() > 0) { String top = entity_labels.get(0); entity_labels.remove(0); list.add(top); } else list.add(ENTITY_LM);
          special_lm = new LabelMaker(list, node_counter_context);
        }
        node_lm = new LabelMaker(entity_labels, node_counter_context);

        // Allocate the shaper and colorer
        switch (node_size) {
          case TYPE:
          case GRAPHINFO:
          case LARGE:     shaper      = new FixedNodeShaper(10.0f);         break;
          case LABEL:     shaper      = new LabelNodeShaper(special_lm);   
                          bkup_shaper = new FixedNodeShaper(10.0f);         break;
          case SMALL:     shaper      = new FixedNodeShaper(4.0f);          break;
          case VARY:      shaper      = new VaryNodeShaper();               break;
          case VARY_LOG:  shaper      = new VaryLogNodeShaper();            break;
          case CLUSTERCO: shaper      = new ClusterCoefficientShaper();     break;

          case SM_XY_VISIBLE:
          case SM_XY_VISIBLE_EQUAL:
          case SM_XY_LOCAL:
          case SM_XY_LOCAL_EQUAL:

          case SM_PIE_SMALL:
          case SM_PIE_LARGE:

          case SM_MONTH:  
          case SM_DOW:
          case SM_HOUR:   

          case SM_STRAIGHT_SMALL:
          case SM_STRAIGHT_LARGE:  shaper      = new SmallMultNodeShaper(node_size); 
                                   bkup_shaper = new FixedNodeShaper(10.0f);  break;
          case INVISIBLE:
          default:        return;
        }
        switch (node_color) {
          case WHITE:     colorer = new FixedNodeColorer(RTColorManager.getColor("background", "reverse")); break;
          case VARY:      colorer = new VaryNodeColorer();             break;
          case LABEL:     colorer = new LabelNodeColorer();            break;
          case CLUSTERCO: colorer = new ClusterCoefficientColorer();   break;
          case DATATYPE:  colorer = new DataTypeNodeColorer();         break;
        }

        // Figure out the bounding box for clipping
        Rectangle2D screenplus = new Rectangle2D.Double(screen.getX()-10,screen.getY()-10,screen.getWidth()+20,screen.getHeight()+20);

        Iterator<String> it = node_counter_context.binIterator();
        while (it.hasNext()) {
          String node  = it.next();
          Shape  shape = shaper.nodeShape(node, g2d);
          if (screenplus.intersects(shape.getBounds())) {
            Shape tmp_shape = null;
            switch (node_size) {
              case TYPE:      tmp_shape = drawNodeType(g2d, node);             if (tmp_shape != null) shape = tmp_shape; break;
              case GRAPHINFO: tmp_shape = drawNodeGraphInfo(g2d, node, shape); if (tmp_shape != null) shape = tmp_shape; break;
              case LABEL:     boolean single = (node_coord_set.get(node).size() == 1);
                              String  str    = ""; if (single && sticky_labels.size() > 0) str = node_coord_set.get(node).iterator().next();
                              if (sticky_labels.size() == 0 || sticky_labels.contains(str)) {
                                g2d.setColor(RTColorManager.getColor("background", "default")); g2d.fill(shape);
                                g2d.setColor(RTColorManager.getColor("background", "reverse")); g2d.draw(shape);
                                special_lm.draw(g2d, node, (int) shape.getBounds().getCenterX(), (int) shape.getBounds().getMinY()-3, true);
                              } else {
                                shape = bkup_shaper.nodeShape(node, g2d);
                                g2d.setColor(colorer.nodeColor(node)); g2d.fill(shape);
                              }
                              break;
              case SM_XY_VISIBLE:
              case SM_XY_VISIBLE_EQUAL:
              case SM_XY_LOCAL:
              case SM_XY_LOCAL_EQUAL:
              case SM_PIE_SMALL:
              case SM_PIE_LARGE:
              case SM_STRAIGHT_SMALL:
              case SM_STRAIGHT_LARGE:
              case SM_MONTH:
              case SM_DOW:
              case SM_HOUR:   BufferedImage sm_bi = ((SmallMultNodeShaper) shaper).render(node_counter_context.getBundles(node));
                              if (sm_bi == null) {
                                shape = bkup_shaper.nodeShape(node, g2d);
                                g2d.setColor(colorer.nodeColor(node)); g2d.fill(shape);
                              } else g2d.drawImage(sm_bi, (int) shape.getBounds().getX(), (int) shape.getBounds().getY(), null);
                              break;

              default:        g2d.setColor(colorer.nodeColor(node)); g2d.fill(shape); break;
            }
            if (node_lm != null && draw_node_labels) node_lm.draw(g2d, node, (int) shape.getBounds().getCenterX(), 
                                                                             (int) shape.getBounds().getMaxY(), true);

            // Accounting for interactivity
            all_shapes.add(shape);
            geom_to_bundles.put(shape, node_counter_context.getBundles(node));
            node_to_geom.put(node, shape);
            Iterator<Bundle> itb = node_counter_context.getBundles(node).iterator();
            while (itb.hasNext()) {
              Bundle bundle = itb.next();
              if (bundle_to_shapes.containsKey(bundle) == false) bundle_to_shapes.put(bundle, new HashSet<Shape>());
              bundle_to_shapes.get(bundle).add(shape);
            }
          }
        }
      }

      /**
       * Class and subclasses responsible for interpreting the label strings and defining the resulting label.
       */
      class LabelMaker {
        /**
         * List of labels to show
         */
        protected java.util.List<String> labels;

        /**
         * Return the list of labels to make as a string array.
         *
         *@param string array representing labels to make
         */
        public String[] labelsArray() {
          String strs[] = new String[labels.size()];
          for (int i=0;i<strs.length;i++) strs[i] = labels.get(i);
          return strs;
        }

        /**
         * Actual label maker subclasses
         */
        protected LM                     makers[];

        /**
         * Counter context for accumulating information for labeling
         */
        protected BundlesCounterContext  cc;

        /**
         * Construct a new label maker with the list of labels and the pre-calculated
         * counter context.
         *
         *@param labels labels to render
         *@param cc     counter context for nodes
         */
        public LabelMaker(java.util.List<String> labels, BundlesCounterContext cc) {
          this.labels = labels; this.cc = cc; makers = new LM[labels.size()];
          for (int i=0;i<labels.size();i++) makers[i] = createLM(labels.get(i), labels.size() == 1);
        }

        /**
         * Return the color of a specific bin (node coord string probably).
         *
         *@param bin node coordinate
         *
         *@return color for label (?)
         */
        public Color getColor(String bin) {
          Color color = RTColorManager.getColor("set", "multi");
          if (makers.length > 0) {
            color = makers[0].draw(null, bin, 0, 0);
            if (color == null) color = Color.darkGray;
          }
          return color;
        }

        /**
         * For a specific node coordinate (called a bin), render the set of labels from the construction stage.
         *
         *@param g2d            graphics primitive
         *@param bin            node coordinate
         *@param x              x coordinate for rendering labels
         *@param y              y coordinate for rendering labels
         *@param top_justified  determines how the vertical justification will occur
         *
         *@return the shape of the first label
         */
        public Shape draw(Graphics2D g2d, String bin, int x, int y, boolean top_justified) {
          // get txt height
          int txt_h = Utils.txtH(g2d, "0"); 
          // Figure you where to start
          if (top_justified) y += txt_h; 
          // else               y -= (int) (txt_h * makers.length/2 + (txt_h/2) * (makers.length%2));
          else {
            if      (makers.length   == 1) y += txt_h/2;
            else if (makers.length%2 == 0) y -= (makers.length/2 - 1)*txt_h;
            else {
              y += txt_h/2;
              y -= (makers.length/2)*txt_h;
            }
          }
          // draw
          Shape shape = null;
          for (int i=0;i<makers.length;i++) if (makers[i] != null) { 
            if (makers[i].draw(g2d, bin, x, y) != null) { y += txt_h * makers[i].renderedLines(); if (i == 0) shape = makers[i].getShape(); }
          }
          return shape;
        }

        /**
         * Return the strings for each label maker.
         *
         *@param bin node coordinate to use for the bin
         *
         *@return array of strings for each label maker result
         */
        public String[] toStrings(String bin) {
          String strs[] = new String[makers.length];
          for (int i=0;i<strs.length;i++) strs[i] = makers[i].toString(bin);
          return strs;
        }

        /**
         * Abstract class for making a label.
         */
        abstract class LM { 
          /**
           * Shape of resulting label
           */
          Shape shape = null;

          /**
           * Number of lines rendered... for multiline labels...
           */
          int rendered_lines = 1;

          /**
           * Return the number of lines rendered
           */
          public int renderedLines() { return rendered_lines; }

          /**
           * Render a label to the screen.
           *
           *@param g2d  graphics primitive
           *@param bin  node coordinate for label
           *@param x    x coordinate for label
           *@param y    y coordinate for label
           *
           *@return color of label (hack to get the "color-by-label" to work)
           */
          public abstract Color draw(Graphics2D g2d, String bin, int x, int y); 

          /**
           * Return the string that will be rendered for the specified bin (node coordinate).
           *
           *@param bin node coordinate for label
           *
           *@return actual string to be rendered
           */
          public abstract String toString(String bin);

          /**
           * Return the actual shape of the label.
           *
           *@return label shape
           */
          public Shape getShape() { return shape; }
        }

        /**
         * Label for showing time information
         */
        protected class TimeLM extends LM {
          static final int BOTH=0, FIRST=1, LAST=2; int type, prec; public TimeLM(int type, int prec) { this.type = type; this.prec = prec; }
          static final int EXACT=0, MONTHS=1, DAYS=2;
          public Color draw(Graphics2D g2d, String bin, int x, int y) { 
            long ts0 = Long.MAX_VALUE, 
                 ts1 = 0L;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                Bundle bundle = it.next(); if (bundle.hasTime()) {
                  if (bundle.ts0() < ts0) ts0 = bundle.ts0();
                  if (bundle.ts1() > ts1) ts1 = bundle.ts1();
                }
              }
            }
            if (ts0 == Long.MAX_VALUE) return null; // No time bundles found
            String ts0_str, ts1_str; Color ts0_color, ts1_color;

            double ts0_fraction = ((double) ts0 - bs.ts0())/(bs.ts1() - bs.ts0()), ts1_fraction = ((double) ts1 - bs.ts0())/(bs.ts1() - bs.ts0());
            if (ts0_fraction < 0.0) ts0_fraction = 0.0; if (ts0_fraction > 1.0) ts0_fraction = 1.0;
            if (ts1_fraction < 0.0) ts1_fraction = 0.0; if (ts1_fraction > 1.0) ts1_fraction = 1.0;

            switch (type) {
              case BOTH: ts0_str = precision(ts0); // Utils.humanReadableDate(ts0);
                         ts1_str = precision(ts1); // Utils.humanReadableDate(ts1); 

                           ts0_color = timing_marks_cs.at((float) ts0_fraction);
                           ts1_color = timing_marks_cs.at((float) ts1_fraction);

                         // Combine the strings together if they are equal...  for the days or months settings
                         if (ts0_str.equals(ts1_str)) {
                           if (g2d != null) {
                             x -= Utils.txtW(g2d, ts0_str)/2;
                             g2d.setColor(RTColorManager.getColor("label", "minor"));
                             g2d.drawString(ts0_str, x, y);
                           }

                         // Standard way
                         } else {
                           if (g2d != null) { x -= Utils.txtW(g2d, ts0_str + " - " + ts1_str)/2; }
                           if (g2d != null) {
                             g2d.setColor(ts0_color);   g2d.drawString(ts0_str, x, y); x += Utils.txtW(g2d, ts0_str);
                             g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y); x += Utils.txtW(g2d, " - ");
                             g2d.setColor(ts1_color);   g2d.drawString(ts1_str, x, y);
                           }
                         }
                         return ts0_color;
              case FIRST: ts0_str = precision(ts0); // Utils.humanReadableDate(ts0);
                          if (g2d != null) { x -= Utils.txtW(g2d, ts0_str + " - ")/2; }
                          ts0_color = timing_marks_cs.at((float) ts0_fraction);
                          if (g2d != null) {
                            g2d.setColor(ts0_color);   g2d.drawString(ts0_str, x, y); x += Utils.txtW(g2d, ts0_str);
                            g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y);
                          }
                          return ts0_color;
              case LAST:  ts1_str = precision(ts1); // Utils.humanReadableDate(ts1);
                          if (g2d != null) { x -= Utils.txtW(g2d, " - " + ts1_str)/2; }
                          ts1_color = timing_marks_cs.at((float) ts1_fraction);
                          if (g2d != null) {
                            g2d.setColor(RTColorManager.getColor("label", "minor")); g2d.drawString(" - ",   x, y); x += Utils.txtW(g2d, " - ");
                            g2d.setColor(ts1_color);   g2d.drawString(ts1_str, x, y);
                          }
                          return ts1_color;
            }
            return null;
          }

          /**
           * Return the timestamp formatted at the specified precision
           */
          private String precision(long ts) {
            switch (prec) {
              case DAYS:   return Utils.dayDateStr(ts);
              case MONTHS: return Utils.monthDateStr(ts);
              case EXACT: 
              default:     return Utils.humanReadableDate(ts);
            }
          }

          /**
           * Return the rendered string.
           */
          public String toString(String bin) { 
            // Find the min and max time for this vertex
            // Only works for the node version... see the render code to modify it to work for links
            long ts0 = Long.MAX_VALUE, ts1 = 0L; 
            Iterator<Bundle> it = cc.getBundles(bin).iterator();
            while (it.hasNext()) {
              Bundle bundle = it.next(); if (bundle.hasTime()) {
                if (bundle.ts0() < ts0) ts0 = bundle.ts0();
                if (bundle.ts1() > ts1) ts1 = bundle.ts1();
              }
            }

            // Choose the right format
            if (ts0 != Long.MAX_VALUE && ts1 != 0L) {
              switch (type) {
                case FIRST: return precision(ts0);
                case LAST:  return precision(ts1);
                case BOTH:
                default:    return precision(ts0) + " - " + precision(ts1);
              }
            }

            return "No Timestamps";
          }
        }

        /**
         * Label for showing node degree information
         */
        protected class DegreeLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            int deg = 0; ColStr colstr = new ColStr();
            if (node_coord_set.get(bin).size() == 1) {
              String entity = node_coord_set.get(bin).iterator().next();
              colstr.str = "" + digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity)) + "/" + graph.getNumberOfNeighbors(graph.getEntityIndex(entity)) + " Nbors";
              deg = graph.getNumberOfNeighbors(graph.getEntityIndex(entity));
            } else {
              Iterator<String> it_str = node_coord_set.get(bin).iterator();
              String  entity = it_str.next(); int min_di, max_di, min, max;
              min_di = max_di = digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity));
              min    = max    = graph.getNumberOfNeighbors(graph.getEntityIndex(entity));
              while (it_str.hasNext()) { entity = it_str.next();
                if (min_di > digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity))) min_di = digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity));
                if (max_di < digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity))) max_di = digraph.getNumberOfNeighbors(digraph.getEntityIndex(entity));
                if (min    > graph.getNumberOfNeighbors(graph.getEntityIndex(entity)))     min    = graph.getNumberOfNeighbors(graph.getEntityIndex(entity));
                if (max    < graph.getNumberOfNeighbors(graph.getEntityIndex(entity)))     max    = graph.getNumberOfNeighbors(graph.getEntityIndex(entity));
              }
              colstr.str = min_di + "/" + min + " to " + max_di + "/" + max + " Nbors";
              deg = max;
            }
            colstr.col = RTColorManager.getLogColor(deg);
            if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            return colstr;
          }

          /**
           *
           */
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label for showing underlying bundle (record) counts
         */
        protected class BundleCountLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str; int total;
            if (isLink(bin)) {
              total = cc.getBundles(bin).size();
              String revlink = reverseLink(bin);
              if (isLink(revlink)) total += cc.getBundles(revlink).size();
              str = "" + total + " Recs";
            } else str = "" + (total = cc.getBundles(bin).size()) + " Recs";
            Color color = RTColorManager.getLogColor(total);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }

          /**
           *
           */
          public String toString(String bin) { 
            int total;
            if (isLink(bin)) {
              total = cc.getBundles(bin).size();
              String revlink = reverseLink(bin);
              if (isLink(revlink)) total += cc.getBundles(revlink).size();
            } else total = cc.getBundles(bin).size();
            return "" + total + " Recs";
          }
        }

        /**
         * Label for counting entities within aggregated nodes.
         */
        protected class EntityCountLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            int total;
            String str = "" + (total = node_coord_set.get(bin).size()) + " Ents";
            Color color = RTColorManager.getLogColor(total);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }

          /**
           *
           */
          public String toString(String bin) { return "" + node_coord_set.get(bin).size() + " Ents"; }
        }

        /**
         * Label for the entity type
         */
        protected class EntityTypeLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str = toString(bin);
            Color  color = RTColorManager.getColor(str);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }
          public String toString(String bin) {
            Map<BundlesDT.DT,Integer> map = typeCounts(bin); StringBuffer sb = new StringBuffer(); Iterator<BundlesDT.DT> it;
            if        (map.keySet().size() == 1) { it = map.keySet().iterator(); sb.append("" + it.next());
            } else if (map.keySet().size() == 2 ||
                       map.keySet().size() == 3) { it = map.keySet().iterator(); sb.append("" + it.next());
                                                   while (it.hasNext()) sb.append("|" + it.next());
            } else sb.append("Multiple Data Types");
            return sb.toString();
          }
          protected Map<BundlesDT.DT,Integer> typeCounts(String bin) {
            Map<BundlesDT.DT,Integer> map = new HashMap<BundlesDT.DT,Integer>();
            Iterator<String> it = node_coord_set.get(bin).iterator(); while (it.hasNext()) {
              String       str = it.next();
              BundlesDT.DT dt  = BundlesDT.getEntityDataType(str);
              if (dt != null) { if (map.containsKey(dt) == false) map.put(dt, 1); else map.put(dt, map.get(dt) + 1); }
            }
            return map;
          }
        }

        /**
         * Label for showing just the entity name.
         */
/* ...DELETE...
        protected class OneLineEntityLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str = toString(bin);
            Color color = RTColorManager.getColor(str);
            if (g2d != null) shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }

          public String toString(String bin) { 
            if (node_coord_set.get(bin).size() == 1) return node_coord_set.get(bin).iterator().next();
            else                                     return Utils.calculateCIDR(node_coord_set.get(bin));
          }
        }
*/
        /**
         * Label for showing just the entity name.
         */
        protected class EntityLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str = toString(bin);
            Color color = RTColorManager.getColor(str);
            if (g2d != null) {
              // Short string ... or can't be broken into separate parts
              if (str.length() < (ml_threshold-2) || Utils.containsWhiteSpace(str) == false) {
                shape = clearStr(g2d, str, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                rendered_lines = 1;

              // Break it into separate parts... but no more than say three lines
              } else {
                Area area = new Area();
                String strs[] = Utils.breakIntoMultiLine(str, ml_threshold);

                if (ml_truncate == -1 || strs.length <= ml_truncate) { rendered_lines = strs.length; } else { rendered_lines = ml_truncate; }

                for (int i=0;i<rendered_lines;i++) {
                  Shape line_shape = clearStr(g2d, strs[i], x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                  area.add(new Area(line_shape));
                  y += Utils.txtH(g2d, strs[i]);
                }

                // Draw some dots to indicate not everything was rendered...
                if (rendered_lines != strs.length) {
                  Shape line_shape = clearStr(g2d, "...", x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
                  area.add(new Area(line_shape));
                  y += Utils.txtH(g2d, "...");
                }

                shape = area;
              }
            }
            return color;
          }
          public String toString(String bin) { 
            if (node_coord_set.get(bin).size() == 1) return node_coord_set.get(bin).iterator().next();
            else                                     return Utils.calculateCIDR(node_coord_set.get(bin));
          }
        }

        /**
         * Label for IP Ranges ... private vs public... some other random classes based
         * on the wikipedia page:  https://en.wikipedia.org/wiki/IP_address
         */
        protected class IPLabelLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            String str;
            if (node_coord_set.get(bin).size() == 1) str = node_coord_set.get(bin).iterator().next();
            else                                     str = Utils.calculateCIDR(node_coord_set.get(bin));

            String label = ipLabel(str);

            Color color = RTColorManager.getColor(label);
            if (g2d != null) shape = clearStr(g2d, label, x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
            
          }

          public String toString(String bin) {
            String str;
            if (node_coord_set.get(bin).size() == 1) str = node_coord_set.get(bin).iterator().next();
            else                                     str = Utils.calculateCIDR(node_coord_set.get(bin));
            return ipLabel(str);
          }

          protected String ipLabel(String str) {
            if        (Utils.isIPv4(str))       { return Utils.ipSpaceLabel(str); // IPv4 address
            } else if (str.indexOf(" - ") >= 0) { // might be an IP range ...
              int i = str.indexOf(" - "); 
              String s1 = str.substring(0,i), 
                     s2 = str.substring(i+3,str.length());
              if (Utils.isIPv4(s1) && Utils.isIPv4(s2)) return "IP Range";
              else                                      return "Not An IP Address";
            } else if (Utils.isIPv6(str))       { return Utils.ipSpaceLabel(str); // IPv6 address
            } else return "Not An IP Address";
          }
        }

        /**
         * Generic caching label maker for handling sets of labels.
         */
        protected abstract class CacheLM extends LM {
          String fld; int fld_i; public CacheLM(String fld) { this.fld = fld; fld_i = getRTParent().getRootBundles().getGlobals().fieldIndex(fld); }
          Map<Tablet,KeyMaker> km_lu    = new HashMap<Tablet,KeyMaker>();
          Set<Tablet>          ignore   = new HashSet<Tablet>();
          //
          public String[] stringKeys(Bundle bundle) {
            if (ignore.contains(bundle.getTablet())) return null;
            if (km_lu.containsKey(bundle.getTablet()) == false) {
              // System.err.println("CacheLM.stringKeys(): fld = \"" + fld + "\"");
              if (KeyMaker.tabletCompletesBlank(bundle.getTablet(), fld)) {
                km_lu.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), fld));
              } else { ignore.add(bundle.getTablet()); return null; }
            }
            return km_lu.get(bundle.getTablet()).stringKeys(bundle);
            }
          //
          public int[]    intKeys(Bundle bundle) {
          if (ignore.contains(bundle.getTablet())) return null;
          if (km_lu.containsKey(bundle.getTablet()) == false) {
            if (KeyMaker.tabletCompletesBlank(bundle.getTablet(), fld)) {
              km_lu.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), fld));
            } else { ignore.add(bundle.getTablet()); return null; }
          }
          return km_lu.get(bundle.getTablet()).intKeys(bundle);
          }
        }

        /**
         * Simple statistic labels (min/max/sum).
         */
        protected class SimpleStatLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public SimpleStatLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE; long sum = 0L;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                int ints[] = intKeys(it.next()); if (ints == null) continue;
                for (int i=0;i<ints.length;i++) {
                  if (min > ints[i]) min = ints[i];
                  if (max < ints[i]) max = ints[i];
                  sum += ints[i];
                }
              }
            }
            String str = (single ? "" : fld + " ") + min + "/" + max + "/" + sum;
            if (g2d != null) {
              x -= Utils.txtW(g2d, str)/2;
              if (single == false) {
                g2d.setColor(RTColorManager.getColor("label", "default")); g2d.drawString(fld + " ", x, y); x += Utils.txtW(g2d, fld + " ");
              }
              g2d.setColor(RTColorManager.getColor("data",  "min"));     g2d.drawString("" + min,  x, y); x += Utils.txtW(g2d, "" + min);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",       x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "max"));     g2d.drawString("" + max,  x, y); x += Utils.txtW(g2d, "" + max);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",       x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "sum"));     g2d.drawString("" + sum,  x, y);
            }
            return RTColorManager.getLogColor(sum);
          }
          public String toString(String bin) { return "Not Implemented (SimpleStatsLM)"; }
        }

        /**
         * Complex stats label maker (mean/avg/stdev).
         */
        protected class ComplexStatLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public ComplexStatLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            List<Integer> al = new ArrayList<Integer>(); double sum = 0.0;
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                int ints[] = intKeys(it.next()); if (ints == null) continue;
                for (int i=0;i<ints.length;i++) { al.add(ints[i]); sum += ints[i]; }
              }
            }
            if (al.size() == 0) return null;
            double avg   = sum/al.size();
            double stdev = Utils.calculateStandardDeviation(al,avg);
            Collections.sort(al);
            int   median   = al.get(al.size()/2);

            String median_str = "" + median,
                   avg_str    = Utils.humanReadableDouble(avg),
                   stdev_str  = Utils.humanReadableDouble(stdev);

            String str = (single ? "" : fld + " ") + median_str + "/" + avg_str + "/" + stdev_str;
            if (g2d != null) {
              x -= Utils.txtW(g2d, str)/2;
              if (single == false) {
                g2d.setColor(RTColorManager.getColor("label", "default")); g2d.drawString(fld + " ",        x, y); x += Utils.txtW(g2d, fld + " | ");
              }
              g2d.setColor(RTColorManager.getColor("data",  "median"));  g2d.drawString("" + median_str,  x, y); x += Utils.txtW(g2d, median_str);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",              x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("data",  "mean"));    g2d.drawString("" + avg_str,     x, y); x += Utils.txtW(g2d, avg_str);
              g2d.setColor(RTColorManager.getColor("label", "minor"));   g2d.drawString("/",            x, y); x += Utils.txtW(g2d, "/");
              g2d.setColor(RTColorManager.getColor("label", "stdev"));   g2d.drawString("" + stdev_str, x, y);
            }
            return RTColorManager.getLogColor(avg);
          }
          public String toString(String bin) { return "Not Implemented (ComplexStatsLM)"; }
        }

        /**
         * Label maker for handling sets of labels.
         */
        protected class ItemsLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public ItemsLM(String fld, boolean single) { super(fld); this.single = single;}
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Map<String,Integer> counter = new HashMap<String,Integer>();
            // Count elements
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { 
                  for (int i=0;i<strs.length;i++) {
                    if (counter.containsKey(strs[i]) == false) counter.put(strs[i], 1);
                    else                                       counter.put(strs[i], counter.get(strs[i]) + 1);
                  }
                }
              }
            }
            // Place them in an array and sort
            StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
            Iterator<String> it_str = counter.keySet().iterator();
            for (int i=0;i<sorter.length;i++) {
              String str = it_str.next(); sorter[i] = new StrCountSorter(str, counter.get(str));
            }
            Arrays.sort(sorter);
            // Put the three most common
            StringBuffer sb = new StringBuffer(); if (!single) sb.append(fld + " | ");
            // for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
            for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " ");
            if (sorter.length > 2) sb.append("... [" + sorter.length + " To]");
            // Draw the string
            if (g2d != null && sb.toString().equals(BundlesDT.NOTSET + " ") == false) shape = clearStr(g2d, sb.toString(), x, y, RTColorManager.getColor(fld), RTColorManager.getColor("label", "defaultbg"), true);
            return (sorter.length == 1 ? RTColorManager.getColor(sorter[0].toString()) : RTColorManager.getColor("set", "multi"));
          }
          public String toString(String bin) { return "Not Implemented (ItemsLM)"; }
        }

        /**
         * Label maker for counting the size of sets.
         */
        protected class CountLM extends CacheLM {
          boolean single; // Used to denote that the field header should not be displayed
          public CountLM(String fld, boolean single) { super(fld); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Set<String> set = new HashSet<String>();
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { for (int i=0;i<strs.length;i++) set.add(strs[i]); }
              }
            }
            if (g2d != null) shape = clearStr(g2d, (single ? "" : fld + " | ") + set.size() + " Els", 
                                              x, y, RTColorManager.getColor(fld), RTColorManager.getColor("label", "defaultbg"), true);
            return RTColorManager.getLogColor(set.size());
          }
          public String toString(String bin) { return "Not Implemented (CountLM)"; }
        }

        /**
         * Label maker for tag fields.
         */
        protected class TagsLM extends LM {
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            // Initialize variables
            StringBuffer sb = new StringBuffer(); Iterator<String> it, it_tags; ColStr colstr = new ColStr();
            long ts0 = bs.ts0(), ts1 = bs.ts1();

            // Determine if one entity or many
            if (node_coord_set.get(bin).size() == 1) {
              Set<String> tags = getRTParent().getEntityTags(node_coord_set.get(bin).iterator().next(),ts0,ts1);
              it_tags = tags.iterator();
              while (it_tags.hasNext()) { if (sb.length() > 0) sb.append(BundlesDT.DELIM); sb.append(it_tags.next()); }
              colstr.col = RTColorManager.getColor(sb.toString());
            } else                                   {
              // Go through the entities, accumulating the tags
              int entity_count = node_coord_set.get(bin).size();
              it = node_coord_set.get(bin).iterator();  Map<String,Integer> map = new HashMap<String,Integer>();
              while (it.hasNext()) {
                String          entity = it.next();
                Set<String> tags   = getRTParent().getEntityTags(entity, ts0, ts1);
                // Count the tags if they exist
                if (tags != null && tags.size() > 0) {
                  it_tags = tags.iterator();
                  while (it_tags.hasNext()) {
                    String tag = it_tags.next();
                    if (map.containsKey(tag) == false) map.put(tag, 1); else map.put(tag, map.get(tag)+1);
                  }
                }
              }
              // put the string buffer together
              if        (map.keySet().size() == 1) {
                it = map.keySet().iterator(); String tag = it.next(); int count = map.get(tag);
                if (count == entity_count) sb.append(tag); else sb.append(tag + " (" + count + ")");
                colstr.col = RTColorManager.getColor(tag);
              } else if (map.keySet().size() >= 2) {
                // Sort the counts
                StrCountSorter sorter[] = new StrCountSorter[map.keySet().size()];
                it = map.keySet().iterator();
                for (int i=0;i<sorter.length;i++) {
                  String tag = it.next();
                  sorter[i] = new StrCountSorter(tag, map.get(tag));
                }
                Arrays.sort(sorter);
                // Add the first two to the string
                sb.append(sorter[0].toString()); if (sorter[0].count() != entity_count) sb.append(" (" + sorter[0].count() + ")");
                sb.append(" " + BundlesDT.DELIM + " ");
                sb.append(sorter[1].toString()); if (sorter[1].count() != entity_count) sb.append(" (" + sorter[1].count() + ")");
                if (sorter.length > 2) sb.append(" ... [" + sorter.length + " To]");
                colstr.col = RTColorManager.getColor("set", "multi");
              }
            }
            if (sb.length() > 0) {
              colstr.str = sb.toString();
              if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, RTColorManager.getColor("label", "default"), RTColorManager.getColor("label", "defaultbg"), true);
            }
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label maker for a specific tag type in type-value pairs.
         */
        protected class TagTypesLM extends LM {
          String tag_type;
          public TagTypesLM(String tag_type) { this.tag_type = tag_type; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            // Initialize variables
            StringBuffer sb = new StringBuffer(); Iterator<String> it, it_tags; ColStr colstr = new ColStr();
            long ts0 = bs.ts0(), ts1 = bs.ts1();

            // Determine if one entity or many
            if (node_coord_set.get(bin).size() == 1) {
              int count = 0; // Keep track of the number of tags for the color
              // Get the tags for this entity and iterate over them
              Set<String> tags = getRTParent().getEntityTags(node_coord_set.get(bin).iterator().next(),ts0,ts1);
              // For each tag, determine if it matches the specified type -- if so, accumulate them into a string buffer
              it_tags = tags.iterator();
              while (it_tags.hasNext()) { 
                String tag = it_tags.next();
                if (Utils.tagIsTypeValue(tag)) {
                  String sep[] = Utils.separateTypeValueTag(tag);
                  if (sep[0].equals(tag_type)) {
                    if (sb.length() > 0) sb.append(BundlesDT.DELIM); 
                    sb.append(sep[1]); count++;
                  }
                }
              }
              // Pick the color based on the number of types assigned to this entity
              if      (count == 0) colstr.col = RTColorManager.getColor("label", "minor"); // Need to also change the color in the force directed color layout (i think...)
              else                 colstr.col = RTColorManager.getColor(sb.toString());

              // Assign the string
              colstr.str = sb.toString();
            } else                                   {
              // Go through the entities, accumulating the tags
              int entity_count = node_coord_set.get(bin).size();
              it = node_coord_set.get(bin).iterator();  Map<String,Integer> map = new HashMap<String,Integer>();
              while (it.hasNext()) {
                String      entity = it.next();
                Set<String> tags   = getRTParent().getEntityTags(entity, ts0, ts1);
                // Count the tags if they exist
                if (tags != null && tags.size() > 0) {
                  it_tags = tags.iterator();
                  while (it_tags.hasNext()) {
                    String tag = it_tags.next();
                    if (Utils.tagIsTypeValue(tag)) {
                      String sep[] = Utils.separateTypeValueTag(tag);
                      if (sep[0].equals(tag_type)) {
                        if (map.containsKey(sep[1]) == false) map.put(sep[1], 1); else map.put(sep[1], map.get(sep[1])+1);
                      }
                    }
                  }
                }
              }
              // put the string buffer together
              if        (map.keySet().size() == 1) {
                it = map.keySet().iterator(); String tag = it.next(); int count = map.get(tag);
                if (count == entity_count) sb.append(tag); else sb.append(tag + " (" + count + ")");
                colstr.col = RTColorManager.getColor(tag);
              } else if (map.keySet().size() >= 2) {
                // Sort the counts
                StrCountSorter sorter[] = new StrCountSorter[map.keySet().size()];
                it = map.keySet().iterator();
                for (int i=0;i<sorter.length;i++) {
                  String tag = it.next();
                  sorter[i] = new StrCountSorter(tag, map.get(tag));
                }
                Arrays.sort(sorter);
                // Add the first two to the string
                sb.append(sorter[0].toString()); if (sorter[0].count() != entity_count) sb.append(" (" + sorter[0].count() + ")");
                sb.append(" " + BundlesDT.DELIM + " ");
                sb.append(sorter[1].toString()); if (sorter[1].count() != entity_count) sb.append(" (" + sorter[1].count() + ")");
                if (sorter.length > 2) sb.append(" ... [" + sorter.length + " To]");
                colstr.col = RTColorManager.getColor("set", "multi");
              }
              colstr.str = sb.toString();
            }
            if (sb.length() > 0) {
              colstr.str = sb.toString();
              if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            }
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Label maker for post processed information.
         */
        protected class LinkPostLM extends CacheLM {
          PostProc proc; boolean single;
          public LinkPostLM(String field, String postproc, BundlesG globals, boolean single) { 
            super(field); proc = BundlesDT.createPostProcessor(postproc, globals); this.single = single; }
          public Color draw(Graphics2D g2d, String bin, int x, int y) {
            Map<String,Integer> counter = new HashMap<String,Integer>(); int no_match = 0;
            // Count elements
            for (int b=0;b<2;b++) {
              Iterator<Bundle> it;
              // Do both directins of the link
              if      (b == 0)                                  it = cc.getBundles(bin).iterator();
              else if (isLink(bin) && isLink(reverseLink(bin))) it = cc.getBundles(reverseLink(bin)).iterator();
              else                                              it = (new HashSet<Bundle>()).iterator();
              // Tally the packets
              while (it.hasNext()) {
                String strs[] = stringKeys(it.next());
                if (strs != null) { 
                  for (int i=0;i<strs.length;i++) {
                    if (proc.type().contains(BundlesDT.getEntityDataType(strs[i])) == false) { no_match++; }
                    String post[] = proc.postProcess(strs[i]);
                    for (int j=0;j<post.length;j++) {
                      if (counter.containsKey(post[j]) == false) counter.put(post[j], 1);
                      else                                       counter.put(post[j], counter.get(post[j]) + 1);
                    }
                  }
                }
              }
            }
            // Place them in an array and sort
            StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
            Iterator<String> it_str = counter.keySet().iterator();
            for (int i=0;i<sorter.length;i++) {
              String str = it_str.next(); sorter[i] = new StrCountSorter(str, counter.get(str));
            }
            Arrays.sort(sorter);
            // Put the three most common
            StringBuffer sb = new StringBuffer(); if (single == false) sb.append(fld + " | ");
            // for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
            for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " ");
            if (sorter.length > 2) sb.append("... [" + sorter.length + " To]");
            if (no_match > 0) sb.append(" [" + no_match + " NoM]");
            // Draw the string
            Color color = (sorter.length == 1 ? RTColorManager.getColor(sorter[0].toString()) : RTColorManager.getColor("set", "multi"));
            if (g2d != null) shape = clearStr(g2d, sb.toString(), x, y, color, RTColorManager.getColor("label", "defaultbg"), true);
            return color;
          }
          public String toString(String bin) { return "Not Implemented (LinkPostLM)"; }
        }

        /**
         * Label maker for post processed node information.
         */
        protected class NodePostLM extends LM {
          PostProc proc;
          public NodePostLM(String postproc, BundlesG globals) { proc = BundlesDT.createPostProcessor(postproc, globals); }
          public Color draw(Graphics2D g2d, String bin, int x, int y) { return gen(g2d, bin, x, y).col; }
          protected ColStr gen(Graphics2D g2d, String bin, int x, int y) {
            ColStr colstr = new ColStr();
            if (node_coord_set.get(bin).size() == 1) {
              colstr.str = node_coord_set.get(bin).iterator().next();
              if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                String       post[] = proc.postProcess(colstr.str);
                if (post == null || post.length == 0 || (post.length == 1 && post[0].equals(BundlesDT.NOTSET))) return colstr;
                StringBuffer sb = new StringBuffer();
                sb.append(post[0]); for (int i=1;i<post.length;i++) sb.append(BundlesDT.DELIM + post[i]);
                colstr.str = sb.toString(); colstr.col = RTColorManager.getColor(colstr.str);
              }  else return colstr;
            } else                                   {
              Map<String,Integer> counter = new HashMap<String,Integer>(); int no_match = 0;
              Iterator<String> it = node_coord_set.get(bin).iterator();
              while (it.hasNext()) {
                colstr.str = it.next();
                if (proc.type().contains(BundlesDT.getEntityDataType(colstr.str))) {
                  String post[] = proc.postProcess(colstr.str);
                  for (int i=0;i<post.length;i++) {
                    if (counter.containsKey(post[i])) counter.put(post[i],counter.get(post[i])+1); else counter.put(post[i],1);
                  }
                } else no_match++;
              }
              // Place them in an array and sort
              StrCountSorter sorter[] = new StrCountSorter[counter.keySet().size()];
              Iterator<String> it_str = counter.keySet().iterator();
              for (int i=0;i<sorter.length;i++) {
                colstr.str = it_str.next(); sorter[i] = new StrCountSorter(colstr.str, counter.get(colstr.str));
              }
              Arrays.sort(sorter);
              // Put the three most common
              StringBuffer sb = new StringBuffer();
              for (int i=0;i<((sorter.length>2)?2:sorter.length);i++) sb.append(sorter[i].toString() + " (" + sorter[i].count() + ") ");
              if (sorter.length > 2) sb.append("... [" + sorter.length + "]");
              if (no_match > 0 && sb.length() > 0) sb.append(" + " + no_match + "NoM"); // Only add no-match when there's a string...
              colstr.str = sb.toString();
              if (sorter.length == 1) colstr.col = Utils.strColor(sorter[0].toString()); else colstr.col = RTColorManager.getColor("set", "multi");
            }
            if (g2d != null) shape = clearStr(g2d, colstr.str, x, y, colstr.col, RTColorManager.getColor("label", "defaultbg"), true);
            return colstr;
          }
          public String toString(String bin) { return gen(null, bin, 0, 0).str; }
        }

        /**
         * Simple structure to hold both a string and a color.
         */
        private class ColStr { public String str; public Color col; public ColStr() { str = "0"; col = RTColorManager.getColor("label", "minor"); } }

        /**
         * From a label string, return the appropriate label maker.
         *
         *@param label labeling option
         *
         *@return label maker
         */
        protected LM createLM(String label, boolean single) {
          BundlesG globals = getRTParent().getRootBundles().getGlobals();
          if      (label.equals(TIMEFRAME_LM))         return new TimeLM(TimeLM.BOTH,  TimeLM.EXACT);
          else if (label.equals(FIRSTHEARD_LM))        return new TimeLM(TimeLM.FIRST, TimeLM.EXACT);
          else if (label.equals(LASTHEARD_LM))         return new TimeLM(TimeLM.LAST,  TimeLM.EXACT);
          else if (label.equals(TIMEFRAME_MONTHS_LM))  return new TimeLM(TimeLM.BOTH,  TimeLM.MONTHS);
          else if (label.equals(TIMEFRAME_DAYS_LM))    return new TimeLM(TimeLM.BOTH,  TimeLM.DAYS);

          else if (label.equals(DEGREE_LM))        return new DegreeLM();
          else if (label.equals(BUNDLECOUNT_LM))   return new BundleCountLM();
          else if (label.equals(TAGS_LM))          return new TagsLM();
          else if (label.startsWith(TAG_TYPE_LM))  return new TagTypesLM(label.substring(TAG_TYPE_LM.length(),label.length()));

          else if (label.equals(ENTITYTYPE_LM))    return new EntityTypeLM();
          else if (label.equals(ENTITYCOUNT_LM))   return new EntityCountLM();
          else if (label.equals(ENTITY_LM))        return new EntityLM();

          else if (label.equals(IP_LABEL_LM))      return new IPLabelLM();

          else if (label.endsWith(SIMPLESTAT_LM))  return new SimpleStatLM(label.substring(0,label.indexOf(BundlesDT.DELIM)),  single);
          else if (label.endsWith(COMPLEXSTAT_LM)) return new ComplexStatLM(label.substring(0,label.indexOf(BundlesDT.DELIM)), single);
          else if (label.endsWith(ITEMS_LM))       return new ItemsLM(label.substring(0,label.lastIndexOf(BundlesDT.DELIM)),       single);
          else if (label.endsWith(COUNT_LM))       return new CountLM(label.substring(0,label.lastIndexOf(BundlesDT.DELIM)),       single);
          else if (label.indexOf(BundlesDT.DELIM) >= 0) {
            String   start   = label.substring(0,label.indexOf(BundlesDT.DELIM)),
                     rest    = label.substring(label.indexOf(BundlesDT.DELIM)+1,label.length());
            if (globals.fieldIndex(start) != -1) return new LinkPostLM(start,rest,globals,single);
            else                                 return new NodePostLM(label,globals);
          } else {
            return new NodePostLM(label,globals);
          }
        }
      }
    }
  }

  /**
   * Strings for the variety of non-application specific labeling options.
   */
  public final static String TIMEFRAME_LM            = BundlesDT.DELIM + "Time Frame"   + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             TIMEFRAME_MONTHS_LM     = BundlesDT.DELIM + "TF Months"    + BundlesDT.DELIM, // 
                             TIMEFRAME_DAYS_LM       = BundlesDT.DELIM + "TF Days"      + BundlesDT.DELIM, // 
                             LASTHEARD_LM            = BundlesDT.DELIM + "Last Heard"   + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             FIRSTHEARD_LM           = BundlesDT.DELIM + "First Heard"  + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             DEGREE_LM               = BundlesDT.DELIM + "Degree"       + BundlesDT.DELIM, // Graph       <= entities       <= Node String
                             BUNDLECOUNT_LM          = BundlesDT.DELIM + "Bundle Count" + BundlesDT.DELIM, // Bundle Set  <= CounterContext <= Bin
                             TAGS_LM                 = BundlesDT.DELIM + "All Tags"     + BundlesDT.DELIM,
                             TAG_TYPE_LM             = BundlesDT.DELIM + "Tag Type"     + BundlesDT.DELIM,
                             ENTITYTYPE_LM           = BundlesDT.DELIM + "Entity Type"  + BundlesDT.DELIM, // Node String (bin)
                             ENTITYCOUNT_LM          = BundlesDT.DELIM + "Entity Count" + BundlesDT.DELIM, // Node String (bin)
                             ENTITY_LM               = BundlesDT.DELIM + "Entity"       + BundlesDT.DELIM, // Node String (bin)
                             SIMPLESTAT_LM           = BundlesDT.DELIM + "Min/Max/Sum",                    // Bundle Set  <= CounterContext <= Bin
                             COMPLEXSTAT_LM          = BundlesDT.DELIM + "Med/Avg/StDev",                  // Bundle Set  <= CounterContext <= Bin
                             ITEMS_LM                = BundlesDT.DELIM + "Items",                          // Bundle Set  <= CounterContext <= Bin
                             COUNT_LM                = BundlesDT.DELIM + "Count",                          // Bundle Set  <= CounterContext <= Bin
                             IP_LABEL_LM             = BundlesDT.DELIM + "IP Label"     + BundlesDT.DELIM; // IP Labeler

  /**
   * Dialog to display fast layout options so that user can more quickly select an initial layout.
   */
  class LayoutSmallMultiplesDialog extends JDialog implements WindowListener {
    private static final long serialVersionUID = -354867787917560057L;
    // List of the layout threads to split the work
    List<LayoutThread> layout_threads = new ArrayList<LayoutThread>();

    // Viewer for layouts
    ViewerComponent    viewer;

    /**
     * Dimensions of the subimages
     */
    final int w = 256, h = 256;

    /**
     * Constructor
     */
    public LayoutSmallMultiplesDialog() {
      super(getRTParent(), "Small Multiples Layouts", true);

      // Execute the efficient layout options
      String layouts[] = GraphLayouts.getLayoutAlgorithms();
      for (int i=0;i<layouts.length;i++) {
        String layout = layouts[i]; if (GraphLayouts.layoutEfficient(layout, graph)) {
          LayoutThread layout_thread; layout_threads.add(layout_thread = new LayoutThread(layout, w, h)); (new Thread(layout_thread)).start();
        }
      }

      // Wait until they finish...
      Iterator<LayoutThread> it = layout_threads.iterator(); while (it.hasNext()) {
        LayoutThread layout_thread = it.next(); while (layout_thread.done == false) { try { Thread.sleep(100); } catch (InterruptedException ie) { } }
      }

      // Create the actual viewer
      JScrollPane scroll_pane; add("Center", scroll_pane = new JScrollPane(viewer = new ViewerComponent()));
      scroll_pane.getVerticalScrollBar().setUnitIncrement(32);
      pack(); setSize(1024+21,768+10); setVisible(true);

      addWindowListener(this);
    }

    /**
     * Close the dialog... dispose of the resources.
     */
    protected void closeDialog() { setVisible(false); dispose(); }

    /**
     * Window Listener
     */
    public void windowDeactivated(WindowEvent we) { } 
    public void windowActivated  (WindowEvent we) { } 
    public void windowDeiconified(WindowEvent we) { } 
    public void windowIconified  (WindowEvent we) { } 
    public void windowClosed     (WindowEvent we) { } 
    public void windowClosing    (WindowEvent we) { this.dispose(); }  // Not sure if this is really needed or not...
    public void windowOpened     (WindowEvent we) { } 

    /**
     * Thread to split each layout across multiple cores
     */
    class LayoutThread implements Runnable {
      String layout; int w, h; BufferedImage bi; boolean done = false; Map<String,Point2D> e2xy = new HashMap<String,Point2D>();

      // Construct -- really just save off the variables
      public LayoutThread(String layout, int w, int h) { 
        this.layout = layout; this.w = w; this.h = h; 
      }

      // Worker thread
      public void run() {
        try {
          // Copy the current layout (it may be relevant to the layout...  not usually though)
          Iterator<String> it = entity_to_wxy.keySet().iterator(); while (it.hasNext()) { String e = it.next(); e2xy.put(e, entity_to_wxy.get(e)); }
          // Execute the layout
          RTGraphComponent.RenderContext myrc = (RTGraphComponent.RenderContext) (getRTComponent().getRTRenderContext()); if (myrc == null) return;
          (new GraphLayouts()).executeLayoutAlgorithm(layout, graph, myrc.filterEntities(getRTParent().getSelectedEntities()), e2xy, null, null, null);
          // Render to an image
          bi = GraphUtils.render(new UniGraph(graph), e2xy);
          // Clear the done flag
        } finally { done = true; }
      }
    }

    /**
     * Simple viewer to show the various layouts.
     */
    class ViewerComponent extends JComponent implements MouseListener, MouseMotionListener {
      private static final long serialVersionUID = -2348699887930666057L;
      // Keep track of the placement of the layout renderings
      Map<LayoutThread,Rectangle2D> map = new HashMap<LayoutThread,Rectangle2D>();
      int mouse_x = -1, mouse_y = -1;
      // Construct the viewer...  get the dimensions right for the scroll pane
      public ViewerComponent() {
        int images = layout_threads.size();
        int comp_w = 4*w, comp_h = (h*images)/4; if ((images%4) != 0) comp_h += h;
        Dimension dimension = new Dimension(comp_w, comp_h); setPreferredSize(dimension); setMinimumSize(dimension);
        addMouseListener(this); addMouseMotionListener(this);
      }
      // Render by pasting the various images into the view - keep track of their locations for selection
      public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g; g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        int x = 0, y = 0; Iterator<LayoutThread> it = layout_threads.iterator(); while (it.hasNext()) {
          LayoutThread layout_thread = it.next();
          if (layout_thread.bi != null) { 
            g2d.drawImage(layout_thread.bi, x*w, y*h, w, h, Color.white, null); 
            if (GraphLayouts.layoutStable(layout_thread.layout) == false) {
              g2d.setColor(Color.red);
              g2d.drawOval(x*w+4,y*w+4,4,4);
            }
            map.put(layout_thread, new Rectangle2D.Double(x*w,y*h,w,h));
            // if the mouse is in this one, write the label beneath the image
            if (map.get(layout_thread).contains(mouse_x, mouse_y)) {
              if (GraphLayouts.layoutStable(layout_thread.layout)) g2d.setColor(Color.black);
              else                                                 g2d.setColor(Color.red);
              g2d.drawString(layout_thread.layout, (int) ((x*w + w/2) - Utils.txtW(g2d, layout_thread.layout)/2.0), (int) (y*h + h - 2));
            }
          }
          x++; if (x >= 4) { x = 0; y++; }
        }
      }
      // Helper to find layout thread
      public LayoutThread find(int x, int y) {
        Iterator<LayoutThread> it = map.keySet().iterator(); while (it.hasNext()) {
          LayoutThread layout_thread = it.next(); if (map.get(layout_thread).contains(x,y)) return layout_thread;
        }
        return null;
      }
      // Listener for mouse motion to write the layout name beneath the image
      public void mouseMoved    (MouseEvent me) { mouse_x = me.getX(); mouse_y = me.getY(); repaint(); }
      public void mouseDragged  (MouseEvent me) { }

      // Listener to user input to select the correct layout
      public void mouseEntered  (MouseEvent me) { }
      public void mouseExited   (MouseEvent me) { }
      public void mousePressed  (MouseEvent me) { }
      public void mouseReleased (MouseEvent me) { }
      public void mouseClicked  (MouseEvent me) { 
        if (me.getButton() == MouseEvent.BUTTON1) { // Select the layout and apply to the parent panel
          LayoutThread layout_thread = find(me.getX(), me.getY());
          if (layout_thread != null) {
            entity_to_wxy = layout_thread.e2xy; closeDialog(); 
            transform(); zoomToFit(); getRTComponent().render(); // All of these shouldn't be required to get the component to re-render :(
          }
        } else { // Re-run the layout if the layout is an unstable approach
          LayoutThread layout_thread = find(me.getX(), me.getY());
          if (layout_thread != null && GraphLayouts.layoutStable(layout_thread.layout) == false) {
            layout_thread.run();
            repaint();
          }
        }
      }
    }
  }

  /**
   * Dialog to construct a graph pattern to search.
   */
  class IsomorphismPatternCreatorDialog extends JDialog {
    private static final long serialVersionUID = -5448951827937566057L;
    /**
     * Node Template - describes a node pattern
     */
    class NodeTemplate {
      Set<Utils.Symbol> symbol_req     = new HashSet<Utils.Symbol>();
      int               degree_min     = -1, // This should be consistent with the described pattern
                        degree_max     = -1, // Ditto
                        bundles_min    = -1,
                        bundles_max    = -1;
      Pattern           entity_pattern = null;
    }

    /**
     * Edge Template - describes an edge pattern
     *
     * edge pattern example:
     *
     * ((source in {x,y}) or (octs < 20)) and (spkt+dpkt > 5) or (dpt in {53,80})
     *
     * precedence:  {}
     *              ()
     *              in
     *              * /
     *              + -
     *              > < <= >= = <>
     *              !
     *              and or xor
     */
    class EdgeTemplate {
      NodeTemplate src_node,
                   dst_node;
      boolean      match_dir    = false; // if true, src must match src and dst must match dst
      int          bundles_min  = -1,
                   bundles_max  = -1;
      String       edge_pattern = "";
    }

    /**
     *
     */
    public IsomorphismPatternCreatorDialog() {
      super(getRTParent(), "Isomorphism Pattern Creator", false);
      add("Center", new CreatorComponent());
      setVisible(true);
    }

    /**
     *
     */
    class CreatorComponent extends JComponent {
      private static final long serialVersionUID = -1924652887937566057L;
      public void paintComponent(Graphics g) {
        // Graphics2D g2d = (Graphics2D) g;
      }
    }
  }

  /**
   * Create an interactive edges dialog.
   */
  public void interactiveEdgesDialog() {
    new InteractiveEdgesDialog();
  }

  /**
   * Dialog for interactively defining the edges in the graph and getting a quick preview
   * of the complexity in the graph.
   */
  class InteractiveEdgesDialog extends JDialog implements WindowListener {
    private static final long serialVersionUID = -2848601880143566057L;
    /**
     * All possible fields to use for the relationships
     */
    String my_blanks[];

    /**
     * Interactive edges component
     */
    IEComponent ie_component;

    /**
     *
     */
    public InteractiveEdgesDialog() {
      super(getRTParent(), "Interactive Edge Dialog", true);
      
      // Determine which fields to use
      my_blanks = nodeBlanks(getRTParent().getRootBundles().getGlobals());

      // Configure the dialog components
      // - Create the interactive component
      getContentPane().add("Center", ie_component = new IEComponent()); 

      // - Create the buttons panel
      JPanel panel = new JPanel(new FlowLayout()); JButton bt;

      // -- The "Create Graph" action
      panel.add(bt = new JButton("Create Graph"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { 
          setVisible(false); dispose();
          createEdges();
      } } );

      // -- The "Cancel" dialog action
      panel.add(bt = new JButton("Cancel"));        
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setVisible(false); dispose(); } } );

      getContentPane().add("South", panel);

      addWindowListener(this);
      Dimension d = new Dimension(512,512); setPreferredSize(d); setSize(d);
      setVisible(true);
    }

    /**
     * Create the edges in the calling graph panel.
     */
    private void createEdges() {
      Iterator<InteractiveEdge> it_ie = interactive_edges.iterator(); while (it_ie.hasNext()) {
        InteractiveEdge ie = it_ie.next();
        addRelationship(ie.fm,  Utils.CIRCLE_STR, false, ie.to, Utils.CIRCLE_STR, false, STYLE_SOLID_STR, true, true);
      }
    }

    /**
     * Remove any interactive edges that contain the specified field.
     *
     *@param field field to search for
     */
    public void removeInteractiveEdges(String field) {
      Iterator<InteractiveEdge> it_ie = interactive_edges.iterator(); while (it_ie.hasNext()) {
        InteractiveEdge ie = it_ie.next();
        if (ie.fm.equals(field) || ie.to.equals(field)) it_ie.remove();
      }
      if (interactive_edges.size() > 0) { ie_graph = createGraphFromInteractiveEdges(); } else { ie_graph = null; }
      bookkeepIEGraph(); 
      ie_component.repaint();
    }

    /**
     * Add an edge between field 0 and field 1.
     *
     *@param field0 field 0
     *@param field1 field 1
     */
    public void addInteractiveEdge(String field0, String field1) {
      interactive_edges.add(new InteractiveEdge(field0, field1));
      ie_graph = createGraphFromInteractiveEdges(); 
      bookkeepIEGraph(); ie_component.repaint();
    }

    /**
     * Perform bookkeeping on cached variables for the ie graph.
     */
    private void bookkeepIEGraph() {
      if (ie_graph != null) {
        ie_graph_edge_count    = GraphUtils.countEdges(ie_graph);
        ie_graph_degree_distro = GraphUtils.calculateDegreeDistributionAggregate(ie_graph);
        ie_wmap                = new HashMap<String,Point2D>();

        for (int i=0;i<ie_graph.getNumberOfEntities();i++) ie_wmap.put(ie_graph.getEntityDescription(i), new Point2D.Double(Math.random(), Math.random()));

        if      (ie_graph.getNumberOfEntities() < 80)   
          (new GraphLayouts()).executeLayoutAlgorithm(GraphLayouts.MDS_ITERATIVE_PROP_STR, ie_graph, null, ie_wmap, null, null, null);
        else if (ie_graph.getNumberOfEntities() < MAX_IE_NODES_FOR_RENDER)
          (new GraphLayouts()).executeLayoutAlgorithm(GraphLayouts.HYPERTREE_PLUS_STR,     ie_graph, null, ie_wmap, null, null, null);
      } else {
        ie_graph_edge_count = 0;
        ie_graph_degree_distro = null;
        ie_wmap = null;
      }
    }

    /**
     * Maximum number of nodes for an interactive render
     */
    final int MAX_IE_NODES_FOR_RENDER = 2000;

    /**
     * Graph represented by the interactive edges
     */
    MyGraph ie_graph = null;

    /**
     * Count of the edges in the ie graph
     */
    int ie_graph_edge_count = 0;

    /**
     * Degree distribution (aggregated) of the ie graph
     */
    Map<Integer,Integer> ie_graph_degree_distro = new HashMap<Integer, Integer>();

    /**
     * Layout for the interactive edge graph
     */
    Map<String,Point2D> ie_wmap = null;

    /**
     * List of interactive edges added so far
     */
    List<InteractiveEdge> interactive_edges = new ArrayList<InteractiveEdge>();

    /**
     * Simple structure for holding an edge
     */
    class InteractiveEdge { 
      String fm, to; 
      public InteractiveEdge(String fm, String to) { this.fm = fm; this.to = to; } 
      public boolean equals(InteractiveEdge other) { return this.fm.equals(other.fm) && this.to.equals(other.to); }
    }

    /**
     * Create the graph represented by the interactive edges.  This just calls another
     * helper method in another class.
     *
     *@return graph representing current interactive edges
     */
    public MyGraph createGraphFromInteractiveEdges() {
      String edges[][] = new String[interactive_edges.size()][2];
      Iterator<InteractiveEdge> it_ie = interactive_edges.iterator();
      int i = 0; while (it_ie.hasNext() && i < edges.length) {
        InteractiveEdge ie = it_ie.next();
        edges[i][0] = ie.fm; edges[i][1] = ie.to;
        i++;
      }
      return new UniGraph(GraphUtils.createGraph(getRTParent().getRootBundles(), edges));
    }

    /**
     * Window event listeners
     */
    public void windowActivated   (WindowEvent we) { }
    public void windowIconified   (WindowEvent we) { }
    public void windowDeactivated (WindowEvent we) { }
    public void windowDeiconified (WindowEvent we) { }
    public void windowOpened      (WindowEvent we) { }
    public void windowClosing     (WindowEvent we) { setVisible(true); }
    public void windowClosed      (WindowEvent we) { dispose(); }

    /**
     * Lookup from field to the connection point for that field
     */
    Map<String,       Point2D>          field_to_point           = new HashMap<String,Point2D>();

    /**
     * Lookup from the field to the polygon that bounds the field
     */ 
    Map<String,       GeneralPath>      field_to_polygon         = new HashMap<String,GeneralPath>();

    /**
     * Lookup from the curve to the underlying interactive edge
     */
    Map<CubicCurve2D, InteractiveEdge>  curve_to_interactiveedge = new HashMap<CubicCurve2D,InteractiveEdge>();

    /**
     * Primary (only) component for the dialog
     */
    class IEComponent extends JComponent implements MouseListener, MouseMotionListener {
      private static final long serialVersionUID = -3168601297932566057L;
      /**
       * Construct the interactive edge component.  In this case, add the mouse listeners.
       */
      public IEComponent() { addMouseListener(this); addMouseMotionListener(this); }

      /**
       * Paint method -- draw the blanks, any assigned relationships for the blanks, any
       * interactions, and stats related to the graph instance that would match the relationships.
       *
       *@param g graphic primitive
       */
      public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Draw information about the nodes and edges
        MyGraph graph = ie_graph; if (graph != null && graph.getNumberOfEntities() > 1) {
          int txt_h_mod = Utils.txtH(g2d, "0") + 3;
          int y         = txt_h_mod;

          // Draw stats about nodes and edges
          g2d.drawString("Nodes = " + graph.getNumberOfEntities(),  5, y); y += txt_h_mod;
          g2d.drawString("Edges = " + ie_graph_edge_count,          5, y); y += txt_h_mod;

          // Draw a degree distribution
          // - Get the maximum (for scaling)
          y = getHeight() - 10 * txt_h_mod;
          int max_count = 1; Iterator<Integer> it_i = ie_graph_degree_distro.keySet().iterator();
          while (it_i.hasNext()) { int count = ie_graph_degree_distro.get(it_i.next()); if (count > max_count) max_count = count; }

          for (int i=0;i<10;i++) {
            int index; String index_str;
            if      (i <= 5) { index = i;   index_str = "Deg " + i; }
            else if (i == 6) { index = 6;   index_str = "Deg <10";  }
            else if (i == 7) { index = 10;  index_str = "Deg <20";  }
            else if (i == 8) { index = 20;  index_str = "Deg <100"; }
            else             { index = 100; index_str = "Deg >100"; }

            int count = 0; if (ie_graph_degree_distro.containsKey(index)) count = ie_graph_degree_distro.get(index);

            int max_bar_w = 80; int bar_w = (max_bar_w * count) / max_count;

            g2d.setColor(new Color(0.9f, 0.9f, 0.9f)); g2d.fillRect(5, y - txt_h_mod + 2, max_bar_w, txt_h_mod - 3);
            g2d.setColor(Color.lightGray);             g2d.fillRect(5, y - txt_h_mod + 2, bar_w,     txt_h_mod - 3);

            g2d.setColor(new Color(0.5f, 0.5f, 0.5f));
            g2d.drawString(index_str, 5, y - 2);

            y += txt_h_mod;
          }

          // Render a thumbnail of the graph (if there are a reasonable number of entities)
          Map<String,Point2D> wmap = ie_wmap;
          if (graph.getNumberOfEntities() < MAX_IE_NODES_FOR_RENDER && wmap != null) {
            // - render the image and place it into the center
            BufferedImage bi = GraphUtils.render(new UniGraph(graph), wmap);
            int my_w = getWidth()/2, my_h = getHeight()/2; if (my_w < 8) my_w = 8; if (my_h < 8) my_h = 8;
            if (my_w > bi.getWidth()) my_w = bi.getWidth(); if (my_h > bi.getHeight()) my_h = bi.getHeight();
            g2d.drawImage(bi, getWidth()/2 - my_w/2, getHeight()/2 - my_h/2, my_w, my_h, Color.black, null);
          }
        }

        // Draw the labels/blanks in a rotated circle
        field_to_point.clear();
        for (int i=0;i<my_blanks.length;i++) {
          double angle        = 2 * Math.PI * i / my_blanks.length,
                 inner_radius = getWidth()/3.0;
          double dx           = Math.cos(angle),
                 dy           = Math.sin(angle), pdx = dy, pdy = -dx;
          double x            = getWidth()  /2.0 + dx * inner_radius, 
                 y            = getHeight() /2.0 + dy * inner_radius;

          // Render the string
          g2d.setColor(Color.black);
          Utils.drawRotatedString(g2d, my_blanks[i], (int) x, (int) y, angle);

          // Save the interior point -- midway up the label
          double txt_w = Utils.txtW(g2d, my_blanks[i]) + 10.0, 
                 txt_h = Utils.txtH(g2d, my_blanks[i]);
          field_to_point.put(my_blanks[i], new Point2D.Double(x + dx*0 + pdx*txt_h/2.0, y + dy*0 + pdy*txt_h/2.0));

          // Calculate the bounding box for the text
          GeneralPath gp = new GeneralPath();
          gp.moveTo(x,                             y);
          gp.lineTo(x + dx*0     + pdx*txt_h,      y + dy*0     + pdy*txt_h);
          gp.lineTo(x + dx*txt_w + pdx*txt_h,      y + dy*txt_w + pdy*txt_h);
          gp.lineTo(x + dx*txt_w + pdx*0,          y + dy*txt_w + pdy*0);
          gp.closePath();
          // g2d.draw(gp);

          field_to_polygon.put(my_blanks[i], gp);
        }

        // Draw the existing edges
        curve_to_interactiveedge.clear();
        Iterator<InteractiveEdge> it_ie = interactive_edges.iterator(); while (it_ie.hasNext()) {
          InteractiveEdge ie = it_ie.next(); String fm = ie.fm, to = ie.to;
          Point2D fm_pt = field_to_point.get(fm), to_pt = field_to_point.get(to);
          // g2d.draw(new Line2D.Double(fm_pt, to_pt));
          double x_avg = (fm_pt.getX() + to_pt.getX() + (getWidth()/2))/3.0,
                 y_avg = (fm_pt.getY() + to_pt.getY() + (getHeight()/2))/3.0;
          CubicCurve2D.Double cc2d = new CubicCurve2D.Double(fm_pt.getX(), fm_pt.getY(),
                                                             x_avg, y_avg, x_avg, y_avg,
                                                             to_pt.getX(), to_pt.getY());
          g2d.draw(cc2d);
          curve_to_interactiveedge.put(cc2d, ie);
        }

        // Draw interactions
        String fld0_copy = m_fld0;
        if (fld0_copy != null) {
          Point2D pt = field_to_point.get(fld0_copy);
          g2d.drawLine((int) pt.getX(), (int) pt.getY(), mx, my);
        }
      }

      /**
       * Mouse field 0 -- null if no interactions
       */
      String m_fld0 = null,

      /**
       * Mouse field 1
       */
             m_fld1 = null;

      /**
       * Mouse x coordinate
       */
      int    mx,

      /**
       * Mouse y coordinate
       */
             my;

      /**
       * Mouse Listener Implementations
       */
      public void mouseEntered  (MouseEvent me) { m_fld0 = m_fld1 = null; repaint(); }
      public void mouseExited   (MouseEvent me) { m_fld0 = m_fld1 = null; repaint(); }
      public void mousePressed  (MouseEvent me) { 
        m_fld0 = m_fld1 = null;
        Iterator<String> it = field_to_polygon.keySet().iterator(); while (it.hasNext()) {
          String      field = it.next();
          GeneralPath gp    = field_to_polygon.get(field);
          if (gp.contains(me.getX(), me.getY())) { m_fld0 = field; return; }
        }
      }
      public void mouseReleased (MouseEvent me) { 
        if (m_fld0 == null) return;
        Iterator<String> it = field_to_polygon.keySet().iterator(); while (it.hasNext()) {
          String      field = it.next();
          GeneralPath gp    = field_to_polygon.get(field);
          if (gp.contains(me.getX(), me.getY())) { m_fld1 = field; addInteractiveEdge(m_fld0, m_fld1); m_fld0 = m_fld1 = null; repaint(); return; }
        }
        m_fld0 = m_fld1 = null;
        repaint();
      }
      public void mouseClicked  (MouseEvent me) { 
        Iterator<String> it = field_to_polygon.keySet().iterator(); while (it.hasNext()) {
          String      field = it.next();
          GeneralPath gp    = field_to_polygon.get(field);
          if (gp.contains(me.getX(), me.getY())) { removeInteractiveEdges(field); }
        }
        repaint();
      }
      public void mouseDragged  (MouseEvent me) { mx = me.getX(); my = me.getY(); if (m_fld0 != null) repaint(); }
      public void mouseMoved    (MouseEvent me) { }
    }
  }
}

