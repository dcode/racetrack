/* 

Copyright 2019 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;


import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListModel;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.util.Utils;

/**
 * Implementation of a dynamically allocated histogram panel based on the number of
 * elements the user selects in the jlist.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTHistoListPanel extends RTPanel implements ListSelectionListener {
  /**
   * 
   */
  private static final long serialVersionUID = -4943159372123192598L;

  /**
   * Field list to include in the parallel coordinates view.
   */
  JList<String> fields_ls;

  /**
   * Panel with the histograms.
   */
  JPanel histos;

  /**
   * Construct an instance of the parallel coordinates panel using
   * the specified parent object.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt parent GUI instance
   */
  public RTHistoListPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type, win_pos, win_uniq, rt);   
    // Construct the GUI
    add("West",   new JScrollPane(fields_ls = new JList<String>()));
    add("Center", histos = new JPanel(new GridLayout(1,1)));

    // Update members with application data
    updateBys();

    // Listeners
    fields_ls.addListSelectionListener(this);
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "histolist"; }

  /**
   * Implementation of the list selection event.  Determine what's changed and
   * reshape the panel to add/subtract the related histograms.
   *
   *@param lse list selection event
   */
  public synchronized void valueChanged(ListSelectionEvent lse) {
    String strs[] = getSelection(); // for (int i=0;i<strs.length;i++) System.err.println("" + i + ". " + strs[i]);

    remove(histos); histos = new JPanel();

    synchronized (my_histos_al) {
      my_histos_al.clear();

      // If nothing is selected, just make the histo area empty
      if (strs == null || strs.length == 0) {
        histos.setLayout(new GridLayout(1,1));

      // Else fill with histograms
      } else {
        histos.setLayout(new GridLayout(1, strs.length));
        for (int i=0;i<strs.length;i++) {
          RTHistoPanel histo;
          histos.add(histo = new RTHistoPanel(win_type, win_pos, win_uniq, getRTParent(), false));
          my_histos_al.add(histo);
          histo.binBy(strs[i]);
        }
      }
    }

    add("Center", histos); 

    getRTPanelFrame().validate(); // Force the component to re-layout the components

    repaint();
  }

  /**
   * Override the standard method that makes the filter/interaction work.
   */
  @Override
  public void setBundles(Bundles bundles) {
    synchronized (my_histos_al) {
      Iterator<RTHistoPanel> it = my_histos_al.iterator(); while (it.hasNext()) {
        RTHistoPanel rt_histo_panel = it.next();
        rt_histo_panel.getRTComponent().render();
      }
    }
  }

  /**
   *
   */
  @Override
  public void highlight(Set<Bundle> set, Set<Bundle> set_p, Set<Bundle> set_pp) {
    synchronized (my_histos_al) {
      Iterator<RTHistoPanel> it = my_histos_al.iterator(); while (it.hasNext()) {
        RTHistoPanel rt_histo_panel = it.next();
        rt_histo_panel.getRTComponent().highlight(set, set_p, set_pp);
      }
    }
  }

  /**
   * List of the currently displayed panels -- needs to be synchronized on mods / iterations
   */
  List<RTHistoPanel> my_histos_al = new ArrayList<RTHistoPanel>();

  /**
   * Set capturing the last set of fields displayed.  Useful to determine
   * which new fields are added so that the user can construct the correct
   * ordering for the histograms...
   */
  Set<String> last_set   = new HashSet<String>(); 

  /**
   * Last string array of selected fields.  Useful to determine which
   * fields are new so that the user can control the ordering.
   */
  String          last_sel[] = new String[0];

  /**
   * Return the selection of fields for the view.  The method is complicated by
   * the fact that it needs to return the list as the user adds each field.  Not
   * in the order of the JList values.
   *
   *@return ordered list of histogram fields to display
   */
  public synchronized String[] getSelection() {
    // What does the gui show?
    java.util.List<String> list = Utils.jListGetValuesWrapper(fields_ls);  Set<String> set = new HashSet<String>();
    String sel[] = new String[list.size()]; for (int i=0;i<sel.length;i++) { sel[i] = list.get(i); set.add(sel[i]); }
    // Let's add from the last selection to figure out what to show
    String strs[] = new String[sel.length]; int strs_i = 0;
    for (int i=0;i<last_sel.length;i++) {
      if (set.contains(last_sel[i]) == true  && strs_i < strs.length) strs[strs_i++] = last_sel[i];
    }
    for (int i=0;i<sel.length;i++) {
      if (last_set.contains(sel[i]) == false && strs_i < strs.length) strs[strs_i++] = sel[i];
    }
    if (strs_i != strs.length) {
      System.err.println("RTHistoListPanel - error calculating the new selection");
      strs = sel;
    }
    // Save it for next time
    last_set = set; last_sel = strs;
    // Return it
    return strs;
  }

  /**
   * Return a string representing the configuration of this component.  Used for
   * bookmarking views to more easily recall them.
   *
   *@return string representing view configuration
   */
  @Override
  public String       getConfig    ()           { 
    return "RTHistoListPanel" + BundlesDT.DELIM + 
           "lastset="         + commaDelimited(last_sel);
  }

  /**
   * Encode an array of strings into a comma-delimited, url-encoded string.  Used for the getConfig() routine.
   */
  private String commaDelimited(String strs[]) {
    StringBuffer sb = new StringBuffer();
    if (strs.length > 0) {
      sb.append(Utils.encToURL(strs[0]));
      for (int i=1;i<strs.length;i++) sb.append("," + Utils.encToURL(strs[i]));
    }
    return sb.toString();
  }

  /**
   * Decode a comma-delimited, url-encoded string into an array of strings.  Used for the setConfig() routine.
   */
  private String[] commaDelimited(String str) {
    StringTokenizer st = new StringTokenizer(str,",");
    String strs[] = new String[st.countTokens()];
    for (int i=0;i<strs.length;i++) strs[i] = Utils.decFmURL(st.nextToken());
    return strs;
  }

  /**
   * Adjust the configuration of this component based on the specified configuration
   * string.
   *
   *@param str configuration string
   */
  @Override
  public void         setConfig    (String str) { 
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTHistoListPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTHistoListPanel");

    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      if (type.equals("lastset")) {
        String strs[]  = commaDelimited(value);
        List<Integer> indexes = new ArrayList<Integer>(); ListModel<String> lm = fields_ls.getModel();
        for (int i=0;i<strs.length;i++) {
          for (int j=0;j<lm.getSize();j++) if (strs[i].equals("" + lm.getElementAt(j))) indexes.add(j);
        }
        int index_array[] = new int[indexes.size()]; for (int i=0;i<index_array.length;i++) index_array[i] = indexes.get(i);
        last_sel = strs; last_set = new HashSet<String>(); for (int i=0;i<last_sel.length;i++) last_set.add(last_sel[i]);
        fields_ls.setSelectedIndices(index_array);
      } else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Update the list of possible axis because new fields were
   * added to the application.  In this case, we need to keep the previously
   * selected elements selected and in the correct order.
   */
  public void updateBys() {
    // Object sels[] = fields_ls.getSelectedValues();
    String strs[] = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, true, true, true);
    List<String> remove_multis = new ArrayList<String>();
    for (int i=0;i<strs.length;i++) {
      if (strs[i].endsWith(BundlesDT.MULTI) || (strs[i].indexOf(BundlesDT.MULTI + BundlesDT.DELIM) >= 0)) {
      } else remove_multis.add(strs[i]);
    }
    String as_array[] = new String[remove_multis.size()]; remove_multis.toArray(as_array);
    fields_ls.setListData(as_array);
  }
}
