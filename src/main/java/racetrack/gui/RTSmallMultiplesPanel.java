/* 

Copyright 2022 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Composite;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.RenderingHints;
import java.awt.Stroke;

import java.awt.event.MouseEvent;

import java.awt.geom.Arc2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.io.IOException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesG;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.Utils;

import racetrack.visualization.AbridgedSpectra;
import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.BrightGrayColorScale;
import racetrack.visualization.ColorScale;
import racetrack.visualization.MadeHexMaps;
import racetrack.visualization.RTColorManager;
import racetrack.visualization.WorldMap;

/**
 * Class that implements small multiples for select types of visualizations.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTSmallMultiplesPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -2231127917649113349L;

  /**
   * Show the small multiple for the visible version of the data
   */
  JCheckBoxMenuItem show_visible_cbmi,

  /**
   * Label the small multiples
   */
                    label_cbmi,

  /**
   * Grid view if two categories are selected
   */
                    grid_view_cbmi,

  /**
   * Vary the color of the bars
   */
                    color_cbmi,

  /**
   * Larger dots in the xy plot
   */
                    larger_dots_cbmi,
  /**
   * Shrinkwrap the rows in grid mode so that they are as close to the left as possible
   */
                    shrinkwrap_cbmi;

  /**
   * Get the show visible setting.
   *@return true to show visible
   */
  public boolean showVisible()                  { return show_visible_cbmi.isSelected(); }
  /**
   * Set the show visible setting.
   *@param b true to show visible
   */
  public void    showVisible(boolean b)         { show_visible_cbmi.setSelected(b); }

  /**
   * Get the label small multiples setting.
   *@return true to label small multiples
   */
  public boolean labelSmallMultiples()          { return label_cbmi.isSelected(); }
  /**
   * Set the label small multiples setting.
   *@param b true to label small multiples
   */
  public void    labelSmallMultiples(boolean b) { label_cbmi.setSelected(b); }

  /**
   * Return if a grid view should be used for two categories.
   *
   *@return true for grid view
   */
  public boolean gridView() { return grid_view_cbmi.isSelected(); }

  /**
   * Set the grid view option.
   *
   *@param b true to enable grid view
   */
  public void gridView(boolean b) { grid_view_cbmi.setSelected(b); }

  /**
   * Return whether to vary the color of the bars
   *@return true to vary color
   */
  public boolean varyColor()          { return color_cbmi.isSelected(); }
  /**
   * Set the vary color setting
   *@param b true to vary color
   */
  public void    varyColor(boolean b) { color_cbmi.setSelected(b); }

  /**
   * Return true for larger dots in the xy plots.
   * 
   *@return true for larger dots
   */
  public boolean largerDots() { return larger_dots_cbmi.isSelected(); }

  /**
   * Set the larger dots option in the gui.
   *
   *@param b true for larger dots
   */
  public void largerDots(boolean b) { larger_dots_cbmi.setSelected(b); }

  /**
   * Shrinkwrap the rows so that they are as close together as possible.
   * 
   * @return true to shrinkwrap the rows
   */
  public boolean shrinkWrap() { return shrinkwrap_cbmi.isSelected(); }

  /**
   *  Set the shrinkwrap flag.
   * 
   * @param b true to shrinkwrap
   */
  public void shrinkWrap(boolean b) { shrinkwrap_cbmi.setSelected(b); }

  /**
   * Bar chart mode for the small multiples
   */
  JRadioButtonMenuItem barchart_mode_rbmi,

  /**
   * XY Scatter mode for the small multiples
   */
                       xyscatter_mode_rbmi,

  /**
   * Pie chart mode for the small multiples
   */
                       piechart_mode_rbmi,

  /**
   * Geo mode for the small multiples
   */
                       geo_mode_rbmi,

  /**
   * Country level mode for small multiples
   */
                       country_mode_rbmi;

  /**
   * Return the mode of the visualization.
   *@return mode string
   */
  public String mode() {
    if      (barchart_mode_rbmi.             isSelected()) return BARCHART_MODE_STR;
    else if (xyscatter_mode_rbmi.            isSelected()) return XYSCATTER_MODE_STR;
    else if (piechart_mode_rbmi.             isSelected()) return PIECHART_MODE_STR;
    else if (geo_mode_rbmi.                  isSelected()) return GEO_MODE_STR;
    else if (country_mode_rbmi.              isSelected()) return COUNTRY_MODE_STR;
    else                                                   return BARCHART_MODE_STR;
  }

  /**
   * Set the mode of the application.
   *@param str mode string
   */
  public void mode(String str) {
    if      (str.equals(BARCHART_MODE_STR))              barchart_mode_rbmi.             setSelected(true);
    else if (str.equals(XYSCATTER_MODE_STR))             xyscatter_mode_rbmi.            setSelected(true);
    else if (str.equals(PIECHART_MODE_STR))              piechart_mode_rbmi.             setSelected(true);
    else if (str.equals(GEO_MODE_STR))                   geo_mode_rbmi.                  setSelected(true);
    else if (str.equals(COUNTRY_MODE_STR))               country_mode_rbmi.              setSelected(true);
  }

  /**
   * String for the barchart mode
   */
  final static String BARCHART_MODE_STR = "Bar Chart",

  /**
   * String for the xyscatter mode
   */
                      XYSCATTER_MODE_STR = "XY Scatter",
  /**
   * String for the pie chart mode
   */
                      PIECHART_MODE_STR = "Pie Chart",

  /**
   * String for the geo mode
   */
                      GEO_MODE_STR      = "GPS",

  /**
   * String for the country mode
   */
                      COUNTRY_MODE_STR  = "Country";

  /**
   * X axis for each xy plot is independent
   */
  final static String X_AXIS_INDEPENDENT_STR = "X Axis Independent (xy)",

  /**
   * Y axis for each xy plot is independent
   */
                      Y_AXIS_INDEPENDENT_STR = "Y Axis Independent (xy,bar,pie,geo)",

  /**
   * Suffix to make a small small multiple
   */
                      SMALL_SUFFIX_STR  = " Small",

  /**
   * Suffix to make a medium small multiple
   */
                      MEDIUM_SUFFIX_STR = " Medium",

  /**
   * Suffix to make a large small multiple
   */
                      LARGE_SUFFIX_STR  = " Large";

  /**
   * Checkbox menu item for independent x axis
   */
  JCheckBoxMenuItem x_axis_independent_cbmi,

  /**
   * Checkbox menu item for independent y axis
   */
                    y_axis_independent_cbmi;

  /**
   * Return true if the x axis should be independent for each xy plot
   *
   *@return true for independent x axis
   */
  public boolean independentXAxis()          { return x_axis_independent_cbmi.isSelected(); }

  /**
   * Set the value for an independent x axis.
   *
   *@param b true for independent x axis
   */
  public void    independentXAxis(boolean b) {        x_axis_independent_cbmi.setSelected(b); }

  /**
   * Return true if the y axis should be independent for each xy plot
   *
   *@return true for independent y axis
   */
  public boolean independentYAxis()          { return y_axis_independent_cbmi.isSelected(); }

  /**
   * Set the value for an independent y axis.
   *
   *@param b true for independent y axis
   */
  public void    independentYAxis(boolean b) {        y_axis_independent_cbmi.setSelected(b); }

  /**
   * Card to switch between the different mode options
   */
  JPanel               cards_panel;

  /**
   * Bar Chart x axis combobox
   */
  JComboBox<String>    barchart_x_axis_cb;

  /**
   * Return the barchart x axis variable.
   *@return barchart x axis variable
   */
  public String barChartXAxis()           { return (String) barchart_x_axis_cb.getSelectedItem(); }
  /**
   * Set the barchart x axis variable.
   *@param str barchart x axis variable
   */
  public void   barChartXAxis(String str) { barchart_x_axis_cb.setSelectedItem(str); }

  /**
   * No Gap Options for Bar Charts
   */
  JCheckBox barchart_no_gaps_cb;

  /**
   * Return the no gaps option.
   *
   *@return true to have no gaps
   */
  public boolean barChartNoGaps() { return barchart_no_gaps_cb.isSelected(); }

  /**
   * Set the no gaps option for bar charts.
   *
   *@param b true to remove gaps
   */
  public void barChartNoGaps(boolean b) { barchart_no_gaps_cb.setSelected(b); }

  /**
   * XY X Axis combobox
   */
  JComboBox<String>    xy_x_axis_cb,

  /**
   * XY Y Axis combobox
   */
                       xy_y_axis_cb;

  /**
   * Return the xy scatter x axis setting
   *@return xy x axis
   */
  public String xyScatterXAxis() { return (String) xy_x_axis_cb.getSelectedItem(); }
  /**
   * Set the xy scatter x axis setting
   *@param str xy x axis
   */
  public void xyScatterXAxis(String str) { xy_x_axis_cb.setSelectedItem(str); }

  /**
   * Return the xy scatter y axis setting
   *@return xy y axis
   */
  public String xyScatterYAxis() { return (String) xy_y_axis_cb.getSelectedItem(); }
  /**
   * Set the xy scatter y axis setting
   *@param str xy y axis
   */
  public void xyScatterYAxis(String str) { xy_y_axis_cb.setSelectedItem(str); }

  /**
   * XY X Axis Scale
   */
  JComboBox<String>    xy_x_scale_cb,

  /**
   * XY Y Axis Scale
   */
                       xy_y_scale_cb;


  /**
   * Return the xy scatter x scale setting
   *@return xy x scale
   */
  public String xyScatterXScale() { return (String) xy_x_scale_cb.getSelectedItem(); }
  /**
   * Set the xy scatter x scale setting
   *@param str xy x scale
   */
  public void xyScatterXScale(String str) { xy_x_scale_cb.setSelectedItem(str); }

  /**
   * Return the xy scatter y scale setting
   *@return xy y scale 
   */
  public String xyScatterYScale() { return (String) xy_y_scale_cb.getSelectedItem(); }
  /**
   * Set the xy scatter y scale setting
   *@param str xy y scale 
   */
  public void xyScatterYScale(String str) { xy_y_scale_cb.setSelectedItem(str); }

  /**
   * XY Linear Scaling
   */
  final static String XY_SCALE_LINEAR_STR = "Linear",

  /**
   * XY Equal Scaling
   */
                      XY_SCALE_EQUAL_STR  = "Equal";

  /**
   * Array for holding the scaling strings
   */
  String xy_scale_strs[] = { XY_SCALE_LINEAR_STR, XY_SCALE_EQUAL_STR };

  /**
   * Latitude combobox for the geo small multiple
   */
  JComboBox<String> lat_cb,

  /**
   * Longitude combobox for the geo small multiple
   */
                    lon_cb;

  /**
   * Return the field to use for the latitude of the visualization.
   */
  public String latitude() { return (String) lat_cb.getSelectedItem(); }

  /**
   * Set the latitude field of the visualization
   */
  public void   latitude(String s) { lat_cb.setSelectedItem(s); }

  /**
   * Return the field to use for the longitude of the visualization.
   */
  public String longitude() { return (String) lon_cb.getSelectedItem(); }

  /**
   * Set the longitude field of the visualization
   */
  public void   longitude(String s) { lon_cb.setSelectedItem(s); }

  /**
   * Country combobox for the country-level geo
   */
  JComboBox<String> country_cb;

  /**
   * Color the countries by a special colorscale...
   */
  JComboBox<String> country_magnitude_cb;

  /**
   * Return the field to use for the country code string.
   */
  public String countryField() { return (String) country_cb.getSelectedItem(); }

  /**
   * Set the field to use for the country code string.
   */
  public void countryField(String country_field) { country_cb.setSelectedItem(country_field); }

  /**
   * Return the color scale for country coloring.
   */
  public String countryMagnitudeColorScale() { return (String) country_magnitude_cb.getSelectedItem(); }

  /**
   * Set the color scale for country coloring.
   */
  public void countryMagnitudeColorScale(String country_magnitude) { country_magnitude_cb.setSelectedItem(country_magnitude); }

  /**
   * Radio buttons for the prefered way to sort the multiples
   */
  JRadioButtonMenuItem sort_rbmis[],

  /**
   * Secondary sort options for grid mode
   */
                       sec_sort_rbmis[];

  /**
   * Return the sort order for the small multiples.
   *@return string representing the sort order
   */
  public String sortSmallMultiples() {
    for (int i=0;i<sort_rbmis.length;i++) if (sort_rbmis[i].isSelected()) return sort_strs[i];
    return sort_strs[0];
  }
  /**
   * Set the sort order for the small multiples.
   *@param str sort order setting
   */
  public void sortSmallMultiples(String str) {
    for (int i=0;i<sort_strs.length;i++) if (sort_strs[i].equals(str)) sort_rbmis[i].setSelected(true);
  }

  /**
   * Return the sort order for the small multiples (secondary).
   *@return string representing the sort order
   */
  public String secSortSmallMultiples() {
    for (int i=0;i<sec_sort_rbmis.length;i++) if (sec_sort_rbmis[i].isSelected()) return sort_strs[i];
    return sort_strs[0];
  }
  /**
   * Set the sort order for the small multiples (secondary).
   *@param str sort order setting
   */
  public void secSortSmallMultiples(String str) {
    for (int i=0;i<sort_strs.length;i++) if (sort_strs[i].equals(str)) sec_sort_rbmis[i].setSelected(true);
  }

  /**
   * Sort options for the small multiples... will determine which multiples don't get shown if
   * there's not enough screen realestate.
   */
  final static String SORT_RECORDS = "Sort by Records",
                      SORT_COUNT   = "Sort by Count",
                      SORT_ALPHA   = "Sort Alphabetically";

  /**
   * Array for sort strings
   */
  final static String sort_strs[] = {
    SORT_RECORDS,
    SORT_COUNT,
    SORT_ALPHA
  };

  /**
   * Category to run the small multiples across
   */
  JComboBox<String> category_cb,

  /**
   * Second category (optional)
   */
                    category2_cb;

  /**
   * Return the categorization of the small multiples views.
   *@return small multiples categories
   */
  public String category() { return (String) category_cb.getSelectedItem(); }

  /**
   * Set the categorization for the small multiples views.
   *@param str small multiples category
   */
  public void category(String str) { category_cb.setSelectedItem(str); }

  /**
   * Return the second (optional) categorization of the small multiples views.
   *@return small multiples category two
   */
  public String category2() { return (String) category2_cb.getSelectedItem(); }

  /**
   * Set the categorization for the small multiples views.
   *@param str small multiples category
   */
  public void category2(String str) { category2_cb.setSelectedItem(str); }

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTSmallMultiplesPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   

    // Make the GUI
    add("Center", component = new RTSmallMultiplesComponent());

    // Add the popup menu items
    getRTPopupMenu().add(show_visible_cbmi        = new JCheckBoxMenuItem("Show Visible",          true));  defaultListener(show_visible_cbmi);
    getRTPopupMenu().add(label_cbmi               = new JCheckBoxMenuItem("Label Small Multiples", true));  defaultListener(label_cbmi);
    getRTPopupMenu().add(grid_view_cbmi           = new JCheckBoxMenuItem("Grid For 2 Cats",       true));  defaultListener(grid_view_cbmi);
    getRTPopupMenu().add(shrinkwrap_cbmi          = new JCheckBoxMenuItem("Shrinkwrap Rows",       false)); defaultListener(shrinkwrap_cbmi);

    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(color_cbmi               = new JCheckBoxMenuItem("Vary Color",            true));  defaultListener(color_cbmi);
    getRTPopupMenu().add(larger_dots_cbmi         = new JCheckBoxMenuItem("Larger Dots (xy)",      false)); defaultListener(larger_dots_cbmi);
    getRTPopupMenu().addSeparator();

    // Mode for the small multiples
    ButtonGroup bg = new ButtonGroup();

    getRTPopupMenu().add(barchart_mode_rbmi              = new JRadioButtonMenuItem(BARCHART_MODE_STR,              true));  
      bg.add(barchart_mode_rbmi);  defaultListener(barchart_mode_rbmi);

    getRTPopupMenu().add(xyscatter_mode_rbmi             = new JRadioButtonMenuItem(XYSCATTER_MODE_STR,             false)); 
      bg.add(xyscatter_mode_rbmi); defaultListener(xyscatter_mode_rbmi);

    getRTPopupMenu().add(piechart_mode_rbmi              = new JRadioButtonMenuItem(PIECHART_MODE_STR,              false)); 
      bg.add(piechart_mode_rbmi); defaultListener(piechart_mode_rbmi);

    getRTPopupMenu().add(geo_mode_rbmi                   = new JRadioButtonMenuItem(GEO_MODE_STR,                   false)); 
      bg.add(geo_mode_rbmi); defaultListener(geo_mode_rbmi);

    getRTPopupMenu().add(country_mode_rbmi               = new JRadioButtonMenuItem(COUNTRY_MODE_STR,               false)); 
      bg.add(country_mode_rbmi); defaultListener(country_mode_rbmi);

    getRTPopupMenu().addSeparator();

    // Indepdent axes
    getRTPopupMenu().add(x_axis_independent_cbmi = new JCheckBoxMenuItem(X_AXIS_INDEPENDENT_STR, true)); defaultListener(x_axis_independent_cbmi);
    getRTPopupMenu().add(y_axis_independent_cbmi = new JCheckBoxMenuItem(Y_AXIS_INDEPENDENT_STR, true)); defaultListener(y_axis_independent_cbmi);

    getRTPopupMenu().addSeparator();

    // Sorting options
    bg = new ButtonGroup(); sort_rbmis = new JRadioButtonMenuItem[sort_strs.length];
    for (int i=0;i<sort_strs.length;i++) {
      getRTPopupMenu().add(sort_rbmis[i] = new JRadioButtonMenuItem(sort_strs[i], i == 0));
      bg.add(sort_rbmis[i]);
      defaultListener(sort_rbmis[i]);
    }
    
    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(new JLabel("Secondary Sort"));

    bg = new ButtonGroup(); sec_sort_rbmis = new JRadioButtonMenuItem[sort_strs.length];
    for (int i=0;i<sort_strs.length;i++) {
      getRTPopupMenu().add(sec_sort_rbmis[i] = new JRadioButtonMenuItem(sort_strs[i], i == 0));
      bg.add(sec_sort_rbmis[i]);
      defaultListener(sec_sort_rbmis[i]);
    }

    // Make the panels
    JPanel all_panel = new JPanel(new BorderLayout());

    JPanel panel    = new JPanel(new FlowLayout());
    panel.add(new JLabel("Cats"));
    panel.add(category_cb  = new JComboBox<String>()); defaultListener(category_cb);
    panel.add(category2_cb = new JComboBox<String>()); defaultListener(category2_cb);
    all_panel.add("West", panel);

    cards_panel = new JPanel(new CardLayout());

    // boxplot card
    panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("X Axis"));
    panel.add(barchart_x_axis_cb = new JComboBox<String>());
    panel.add(barchart_no_gaps_cb = new JCheckBox("No Gaps", false));
    defaultListener(barchart_x_axis_cb);
    defaultListener(barchart_no_gaps_cb);
    cards_panel.add(panel, BARCHART_MODE_STR);

    // xyscatter card
    panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("X"));
    panel.add(xy_x_axis_cb = new JComboBox<String>());                defaultListener(xy_x_axis_cb);
    panel.add(xy_x_scale_cb = new JComboBox<String>(xy_scale_strs));  defaultListener(xy_x_scale_cb);
    panel.add(new JLabel("Y"));
    panel.add(xy_y_axis_cb = new JComboBox<String>());                defaultListener(xy_y_axis_cb);
    panel.add(xy_y_scale_cb = new JComboBox<String>(xy_scale_strs));  defaultListener(xy_y_scale_cb);
    cards_panel.add(panel, XYSCATTER_MODE_STR);
    
    // piechart card
    panel = new JPanel(new FlowLayout());
    cards_panel.add(panel, PIECHART_MODE_STR);
    
    // geo card
    panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("Lat"));
    panel.add(lat_cb = new JComboBox<String>()); defaultListener(lat_cb);
    panel.add(new JLabel("Lon"));
    panel.add(lon_cb = new JComboBox<String>()); defaultListener(lon_cb);
    cards_panel.add(panel, GEO_MODE_STR);

    // country card
    panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("Country Field"));
    panel.add(country_cb = new JComboBox<String>()); 
      defaultListener(country_cb);
    panel.add(new JLabel("Color Scale"));
    panel.add(country_magnitude_cb = new JComboBox<String>(country_colorscale_strs)); defaultListener(country_cb);
      defaultListener(country_magnitude_cb);
    cards_panel.add(panel, COUNTRY_MODE_STR);

    // Add the card panel to the bottom panel
    all_panel.add("Center", cards_panel);
    add("South", all_panel);

    // Make the listener to switch panels
    barchart_mode_rbmi.             addChangeListener(new SwitchCards());
    xyscatter_mode_rbmi.            addChangeListener(new SwitchCards());
    piechart_mode_rbmi.             addChangeListener(new SwitchCards());
    geo_mode_rbmi.                  addChangeListener(new SwitchCards());
    country_mode_rbmi.              addChangeListener(new SwitchCards());

    // Update the items
    updateBys();
  }

  /**
   * Default colorscale for country coloring ... use the application defaults
   */
  String DEFAULT_COLORSCALE_STR  = "Default", 

  /**
   * Gray magnitude colorscale for country coloring
   */
         GRAY_COLORSCALE_STR     = "Gray", 

  /**
   * Brewer magnitude colorscale for country coloring
   */
         BREWER_COLORSCALE_STR   = "Brewer", 

  /**
   * Spectra magnitude colorscale for country coloring
   */
         SPECTRA_COLORSCALE_STR  = "Spectra";

  /**
   * Color scale for the country coloring... for magnitude coloring
   */
  String country_colorscale_strs[] = { DEFAULT_COLORSCALE_STR, GRAY_COLORSCALE_STR, BREWER_COLORSCALE_STR, SPECTRA_COLORSCALE_STR };

  /**
   *
   */
  class SwitchCards implements ChangeListener {
    public void stateChanged(ChangeEvent ce) {
      if      (barchart_mode_rbmi.             isSelected()) ((CardLayout) cards_panel.getLayout()).show(cards_panel, BARCHART_MODE_STR);
      else if (xyscatter_mode_rbmi.            isSelected()) ((CardLayout) cards_panel.getLayout()).show(cards_panel, XYSCATTER_MODE_STR);
      else if (piechart_mode_rbmi.             isSelected()) ((CardLayout) cards_panel.getLayout()).show(cards_panel, PIECHART_MODE_STR);
      else if (geo_mode_rbmi.                  isSelected()) ((CardLayout) cards_panel.getLayout()).show(cards_panel, GEO_MODE_STR);
      else if (country_mode_rbmi.              isSelected()) ((CardLayout) cards_panel.getLayout()).show(cards_panel, COUNTRY_MODE_STR);
    }
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "smallmultiples"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
      return "RTSmallMultiplesPanel"                                         + BundlesDT.DELIM +
              "category="          + Utils.encToURL(category             ()) + BundlesDT.DELIM +
              "category2="         + Utils.encToURL(category2            ()) + BundlesDT.DELIM +
              "showvisible="       +                showVisible          ()  + BundlesDT.DELIM +
              "label="             +                labelSmallMultiples  ()  + BundlesDT.DELIM +
              "gridview="          +                gridView             ()  + BundlesDT.DELIM +
              "vcolor="            +                varyColor            ()  + BundlesDT.DELIM +
              "largerdots="        +                largerDots           ()  + BundlesDT.DELIM +
              "shrinkwrap="        +                shrinkWrap           ()  + BundlesDT.DELIM +
              "xaxisind="          +                independentXAxis     ()  + BundlesDT.DELIM +
              "yaxisind="          +                independentYAxis     ()  + BundlesDT.DELIM +
              "latitude="          + Utils.encToURL(latitude             ()) + BundlesDT.DELIM +
              "longitude="         + Utils.encToURL(longitude            ()) + BundlesDT.DELIM +
              "mode="              + Utils.encToURL(mode                 ()) + BundlesDT.DELIM +
              "barchartxaxis="     + Utils.encToURL(barChartXAxis        ())  + BundlesDT.DELIM +
              "barchartnogaps="    +                barChartNoGaps       ()  + BundlesDT.DELIM +
              "xyscatterxaxis="    + Utils.encToURL(xyScatterXAxis       ()) + BundlesDT.DELIM +
              "xyscatterxscale="   + Utils.encToURL(xyScatterXScale      ()) + BundlesDT.DELIM +
              "xyscatteryaxis="    + Utils.encToURL(xyScatterYAxis       ()) + BundlesDT.DELIM +
              "xyscatteryscale="   + Utils.encToURL(xyScatterYScale      ()) + BundlesDT.DELIM +
              "countryfield="      + Utils.encToURL(countryField         ()) + BundlesDT.DELIM +
              "countrycolorscale=" + Utils.encToURL(countryMagnitudeColorScale()) + BundlesDT.DELIM +
              "sort="              + Utils.encToURL(sortSmallMultiples   ()) + BundlesDT.DELIM +
              "sort2="             + Utils.encToURL(secSortSmallMultiples());
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTSmallMultiplesPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTSmallMultiplesPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      if      (type.equals("category"))          category             (Utils.decFmURL(value));
      else if (type.equals("category2"))         category2            (Utils.decFmURL(value));
      else if (type.equals("showvisible"))       showVisible          (value.toLowerCase().equals("true"));
      else if (type.equals("label"))             labelSmallMultiples  (value.toLowerCase().equals("true"));
      else if (type.equals("gridview"))          gridView             (value.toLowerCase().equals("true"));
      else if (type.equals("vcolor"))            varyColor            (value.toLowerCase().equals("true"));
      else if (type.equals("largerdots"))        largerDots           (value.toLowerCase().equals("true"));
      else if (type.equals("shrinkwrap"))        shrinkWrap           (value.toLowerCase().equals("true"));
      else if (type.equals("xaxisind"))          independentXAxis     (value.toLowerCase().equals("true"));
      else if (type.equals("yaxisind"))          independentYAxis     (value.toLowerCase().equals("true"));
      else if (type.equals("latitude"))          latitude             (Utils.decFmURL(value));
      else if (type.equals("longitude"))         longitude            (Utils.decFmURL(value));
      else if (type.equals("mode"))              mode                 (Utils.decFmURL(value));
      else if (type.equals("barchartxaxis"))     barChartXAxis        (Utils.decFmURL(value));
      else if (type.equals("barchartnogaps"))    barChartNoGaps       (value.toLowerCase().equals("true"));
      else if (type.equals("xyscatterxaxis"))    xyScatterXAxis       (Utils.decFmURL(value));
      else if (type.equals("xyscatterxscale"))   xyScatterXScale      (Utils.decFmURL(value));
      else if (type.equals("xyscatteryaxis"))    xyScatterYAxis       (Utils.decFmURL(value));
      else if (type.equals("xyscatteryscale"))   xyScatterYScale      (Utils.decFmURL(value));
      else if (type.equals("countryfield"))      countryField         (Utils.decFmURL(value));
      else if (type.equals("countrycolorscale")) countryMagnitudeColorScale(Utils.decFmURL(value));
      else if (type.equals("sort"))              sortSmallMultiples   (Utils.decFmURL(value));
      else if (type.equals("sort2"))             secSortSmallMultiples(Utils.decFmURL(value));
      else    throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }


  /**
   * Update a combobox
   */
  protected void updateBys(JComboBox<String> cb, String strs[]) {
    String sel = (String) cb.getSelectedItem();
    cb.removeAllItems();
    for (int i=0;i<strs.length;i++) cb.addItem(strs[i]);
    if (sel != null) cb.setSelectedItem(sel); else if (strs.length > 0) cb.setSelectedIndex(0);
  }

  /**
   * None String -- used for the second category
   */
  final static String NONE_STR = "|None|";

  /**
   * Update the cateogization drop down
   */
  public void updateBys() {
    BundlesG globals = getRTParent().getRootBundles().getGlobals();

    // Category CB First
    String strs[] = KeyMaker.blanks(globals, false, true, false, false); List<String> list = new ArrayList<String>();
    for (int i=0;i<strs.length;i++) {
      // Ignore certain key makers -- 2020-08-10 - this removed the tag multiplexer... which is still useful
      // if (strs[i].equals(KeyMaker.TABLET_SEP_STR) || strs[i].equals(KeyMaker.ALL_ENTITIES_STR) || strs[i].indexOf(BundlesDT.MULTI) >= 0) {
      // } else list.add(strs[i]);
      
      // Ignore certain key makers
      if (strs[i].equals(KeyMaker.TABLET_SEP_STR) || strs[i].equals(KeyMaker.ALL_ENTITIES_STR)) {
      } else list.add(strs[i]);
    }
    // Add some other back in
    list.add(KeyMaker.BY_YEAR_STR);
    list.add(KeyMaker.BY_YEAR_MONTH_STR);
    list.add(KeyMaker.BY_MONTH_STR);
    list.add(KeyMaker.BY_DAYOFWEEK_STR);
    list.add(KeyMaker.BY_HOUR_STR);

    strs = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i);
    updateBys(category_cb, strs);

    list.add(0, NONE_STR);
    strs = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i);
    updateBys(category2_cb, strs);

    // Bar Chart -- Need to re-examine the totalBars method if additional options are added...
    strs = new String[12];
    strs[0]  = KeyMaker.BY_STRAIGHT_STR + SMALL_SUFFIX_STR;
    strs[1]  = KeyMaker.BY_STRAIGHT_STR + MEDIUM_SUFFIX_STR;
    strs[2]  = KeyMaker.BY_STRAIGHT_STR + LARGE_SUFFIX_STR;
    strs[3]  = KeyMaker.BY_YEAR_STR;
    strs[4]  = KeyMaker.BY_YEAR_QUARTER_STR;
    strs[5]  = KeyMaker.BY_YEAR_MONTH_STR;
    strs[6]  = KeyMaker.BY_YEAR_MONTH_DAY_STR;
    strs[7]  = KeyMaker.BY_MONTH_STR;
    strs[8]  = KeyMaker.BY_MONTH_DAY_STR;
    strs[9]  = KeyMaker.BY_DAYOFWEEK_STR;
    strs[10] = KeyMaker.BY_DAYOFWEEK_HOUR_STR;
    strs[11] = KeyMaker.BY_HOUR_STR;
    updateBys(barchart_x_axis_cb, strs);

    // XY Scales
    updateBys(xy_x_axis_cb, KeyMaker.blanks(globals, false, true, true, true));
    updateBys(xy_y_axis_cb, KeyMaker.blanks(globals, false, true, true, true));
    
    // Latitude and longitude
    // -- only pick fields that are of type float
    String        fields[] = KeyMaker.blanks(globals, false, true, true, true);
    List<String>  matching = new ArrayList<String>();
    for (int i=0;i<fields.length;i++) {
      int               fld_i = globals.fieldIndex(fields[i]);
      Set<BundlesDT.DT> dts   = globals.getFieldDataTypes(fld_i);
      if (dts != null) {
        if ( (dts.size() == 1 && dts.contains(BundlesDT.DT.FLOAT)) || (dts.size() == 2 && dts.contains(BundlesDT.DT.FLOAT) && dts.contains(BundlesDT.DT.NOTSET)) ) 
          matching.add(fields[i]);
      }
    }
    fields = new String[matching.size()];
    Iterator<String> it = matching.iterator();
    for (int i=0;i<fields.length;i++) fields[i] = it.next();

    updateBys(lat_cb, fields);
    updateBys(lon_cb, fields);

    // Country modes
    fields = KeyMaker.blanks(globals, false, true, true, true);
    List<String> country_fields = new ArrayList<String>();
    for (int i=0;i<fields.length;i++) {
      if (Utils.countryCodeField(fields[i])) country_fields.add(fields[i]);
    }
    String country_fields_array[] = new String[country_fields.size()]; 
    it = country_fields.iterator(); for (int i=0;i<country_fields_array.length;i++) country_fields_array[i] = it.next();
    updateBys(country_cb, country_fields_array);
  }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTSmallMultiplesComponent extends RTComponent {
    private static final long serialVersionUID = 322413271121788861L;
    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>();    RTRenderContext myrc = rc;  if (myrc == null) return set; else return ((SMRC) myrc).allShapes(); }
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RTRenderContext myrc = rc; if (myrc == null) return shapes; else return ((SMRC) myrc).shapes(bundles); }
    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>();  RTRenderContext myrc = rc; if (myrc == null) return set; else return ((SMRC) myrc).shapeBundles(shape); }
    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>();    RTRenderContext myrc = rc; if (myrc == null) return set; else return ((SMRC) myrc).overlappingShapes(shape); }
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> set = new HashSet<Shape>();    RTRenderContext myrc = rc; if (myrc == null) return set; else return ((SMRC) myrc).containingShapes(x,y); }

    /**
     *
     */
    @Override
    public void mouseClicked(MouseEvent me) {
      super.mouseClicked(me);

      RTRenderContext myrc = rc; if (myrc == null) return;

      if (me.getButton() == MouseEvent.BUTTON2 && 
          (myrc instanceof BarChartRenderContext || myrc instanceof PieChartRenderContext || myrc instanceof CountryRenderContext)) {

        // Find the category under the mouse
        String                skey = null;
        Rectangle2D           rect = null;
        int                   mx   = me.getX(), 
                              my   = me.getY();

        //
        // For Bar Chart small multiples...
        //
        if        (myrc instanceof BarChartRenderContext) {
          BarChartRenderContext bcrc = (BarChartRenderContext) myrc; Iterator<Rectangle2D> it = bcrc.geom_to_skey.keySet().iterator(); 
          while (skey == null && it.hasNext()) { rect = it.next(); if (rect.contains(mx,my)) skey = bcrc.geom_to_skey.get(rect); }

          // Set that category in the render context
          bcrc.cat_most_similar_to = skey;
          bcrc.base_bi             = null; // Will force just a render... not the counting..

        //
        // For Pie Chart small multiples...
        //
        } else if (myrc instanceof PieChartRenderContext) {
          PieChartRenderContext pcrc = (PieChartRenderContext) myrc; Iterator<Rectangle2D> it = pcrc.geom_to_skey.keySet().iterator(); 
          while (skey == null && it.hasNext()) { rect = it.next(); if (rect.contains(mx,my)) skey = pcrc.geom_to_skey.get(rect); }

          // Set that category in the render context
          pcrc.cat_most_similar_to = skey;
          pcrc.base_bi             = null; // Will force just a render... not the counting..

        //
        // For Country small multiples...
        //
        } else if (myrc instanceof CountryRenderContext) {
          CountryRenderContext crc = (CountryRenderContext) myrc; Iterator<Rectangle2D> it = crc.geom_to_skey.keySet().iterator(); 
          while (skey == null && it.hasNext()) { rect = it.next(); if (rect.contains(mx,my)) skey = crc.geom_to_skey.get(rect); }

          // Set that category in the render context
          crc.cat_most_similar_to = skey;
          crc.base_bi             = null; // Will force just a render... not the counting..
        }

        // And force another render
        repaint();
      }
    }

    /**
     * Override to show the entity selection
     */
    @Override
    public void paintComponent(Graphics g) {
      super.paintComponent(g); Graphics2D g2d = (Graphics2D) g;

      // Provide information about what's currently selected (in other windows)
      RTRenderContext myrc = rc; if (myrc != null) {
        Set<String> sel = getRTParent().getSelectedEntities(); // what's selected

        g2d.setColor(RTColorManager.getColor("linknode", "movenodes"));

        Iterator<String> it = sel.iterator(); while (it.hasNext()) {
          String entity = it.next();
          if (((SMRC) myrc).skey_to_geom.containsKey(entity)) { g2d.draw(((SMRC) myrc).skey_to_geom.get(entity)); }
          entity += "|";
          if (((SMRC) myrc).skey_to_geom.containsKey(entity)) { g2d.draw(((SMRC) myrc).skey_to_geom.get(entity)); }
        }
      }
    }

    /**
     * Add information interactively... specifically the categorization (since these
     * strings can get clobbered if they are too long)
     */
    @Override
    public void addGarnish(Graphics2D g2d, int mx, int my) {
      RTRenderContext myrc = rc; if (myrc == null) return;

      String                skey = null;
      BundlesCounterContext bcc  = null;
      Rectangle2D           rect = null;

      // Find the corresponding geometry, category information, and counter context
      if (myrc instanceof SMRC) {
        Iterator<Rectangle2D> it = ((SMRC) myrc).geom_to_skey.keySet().iterator();
        while (skey == null && it.hasNext()) { 
          rect = it.next();
          if (rect.contains(mx,my)) {
            skey = ((SMRC) myrc).geom_to_skey.get(rect); 
            bcc  = ((SMRC) myrc).cat_map.get(skey);
          }
        }
      }

      // Make sure we have valid info...
      if (skey != null && bcc != null) {
        String label; int io = skey.indexOf("|");
        if (io >= 0) {
          if      (io == 0)               label = Utils.decFmURL(skey.substring(1,skey.length()));
          else if (io == skey.length()-1) label = Utils.decFmURL(skey.substring(0,skey.length()-1));
          else                            label = Utils.decFmURL(skey.substring(0,io)) + " x " + Utils.decFmURL(skey.substring(io+1,skey.length()));
        } else label = Utils.decFmURL(skey);

        Stroke orig_stroke = g2d.getStroke();

        g2d.setColor(RTColorManager.getColor("annotate", "region"));
        g2d.setStroke(new BasicStroke(0.5f));
        g2d.draw(rect);

        int txt_h   = Utils.txtH(g2d, label);

        int label_x    = (int) (rect.getX() + rect.getWidth()/2),
            label_y    = (int) (rect.getY() + rect.getHeight() + 2 + 1*txt_h),
            info_y     = (int) (rect.getY() + rect.getHeight() + 2 + 2*txt_h);
        if (info_y >= getHeight()) { label_y = (int) (rect.getY() -  2*txt_h - 2);
                                     info_y  = (int) (rect.getY() -  1*txt_h - 2); }

        String info = "Max = " + bcc.totalMaximum() + " | Recs = " + bcc.totalRecords();

        clearStr(g2d, label, label_x - Utils.txtW(g2d, label)/2, label_y, RTColorManager.getColor("annotate", "labelfg"), RTColorManager.getColor("annotate", "labelbg"));
        clearStr(g2d, info,  label_x - Utils.txtW(g2d, info)/2,  info_y,  RTColorManager.getColor("annotate", "labelfg"), RTColorManager.getColor("annotate", "labelbg"));

        g2d.setStroke(orig_stroke);
      }
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy();

      // Create the render context based on the user's parameters
      RTRenderContext myrc = null; RTRenderContext last_rc = rc;

      if      (mode().equals(BARCHART_MODE_STR)) {
        myrc = new BarChartRenderContext(id, bs, count_by, color_by, 
                                         showVisible(), labelSmallMultiples(), gridView(), varyColor(), shrinkWrap(),
                                         category(), category2(), barChartNoGaps(),
                                         barChartXAxis(), independentYAxis(),
                                         sortSmallMultiples(), secSortSmallMultiples(),
                                         getWidth(), getHeight());

        // Keep the same similarity (if it's set...)
        if (last_rc != null && last_rc instanceof BarChartRenderContext && ((SMRC) last_rc).cat_most_similar_to != null)
          ((SMRC) myrc).cat_most_similar_to = ((SMRC) last_rc).cat_most_similar_to;

      } else if (mode().equals(XYSCATTER_MODE_STR)) {
        myrc = new XYScatterRenderContext(id, bs, count_by, color_by, 
                                          showVisible(), labelSmallMultiples(), gridView(), varyColor(), largerDots(),shrinkWrap(),
                                          category(), category2(),
                                          xyScatterXAxis(), xyScatterXScale(), independentXAxis(),
                                          xyScatterYAxis(), xyScatterYScale(), independentYAxis(),
                                          sortSmallMultiples(), secSortSmallMultiples(),
                                          getWidth(), getHeight());
      } else if (mode().equals(PIECHART_MODE_STR)) {
        myrc = new PieChartRenderContext(id, bs, count_by, color_by, 
                                         showVisible(), labelSmallMultiples(), gridView(), varyColor(), shrinkWrap(),
                                         category(), category2(),
                                         independentYAxis(),
                                         sortSmallMultiples(), secSortSmallMultiples(),
                                         getWidth(), getHeight());

        // Keep the same similarity (if it's set...)
        if (last_rc != null && last_rc instanceof PieChartRenderContext && ((SMRC) last_rc).cat_most_similar_to != null)
          ((SMRC) myrc).cat_most_similar_to = ((SMRC) last_rc).cat_most_similar_to;

      } else if (mode().equals(GEO_MODE_STR)) {
        myrc = new GeoRenderContext(id, bs, count_by, color_by, 
                                    showVisible(), labelSmallMultiples(), gridView(), varyColor(), largerDots(), shrinkWrap(),
                                    category(), category2(),
                                    latitude(), longitude(), independentYAxis(),
                                    sortSmallMultiples(), secSortSmallMultiples(),
                                    getWidth(), getHeight());
      } else if (mode().equals(COUNTRY_MODE_STR)) {
        myrc = new CountryRenderContext(id, bs, count_by, color_by,
                                        showVisible(), labelSmallMultiples(), gridView(), varyColor(), shrinkWrap(),
                                        category(), category2(),
                                        countryField(), countryMagnitudeColorScale(), independentYAxis(),
                                        sortSmallMultiples(), secSortSmallMultiples(),
                                        getWidth(),getHeight());
      }
      return myrc;
    }

    /**
     * Generic class for the methods and data in common across the render contexts.
     */
    public abstract class SMRC extends RTRenderContext {
      /**
       * Constructor... get the txt_h
       */
      public SMRC() {
        render_ts0 = System.currentTimeMillis();
        Graphics2D g2d = null; try { 
          g2d   = (Graphics2D) ((new BufferedImage(10,10,BufferedImage.TYPE_INT_RGB))).getGraphics(); 
          txt_h = Utils.txtH(g2d, "ABC");
        } finally { if (g2d != null) g2d.dispose(); }
      }

      /**
       * Start of the render time
       */
      long render_ts0;

      /**
       * Text height
       */
      protected int txt_h = 10;

      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Count specification for how a bundle contributes thee view
       */
      String  count_by, 

      /**
       * Color variable for the rendering
       */
              color_by,

      /**
       * Categorization of the small multiples
       */
              category,

      /**
       * Second categorization of the small multiples -- may be set to None...
       */
              category2;

      /**
       * Show the small multiple for the visible dataset
       */
      boolean show_visible;

      /**
       * Label each small multiple
       */
      boolean label_small_multiples;

      /**
       * Use a 2D grid for two category option
       */
      boolean grid_view;

      /**
       * Vary the color of the bars
       */
      boolean vary_color;

      /**
       *  Shrinkwrap the rows
       */
      boolean shrinkwrap;

      /**
       * The y-axis for each small multiple is independent -- i.e., it scales to its own max values (not the global max value)
       */
      boolean y_independent;

      /**
       * Category to a vector representation of the pie chart... used for similarity / distance metrics
       */
      Map<String,Map<String,Double>> cat_vec = new HashMap<String,Map<String,Double>>();

      /**
       * Counter context for each category
       */
      Map<String,BundlesCounterContext> cat_map = new HashMap<String,BundlesCounterContext>();

      /**
       * How to sort thte small multiples in the window
       */
      String  sort_small_multiples,

      /**
       * Secondary sorting for small multiples grid
       */
              sec_sort_small_multiples;

      /**
       * Counter context for the visible set
       */
      BundlesCounterContext visible_cc,

      /**
       * Counter context for category (for grid view only)
       */
                            cat_cc,

      /**
       * Counter context for category 2 (for grid view only)
       */
                            cat2_cc;

      /**
       * Small multiple width
       */
      protected int mult_w = -10,

      /**
       * Small multiple height
       */
                    mult_h = -10;

      /**
       * y insert for the small multiple
       */
      protected int mult_y_ins = 2;

      /**
       * x insert for the small multiple
       */
      protected int mult_x_ins = 2;

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      @Override
      public int           getRCWidth()  { return rc_w; }

      @Override
      public int           getRCHeight() { return rc_h; }

      /**
       * X space between multiples
       */
      protected final int x_spc_btwn_mults = 2,

      /**
       * Y space between multiples
       */
                          y_spc_btwn_mults = 2;

      /**
       * Txt space after the label (if the label shown)
       */
      final static int txt_space  = 4;

      /**
       * For similarity ordering, this is the category to compare all others against
       */
      String              cat_most_similar_to;

      /**
       * Base image rendering
       */
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (cat_cc != null) return getGridBase();    // Grid-based layout version
        else                return getNonGridBase(); // Non-grid-based layout version
      }

      /**
       *
       */
      protected abstract BufferedImage renderSmallMultiple(String cat_key, BundlesCounterContext bcc);

      /**
       *
       */
      protected abstract void calculateVectors();

      /**
       * Calculate the vector distance between two categories.
       */
      protected double vectorDistance(String cat_1, String cat_2) {
        if (cat_1 == null || cat_2 == null || cat_vec.containsKey(cat_1) == false || cat_vec.containsKey(cat_2) == false) 
          return Double.POSITIVE_INFINITY;

        Map<String,Double> m1 = cat_vec.get(cat_1),
                           m2 = cat_vec.get(cat_2);
        if (m1 != null && m2 != null) {
          Iterator<String> it;
         
          double      square_sum = 0.0;
          Set<String> found = new HashSet<String>();

          // Find all the keys in common... add to the root mean square
          it  = m1.keySet().iterator(); while (it.hasNext()) {
            String key = it.next(); found.add(key);
            if (m2.containsKey(key)) square_sum += ((m1.get(key) - m2.get(key)) * (m1.get(key) - m2.get(key)));
            else                     square_sum += m1.get(key) * m1.get(key);
          }

          // Add any remaining keys
          it  = m2.keySet().iterator(); while (it.hasNext()) { 
            String key = it.next(); found.add(key); 
            if (m1.containsKey(key) == false) square_sum += m2.get(key) * m2.get(key);
          }

          double rms = Math.sqrt(square_sum/found.size());

          if (Double.isNaN(rms)) return Double.POSITIVE_INFINITY;

          return rms;
        } else return Double.POSITIVE_INFINITY;
      }

      /**
       *
       */
      protected BufferedImage getNonGridBase() {
        if (base_bi == null) {
          Graphics2D g2d = null; try {
            BufferedImage bi = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) bi.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            RTColorManager.renderVisualizationBackground(bi, g2d);

            // Determine if this is a similarity ordering...
            boolean similar_possible = true;
            if (cat_most_similar_to != null) {
              if (cat_map.containsKey(cat_most_similar_to)) similar_possible = true; else similar_possible = false;
            }

            // Apply a sort to the counter contexts
            // -- Standard sorters
            List<MySorter> sorter = new ArrayList<MySorter>();
            if (cat_most_similar_to == null || similar_possible == false) {
              Iterator<String> it = cat_map.keySet().iterator(); while (it.hasNext()) {
                String cat = it.next(); BundlesCounterContext bcc = cat_map.get(cat);
                if      (sort_small_multiples.equals(SORT_RECORDS)) sorter.add(new RecordsSorter(cat,bcc));
                else if (sort_small_multiples.equals(SORT_COUNT))   sorter.add(new CountSorter  (cat,bcc));
                else if (sort_small_multiples.equals(SORT_ALPHA))   sorter.add(new AlphaSorter  (cat,bcc));
                else throw new RuntimeException("Do Not Understand Sort Option \"" + sort_small_multiples + "\"");
              }
              Collections.sort(sorter);

              // Apply natural sort for DoW and Months...
              if (sort_small_multiples.equals(SORT_ALPHA) &&
                  (category.equals(KeyMaker.BY_DAYOFWEEK_STR) ||
                   category.equals(KeyMaker.BY_MONTH_STR))) { sorter = naturalSort(sorter); }
            } else {
              // -- Sorter by vector distance metrics
              calculateVectors();
              Iterator<String> it = cat_vec.keySet().iterator(); while (it.hasNext()) {
                String cat = it.next(); if (cat.equals(cat_most_similar_to) || cat.equals("Visible")) continue;
                sorter.add(new DoubleSorter(cat, cat_map.get(cat), (double) vectorDistance(cat,cat_most_similar_to)));
              }
              Collections.sort(sorter);

              // Add the original back and reverse sort...
              sorter.add(new DoubleSorter(cat_most_similar_to, cat_map.get(cat_most_similar_to), 0.0)); 
              Collections.reverse(sorter);
            }

            // Render the small multiples -- set up the threads first...
            // - render thread queue
            Queue<RenderSMThread> queue = new LinkedList<RenderSMThread>();

            // - geometry variables
            int x_base = 0, y_base = 0;

            if (show_visible) {
              // Add this specific small multiple to the render queue
              queue.add(new RenderSMThread("Visible", x_base, y_base, txt_h));

              x_base += mult_w + x_spc_btwn_mults; 
              if (x_base > (rc_w - mult_w/2)) { x_base = 0; y_base += y_spc_btwn_mults + mult_h; } // if (x_base > rc_w+mult_w/3) { x_base = 0; y_base += y_spc_btwn_mults + mult_h; }
            }

            int sort_i = 0; while (sort_i < sorter.size() && y_base < rc_h+mult_h/3) {
              // Maintain info on the geometry for interactivity
              Rectangle2D rect = new Rectangle2D.Double(x_base, y_base, mult_w + x_spc_btwn_mults, mult_h + y_spc_btwn_mults);
              skey_to_geom.put(sorter.get(sort_i).key, rect);
              geom_to_skey.put(rect, sorter.get(sort_i).key);

              // Add to the render queue
              queue.add(new RenderSMThread(sorter.get(sort_i).key, x_base, y_base, txt_h));

              x_base += mult_w + x_spc_btwn_mults;
              if (x_base > (rc_w - mult_w/2)) { x_base = 0; y_base += y_spc_btwn_mults + mult_h; } // if (x_base > rc_w+mult_w/3) { x_base = 0; y_base += y_spc_btwn_mults + mult_h; }

              sort_i++;
            }

            // Run the threads
            Thread               threads[] = new Thread[16]; // Run 16 threads
            List<RenderSMThread> done      = new ArrayList<RenderSMThread>();
            for (int i=0;i<threads.length;i++) { threads[i] = new Thread(new RenderQueue(queue, done)); threads[i].start(); }
            for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }

            // Copy the small multiple rendering to the screen
            for (int i=0;i<done.size();i++) {
              RenderSMThread rend = done.get(i);
              g2d.drawImage(rend.sm_bi, rend.x_base, rend.y_base, null);

              if (cat_most_similar_to != null && similar_possible && rend.cat_key.equals(cat_most_similar_to)) {
                g2d.setColor(RTColorManager.getColor("annotate", "labelfg"));
                g2d.draw(new Rectangle2D.Double(rend.x_base, rend.y_base, rend.sm_bi.getWidth(), rend.sm_bi.getHeight()));
              }
            }

            // Label the render time
            long render_ts1 = System.currentTimeMillis();
            g2d.setColor(RTColorManager.getColor("label", "performance"));
            String str = "RTime: " + (render_ts1 - render_ts0) + " ms";
            g2d.drawString(str, bi.getWidth() - Utils.txtW(g2d, str), bi.getHeight() - 2);

            // Set it to the global variable for re-requests
            base_bi = bi;
          } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }

      /**
       *
       */
      protected BufferedImage getGridBase() {
        if (base_bi == null) {
          Graphics2D g2d = null; try {
            // Construct the image ... setup the rendering image
            BufferedImage bi    = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) bi.getGraphics();
                  g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            RTColorManager.renderVisualizationBackground(bi, g2d);

            // Apply a sort to the counter contexts -- separate by rows and columns ... rows first
            List<MySorter> row_sorter = new ArrayList<MySorter>();
            Iterator<String> it = cat_cc.binIterator(); while (it.hasNext()) {
              String key = it.next(); 
              if      (sec_sort_small_multiples.equals(SORT_RECORDS)) row_sorter.add(new DoubleSorter(key,cat_cc,cat_cc.binRecordCount(key)));
              else if (sec_sort_small_multiples.equals(SORT_COUNT))   row_sorter.add(new DoubleSorter(key,cat_cc,cat_cc.total(key)));
              else if (sec_sort_small_multiples.equals(SORT_ALPHA))   row_sorter.add(new AlphaSorter (key,cat_cc));
            }
            Collections.sort(row_sorter);
            if (sec_sort_small_multiples.equals(SORT_ALPHA) &&
                (category.equals(KeyMaker.BY_DAYOFWEEK_STR) ||
                 category.equals(KeyMaker.BY_MONTH_STR))) row_sorter = naturalSort(row_sorter);

            // Apply a sort to the counter contexts -- separate by rows and columns .. now columns
            List<MySorter> col_sorter = new ArrayList<MySorter>();
            it = cat2_cc.binIterator(); while (it.hasNext()) {
              String key = it.next(); 
              if      (sort_small_multiples.equals(SORT_RECORDS)) col_sorter.add(new DoubleSorter(key,cat2_cc,cat2_cc.binRecordCount(key)));
              else if (sort_small_multiples.equals(SORT_COUNT))   col_sorter.add(new DoubleSorter(key,cat2_cc,cat2_cc.total(key)));
              else if (sort_small_multiples.equals(SORT_ALPHA))   col_sorter.add(new AlphaSorter (key,cat2_cc));
            }
            Collections.sort(col_sorter);
            if (sort_small_multiples.equals(SORT_ALPHA) &&
                (category2.equals(KeyMaker.BY_DAYOFWEEK_STR) ||
                 category2.equals(KeyMaker.BY_MONTH_STR))) col_sorter = naturalSort(col_sorter);

            // Draw the row and column labels
            int row_i = 0, y_base = (txt_h + 3);
            while (row_i < row_sorter.size() && y_base < rc_h) {
              // Do the labels for the rows and cols
              g2d.setColor(RTColorManager.getColor("label", "default"));
              String label = row_sorter.get(row_i).key;
              if (label.length() < 48) while (label.length() > 3 && Utils.txtW(g2d, label) > mult_h) label = label.substring(0,label.length()-1);
              else                     label = label.substring(0,3) + "..."; // Should never happen... but could see someone choosing a bad field
              Utils.drawRotatedString(g2d, label, txt_h+1, y_base + mult_h/2 + Utils.txtW(g2d, label)/2);
              row_i++; y_base += mult_h + y_spc_btwn_mults;
            } 

            int col_i, x_base;

            // Don't draw column headers if shrinkwrapped
            if (shrinkwrap == false) {
              col_i = 0; x_base = (txt_h + 3);
              while (col_i < col_sorter.size() && x_base < rc_w) {
                // Do the labels for the rows and cols
                g2d.setColor(RTColorManager.getColor("label", "default"));
                String label = col_sorter.get(col_i).key;
                if (label.length() < 48) while (label.length() > 3 && Utils.txtW(g2d, label) > mult_w) label = label.substring(0,label.length()-1);
                else                     label = label.substring(0,3) + "..."; // Should never happen... but could see someone choosing a bad field
                g2d.drawString(label, x_base + mult_w/2 - Utils.txtW(g2d,label)/2, txt_h+1);
                col_i++; x_base += mult_w + x_spc_btwn_mults;
              }
            }

            // Render thread queue
            Queue<RenderSMThread> queue = new LinkedList<RenderSMThread>();

            // Thread the rendering of the small multiples
            row_i = 0; y_base = (txt_h + 3);
            while (row_i < row_sorter.size() && y_base < rc_h) {
              col_i  = 0; 
              x_base = (txt_h + 3);

              while (col_i < col_sorter.size() && x_base < rc_w) {
                // Create the category key
                String cat_key = Utils.encToURL(row_sorter.get(row_i).key) + "|" + Utils.encToURL(col_sorter.get(col_i).key);
                if (cat_map.containsKey(cat_key)) {
                  // Add this specific small multiple to the render queue
                  queue.add(new RenderSMThread(cat_key, x_base, y_base, txt_h));

                  // Make the references for interactivity
                  Rectangle2D rect = new Rectangle2D.Double(x_base, y_base, mult_w + x_spc_btwn_mults, mult_h + y_spc_btwn_mults);
                  skey_to_geom.put(cat_key, rect);
                  geom_to_skey.put(rect, cat_key);
                } 
                col_i++; 
                
                if (shrinkwrap == false || cat_map.containsKey(cat_key)) x_base += (mult_w + x_spc_btwn_mults);
              }
              row_i++; y_base += (mult_h + y_spc_btwn_mults);
            }
            
            // Run the threads
            Thread               threads[] = new Thread[16]; // Run 16 threads
            List<RenderSMThread> done      = new ArrayList<RenderSMThread>();
            for (int i=0;i<threads.length;i++) { threads[i] = new Thread(new RenderQueue(queue, done)); threads[i].start(); }
            for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }

            // Copy the small multiple rendering to the screen
            for (int i=0;i<done.size();i++) {
              RenderSMThread rend = done.get(i);
              g2d.drawImage(rend.sm_bi, rend.x_base, rend.y_base, null);
            }

            // Label the render time
            long render_ts1 = System.currentTimeMillis();
            g2d.setColor(RTColorManager.getColor("label", "performance"));
            String str = "RTime: " + (render_ts1 - render_ts0) + " ms";
            g2d.drawString(str, bi.getWidth() - Utils.txtW(g2d, str), bi.getHeight() - 2);


            // Set it to the global variable for re-requests
            base_bi = bi;
          } finally { if (g2d != null) g2d.dispose(); }
        } return base_bi;
      }

      /**
       * Pull threads off of the queue and run them.  Stop once there are no more
       * items to pull off the queue.
       */
      protected class RenderQueue implements Runnable {
        Queue<RenderSMThread> queue; List<RenderSMThread> done;
        public RenderQueue(Queue<RenderSMThread> queue, List<RenderSMThread> done) { this.queue = queue; this.done = done; }
        public void run() { 
          boolean queue_empty = false;
          while (queue_empty == false) {
            RenderSMThread thread = null;
            synchronized (queue) { if (queue.size() > 0) thread = queue.remove(); else queue_empty = true; }
            if (thread != null) { thread.run(); synchronized (done) { done.add(thread); } }
          }
        }
      }

      /**
       * Render thread for a small multiple
       */
      protected class RenderSMThread implements Runnable {
        String                cat_key;  // category key
        int                   x_base,   // x position of the small multiple
                              y_base;   // y position of the small multiple
        BufferedImage         sm_bi;    // rendered version
        public RenderSMThread(String cat_key, int x_base, int y_base, int txt_h) { this.x_base = x_base; this.y_base = y_base; this.cat_key = cat_key; }
        public void run() { 
          if (cat_key.equals("Visible")) sm_bi = renderSmallMultiple(cat_key, visible_cc);
          else                           sm_bi = renderSmallMultiple(cat_key, cat_map.get(cat_key)); 
        }
      }

      /**
       * Lookup for a record to the string keys
       */
      Map<Bundle,Set<String>> bundle_to_skeys = new HashMap<Bundle,Set<String>>();

      /**
       * Lookup for a string key to the bundles
       */
      Map<String,Set<Bundle>> skey_to_bundles = new HashMap<String,Set<Bundle>>();

      /**
       * Lookup for a string key to the geometry
       */
      Map<String,Rectangle2D> skey_to_geom    = new HashMap<String,Rectangle2D>();

      /**
       * Lookup for a geometry to a string key
       */
      Map<Rectangle2D,String> geom_to_skey    = new HashMap<Rectangle2D,String>();

      /**
       * Return all shapes that were rendered.
       *
       *@return set of rendered shapes
       */
      public Set<Shape>      allShapes()                     { 
        Set<Shape> set = new HashSet<Shape>();
        set.addAll(geom_to_skey.keySet());
        return set;
      }

      /**
       * For a set of bundles, return all the shapes representing those bundles.
       *
       *@param bundles bundles to look up
       *
       *@return all shapes that were rendered because of those bundles
       */
      public Set<Shape>  shapes(Set<Bundle> bundles)         {
        Set<Shape> shapes = new HashSet<Shape>();
        // Convert to skeys
        Set<String> skeys = new HashSet<String>();
        Iterator<Bundle> it = bundles.iterator(); while (it.hasNext()) {
          Bundle bundle = it.next();
          if (bundle_to_skeys.containsKey(bundle)) skeys.addAll(bundle_to_skeys.get(bundle));
        }
        // Convert to geometry
        Iterator<String> it_skeys = skeys.iterator(); while (it_skeys.hasNext()) shapes.add(skey_to_geom.get(it_skeys.next()));
        return shapes; }

      /**
       * Return all bundles related to a specific shape... the shape should have been found
       * in the allShapes() method -- i.e., generic shapes are not supported.
       *
       *@param shape shape to look for
       *
       *@return bundles related to the shape
       */
      public Set<Bundle> shapeBundles(Shape shape)           { 
        Set<Bundle> set = new HashSet<Bundle>();
        if (geom_to_skey.containsKey(shape)) set.addAll(skey_to_bundles.get(geom_to_skey.get(shape)));
        return set; }

      /**
       * Return shapes that overlap with the specified shape.
       *
       *@param shape shape to examinate for overlap
       *
       *@return shapes that were rendered that overlap with the specified shape
       */
      public Set<Shape>  overlappingShapes(Shape shape)      { 
        Set<Shape> set = new HashSet<Shape>();
        Iterator<Rectangle2D> it = geom_to_skey.keySet().iterator(); while (it.hasNext()) {
          Rectangle2D rect = it.next();
          if (Utils.genericIntersects(rect,shape)) set.add(rect);
        }
        return set; }

      /**
       * Return shapes that contain the specified coordinate point.
       *
       *@param x x coordinate
       *@param y y coordinate
       *
       *@return set of shapes that contain the (x,y) coordinate
       */
      public Set<Shape>  containingShapes(int x, int y)      { 
        Set<Shape> set = new HashSet<Shape>();
        Iterator<Rectangle2D> it = geom_to_skey.keySet().iterator(); while (it.hasNext()) {
          Rectangle2D rect = it.next();
          if (rect.contains(x,y)) set.add(rect);
        }
        return set; }
    }

    /**
     * Count the number of items returned by an iterator.
     * - This is used to count rows and columns for the grid
     */
    int count(Iterator<String> it) { int sum = 0; while (it.hasNext()) { sum++; it.next(); } return sum; }

    /**
     * Count the max number of rows.  The input is a set of strings with "cat_1|cat_2" format.
     */
    int maxRowCount(Set<String> cat_keys) {
      int max_row_count = 1; Map<String,Set<String>> row_lu = new HashMap<String,Set<String>>();
      Iterator<String> it = cat_keys.iterator(); while (it.hasNext()) {
        String cat_key = it.next();
        if (cat_key.indexOf('|') >= 0) {
          String row = cat_key.substring(0,cat_key.indexOf("|")),
                 col = cat_key.substring(cat_key.indexOf("|")+1,cat_key.length());
          if (row_lu.containsKey(row) == false) row_lu.put(row, new HashSet<String>());
          row_lu.get(row).add(col);
          if (row_lu.get(row).size() > max_row_count) max_row_count = row_lu.get(row).size();
        }
      }
      return max_row_count;
    }

    /**
     * RenderContext implementation for the correlation matrix
     */
    public class PieChartRenderContext extends SMRC {

      /**
       * Construct the rendering context for pie chart small multiples.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public PieChartRenderContext(short id, Bundles bs, String count_by, String color_by, 
                                   boolean show_visible, boolean label_small_multiples, boolean grid_view, boolean vary_color, boolean shrinkwrap,
                                   String  category, String category2,
                                   boolean y_independent,
                                   String  sort_small_multiples, String sec_sort_small_multiples,
                                   int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
              this.count_by              = count_by;
              this.color_by              = color_by;
        this.show_visible          = show_visible;
        this.label_small_multiples = label_small_multiples;
        this.grid_view             = grid_view;
        this.vary_color            = vary_color;
        this.shrinkwrap            = shrinkwrap;
        this.category              = category;
        this.category2             = category2;
        this.y_independent         = y_independent;
        this.sort_small_multiples     = sort_small_multiples;
        this.sec_sort_small_multiples  = sec_sort_small_multiples;

        if (shrinkwrap == false && 
            category2.equals(NONE_STR) == false && 
            grid_view) this.label_small_multiples = false; // Turn off individual labeling since rows and columns will have labels

              Iterator<Tablet> it_tab = bs.tabletIterator();

        visible_cc = new BundlesCounterContext(bs, count_by, color_by);

              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute? ... a null color means that there can be no rendering
                boolean tablet_can_count = (count_by.equals(BundlesDT.COUNT_BY_BUNS) || 
                                      KeyMaker.tabletCompletesBlank(tablet, count_by)) &&
                                     (color_by != null &&
                                      KeyMaker.tabletCompletesBlank(tablet, color_by));

          if (tablet_can_count) {

            KeyMaker km = new KeyMaker(tablet, color_by);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);
            if (cat_km != null && cat2_km != null && grid_view) { // Needed for grid view
              if (cat_cc == null) {
                cat_cc  = new BundlesCounterContext(bs, count_by, color_by);
                cat2_cc = new BundlesCounterContext(bs, count_by, color_by);
              }
            }

            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // State for interaction
              if (bundle_to_skeys.containsKey(bundle) == false) bundle_to_skeys.put(bundle, new HashSet<String>());

              String keys[] = km.stringKeys(bundle); if (keys != null && keys.length > 0) {
                for (int key_i=0;key_i<keys.length;key_i++) {
                  String key = keys[key_i];

                  visible_cc.count(bundle, key);      // Add to the visible set at least
                  if (cat_km != null || cat2_km != null) {
                    String cats [] = {""}, 
                           cats2[] = {""};

                    if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
                    if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

                    // For grid view, sum rows and columns (for sorting)
                    if (cat_cc  != null && cat_km  != null) { for (int cat_i=0; cat_i <cats.length; cat_i++)  cat_cc. count(bundle, cats [cat_i]);  }
                    if (cat2_cc != null && cat2_km != null) { for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) cat2_cc.count(bundle, cats2[cat2_i]); }

                    for (int cat_i=0;cat_i<cats.length;cat_i++) {
                      for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) {
                        String cat_key = Utils.encToURL(cats[cat_i]) + "|" + Utils.encToURL(cats2[cat2_i]);

                        if (cat_map.containsKey(cat_key) == false) cat_map.put(cat_key, new BundlesCounterContext(bs, count_by, color_by));
                        cat_map.get(cat_key).count(bundle,key);

                        // State for interaction
                        bundle_to_skeys.get(bundle).add(cat_key);
                        if (skey_to_bundles.containsKey(cat_key) == false) skey_to_bundles.put(cat_key, new HashSet<Bundle>());
                        skey_to_bundles.get(cat_key).add(bundle);
                      }
                    }
                  }
                }
              }
                  }
                } else { addToNoMappingSet(tablet); }
              }

        // Optimize the size of the small multiple
        int sizes[] = { 128, 112, 96, 80, 72, 64, 56, 48, 40, 32 };
        if (cat2_cc != null) {
          // Optimize for grid
          int rows =  count(cat_cc. binIterator());
          int cols;
          
          if (shrinkwrap) cols = maxRowCount(cat_map.keySet());
          else            cols = count(cat2_cc.binIterator());
          for (int i=0;i<sizes.length;i++) {
            mult_w = sizes[i]; mult_h = sizes[i] + (this.label_small_multiples ? txt_h + txt_space : 0);
            if (((mult_w*cols) < rc_w) && ((mult_h*rows) < rc_h)) break;
          }
        } else {
          // Optimize for non-grid
          int total = (show_visible ? 1 : 0) + cat_map.keySet().size();
          for (int i=0;i<sizes.length;i++) {
            mult_w = sizes[i]; mult_h = sizes[i] + (this.label_small_multiples ? txt_h + txt_space : 0);
            if (total < (rc_w / mult_w) * (rc_h / mult_h)) break;
          }
        }
      }

      /**
       * Calculate the vector representations of each small multiple for a distance metric comparison.
       */
      protected void calculateVectors() {
        Iterator<String> it_cat = cat_map.keySet().iterator(); while (it_cat.hasNext()) {
          String cat = it_cat.next(); BundlesCounterContext bcc = cat_map.get(cat);

          // Representation by bin and normalized value
          Map<String,Double> norms = new HashMap<String,Double>();
          double total = bcc.totalCounts(); // total sum of all the slices

          // Every slice of the pie chart
          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String bin = it.next(); double n = bcc.total(bin)/total; norms.put(bin,n);
          }

          // Save it for comparison
          cat_vec.put(cat,norms);
        }
      }

      /**
       * Render a single small multiple.
       *
       *@param key       small multiple key
       *@param bcc       bundles counter context for that key
       */
      public BufferedImage renderSmallMultiple(String key, BundlesCounterContext bcc) {
        BufferedImage sm_bi = new BufferedImage(mult_w, mult_h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = null; try {
          g2d = (Graphics2D) sm_bi.getGraphics();
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          // Fill with the background
                RTColorManager.renderVisualizationBackground(sm_bi, g2d);

          // First pass will either be the actual small multiple partition... or
          // the complete visible set as the background for the small multiple partition
          BundlesCounterContext to_render = null;
          Composite             comp_orig = g2d.getComposite();
          if (y_independent) { to_render = bcc; }
          else               { to_render = visible_cc;
                               g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f)); }

          // First pass render
          renderPieChart(g2d, sm_bi, to_render, false);

          // Do it all over again for partials (if independent is false)
          if (y_independent == false) { g2d.setComposite(comp_orig); renderPieChart(g2d, sm_bi, bcc, true); }

          // Figure out the labeling
          if (label_small_multiples) {
            g2d.setColor(RTColorManager.getColor("label", "default"));

            // Figure out the label
            String label = "unassigned";
            if      (key.equals("Visible")) label = key;
            else if (key.startsWith("|"))   label = Utils.decFmURL(key.substring(1,key.length()));
            else if (key.endsWith  ("|"))   label = Utils.decFmURL(key.substring(0,key.length()-1));
            else if (key.indexOf   ("|") >= 0) {
              int io = key.indexOf("|");

              if (shrinkwrap) label = Utils.decFmURL(key.substring(io+1,key.length()));
              else            label = Utils.decFmURL(key.substring(0,io)) + "|" + 
                                      Utils.decFmURL(key.substring(io+1,key.length()));
            }

            int txt_w = Utils.txtW(g2d, label);
            g2d.drawString(label, sm_bi.getWidth()/2 - txt_w/2, sm_bi.getHeight() - (txt_space/2));
          }

          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          g2d.drawRect(0, 0, sm_bi.getWidth() - 1, sm_bi.getWidth() - 1);

        } finally { if (g2d != null) g2d.dispose(); }
        return sm_bi;
      }

      /**
       * Render the pie chart itself. 
       *
       *@param g2d       graphics primitive
       *@param sm_bi     buffered image for the small multiple
       *@param to_render bundles counter context to render
       *@param partials  true to render a partial version of the pie chart (dependent rendering mode)
       */
      protected void renderPieChart(Graphics2D g2d, BufferedImage sm_bi, BundlesCounterContext to_render, boolean partials) {
          double circle_x = mult_x_ins,
                 circle_y = mult_x_ins,
                 circle_d = sm_bi.getWidth() - 2 * mult_x_ins;

          // Not every bin from the global set may be reprented in this specific counter context...
          Set<String> my_bin_set = new HashSet<String>();
          my_bin_set.addAll(to_render.getBinsSortedByCount());

          // But we still need to sort them according to the global sorting order
          List<String> bins             = visible_cc.getBinsSortedByCount();
          double       total            = to_render.totalCounts(),
                       total_v          = visible_cc.totalCounts();
          double       deg_start        = 90.0,
                       render_deg_start = 90.0;

          // Need to find the maximum ratio for the partials...
          double ratio_max = 1.0;
          if (partials) {
            for (int i=0;i<bins.size();i++) {
              String bin = bins.get(i); if (my_bin_set.contains(bin)) {
                double total_bin   = to_render.total(bin),
                       total_bin_v = visible_cc.total(bin);
                double ratio = ((total_bin / total)/(total_bin_v / total_v));
                if (ratio > ratio_max) ratio_max = ratio;

              }
            }
          }

          // Go through the bins -- greatest to least drawin the pies
          int bin_i = bins.size()-1;
          while (bin_i >= 0) {
            String bin       = bins.get(bin_i); if (my_bin_set.contains(bin)) {
              double total_bin   = to_render.total(bin),
                     total_bin_v = visible_cc.total(bin);

              // Figure out the degree increment... if it's partial, use the visible_cc proportions
              double deg_inc;
              if (partials) deg_inc = (total_bin_v / total_v) * 360.0;
              else          deg_inc = (total_bin   / total  ) * 360.0;

              g2d.setColor(RTColorManager.getColor(bin));

              // Modulate the circle size for partials
              if (partials) {
                double c_x = circle_x, c_y = circle_y, c_d = circle_d;

                // Tricky... believe it's the ratio of this small as compared to the ratio of all
                double ratio = ((total_bin / total)/(total_bin_v / total_v));
                ratio = ratio / ratio_max;
                c_d = ratio * circle_d;
                c_x = circle_x + circle_d/2.0 - c_d/2.0;
                c_y = circle_y + circle_d/2.0 - c_d/2.0;

                g2d.fill(new Arc2D.Double(c_x, c_y, c_d, c_d, deg_start, deg_inc, Arc2D.PIE));
                render_deg_start += deg_inc;
              } else {
                // Only render if degree inc is greater than 3 degrees... 
                if (deg_inc > 3.0) {
                  g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, render_deg_start, deg_inc, Arc2D.PIE));
                  render_deg_start += deg_inc;
                }
              }

              // Update he start degrees and the loop control variable
              deg_start += deg_inc;
            }
            bin_i--;
          }

          // Draw the last wedge for everything else...
          if (render_deg_start < 360.0+90.0) {
            if (partials == false) {
              g2d.setColor(RTColorManager.getColor("set", "multi"));
              g2d.fill(new Arc2D.Double(circle_x, circle_y, circle_d, circle_d, render_deg_start, 360.0 + 90.0 - render_deg_start, Arc2D.PIE));
            } else {
              // Not sure what to do here... can the partials be more than the rest?
            }
          }
      }
    }

    /**
     * RenderContext implementation for the correlation matrix
     */
    public class BarChartRenderContext extends SMRC {
      /**
       * The x-axis for the barchart.... all of them were time-based
       */
      String  x_axis;

      /**
       * No gaps between the bars...
       */
      boolean no_gaps;

      /**
       * Size settings... or why can't enum be placed in inner classes :(
       */
      static final int SZ_UNDEFINED = -1,
                       SZ_SMALL     =  48,
                       SZ_MEDIUM    =  72,
                       SZ_LARGE     =  96;

      /**
       * Preferred size (if set)
       */
      int pref_size = SZ_UNDEFINED;

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public BarChartRenderContext(short id, Bundles bs, String count_by, String color_by, 
                                   boolean show_visible, boolean label_small_multiples, boolean grid_view, boolean vary_color, boolean shrinkwrap,
                                   String  category, String category2,
                                   boolean no_gaps,
                                   String  x_axis,   boolean y_independent,
                                   String  sort_small_multiples, String sec_sort_small_multiples,
                                   int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
              this.count_by                 = count_by;
              this.color_by                 = color_by;
        this.show_visible             = show_visible;
        this.label_small_multiples    = label_small_multiples;
        this.grid_view                = grid_view;
        this.vary_color               = vary_color;
        this.shrinkwrap               = shrinkwrap;
        this.category                 = category;
        this.category2                = category2;
        this.no_gaps                  = no_gaps;
        this.x_axis                   = x_axis;
        this.y_independent            = y_independent;
        this.sort_small_multiples     = sort_small_multiples;
        this.sec_sort_small_multiples = sec_sort_small_multiples;
        
        // By default, the pref_size is set to UNDEFINED which covers all of the non-linear-straight times
        if      (x_axis == null)                     { pref_size = SZ_SMALL;  this.x_axis = KeyMaker.BY_STRAIGHT_STR; }
        else if (x_axis.endsWith(SMALL_SUFFIX_STR))  { pref_size = SZ_SMALL;  this.x_axis = KeyMaker.BY_STRAIGHT_STR; }
        else if (x_axis.endsWith(MEDIUM_SUFFIX_STR)) { pref_size = SZ_MEDIUM; this.x_axis = KeyMaker.BY_STRAIGHT_STR; }
        else if (x_axis.endsWith(LARGE_SUFFIX_STR))  { pref_size = SZ_LARGE;  this.x_axis = KeyMaker.BY_STRAIGHT_STR; }

        if (shrinkwrap == false &&
            category2.equals(NONE_STR) == false && 
            grid_view) this.label_small_multiples = false; // Turn off individual labeling since rows and columns will have labels

        Map<Tablet,KeyMaker> tab_to_x_km = new HashMap<Tablet,KeyMaker>();

        visible_cc = new BundlesCounterContext(bs, count_by, color_by);

              Iterator<Tablet> it_tab = bs.tabletIterator();
              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
                boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);

          // All of the bar chart options are time based
          if (tablet_can_count && tablet.hasTimeStamps()) {

            KeyMaker x_km   = new KeyMaker(tablet, this.x_axis); tab_to_x_km.put(tablet, x_km);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);
            if (cat_km != null && cat2_km != null && grid_view) { // Needed for grid view
              if (cat_cc == null) {
                cat_cc  = new BundlesCounterContext(bs, count_by, color_by);
                cat2_cc = new BundlesCounterContext(bs, count_by, color_by);
              }
            }

            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // State for interaction
              if (bundle_to_skeys.containsKey(bundle) == false) bundle_to_skeys.put(bundle, new HashSet<String>());

              String key;
              if (this.x_axis.equals(KeyMaker.BY_STRAIGHT_STR)) key = straightTimeKey(bundle);
              else                                              key = (x_km.stringKeys(bundle))[0]; // Should only be one since they are time-based

              visible_cc.count(bundle, key);      // Add to the visible set at least
              if (cat_km != null || cat2_km != null) {
                String cats [] = {""}, 
                       cats2[] = {""};

                if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
                if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

                // For grid view, sum rows and columns (for sorting)
                if (cat_cc  != null && cat_km  != null) { for (int cat_i=0; cat_i <cats.length; cat_i++)  cat_cc. count(bundle, cats [cat_i]);  }
                if (cat2_cc != null && cat2_km != null) { for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) cat2_cc.count(bundle, cats2[cat2_i]); }

                for (int cat_i=0;cat_i<cats.length;cat_i++) {
                  for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) {
                    String cat_key = Utils.encToURL(cats[cat_i]) + "|" + Utils.encToURL(cats2[cat2_i]);

                    if (cat_map.containsKey(cat_key) == false) cat_map.put(cat_key, new BundlesCounterContext(bs, count_by, color_by));
                    cat_map.get(cat_key).count(bundle,key);

                    // State for interaction
                    bundle_to_skeys.get(bundle).add(cat_key);
                    if (skey_to_bundles.containsKey(cat_key) == false) skey_to_bundles.put(cat_key, new HashSet<Bundle>());
                    skey_to_bundles.get(cat_key).add(bundle);
                  }
                }
              }
                  }
                } else { addToNoMappingSet(tablet); }
              }

        // Figure out the geometry... (bar_w, max_bar_h, mult_w, mult_h, bar_gap)
        if (pref_size == SZ_UNDEFINED) {
          int bar_total = barTotal(tab_to_x_km);

          // width and gap in preferential order...
          int w_and_g_gaps   [][] = { {12, 1}, {10, 1}, {9, 1}, {8, 1}, {7,1}, {6,1}, {5,1}, {4,1}, {3,1}, {2,1}, {4,0}, {3,0}, {2,0}, {1,0} };
          int w_and_g_no_gaps[][] = { {12, 0}, {10, 0}, {9, 0}, {8, 0}, {7,0}, {6,0}, {5,0}, {4,0}, {3,0}, {2,0}, {1,0} };

          int w_and_g[][] = (no_gaps ? w_and_g_no_gaps : w_and_g_gaps);

          // Optimize the size of the small multiple
          if (cat2_cc != null) {
            // Optimize for grid
            int rows =  count(cat_cc. binIterator()), cols;
            
            if (shrinkwrap) cols = maxRowCount(cat_map.keySet());
            else            cols = count(cat2_cc.binIterator());

            for (int i=0;i<w_and_g.length;i++) {
              bar_w = w_and_g[i][0]; bar_gap = w_and_g[i][1]; 
              mult_w = 2*mult_x_ins + bar_total * (bar_w + bar_gap);
              mult_h = (2*mult_w) / 4; if (mult_h > 64) { mult_h = 64; } mult_h += (this.label_small_multiples) ? (txt_space + txt_h) : (0);
              //System.err.println(i + " | mult = " + mult_w + "x" + mult_h + " | rc = " + rc_w + "x" + rc_h +
              //                   " | cols x rows = " + cols + "x" + rows + " | " + (mult_w*cols) + "x" + (mult_h*rows) +
              //                   " | " + ((mult_w*cols) < rc_w) + "x" + ((mult_h*rows)<rc_h));
              if (((mult_w*cols) < rc_w) && ((mult_h*rows) < rc_h)) break;
            }
          } else {
            // Optimize for non-grid
            int total = (show_visible ? 1 : 0) + cat_map.keySet().size();
            for (int i=0;i<w_and_g.length;i++) {
              bar_w = w_and_g[i][0]; bar_gap = w_and_g[i][1];
              mult_w = 2*mult_x_ins + bar_total * (bar_w + bar_gap);
              mult_h = (3*mult_w) / 4; if (mult_h > 64) { mult_h = 64; } mult_h += (this.label_small_multiples) ? (txt_space + txt_h) : (0);
              if (total < (rc_w / mult_w) * (rc_h / mult_h)) break;
            }
          }
        } else {
          bar_w   = 1;
          bar_gap = 0;
          mult_w  = 2*mult_x_ins + pref_size;
        }

        mult_h     = (3*mult_w) / 4; if (mult_h > 64) mult_h = 64;        // without the label so that the max_bar_h gets set correctly
        max_bar_h  = mult_h - 2*mult_y_ins;
        mult_h    += (this.label_small_multiples) ? (txt_space + txt_h) : (0); // re-add the label space back in
      }

      /**
       * maximum bar height
       */
      protected int max_bar_h;

      /**
       * Bar width... should get shorter as there are more bars
       */
      protected int bar_w;

      /**
       * Gap between bars... should either be 0 or 1
       */
      protected int bar_gap;


      /**
       * Converts a timestamp into an integer index based on the preferred size
       * of the window.
       */
      String straightTimeKey(Bundle bundle) {
        long denom = bs.ts1() - bs.ts0(); if (denom == 0L) denom = 1L;
        int x = (int) ((pref_size * (bundle.ts0() - bs.ts0()))/denom);
        return "" + x;
      }

      /**
       * Calculate the vector representations of each small multiple for a distance metric comparison.
       */
      protected void calculateVectors() {
        Iterator<String> it_cat = cat_map.keySet().iterator(); while (it_cat.hasNext()) {
          String                cat    = it_cat.next();
          BundlesCounterContext bcc    = cat_map.get(cat);

          Map<String,Double> vec = new HashMap<String,Double>();

          Iterator<String>      it_bin = bcc.binIterator(); while (it_bin.hasNext()) {
            String bin  = it_bin.next();
            double norm = bcc.totalNormalized(bin);

            vec.put(bin,norm);
          }
          cat_vec.put(cat, vec);
        }
      }

      /**
       * Find the largest total maximum across all of the small multiple counter contexts.  Cached for quicker access...
       *
       *@return largest total maximum
       */
      protected double largestTotalMaximum() {
        if (Double.isNaN(largest_total_maximum)) {
          if (show_visible) {
            largest_total_maximum = visible_cc.totalMaximum(); // should be the max...
          } else {
            Iterator<String> it = cat_map.keySet().iterator(); while (it.hasNext()) {
              String cat = it.next();
              if      (Double.isNaN(largest_total_maximum))                      largest_total_maximum = cat_map.get(cat).totalMaximum();
              else if (largest_total_maximum < cat_map.get(cat).totalMaximum())  largest_total_maximum = cat_map.get(cat).totalMaximum();
            }
          }
        }
        return largest_total_maximum;
      }

      /**
       * Cached version of the largest total maximum
       */
      private double largest_total_maximum = Double.NaN;

      /**
       * Cache for converting integer strings to integers.
       */
      private synchronized int asInt(String s) {
        if (as_int_lu.containsKey(s) == false) { as_int_lu.put(s, Integer.parseInt(s)); }
        return as_int_lu.get(s);
      }
      Map<String,Integer> as_int_lu = new HashMap<String,Integer>();

      /**
       * Render a single small multiple.
       *
       *@param key       small multiple key
       *@param bcc       bundles counter context for that key
       *@param bar_total total number of bars for the small multiple
       */
      public BufferedImage renderSmallMultiple(String key, BundlesCounterContext bcc) {
        BufferedImage sm_bi = new BufferedImage(mult_w, mult_h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = null; try {
          g2d = (Graphics2D) sm_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

          // Fill with the background
          RTColorManager.renderVisualizationBackground(sm_bi, g2d);

          // Y base
          int    y_base = mult_y_ins + max_bar_h;

          // Color
          g2d.setColor(RTColorManager.getColor("data", "default"));

          // Iterate over the bins drawing each bar
          List<String> cbins = visible_cc.getColorBinsSortedByCount(); // was bcc.getColorBinsSortedByCount();

          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String bin    = it.next();
            int    bar_h;

            if (y_independent) bar_h  = (int) (bcc.totalNormalized(bin) * max_bar_h);
            else               bar_h  = (int) (((bcc.total(bin) * max_bar_h) / largestTotalMaximum()));

            int    x_i;
            if (pref_size == SZ_UNDEFINED) x_i = time_order_map.get(bin); // discrete time
            else                           x_i = asInt(bin);              // continuous time

            int    x      = mult_x_ins + x_i * (bar_w + bar_gap);

            if (vary_color && color_by != null && color_by.equals(BundlesDT.COUNT_BY_NONE) == false) {
              int          y_inc = y_base; double left_overs = 0.0;
              for (int i=cbins.size()-1;i>=0;i--) {
                String cbin = cbins.get(i);
                double ctotal = bcc.total(bin,cbin);
                if (ctotal > 0.0) {
                  int subh = (int) ((ctotal * bar_h)/bcc.binColorTotal(bin));
                  if (subh >0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(x, y_inc-subh, bar_w, subh);
                    y_inc -= subh;
                  } else { left_overs += ctotal; }
                }
              }

              // Render the left overs... assuming that they actually make up a pixel
              int subh = (int) ((left_overs * bar_h)/bcc.binColorTotal(bin));
              if (subh > 0) {
                g2d.setColor(RTColorManager.getColor("set","multi"));
                g2d.fillRect(x, y_inc-subh, bar_w, subh);
                y_inc -= subh;
              }
            } else { g2d.fillRect(x, y_base - bar_h, bar_w, bar_h); }
          }

          if (label_small_multiples) {
            g2d.setColor(RTColorManager.getColor("label", "default"));

            // Figure out the label
            String label = "unassigned";
            if      (key.equals("Visible")) label = key;
            else if (key.startsWith("|"))   label = Utils.decFmURL(key.substring(1,key.length()));
            else if (key.endsWith  ("|"))   label = Utils.decFmURL(key.substring(0,key.length()-1));
            else if (key.indexOf   ("|") >= 0) {
              int io = key.indexOf("|");
              if (shrinkwrap) label = Utils.decFmURL(key.substring(io+1,key.length()));
              else            label = Utils.decFmURL(key.substring(0,io)) + "|" + 
                                      Utils.decFmURL(key.substring(io+1,key.length()));
            }

            int txt_w = Utils.txtW(g2d, label);
            g2d.drawString(label, sm_bi.getWidth()/2 - txt_w/2, mult_y_ins + max_bar_h + mult_y_ins + (txt_space/2) + txt_h);
          }

          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          g2d.drawRect(0, 0, sm_bi.getWidth() - 1, mult_y_ins + max_bar_h + mult_y_ins - 1);

        } finally { if (g2d != null) g2d.dispose(); }
        return sm_bi;
      }

      /**
       * List of time order strings ... order is from min to max
       */
      protected List<String> time_order_strs = new ArrayList<String>();

      /**
       * Map from the time string to the bin index ... zero-based
       */
      protected Map<String,Integer> time_order_map = new HashMap<String,Integer>();

      /**
       * Determine the total number of bars in the plot ... probably needs to be re-examined if additional BY_{time} strings are added to the GUI...
       * - side effects:  fills in time_order_strs and time_order_map
       */
      protected int barTotal(Map<Tablet,KeyMaker> tab_to_x_km) {
        if (tab_to_x_km.keySet().size() == 0) return 0;

        KeyMaker km = tab_to_x_km.get(tab_to_x_km.keySet().iterator().next());

        if (km.linearTime() == false) { // Periodic time
          long min = km.minPeriodicValue(),
               max = km.maxPeriodicValue(),
               inc = 1L;
          int  ret = 0;
          if      (max < 2048)                          { ret = (int) (max+1); inc = 1L;                }
          else if (x_axis.equals(KeyMaker.BY_HOUR_STR)) { ret = 24;            inc = 60L * 60L * 1000L; }
          else                                          throw new RuntimeException("Do Not Understand Time \"" + x_axis + "\"");

          int index = 0;
          for (long l=min;l<=max;l+=inc) {
            String ts_str = km.toString(l);

            // Hack to make the binning strings line up with these strings
            if      (x_axis.equals(KeyMaker.BY_HOUR_STR))           ts_str = ts_str.substring(0,2);
            else if (x_axis.equals(KeyMaker.BY_MONTH_STR))          ts_str = ((((l+1) < 10) ? "0" : "")  + (l+1));
            else if (x_axis.equals(KeyMaker.BY_MONTH_DAY_STR))      ts_str = mon_map.get(ts_str.substring(0,3)) + ts_str.substring(3,6);
            else if (x_axis.equals(KeyMaker.BY_DAYOFWEEK_HOUR_STR)) ts_str = ts_str.substring(0,6);

            time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);
          }

          return ret;
        } else if (x_axis.equals(KeyMaker.BY_STRAIGHT_STR)) { // Linear time continuous
          return pref_size;
        } else                        { // Linear time discrete
          long ts = bs.ts0(), ts_stop = bs.ts1();

          Calendar cal_inc  = Calendar.getInstance(TimeZone.getTimeZone("GMT")),
                   cal_stop = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
          cal_inc.setTimeInMillis(ts); cal_stop.setTimeInMillis(ts_stop);

          int index = 0;
          if        (x_axis.equals(KeyMaker.BY_YEAR_STR))            {
            while (cal_inc.get(Calendar.YEAR) != cal_stop.get(Calendar.YEAR)) {
              String ts_str = km.toString(cal_inc.getTimeInMillis());
              time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);
              cal_inc.add(Calendar.YEAR, 1);
            }
          } else if (x_axis.equals(KeyMaker.BY_YEAR_QUARTER_STR))    {
          
            while (km.toString(cal_inc.getTimeInMillis()).equals(km.toString(cal_stop.getTimeInMillis())) == false) {
              String ts_str = km.toString(cal_inc.getTimeInMillis());
              time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);
              cal_inc.add(Calendar.MONTH, 3);
            }
          } else if (x_axis.equals(KeyMaker.BY_YEAR_MONTH_STR))      {
            while (cal_inc.get(Calendar.YEAR)  != cal_stop.get(Calendar.YEAR) ||
                   cal_inc.get(Calendar.MONTH) != cal_stop.get(Calendar.MONTH)) {
              String ts_str = km.toString(cal_inc.getTimeInMillis());
              time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);
              cal_inc.add(Calendar.MONTH, 1);
            }
          } else if (x_axis.equals(KeyMaker.BY_YEAR_MONTH_DAY_STR))  {
            while (cal_inc.get(Calendar.YEAR)          != cal_stop.get(Calendar.YEAR)  ||
                   cal_inc.get(Calendar.MONTH)         != cal_stop.get(Calendar.MONTH) ||
                   cal_inc.get(Calendar.DAY_OF_MONTH)  != cal_stop.get(Calendar.DAY_OF_MONTH)) {
              String ts_str = km.toString(cal_inc.getTimeInMillis());
              time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);
              cal_inc.add(Calendar.DAY_OF_MONTH, 1);
            }
          } else throw new RuntimeException("Do Not Understand Time \"" + x_axis + "\"");

          String ts_str = km.toString(cal_inc.getTimeInMillis());
          time_order_strs.add(ts_str); time_order_map.put(ts_str, index++);

          return index;
        }
      }
    }

    /**
     * RenderContext implementation for the correlation matrix
     */
    public class XYScatterRenderContext extends SMRC {
      /**
       * Use larger dots in the scatter plot
       */
      boolean larger_dots,

      /**
       * True to make the x axis independent for the small multiple
       */
              independent_x_axis;

      /**
       * X axis
       */
      String  x_axis,

      /**
       * X scale
       */
              x_scale,

      /**
       * Y axis
       */
              y_axis,

      /**
       * Y scale
       */
              y_scale;

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public XYScatterRenderContext(short id, Bundles bs, String count_by, String color_by, 
                                    boolean show_visible, boolean label_small_multiples, boolean grid_view, boolean vary_color, boolean larger_dots, boolean shrinkwrap,
                                    String  category,  String  category2,
                                    String  x_axis,    String  x_scale,   boolean independent_x_axis,
                                    String  y_axis,    String  y_scale,   boolean independent_y_axis,
                                    String  sort_small_multiples, String  sec_sort_small_multiples,
                                    int w, int h) {
        render_id = id; this.bs = bs; 
        this.show_visible          = show_visible;
        this.label_small_multiples = label_small_multiples;
        this.grid_view             = grid_view;
        this.vary_color            = vary_color;
        this.larger_dots           = larger_dots;
        this.shrinkwrap            = shrinkwrap;
        this.category              = category;
        this.category2             = category2;
        this.independent_x_axis    = independent_x_axis;
        this.y_independent         = independent_y_axis;
        this.x_axis                = x_axis;
        this.x_scale               = x_scale;
        this.y_axis                = y_axis;
        this.y_scale               = y_scale;
        this.sort_small_multiples     = sort_small_multiples;
        this.sec_sort_small_multiples = sec_sort_small_multiples;
        
        this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;

        if (shrinkwrap == false &&
            category2.equals(NONE_STR) == false && 
            grid_view) this.label_small_multiples = false; // Turn off individual labeling since rows and columns will have labels

        //
        // First pass is to get all of the values to determine
        // how to map the world coordinatees to local screen 
        // coordinates...
        //
        Iterator<Tablet> it_tab = bs.tabletIterator();
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
                boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, x_axis)    && KeyMaker.tabletCompletesBlank(tablet, y_axis);
          boolean x_is_time            = KeyMaker.isTimeBlank(x_axis),
                  y_is_time            = KeyMaker.isTimeBlank(y_axis);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            // Create keymakers for the category, x-axis, and y-axis
            KeyMaker x_km    = new KeyMaker(tablet, x_axis),
                     y_km    = new KeyMaker(tablet, y_axis);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Grid state - only create once
            if (cat_km != null && cat2_km != null && grid_view && cat_cc == null) {
              cat_cc  = new BundlesCounterContext(bs, count_by, color_by);
              cat2_cc = new BundlesCounterContext(bs, count_by, color_by);
            }

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // Book keeping for interactivity
              bundle_to_skeys.put(bundle, new HashSet<String>());

              // Create the integer keys for the coordinates
              long xs[], ys[];

              if (x_is_time) { xs = new long[1]; xs[0] = x_km.timeStampKey(bundle); }
              else           xs = Utils.toLongs(x_km.intKeys(bundle));

              if (y_is_time) { ys = new long[1]; ys[0] = y_km.timeStampKey(bundle); }
              else           ys = Utils.toLongs(y_km.intKeys(bundle));

              // Setup ideal geometry ratio based on use of time
              if        (x_is_time && y_is_time) { ratio = 1.0;
              } else if (x_is_time)              { ratio = 1.0/3.0;  // height as a multiple of width
              } else if (             y_is_time) { ratio = 3.0/1.0;  // height as a multiple of width
              } else                             { ratio = 1.0;      }

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Grid view
              if (cat_cc  != null && cat_km  != null) { for (int cat_i=0; cat_i <cats.length; cat_i++)  cat_cc. count(bundle, cats [cat_i]);  }
              if (cat2_cc != null && cat2_km != null) { for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) cat2_cc.count(bundle, cats2[cat2_i]); }

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);
                
                  // Book keeping for interactivity
                  bundle_to_skeys.get(bundle).add(cat_key);
                  if (skey_to_bundles.containsKey(cat_key) == false) skey_to_bundles.put(cat_key, new HashSet<Bundle>());
                  skey_to_bundles.get(cat_key).add(bundle);

                  if (cat_map.containsKey(cat_key) == false) {
                    cat_map.put(cat_key, new BundlesCounterContext(bs, count_by, color_by));
                    cat_x_map.put(cat_key, new HashSet<Long>());
                    cat_y_map.put(cat_key, new HashSet<Long>());
                  }

                  // Iterate over the coordinates
                  for (int xs_i=0;xs_i<xs.length;xs_i++) {
                    long x = xs[xs_i]; cat_x_map.get(cat_key).add(x); x_set.add(x);
                    for (int ys_i=0;ys_i<ys.length;ys_i++) {
                      long y = ys[ys_i]; cat_y_map.get(cat_key).add(y); y_set.add(y);
                    }
                  }
                }
              }
                  }
                } else { addToNoMappingSet(tablet); }
              }

        //
        // Determine the size of the small multiples -- calculate the coordinate transformations
        // -- Need the first pass state so that the transforms can be calculated for the second pass
        //
        // Optimize the size of the small multiple
        int sizes[] = { 384, 256, 224, 192, 160, 128, 112, 96, 80, 64, 48 };
        if (cat2_cc != null) {
          // Optimize for grid
          int rows =  count(cat_cc. binIterator()), cols;
          
          if (shrinkwrap) cols = maxRowCount(cat_map.keySet());
          else            cols =  count(cat2_cc.binIterator());

          for (int i=0;i<sizes.length;i++) {
            mult_w = sizes[i]; mult_h = (int) (sizes[i] * ratio) + (this.label_small_multiples ? txt_h + txt_space : 0);
            if (((mult_w*cols) < rc_w) && ((mult_h*rows) < rc_h)) break;
          }
        } else {
          // Optimize for non-grid
          int total = (show_visible ? 1 : 0) + cat_map.keySet().size();

          for (int my_w=384;my_w>=48;my_w--) {
            mult_w = my_w; mult_h = (int) (mult_w * ratio + (this.label_small_multiples ? txt_h + txt_space : 0));
            int mults_in_x = (rc_w - mult_w/3) / mult_w,
                mults_in_y = (rc_h - mult_h/3) / mult_h;
            if (mults_in_x < 1) mults_in_x = 1;
            if (mults_in_y < 1) mults_in_y = 1;
            if (total < mults_in_x * mults_in_y) break;
          }
        }

        // Determine the actual graph width and height
        int graph_w = mult_w - 2 * mult_x_ins;
        int graph_h; if (this.label_small_multiples) graph_h = mult_h - 2 - txt_h - 2 - 2 * mult_y_ins;
                     else                            graph_h = mult_h                 - 2 * mult_y_ins;
        int graph_x = mult_x_ins,           // Coords for the lower left corner of the plot ... (0,0)
            graph_y = mult_y_ins + graph_h;

        global_coord_mapper = new CoordMapper(x_set, x_scale, y_set, y_scale, graph_x, graph_y, graph_w, graph_h);


        Iterator<String> it = cat_map.keySet().iterator(); while (it.hasNext()) {
          String cat = it.next(); 

          // Create variations based on the independence of the axes
          if        (independent_x_axis && y_independent) {
            cat_coord_mapper.put(cat, new CoordMapper(cat_x_map.get(cat), x_scale, 
                                                      cat_y_map.get(cat), y_scale, 
                                                      graph_x, graph_y, graph_w, graph_h));
          } else if (independent_x_axis)                       {
            cat_coord_mapper.put(cat, new CoordMapper(cat_x_map.get(cat), x_scale, 
                                                      y_set,              y_scale, 
                                                      graph_x, graph_y, graph_w, graph_h));
          } else if (                      y_independent) {
            cat_coord_mapper.put(cat, new CoordMapper(x_set,              x_scale, 
                                                      cat_y_map.get(cat), y_scale, 
                                                      graph_x, graph_y, graph_w, graph_h));
          } else                                               {
            cat_coord_mapper.put(cat, global_coord_mapper);
          }
        }

        //
        // Second pass is to perform mapping to screen coordinates
        // and to calculate the counter contexts
        //
              it_tab       = bs.tabletIterator();
        visible_cc   = new BundlesCounterContext(bs, count_by, color_by);
              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
                boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, x_axis)    && KeyMaker.tabletCompletesBlank(tablet, y_axis);
          boolean x_is_time            = KeyMaker.isTimeBlank(x_axis),
                  y_is_time            = KeyMaker.isTimeBlank(y_axis);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            // Create keymakers for the category, x-axis, and y-axis
            KeyMaker x_km   = new KeyMaker(tablet, x_axis),
                     y_km   = new KeyMaker(tablet, y_axis);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // Create the integer keys for the coordinates
              long xs[], ys[];

              if (x_is_time) { xs = new long[1]; xs[0] = x_km.timeStampKey(bundle); }
              else           xs = Utils.toLongs(x_km.intKeys(bundle));

              if (y_is_time) { ys = new long[1]; ys[0] = y_km.timeStampKey(bundle); }
              else           ys = Utils.toLongs(y_km.intKeys(bundle));

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);
                
                  // Iterate over the coordinates
                  for (int xs_i=0;xs_i<xs.length;xs_i++) {
                    long x = xs[xs_i];
                    for (int ys_i=0;ys_i<ys.length;ys_i++) {
                      long y = ys[ys_i];
        
                      CoordMapper mapper = global_coord_mapper;
                      String skey = mapper.makeKey(x,y);
                      visible_cc.count(bundle, skey);

                      skey = cat_coord_mapper.get(cat_key).makeKey(x,y);
                      cat_map.get(cat_key).count(bundle, skey);
                    }
                  }
                }
              }
                  }
                }
              }
      }

      /**
       * Ratio of the width to the height for the small multiple
       */
      double ratio = 1.0;

      /**
       * Calculate the vector representations of each small multiple for a distance metric comparison.
       */
      protected void calculateVectors() {
        if (first_time_error) { System.err.println("Vectors not applicable for xy scatter charts..."); first_time_error = false; }
      }

      private boolean first_time_error = true;

      /**
       * Converts world coordinatees to screen coordinates (small multiple coordinates)
       */
      class CoordMapper {

        /**
         * Linear mapper -- each element is placed based on their linear amount
         */
        class LinearAxisMapper implements MyAxisMapper {
          int base, length; boolean add; long min, max; Map<Long,Integer> map = new HashMap<Long,Integer>();
          public LinearAxisMapper(Set<Long> set, int base, int length, boolean add) {
            this.base = base; this.length = length; this.add = add;
            // Find min and max
            min = Long.MAX_VALUE; max = Long.MIN_VALUE;
            Iterator<Long> it = set.iterator(); while (it.hasNext()) { long l = it.next(); if (l < min) min = l; if (l > max) max = l; }
            long max_minus_min = max - min; if (max_minus_min == 0L) max_minus_min = 1L;
            // Make a map
            it = set.iterator(); while (it.hasNext()) {
              long l     = it.next();
              int  value;
              if (add) value = (int) (base + (length * (l - min)) / (max_minus_min));
              else     value = (int) (base - (length * (l - min)) / (max_minus_min));
              map.put(l,value);
            }
          }
          public int wToS(long w) { return map.get(w); }
        }

        /**
         * Equal scale mapper -- each element gets the same amount of space... in their linear order
         */
        class EqualAxisMapper implements MyAxisMapper {
          int base, length; boolean add; Map<Long,Integer> map = new HashMap<Long,Integer>();
          public EqualAxisMapper(Set<Long> set, int base, int length, boolean add) {
            this.base = base; this.length = length; this.add = add;
            List<Long> sorter = new ArrayList<Long>(); sorter.addAll(set); Collections.sort(sorter);

            if (sorter.size() < 4) {
              int mul; if (add) mul = 1; else mul = -1;
              if        (sorter.size() == 1) {
                map.put(sorter.get(0), base + mul*length/2);
              } else if (sorter.size() == 2) {
                map.put(sorter.get(0), base + mul*1*length/3);
                map.put(sorter.get(1), base + mul*2*length/3);
              } else if (sorter.size() == 3) {
                map.put(sorter.get(0), base + mul*1*length/4);
                map.put(sorter.get(1), base + mul*2*length/4);
                map.put(sorter.get(2), base + mul*3*length/4);
              }
            } else {
              int div = sorter.size(); if (div == 0) div = 1;
              for (int i=0;i<sorter.size();i++) {
                long l = sorter.get(i); 
                if (add) map.put(l, base + (i * length)/div);
                else     map.put(l, base - (i * length)/div);
              }
            }
          }
          public int wToS(long w) { return map.get(w); }
        }

        /**
         * Mapper for the x and y axis
         */
        MyAxisMapper x_axis_mapper, y_axis_mapper;

        public CoordMapper(Set<Long> xset, String xscale, Set<Long> yset, String yscale, int x0, int y0, int w0, int h0) {
          if      (xscale.equals(XY_SCALE_LINEAR_STR)) x_axis_mapper = new LinearAxisMapper(xset, x0, w0, true);
          else if (xscale.equals(XY_SCALE_EQUAL_STR))  x_axis_mapper = new EqualAxisMapper (xset, x0, w0, true);

          if      (yscale.equals(XY_SCALE_LINEAR_STR)) y_axis_mapper = new LinearAxisMapper(yset, y0, y0, false);
          else if (yscale.equals(XY_SCALE_EQUAL_STR))  y_axis_mapper = new EqualAxisMapper (yset, y0, y0, false);
        }

        Map<String,Integer> key_to_sx = new HashMap<String,Integer>(),
                            key_to_sy = new HashMap<String,Integer>();

        /**
         * Make a key for the world x and world y coordinate
         */
        public String makeKey(long wx, long wy) {
          int    sx  = x_axis_mapper.wToS(wx), sy = y_axis_mapper.wToS(wy);
          String key = sx + "," + sy;
          key_to_sx.put(key, sx);
          key_to_sy.put(key, sy);
          return key;
        }

        /**
         * For a previously created key, return the screen x coordinate
         */
        public int keyToSx(String key) { return key_to_sx.get(key); }
        
        /**
         * For a previously created key, return the screen y coordinate
         */
        public int keyToSy(String key) { return key_to_sy.get(key); }
      }

      /**
       * Mapper for the global coordinate transform
       */
      CoordMapper                       global_coord_mapper;

      /**
       * Localized mapper for independent axes
       */
      Map<String,CoordMapper> cat_coord_mapper = new HashMap<String,CoordMapper>();

      /**
       * Category to the x axis elements (for local mapping)
       */
      Map<String,Set<Long>>             cat_x_map = new HashMap<String,Set<Long>>(),

      /**
       * Category to the y axis elements (for local mapping)
       */
                                        cat_y_map = new HashMap<String,Set<Long>>();

      /**
       * All x coordinates seen - for global mapping
       */
      Set<Long>                         x_set = new HashSet<Long>(),

      /**
       * All y coordinates seen - for global mapping
       */
                                        y_set = new HashSet<Long>();

      /**
       * Render an xy small multiple
       */
      protected BufferedImage renderSmallMultiple(String cat, BundlesCounterContext bcc) {
        BufferedImage bi = new BufferedImage(mult_w, mult_h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = null; try {
          g2d = (Graphics2D) bi.getGraphics();
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                RTColorManager.renderVisualizationBackground(bi, g2d);

          // Calculate (or retrieve) the mapping from world to screen space
          CoordMapper mapper = null;
          if (bcc == visible_cc) { mapper = global_coord_mapper; } else { mapper = cat_coord_mapper.get(cat); }

          // draw the small multiples label (if set)
          if (label_small_multiples) {
            g2d.setColor(RTColorManager.getColor("label", "default"));

            // Figure out the label
            String label = "unassigned";
            if      (cat.equals("Visible")) label = cat;
            else if (cat.startsWith("|"))   label = Utils.decFmURL(cat.substring(1,cat.length()));
            else if (cat.endsWith  ("|"))   label = Utils.decFmURL(cat.substring(0,cat.length()-1));
            else if (cat.indexOf   ("|") >= 0) {
              int io = cat.indexOf("|");
              if (shrinkwrap) label = Utils.decFmURL(cat.substring(io+1,cat.length()));
              else            label = Utils.decFmURL(cat.substring(0,io)) + "|" + 
                                      Utils.decFmURL(cat.substring(io+1,cat.length()));
            }

            int txt_w = Utils.txtW(g2d, label);
            g2d.drawString(label, bi.getWidth()/2 - txt_w/2, bi.getHeight() - 2);
          }

          // Plot the data using the counter context and the coordinate mapper
          g2d.setColor(RTColorManager.getColor("data", "default"));
          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String bin = it.next(); int sx = mapper.keyToSx(bin), sy = mapper.keyToSy(bin);
            if (vary_color) g2d.setColor(bcc.binColor(bin));
            if (larger_dots) g2d.fillRect(sx, sy, 2, 2); else g2d.fillRect(sx, sy, 1, 1);
          }

          // draw a box around the small multiple
          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          if (label_small_multiples) g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 2 - txt_h - 2);
          else                       g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 1);

        } finally { if (g2d != null) g2d.dispose(); }
        return bi;
      }
    }

    /**
     * RenderContext implementation for the geospatial small multiples
     */
    public class GeoRenderContext extends SMRC {
      /**
       * Use larger dots in the scatter plot
       */
      boolean larger_dots,

      /**
       * True to make the axes independent in the small mutiples
       */
              independent_axes;

      /**
       * Latitude field
       */
      String  lat,

      /**
       * Longitude field
       */
              lon;

      /**
       * Minimum x coordinate across all of the small multiples
       */
      double  global_min_x = Double.POSITIVE_INFINITY,

      /**
       * Maximum x coordinate across all of the small multiples
       */
              global_max_x = Double.NEGATIVE_INFINITY;

      /**
       * Minimum y coordinate across all of the small multiples
       */
      double  global_min_y = Double.POSITIVE_INFINITY,

      /**
       * Maxmium y coordinate across all of the small multiples
       */
              global_max_y = Double.NEGATIVE_INFINITY;

      /**
       * Minimum x value for a category
       */
      Map<String,Double> cat_x_min = new HashMap<String,Double>(),

      /**
       * Maximum x value for a category
       */
                         cat_x_max = new HashMap<String,Double>(),

      /**
       * Minimum y value for a category
       */
                         cat_y_min = new HashMap<String,Double>(),

      /**
       * Maximum y value for a category
       */
                         cat_y_max = new HashMap<String,Double>();

      /**
       * Constructor
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public GeoRenderContext(short id, Bundles bs, String count_by, String color_by, 
                              boolean show_visible, boolean label_small_multiples, boolean grid_view, boolean vary_color, boolean larger_dots, boolean shrinkwrap,
                              String  category,  String  category2,
                              String  lat0, String  lon0, boolean independent_axes0,
                              String  sort_small_multiples, String  sec_sort_small_multiples,
                              int w, int h) {
        render_id = id; this.bs = bs; 
        this.show_visible          = show_visible;
        this.label_small_multiples = label_small_multiples;
        this.grid_view             = grid_view;
        this.vary_color            = vary_color;
        this.larger_dots           = larger_dots;
        this.shrinkwrap            = shrinkwrap;
        this.category              = category;
        this.category2             = category2;
        this.independent_axes      = independent_axes0;
        this.lat                   = lat0;
        this.lon                   = lon0;
        this.sort_small_multiples     = sort_small_multiples;
        this.sec_sort_small_multiples = sec_sort_small_multiples;
        
        this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;

        if (shrinkwrap == false && 
            category2.equals(NONE_STR) == false && 
            grid_view) this.label_small_multiples = false; // Turn off individual labeling since rows and columns will have labels

        //
        // First pass is to get all of the values to determine
        // how to map the world coordinatees to local screen 
        // coordinates...
        //
        Iterator<Tablet> it_tab = bs.tabletIterator();
              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
                boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, lat)  && KeyMaker.tabletCompletesBlank(tablet, lon);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            // Create keymakers for the category, x-axis, and y-axis
            KeyMaker x_km    = new KeyMaker(tablet, lon),
                     y_km    = new KeyMaker(tablet, lat);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Grid state - only create once
            if (cat_km != null && cat2_km != null && grid_view && cat_cc == null) {
              cat_cc  = new BundlesCounterContext(bs, count_by, color_by);
              cat2_cc = new BundlesCounterContext(bs, count_by, color_by);
            }

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // Book keeping for interactivity
              bundle_to_skeys.put(bundle, new HashSet<String>());

              // Create the integer keys for the coordinates
              String xs[], 
                     ys[]; 
              xs = x_km.stringKeys(bundle); 
              ys = y_km.stringKeys(bundle);

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Grid view
              if (cat_cc  != null && cat_km  != null) { for (int cat_i=0; cat_i <cats.length; cat_i++)  cat_cc. count(bundle, cats [cat_i]);  }
              if (cat2_cc != null && cat2_km != null) { for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) cat2_cc.count(bundle, cats2[cat2_i]); }

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);
                
                  // Book keeping for interactivity
                  bundle_to_skeys.get(bundle).add(cat_key);
                  if (skey_to_bundles.containsKey(cat_key) == false) skey_to_bundles.put(cat_key, new HashSet<Bundle>());
                  skey_to_bundles.get(cat_key).add(bundle);

                  if (cat_map.containsKey(cat_key) == false) {
                    cat_map.put(cat_key, new BundlesCounterContext(bs, count_by, color_by));

                    cat_x_map.put(cat_key, new HashSet<Double>());
                    cat_x_min.put(cat_key, Double.POSITIVE_INFINITY);
                    cat_x_max.put(cat_key, Double.NEGATIVE_INFINITY);

                    cat_y_map.put(cat_key, new HashSet<Double>());
                    cat_y_min.put(cat_key, Double.POSITIVE_INFINITY);
                    cat_y_max.put(cat_key, Double.NEGATIVE_INFINITY);

                  }

                  // Iterate over the coordinates
                  boolean x_valid = false, y_valid = false;
                  for (int xs_i=0;xs_i<xs.length;xs_i++) {
                    if (xs[xs_i].equals(BundlesDT.NOTSET)) {
                    } else { 
                      double x = Double.parseDouble(xs[xs_i]); cat_x_map.get(cat_key).add(x); x_set.add(x); x_valid = true; 
                      if (global_max_x < x) global_max_x = x;
                      if (global_min_x > x) global_min_x = x;
                      if (cat_x_min.get(cat_key) > x) cat_x_min.put(cat_key,x);
                      if (cat_x_max.get(cat_key) < x) cat_x_max.put(cat_key,x);
                    }
                  }
                  for (int ys_i=0;ys_i<ys.length;ys_i++) {
                    if (ys[ys_i].equals(BundlesDT.NOTSET)) {
                    } else { 
                      double y = Double.parseDouble(ys[ys_i]); cat_y_map.get(cat_key).add(y); y_set.add(y); y_valid = true; 
                      if (global_max_y < y) global_max_y = y;
                      if (global_min_y > y) global_min_y = y;
                      if (cat_y_min.get(cat_key) > y) cat_y_min.put(cat_key,y);
                      if (cat_y_max.get(cat_key) < y) cat_y_max.put(cat_key,y);
                    }
                  }
                  if (x_valid == false || y_valid == false) addToNoMappingSet(bundle);
                }
              }
                  }
                } else { addToNoMappingSet(tablet); }
              }

        //
        // Determine the size of the small multiples -- calculate the coordinate transformations
        // -- Need the first pass state so that the transforms can be calculated for the second pass
        //
        // Optimize the size of the small multiple
        int sizes[] = { 256, 192, 128, 112, 96, 80, 64 }; 

        double ratio = (global_max_x - global_min_x)/(global_max_y - global_min_y); // Use the ratio of the entire dataset
        if (Double.isNaN(ratio)) ratio = 1.0;

        if (cat2_cc != null) {
          // Optimize for grid
          int rows =  count(cat_cc. binIterator()), cols;
          
          if (shrinkwrap) cols = maxRowCount(cat_map.keySet());
          else            cols = count(cat2_cc.binIterator());

          for (int i=0;i<sizes.length;i++) {
            mult_w = sizes[i]; mult_h = (int) (sizes[i] * ratio) + (this.label_small_multiples ? txt_h + txt_space : 0);
            if (((mult_w*cols) < rc_w) && ((mult_h*rows) < rc_h)) break;
          }
        } else {
          // Optimize for non-grid
          int total = (show_visible ? 1 : 0) + cat_map.keySet().size();

          for (int my_w=384;my_w>=48;my_w--) {
            mult_w = my_w; mult_h = (int) (mult_w * ratio + (this.label_small_multiples ? txt_h + txt_space : 0));
            int mults_in_x = (rc_w - mult_w/3) / mult_w,
                mults_in_y = (rc_h - mult_h/3) / mult_h;
            if (mults_in_x < 1) mults_in_x = 1;
            if (mults_in_y < 1) mults_in_y = 1;
            if (total < mults_in_x * mults_in_y) break;
          }
        }

        // Determine the actual graph width and height
        int graph_w = mult_w - 2 * mult_x_ins;
        int graph_h; if (this.label_small_multiples) graph_h = mult_h - 2 - txt_h - 2 - 2 * mult_y_ins;
                     else                            graph_h = mult_h                 - 2 * mult_y_ins;

        //
        // Second pass is to perform mapping to screen coordinates
        // and to calculate the counter contexts
        //
              it_tab       = bs.tabletIterator();
        visible_cc   = new BundlesCounterContext(bs, count_by, color_by);
              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
                boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, lon)    && KeyMaker.tabletCompletesBlank(tablet, lat);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            // Create keymakers for the category, x-axis, and y-axis
            KeyMaker x_km   = new KeyMaker(tablet, lon),
                     y_km   = new KeyMaker(tablet, lat);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    Bundle bundle = it_bun.next();

              // Create the integer keys for the coordinates
              String x_strs[] = x_km.stringKeys(bundle),
                     y_strs[] = y_km.stringKeys(bundle);

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);
                
                  // Iterate over the coordinates
                  for (int xs_i=0;xs_i<x_strs.length;xs_i++) {
                    if (x_strs[xs_i].equals(BundlesDT.NOTSET)) continue;
                    double x = Double.parseDouble(x_strs[xs_i]);

                    int sx  = (int) (mult_x_ins + graph_w * (x - cat_x_min.get(cat_key))/(cat_x_max.get(cat_key) - cat_x_min.get(cat_key)));
                    int gsx = (int) (mult_x_ins + graph_w * (x - global_min_x)          /(global_max_x           - global_min_x));
                    if (independent_axes == false) sx = gsx;

                    for (int ys_i=0;ys_i<y_strs.length;ys_i++) {
                      if (y_strs[ys_i].equals(BundlesDT.NOTSET)) continue;
                      double y = Double.parseDouble(y_strs[ys_i]);

                      int sy  = (int) (mult_y_ins + graph_h - graph_h * (y - cat_y_min.get(cat_key))/(cat_y_max.get(cat_key) - cat_y_min.get(cat_key)));
                      int gsy = (int) (mult_y_ins + graph_h - graph_h * (y - global_min_y)          /(global_max_y           - global_min_y));
                      if (independent_axes == false) sy = gsy;

                      String skey = sx + "," + sy; 
                      sx_lu.put(skey,sx); sy_lu.put(skey,sy);     // faster access to the coordinates from the key
                      cat_map.get(cat_key).count(bundle, skey);

                      skey = gsx + "," + gsy;
                      sx_lu.put(skey,gsx); sy_lu.put(skey,gsy);   // faster access to the coordinates from the key
                      visible_cc.count(bundle, skey);
                    }
                  }
                }
              }
            }
          }
        }
      }

      /**
       * Fast lookup between the skey and the screen x coordinate
       */
      Map<String,Integer> sx_lu = new HashMap<String,Integer>(),

      /**
       * Fast lookup between the skey and the screen y coordinate
       */
                          sy_lu = new HashMap<String,Integer>();

      /**
       * Calculate the vector representations of each small multiple for a distance metric comparison.
       */
      protected void calculateVectors() {
        if (first_time_error) { System.err.println("Vectors not applicable for geo charts..."); first_time_error = false; }
      }

      private boolean first_time_error = true;

      /**
       * Category to the x axis elements (for local mapping)
       */
      Map<String,Set<Double>>           cat_x_map = new HashMap<String,Set<Double>>(),

      /**
       * Category to the y axis elements (for local mapping)
       */
                                        cat_y_map = new HashMap<String,Set<Double>>();

      /**
       * All x coordinates seen - for global mapping
       */
      Set<Double>                       x_set = new HashSet<Double>(),

      /**
       * All y coordinates seen - for global mapping
       */
                                        y_set = new HashSet<Double>();

      /**
       * Render an xy small multiple
       */
      protected BufferedImage renderSmallMultiple(String cat, BundlesCounterContext bcc) {
        BufferedImage bi = new BufferedImage(mult_w, mult_h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = null; try {
          g2d = (Graphics2D) bi.getGraphics();
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                RTColorManager.renderVisualizationBackground(bi, g2d);

          // draw the small multiples label (if set)
          if (label_small_multiples) {
            g2d.setColor(RTColorManager.getColor("label", "default"));

            // Figure out the label
            String label = "unassigned";
            if      (cat.equals("Visible")) label = cat;
            else if (cat.startsWith("|"))   label = Utils.decFmURL(cat.substring(1,cat.length()));
            else if (cat.endsWith  ("|"))   label = Utils.decFmURL(cat.substring(0,cat.length()-1));
            else if (cat.indexOf   ("|") >= 0) {
              int io = cat.indexOf("|");

              // For shrinkwrapped, you only need the second part of the concatenated strings
              if (shrinkwrap) label = Utils.decFmURL(cat.substring(io+1,cat.length()));
              else            label = Utils.decFmURL(cat.substring(0,io)) + "|" + 
                                      Utils.decFmURL(cat.substring(io+1,cat.length()));
            }

            int txt_w = Utils.txtW(g2d, label);
            g2d.drawString(label, bi.getWidth()/2 - txt_w/2, bi.getHeight() - 2);
          }

          // Actual rendering
          g2d.setColor(RTColorManager.getColor("data", "default"));
          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String skey = it.next(); int sx = sx_lu.get(skey), sy = sy_lu.get(skey);
            if (vary_color) g2d.setColor(bcc.binColor(skey));
            if (larger_dots) g2d.fillRect(sx, sy, 2, 2); else g2d.fillRect(sx, sy, 1, 1);
          }

          // draw a box around the small multiple
          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          if (label_small_multiples) g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 2 - txt_h - 2);
          else                       g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 1);

        } finally { if (g2d != null) g2d.dispose(); }
        return bi;
      }
    }

    /**
     * RenderContext implementation for the geospatial small multiples
     */
    public class CountryRenderContext extends SMRC {
      /**
       * Use independend axies
       */
      boolean independent_axes;

      /**
       * Country code field
       */
      String  cc_field,

      /**
       * Country color scale ... for magnitude coloring...
       */
              cc_cs;

      /**
       * Worldmap information and shapes
       */
      WorldMap world_map = null; 

      /**
       * Constructor
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public CountryRenderContext(short id, Bundles bs, String count_by, String color_by, 
                                  boolean show_visible, boolean label_small_multiples, boolean grid_view, boolean vary_color, boolean shrinkwrap,
                                  String  category,  String  category2,
                                  String  cc_field0, String cc_cs0, boolean independent_axes0,
                                  String  sort_small_multiples, String  sec_sort_small_multiples,
                                  int w, int h) {
        render_id = id; this.bs = bs; 
        this.show_visible             = show_visible;
        this.label_small_multiples    = label_small_multiples;
        this.grid_view                = grid_view;
        this.vary_color               = vary_color;
        this.shrinkwrap               = shrinkwrap;
        this.category                 = category;
        this.category2                = category2;
        this.cc_field                 = cc_field0;
        this.cc_cs                    = cc_cs0;
        this.independent_axes         = independent_axes0;
        this.sort_small_multiples     = sort_small_multiples;
        this.sec_sort_small_multiples = sec_sort_small_multiples;
        
        this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;

        if (shrinkwrap == false && 
            category2.equals(NONE_STR) == false && 
            grid_view) this.label_small_multiples = false; // Turn off individual labeling since rows and columns will have labels

        // distinct rows and columns
        Set<String> rows = new HashSet<String>(), cols = new HashSet<String>();

        //
        // First pass is to get all of the values to determine
        // how to map the world coordinatees to local screen 
        // coordinates...
        //
        Iterator<Tablet> it_tab = bs.tabletIterator();
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
          boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, cc_field);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Grid state - only create once
            if (cat_km != null && cat2_km != null && grid_view && cat_cc == null) {
              cat_cc  = new BundlesCounterContext(bs, count_by, color_by);
              cat2_cc = new BundlesCounterContext(bs, count_by, color_by);
            }

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle = it_bun.next();

              // Book keeping for interactivity
              bundle_to_skeys.put(bundle, new HashSet<String>());

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Grid view
              if (cat_cc  != null && cat_km  != null) { for (int cat_i=0; cat_i <cats.length; cat_i++)  cat_cc. count(bundle, cats [cat_i]);  }
              if (cat2_cc != null && cat2_km != null) { for (int cat2_i=0;cat2_i<cats2.length;cat2_i++) cat2_cc.count(bundle, cats2[cat2_i]); }

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);

                  cols.add(cat2); rows.add(cat);
                
                  // Book keeping for interactivity
                  bundle_to_skeys.get(bundle).add(cat_key);
                  if (skey_to_bundles.containsKey(cat_key) == false) skey_to_bundles.put(cat_key, new HashSet<Bundle>());
                  skey_to_bundles.get(cat_key).add(bundle);

                  if (cat_map.containsKey(cat_key) == false) {
                    cat_map.put(cat_key, new BundlesCounterContext(bs, count_by, color_by));
                  }
                }
              }
            }
          } else { addToNoMappingSet(tablet); }
        }

        //
        // Determine the size of the small multiples
        //
        int sizes[] = { 384, 256, 192, 160, 128, 96 }; boolean fits = false; int fits_i = 0;
        while (fits == false && fits_i < sizes.length) {
          // multiple size
          mult_w = sizes[fits_i]; mult_h = (3*mult_w)/4; fits_i++;

          // actual size w/ label
          int graph_w = mult_w - 2 * mult_x_ins;
          int graph_h; if (this.label_small_multiples) graph_h = mult_h - 2 - txt_h - 2 - 2 * mult_y_ins;
                       else                            graph_h = mult_h                 - 2 * mult_y_ins;

          //
          // how many fit? ... grid version
          //
          if (category2.equals(NONE_STR) == false) {
            fits = ((cols.size() * mult_w) < getRCWidth()) &&
                   ((rows.size() * mult_h) < getRCHeight());

          //
          // how many fit? ... non-grid version
          // ... do a very simple test with area sizes
          //
          } else {
            int area_calc = (getRCWidth() * getRCHeight())/(mult_w*mult_h);
            if (area_calc >= cat_map.keySet().size()) fits = true;
          }
        }

        // Create the worldmap structure...  choose the resolution based on the multiple size
        try { 
          if (mult_w >= 192) world_map = MadeHexMaps.getInstance(MadeHexMaps.ShapeQuality.SMALL_MULTIPLES_50M_MEDRES); 
          else               world_map = MadeHexMaps.getInstance(MadeHexMaps.ShapeQuality.SMALL_MULTIPLES_50M_LOWRES); 
        } catch (IOException ioe) { System.err.println("IOException: " + ioe); }

        //
        // Second pass is to perform mapping to screen coordinates
        // and to calculate the counter contexts
        //
        it_tab       = bs.tabletIterator();
        visible_cc   = new BundlesCounterContext(bs, count_by, color_by);
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          // Can the tablet contribute?
          boolean tablet_can_count     = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          boolean tablet_fulfills_axes = KeyMaker.tabletCompletesBlank(tablet, cc_field);

          if (tablet_can_count && tablet_fulfills_axes && KeyMaker.tabletCompletesBlank(tablet, category)) {

            // Create keymakers for the category and cc field
            KeyMaker cc_km   = new KeyMaker(tablet, cc_field);

            KeyMaker cat_km = null, cat2_km = null;
            if (KeyMaker.tabletCompletesBlank(tablet, category))  cat_km  = new KeyMaker(tablet, category);
            if (category2.equals(NONE_STR) == false && 
                KeyMaker.tabletCompletesBlank(tablet, category2)) cat2_km = new KeyMaker(tablet, category2);

            // Iterate over the bundles
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle = it_bun.next();

              // Create the integer keys for the coordinates
              String cc_strs[] = cc_km.stringKeys(bundle);

              // Create the categories for this data element
              String  cats[] = { "" }, cats2[] = { "" };
              if (cat_km  != null) cats  = cat_km. stringKeys(bundle);
              if (cat2_km != null) cats2 = cat2_km.stringKeys(bundle);

              // Iterate over the categories
              for (int cats_i=0;cats_i<cats.length;cats_i++) {
                String cat = cats[cats_i]; 
                for (int cats2_i=0;cats2_i<cats2.length;cats2_i++) {
                  String cat2 = cats2[cats2_i];

                  String cat_key = Utils.encToURL(cat) + "|" + Utils.encToURL(cat2);
                
                  // Iterate over the country code(s)
                  for (int cc_i=0;cc_i<cc_strs.length;cc_i++) {
                    if (cc_strs[cc_i].equals(BundlesDT.NOTSET)) continue;

                    String a3code = world_map.a3code(cc_strs[cc_i]);

                    cat_map.get(cat_key).count(bundle, a3code);
                    visible_cc.count(bundle, a3code);
                  }
                }
              }
            }
          }
        }

        // Find the maximum (if not independent...)
        if (independent_axes == false) {
          Iterator<String> it_cat = cat_map.keySet().iterator(); while (it_cat.hasNext()) {
            String cat_key = it_cat.next(); BundlesCounterContext bcc = cat_map.get(cat_key); if (bcc == visible_cc) continue;
            if (bcc.totalMaximum() > total_maximum) total_maximum = bcc.totalMaximum();
          }
        }
      }

      /**
       * totalMaximum() across all of the small multiples...
       * ... used for dependent axes option
       */
      double total_maximum = 1.0;

      /**
       * Calculate the vector representations of each small multiple for a distance metric comparison.
       */
      protected void calculateVectors() {
        Iterator<String> it_cat = cat_map.keySet().iterator(); while (it_cat.hasNext()) {
          String cat = it_cat.next(); BundlesCounterContext bcc = cat_map.get(cat);

          // Representation by bin and normalized value
          Map<String,Double> norms = new HashMap<String,Double>();
          double total = bcc.totalCounts(); // total sum of all the slices

          // Every slice of the pie chart
          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String bin = it.next(); double n = bcc.total(bin)/total; norms.put(bin,n);
          }

          // Save it for comparison
          cat_vec.put(cat,norms);
        }
      }

      /**
       * Render an xy small multiple
       */
      protected BufferedImage renderSmallMultiple(String cat, BundlesCounterContext bcc) {
        BufferedImage bi = new BufferedImage(mult_w, mult_h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = null; try {
          g2d = (Graphics2D) bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(bi, g2d);

          // draw the small multiples label (if set)
          if (label_small_multiples) {
            g2d.setColor(RTColorManager.getColor("label", "default"));

            // Figure out the label
            String label = "unassigned";
            if      (cat.equals("Visible")) label = cat;
            else if (cat.startsWith("|"))   label = Utils.decFmURL(cat.substring(1,cat.length()));
            else if (cat.endsWith  ("|"))   label = Utils.decFmURL(cat.substring(0,cat.length()-1));
            else if (cat.indexOf   ("|") >= 0) {
              int io = cat.indexOf("|");

              if (shrinkwrap) label = Utils.decFmURL(cat.substring(io+1,cat.length()));
              else            label = Utils.decFmURL(cat.substring(0,io)) + "|" + 
                                      Utils.decFmURL(cat.substring(io+1,cat.length()));
            }

            int txt_w = Utils.txtW(g2d, label);
            g2d.drawString(label, bi.getWidth()/2 - txt_w/2, bi.getHeight() - 2);
          }

          // Figure out the colorscale
          // ... mirrors the RTGeoHistogramPanel version...
          ColorScale cs = null;
          if        (cc_cs.equals(DEFAULT_COLORSCALE_STR)) { cs = null;
          } else if (cc_cs.equals(GRAY_COLORSCALE_STR))    { cs = RTColorManager.getContinuousColorScale(); // new BrightGrayColorScale();
          } else if (cc_cs.equals(BREWER_COLORSCALE_STR))  { cs = new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 9, 0);
          } else if (cc_cs.equals(SPECTRA_COLORSCALE_STR)) { cs = new AbridgedSpectra(); }

          // Actual rendering
          int adj_h = bi.getHeight(); if (label_small_multiples) adj_h = adj_h - txt_h - 2;
          g2d.setColor(RTColorManager.getColor("data", "default"));
          Iterator<String> it = bcc.binIterator(); while (it.hasNext()) {
            String a3 = it.next();

            if      (cs == null && vary_color) g2d.setColor(bcc.binColor(a3));
            else if (cs != null) {
              if (bcc == visible_cc) { g2d.setColor(cs.at((float) bcc.totalNormalized(a3)));
              } else {
                if (independent_axes)  g2d.setColor(cs.at((float) bcc.totalNormalized(a3)));
                else                   g2d.setColor(cs.at((float) (bcc.total(a3)/total_maximum)));
              }
            }

            Set<Shape> shapes = world_map.getCountryShapes(a3); 
            if (shapes != null && shapes.size() > 0) {
              Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
                Shape wshape = it_shape.next(); Shape sshape = world_map.worldShapeToScreenShape(wshape, bi.getWidth(), adj_h);
                g2d.fill(sshape);
                g2d.draw(sshape);
              }
            }
          }

          // draw a box around the small multiple
          g2d.setColor(RTColorManager.getColor("axis", "minor"));
          if (label_small_multiples) g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 2 - txt_h - 2);
          else                       g2d.drawRect(0,0,bi.getWidth()-1,bi.getHeight() - 1);

        } finally { if (g2d != null) g2d.dispose(); }
        return bi;
      }
    }
  }

  protected static Map<String,String> mon_map; static {
    mon_map = new HashMap<String,String>();
    mon_map.put("Jan", "01"); mon_map.put("Feb", "02"); mon_map.put("Mar", "03"); mon_map.put("Apr", "04"); mon_map.put("May", "05"); mon_map.put("Jun", "06");
    mon_map.put("Jul", "07"); mon_map.put("Aug", "08"); mon_map.put("Sep", "09"); mon_map.put("Oct", "10"); mon_map.put("Nov", "11"); mon_map.put("Dec", "12");
  }

  /**
   * Sort list according to it's natural order... for example, days would be "Sunday, Monday, Tuesday, ..."
   * ... more complicated than I would like but handles the case where not all of the days or months are
   * in the list...
   */
  protected List<MySorter> naturalSort(List<MySorter> sorter) {
    // Put into a map
    Map<String,MySorter> map = new HashMap<String,MySorter>();
    for (int i=0;i<sorter.size();i++) map.put(sorter.get(i).key, sorter.get(i));
    
    String days[]  = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" },
           daysp[] = { "Sun|","Mon|","Tue|","Wed|","Thu|","Fri|","Sat|"},  // ... sigh ... dumb hack for the pipe version :(
           mons[]  = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
    int    day_match     = 0,
           dayp_match    = 0,
           mon_match     = 0,
           no_day_match  = 0,
           no_dayp_match = 0,
           no_mon_match  = 0;

    // Determine which natural ordering applies
    for (int i=0;i<days.length;i++)  if (map.keySet().contains(days[i]))  day_match++;  else no_day_match++;
    for (int i=0;i<daysp.length;i++) if (map.keySet().contains(daysp[i])) dayp_match++; else no_dayp_match++;
    for (int i=0;i<mons.length;i++)  if (map.keySet().contains(mons[i]))  mon_match++;  else no_mon_match++;

    // System.err.println("day_match = " + day_match + " | no_day_match = " + no_day_match);
    // for (int i=0;i<sorter.size();i++) System.err.println("  " + sorter.get(i).key);

    if ((day_match  > 0 && no_day_match  == 0) || 
        (dayp_match > 0 && no_dayp_match == 0) ||
        (mon_match  > 0 && no_mon_match  == 0)) {
      String order[] = null; 

      if      (day_match  > 0) order = days; 
      else if (dayp_match > 0) order = daysp;
      else if (mon_match  > 0) order = mons;

      if (order != null) {
        List<MySorter> list = new ArrayList<MySorter>();
        for (int i=0;i<order.length;i++) if (map.containsKey(order[i])) list.add(map.get(order[i]));
        return list;
      }
    }

    return sorter;
  }
}

abstract class MySorter implements Comparable<MySorter> {
  String key; BundlesCounterContext bcc;
  public MySorter(String key, BundlesCounterContext bcc) { this.key = key; this.bcc = bcc; }
}

class AlphaSorter extends MySorter {
  public AlphaSorter(String key, BundlesCounterContext bcc) { super(key,bcc); }
  public int compareTo(MySorter other) {
    return key.compareTo(other.key);
  }
}

class RecordsSorter extends MySorter {
  public RecordsSorter(String key, BundlesCounterContext bcc) { super(key,bcc); }
  public int compareTo(MySorter other) {
    if      (other.bcc.totalRecords() > bcc.totalRecords()) return  1;
    else if (other.bcc.totalRecords() < bcc.totalRecords()) return -1;
    else                                                    return  0;
  }
}

class CountSorter extends MySorter {
  public CountSorter(String key, BundlesCounterContext bcc) { super(key,bcc); }
  public int compareTo(MySorter other) {
    if      (other.bcc.totalCounts() > bcc.totalCounts()) return  1;
    else if (other.bcc.totalCounts() < bcc.totalCounts()) return -1;
    else                                                  return  0;
  }
}

class DoubleSorter extends MySorter {
  double d;
  public DoubleSorter(String key, BundlesCounterContext bcc, double d) { super(key,bcc); this.d = d; } 
  public int compareTo(MySorter other) {
    if      (((DoubleSorter) other).d > d) return  1;
    else if (((DoubleSorter) other).d < d) return -1;
    else                                   return  0;
  }
}

interface MyAxisMapper { public int wToS(long w); }

