/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.Graphics2D;
import java.awt.Shape;

import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;

import javax.swing.JCheckBoxMenuItem;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.EntityExtractor;
import racetrack.util.SubText;

import racetrack.visualization.RTColorManager;

/**
 * Class that implements a dynamically update reports views.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTWordCloudPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -2231127917647471369L;

  /**
   * Include focus items -- focus items occur under the mouse
   */
  JCheckBoxMenuItem include_focus_cbmi,

  /**
   * Include selects -- selects occur when the user actively selects items
   */
                    include_selection_cbmi,

  /**
   * Include report extracts -- for reports that are focused/selected, extract the report text
   */
                    include_report_extracts_cbmi,

  /**
   * Only include known types -- all other words/tokens will be ignored
   */
                    only_include_known_types_cbmi,

  /**
   * For unstructured text, the text will first be tokenized based on whitespace characters
   */
                    tokenize_utext_cbmi;

  /**
   * Construct the correlation panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTWordCloudPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   

    // Make the GUI
    add("Center", component = new RTWordCloudComponent());

    // Add the popup menu items
    getRTPopupMenu().add(include_focus_cbmi            = new JCheckBoxMenuItem("Include Focus", true));
    getRTPopupMenu().add(include_selection_cbmi        = new JCheckBoxMenuItem("Include Selects", true));
    getRTPopupMenu().add(include_report_extracts_cbmi  = new JCheckBoxMenuItem("Include Report Extracts", true));
    getRTPopupMenu().addSeparator();
    getRTPopupMenu().add(only_include_known_types_cbmi = new JCheckBoxMenuItem("Only Include Known Types", true));
    getRTPopupMenu().add(tokenize_utext_cbmi           = new JCheckBoxMenuItem("Tokenize Unstructured Text", false));
  }

  /**
   * Include Focus as part of the word cloud.  Focus is what's under the mouse.
   *
   *@return true to include focus
   */
  public boolean includeFocus() { return include_focus_cbmi.isSelected(); }

  /**
   * Include Selection as part of the word cloud.  Selections are actively selected by the user (not just a mouse over).
   *
   *@return true to include selection
   */
  public boolean includeSelection() { return include_selection_cbmi.isSelected(); }

  /**
   * When a report title is selected/focused, include the extracted entities from that report.
   *
   *@return true to include report extractions
   */
  public boolean includeReportExtracts() { return include_report_extracts_cbmi.isSelected(); }

  /**
   * Only include known types in the word cloud.
   *
   *@return true to only include known types
   */
  public boolean onlyIncludeKnownTypes() { return only_include_known_types_cbmi.isSelected(); }

  /**
   * Tokenize any unstructured text strings passed in.
   *
   *@return true to tokenize unstructured text
   */
  public boolean tokenizeUnstructuredText() { return tokenize_utext_cbmi.isSelected(); }

  /**
   * Override the parent method and capture the focus entities.
   */
  @Override
  public void updateFocusEntities(Set<String> focus_entities) { if (includeFocus()) tabulate(focus_entities, true); }

  /**
   * Override the parent method and capture the new selected entities.
   */
  @Override
  public void updateSelection() { if (includeSelection()) tabulate(getRTParent().getSelectedEntities(), true); }

  /**
   * Entity counts - basis for the word map
   */
  Map<String,Integer> entity_count = new HashMap<String,Integer>();

  /**
   * Tabulate new updated entities.  Request a re-render...
   *
   *@param set        new entities
   *@param first_pass first pass through this method
   */
  protected void tabulate(Set<String> set, boolean first_pass) {
    Iterator<String> it = set.iterator(); while (it.hasNext()) {
      String str = it.next(); BundlesDT.DT datatype = BundlesDT.getEntityDataType(str);

      //
      // Approved entities for the word cloud
      //
      if (datatype == BundlesDT.DT.IPv4       || datatype == BundlesDT.DT.IPv6       ||
          datatype == BundlesDT.DT.DOMAIN     || datatype == BundlesDT.DT.MACADDRESS ||
          datatype == BundlesDT.DT.EMAIL      || datatype == BundlesDT.DT.MD5) {
        if (entity_count.containsKey(str) == false) entity_count.put(str, 1);
        else                                        entity_count.put(str, entity_count.get(str) + 1);
      } 

      //
      // Tokenizer the text... and feed it back into this method
      //
      if (first_pass && datatype == BundlesDT.DT.UTEXT && tokenizeUnstructuredText()) {
        List<SubText> subtexts = EntityExtractor.list(str); HashSet<String> new_set = new HashSet<String>();
        Iterator<SubText> it_subtext = subtexts.iterator(); while (it_subtext.hasNext()) { new_set.add(it_subtext.next().toString()); }
        tabulate(new_set, false);
      }

      //
      // Get the report text, tokenize it, and feed it back into this method
      //
      if (first_pass && datatype == BundlesDT.DT.UTEXT && includeReportExtracts() && getRTParent().isReportTitle(str)) {
        Set<String> texts = getRTParent().getReportText(str);
        Iterator<String> it_text = texts.iterator(); while (it_text.hasNext()) {
          String text = it_text.next(); List<SubText> subtexts = EntityExtractor.list(text); HashSet<String> new_set = new HashSet<String>();
          Iterator<SubText> it_subtext = subtexts.iterator(); while (it_subtext.hasNext()) { new_set.add(it_subtext.next().toString()); }
          tabulate(new_set, false);
        }
      }
    }
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "wordcloud"; }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTWordCloudPanel";
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTWordCloudPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTWordCloudPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * {@link JComponent} implementing the correlation matrix.
   */
  public class RTWordCloudComponent extends RTComponent {
    private static final long serialVersionUID = 322413271127728861L;
    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      return shapes; }
    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      return set; }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      // Basics...
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy();

      // Create the render context based on the user's parameters
      RenderContext myrc = new RenderContext(id, bs, count_by, color_by, getWidth(), getHeight());
      return myrc;
    }
    
    /**
     * RenderContext implementation for the correlation matrix
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by;

      /**
       * Lookup for a record to the string keys
       */
      Map<Bundle,Set<String>> bundle_to_skeys = new HashMap<Bundle,Set<String>>();

      /**
       * Lookup for a string key to the bundles
       */
      Map<String,Set<Bundle>> skey_to_bundles = new HashMap<String,Set<Bundle>>();

      /**
       * Lookup for a string key to the geometry
       */
      Map<String,Rectangle2D> skey_to_geom    = new HashMap<String,Rectangle2D>();

      /**
       * Lookup for a geometry to a string key
       */
      Map<Rectangle2D,String> geom_to_skey    = new HashMap<Rectangle2D,String>();

      /**
       * Construct the rendering context for the day matrix
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_by           color option based on global settings
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
              this.count_by           = count_by;
              this.color_by           = color_by;
              Iterator<Tablet> it_tab = bs.tabletIterator();
              while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
                boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);
          if (tablet.hasTimeStamps() && tablet_can_count) {
            Iterator<Bundle> it_bun = tablet.bundleIterator();
                  while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                    /* Bundle bundle     = */ it_bun.next();
                  }
                } else { addToNoMappingSet(tablet); }
              }
      }

      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }
      BufferedImage base_bi = null;
      @Override
      public BufferedImage getBase() { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }
  }
}

