/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;

import java.awt.geom.AffineTransform;
import java.awt.geom.PathIterator;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.QuickStats;
import racetrack.util.QuickStatsFloat;
import racetrack.util.Utils;

import racetrack.visualization.AbridgedSpectra;
import racetrack.visualization.BrewerColorScale;
import racetrack.visualization.BrightGrayColorScale;
import racetrack.visualization.ColorScale;
import racetrack.visualization.MadeHexMaps;
import racetrack.visualization.RTColorManager;
import racetrack.visualization.WorldMap;
import racetrack.visualization.WorldHexMap;

/**
 * Class that implemens a geospatial histogram.  A
 * geospatial histogram displays histogram information
 * as color intentensities that correspond to the
 * related country shape.
 *
 */
public class RTGeoHistoPanel extends RTPanel {
  /**
   *
   */
  private static final long serialVersionUID = -6260147185019948463L;

  /**
   * {@link JComboBox} member to determine which field(s)
   * to use for the geospatial contributions for each
   * bundle/record.
   */
  JComboBox<String>    geolu_cb;

  /**
   * Checkbox menu item to determine a linear (or logarithmic)
   * colorscale will be used.
   */
  JCheckBoxMenuItem    linear_color_cbmi;

  /**
   * Magnitude colording checkbox
   */
  JCheckBox            magnitude_coloring_cb;

  /**
   * Colorscales
   */
  JRadioButtonMenuItem cs_default_rbmi,
                       cs_gray_rbmi,
                       cs_brewer_rbmi,
                       cs_spectra_rbmi;

  /**
   * Variations on the stat calculated
   */
  JRadioButtonMenuItem stat_sum_rbmi,
                       stat_median_rbmi,
                       stat_average_rbmi,
                       stat_min_rbmi,
                       stat_max_rbmi,
                       stat_stdev_rbmi;

  /**
   * Shapes for the world ... and also country code lookups
   */
  WorldMap world_map;

  /**
   * Construct the geospatial histogram panel with the specified parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt application parent
   */
  public RTGeoHistoPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt)      { 
    super(win_type,win_pos,win_uniq,rt);   

    //
    try { world_map = MadeHexMaps.getInstance(MadeHexMaps.ShapeQuality.REGULAR_50M_HIGHRES); } catch (IOException ioe) {
      System.err.println("RTGeoHistoPanel():  Cannot Load MadeHexMaps ... trying WorldHexMap instead...");
      System.err.println("  IOException: " + ioe);
      ioe.printStackTrace(System.err);
      world_map = new WorldHexMap();
    }


    // Make the GUI
    add("Center",  component = new RTGeoHistoComponent());
    JPanel south = new JPanel(new FlowLayout());
    south.add(magnitude_coloring_cb = new JCheckBox("Magnitude Coloring", true)); defaultListener(magnitude_coloring_cb);
    south.add(new JLabel("GeoLU"));
    south.add(geolu_cb = new JComboBox<String>()); defaultListener(geolu_cb);
    add("South", south);
    // Update the menu
    getRTPopupMenu().add(linear_color_cbmi    = new JCheckBoxMenuItem("Linear Color",true));  defaultListener(linear_color_cbmi);
    linear_color_cbmi.setEnabled(false); // no confidence at this point that the log-scale-based coloring is working

    getRTPopupMenu().addSeparator();

    ButtonGroup bg = new ButtonGroup();
    getRTPopupMenu().add(stat_sum_rbmi        = new JRadioButtonMenuItem("Sum", true));  bg.add(stat_sum_rbmi);     defaultListener(stat_sum_rbmi);
    getRTPopupMenu().add(stat_median_rbmi     = new JRadioButtonMenuItem("Median"));     bg.add(stat_median_rbmi);  defaultListener(stat_median_rbmi);
    getRTPopupMenu().add(stat_average_rbmi    = new JRadioButtonMenuItem("Average"));    bg.add(stat_average_rbmi); defaultListener(stat_average_rbmi);
    getRTPopupMenu().add(stat_min_rbmi        = new JRadioButtonMenuItem("Min"));        bg.add(stat_min_rbmi);     defaultListener(stat_min_rbmi);
    getRTPopupMenu().add(stat_max_rbmi        = new JRadioButtonMenuItem("Max"));        bg.add(stat_max_rbmi);     defaultListener(stat_max_rbmi);
    getRTPopupMenu().add(stat_stdev_rbmi      = new JRadioButtonMenuItem("StDev"));      bg.add(stat_stdev_rbmi);   defaultListener(stat_stdev_rbmi);

    getRTPopupMenu().addSeparator();

    bg = new ButtonGroup();
    getRTPopupMenu().add(cs_default_rbmi      = new JRadioButtonMenuItem("Default Colorscale", true)); bg.add(cs_default_rbmi); defaultListener(cs_default_rbmi);
    getRTPopupMenu().add(cs_gray_rbmi         = new JRadioButtonMenuItem("Gray Colorscale"));          bg.add(cs_gray_rbmi);    defaultListener(cs_gray_rbmi);
    getRTPopupMenu().add(cs_brewer_rbmi       = new JRadioButtonMenuItem("Brewer Colorscale"));        bg.add(cs_brewer_rbmi);  defaultListener(cs_brewer_rbmi);
    getRTPopupMenu().add(cs_spectra_rbmi      = new JRadioButtonMenuItem("Spectra"));                  bg.add(cs_spectra_rbmi); defaultListener(cs_spectra_rbmi);

    // Update the panels
    updateBys();
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "geohisto"; }

  /**
   * Update the dropdown boxes with new fields that are geospatially enabled.
   * Note that this method requires specific naming conventions for the fields.
   */
  @Override
  public void updateBys() {
    Object sel = geolu_cb.getSelectedItem();
    geolu_cb.removeAllItems();
    String strs[] = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals());
    for (int i=0;i<strs.length;i++) { if (Utils.countryCodeField(strs[i])) geolu_cb.addItem(strs[i]); }
    if (sel == null) {
      if (geolu_cb.getItemCount() > 0) geolu_cb.setSelectedIndex(0); 
    } else geolu_cb.setSelectedItem(sel);
  }

  /**
   * Return the field to bin (geolocate) the records by.
   *
   *@return field for binning
   */
  public String binBy() { return (String) geolu_cb.getSelectedItem(); }

  /**
   * Set the field to bin (geolocate) the records by.
   *
   *@param str field to bin by
   */
  public void   binBy(String str) { geolu_cb.setSelectedItem(str); }

  /**
   * Linear color selection.
   *
   *@return  true for linear coloring, false for logarithmic coloring
   */
  public boolean    linearColor()                { return linear_color_cbmi.isSelected();    }

  /**
   * Linear color configuration.
   *
   *@param b true for linear coloring, false for logarithmic coloring
   */
  public void       linearColor(boolean b)       { linear_color_cbmi.setSelected(b);         }

  /**
   * Determines if sets should be counted by their set magnitude or
   * by their set elements (e.g., single color if all elements agree, multi
   * color otherwise).
   */
  public boolean    magnitudeColoring()          { return magnitude_coloring_cb.isSelected();    }

  /**
   *
   */
  public void       magnitudeColoring(boolean b) { magnitude_coloring_cb.setSelected(b);         }

  /**
   * Return the calculated statistic.
   *
   *@return statistic to calculate
   */
  public QuickStats.STAT calculateStat() {
    if      (stat_sum_rbmi.    isSelected()) return QuickStats.STAT.SUM;
    else if (stat_median_rbmi. isSelected()) return QuickStats.STAT.MEDIAN;
    else if (stat_average_rbmi.isSelected()) return QuickStats.STAT.AVERAGE;
    else if (stat_min_rbmi.    isSelected()) return QuickStats.STAT.MIN;
    else if (stat_max_rbmi.    isSelected()) return QuickStats.STAT.MAX;
    else if (stat_stdev_rbmi.  isSelected()) return QuickStats.STAT.STDEV;
    else                                     return QuickStats.STAT.SUM;
  }

  /** 
   * Set the calculated statistics.
   *
   *@param str string version of the statistic enumeration
   */
  public void calculateStat(String str) {
    if      (str.equals("" + QuickStats.STAT.SUM))     stat_sum_rbmi.    setSelected(true);
    else if (str.equals("" + QuickStats.STAT.MEDIAN))  stat_median_rbmi. setSelected(true);
    else if (str.equals("" + QuickStats.STAT.AVERAGE)) stat_average_rbmi.setSelected(true);
    else if (str.equals("" + QuickStats.STAT.MIN))     stat_min_rbmi.    setSelected(true);
    else if (str.equals("" + QuickStats.STAT.MAX))     stat_max_rbmi.    setSelected(true);
    else if (str.equals("" + QuickStats.STAT.STDEV))   stat_stdev_rbmi.  setSelected(true);
    else System.err.println("Do Not Understand Stat \"" + str + "\" in RTGeoHistoPanel.calculateStat()");
  }

  /**
   * Get the actual colorscale that is specified by the user.
   * ... mirrored in the RTSmallMultiplesPanel...
   *
   *@return color scale based on menu selection
   */
  public ColorScale getColorScale() {
    if       (cs_gray_rbmi.   isSelected()) return new BrightGrayColorScale();
    else if  (cs_brewer_rbmi. isSelected()) return new BrewerColorScale(BrewerColorScale.BrewerType.SEQUENTIAL, 9, 0);
    else if  (cs_spectra_rbmi.isSelected()) return new AbridgedSpectra();
    else                                    return RTColorManager.getContinuousColorScale();
  }

  /**
   * Return a string describing the selected color scale.
   *
   *@return color scale description
   */
  public String colorScale() {
    if      (cs_default_rbmi.isSelected()) return "default";
    else if (cs_gray_rbmi   .isSelected()) return "gray";
    else if (cs_brewer_rbmi .isSelected()) return "brewer";
    else if (cs_spectra_rbmi.isSelected()) return "spectra";
    else                                   return "default";
  }

  /**
   * Set the color scale.
   *
   *@param str string describing the colorscale
   */
  public void colorScale(String str) {
    if      (str.equals("default"))  cs_default_rbmi.setSelected(true);
    else if (str.equals("gray"))     cs_gray_rbmi.setSelected(true);
    else if (str.equals("brewer"))   cs_brewer_rbmi.setSelected(true);
    else if (str.equals("spectra"))  cs_spectra_rbmi.setSelected(true);
    else                             cs_default_rbmi.setSelected(true);
  }

  /**
   * Get the configuration for this panel.  Planned to be used for bookmarking.
   *
   *@return string representation of this configuration
   */
  public String       getConfig    ()           { 
    return "RTGeoHistoPanel"                              + BundlesDT.DELIM + 
           "bin="         + Utils.encToURL(binBy())       + BundlesDT.DELIM +
           "calcstat="    + calculateStat()               + BundlesDT.DELIM +
           "linearcolor=" + linearColor()                 + BundlesDT.DELIM +
           "colorscale="  + colorScale()                  + BundlesDT.DELIM +
           "magcolor="    + magnitudeColoring();
  }

  /**
   * Set the configuration for this panel.  Could be used to recall bookmarks.
   *
   *@param str string representation for new configuration
   */
  public void         setConfig    (String str) {
    StringTokenizer st = new StringTokenizer(str, BundlesDT.DELIM);
    if (st.nextToken().equals("RTGeoHistoPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTGeoHistoPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";

      if      (type.equals("bin"))         binBy(Utils.decFmURL(value));
      else if (type.equals("linearcolor")) linearColor(value.toLowerCase().equals("true"));
      else if (type.equals("colorscale"))  colorScale(value);
      else if (type.equals("magcolor"))    magnitudeColoring(value.toLowerCase().equals("true"));
      else if (type.equals("calcstat"))    calculateStat(value);
      else throw new RuntimeException("Do Not Understand Type Value Pair \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * {@link JComponent} implementing the geospatial hisogram 
   */
  public class RTGeoHistoComponent extends RTComponent {
    private static final long serialVersionUID = 822469844334778261L;
    @Override
    public Set<Shape>      allShapes()                     {
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      set.addAll(myrc.geom_to_a3.keySet());
      return set; }
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> shapes = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Bundle> it = bundles.iterator(); while (it.hasNext()) {
        Bundle bundle = it.next();
        if (myrc.bundle_to_geoms.containsKey(bundle)) shapes.addAll(myrc.bundle_to_geoms.get(bundle));
      }
      return shapes; }
    @Override
    public Set<Bundle> shapeBundles(Shape shape)       { 
      Set<Bundle> set = new HashSet<Bundle>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      String cc = myrc.geom_to_a3.get(shape);
      return myrc.counter_context.getBundles(cc);
    }
    @Override
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      Iterator<Shape> it = myrc.geom_to_a3.keySet().iterator(); while (it.hasNext()) {
         Shape to_test = it.next();
        if (Utils.genericIntersects(to_test,shape)) set.add(to_test);
      }
      return set; }
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> set = new HashSet<Shape>(); RenderContext myrc = (RenderContext) rc; if (myrc == null) return set;
      Iterator<Shape> it = myrc.geom_to_a3.keySet().iterator(); while (it.hasNext()) {
        Shape to_test = it.next();
        while (to_test.contains(x,y)) set.add(to_test);
      }
      return set; }

    /**
     * Add Garnish ... in this specific visualization, add the information about the country --
     * especially since the placement of countries is distorted.
     */
    @Override
    public void addGarnish(Graphics2D g2d, int x, int y) {
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return;

      // Find the first shape that contains this x and y
      Shape found_shape = null;
      Iterator<Shape> it = myrc.geom_to_a3.keySet().iterator(); while (it.hasNext()) {
       Shape shape = it.next(); if (shape.contains(x, y)) { found_shape = shape; break; }
      }

      // If the shape isn't null, gather information about the country and display it
      if (found_shape != null) {
        // Highlight the country shape
        g2d.setColor(RTColorManager.getColor("annotate","region"));
        g2d.draw(found_shape);

        // Get the ISO row
        String cc = myrc.geom_to_a3.get(found_shape);
        String iso_row[] = world_map.getISO3166Row(cc);
        if (iso_row != null) {
          String country_name        = iso_row[WorldMap.COUNTRY_NAME_INDEX];
          String official_state_name = iso_row[WorldMap.OFFICIAL_STATE_NAME_INDEX];
          String sovereignty         = iso_row[WorldMap.SOVEREIGNTY_INDEX];
          String a2_code             = iso_row[WorldMap.A2_CODE_INDEX];
          String a3_code             = iso_row[WorldMap.A3_CODE_INDEX];
          String numeric_code        = iso_row[WorldMap.NUMERIC_CODE_INDEX];
          String tld                 = iso_row[WorldMap.TLD_INDEX];

          // Put it into a string array
          String strs[];

          if (myrc.float_mode == null) strs = new String[4];
          else                         strs = new String[5];

          strs[0] = country_name;
          strs[1] = official_state_name;
          strs[2] = sovereignty;
          strs[3] = "A2: " + a2_code + " | A3: " + a3_code + " | Num: " + numeric_code + " | TLD: " + tld;

          if (strs.length == 5) strs[4] = "" + Utils.humanReadableDouble(myrc.float_mode_calc.get(cc)) + " | " + myrc.float_mode.get(cc).size() + " | " + myrc.statistic;

          int txt_h     = Utils.txtH(g2d,strs[0]);
          int max_txt_w = Utils.txtW(g2d,strs[0]);
          for (int i=1;i<strs.length;i++) { int w = Utils.txtW(g2d,strs[i]); if (w > max_txt_w) max_txt_w = w; }
          
          // Find a location...
          Rectangle2D text_frame; int lines = strs.length; int lines_2 = lines+2;
          text_frame = new Rectangle2D.Double(x - (max_txt_w+10)/2, y - lines_2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
          if (fits(text_frame) == false) { text_frame = new Rectangle2D.Double(x - (max_txt_w+10)/2, y + 2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
            if (fits(text_frame) == false) { text_frame = new Rectangle2D.Double(x - 10, y - lines_2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
              if (fits(text_frame) == false) { text_frame = new Rectangle2D.Double(x - max_txt_w + 10, y - lines_2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
                if (fits(text_frame) == false) { text_frame = new Rectangle2D.Double(x - 10, y + 2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
                  if (fits(text_frame) == false) { text_frame = new Rectangle2D.Double(x - max_txt_w + 10, y + 2*(txt_h+2), max_txt_w + 10, lines*(txt_h+2) + txt_h/2);
                  }
                }
              }
            }
          }

          g2d.setColor(RTColorManager.getColor("label", "defaultbg")); g2d.fill(text_frame);
          g2d.setColor(RTColorManager.getColor("label", "defaultfg")); g2d.draw(text_frame);
          g2d.setColor(RTColorManager.getColor("label", "default"));
          for (int i=0;i<strs.length;i++) g2d.drawString(strs[i], (int) (text_frame.getMinX() + 5), (int) (text_frame.getMinY() + (1+i)*(txt_h+2)));
        }
      }
    }

    /**
     * Does the rectangle fit completely on the screen?
     */
    private boolean fits(Rectangle2D rect) {
      if (rect.getMinX() < 0)           return false;
      if (rect.getMaxX() > getWidth())  return false;
      if (rect.getMinY() < 0)           return false;
      if (rect.getMaxY() > getHeight()) return false;
      return true;
    }

    /**
     * Pull the current configurations from the view and instantiate
     * the renderer for this visualization.
     *
     *@param id render id used to abort superceded renderings
     */
    @Override
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      Bundles    bs        = getRenderBundles();
      String     count_by  = getRTParent().getCountBy(),
                 color_by  = getRTParent().getColorBy(),
                 geolu_by  = binBy();
      ColorScale cs = getColorScale();
      boolean    magnitude_coloring = magnitudeColoring();
      // if (color_by == null) magnitude_coloring = true; // Give it color by defaut if no color defined
      boolean    log_color          = (linearColor()==false);
      RenderContext myrc = new RenderContext(id, bs, count_by, color_by, geolu_by, cs, magnitude_coloring, log_color, calculateStat(), getWidth(), getHeight());
      return myrc;
    }
    
    /**
     * RenderContext implementation for the GeoSpatial histogram.
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Bundles/records for this rendering
       */
      Bundles bs; 

      /**
       * Width of component in pixels
       */
      int     rc_w, 

      /**
       * Height of the component in pixels
       */
              rc_h;

      /**
       * Count specification for how a bundle contributes thee view
       */
      String                count_by, 

      /**
       * Color variable for the rendering
       */
                            color_by, 
      /**
       * Geospatial header selection
       */
                            geolu_by;
                            
      /**
       * ColorScale to use for this rendering
       */
      ColorScale            cs;

      /**
       * Flag to indcate magnitude coloring is in effect.  Magnitude
       * coloring changes how the application displays Sets.  If
       * selected, Sets are displayed by their overall total 
       * in elements.  Alternatively, if not selected, sets are
       * either displayed by a single color if all records have
       * identical settings for that field or by the multi-set color.
       */
      boolean               magnitude_coloring,

      /**
       * Flag to denote linear (or logarithmic) color scale
       */
                            use_log_color;

      /**
       * Maps geometry on the screen to the 3-letter country code
       */
      Map<Shape,String>     geom_to_a3 = new HashMap<Shape,String>();

      /**
       * Record to shape
       */
      Map<Bundle,Set<Shape>> bundle_to_geoms = new HashMap<Bundle,Set<Shape>>();

      /**
       * Counter context used by the rendered to accumulate
       * sums.
       */
      BundlesCounterContext counter_context;

      /**
       * Alternative way for counting that enables floating point options.
       */
      Map<String,List<Float>> float_mode = null;

      /**
       * Statistic calculation for each a3
       */
      Map<String,Double> float_mode_calc = null;

      /**
       * Statistic to calculate
       */
      QuickStats.STAT statistic;

      /**
       * Construct the rendering context for the geospatial histogram
       * with the specified settings.
       *
       *@param id                 render id
       *@param bs                 bundles to render
       *@param count_by           how to count the record contribution to each country
       *@param color_cy           color option based on global settings
       *@param geolu_by           global field to use for identifying geospatial lookups
       *@param cs                 colorscale to use for the rendering
       *@param magnitude_coloring color by set magnitude versus set elements
       *@param use_log_color      flag to indicate logarithmic coloring is in effect
       *@param w                  width for this render
       *@param h                  height for this render
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, String geolu_by, 
                           ColorScale cs, boolean magnitude_coloring, boolean use_log_color, QuickStats.STAT statistic, 
                           int w, int h) {
        render_id = id; this.bs = bs; this.rc_w = w; this.rc_h = h;
        this.count_by           = count_by;
        this.color_by           = color_by;
        this.geolu_by           = geolu_by;
        this.cs                 = cs;
        this.magnitude_coloring = magnitude_coloring;
        this.use_log_color      = use_log_color;
        this.statistic          = statistic;

        // Use a default counter context... but if the count_by field is composed of only integers and floating point, then use the float_mode
        counter_context = new BundlesCounterContext(bs, count_by, color_by);

        // Early termination if there's no geo field...
        if (geolu_by == null) return;

        Set<BundlesDT.DT> field_dts = bs.getGlobals().getFieldDataTypes(bs.getGlobals().fieldIndex(count_by));
        if ((field_dts != null) &&
            ( (field_dts.size() == 2 && field_dts.contains(BundlesDT.DT.INTEGER) && field_dts.contains(BundlesDT.DT.FLOAT)) ||
              (field_dts.size() == 1 && field_dts.contains(BundlesDT.DT.INTEGER))                                           ||
              (field_dts.size() == 1 &&                                             field_dts.contains(BundlesDT.DT.FLOAT)))) float_mode = new HashMap<String,List<Float>>();

        // Go through the tablets... if it can satisfy both country and count_by then move forward
        Iterator<Tablet> it_tab = bs.tabletIterator();
        while (it_tab.hasNext() && currentRenderID() == getRenderID()) {
          Tablet  tablet           = it_tab.next();
          boolean tablet_can_count = count_by.equals(BundlesDT.COUNT_BY_BUNS) || KeyMaker.tabletCompletesBlank(tablet, count_by);

          KeyMaker count_km = null;
          if (float_mode != null) count_km = new KeyMaker(tablet, count_by);

          if (KeyMaker.tabletCompletesBlank(tablet, geolu_by) && tablet_can_count) {
            KeyMaker binner = new KeyMaker(tablet, geolu_by);          
            Iterator<Bundle> it_bun = tablet.bundleIterator();
            while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle = it_bun.next();
              String bins[] = binner.stringKeys(bundle);
              for (int i=0;i<bins.length;i++) { 
                String bin = world_map.a3code(bins[i]);
                counter_context.count(bundle, bin);

                // Keep a list of floats for floating point numbers/calculations
                if (float_mode != null) {
                  if (float_mode.containsKey(bin) == false) float_mode.put(bin, new ArrayList<Float>());
                  String count_bins[] = count_km.stringKeys(bundle);
                  for (int j=0;j<count_bins.length;j++) float_mode.get(bin).add(Float.parseFloat(count_bins[j]));
                }
              }
            }
          } else { addToNoMappingSet(tablet); }
        }
      }
      @Override
      public int           getRCHeight() { return rc_h; }
      @Override
      public int           getRCWidth()  { return rc_w; }

      // If already rendered, store the image here
      BufferedImage base_bi = null;

      Rectangle2D adj_bounds = null;
      private void calculateAdjustedBounds() {
        double perc = 0.02;
        double x_adj = world_map.getBounds().getWidth()  * perc;
        double y_adj = world_map.getBounds().getHeight() * perc;
        adj_bounds = new Rectangle2D.Double(world_map.getBounds().getX() - x_adj,
                                            world_map.getBounds().getY() - y_adj,
                                            world_map.getBounds().getWidth()  + 2*x_adj,
                                            world_map.getBounds().getHeight() + 2*y_adj);
      }

      // World X To Screen X Transform
      public int wxToSx(double wx) {
        if (adj_bounds == null) calculateAdjustedBounds();
        // double sx = getRCWidth() * (wx - world_map.getBounds().getX()) / (world_map.getBounds().getWidth());
        double sx = getRCWidth() * (wx - adj_bounds.getX()) / (adj_bounds.getWidth());
        return (int) sx;
      }
      // World Y TO Screen Y Transform
      public int wyToSy(double wy) {
        if (adj_bounds == null) calculateAdjustedBounds();
        // double sy = getRCHeight() - (getRCHeight() * (wy - world_map.getBounds().getY()) / (world_map.getBounds().getHeight()));
        double sy = getRCHeight() - (getRCHeight() * (wy - adj_bounds.getY()) / (adj_bounds.getHeight()));
        return (int) sy;
      }
      // World Path To Screen Path Transform
      Path2D worldPath2DToScreenPath2D(Path2D wpath) {
        Path2D spath = new Path2D.Double(); PathIterator iterator = wpath.getPathIterator(null,1.0); while (iterator.isDone() == false) {
          double points[] = new double[6];
          int type = iterator.currentSegment(points);
          if        (type == PathIterator.SEG_MOVETO) { spath.moveTo(wxToSx(points[0]),wyToSy(points[1]));
          } else if (type == PathIterator.SEG_LINETO) { spath.lineTo(wxToSx(points[0]),wyToSy(points[1]));
          }
          iterator.next();
        }
        return spath;
      }
      // World Rectangle to Screen Rectangle Transform
      Rectangle2D worldRectangle2DToScreenRectangle2D(Rectangle2D wrect) {
        int sx = wxToSx(wrect.getX()), sy = wyToSy(wrect.getY());
        return new Rectangle2D.Double(sx,sy,wxToSx(wrect.getX() + wrect.getWidth()) - sx,wyToSy(wrect.getY() + wrect.getHeight()) - sy);
      }

      @Override
      public BufferedImage getBase() {
        if (base_bi == null) {
          Graphics2D g2d = null;
          try {
            // Basics
            base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            RTColorManager.renderVisualizationBackground(base_bi, g2d);
            int     txt_h   = Utils.txtH(g2d,"0");

            // Get the min and max... alternate version uses the float_mode
            double  min = 0.0, max; 
            if (float_mode != null) {
              min = Double.POSITIVE_INFINITY; max = Double.NEGATIVE_INFINITY;

              // Do all the calcs
              float_mode_calc = new HashMap<String,Double>();
              Iterator<String> it = float_mode.keySet().iterator(); while (it.hasNext()) {
                String key = it.next();
                QuickStatsFloat qsf = new QuickStatsFloat(float_mode.get(key));
                double value = qsf.getStat(statistic);
                float_mode_calc.put(key, value);
                if (value < min) min = value;
                if (value > max) max = value;
              }

              // May not be any elements...
              if (Double.isInfinite(min)) { min = 0.0; max = 1.0; }
              if (min == max)             { min = max - 1.0; }
            } else { max = counter_context.totalMaximum(); if (max < 0.1) max = 0.1; }

            // For the missing countries...
            double missing_x = 5;

            // Draw the countries that occur in the data
            Iterator<String> it_a3 = counter_context.binIterator(); Set<String> drawn = new HashSet<String>(); while (it_a3.hasNext()) {
              String a3 = it_a3.next();

              // Figure out coloring...
              if (magnitude_coloring) {
                if (float_mode != null) {
                  // Use the float_mode
                  double total = float_mode_calc.get(a3);
                  if (use_log_color) {
                    if (total > 0.0f) { g2d.setColor(cs.at((float) (Math.log(total)/Math.log(max)))); } else continue;
                  } else g2d.setColor(cs.at((float) ((total-min)/(max-min))));
                } else {
                  // Use the counter_context
                  if (use_log_color) {
                    double total = counter_context.total(a3); if (total > 0.0) {
                           g2d.setColor(cs.at((float) (Math.log(total)/Math.log(max)))); } else continue;
                 } else g2d.setColor(cs.at((float) counter_context.totalNormalized(a3)));
                }
              } else g2d.setColor(counter_context.binColor(a3));

              // Get the shapes and fill them... and then keep the lookups for interactivity
              Set<Shape> shapes = world_map.getCountryShapes(a3); 

              // If the shape is missing... put it in the lower left (incrementally)
              if (shapes == null) { 
                shapes = new HashSet<Shape>();
                shapes.add(new Rectangle2D.Double(missing_x, getRCHeight() - 20, 10, 10)); missing_x += 15;
              }

              Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
                Shape shape = it_shape.next(); Shape screen = null;
                if        (shape instanceof Path2D)       { screen = worldPath2DToScreenPath2D((Path2D) shape);
                } else if (shape instanceof Rectangle2D)  { screen = shape; // worldRectangle2DToScreenRectangle2D((Rectangle2D) shape);
                } else { System.err.println("No transform for shape type: " + shape); }

                // Do the fill
                g2d.fill(screen);
                g2d.draw(screen);

                // Bookkeeping for interactivity
                geom_to_a3.put(screen,a3);
                Iterator<Bundle> it_bun = counter_context.getBundles(a3).iterator(); while (it_bun.hasNext()) {
                  Bundle bundle = it_bun.next();
                  if (bundle_to_geoms.containsKey(bundle) == false) bundle_to_geoms.put(bundle, new HashSet<Shape>());
                  bundle_to_geoms.get(bundle).add(screen);
                }
              }
            }

            // Draw the outlines of all of the countries for context
            it_a3 = world_map.countryCodeIterator(); while (it_a3.hasNext()) {
              String a3code = it_a3.next().toLowerCase();
              Set<Shape> shapes = world_map.getCountryShapes(a3code);
              g2d.setColor(RTColorManager.getColor("background","nearbg"));
              g2d.setStroke(new BasicStroke(0.5f));
              Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
                Shape shape = it_shape.next();
                if (shape instanceof Path2D) {
                  Path2D spath = worldPath2DToScreenPath2D((Path2D) shape);
                  g2d.draw(spath);
                }
              }
            }
          } finally { if (g2d != null) g2d.dispose(); }
        } return base_bi;
      }

      // Old version using java's transforms :(
      public BufferedImage getBaseXXX() { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi         = new BufferedImage(rc_w, rc_h, BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);

          int     txt_h   = Utils.txtH(g2d,"0");
          // Set up the transform
          AffineTransform orig_trans = g2d.getTransform();

          int adj_h = rc_h - 2*txt_h; if (adj_h < 4*txt_h) adj_h = 4*txt_h;

          Rectangle2D bounds = world_map.getBounds();
          g2d.scale(0.9 * rc_w/bounds.getWidth(), -0.9 * rc_h/bounds.getHeight());
          g2d.translate(bounds.getWidth()/2.0, -bounds.getHeight()/2.0);

          Set<String> no_shapes = new HashSet<String>();

          // Get the min and max... alternate version uses the float_mode
          double  min = 0.0, max; 
          if (float_mode != null) {
            min = Double.POSITIVE_INFINITY; max = Double.NEGATIVE_INFINITY;

            // Do all the calcs
            float_mode_calc = new HashMap<String,Double>();
            Iterator<String> it = float_mode.keySet().iterator(); while (it.hasNext()) {
              String key = it.next();
              QuickStatsFloat qsf = new QuickStatsFloat(float_mode.get(key));
              double value = qsf.getStat(statistic);
              float_mode_calc.put(key, value);
              if (value < min) min = value;
              if (value > max) max = value;
            }

            // May not be any elements...
            if (Double.isInfinite(min)) { min = 0.0; max = 1.0; }
            if (min == max)             { min = max - 1.0; }
          } else { max = counter_context.totalMaximum(); if (max < 0.1) max = 0.1; }

          // Go through the cc's
          Iterator<String> it_a3 = counter_context.binIterator(); Set<String> drawn = new HashSet<String>();
          while (it_a3.hasNext()) {
            String     a3           = it_a3.next();

            // CCShapeRec cc_shape_rec = geodata.getCCShapeRec(cc);
            Set<Shape> shapes       = world_map.getCountryShapes(a3);

            // Figure out coloring...
            if (magnitude_coloring) {
              if (float_mode != null) {
                //
                // Use the float_mode
                //
                double total = float_mode_calc.get(a3);
                if (use_log_color) {
                  if (total > 0.0f) { g2d.setColor(cs.at((float) (Math.log(total)/Math.log(max)))); } else continue;
                } else g2d.setColor(cs.at((float) ((total-min)/(max-min))));

              } else {
                //
                // Use the counter_context
                //
                if (use_log_color) {
                  double total = counter_context.total(a3); if (total > 0.0) {
                         g2d.setColor(cs.at((float) (Math.log(total)/Math.log(max)))); } else continue;
                } else g2d.setColor(cs.at((float) counter_context.totalNormalized(a3)));
              }
            } else g2d.setColor(counter_context.binColor(a3));

            // Draw it...
            if (shapes != null && shapes.size() > 0) {
             Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
              Shape shape = it_shape.next();
              g2d.fill(shape); drawn.add(a3);

              // Book keeping for interactivity
              Path2D screen_shape = new Path2D.Double(); // Need the transformed shape...
              screen_shape.append(shape.getPathIterator(g2d.getTransform()), true);
              geom_to_a3.put(screen_shape,a3);
              Iterator<Bundle> it_bun = counter_context.getBundles(a3).iterator(); while (it_bun.hasNext()) {
                Bundle bundle = it_bun.next();
                if (bundle_to_geoms.containsKey(bundle) == false) bundle_to_geoms.put(bundle, new HashSet<Shape>());
                bundle_to_geoms.get(bundle).add(screen_shape);
              }

              // Labeling ... this only applies to the hex version...

              /*
              Rectangle2D rect = shape.getBounds();
              g2d.setColor(RTColorManager.getColor("label", "defaultbg"));
              g2d.drawString(a3.toUpperCase(), 
                             (int) (rect.getCenterX() - Utils.txtW(g2d, a3.toUpperCase())/2.0)-1, 
                             (int) (rect.getCenterY() + Utils.txtH(g2d, a3.toUpperCase())/2.0)-1);
              g2d.setColor(RTColorManager.getColor("label", "default"));
              g2d.drawString(a3.toUpperCase(), 
                             (int) (rect.getCenterX() - Utils.txtW(g2d, a3.toUpperCase())/2.0), 
                             (int) (rect.getCenterY() + Utils.txtH(g2d, a3.toUpperCase())/2.0));
              */
             }
            } else {
              // System.err.println("adding \"" + a3 + "\" to no_shapes...");
              no_shapes.add(a3);
            }
          }

          // Draw all the countries (for context)
          it_a3 = world_map.countryCodeIterator(); while (it_a3.hasNext()) {
            String a3code = it_a3.next().toLowerCase();
            if (drawn.contains(a3code) == false) {
              Set<Shape> shapes = world_map.getCountryShapes(a3code);
              g2d.setColor(RTColorManager.getColor("background","nearbg"));
              Iterator<Shape> it_shape = shapes.iterator(); while (it_shape.hasNext()) {
                Shape shape = it_shape.next();
                g2d.draw(shape);
                Rectangle2D rect = shape.getBounds();
                /*
                g2d.drawString(a3code.toUpperCase(), 
                               (int) (rect.getCenterX() - Utils.txtW(g2d, a3code.toUpperCase())/2.0), 
                               (int) (rect.getCenterY() + Utils.txtH(g2d, a3code.toUpperCase())/2.0));
                */
              }
            }
          }

          g2d.setTransform(orig_trans);
          // Put the no shapes at the bottom
          int x0 = 25;
          Iterator<String> it = no_shapes.iterator();
          while (it.hasNext()) {
            String cc = it.next();
            // Figure out coloring...
            if (magnitude_coloring) {
              if (use_log_color) {
                double total = counter_context.total(cc); if (total > 0.0) {
                       g2d.setColor(cs.at((float) (Math.log(total)/Math.log(max)))); } else continue;
              } else g2d.setColor(cs.at((float) counter_context.totalNormalized(cc)));
            } else g2d.setColor(counter_context.binColor(cc));
            // Draw it...
            g2d.fillRect(x0, adj_h-txt_h, 40, txt_h);
            g2d.setColor(Color.red);
            g2d.drawString(cc, x0 + 20 - Utils.txtW(g2d,cc)/2, adj_h - txt_h - 2);
            x0 += 45;
          }
          // draw a legend
          if (magnitude_coloring) {
            int adj_w = rc_w - 40; if (adj_w < 40) adj_w = 40;
            // Draw the tick marks for log
            if (use_log_color) {
              double var = 10.0; g2d.setColor(Color.white);
              while (var < max) {
                int x = (int) (20 + (adj_w * Math.log(var)/Math.log(max)));
                g2d.drawLine(x, adj_h, x, adj_h + txt_h);
                var *= 10.0;
              }
            }
            // Draw the color bar
            for (int i=0;i<adj_w;i++) {
              g2d.setColor(cs.at(((float) i)/(adj_w-1)));
              g2d.drawLine(20 + i, adj_h+2, 20 + i, adj_h + txt_h - 4);
            }

            // Give it text ... preferably human readable text
            String max_hr = Utils.humanReadableDouble(max),
                   min_hr = Utils.humanReadableDouble(min);

            g2d.setColor(Color.white); g2d.drawString(max_hr, 20+adj_w-Utils.txtW(g2d,max_hr),   adj_h + 2*txt_h-2);
            if (use_log_color)         g2d.drawString("1.0",  20,                                adj_h + 2*txt_h-2);
            else                       g2d.drawString(min_hr, 20,                                adj_h + 2*txt_h-2);
            g2d.setColor(Color.red);
            if (use_log_color)         g2d.drawString("Log",  20+adj_w/2-Utils.txtW(g2d,"Log"),  adj_h + 2*txt_h-2);
          }
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }
    }
  }
}

