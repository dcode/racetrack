/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesUtils;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.util.Utils;

/**
 * Class that implements a dialog to create/edit a report.
 *
 *@author  D. Trimm
 *@version 0.1
 */
public class RTReportDialog extends JDialog {
  /**
   *
   */
  private static final long serialVersionUID = -1931167927943748269L;

  /**
   * Title Textfield
   */
  JTextField title_tf,

  /**
   * Report Source Textfield
   */
             source_tf,

  /**
   * UUID Textfield (non-editable)
   */
             uuid_tf,

  /**
   * URL Textfield
   */
             url_tf,

  /**
   * Tags Textfield
   */
             tags_tf;


  /**
   * Report body text area
   */
  JTextArea  text_ta;

  /**
   * Reference to the edited report bundle - null if this is a new report
   */
  Bundle     report_bundle;

  /**
   * Parent widget... also contains access to the application data
   */
  RTPanel    parent;

  /**
   * Constructor
   */
  public RTReportDialog(RTPanel parent, Bundle report_bundle) {
    super(parent.getRTParent(), "Report Dialog", false);
    this.parent        = parent;
    this.report_bundle = report_bundle;

    JPanel panel = null;

    //
    // TextFields
    //
    getContentPane().setLayout(new BorderLayout(8,8));
    
    JPanel upper = new JPanel(new BorderLayout());
    panel = new JPanel(new GridLayout(5,0,5,5));
      panel.add(new JLabel("Title"));
      panel.add(new JLabel("Source"));
      panel.add(new JLabel("URL"));
      panel.add(new JLabel("Tags"));
      panel.add(new JLabel("UUID"));
    upper.add("West", panel);
    panel = new JPanel(new GridLayout(5,0,5,5));
      panel.add(title_tf = new JTextField());
      panel.add(source_tf = new JTextField());
      panel.add(url_tf    = new JTextField());
      panel.add(tags_tf   = new JTextField());
      panel.add(uuid_tf   = new JTextField()); uuid_tf.setEditable(false);
    upper.add("Center", panel);

    getContentPane().add("North", upper);

    //
    // TextArea
    //

    JPanel center = new JPanel(new BorderLayout());
    center.add("Center", new JScrollPane(text_ta = new JTextArea())); text_ta.setLineWrap(true);
    // center.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Report Body"));

    getContentPane().add("Center", center);

    //
    // Buttons
    //

    panel = new JPanel(new FlowLayout()); JButton bt;
    panel.add(bt = new JButton("Cancel"));       bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { cancelDialog(); } } );
    String create_or_edit = (report_bundle == null) ? "Create" : "Edit";
    panel.add(bt = new JButton(create_or_edit)); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { createOrEdit(); } } );

    getContentPane().add("South", panel);

    // Add additional listener
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { cancelDialog(); } } );

    if (report_bundle != null) fillFields(report_bundle);
    else                       uuid_tf.setText(UUID.randomUUID().toString());

    // Show the dialog
    pack(); setSize(512, 512); setVisible(true);
  }


  /**
   * Cancel the dialog and free all related resources.
   */
  public void cancelDialog() {
    setVisible(false); dispose();
  }

  /**
   * Create or edit the report.  Close the dialog as well...
   */
  public void createOrEdit() {
    Bundles root = parent.getRTParent().getRootBundles();

    //
    // If there wasn't a report bundle, then this is a create function
    //
    if (report_bundle == null) {
      String hdr[]   = BundlesUtils.reportPartialHeader(),
             hdr_e[] = BundlesUtils.reportFullHeader();

      // Find the tablet
      Tablet tablet  = root.findTablet(hdr); boolean etime = false;
      if (tablet == null) { tablet = root.findTablet(hdr_e);       etime = true;  }
      if (tablet == null) { tablet = root.findOrCreateTablet(hdr); etime = false; }

      // Create attribute set
      Map<String,String> attr = new HashMap<String,String>();
      attr.put(BundlesUtils.REPORT_TITLE_STR,       title_tf. getText());
      attr.put(BundlesUtils.REPORT_SOURCE_STR,      source_tf.getText());
      attr.put(BundlesUtils.REPORT_URL_STR,         url_tf.   getText());
      attr.put(BundlesUtils.REPORT_TAGS_STR,        tags_tf.  getText());
      attr.put(BundlesUtils.REPORT_UUID_STR,        uuid_tf.  getText());
      attr.put(BundlesUtils.REPORT_PARENT_UUID_STR, "");
      attr.put(BundlesUtils.REPORT_TEXT_STR,        text_ta.  getText());

      if (etime) tablet.addBundle(attr, Utils.exactDate(System.currentTimeMillis()), Utils.exactDate(System.currentTimeMillis()));
      else       tablet.addBundle(attr, Utils.exactDate(System.currentTimeMillis()));

    //
    // Otherwise, it's an update to an existing report
    //
    } else {
      Set<Bundle> set         = new HashSet<Bundle>(); set.add(report_bundle);
      Bundles     just_report = root.subset(set);

      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_TITLE_STR,      title_tf. getText());
      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_SOURCE_STR,     source_tf.getText());
      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_URL_STR,        url_tf.   getText());
      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_TAGS_STR,       tags_tf.  getText());
      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_UUID_STR,       uuid_tf.  getText());
      root.getGlobals().setField(just_report, root, BundlesUtils.REPORT_TEXT_STR,       text_ta.  getText());
    }

    // Update the application data
    parent.getRTParent().updateBys();
    parent.getRTParent().refreshPanelApplicationData();

    // Close out the dialog
    setVisible(false); dispose();
  }

  /**
   *
   */
  protected void fillFields(Bundle bundle) {
    KeyMaker km; String strs[];

    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_TITLE_STR);  strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) title_tf.   setText(strs[0]);
    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_SOURCE_STR); strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) source_tf.  setText(strs[0]);
    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_URL_STR);    strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) url_tf.     setText(strs[0]);
    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_TAGS_STR);   strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) tags_tf.    setText(strs[0]);
    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_UUID_STR);   strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) uuid_tf.    setText(strs[0]);
    km = new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_TEXT_STR);   strs = km.stringKeys(bundle); if (strs != null && strs.length > 0) text_ta.    setText(strs[0]);
  }
}

