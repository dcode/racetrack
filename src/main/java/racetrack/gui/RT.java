/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.Color;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesG;
import racetrack.framework.BundlesRecs;
import racetrack.framework.BundlesUtils;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;
import racetrack.kb.EntityTag;
import racetrack.transform.GeoData;
import racetrack.util.Entity;
import racetrack.util.EntityExtractor;
import racetrack.util.Interval;
import racetrack.util.Relationship;
import racetrack.util.SubText;
import racetrack.util.TimeStamp;
import racetrack.util.Utils;
import racetrack.visualization.StatsOverlay;
import racetrack.visualization.RTColorManager;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Interface to listen for time marker updates, changes.
 *
 *@author  D. Trimm
 *@version 1.0
 */
interface TimeMarkerListener { 
  /**
   * New time marker created.
   *
   *@param tm new time marker
   */
  public void newTimeMarker(TimeMarker tm);

  /**
   * List of time markers has changed.
   */
  public void timeMarkerListChanged(); }

/**
 * Interface to listen to entity tag updates, changes.
 *
 *@author  D. Trimm
 *@version 1.0
 */
interface EntityTagListener  { 
  /**
   * New entity tag created
   *
   *@param tg new tag
   */
  public void newEntityTag(EntityTag tg);

  /**
   * List of entity tags has changed.
   */
  public void entityTagListChanged(); }

/**
 * Main class for controlling the GUI portion of the application.  Handles
 * the overall state of the application by leveraging the {@link Bundles}
 * framework class. 
 */
public class RT extends JFrame {
  private static final long serialVersionUID = 4482950148773803543L;

  /**
   * Current stack of dataset for rendering.  The bottom of the stack
   * is the root bundle and should be the superset of all of the data
   * elements.  All other portions of the stack *should* be subsets
   * of the root.
   */
  List<Bundles> bundles_stack     = new ArrayList<Bundles>(); 

  /**
   * Index to the currently visible portion of the dataset.  Value
   * should be greater than equal to 0 and less than the size of the
   * bundles_stack
   */
  int                bundles_stack_i   = 0;

  /**
   * Current set of records to highlight.
   */
  Bundles            highlight_bundles = null; 

  /**
   * Component that issued the highlight request.  Used for
   * the "Replace" version of highlight so that the original
   * component won't redraw.
   */
  RTPanel            highlight_source  = null;

  /**
   * Get a copy of the root bundles (the entire dataset)
   *
   *@return root (all) bundles (records)
   */
  public Bundles getRootBundles()                      { 
    synchronized (bundles_stack) { return bundles_stack.get(0); } }

  /**
   * Duplicate the bundles stack.
   *
   *@return duplicate stack
   */
  public List<Bundles> dupeBundlesStack() {
    synchronized (bundles_stack) { List<Bundles> dupe = new ArrayList<Bundles>(); dupe.addAll(bundles_stack); return dupe; }
  }

  /**
   *
   */
  public String stackDescriptions() {
    StringBuffer sb = new StringBuffer();
    synchronized (bundles_stack) {
      for (int i=0;i<bundles_stack.size();i++) {
        Bundles bundles = bundles_stack.get(i);
        int    tablet_count      = bundles.tabletCount(),
               record_count      = bundles.size();
        String timestamp         = Utils.exactDate(bundles.ts0()),
               timestamp_end     = Utils.exactDate(bundles.ts1());
        String slice_description = bundles.sliceDescription();

        sb.append("{\"stack_position\",\""  + i + "\",\n");
        sb.append(" \"tablet_count\",\""    + tablet_count      + "\",\n");
        sb.append(" \"record_count\",\""    + record_count      + "\",\n");
        sb.append(" \"timestamp\",\""       + timestamp         + "\",\n");
        sb.append(" \"timestamp_end\",\""   + timestamp_end     + "\",\n");
        sb.append(" \"slice_description\"," + slice_description + "}\n\n");
      }
    }
    return sb.toString();
  }

  /**
   * Set a new root for the application.  This will destroy the data
   * that isn't contained within the new set of bundles.
   *
   *@param new_root new root dataset
   *
   */
  public void    setRootBundles(Bundles new_root) {
    synchronized (bundles_stack) { 
      bundles_stack.clear(); bundles_stack.add(new_root); bundles_stack_i = 0; 

      // Make sure every record points to the right tablet
      new_root.resetTablets();
      new_root.setSliceDescription("{\"operation\":\"setRootBundles\", \"RTPanel\":\"RT\"}");

      // Clean up the entities to integers conversions
      Set<Bundles> as_set = new HashSet<Bundles>(); as_set.add(new_root);
      new_root.getGlobals().cleanse(as_set);

      // Update the gui
      refreshPanelApplicationData();
    }
  }

  /**
   * Refresh the underlying application data for all the GUI panels.
   */
  public void refreshPanelApplicationData() {
    synchronized (bundles_stack) { for (int i=0;i<panels.size();i++) panels.get(i).newBundlesRoot(getRootBundles()); }
  }

  /**
   * Return the visible (to-be-rendered) data records.
   *
   *@return visible records
   */
  public Bundles getVisibleBundles()   { synchronized (bundles_stack) { return bundles_stack.get(bundles_stack_i); } }

  /**
   * Return the visible records.  Ensure that the requesting get the right set in the  case of the 
   * "replace" highlighting option.
   *
   *@param  whos_asking requesting component
   *
   *@return             visible records with consideration for the requesting component
   */
  public Bundles getVisibleBundles(RTPanel whos_asking)   { 
    synchronized (bundles_stack) { 
      Bundles highlight_copy = highlight_bundles; Object highlight_source_copy = highlight_source;
      if (highlight_copy != null && highlight_source_copy != null && whos_asking != highlight_source_copy) return highlight_copy;
      else return bundles_stack.get(bundles_stack_i); 
    } }

  /**
   * Return the bundles that are to be highlighted (brushed) by the
   * component.
   *
   *@return bundles to be brushed/highlighted
   */
  public Bundles getHighlightBundles() { return highlight_bundles; }

  /**
   * Push a subset of the records onto the stack.  Notify each view to re-render.
   *
   *@param bs subset of the overall records for filtering
   */
  public void    push(Bundles bs) { 
    System.err.println("push(" + bs + ")"); if (bundles_stack.size() > 0 && bs.size() == 0) return;
    synchronized (bundles_stack) {
      // Check to see if the bundles is somewhere in the stack
      boolean found = false;
      for (int i=0;i<bundles_stack.size();i++) {
        if (bundles_stack.get(i) == bs) { bundles_stack_i = i; found = true; }
      }
      if (found) { refreshAll(); return; }

      // Otherwise, figure out where to add it
      while (bundles_stack.size() > (bundles_stack_i+1)) bundles_stack.remove(bundles_stack.size()-1);
      if (bundles_stack.size() > 0 && bs.equals(bundles_stack.get(bundles_stack.size()-1))) return;
      bundles_stack.add(bs); bundles_stack_i = bundles_stack.size() - 1;
      System.err.println("Pushing [" + bundles_stack.size() + "/" + bundles_stack_i + "] \"" + bs + "\""); refreshAll(); 
    } 
  }

  /**
   * Re-filter the dataset based on sets that were previously unfiltered.  Only works for a limited set
   * of subsets of data.
   */
  public void    repush() { 
    synchronized (bundles_stack) {
      bundles_stack_i++; if (bundles_stack_i > (bundles_stack.size()-1)) bundles_stack_i = bundles_stack.size()-1;
      System.err.println("RePushing [" + bundles_stack.size() + "/" + bundles_stack_i + "]"); refreshAll(); 
    } 
  }

  /**
   * Un-filter the dataset to the last level.  Calling this when the root bundles
   * are already the current visible set causes no change.
   */
  public void    pop()                 { synchronized (bundles_stack) {
                                           if (bundles_stack_i > 0) bundles_stack_i--;
                                           System.err.println("Popping [" + bundles_stack.size() + "/" + bundles_stack_i + "] \"" + top() +"\"");
                                           refreshAll(); } }

  /**
   * Return to the root bundles.
   */
  public void    popAll()              { synchronized (bundles_stack) {
                                           bundles_stack_i = 0;
                                           System.err.println("Pop All \"" + top() + "\" [" + bundles_stack.size() + "/" + bundles_stack_i + "]");
                                           refreshAll(); } }

  /**
   * Return the top of the stack which is the visible bundle set.
   *
   *@return visible bundle set
   */
  public Bundles top()                 { synchronized (bundles_stack) { return bundles_stack.get(bundles_stack_i); } }


  /**
   * Clear all of the data elements from the 
   */
  public void    zeroizeRoot()         { synchronized (bundles_stack) {
                                           setRootBundles(new BundlesRecs()); } }

  /**
   * List of active rendering panels for notifying when the dataset changes.
   */
  List<RTPanel> panels = new ArrayList<RTPanel>();

  /**
   * The overall controlling window for the application.
   */
  RTControlFrame     rt_control_frame;

  /**
   * The set of bundles to directly highlight.
   */
  Set<Bundle>    highlights    = null;

  /**
   * The set of bundles equivalent to a first order derivative.
   */
  Set<Bundle>    highlights_p  = null;

  /**
   * The set of bundles equivalent to a second order derivative.
   */
  Set<Bundle>    highlights_pp = null;

  /**
   * Construct the default implementation of the application.
   * Push a blank set of bundles onto the stack and start any application-level
   * polling threads.
   */
  public RT(boolean native_messaging) {
    super("RACETrack");
    push(new BundlesRecs());
    rt_control_frame = new RTControlFrame(this);

    // JsonObject obj = new JsonObject(); // test to see if gson library loaded correctly

    // Animation Thread - For Entity Highlights...  doesn't seem to work correctly
    (new Thread(new Runnable() { public void run() {
      while (true) {
        try { Thread.sleep(100); } catch (InterruptedException ie) { }
              Set<SubText> subs = getEntityHighlights();
              if (subs != null && subs.size() > 0) {
                Iterator<RTPanel> it = panels.iterator();
                while (it.hasNext()) {
                  RTPanel panel = it.next(); if (panel.getRTComponent().mouseIn()) continue; // Mouse In Test Seems To Solve Interaction Problems
                  if (panel.getRTComponent().getRTRenderContext().hasEntityShapes()) panel.repaint();
                }
              }
      } } } ) ).start();

    // Native messaging thread
    // ... probably should make option on load... otherwise, may waste a minor amount of resources.
    if (native_messaging) {
      (new Thread(new Runnable() { public void run() {
        while (true) {
          // Read a message
          String message = receiveMessage();

          if (message.equals("") == false) {
            System.err.println("Message: \"" + message + "\""); // DEBUG ... checking the browser web console

            // Non-threaded version...  for now
            handleNativeMessage(message);
          }
        }
      } } ) ).start(); 
    }
  }

  /**
   * Create an integer from a byte array. ... and receive a message from the native messaging protocol.
   *
   * From:
   *
   * https://stackoverflow.com/questions/32553999/how-to-implement-chrome-native-messaging-message-handling-protocol-in-java/33113627#33113627
   */
  //Convert length from Bytes to int
  public static int getInt(byte[] bytes) {
    return (bytes[3] << 24) & 0xff000000|
           (bytes[2] << 16) & 0x00ff0000|
           (bytes[1] << 8)  & 0x0000ff00|
           (bytes[0] << 0)  & 0x000000ff;
  }
  // Read an input from Chrome Extension
  static public String receiveMessage(){
    byte[] b = new byte[4];
    try{
      System.in.read(b);
      int size = getInt(b); // System.err.println("size = " + size); // DEBUG
      byte[] msg = new byte[size];
      System.in.read(msg);

      // make sure to get message as UTF-8 format
      String msgStr = new String(msg, "UTF-8");
      return msgStr;
    }catch (IOException ioe){
      System.err.println("IOException in native messaging receiveMessage(): " + ioe);
      ioe.printStackTrace(System.err);
      return null;
    }
  }

  /**
   * Parse a native message and provide the information to other visualization components for further processing / rendering / animations.
   *
   *@param message JSON-formatted string from a native messaging application.
   *
   */
  protected synchronized void handleNativeMessage(String message) {
    try {
      JsonObject root = new JsonParser().parse(message).getAsJsonObject();
      System.err.println("Root = \"" + root + "\"");

      JsonElement event_el  = root.get("event");
      String      event_str = Utils.removeDQuotes(event_el.toString());

      if        (event_str.equals("page scroll event")) {
        native_messaging_last_title_str      = Utils.removeDQuotes(root.get("title").toString());
        native_messaging_last_url_str        = Utils.removeDQuotes(root.get("url").toString());
        native_messaging_last_event_str      = event_str;
        native_messaging_last_event_mode_str = "visible";
        updateNativeIndicatorMap(native_messaging_last_url_str,
                                 native_messaging_last_event_mode_str,
                                 new WebInfo(native_messaging_last_url_str,
                                             native_messaging_last_title_str,
                                             transformAndFilterIndicators(root.getAsJsonArray("indicators"))));
      } else if (event_str.equals("active tab"))        {
        // String    url_str    = root.get("url").toString();
      } else if (event_str.equals("page info"))         {
        native_messaging_last_title_str      = Utils.removeDQuotes(root.get("title").toString());
        native_messaging_last_url_str        = Utils.removeDQuotes(root.get("url").toString());
        native_messaging_last_event_str      = event_str;
        native_messaging_last_event_mode_str = "page";
        updateNativeIndicatorMap(native_messaging_last_url_str,
                                 native_messaging_last_event_mode_str,
                                 new WebInfo(native_messaging_last_url_str,
                                             native_messaging_last_title_str,
                                             transformAndFilterIndicators(root.getAsJsonArray("indicators"))));
      } else if (event_str.equals("document hidden")) {
        // String    title_str  = root.get("title").toString(),
        //           url_str    = root.get("url").toString();
      } else if (event_str.equals("document shown"))  {
        native_messaging_last_title_str      = Utils.removeDQuotes(root.get("title").toString());
        native_messaging_last_url_str        = Utils.removeDQuotes(root.get("url").toString());
        native_messaging_last_event_str      = event_str;
        native_messaging_last_event_mode_str = "page";
        updateNativeIndicatorMap(native_messaging_last_url_str,
                                 native_messaging_last_event_mode_str,
                                 new WebInfo(native_messaging_last_url_str,
                                             native_messaging_last_title_str,
                                             transformAndFilterIndicators(root.getAsJsonArray("indicators"))));
      } else if (event_str.equals("document shown visible indicators")) {
        native_messaging_last_title_str      = Utils.removeDQuotes(root.get("title").toString());
        native_messaging_last_url_str        = Utils.removeDQuotes(root.get("url").toString());
        native_messaging_last_event_str      = event_str;
        native_messaging_last_event_mode_str = "visible";
        updateNativeIndicatorMap(native_messaging_last_url_str,
                                 native_messaging_last_event_mode_str,
                                 new WebInfo(native_messaging_last_url_str,
                                             native_messaging_last_title_str,
                                             transformAndFilterIndicators(root.getAsJsonArray("indicators"))));
      //
      // Harvest (all) indicators
      //
      } else if (event_str.equals("harvest indicators")) {
        if (native_messaging_map.containsKey("page") &&
            native_messaging_map.get("page").containsKey(native_messaging_last_url_str)) {
          WebInfo web_info = native_messaging_map.get("page").get(native_messaging_last_url_str);
          getControlPanel().harvestIndicators(web_info.url, web_info.title, web_info.indicators);
        }

      //
      // Harvest visible indicators
      //
      } else if (event_str.equals("harvest visible indicators")) {
        if (native_messaging_map.containsKey("visible") &&
            native_messaging_map.get("visible").containsKey(native_messaging_last_url_str)) {
          WebInfo web_info = native_messaging_map.get("visible").get(native_messaging_last_url_str);
          getControlPanel().harvestIndicators(web_info.url, web_info.title, web_info.indicators);
        }

      //
      // Select all indicators
      //
      } else if (event_str.equals("select all indicators")) {
        if (native_messaging_map.containsKey("page") &&
            native_messaging_map.get("page").containsKey(native_messaging_last_url_str)) {
          setSelectedEntities(native_messaging_map.get("page").get(native_messaging_last_url_str).indicators); }

      //
      // Select visible indicators
      //
      } else if (event_str.equals("select visible indicators")) {
        if (native_messaging_map.containsKey("visible") &&
            native_messaging_map.get("visible").containsKey(native_messaging_last_url_str)) {
          setSelectedEntities(native_messaging_map.get("visible").get(native_messaging_last_url_str).indicators); }
      } else System.err.println("Unknown Native Messaging Event: \"" + event_str + "\"");
    } catch (Throwable t) {
      t.printStackTrace(System.err);
    }
  }
  
  /**
   * Simple capture of what's in the browser...
   */
  class WebInfo { String url; String title; Set<String> indicators;
                  public WebInfo(String my_url, String my_title, Set<String> my_indicators) {
                    this.url = my_url; this.title = my_title; this.indicators = my_indicators;
                  } };

  /**
   * Update the native messaging indicator map with the the specified mode and key (url of the page).
   *
   *@param key       key to use for the web page -- the url
   *@param mode      either "visible" or "page"
   *@param web_info  indicator info about the page
   */
  private void updateNativeIndicatorMap(String key, String mode, WebInfo web_info) {
    if (native_messaging_map.containsKey(mode) == false) native_messaging_map.put(mode, new HashMap<String,WebInfo>());
    native_messaging_map.get(mode).put(key, web_info);
  }

  /**
   * Native messaging state...
   *
   * "Visible"|"Page" to url to WebInfo
   * ...  assumes that the url is consistent/cohesive...
   */
  private Map<String,Map<String,WebInfo>> native_messaging_map = new HashMap<String,Map<String,WebInfo>>();
  
  /**
   * Last URL message...
   */
  private String native_messaging_last_url_str = "";

  /**
   * Last Title message...
   */
  private String native_messaging_last_title_str = "";

  /**
   * Last Event... only applies to the content script events... not the user popup events
   */
  private String native_messaging_last_event_str = "";

  /**
   * Last Event Mode ... either "page" or "visible"
   */
  private String native_messaging_last_event_mode_str = "page";

  /**
   * String for storing native messaging filter list
   */
  static String native_messaging_store = "native_messaging.filter";

  /**
   * Transform a json array of indicators into a string set.  Filter out
   * any indicators that the user has indicated shouldn't be consumed.
   *
   *@param array json array of indicators
   *
   *@return string set of indicators post filtering
   */
  private Set<String> transformAndFilterIndicators(JsonArray array) {
    // Ignore if null or empty...
    if (array == null || array.size() == 0) return new HashSet<String>();

    // Get the filter list...
    String filters[] = RTPrefs.retrieveStrings(native_messaging_store);

    // If it's empty... it hasn't been setup... create it here...
    if (filters == null || filters.length == 0) {
      filters    = new String[4];

      filters[0] = "127.0.0.0/8";    // No Loop Backs

      filters[1] = "10.0.0.0/8";     // No Private IP's
      filters[2] = "172.16.0.0/12";  
      filters[3] = "192.168.0.0/16";

      RTPrefs.store(native_messaging_store, filters);
    }

    // Convert every element
    Set<String> set = new HashSet<String>();
    Iterator<JsonElement> it = array.iterator(); while (it.hasNext()) {
      JsonElement el = it.next(); String str = Utils.removeDQuotes(el.toString());
      if (Utils.filterMatches(filters,str) == false) set.add(str);
    }

    // Return the set
    return set;
  }

  /**
   * Show the RT Control Panel which enables the creation of rendering panels
   * and other management functions.
   */
  public void  showControlPanel() { rt_control_frame.setVisible(true); }

  /**
   * Get the control panel for the application.
   *
   *@return control panel for application
   */
  public RTControlFrame getControlPanel() { return rt_control_frame; }

  /**
   * Add a new RTPanel to the application so that it receives
   * updates to the underlying dataset.
   *
   *@param panel new panel
   */
  public void  addRTPanel   (RTPanel panel)     { synchronized (panels) { panels.add(panel);    } }

  /**
   * Remove a RTPanel from te application so that it will no
   * longer received updates about the current view.
   *
   *@param panel panel to remove
   */
  public void  removeRTPanel(RTPanel panel)     { synchronized (panels) { panels.remove(panel); } }

  /**
   * Return an iterator that goes through the active panels.
   *
   *@return iterator over active panels
   */
  public Iterator<RTPanel> rtPanelIterator() { return panels.iterator(); }

  /**
   * Set of entities that is the current user focus.
   */
  private Set<String> focus_entities = new HashSet<String>();

  /**
   * Set entities under the mouse as the focus.
   *
   *@param entities set the focus to these
   */
  public synchronized void setEntitiesUnderMouse(Set<String> entities) {
    boolean change = false;

    // Make an empty set to simplify the logic
    if (entities == null) entities = new HashSet<String>();

    // Go through the variations
    //
    // Both are empty
    //
    if        (focus_entities.size() == 0 && entities.size() == 0) { return; // No update required

    //
    // New ones are not empty, previous were empty
    //
    } else if (focus_entities.size() == 0 && entities.size() >  0) { focus_entities.addAll(entities); change = true;

    //
    // Previous were set... new are empty
    //
    } else if (focus_entities.size() >  0 && entities.size() == 0) { focus_entities.clear(); change = true;

    //
    // Size differs... must be different
    //
    } else if (focus_entities.size() != entities.size())           { focus_entities.clear(); focus_entities.addAll(entities); change = true;

    //
    // Most difficult case.. need to figure out if they are different or the same
    // - sizes are the same
    //
    } else {
      // Check each .. if there's a single difference, break out of the loop
      Iterator<String> it = entities.iterator(); while (it.hasNext() && change == false) if (focus_entities.contains(it.next()) == false) change = true;

      // If change, copy them over
      if (change) { focus_entities.clear(); focus_entities.addAll(entities); }
    }

    //
    // Notify all of the panels if there was a change
    // - should there be a new thread for each notification?
    //
    if (change) {
      Iterator<RTPanel> it = panels.iterator(); while (it.hasNext()) { 
        RTPanel panel = it.next(); Set<String> copy = new HashSet<String>(); copy.addAll(focus_entities);
        panel.updateFocusEntities(copy);
      }
    }
  }

  /**
   * Set a single entity that is under the mouse as the focus.
   */
  public void setEntityUnderMouse(String entity) {
    Set<String> set = new HashSet<String>(); set.add(entity); setEntitiesUnderMouse(set);
  }

  /**
   * Return the username that ran this application.  Useful for sourcing comments.
   *
   *@return user name for application proces
   */
  public static String getUserName() { return System.getProperty("user.name"); }

  /**
   * Denote if stats should be overlaid
   *
   *@return true if overlay stats enabled
   */
  public boolean overlayStats()         { return rt_control_frame.overlayStats(); }

  /**
   * Denote if brushing/highlights are enabled
   *
   *@return true if brushing is enabled
   */
  public boolean highlight()            { return rt_control_frame.highlight(); }

  /**
   * Denote if first order brushing is enabled.
   *
   *@return true if first order brushing is enabled
   */
  public boolean highlightFirstOrder()  { return rt_control_frame.highlightFirstOrder(); }

  /**
   * Denote if second order brushing is enabled.
   *
   *@return true if second order brushing is enabled
   */
  public boolean highlightSecondOrder() { return rt_control_frame.highlightSecondOrder(); }

  /**
   * Set the highlights / brushed elements to be shown across view components.
   *
   *@param set              bundles/records directly under the mouse
   *@param set_p            first order bundles/records near the mouse
   *@param set_pp           second order bundles/records a little bit further from the mouse
   *@param highlight_source source component that set the highlights, used to deconflict the replace brushing method
   */
  public void setHighlights(Set<Bundle> set, Set<Bundle> set_p, Set<Bundle> set_pp, RTPanel highlight_source) {
    boolean need_update = false;
    this.highlight_source = highlight_source;

    // First order
    if        (highlights == null && set == null) {
    } else if (highlights == null)                { highlights = set; need_update = true;
    } else if (set        == null)                { highlights = set; need_update = true;
    } else if (highlights.size() != set.size())   { highlights = set; need_update = true;
    } else                                        { Iterator<Bundle> it;
                                                    boolean differ = false;
                                                    it = set.iterator(); while (it.hasNext()) { Bundle bundle = it.next(); if (highlights.contains(bundle) == false) { differ = true; break; } }
                                                    if (differ) { highlights = set; need_update = true; } }
    // Second order (p)
    if        (need_update)                           { highlights_p = set_p;
    } else if (highlights_p == null && set_p == null) {
    } else if (highlights_p == null)                  { highlights_p = set_p; need_update = true;
    } else if (set_p        == null)                  { highlights_p = set_p; need_update = true;
    } else if (highlights_p.size() != set_p.size())   { highlights = set_p; need_update = true;
    } else                                            { Iterator<Bundle> it;
                                                        boolean differ = false; it = set_p.iterator(); 
                                                        while (it.hasNext()) { Bundle bundle = it.next(); if (highlights_p.contains(bundle) == false) { differ = true; break; } }
                                                        if (differ) { highlights_p = set_p; need_update = true; } }
    // Third order (pp)
    if        (need_update)                             { highlights_pp = set_pp;
    } else if (highlights_pp == null && set_pp == null) {
    } else if (highlights_pp == null)                   { highlights_pp = set_pp; need_update = true;
    } else if (set_pp        == null)                   { highlights_pp = set_pp; need_update = true;
    } else if (highlights_pp.size() != set_pp.size())   { highlights = set_pp; need_update = true;
    } else                                              { Iterator<Bundle> it;
                                                          boolean differ = false; it = set_pp.iterator(); 
                                                          while (it.hasNext()) { Bundle bundle = it.next(); if (highlights_pp.contains(bundle) == false) { differ = true; break; } }
                                                          if (differ) { highlights_pp = set_pp; need_update = true; } }
    boolean re_render = false;
    if (rt_control_frame.highlightsReplace()) {
      Set<Bundle> all = new HashSet<Bundle>();
      if (highlights    != null && highlights.size()    > 0) all.addAll(highlights);
      if (highlights_p  != null && highlights_p.size()  > 0) all.addAll(highlights_p);
      if (highlights_pp != null && highlights_pp.size() > 0) all.addAll(highlights_pp);
      if (all.size() > 0) highlight_bundles = top().subset(all); else highlight_bundles = null;
      re_render = true;
    } else highlight_bundles = null;

    // Update if change
    if (need_update) { updateHighlights(re_render); }
  }

  /**
   * Return a set of bundles/records right under the mouse.
   *
   *@return bundles under mouse
   */
  public Set<Bundle> getHighlights()                 { return highlights; }

  /**
   * Return a set of bundles/records near the mouse.
   *
   *@return bundles near mouse
   */
  public Set<Bundle> getHighlightsP()                { return highlights_p; }

  /**
   * Return a set of bundles/records a little further from the mouse.
   *
   *@return bundles a little further from the mouse
   */
  public Set<Bundle> getHighlightsPP()               { return highlights_pp; }

  /**
   * Denote if the the background should be darked by default when brushing is enabled and active.
   *
   *@return true if background is to be darkened
   */
  public boolean         darkenBackgroundForHighlights() { return rt_control_frame.darkenBackground(); }

  /**
   * Position (in milliseconds since the epoch) of the mouse cursor in time-based components
   */
  long dyn_ts = 0L;

  /**
   * Set the position (in ms) of the dynamic time marker.
   *
   *@param ts      new position of time
   *@param source  source component issuing the update
   */
  public void setDynamicTimeMarker(long ts, JPanel source) { dyn_ts = ts;   }

  /**
   * Return the position of the dynamic time marker.
   *
   *@return position
   */
  public long getDynamicTimeMarker()                       { return dyn_ts; }

  /**
   * Update the components with the new highlights.
   *
   *@return re_render true if the components should just replace and not brush their views
   */
  public void updateHighlights(boolean re_render) {
    Bundles highlight_copy = highlight_bundles;
    Iterator<RTPanel> it = panels.iterator();
    while (it.hasNext()) { 
      RTPanel panel = it.next(); 
      if      (re_render && highlight_copy != null && panel != highlight_source) panel.setBundles(highlight_bundles);
      else if (re_render && highlight_copy == null && panel != highlight_source) panel.setBundles(top());
      else                                                                       panel.highlight(getHighlights(), getHighlightsP(), getHighlightsPP()); 
    }
  }

  /**
   * Cycle through the highlight (brush) settings.
   *
   *@param amount to increment by
   */
  public void adjustHighlightSetting(int amount) { rt_control_frame.adjustHighlightSetting(amount); }

  /**
   * For the specified document, highlight extract entities in their approrpriae places.  Return the
   * extracted entities as {@link SubText}.
   *@param document      document containing text for entity extraction and highlighting
   *@param style_context style context needed to cause the document to have styles
   *
   *@return              list of extracted entity elements (and their locations)
   */
  public List<SubText> colorizeDocument(DefaultStyledDocument document, StyleContext style_context) {
    return colorizeDocument(document, style_context, true);
  }

  /**
   * For the specified document, highlight extract entities in their approrpriae places.  Return the
   * extracted entities as {@link SubText}.
   *
   *@param document      document containing text for entity extraction and highlighting
   *@param style_context style context needed to cause the document to have styles
   *@param highlight     indicates that the document should be highlighted
   *
   *@return              list of extracted entity elements (and their locations)
   */
  public List<SubText> colorizeDocument(DefaultStyledDocument document, StyleContext style_context, boolean highlight) {
    List<SubText> al            = new ArrayList<SubText>();
    try {
      // Get the text
      String all_text = document.getText(0,document.getLength());
      // Initialize the gui components
      document.setCharacterAttributes(0, document.getLength(), style_context.getStyle(StyleContext.DEFAULT_STYLE), true);
      // Now, get all the relationships and colorize them appropriately
      al = EntityExtractor.list(all_text);
      if (highlight) for (int i=0;i<al.size();i++) {
        // Set the foreground/background color
        SubText subtext = al.get(i); Color fg = RTColorManager.getColor("annotate", "labelfg"), bg = RTColorManager.getColor("annotate", "labelbg");
        if        (subtext instanceof Entity)       { fg = Color.white;  bg = RTColorManager.getColor(subtext.toString());
        } else if (subtext instanceof Relationship) { fg = Color.yellow; bg = Color.darkGray;
        } else if (subtext instanceof Interval)     { fg = Color.orange; bg = Color.darkGray;
        } else if (subtext instanceof TimeStamp)    { fg = Color.orange; bg = Color.darkGray;
        } else System.err.println("colorDocument():  Unknown SubText Type \"" + subtext + "\"");
        // Modify the text
        String str = subtext.toString(); int i0 = subtext.getIndex0(), i1 = subtext.getIndex1();
        Style style = style_context.addStyle(str,null);
        style.addAttribute(StyleConstants.Foreground, fg);
        style.addAttribute(StyleConstants.Background, bg);
        document.setCharacterAttributes(i0, i1 - i0, style, true);
      }
    } catch (BadLocationException ble) { }
    return al;
  }

  /**
   * Excerpt map
   */
  Map<String,Set<SubText>> excerpt_map = new HashMap<String,Set<SubText>>();

  /**
   * Return the current excerpt map.
   *
   *@return excerpt map -- should never be null
   */
  public Map<String,Set<SubText>> getExcerptMap() { return excerpt_map; }

  /**
   * Set the excerpt map -- this exchanges entities with the subtexts
   * and their corresponding excerpts.
   *
   *@param e_map mapping from entity to set of subtexts
   */
  public void setExcerptMap(Map<String,Set<SubText>> e_map) {
    if (e_map != null) {
      boolean diff = false;

      // Check to see if it differs...
      // - Key set size
      if (excerpt_map.keySet().size() != e_map.keySet().size()) diff = true;
      // - Each 
      if (!diff) {
        Iterator<String> it = excerpt_map.keySet().iterator();
              while (it.hasNext() && diff == false) {
                String entity = it.next();
                if (e_map.containsKey(entity) && e_map.get(entity).size() == excerpt_map.get(entity).size()) {
                  Iterator<SubText> it_sub = excerpt_map.get(entity).iterator();
                  while (it_sub.hasNext() && diff == false) {
                    if (e_map.get(entity).contains(it_sub.next()) == false) diff = true;
                  }
                } else diff = true;
              }
      }

      // If it is truly different, set the value and request a repaint across all windows
      if (diff) { excerpt_map = e_map; repaintAll(); }
    }
  }

  /**
   * Entities to highlight (not the same as brushing)
   */
  Set<SubText> entity_highlights = new HashSet<SubText>();

  /**
   * Set the entities to highlight.  By default, these entities are extracted
   * from unstructured text and are therefore structured as {@link SubText}.
   *
   *@param eh entities to higlight
   */
  public void setEntityHighlights(Set<SubText> eh) {
    int already_showing_count = entity_highlights.size();
    // Make sure the entity highlights are never null
    if (eh == null) entity_highlights = new HashSet<SubText>(); 
    else            entity_highlights = eh;
    // Try to avoid needless repaints
    Iterator<SubText> it = entity_highlights.iterator(); Set<SubText> to_add = new HashSet<SubText>();
    while (it.hasNext()) {
      SubText subtext = it.next();
      if (subtext instanceof Entity) {
        Entity entity = (Entity) subtext;
        switch (entity.getDataType()) {
          case IPv4CIDR:  to_add.addAll(getRootBundles().getGlobals().getCIDRMatches(entity, entity.toString()));
                          break;
          default:        break;
        }
      }
    }
    entity_highlights.addAll(to_add);
    //
    if (entity_highlights.size() == 0 && already_showing_count == 0) { } else repaintAll();
  }

  /**
   * Get the entities to highlight (not the same as brushing).
   *
   *@return List of SubTexts describing which entities to highlight
   */
  public Set<SubText> getEntityHighlights() { return entity_highlights; }

  /**
   * List of objects that need to receive updates when time markers are added/removed
   */
  List<TimeMarkerListener> time_marker_listeners = new ArrayList<TimeMarkerListener>();

  /**
   * Add a new object to the time marker listener list. These objects will receive updates
   * when the time marker list changes.
   *
   *@param tml new object to add to the time marker listeners
   */
  public void addTimeMarkerListener    (TimeMarkerListener tml) { time_marker_listeners.add(tml);    }

  /**
   * Remove an object from the time marker listener list.
   *
   *@param tml object to remove from time marker listeners
   */
  public void removeTimeMarkerListener (TimeMarkerListener tml) { time_marker_listeners.remove(tml); }

  /**
   * List that keeps track of the time markers
   */
  List<TimeMarker> time_markers_list = new ArrayList<TimeMarker>();

  /**
   * Set that maintains information on the unique ID's related to time markers.
   */
  Set<UUID>         time_marker_uuids = new HashSet<UUID>();

  /**
   * Remove all of time markers from the application and update the listeners.
   */
  public void clearTimeMarkers() { 
    time_markers_list.clear(); time_marker_uuids.clear();
    notifyTimeMarkerListeners();
    refreshAll(); 
  }

  /**
   * Add a time marker to the application.
   *
   *@param decription written description of that mark in time
   *@param timestamp  millisecond timestamp
   */
  public void addTimeMarker(String description, long timestamp) {
    TimeMarker tm;
    time_markers_list.add(tm = new TimeMarker(description, timestamp)); time_marker_uuids.add(tm.getUUID());
    notifyTimeMarkerListeners(tm);
    refreshAll(); }

  /**
   * Add a time marker (interval) to the application.
   *
   *@param decription written description of that mark in time
   *@param ts0        millisecond timestamp at beginning of the inerval
   *@param ts1        milliseoncd timestamp at end of the interval
   */
  public void addTimeMarker(String description, long ts0, long ts1) {
    TimeMarker tm;
    time_markers_list.add(tm = new TimeMarker(description, ts0, ts1)); time_marker_uuids.add(tm.getUUID());
    notifyTimeMarkerListeners(tm);
    refreshAll(); }

  /**
   * Add an entire list of time markers to the application.  Useful for loading them
   * in bulk on initialization or after they've been saved to a file.
   *
   *@param tms list of time markers to add
   */
  public void addTimeMarkers(List<TimeMarker> tms) {
    Iterator<TimeMarker> it = tms.iterator();
    while (it.hasNext()) {
      TimeMarker tm = it.next();
      if (time_marker_uuids.contains(tm.getUUID()) == false) { 
        time_markers_list.add(tm); time_marker_uuids.add(tm.getUUID()); 
      }
    }
    notifyTimeMarkerListeners();
    refreshAll(); }

  /**
   * Remove a specific time marker from the application.
   *
   *@param tm time marker to remove
   */
  public void removeTimeMarker(TimeMarker tm) {
    time_markers_list.remove(tm);
    time_marker_uuids.remove(tm.getUUID());
    notifyTimeMarkerListeners();
    refreshAll();
  }

  /**
   * Method to notify the list of time marker listeners that a new time
   * marker has been created.
   *
   *@param tm newly create time marker
   */
  private void notifyTimeMarkerListeners(TimeMarker tm) {
    for (int i=0;i<time_marker_listeners.size();i++) time_marker_listeners.get(i).newTimeMarker(tm);
  }

  /**
   * Method to notify the list of time marker listeners that the list
   * of time markers has changed.
   */
  private void notifyTimeMarkerListeners() {
    for (int i=0;i<time_marker_listeners.size();i++) time_marker_listeners.get(i).timeMarkerListChanged();
  }

  /**
   * Return the number of time markers.
   *
   *@return number of time markers
   */
  public int  getNumberOfTimeMarkers() { return time_markers_list.size(); }

  /**
   * Return the time marker in the specified slot.
   *
   *@param  i index of time marker to return
   *
   *@return   time marker at index i
   */
  public TimeMarker getTimeMarker(int i) { return time_markers_list.get(i); }

  /**
   * Get the set of time markers between the specified times.
   *
   *@param ts0 initial timestamp
   *@param ts1 last timestamp
   *
   *@return set of timemarkers between the specified times
   */
  public Set<TimeMarker> getTimeMarkers(long ts0, long ts1) {
    Set<TimeMarker> set = new HashSet<TimeMarker>();
    Iterator<TimeMarker> it = time_markers_list.iterator();
    while (it.hasNext()) {
      TimeMarker tm = it.next();
      if (tm.ts1() < ts0 || tm.ts0() > ts1) { } else set.add(tm);
    }
    return set;
  }

  /**
   * Return the entier list of time markers.
   *
   *@return list of time markers
   */
  public List<TimeMarker> getTimeMarkers() {
    return time_markers_list;
  }

  /**
   * Set of entities that are currently selected
   */
  Set<String> selected_entities = new HashSet<String>();

  /**
   * Return the set of the selected entities.
   *
   *@return set of selected entities
   */
  public Set<String> getSelectedEntities() { return new HashSet<String>(selected_entities); }

  /**
   * Set the selected entities and notify all views to repaint themselves (not re-render) -- generic version...
   *
   *@param new_selection    newly selected entities
   */
  public void            setSelectedEntities(Set<String> new_selection) { setSelectedEntities(new_selection, false); }

  /**
   * Set the selected entities and notify all views to repaint themselves (not re-render).
   * May need to make the selection process more abstract so that this class does not need
   * to worry about CIDR (and other types) of matches.
   *
   *@param new_selection    newly selected entities
   *@param report_entities  if true, will take any selected reports (by title) and add those entities to the selection
   */
  public void            setSelectedEntities(Set<String> new_selection, boolean report_entities) {
    // Create a copy so that the caller can't change the structure
    Set<String> set = new HashSet<String>(); set.addAll(new_selection);
    new_selection = set;

    // Special upfront work for report entities
    Map<String,Set<Bundle>> title_to_bundle = new HashMap<String,Set<Bundle>>(); Map<Tablet,KeyMaker> kms_lu = new HashMap<Tablet,KeyMaker>();
    if (report_entities) {
      BundlesG         globals = getRootBundles().getGlobals();
      Iterator<Tablet> it_tab  = getRootBundles().tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next();
        if (tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TITLE_STR)) && tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TEXT_STR))) {
          KeyMaker titles_km = new KeyMaker(tablet, BundlesUtils.REPORT_TITLE_STR);
          Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
            Bundle bundle = it_bun.next();
            if (kms_lu.containsKey(bundle.getTablet()) == false) kms_lu.put(bundle.getTablet(), new KeyMaker(bundle.getTablet(), BundlesUtils.REPORT_TEXT_STR + "|" + BundlesDT.ENTITY_EXTRACTOR));
            String title[] = titles_km.stringKeys(bundle);
            if (title != null) for (int i=0;i<title.length;i++) {
              if (title_to_bundle.containsKey(title[i]) == false) title_to_bundle.put(title[i], new HashSet<Bundle>());
              title_to_bundle.get(title[i]).add(bundle);
            }
          }
        }
      }
    }

    // Create a "to_add" for expansion
    Set<String> to_add = new HashSet<String>();
    Iterator<String> it = new_selection.iterator();
    while (it.hasNext()) {
      String str = it.next();

      // Special cases for strings - CIDR
      if (getControlPanel().includeCIDRMatchesOnSelection() && str.indexOf("/") >= 0) { // abc
        if (Utils.isIPv4CIDR(str)) to_add.addAll(getRootBundles().getGlobals().getCIDRMatches(str));
      }

      // Special cases for strings - Report Titles - Get their entities
      if (report_entities && title_to_bundle.containsKey(str)) {
        Iterator<Bundle> it_bun = title_to_bundle.get(str).iterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next(); if (kms_lu.containsKey(bundle.getTablet())) {
            String ents[] = kms_lu.get(bundle.getTablet()).stringKeys(bundle);
            if (ents != null && ents.length > 0) for (int i=0;i<ents.length;i++) to_add.add(ents[i]);
          }
        }
      }
    }

    // Finalize the selection
    new_selection.addAll(to_add); this.selected_entities = new_selection;

    // Notify panels that the selection has changed
    Iterator<RTPanel> it_panel = panels.iterator(); while (it_panel.hasNext()) it_panel.next().updateSelection();

    // Ask for all components to repaint (not re-render... selections are overlaid on top of the renderings)
    repaintAll();
  }

  /**
   * For a set of titles, find reports that match that title.  Assumes that reports
   * will have unique titles.
   *
   *@param titles set of titles to find
   *
   *@return set of bundles that match those titles
   */
  public synchronized Set<Bundle> findReportsByTitles(Set<String> titles) {
    // Make the titles safe
    if (titles == null) titles = new HashSet<String>();

    // Put lower case versions of the titles into the set... get rid of any null values (not really sure they are in here)
    Set<String> to_add = new HashSet<String>();
    Iterator<String> it = titles.iterator(); while (it.hasNext()) {
      String title = it.next();
      if (title == null) it.remove(); else to_add.add(title.toLowerCase());
    }
    titles.addAll(to_add);

    // Create the return structure
    Set<Bundle>      found   = new HashSet<Bundle>();
    BundlesG         globals = getRootBundles().getGlobals();

    // Go through the tablets and identify the reporting ones
    Iterator<Tablet> it_tab  = getRootBundles().tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next();
      // Reporting tablets will have these fields
      if (tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TITLE_STR)) && tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TEXT_STR))) {
        // Create a keymaker for the title
        KeyMaker titles_km = new KeyMaker(tablet, BundlesUtils.REPORT_TITLE_STR);
        // Go through each bundle and see if has a matching title
        Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next(); String title[] = titles_km.stringKeys(bundle);
          if (title != null) for (int i=0;i<title.length;i++) { if (titles.contains(title[i])) found.add(bundle); }
        }
      }
    }
    return found;
  }

  /**
   * Determine if a string matches a report title.  Not the most efficient way to do this...
   *
   *@param str string to test as a title
   *
   *@return true if the string matches a report title
   */
  public boolean isReportTitle(String str) {
    Set<String> titles  = new HashSet<String>(); titles.add(str);
    Set<Bundle> bundles = findReportsByTitles(titles);
    return bundles.size() > 0;
  }

  /**
   * Return the set of report texts for this title.  The title may equate to more than one report.
   * As a result, a set of the report texts is returned.
   *
   *@param title title to seach for (exact match)
   *
   *@return report text(s)
   */
  public Set<String> getReportText(String title) {
    Set<String>      return_set = new HashSet<String>();
    Set<String>      titles     = new HashSet<String>(); titles.add(title);
    Set<Bundle>      reports    = findReportsByTitles(titles);
    Iterator<Bundle> it_bun     = reports.iterator(); while (it_bun.hasNext()) {
      Bundle   report  = it_bun.next();
      KeyMaker km      = new KeyMaker(report.getTablet(), BundlesUtils.REPORT_TEXT_STR);
      String   texts[] = km.stringKeys(report);
      if (texts != null && texts.length > 0) for (int i=0;i<texts.length;i++) return_set.add(texts[i]);
    }
    return return_set;
  }

  /**
   * Return the title for a single report.
   *
   *@param report_bundle single bundle from the reports tablet
   *
   *@return report title for the bundle
   */
  public String getReportTitle(Bundle report_bundle) {
    KeyMaker     km       = new KeyMaker(report_bundle.getTablet(), BundlesUtils.REPORT_TITLE_STR);
    String       titles[] = km.stringKeys(report_bundle);
    StringBuffer sb       = new StringBuffer();
    if (titles != null && titles.length > 0) {
      if (titles.length == 1) sb.append(titles[0]); // Should always be this one...
      else for (int i=0;i<titles.length;i++) sb.append(titles[i] + " ");
    }
    return sb.toString();
  }

  /**
   * Return the text for a single report.
   *
   *@param report_bundle single bundle from the reports tablet
   *
   *@return report text for the bundle
   */
  public String getReportText(Bundle report_bundle) {
    KeyMaker     km      = new KeyMaker(report_bundle.getTablet(), BundlesUtils.REPORT_TEXT_STR);
    String       texts[] = km.stringKeys(report_bundle);
    StringBuffer sb      = new StringBuffer();
    if (texts != null && texts.length > 0) {
      if (texts.length == 1) sb.append(texts[0]); // Should always be this one...
      else for (int i=0;i<texts.length;i++) sb.append(texts[i] + " ");
    }
    return sb.toString();
  }
  
  /**
   * Filter out the reports that don't contain at least one of the eneities selected.
   */
  public void filterReportsWithoutSelectedEntities() {
    // Get the selection... make sure it has something in it
    Set<String> selected = getSelectedEntities(); if (selected == null || selected.size() == 0) return;

    // Work with the visible bundles
    Bundles visible = getVisibleBundles(); BundlesG globals = visible.getGlobals();
    Set<Bundle> to_keep = new HashSet<Bundle>();

    // Go through the tablets and identify the reporting ones
    Iterator<Tablet> it_tab  = visible.tabletIterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next();

      //
      // Reporting tablets will have these fields
      //
      if (tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TITLE_STR)) && tablet.hasField(globals.fieldIndex(BundlesUtils.REPORT_TEXT_STR))) {
        // Create a keymaker for the entity extractor
        KeyMaker ents_km = new KeyMaker(tablet, BundlesUtils.REPORT_TEXT_STR + "|" + BundlesDT.ENTITY_EXTRACTOR);

        Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
          Bundle bundle = it_bun.next();
          String ents[] = ents_km.stringKeys(bundle);
          int i = 0; boolean found = false; while (i < ents.length && found == false) { 
            if (selected.contains(ents[i]) || selected.contains(ents[i].toLowerCase())) found = true; 
            i++; 
          }
          if (found) to_keep.add(bundle);
        }

      //
      // Else just add the complete tablet to the next pushed bundle
      //
      } else { to_keep.addAll(tablet.bundleSet()); }
    }

    // Push the new bundle set to the visible
    push(visible.subset(to_keep));
  }

  /**
   * List of objects that need to be updated when entity tags are added or removed.
   */
  List<EntityTagListener> entity_tag_listeners = new ArrayList<EntityTagListener>();

  /**
   * Add a new entity tag listener to the list.
   *
   *@param etl new entity tag listener to add
   */
  public void addEntityTagListener    (EntityTagListener etl) { entity_tag_listeners.add(etl);    }

  /**
   * Remove an entity tag listener from the list.
   *
   *@param etl entity tag listener to remove 
   */
  public void removeEntityTagListener (EntityTagListener etl) { entity_tag_listeners.remove(etl); }

  /**
   * Return the number of entity tags.
   *
   *@return number of entity tags
   */
  public int                  getNumberOfEntityTags()        { return entity_tags_list.size(); }

  /**
   * Return the entity tag at the specified index
   *
   *@param  i index of entity tag to return
   *
   *@return   entity tag at specific index
   */
  public EntityTag            getEntityTag(int i)            { if (i >= 0 && i < entity_tags_list.size()) return entity_tags_list.get(i); else return null; }

  /**
   * Get the list of entity tags.
   *
   *@return list of entity tags
   */
  public List<EntityTag> getEntityTags()                { return entity_tags_list; }

  /**
   * Return the set of entities that match the specified tag.
   *
   *@param  tag tag to match against
   *
   *@return     entities that match specified tag
   */
  public Set<String>      getEntitiesWithTag(String tag) {
    Set<String>     ret   = new HashSet<String>();
    Iterator<EntityTag> it_et = entity_tags_list.iterator();
    while (it_et.hasNext()) {
      EntityTag et = it_et.next();
      if (et.getTag().equals(tag)) { ret.add(et.getEntity()); }
    }
    return ret;
  }

  /**
   * List of entity tags in the application
   */
  List<EntityTag>                 entity_tags_list  = new ArrayList<EntityTag>();

  /**
   * Unique ID's for the entity tags in the application.  UUID's are set when
   * the entity tag is created and do not get changed.
   */
  Set<UUID>                        entity_tags_uuids = new HashSet<UUID>();

  /**
   * Mapping of entities to entity tags
   */
  Map<String,List<EntityTag>> entity_tags       = new HashMap<String, List<EntityTag>>();
  
  /**
   * List of types for tag type pairs
   */
  Set<String>                      tag_types         = new HashSet<String>();

  /**
   * For the selected entities, tag them with the specific tags.  For the newly
   * created entity tags, set the specified timestamps as to the first and last
   * heard for those entity-tag pairings.  At the end of the routine, update
   * the view accordingly to show the tags.
   *
   *@param tags tags to apply to selected entities
   *@param ts0  first heard
   *@param ts1  last heard
   */
  public void tagSelectedEntities(String tags, long ts0, long ts1) {

    System.err.println("tagSelectedEntities() - Need (1) fix timeframe frame for tags to bundles (maybe) (2) bridges between two tags");

    if (selected_entities.size() == 0) return;
    int prev_tag_types_size = tag_types.size();
    // Tokenize tags
    List<String> as_set = Utils.tokenizeTags(tags); if (as_set == null || as_set.size() == 0) return;
    String tokenized[] = new String[as_set.size()]; 
    Iterator<String> it = as_set.iterator(); for (int i=0;i<tokenized.length;i++) {
      tokenized[i] = it.next();
      if (Utils.tagIsTypeValue(tokenized[i])) tag_types.add((Utils.separateTypeValueTag(tokenized[i]))[0]);
    }
    // Apply to selected entities
    it = selected_entities.iterator();
    while (it.hasNext()) {
      String entity = it.next(); entity = Utils.removeUniquifier(entity);
      if (entity_tags.containsKey(entity) == false) entity_tags.put(entity, new ArrayList<EntityTag>());
      for (int i=0;i<tokenized.length;i++) {
        boolean merged = false; EntityTag et = null;
        Iterator<EntityTag> it_ets = entity_tags.get(entity).iterator();
        // Determine if the entity -> tag pair exists, if so try to merge
        while (it_ets.hasNext() && merged == false) {
          et = it_ets.next();
          if (et.getTag().equals(tokenized[i])) {
            if        (et.ts0() <= ts0 && et.ts1() >= ts1) { // Duplicate...  merged by default
              // System.err.println("Duplicate Tag Timeframe (\"" + entity + "\")");
              merged = true;
            } else if (ts0 < et.ts0() && ts1 >  et.ts1())  { // Expansion
              // System.err.println("Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts0(ts0); et.ts1(ts1); merged = true;
            } else if (ts0 < et.ts0() && ts1 >= et.ts0())  { // Merge
              // System.err.println("Pre Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts0(ts0); merged = true;
            } else if (ts0 < et.ts1() && ts1 >  et.ts1())  { // Merge
              // System.err.println("Post Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts1(ts1); merged = true;
            }
          }
        }
        // If not merged, create a new entity tag
        if (merged == false) {
          entity_tags.get(entity).add(et = new EntityTag(entity, tokenized[i], ts0, ts1));
          entity_tags_list.add(et); entity_tags_uuids.add(et.getUUID());
          notifyEntityTagListeners(et);
        }
      }
    }
    if (tag_types.size() != prev_tag_types_size) { for (int i=0;i<panels.size();i++) panels.get(i).updateEntityTagTypes(tag_types); }
    refreshAll();
  }

  /**
   * Add a collection of entity tags to the application.  Useful for bulk loading
   * entity tags (for example, at application start time).
   *
   *@param ets entity tags to load
   */
  public void addEntityTags(Collection<EntityTag> ets) {
    int prev_tag_types_size = tag_types.size();
    Iterator<EntityTag> it = ets.iterator();
    while (it.hasNext()) {
      EntityTag et = it.next();
      // check for the existence of the entity in the data set
      // if (getRootBundles().getGlobals().containsEntity(et.getEntity()) == false) continue;
      // Add the tag type if applicable
      // System.err.println("tag = " + et.getTag() + " (" + Utils.tagIsTypeValue(et.getTag()) + ")"); // DEBUG
      if (Utils.tagIsTypeValue(et.getTag())) tag_types.add((Utils.separateTypeValueTag(et.getTag()))[0]);
      // Check to make sure the UUID is unique
      if (entity_tags_uuids.contains(et.getUUID()) == false) {
        // Add the lookup entry
        if (entity_tags.containsKey(et.getEntity()) == false) entity_tags.put(et.getEntity(), new ArrayList<EntityTag>());
        // Update the lists, maps, etc.
        entity_tags.get(et.getEntity()).add(et);
        entity_tags_list.add(et); entity_tags_uuids.add(et.getUUID());
        notifyEntityTagListeners(et);
      }
    }

    // System.err.println("tag_types = " + tag_types); // DEBUG

    // Update the menus for the new tag types
    if (tag_types.size() != prev_tag_types_size) { for (int i=0;i<panels.size();i++) panels.get(i).updateEntityTagTypes(tag_types); }
    refreshAll();

    // Make a note that there needs to be a de-duplicator...
    System.err.println("addEntityTags() -- should implement a deduplicator for entity tags");
  }

  /**
   * Remove a specific entity tag from the application.
   *
   *@param et entity tag to remove
   */
  public void removeEntityTag(EntityTag et) {
    entity_tags.get(et.getEntity()).remove(et);
    entity_tags_list.remove(et);
    entity_tags_uuids.remove(et.getUUID());
    notifyEntityTagListeners();
    refreshAll();
  }

  /**
   * Tag the specified entity with the specified tag with an infinite timeframe
   *
   *@param entity entity to tag
   *@param tags   tags to apply
   */
  public void tagEntity(String entity, String tags) {
    tagEntity(entity, tags, EntityTag.t0_forever, EntityTag.t1_forever);
  }

  /**
   * Tag the specified entity with the specified tag over the specified timeframe.
   *
   *@param entity entity to tag
   *@param tags   tags to apply
   *@param ts0    first heard for the entity-tag pairing
   *@param ts1    last heard for the entity-tag pairing
   */
  public void tagEntity(String entity, String tags, long ts0, long ts1) {
    int prev_tag_types_size = tag_types.size();
    // Tokenize tags
    List<String> as_set = Utils.tokenizeTags(tags); if (as_set == null || as_set.size() == 0) return;
    String tokenized[] = new String[as_set.size()]; 
    Iterator<String> it = as_set.iterator(); for (int i=0;i<tokenized.length;i++) {
      tokenized[i] = it.next();
      if (Utils.tagIsTypeValue(tokenized[i])) tag_types.add((Utils.separateTypeValueTag(tokenized[i]))[0]);
    }
    // Do the tags
    if (entity_tags.containsKey(entity) == false) entity_tags.put(entity, new ArrayList<EntityTag>());
    for (int i=0;i<tokenized.length;i++) {
        // REFACTOR - Duplicate for the tagSelectedEntities...
        boolean merged = false; EntityTag et = null;
        Iterator<EntityTag> it_ets = entity_tags.get(entity).iterator();
        // Determine if the entity -> tag pair exists, if so try to merge
        while (it_ets.hasNext() && merged == false) {
          et = it_ets.next();
          if (et.getTag().equals(tokenized[i])) {
            if        (et.ts0() <= ts0 && et.ts1() >= ts1) { // Duplicate...  merged by default
              // System.err.println("Duplicate Tag Timeframe (\"" + entity + "\")");
              merged = true;
            } else if (ts0 < et.ts0() && ts1 >  et.ts1())  { // Expansion
              // System.err.println("Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts0(ts0); et.ts1(ts1); merged = true;
            } else if (ts0 < et.ts0() && ts1 >= et.ts0())  { // Merge
              // System.err.println("Pre Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts0(ts0); merged = true;
            } else if (ts0 < et.ts1() && ts1 >  et.ts1())  { // Merge
              // System.err.println("Post Expansion Tag Timeframe (\"" + entity + "\")");
              et.ts1(ts1); merged = true;
            }
          }
        }
        // If not merged, create a new entity tag
        if (merged == false) {
          entity_tags.get(entity).add(et = new EntityTag(entity, tokenized[i], ts0, ts1));
          entity_tags_list.add(et); entity_tags_uuids.add(et.getUUID());
          notifyEntityTagListeners(et);
        }
    }
    // Check to see if the types changed
    // System.err.println("tag_types.size() = " + tag_types.size());
    if (tag_types.size() != prev_tag_types_size) { 
      for (int i=0;i<panels.size();i++) 
        panels.get(i).updateEntityTagTypes(tag_types); 
    }
    refreshAll();
  }

  /**
   * Remove the entity tags for the selected entities.
   * QUESTION should we remove these from local storage?
   */
  public void clearEntityTagsForSelectedEntities() { clearEntityTags(selected_entities); }

  /**
   * Clear the entity tags for the specified collection of entities.
   *
   *@param entities entities to remove tags from
   */
  public void clearEntityTags(Collection<String> entities) {
    // Make sure the input is valid
    if (entities == null || entities.size() == 0) return;
    // Go through the data
    Iterator<String> it = entities.iterator();
    while (it.hasNext()) { 
      String entity = it.next();
      if (entity_tags.containsKey(entity)) {
        List<EntityTag> ets = entity_tags.get(entity);
        entity_tags.remove(entity);
        Iterator<EntityTag> it_et = ets.iterator();
        while (it_et.hasNext()) entity_tags_uuids.remove(it_et.next().getUUID());
        entity_tags_list.removeAll(ets);
      }
    }
    notifyEntityTagListeners();
    refreshAll();
  }

  /**
   * For the selected entities, replace the tag specified.  If the entity didn't have the
   * old tag, just give it this one.  Assumes it's a type value tag...
   */
  public void replaceEntityTagForSelectedEntities(String new_tag) {
    Set<String> dupe = new HashSet<String>(); dupe.addAll(selected_entities); // Use the dupe for iteration in case it changes
    Iterator<String> it = dupe.iterator(); while (it.hasNext()) {
      String entity = it.next(); 

      // Check for the existance
      if (entity_tags.containsKey(entity)) {
        // Determine which to remove
        Set<EntityTag> to_remove = new HashSet<EntityTag>();
        List<EntityTag> ets = entity_tags.get(entity);  Iterator<EntityTag> it_et = ets.iterator(); while (it_et.hasNext()) {
          EntityTag et = it_et.next();
          if (tagsMatchReplacement(new_tag, et)) to_remove.add(et); 
        }

        // Remove them
        it_et = to_remove.iterator(); while (it_et.hasNext()) removeEntityTag(it_et.next());
      }

      // Add the new tag
      tagEntity(entity, new_tag, getVisibleBundles().ts0(), getVisibleBundles().ts1());
    }

    notifyEntityTagListeners();
    refreshAll();
  }

  /**
   * Does the tag match?  For replacement purposes....
   */
  private boolean tagsMatchReplacement(String tag_to_remove, EntityTag et) {
    String tag = et.getTag();
    //
    // Tag is type value
    //
    if (Utils.tagIsTypeValue(tag)) {
      String tag_sep[] = Utils.separateTypeValueTag(tag);

      //
      // For replacement purposes, as long as the type matches, return true
      //
      if     (Utils.tagIsTypeValue(tag_to_remove)) {
        String tag_to_remove_sep[] = Utils.separateTypeValueTag(tag_to_remove);
        return tag_sep[0].equals(tag_to_remove_sep[0]);
      } else {
        return tag_sep[0].equals(tag_to_remove);
      }

    //
    // Otherwise requires an exact match (but doesn't correctly handle hierarchical... which have
    // never been used...)
    //
    } else {
      return tag_to_remove.equals(tag);
    }
  }

  /**
   * For the selected entities, remove the specified tag.
   *
   *@param tag_to_remove tag to remove
   */
  public void removeEntityTagForSelectedEntities(String tag_to_remove) {
    Set<String> dupe = new HashSet<String>(); dupe.addAll(selected_entities); // Use the dupe for iteration in case it changes
    Iterator<String> it = dupe.iterator(); while (it.hasNext()) {
      String entity = it.next(); 

      // Determine what to remove
      Set<EntityTag> to_remove = new HashSet<EntityTag>();
      List<EntityTag> ets = entity_tags.get(entity);  Iterator<EntityTag> it_et = ets.iterator(); while (it_et.hasNext()) {
        EntityTag et = it_et.next();
        if (tagsMatchRemoval(tag_to_remove, et)) { to_remove.add(et); }
      }

      // Remove them 
      it_et = to_remove.iterator(); while (it_et.hasNext()) removeEntityTag(it_et.next());
    }

    notifyEntityTagListeners();
    refreshAll();
  }

  /**
   * Does the tag match?  For removal purposes....
   */
  private boolean tagsMatchRemoval(String tag_to_remove, EntityTag et) {
    String tag = et.getTag();
    //
    // Tag is type value
    //
    if (Utils.tagIsTypeValue(tag)) {
      String tag_sep[] = Utils.separateTypeValueTag(tag);

      //
      // If the tag_to_remove has both parts (type and value), then require them to be the same...
      // ... but if the tag_to_remove is just a part of it, just require equality of the type part...
      //
      if     (Utils.tagIsTypeValue(tag_to_remove)) {
        String tag_to_remove_sep[] = Utils.separateTypeValueTag(tag_to_remove);
        return tag_sep[0].equals(tag_to_remove_sep[0]) &&
               tag_sep[1].equals(tag_to_remove_sep[1]);
      } else {
        return tag_sep[0].equals(tag_to_remove);
      }

    //
    // Otherwise requires an exact match (but doesn't correctly handle hierarchical... which have
    // never been used...)
    //
    } else {
      return tag_to_remove.equals(tag);
    }
  }

  /**
   * Get the set of types for the type-value entity tags.
   *
   *@return set of types
   */
  public Set<String> getEntityTagTypes() { return tag_types; }

  /**
   * Get the tags for the specified entity with consideration for the
   * specified timeframe.
   *
   *@param  entity entity to find the tags for
   *@param  ts0    start of timeframe
   *@param  ts1    end of timeframe
   *
   *@return        matching tags
   */
  public Set<String> getEntityTags(String entity, long ts0, long ts1) { 
    Set<String>  set = new HashSet<String>();
    if (entity_tags.containsKey(entity) == false) return set;
    Iterator<EntityTag> it = entity_tags.get(entity).iterator();
    while (it.hasNext()) {
      EntityTag et = it.next();
      if        (et.isForever())                           { set.add(et.getTag());   // Tag applies all the time - tag added
      } else if (getRootBundles().ts0() == Long.MAX_VALUE) { set.add(et.getTag());   // Bundles don't have time - tag added
      } else if (et.ts1() < ts0 || et.ts0() > ts1)         {                         // Bundle time does not match bundles - tag NOT added
      } else                                               { set.add(et.getTag()); } // Else tag should be added
    }
    return set;
  }

  /**
   * Notify entity tag listeners that a new entity tag has been added.
   *
   *@param et new entity tag added
   */
  private void notifyEntityTagListeners(EntityTag et) {
    for (int i=0;i<entity_tag_listeners.size();i++) {
      try { entity_tag_listeners.get(i).newEntityTag(et); } catch (RuntimeException re) {
        System.err.println("RuntimeException occured while notifying the tag listeners");
        re.printStackTrace(System.err);
      }
    }
  }

  /**
   * Notify entity tag listeners that the list of entity tags has changed.
   */
  private void notifyEntityTagListeners() {
    for (int i=0;i<entity_tag_listeners.size();i++) {
      try { entity_tag_listeners.get(i).entityTagListChanged(); } catch (RuntimeException re) {
        System.err.println("RuntimeException occured while notifying the tag listeners");
        re.printStackTrace(System.err);
      }
    }
  }

  /**
   * Get the user-specified color by string.
   *
   *@return color by string
   */
  public String getColorBy() { return rt_control_frame.getColorBy(); }

  /**
   * Get the user-specified count by string
   *
   *@return count by string
   */
  public String getCountBy() { return rt_control_frame.getCountBy(); }

  /**
   * Runnable class to refresh the views concurrently.
   */
  class RefreshThread implements Runnable {
    RTPanel panel;
    public RefreshThread(RTPanel panel) { this.panel = panel; }
    public void run() { panel.setBundles(top()); }
  }

  /**
   * Method to create multiple threads that refresh the known panels (visualizations).
   */
  public void refreshAll() {
    Iterator<RTPanel> it = panels.iterator();
    while (it.hasNext()) {
      RTPanel rt_panel = it.next();
      // System.err.println("Refreshing Panel " + rt_panel); // DEBUG
      (new Thread(new RefreshThread(rt_panel))).start();
    }
    if (rt_control_frame != null) rt_control_frame.repaint();
  }

  /**
   * Method to request a repaint across all of the views.  Note that this isn't
   * multithreaded because it is assumed that repainting is much faster than
   * re-rendering.
   */
  public void repaintAll() {
    Iterator<RTPanel> it = panels.iterator();
    while (it.hasNext()) it.next().repaint();
    if (rt_control_frame != null) rt_control_frame.repaint();
  }

  /**
   * Load a data file.
   *
   *@param file       file to load
   *
   *@return application configuration data embedded in the loaded file
   */
  public List<String> load(File file) throws IOException {
    // Take the real data out of line
    Bundles       root       = getRootBundles(); 
    List<Bundles> orig_stack = bundles_stack, 
                  tmp_stack  = new ArrayList<Bundles>();
                  tmp_stack.add(new BundlesRecs());
    bundles_stack_i = 0; bundles_stack = tmp_stack;
    // Load the file
    System.err.println("Loading File : " + file);
    List<String> appconfs = new ArrayList<String>(); long t0 = System.currentTimeMillis();
    Set<Bundle> set = BundlesUtils.parse(root, this, file, appconfs);
    long t1 = System.currentTimeMillis(); System.err.println("  Done Loading File : " + file + " (" + (t1-t0) + " milliseconds)");
    // Put the real data back in line
    bundles_stack = orig_stack;
    // Update the panels
    updatePanelsForNewBundles(set);
    // Return the application configuration information (if any)
    return appconfs;
  }

  /**
   * Update the "bys" and tell all of the panels about the new bundles.
   *
   *@param set new bundle set
   */
  public void updatePanelsForNewBundles(Set<Bundle> set) {
    // Update the panels
    updateBys();
    // Let panels know that new bundles were added
    Iterator<RTPanel> it = panels.iterator(); while (it.hasNext()) it.next().newBundlesAdded(set);
  }

  /**
   * Update the "By..." fields within each panel.  This is necessary if the fields change
   * or transforms are enabled.
   */
  public void updateBys() {
    // Update the main control panel
    rt_control_frame.updateBys();
    // Update the other panels
    Iterator<RTPanel> it = panels.iterator(); while (it.hasNext()) it.next().updateBys();
  }

  /**
   * Examine the data fields and determine if there is a predefined template for the GUI.
   *
   *@return list of gui configuration strings
   */
  public List<String> profileForData() {
    // Get the flavors that this data supports
    Map<String,Map<String,String>> flavors = StatsOverlay.dataFlavors(getRootBundles().bundleSet());
    if (flavors != null && flavors.keySet().size() > 0) {
      String       flavor     = flavors.keySet().iterator().next(); // Choose randomly... should choose by the highest volume tablet...
      // Find the setting for that configuration (settings are for 1920x1080)
      String config[] = null;
      for (int i=0;i<gui_configs.length;i++) {
        if (gui_configs[i][0].equals(flavor)) {
          config = new String[gui_configs[i].length - 1]; // First entry is the flavor name
          for (int j=1;j<gui_configs[i].length;j++) config[j-1] = gui_configs[i][j];
          break;
        }
      }
      if (config == null) return null;

      // Replace the fields with the translations
      List<String> gui_config = new ArrayList<String>(); Map<String,String> lu = flavors.get(flavor);
      for (int i=0;i<config.length;i++) {
        String str = config[i]; // Utils.decFmURL(config[i]);
        Iterator<String> it = lu.keySet().iterator(); while (it.hasNext()) {
          String key = it.next(); String val = lu.get(key); if (key.equals(val)) continue;
          str = str.replace(Utils.encToURL(key), Utils.encToURL(val));
        }
        gui_config.add(str);
      }
      return gui_config;
    } else return null;
  }

  /**
   * Print the license for the application.
   */
  public static void printLicense() {
    System.err.println(
"\n\nLicense Information\n\n" +
"Copyright 2021 David Trimm\n\n" +
"Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
"you may not use this file except in compliance with the License.\n" +
"You may obtain a copy of the License at\n\n" +
"http://www.apache.org/licenses/LICENSE-2.0\n\n" +
"Unless required by applicable law or agreed to in writing, software\n" +
"distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
"WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
"See the License for the specific language governing permissions and\n" +
"limitations under the License.\n"
    );
  }

  /**
   * Print additional information providing libraries used, credit, etc.
   */
  public static void printLibraries() {
    System.err.println(
"GeoLite2 data created by MaxMind, available from http://www.maxmind.com.\n" +
"\n" +
"UASParser library (http://user-agent-string.info/) available under the Creative Commons license.\n" +
"\n" +
"SQLite library (http://www.sqlite.org/) available within the public domain.\n" +
"\n" +
"Java Matrix Library (http://math.nist.gov/javanumerics/jama/) available in the public domain.\n" + 
"\n" +
"Resistive Distance methods derived from http://the-lost-beauty.blogspot.com/2009/04/moore-penrose-pseudoinverse-in-jama.html (Ahmed Abdelkader).\n" +
"\n" +
"Colorgorical Schemes: @article{gramazio-2017-ccd, author={Gramazio, Connor C. and Laidlaw, David H. and Schloss, Karen B.},\n" +
"                               journal={IEEE Transactions on Visualization and Computer Graphics},title={Colorgorical: creating\n" +
"                                discriminable and preferable color palettes for information visualization},year={2017}}\n" +
"\n" +
"UMAP:  @article{2018arXivUMAP, author = {{McInnes}, L. and {Healy}, J. and {Melville}, J.}, title = \"{UMAP: Uniform Manifold Approximation\n" +
"                and Projection for Dimension Reduction}\", journal = {ArXiv e-prints}, archivePrefix = \"arXiv\", eprint = {1802.03426},\n" +
"                primaryClass = \"stat.ML\", keywords = {Statistics - Machine Learning, Computer Science - Computational Geometry,\n" +
"                Computer Science - Learning}, year = 2018, month = feb }\n" +
"\n" +
"MALLET:  McCallum, Andrew Kachites.  \"MALLET: A Machine Learning for Language Toolkit.\"\n" +
"                http://mallet.cs.umass.edu. 2002.\n" +
"\n" +
"Brewer Color Schemas: Cythia Brewer, https://colorbrewer2.org\n" +
"\n" +
"Geo representations made with Natural Earth.\n" // https://github.com/nvkelso/natural-earth-vector
      );
  }

  /**
   * Main routine for the application.  Creates the framework class {@link RT} and then
   * loads files for the application.
   *
   *@param args command line arguments -- in this case, input data files
   */
  public static void main(String args[]) {

    // Look for a -nobanner option ... otherwise, show the banner info (license, credits)
    boolean show_banner = true;
    for (int i=0;i<args.length;i++) { if (args[i].equals("-nobanner")) show_banner = false; }
    if (show_banner) { printLicense(); printLibraries(); }

    // Flag to indicate that the application should look for native messaging on standard input
    boolean native_messaging = false;

    // Parse the files on the command line
    try {
      List<File> files = new ArrayList<File>();
      for (int i=0;i<args.length;i++) {
        if        (args[i].equals("-nogeo")) {
          GeoData.disableGeoService();
        } else if (args[i].equals("-nobanner")) {
        } else if (args[i].equals("-nativemessaging")) { native_messaging = true;
        } else {
          File file = new File(args[i]);
          if (file.exists()) { files.add(file); } else System.err.println("File \"" + args[i] + "\" Doesn't Exist!");
        }
      }
      RT rt = new RT(native_messaging); List<String> last_appconf = null;
      rt.getControlPanel().disableRenders();
      for (int i=0;i<files.size();i++) {
        try { 
          last_appconf = rt.load(files.get(i));
        } catch (IOException ioe) { System.err.println("IOException : " + ioe); }
      }
      if (last_appconf != null && last_appconf.size() > 0) {
        if (JOptionPane.showConfirmDialog(rt.getControlPanel(), "Apply GUI Configuration From File?", "Apply GUI Config", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
          rt.getControlPanel().applyGUIConfiguration(last_appconf, true);
        }
      } else {
        List<String> data_profile = rt.profileForData();
        if (data_profile != null && data_profile.size() > 0 &&
            JOptionPane.showConfirmDialog(rt.getControlPanel(), "Apply Profile For Data?", "Apply GUI Profile", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
          rt.getControlPanel().applyGUIConfiguration(data_profile, false);
        }
      }
      rt.getControlPanel().enableRenders();
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }

  /**
   * GUI Configurations
   */
  String gui_configs[][] = 
    { { StatsOverlay.NETFLOW_FULL,
      "#AC Application Configuration|0.0|0.0|500.0|300.0|%7cbundles%7c|dpt|",
      "#AC RTPanelFrame|LINKNODE|0de27a57-9458-4cd4-af45-5ded99fe754c|0.0|300.0|966.0|780.0",
      "#AC 0 RTGraphPanel|nodesize=Large|nodecolor=Default|linksize=Normal|linkcolor=Gray|linktrans=false|arrows=false|timing=false|strict=true|dynlabels=true|nodelabels=false|linklabels=false|nlabels=|clabels=|llabels=|relates=sip%7cSquare%7cfalse%7cdip%7cSquare%7cfalse%7cStyle%2b%252d%2bSolid%7ctrue",
      "#AC RTPanelFrame|TEMPORAL|3be50ffe-f822-4092-8cdb-b894a30e69c5|970.0|878.0|950.0|202.0",
      "#AC 0 RTTimePanel|charttype=Bars|count=%7cdefault%7c|mapper=Continuous+4p|fixed=false|log=false|markers=true|aggregate=false",
      "#AC RTPanelFrame|HISTOGRAMx5s|ba156f09-d87a-4b1a-a537-17853c691973|970.0|0.0|947.0|452.0",
      "#AC 0 RTHistoPanel|binby=sip|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 1 RTHistoPanel|binby=spt|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 2 RTHistoPanel|binby=pro|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 3 RTHistoPanel|binby=dpt|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 4 RTHistoPanel|binby=dip|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC RTPanelFrame|XYtTb|e377578c-5f20-41f0-a876-e13189b28afe|970.0|452.0|950.0|426.0",
      "#AC 0 RTXYPanel|xaxis=%7cTm%7cStraight%7c|xscale=Linear|yaxis=OCTS|y2axis=%7cnone%7c|yscale=Log|width=LARGE|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true",
      "#AC 1 RTXYPanel|xaxis=%7cTm%7cStraight%7c|xscale=Linear|yaxis=sip|y2axis=dip|yscale=Equal|width=LARGE|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true",
      "#AC RTPanelFrame|XY|f8e0a665-447f-43ab-b340-d15a96d9ad1f|501.0|0.0|465.0|299.0",
      "#AC 0 RTXYPanel|xaxis=SOCTS|xscale=Log|yaxis=DOCTS|y2axis=%7cnone%7c|yscale=Log|width=LARGE|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true"
      },
      { StatsOverlay.NETFLOW_VOLUME,
      "#AC Application Configuration|0.0|0.0|500.0|300.0|%7cbundles%7c|dpt|",
      "#AC RTPanelFrame|HISTOGRAMx5s|05250eb5-c73e-4d13-b8cc-a6067ca2142e|860.0|0.0|1060.0|519.0",
      "#AC 0 RTHistoPanel|binby=sip|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 1 RTHistoPanel|binby=spt|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 2 RTHistoPanel|binby=pro|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 3 RTHistoPanel|binby=dpt|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 4 RTHistoPanel|binby=dip|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC RTPanelFrame|TEMPORAL|10c7ae0f-a8ea-4799-b388-f08f786ae180|859.0|791.0|1061.0|289.0",
      "#AC 0 RTTimePanel|charttype=Bars|count=%7cdefault%7c|mapper=Continuous+4p|fixed=false|log=false|markers=true|aggregate=false",
      "#AC RTPanelFrame|LINKNODE|13451bd6-1544-49b7-8909-a870fc2babe3|0.0|300.0|859.0|780.0",
      "#AC 0 RTGraphPanel|nodesize=Large|nodecolor=Default|linksize=Normal|linkcolor=Gray|linktrans=false|arrows=false|timing=false|strict=true|dynlabels=true|nodelabels=false|linklabels=false|nlabels=|clabels=|llabels=|relates=sip%7cSquare%7cfalse%7cdip%7cSquare%7cfalse%7cStyle%2b%252d%2bSolid%7ctrue",
      "#AC RTPanelFrame|XY|35ba42d7-06eb-479a-be39-f41e84f41ba6|500.0|0.0|360.0|300.0",
      "#AC 0 RTXYPanel|xaxis=PKTS|xscale=Log|yaxis=OCTS|y2axis=%7cnone%7c|yscale=Log|width=LARGE|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true",
      "#AC RTPanelFrame|XY|6fb60229-239f-4629-8e14-25ddf3b49682|859.0|519.0|1061.0|272.0",
      "#AC 0 RTXYPanel|xaxis=%7cTm%7cStraight%7c|xscale=Linear|yaxis=sip|y2axis=dip|yscale=Equal|width=MEDIUM|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true"
      },
      { StatsOverlay.NETFLOW_DEFAULT,
      "#AC Application Configuration|0.0|0.0|859.0|298.0|%7cbundles%7c|dpt|",
      "#AC RTPanelFrame|XY|8785f5c1-f33c-4fd6-be32-f062c5b61627|859.0|519.0|1061.0|272.0",
      "#AC 0 RTXYPanel|xaxis=%7cTm%7cStraight%7c|xscale=Linear|yaxis=sip|y2axis=dip|yscale=Equal|width=MEDIUM|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true",
      "#AC RTPanelFrame|TEMPORAL|b77c5147-2ce4-4715-a6b8-b86c83775d2d|859.0|791.0|1061.0|289.0",
      "#AC 0 RTTimePanel|charttype=Bars|count=%7cdefault%7c|mapper=Continuous+4p|fixed=false|log=false|markers=true|aggregate=false",
      "#AC RTPanelFrame|HISTOGRAMx5s|dba45ee7-544a-48c4-9788-2b0b95cc9567|860.0|0.0|1060.0|519.0",
      "#AC 0 RTHistoPanel|binby=sip|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 1 RTHistoPanel|binby=spt|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 2 RTHistoPanel|binby=pro|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 3 RTHistoPanel|binby=dpt|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC 4 RTHistoPanel|binby=dip|binby2=%7cnone%7c|log=true|label=true|tags=true|reverse=false",
      "#AC RTPanelFrame|LINKNODE|faa1b56d-cd89-4db7-be14-0e9df6a15577|0.0|300.0|859.0|780.0",
      "#AC 0 RTGraphPanel|nodesize=Large|nodecolor=Default|linksize=Normal|linkcolor=Gray|linktrans=false|arrows=false|timing=false|strict=true|dynlabels=true|nodelabels=false|linklabels=false|nlabels=|clabels=|llabels=|relates=sip%7cSquare%7cfalse%7cdip%7cSquare%7cfalse%7cStyle%2b%252d%2bSolid%7ctrue"
      },
      { StatsOverlay.NETFLOW_MINIMAL,
      "#AC Application Configuration|0.0|0.0|859.0|298.0|%7cbundles%7c|dpt|",
      "#AC RTPanelFrame|TEMPORAL|5b12444e-6a16-4e57-99ff-7fa3b15f9258|859.0|787.0|1057.0|293.0",
      "#AC 0 RTTimePanel|charttype=Bars|count=%7cdefault%7c|mapper=Continuous+4p|fixed=false|log=false|markers=true|aggregate=false",
      "#AC RTPanelFrame|LINKNODE|9b4307b4-e46c-4735-9dc9-95a89bfc62a1|0.0|300.0|859.0|780.0",
      "#AC 0 RTGraphPanel|nodesize=Large|nodecolor=Default|linksize=Normal|linkcolor=Gray|linktrans=false|arrows=false|timing=false|strict=true|dynlabels=true|nodelabels=false|linklabels=false|nlabels=|clabels=|llabels=|relates=sip%7cSquare%7cfalse%7cdip%7cSquare%7cfalse%7cStyle%2b%252d%2bSolid%7ctrue",
      "#AC RTPanelFrame|HISTOGRAMx3s|b4dd5b36-a9bc-4878-842a-13af93ecf0d8|859.0|0.0|1054.0|517.0",
      "#AC 0 RTHistoPanel|binby=sip|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 1 RTHistoPanel|binby=dpt|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC 2 RTHistoPanel|binby=dip|binby2=%7cnone%7c|log=false|label=true|tags=true|reverse=false",
      "#AC RTPanelFrame|XY|d04f723f-e983-43e3-a8ab-80294a4fa508|859.0|519.0|1056.0|267.0",
      "#AC 0 RTXYPanel|xaxis=%7cTm%7cStraight%7c|xscale=Linear|yaxis=sip|y2axis=dip|yscale=Equal|width=MEDIUM|vcolor=true|duration=true|hlshape=Circular|drawtm=true|drawsh=true"
      } };
}

