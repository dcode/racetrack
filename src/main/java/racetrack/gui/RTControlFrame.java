/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.gui;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Composite;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.zip.GZIPOutputStream;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import racetrack.analysis.NetflowAnalytics;
import racetrack.analysis.TextAnalytics;
import racetrack.analysis.TimeAnalytics;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesG;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesUtils;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

import racetrack.kb.EntityTag;

import racetrack.test.AsyncDataGenerator;

import racetrack.util.CacheManager;
import racetrack.util.CaseInsensitiveComparator;
import racetrack.util.Entity;
import racetrack.util.EntityExtractor;
import racetrack.util.Interval;
import racetrack.util.JREMemComponent;
import racetrack.util.JTextFieldHistory;
import racetrack.util.ShuntingYardAlgorithm;
import racetrack.util.StrSet;
import racetrack.util.SubText;
import racetrack.util.TimeStamp;
import racetrack.util.Utils;

import racetrack.visualization.RTColorManager;

/**
 * Control panel window for the application.  This window allows the creation of
 * all the different views and provides management features for time markers and
 * entity tags.
 *
 * V61 - Transitioned away from cached panel into manipulating records directly.
 * V62 - Added RT Table Component
 * V70 - Begin adding analytics
 * V80 - Removed reports/comments and made them the same as everything else
 * V90 - Not sure yet
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class RTControlFrame extends JFrame implements MouseListener {
  private static final long serialVersionUID = -5169469156525234838L;

  /**
   * Main application parent class
   */
  RT rt;

  /**
   * GUI member to determine if components darken during highlights/brushing
   */
  JCheckBoxMenuItem    darken_cbmi,
  /**
   * GUI member to determin if the components should render -- disabling render is useful
   * during file load and window configuration setup times.
   */
                       render_cbmi;
  /**
   * Include CIDR matches when selecting a CIDR entity
   */
  JCheckBoxMenuItem inc_cidr_on_select_cbmi;

  /**
   * Checkbox Menu Item - No highlights
   */
  JRadioButtonMenuItem hl_none_rbmi, 
  /**
   * Checkbox Menu Item - Normal brushing
   */
                       hl_normal_rbmi, 
  /**
   * Checkbox Menu Item - Normal brushing with one derivative
   */
           hl_normalp_rbmi, 
  /**
   * Checkbox Menu Item - Normal brushing with two derivatives
   */
           hl_normalpp_rbmi, 
  /**
   * Checkbox Menu Item - Replacement brushing
   */
           hl_replace_rbmi, 
  /**
   * Checkbox Menu Item - Replacement brushing with one derivative
   */
           hl_replacep_rbmi, 
  /**
   * Checkbox Menu Item - Replacement brushing with two derivatives
   */
           hl_replacepp_rbmi,
  /**
   * Checkbox Menu Item - Overlay Stats when brusing
   */
                       overlay_stats_rbmi;
  /**
   * Color by dropdown for global coloring
   */
  JComboBox<String>    color_by_cb, 
  /**
   * Count by dropdown for global counting
   */
                       count_by_cb;
  /**
   * Table for managing the entity tags
   */
  JTable               entity_table, 
  /**
   * Table for managing the time markers
   */
           times_table;
  /**
   * Popup menu for the time markers menu (remove rows...)
   */
  PopupListener        times_popup,  
  /**
   * Popup menu for the entity tags menu (remove rows...)
   */
           entity_popup;
  /**
   * Table model for the entity tag table
   */
  EntityTableModel     entity_tm;
  /**
   * Table model for the time marker table
   */
  TimesTableModel      times_tm;
  /**
   * File chooser for loading files
   */
  JFileChooser         file_chooser = new JFileChooser(".");

  /**
   * Tabbed panel for switching between the cache, entity tags, and time marker tables
   */
  JTabbedPane          tabs;

  /**
   * Component to display the current data stack (root and filters)
   */
  StackComponent       stack_component;

  /**
   * Menu storing analyst preferred layouts
   */
  JMenu                layouts_menu;

  /**
   * Text field for expression
   */
  JTextField           expression_tf;

  /**
   * Timeframe is based on the visible data ... default behavior...
   */
  JRadioButtonMenuItem timeframe_is_visible_rbmi,

  /**
   * Timeframe is constrained to a specific set of tablets
   */
                       timeframe_is_constrained_rbmi;
  /**
   * List of the post proc checkbox menu items
   */
  Set<JCheckBoxMenuItem> post_proc_cbmis = new HashSet<JCheckBoxMenuItem>();

  /**
   * Return the version string.
   *
   *@return version string
   */
  public static String getVersion() { return version_str; }

  /**
   * Version string
   */
  private static final String version_str = "2022-02-06";

  /**
   * Construct the control panel with the specified parent class.  Detect
   * if SQL is availble and create the GUI.
   *
   *@param rt parent GUI component
   */
  public RTControlFrame(RT rt) { 
    super("RACETrack Control - " + version_str);
    this.rt  = rt; 
    try { createGUI(); } catch (MalformedURLException mue) { System.err.println("MalURLE: " + mue); }
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
  }

  /**
   * Return the GUI parent.
   *
   *@return GUI parent
   */
  public RT getRTParent() { return rt; }

  /**
   * Return the JFrame for this component.  Useful for the anonymous classes.
   *
   *@return JFrame of this window... the window.
   */
  protected JFrame getJFrame() { return (JFrame) this; }

  /**
   * Create the GUI for the control panel.
   */
  private void createGUI() throws MalformedURLException {
    //
    // Get the base directory   What if we are in a JAR file?
    //
    //URL src_url  = RTGraphPanel.class.getProtectionDomain().getCodeSource().getLocation();
    
    //
    // Create the menu first
    //
    JMenuItem mi; JButton bt;
    JMenuBar  menu_bar  = new JMenuBar();

    // File Menu
    JMenu     file_menu = new JMenu("File");       menu_bar.add(file_menu);
      file_menu.add(mi = new JMenuItem("Load File..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadFile(); } } );
      file_menu.add(mi = new JMenuItem("Import RFC4180 CSV...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadRFC4180CSV(); } } );
      file_menu.add(mi = new JMenuItem("Paste Indicators..."));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { pasteIndicators(); } } );
      file_menu.addSeparator();
      file_menu.add(mi = new JMenuItem("Save Root To File...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveFile(getRTParent().getRootBundles(), false); } } );
      file_menu.add(mi = new JMenuItem("Save Visible To File...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveFile(getRTParent().getVisibleBundles(), true); } } );
      file_menu.addSeparator();

      // Entity Tag Menu Items
      file_menu.add(mi = new JMenuItem("Load Entity Tags From File..."));      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadEntityTagsFromFile();  } } );
      // if (RTStore.sqlAvailable()) { file_menu.add(mi = new JMenuItem("Retrieve Entity Tags From Local DB")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadEntityTagsLocally();  } } ); }
      // file_menu.add(mi = new JMenuItem("Search P2P..."));          mi.setEnabled(false);
      file_menu.add(mi = new JMenuItem("Save Entity Tags To File..."));        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveEntityTagsToFile(); } } );
      // if (RTStore.sqlAvailable()) { file_menu.add(mi = new JMenuItem("Save Entity Tags To Local DB"));       mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveEntityTagsLocally(); } } ); }
      // file_menu.add(mi = new JMenuItem("Publish To P2P..."));      mi.setEnabled(false);
      // file_menu.addSeparator();
      // file_menu.add(mi = new JMenuItem("Consolidate Tag Time Frames...")); mi.setEnabled(false); // Provide a dialog for a slop value -- go through the entity tags and fix them up...
      file_menu.add(mi = new JMenuItem("Convert Entity Tags To Tablet"));      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { convertEntityTagsToTablet(); } } );
      file_menu.addSeparator();

      // Time Marker Menu Items
      file_menu.add(mi = new JMenuItem("Load Time Markers From File..."));       mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { loadTimeMarkersFromFile(); } } );
      // if (RTStore.sqlAvailable()) { file_menu.add(mi = new JMenuItem("Retrieve Time Markers From Local DB"));  mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae)   { loadTimeMarkersLocally();  } } ); }
      // file_menu.add(mi = new JMenuItem("Search P2P..."));           mi.setEnabled(false);
      file_menu.add(mi = new JMenuItem("Save Time Markers To File..."));         mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveTimeMarkersToFile();  } } );
      // if (RTStore.sqlAvailable()) { file_menu.add(mi = new JMenuItem("Save Time Markers To Local DB"));        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveTimeMarkersLocally(); } } ); }
      // file_menu.add(mi = new JMenuItem("Publish To P2P..."));       mi.setEnabled(false);
      // file_menu.addSeparator();
      // file_menu.add(mi = new JMenuItem("Only Keep Bundles In Time Markers")); mi.setEnabled(false);

      file_menu.addSeparator();

      file_menu.add(mi = new JMenuItem("Exit")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { exit(); } } );

    // Preferences Menu
    JMenu     pref_menu = new JMenu("Prefs"); menu_bar.add(pref_menu);
      String theme_strs[] = RTColorManager.getThemes();
      for (int i=0;i<theme_strs.length;i++) {
        pref_menu.add(mi = new JMenuItem(theme_strs[i]));
        mi.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent ae) {
           CacheManager.clearCaches();
           RTColorManager.setTheme(((JMenuItem) ae.getSource()).getText());
           rt.refreshAll();
         }
        } );
      }

      pref_menu.addSeparator();

      ButtonGroup bg = new ButtonGroup(); ActionListener fixtimeframe_al = new ActionListener() { public void actionPerformed(ActionEvent ae) { setTimeframeTablets(); } };
      pref_menu.add(timeframe_is_visible_rbmi     = new JRadioButtonMenuItem("Visible Data Timeframe", true));  bg.add(timeframe_is_visible_rbmi);
        timeframe_is_visible_rbmi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { rt.refreshAll(); } } );
      pref_menu.add(timeframe_is_constrained_rbmi = new JRadioButtonMenuItem("Constrained Timeframe Tablets")); bg.add(timeframe_is_constrained_rbmi);
        timeframe_is_constrained_rbmi.addActionListener(fixtimeframe_al);
        timeframe_is_constrained_rbmi.setToolTipText("Timeframe is constrained to the currently visible tablets");
      pref_menu.add(mi                            = new JMenuItem("Set Constrained Timeframe Tablets"));
        mi.addActionListener(fixtimeframe_al);

      pref_menu.addSeparator();
      pref_menu.add(layouts_menu = new JMenu("Layouts")); fillLayoutsMenu();
      pref_menu.addSeparator();
      pref_menu.add(darken_cbmi   = new JCheckBoxMenuItem("Darken During Highlights",      true));
      pref_menu.addSeparator();
      pref_menu.add(render_cbmi   = new JCheckBoxMenuItem("Render", true));
      pref_menu.addSeparator();
      pref_menu.add(inc_cidr_on_select_cbmi = new JCheckBoxMenuItem("Inc CIDR Matches On Selection", false));
      pref_menu.addSeparator();
      pref_menu.add(mi = new JMenuItem("Manage Indicator Filters..."));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { manageIndicatorFilters(); } } );
      pref_menu.addSeparator();
      pref_menu.add(mi = new JMenuItem("Add Selected To Stop Words"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { TextAnalytics.addStopWords(getRTParent().getSelectedEntities()); } } );
      pref_menu.add(mi = new JMenuItem("Clear Stop Words"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { TextAnalytics.clearStopWords(); } } );
      pref_menu.add(mi = new JMenuItem("Set Selected As Stop Words"));
        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { TextAnalytics.setStopWords(getRTParent().getSelectedEntities()); } } );

    // Highlights Menu
    JMenu     highlights_menu = new JMenu("Highlights"); menu_bar.add(highlights_menu); bg = new ButtonGroup();
      highlights_menu.add(hl_none_rbmi      = new JRadioButtonMenuItem("No Highlights", true)); bg.add(hl_none_rbmi);
      highlights_menu.addSeparator();
      highlights_menu.add(hl_normal_rbmi    = new JRadioButtonMenuItem("Normal (R)"));          bg.add(hl_normal_rbmi);
      highlights_menu.add(hl_normalp_rbmi   = new JRadioButtonMenuItem("Normal+ (R-Y)"));       bg.add(hl_normalp_rbmi);
      highlights_menu.add(hl_normalpp_rbmi  = new JRadioButtonMenuItem("Normal++ (R-Y-G)"));    bg.add(hl_normalpp_rbmi);
      highlights_menu.addSeparator();
      highlights_menu.add(hl_replace_rbmi   = new JRadioButtonMenuItem("Replace"));             bg.add(hl_replace_rbmi);
      highlights_menu.add(hl_replacep_rbmi  = new JRadioButtonMenuItem("Replace+"));            bg.add(hl_replacep_rbmi);
      highlights_menu.add(hl_replacepp_rbmi = new JRadioButtonMenuItem("Replace++"));           bg.add(hl_replacepp_rbmi);
      highlights_menu.addSeparator();
      highlights_menu.add(overlay_stats_rbmi= new JRadioButtonMenuItem("Overlay Stats"));       bg.add(overlay_stats_rbmi);

    // Bundles Menu
    JMenu     bundles_menu = new JMenu("Bundles"); menu_bar.add(bundles_menu);
      // bundles_menu.add(mi = new JMenuItem("Stack Top"));                 mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { stackTop(); } } );
      // bundles_menu.add(mi = new JMenuItem("Re-Push Stack"));             mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { rePushStack(); } } );
      bundles_menu.add(mi = new JMenuItem("Remove Duplicates"));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { pushDeDupe(); } } );
      bundles_menu.add(mi = new JMenuItem("Top Minus Visible"));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { topMinusVisible(); } } );
      bundles_menu.addSeparator();
      bundles_menu.add(mi = new JMenuItem("Copy Slice Descriptions To Clipboard")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { copySliceDescriptionsToClipboard(); } } );
      bundles_menu.addSeparator();
      bundles_menu.add(mi = new JMenuItem("Join Tablets..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { joinTabletsDialog(); } } );
      bundles_menu.addSeparator();
      bundles_menu.add(mi = new JMenuItem("Add & Set Field (Beta)..."));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addAndSetField(); } } );
      bundles_menu.add(mi = new JMenuItem("Swap Field Values (Beta)...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { swapVisibleFieldValues(); } } );
      bundles_menu.add(mi = new JMenuItem("Manipulate Tags (Beta)..."));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { manipulateTags(); } } );
      bundles_menu.add(mi = new JMenuItem("Remove Fields (Beta)..."));     mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFields(); } } );
      bundles_menu.addSeparator();
      bundles_menu.add(mi = new JMenuItem("Set Visible As Root..."));    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setVisibleAsRoot(); } } );
      bundles_menu.addSeparator();
      bundles_menu.add(mi = new JMenuItem("Zeroize Root..."));           mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { zeroizeRoot(); } } );

    // Analytical Views
    JMenu     views_menu = new JMenu("Viz"); menu_bar.add(views_menu);
      views_menu.add(mi = new JMenuItem("Link/Node Graph..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.LINKNODE,       getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Entities..."));                      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.ENTITIES,             getRTParent()); } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("Temporal Graph..."));                mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TEMPORAL,       getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Temporal Graph (x3)..."));           mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TEMPORALx3,     getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Temporal Graph (Grid)..."));         mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TEMPORALg,      getRTParent()); } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("Yet Another Time Panel..."));        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.YAFTP,          getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("YATP Periodics..."));                mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.YAFTPPeriodics, getRTParent()); } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("Histogram..."));                     mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAM,      getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Histogram (s)..."));                 mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMs,     getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Histogram (x3s)..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx3s,   getRTParent()); } } );
      JMenu histos_menu = new JMenu("Histograms"); views_menu.add(histos_menu);
        histos_menu.add(mi = new JMenuItem("Histogram (x5s)..."));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx5s,   getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (x8s)..."));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8s,   getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (x8x2s)..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8x2s, getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (x8x3s)..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8x3s, getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (x8x4s)..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8x4s, getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (x8x5s)..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8x5s, getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (Grid)..."));           mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMg,     getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram (Grid, Simple)..."));   mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMgs,    getRTParent()); } } );
        histos_menu.add(mi = new JMenuItem("Histogram List..."));             mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMLIST,  getRTParent()); } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("TableC..."));                        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TABLEC,         getRTParent());  } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("XY Scatter..."));                    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XY,             getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("XY Scatter (2x1)..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XYsBs,          getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("XY Scatter (1x2)..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XYtTb,          getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("XY Scatter (1x3)..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XYtTbTb,        getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("XY Scatter (1x4)..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XY1x4,          getRTParent()); } } );
      // views_menu.addSeparator();
      // views_menu.add(mi = new JMenuItem("Reports..."));                       mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.REPORTS,        getRTParent()); } } );
      views_menu.addSeparator();
      views_menu.add(mi = new JMenuItem("Box Plot"));                         mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.BOXPLOT,         getRTParent()); } } );
      // views_menu.add(mi = new JMenuItem("Bar Graph"));                        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.BARGRAPH,        getRTParent()); } } );
      views_menu.add(mi = new JMenuItem("Small Multiples"));                  mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.SMALL_MULTIPLES, getRTParent()); } } );
      views_menu.addSeparator();
      JMenu specialty_menu = new JMenu("Specialty Views"); views_menu.add(specialty_menu);
        specialty_menu.add(mi = new JMenuItem("Box Plot x2"));                 mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.BOXPLOTx2,            getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Chi Squared Test..."));         mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.CHI_SQUARED_TEST,     getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Correlation..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.CORRELATE,            getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Day Matrix..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.DAY_MATRIX,           getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Distribution... (Beta)"));      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.DISTRIBUTION,         getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Field Correlation... (Beta)")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.FIELD_CORRELATION,    getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Edge Times..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.EDGE_TIMES,           getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Event Horizon..."));            mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.EVENT_HORIZON,        getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Geospatial Histogram..."));     mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.GEOHISTO,             getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("GPS..."));                      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.GPS,                  getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("MDS... (Beta)"));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.MDS,                  getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Parallel Coordinates..."));     mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.PARALLEL_COORDINATES, getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Pivot..."));                    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.PIVOT,                getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Rug Plot..."));                 mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.RUGPLOT,              getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Stacked Histogram..."));        mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.STACKHISTO,           getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Time Series..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TIMESERIES,           getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Venn Diagram..."));             mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.VENN,                 getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("XY Entity Scatter"));           mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XYENTITY,             getRTParent()); } } );
        // specialty_menu.add(mi = new JMenuItem("Word Cloud..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.WORD_CLOUD,           getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Reports View..."));             mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.REPORTS_VIEW,         getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Deviations..."));               mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.DEVIATIONS,           getRTParent()); } } );
        specialty_menu.add(mi = new JMenuItem("Set Overlap..."));              mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.SET_OVERLAP,          getRTParent()); } } );
      views_menu.addSeparator();
      JMenu combined_menu = new JMenu("Combined Views"); views_menu.add(combined_menu);
        combined_menu.add(mi = new JMenuItem("Temporal/Histo Combo..."));          mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TIME_HISTO,           getRTParent()); } } );
        combined_menu.add(mi = new JMenuItem("LinkNode/Temporal Combo..."));       mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.LINKNODE_TIME,        getRTParent()); } } );
        combined_menu.add(mi = new JMenuItem("LinkNode/Temporal/Histo Combo...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.LINKNODE_TIME_HISTO,  getRTParent()); } } );
    
    // Post Processors
    JMenu post_procs_menu = new JMenu("TForms"); menu_bar.add(post_procs_menu);
    ItemListener post_procs_il = new ItemListener() {
      public void itemStateChanged(ItemEvent ie) {
        JCheckBoxMenuItem cbmi = (JCheckBoxMenuItem) ie.getSource();

        boolean prev_render_state = renderVisualizations();
        try {
          disableRenders();
          if (cbmi.isSelected()) BundlesDT.enablePostProcessor (getRTParent().getRootBundles(), cbmi.getText()); 
          else                   BundlesDT.disablePostProcessor(getRTParent().getRootBundles(), cbmi.getText());
          getRTParent().updateBys();
        } finally { 
          if (prev_render_state) enableRenders(); 
        }
      } };
    String post_procs[] = BundlesDT.listAvailablePostProcessors();
    for (int i=0;i<post_procs.length;i++) {
      JCheckBoxMenuItem cbmi = new JCheckBoxMenuItem(post_procs[i]); post_procs_menu.add(cbmi); post_proc_cbmis.add(cbmi);
      cbmi.addItemListener(post_procs_il);
    }

    //
    // Analytics menu
    //
    JMenu analytics_menu = new JMenu("Analytics"); menu_bar.add(analytics_menu);
      JMenu netflow_analytics_menu;
        analytics_menu.add(netflow_analytics_menu = new JMenu("Netflow"));

          // Uniflow to biflow converter
    netflow_analytics_menu.add(mi = new JMenuItem("Create Biflow Tablet From Uniflow...")); 
          mi.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent ae) { 
              /* Set<Bundle> set = */ (new NetflowAnalytics()).createBiflowFromUniflow(getJFrame(), getRTParent().getRootBundles()); 

              // Finalize the new tablet and bundles
              // Reset the transforms to force the lookups to be created
              Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(getRTParent().getRootBundles());
              getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
              // Update the dropdowns
              getRTParent().updateBys();
          } } );

          // Server or client
    netflow_analytics_menu.add(mi = new JMenuItem("Server or Client")); 
          mi.addActionListener(new ActionListener() { 
            public void actionPerformed(ActionEvent ae) { 
              List<EntityTag> entity_tags = (new NetflowAnalytics()).clientOrServer(getRTParent().getRootBundles());
              getRTParent().addEntityTags(entity_tags);
          } } );


    //
    // Time menu
    //
    JMenu time_menu = new JMenu("Temporal"); analytics_menu.add(time_menu);
    time_menu.add(mi = new JMenuItem("Time Filter Selected & Visible"));
    mi.setToolTipText("<html>" +
                      "For the selected entities, determine where they are"     + " <br>" +
                      "active within the visible set.  Then return all records" + " <br>" +
                      "from the root set that matches them.");
      mi.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
          Set<String> selected = getRTParent().getSelectedEntities(); if (selected == null || selected.size() == 0) return;
          Set<Bundle> matches = TimeAnalytics.bundlesInTimeFrame(selected,
                                                                 getRTParent().getVisibleBundles(), 
                                                                 getRTParent().getRootBundles(), 60L * 60L * 1000L);
          if (matches.size() > 0) { rt.push(getRTParent().getVisibleBundles().subset(matches)); }
        }
      } );

    //
    // Entity Timeframes analytic...
    //
    time_menu.add(mi = new JMenuItem("Entity Timeframes...")); 
    mi.setToolTipText("<html>" + "Creates a new table of first and last heards for entities." + "<br>" +
                                 "Entities are selected by specifying fields.");
      mi.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
          new EntityTimeframesDialog(getRTControlFrame());
        } } );

    //
    // Overlaps between tablets
    //
    time_menu.add(mi = new JMenuItem("Temporal Overlap Between Tablets / Cheap... (Visible Records)"));
    mi.setToolTipText("<html>" + "For entities in a specific tablet / field, find the overlapping entities" + "<br>" +
                                 "in another tablet / field.  Applies only to visible records.");
      mi.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) {
          new TemporalOverlapDialog(getRTControlFrame());
        } } );

    // About menu
    JMenu about_menu = new JMenu("About"); menu_bar.add(about_menu);

    //
    // Dump Threads For Debugging
    //
    about_menu.add(mi = new JMenuItem("Dump Threads (Debug)"));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { 
        Map<Thread,StackTraceElement[]> thread_map = Thread.getAllStackTraces();
        Iterator<Thread> it = thread_map.keySet().iterator();
        while (it.hasNext()) {
          Thread thread = it.next();
          System.err.println("For Thread \"" + thread + "\" -- " + (thread_map.containsKey(thread) && thread_map.get(thread) != null ? thread_map.get(thread).length : "Null"));
          StackTraceElement stack[] = thread_map.get(thread);
          if (stack != null && stack.length > 0) {
            for (int i=0;i<stack.length;i++) System.err.println("  " + stack[i]); 
          }
      }
      } } );

    //
    // Generate data asynchronously and add to the application
    // - Tests where race conditions occur within the render engines / framework
    //
    about_menu.add(mi = new JMenuItem("Add Tablets/Data Async (Debug)"));
    mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
      for (int i=0;i<4;i++) { Thread thread = new Thread(new AsyncDataGenerator(getRTControlFrame())); thread.start(); }
    } } );
     
    // Add the menu bar to the frame
    setJMenuBar(menu_bar);

    //
    // Create the tabs for data management
    //
    tabs = new JTabbedPane();
    JPanel stack_panel = new JPanel(new BorderLayout()); stack_panel.add("Center", stack_component = new StackComponent());
    JPanel expr_panel  = new JPanel(new BorderLayout(5,5)); 
           expr_panel.add("West", new JLabel("Expr"));
           expr_panel.add("Center", expression_tf = new JTextField()); new JTextFieldHistory(expression_tf);
           stack_panel.add("South", expr_panel);
    tabs.add("Stack",    stack_panel);
    tabs.add("Entities", new JScrollPane(entity_table = new JTable(entity_tm = new EntityTableModel()))); entity_table.setAutoCreateRowSorter(true);
    tabs.add("Times",    new JScrollPane(times_table  = new JTable(times_tm  = new TimesTableModel())));  times_table.setAutoCreateRowSorter(true);
    getContentPane().add("Center", tabs);

    //
    // Create the Comboboxes
    //
    JPanel south_panel = new JPanel(new FlowLayout());
    south_panel.add(new JLabel("Color"));
    south_panel.add(color_by_cb = new JComboBox<String>(Utils.remove(KeyMaker.ALL_ENTITIES_STR, Utils.prepend(BundlesDT.COUNT_BY_NONE, KeyMaker.blanks(rt.getRootBundles().getGlobals())))));
    south_panel.add(new JLabel("Count"));
    south_panel.add(count_by_cb = new JComboBox<String>(KeyMaker.blanks(rt.getRootBundles().getGlobals(), true, true, true, true)));
    south_panel.add(bt = new JButton("Top"));   bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { stackTop(); } } );
    getContentPane().add("South", south_panel);

    // Create the memory component
    JPanel north = new JPanel(new BorderLayout());
    north.add("Center", new JREMemComponent());
    getContentPane().add("North", north);

    // Tools
    JPanel tools = new JPanel(new GridLayout(5,2));

    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/linknode.png"))));       bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.LINKNODE,        getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/histos.png"))));         bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMs,      getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/time.png"))));           bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.TEMPORAL,        getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/histo.png"))));          bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAM,       getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/smallmultiples.png")))); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.SMALL_MULTIPLES, getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/histox3.png"))));        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx3s,    getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/xy.png"))));             bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XY,              getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/histox8.png"))));        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.HISTOGRAMx8s,    getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/xy1x2.png"))));          bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.XYtTb,           getRTParent()); } } );
    tools.add(bt = new JButton(new ImageIcon(getClass().getResource("/images/boxplot.png"))));        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { new RTPanelFrame(RTPanelFrame.Type.BOXPLOT,         getRTParent()); } } );

    getContentPane().add("East", tools);

    // Time Marker Popup
    JPopupMenu times_popup_menu = new JPopupMenu();
    mi = new JMenuItem("Remove Row(s)");               times_popup_menu.add(mi);  mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFromTimeMarkers  (times_popup.row_i); } } );
    // mi = new JMenuItem("Remove Row(s) From Local DB"); times_popup_menu.add(mi);  mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFromTimeMarkersDB(times_popup.row_i); } } );
    times_popup_menu.addSeparator();
    mi = new JMenuItem("Filter Viewable");             times_popup_menu.add(mi);  mi.setEnabled(false); // mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { filterViewableToTime(times_popup.row_i, 0L);             } } );
    mi = new JMenuItem("Filter Viewable (+/-5min)");   times_popup_menu.add(mi);  mi.setEnabled(false); // mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { filterViewableToTime(times_popup.row_i, 1000L*60L*5L);   } } );

    // Entity Tag Popup Menu
    JPopupMenu entity_popup_menu = new JPopupMenu();
    mi = new JMenuItem("Remove Row(s)");               entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFromEntityTags(entity_popup.row_i);   } } );
    // mi = new JMenuItem("Remove Row(s) From Local DB"); entity_popup_menu.add(mi); mi.setEnabled(false); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFromEntityTagsDB(entity_popup.row_i); } } );
    entity_popup_menu.addSeparator();
    mi = new JMenuItem("Set As Forever");              entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setEntityTagsToForever(entity_popup.row_i); } } );
    entity_popup_menu.addSeparator();
    mi = new JMenuItem("Set As Selected");             entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyEntityTagsSetOp(entity_popup.row_i, StrSet.Op.SELECT);    } } );
    mi = new JMenuItem("Add To Selected");             entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyEntityTagsSetOp(entity_popup.row_i, StrSet.Op.ADD);       } } );
    mi = new JMenuItem("Remove From Selected");        entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyEntityTagsSetOp(entity_popup.row_i, StrSet.Op.REMOVE);    } } );
    mi = new JMenuItem("Intersect Selected");          entity_popup_menu.add(mi); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyEntityTagsSetOp(entity_popup.row_i, StrSet.Op.INTERSECT); } } );

    //
    // Add the listeners
    //
    ItemListener refresh_all_il = new ItemListener() { public void itemStateChanged(ItemEvent ie) { rt.refreshAll(); } };
    color_by_cb.addItemListener(refresh_all_il);
    count_by_cb.addItemListener(refresh_all_il);
    render_cbmi.addItemListener(refresh_all_il);
    times_table.addMouseListener  (times_popup  = new PopupListener(times_popup_menu,  times_table));
    entity_table.addMouseListener (entity_popup = new PopupListener(entity_popup_menu, entity_table));
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { exit(); } } );
    expression_tf.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { applyExpression(); } } );

    // Show it
    pack(); setSize(500,300); setVisible(true);
  }

  /**
   * Set the timeframe based on either fixed or constrained timeframes.
   */
  public void setTimeframeTablets() {
    Bundles bundles = getRTParent().getVisibleBundles();
    timeframe_tablet_headers.clear();
    Iterator<Tablet> it = bundles.tabletIterator(); while (it.hasNext()) timeframe_tablet_headers.add(it.next().fileHeader());
    rt.refreshAll();
  }

  /**
   * For fixed/constrained timeframe, these are the active tablets...
   */
  protected Set<String> timeframe_tablet_headers = new HashSet<String>();

  /**
   * Determine if a tablet can contribute to a time-based view.
   *
   *@param tablet tablet to check
   *
   *@return true if tablet should contribute to a timeframe
   */
  public boolean renderTimeframeForTablet(Tablet tablet) {
    return timeframe_is_visible_rbmi.isSelected() ||
           timeframe_tablet_headers.contains(tablet.fileHeader());
  }

  /**
   * True if all of the visible data should be used to calculate the timeframe for time-based views.
   *
   *@return true to use all visible data in timeframe determination
   */
  public boolean useAllVisibleAsTimeframe() { return timeframe_is_visible_rbmi.isSelected(); }

  /**
   * Needed for anonymous class access to the 'this' variable for the rt control frame.
   *
   *@return rt control frame
   */
  public RTControlFrame getRTControlFrame() { return this; }

  /**
   * Return the flag indicating that selecting an entity that is a CIDR will also select
   * any matching IP entities.
   *
   *@return true to perform a CIDR selection
   */
  public boolean includeCIDRMatchesOnSelection() { return inc_cidr_on_select_cbmi.isSelected(); }

  /**
   * Instantiate the indicator filters dialog. Probably need to make sure that the user doesn't create more than one...
   */
  public void manageIndicatorFilters() { new ManageIndicatorFiltersDialog(this); }

  /**
   * Map to store the layout configuration strings.
   */
  Map<String,String[]> layout_lu = new HashMap<String,String[]>();

  /**
   * Retrieve the information from prefs about layouts.  Populate the menu with the information.
   */
  public void fillLayoutsMenu() {
    layouts_menu.removeAll(); JMenuItem mi;
    layouts_menu.add(mi = new JMenuItem("Save GUI Layout...")); mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { saveGUILayout(); } } );
    layouts_menu.addSeparator();

    // Get the layout names and settings
    String layouts = RTPrefs.retrieveString("layouts");
    if (layouts != null && layouts.length() > 0) {
      StringTokenizer st = new StringTokenizer(layouts, "|");
      while (st.hasMoreTokens()) {
        String layout_name      = Utils.decFmURL(st.nextToken());
        String layout_setting[] = RTPrefs.retrieveStrings("layout." + Utils.encToURL(layout_name));
  if (layout_setting != null && layout_setting.length > 0) {
    layout_lu.put(layout_name, layout_setting);
  }
      }
    }
    // For the validated ones, add them to the jmenu
    List<String> validated = new ArrayList<String>(); validated.addAll(layout_lu.keySet());
    Collections.sort(validated);
    Iterator<String> it = validated.iterator();
    while (it.hasNext()) {
      String layout_name = it.next();
      layouts_menu.add(mi = new JMenuItem(layout_name));
      mi.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) {
        applyGUILayout(((JMenuItem) ae.getSource()).getText());
      } } );
    }
  }

  /**
   * Apply the expression in the textfield to the visible data.
   */
  public void applyExpression() {
    String expr = expression_tf.getText(); if (Utils.stripSpaces(expr).equals("")) return;
    try {
      ShuntingYardAlgorithm sya = new ShuntingYardAlgorithm(expr);
      Set<Bundle> matches = sya.matches(getRTParent().getVisibleBundles().bundleSet());
      if (matches.size() > 0) rt.push(getRTParent().getVisibleBundles().subset(matches));
    } catch (Throwable t) { JOptionPane.showMessageDialog(file_chooser, "Throwable: " + t, "Expression Error", JOptionPane.ERROR_MESSAGE); }
  }

  /**
   * For a named GUI layout, retrieve the settings and apply them.
   *
   *@param layout_name named layout
   */
  public void applyGUILayout(String layout_name) {
    // System.err.println("applyGUILayout(\"" + layout_name + "\")");
    String settings[] = layout_lu.get(layout_name);
    if (settings != null && settings.length > 0) {
      List<String> list = new ArrayList<String>();
      for (int i=0;i<settings.length;i++) {
        // System.err.println("  " + i + ".  \"" + settings[i] + "\"");
        list.add(settings[i]);
      }
      applyGUIConfiguration(list, false);
    }
  }

  /**
   * Enable user to name the current GUI configuration and save it to the preferences.
   */
  public void saveGUILayout() {
    String layout_name = JOptionPane.showInputDialog(this, "GUI Layout Name", "GUI Layout", JOptionPane.QUESTION_MESSAGE);
    if (layout_name != null) {
      // Get the gui configuration and store it
      List<String> settings = getGUIConfiguration(false, false);
      String strs[] = new String[settings.size()];
      for (int i=0;i<strs.length;i++) strs[i] = settings.get(i);
      RTPrefs.store("layout." + Utils.encToURL(layout_name), strs);
      // Add the layout name to the strings
      String layouts = RTPrefs.retrieveString("layouts");
      if (layouts == null || layouts.equals("")) {
        RTPrefs.store("layouts", Utils.encToURL(layout_name));
      } else {
        StringTokenizer st   = new StringTokenizer(layouts, "|");
  Set<String>     set  = new HashSet<String>(); set.add(layout_name);
        while (st.hasMoreTokens()) set.add(Utils.decFmURL(st.nextToken()));
        // Re-store
        List<String>    list = new ArrayList<String>(); list.addAll(set);
  StringBuffer    sb   = new StringBuffer();
  for (int i=0;i<list.size();i++) {
    if (sb.length() > 0) sb.append("|");
    sb.append(Utils.encToURL(list.get(i)));
  }
  RTPrefs.store("layouts", sb.toString());
      }
      fillLayoutsMenu();
    }
  }

  /**
   * Return true if views should darken their rendering during brushing operationss.
   *
   *@return true if they should darken
   */
  public boolean darkenBackground() { return darken_cbmi.isSelected(); }

  /**
   * Return true if windows/views should re-render themselves.  This option is
   * useful for turning off the rendering while files load / configurations are
   * applied / and window setup times.
   *
   *@return true if windows should render themselves
   */
  public boolean renderVisualizations() { return render_cbmi.isSelected(); }

  /**
   * disable the rendering engines.
   */
  public void disableRenders() { render_cbmi.setSelected(false); }

  /**
   * Enable the rendering engines.
   */
  public void enableRenders() { render_cbmi.setSelected(true); }

  /**
   * Determine if overlay stats is enabled.
   *
   *@return true if overlay stats is enabled
   */
  public boolean overlayStats()         { return overlay_stats_rbmi.isSelected(); }

  /**
   * Determine if highlighting (brushing) is enabled.
   *
   *@return true if highlights are enabled
   */
  public boolean highlight()            { return hl_none_rbmi.isSelected()       == false &&
                                                 overlay_stats_rbmi.isSelected() == false; }

  /**
   * Determine if first order (records near the mouse) is enabled
   *
   *@return true if first order highlighting is on
   */
  public boolean highlightFirstOrder()  { return hl_normalp_rbmi.isSelected()   || hl_normalpp_rbmi.isSelected()  ||
                                                 hl_replacep_rbmi.isSelected()  || hl_replacepp_rbmi.isSelected(); }
  /**
   * Determine if second order (records a little further from the mouse) is enabled
   *
   *@return true if second order highlighting is on
   */
  public boolean highlightSecondOrder() { return hl_normalpp_rbmi.isSelected()  || hl_replacepp_rbmi.isSelected(); }

  /**
   * Determine if components should replace their current view based
   * on highlighted (brushed) records.
   *
   *@return true if views should be in replace mode
   */
  public boolean highlightsReplace()    { return hl_replace_rbmi.isSelected()   ||
                                                 hl_replacep_rbmi.isSelected()  ||
             hl_replacepp_rbmi.isSelected(); }
  /**
   * Cycle through the highlight settings by the specified amount.
   *
   *@param amount to change the highlight settings
   */
  public void    adjustHighlightSetting(int amount) {
    System.err.println("adjustHighlightSetting() - Not Implemented");
  }

  /**
   * Return the global color by string.  Null indicates that no
   * coloring is chosen and that default (logarithmic) coloring
   * will be used.
   *
   *@return global color by string
   */
  public String getColorBy() { 
    if (color_by_cb == null) return null;
    String color_by = (String) color_by_cb.getSelectedItem(); 
    if (color_by != null && color_by.equals(BundlesDT.COUNT_BY_NONE)) color_by = null;
    return color_by;
  }

  /**
   * Return the global count by string.
   *
   *@return global count by string
   */
  public String getCountBy() { 
    if (count_by_cb == null) return null;
    return (String) count_by_cb.getSelectedItem(); 
  }

  /**
   * Update the color by and count by dropdown boxes (most likely
   * because new data has been loaded and the fields have changed).
   */
  public void updateBys() {
    Object color_sel = color_by_cb.getSelectedItem();
    color_by_cb.removeAllItems();

    // Create the valid color-by strings
    String strs[];
    // Make the none first...
    strs = Utils.remove(KeyMaker.ALL_ENTITIES_STR, Utils.prepend(BundlesDT.COUNT_BY_NONE, KeyMaker.blanks(rt.getRootBundles().getGlobals())));
    // Add in the various time-based options that make sense
    strs = Utils.append(strs, KeyMaker.BY_YEAR_STR);
    strs = Utils.append(strs, KeyMaker.BY_YEAR_QUARTER_STR);
    strs = Utils.append(strs, KeyMaker.BY_YEAR_MONTH_STR);
    strs = Utils.append(strs, KeyMaker.BY_QUARTER_STR);
    strs = Utils.append(strs, KeyMaker.BY_MONTH_STR);
    strs = Utils.append(strs, KeyMaker.BY_DAYOFWEEK_STR);
    strs = Utils.append(strs, KeyMaker.BY_HOUR_STR);
    
    // Add to the combobox and set to the previously selected value
    for (int j=0;j<strs.length;j++) color_by_cb.addItem(strs[j]);
    color_by_cb.setSelectedItem(color_sel);

    Object count_sel = count_by_cb.getSelectedItem();
    count_by_cb.removeAllItems();
    strs = KeyMaker.blanks(rt.getRootBundles().getGlobals(), true, true, true, true); for (int j=0;j<strs.length;j++) count_by_cb.addItem(strs[j]);
    count_by_cb.setSelectedItem(count_sel);
  }

  /**
   * Load a data file by providing the user with a file chooser then a set of
   * dialogs to specify the format.
   */
  private void loadRFC4180CSV() {
    file_chooser.setMultiSelectionEnabled(true);
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        File files[] = file_chooser.getSelectedFiles();
  for (int i=0;i<files.length;i++) {
          if (files[i].exists()) {
            new RFC4180ImportDialog(this, files[i]);
    } else throw new IOException("File \"" + files[i].getName() + "\" Not Found");
        }
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(file_chooser, "IOException: " + ioe, "File Load Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Load a data file by providing the user with a file chooser dialog and then
   * having the application parse the file.
   */
  private void loadFile() {
    file_chooser.setMultiSelectionEnabled(true);
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
      boolean prev_render_state = renderVisualizations();
      try {
        disableRenders();
        File files[] = file_chooser.getSelectedFiles(); List<String> last_appconf = null;
        for (int i=0;i<files.length;i++) {
          if (files[i].exists()) {
            last_appconf = getRTParent().load(files[i]);
          } else throw new IOException("File \"" + files[i].getName() + "\" Not Found");
        }
        // If there are no panels open, try to apply the application configuration information (if available)
        if (getRTParent().rtPanelIterator().hasNext() == false && last_appconf.size() > 0) { 
          if (JOptionPane.showConfirmDialog(this, "Apply GUI Configuration From File?", "Apply GUI Config", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            applyGUIConfiguration(last_appconf, true); 
          }
        }
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(file_chooser, "IOException: " + ioe, "File Load Error", JOptionPane.ERROR_MESSAGE);
      } finally { 
        if (prev_render_state) enableRenders(); 
      }
    }
  }

  /**
   * Provide a window to paste a bunch of indicators -- can include unstructured text.
   */
  private void pasteIndicators() { new PasteIndicatorsDialog(this); }

  /**
   * Harvest indicators by placing them into a tablet.  Make sure they are not duplicates.
   *
   *@param url         url where indicators were obtained from
   *@param title       title of page where indicators were obtained from
   *@param indicators  set of indicators
   */
  public void harvestIndicators(String url, String title, Set<String> indicators) {

    long ts = System.currentTimeMillis();

    Set<Bundle> bundles_added = new HashSet<Bundle>();
    Tablet indicator_tablet = findOrCreateIndicatorTablet();

    Iterator<String> it = indicators.iterator(); while (it.hasNext()) {
      String indicator = it.next();

      Map<String,String> attr_map = new HashMap<String,String>();
      attr_map.put(RTControlFrame.IT_INDICATOR_STR, indicator);
      attr_map.put(RTControlFrame.IT_USER_STR,      RT.getUserName());
      attr_map.put(RTControlFrame.IT_TAGS_STR,      "");
      attr_map.put(RTControlFrame.IT_URL_STR,       url);
      attr_map.put(RTControlFrame.IT_TITLE_STR,     title);
      attr_map.put(RTControlFrame.IT_SOURCE_STR,    "Browser Integration");

      Bundle bundle = indicator_tablet.addBundle(attr_map, Utils.exactDate(ts));
      bundles_added.add(bundle);
    }

    // Propagate changes across the application
    Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(getRTParent().getRootBundles());
    getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
    getRTParent().updatePanelsForNewBundles(bundles_added);
  }

  /**
   * Apply a set of configuration lines to the current instance.  Abort if there are already existing panels.
   */
  public void applyGUIConfiguration(List<String> strs, boolean apply_additional_settings) {
    if (getRTParent().rtPanelIterator().hasNext() == true || strs == null || strs.size() == 0) return;

    StringTokenizer st; String last_uuid = "";

    int i = 0;
    while (i < strs.size()) {
      String line = strs.get(i++); if (line.startsWith("#AC") == false) continue;

      //
      // Configure the global application settings
      //
      if        (line.startsWith("#AC Application Configuration")) {
        st = new StringTokenizer(line,"|"); st.nextToken();
        int    x        = (int) Double.parseDouble(st.nextToken()), y = (int) Double.parseDouble(st.nextToken()),
               w        = (int) Double.parseDouble(st.nextToken()), h = (int) Double.parseDouble(st.nextToken());
        String count_by = Utils.decFmURL(st.nextToken()),
               color_by = Utils.decFmURL(st.nextToken());
        setLocation(x,y); setSize(w,h); 

        // Check to see if their are post proc enable strings
        if (st.hasMoreTokens()) {
          StringTokenizer pp_st = new StringTokenizer(Utils.decFmURL(st.nextToken()), "|");
          while (pp_st.hasMoreTokens()) {
            String post_proc = Utils.decFmURL(pp_st.nextToken());
            Iterator<JCheckBoxMenuItem> it = post_proc_cbmis.iterator();
            while (it.hasNext()) {
              JCheckBoxMenuItem cbmi = it.next();
              if (cbmi.getText().equals(post_proc)) cbmi.setSelected(true);
            }
          }
        }

        // Update the JComboBoxes
        if (count_by != null && count_by.equals("null") == false) count_by_cb.setSelectedItem(count_by); 
        if (color_by != null && color_by.equals("null") == false) color_by_cb.setSelectedItem(color_by);

      //
      // Set the theme color
      //
      } else if (line.startsWith("#AC Color")) {
        st = new StringTokenizer(line,"|"); st.nextToken(); String color_str = Utils.decFmURL(st.nextToken());
        RTColorManager.setTheme(color_str);

      //
      // Make a new panel frame
      //
      } else if (line.startsWith("#AC RTPanelFrame"))              {
        st = new StringTokenizer(line,"|"); st.nextToken(); String type_str = st.nextToken(); st.nextToken();
        int    x        = (int) Double.parseDouble(st.nextToken()), y = (int) Double.parseDouble(st.nextToken()),
               w        = (int) Double.parseDouble(st.nextToken()), h = (int) Double.parseDouble(st.nextToken());

        // Escape to remove a historical feature
        if (RTPanelFrame.stringToType(type_str) == RTPanelFrame.Type.REPORTS) { last_uuid = null; continue; }

        // Create the frame and set the position
        RTPanelFrame rt_panel_frame = new RTPanelFrame(RTPanelFrame.stringToType(type_str), getRTParent());
        rt_panel_frame.setLocation(x,y); rt_panel_frame.setSize(w,h); last_uuid = rt_panel_frame.getUUID();

      //
      // Historical removal of Reports view
      //
      } else if (line.startsWith("#AC ") && line.indexOf(" RTReports|") >= 0) {
      } else if (line.startsWith("#AC rep|")) {
      } else if (line.startsWith("#AC reportsend")) {

      //
      // Consume additional settings for one of the panel frames
      //
      } else if (line.startsWith("#AC "))                          {
        st = new StringTokenizer(line, " "); 
        if (st.countTokens() != 3) System.err.println("Configuration Line Without Three Tokens \"" + line + "\"");
        st.nextToken(); int pos = Integer.parseInt(st.nextToken()); String config = st.nextToken();
        if (last_uuid != null && last_uuid.equals("") == false) {
          // Find the component to apply the configuration to
          Iterator<RTPanel> it = getRTParent().rtPanelIterator();
          while (it.hasNext()) {
            RTPanel panel = it.next();
            if (panel.getWinUniq().equals(last_uuid) && panel.getWinPos() == pos) {
              // System.err.println("Setting Config For Panel \"" + panel + "\"");
              panel.setConfig(config);
              if (apply_additional_settings && panel.hasAdditionalConfig()) { i = panel.parseAdditionalConfig(strs, i); }
            }
          }
        }
      }
    }
  }

  /**
   * Save the specific dataset to a file.  Provide the user with a dialog to choose
   * the save file.
   *
   *@param bundles           bundles (records) to save
   *@param save_visible_only save the visible information only (saves space)
   */
  private void saveFile(Bundles bundles, boolean save_visible_only) {
    if (file_chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) 
      saveFile(bundles, file_chooser.getSelectedFile(), save_visible_only);
  }
  /**
   * Save the specific dataset to the specified file.  In addition to saving
   * the dataset, the entity tags, time markers, and comments are also included
   * into the file.  For all intense purposes, the file is a gzipped multi-csv
   * file separated by blank lines with included headers.
   *
   *@param bundes            bundles (records) to save
   *@param file              file to save to
   *@param save_visible_only save the visible information only (saves space)
   */
  public void saveFile(Bundles bundles, File file, boolean save_visible_only) {
      if (file.getName().toLowerCase().endsWith(".mcsv.gz") == false) {
        file = new File(file.getParentFile(), file.getName() + ".mcsv.gz");
      }
      System.err.println("Saving Bundles To File... (" + bundles + ")");
      PrintStream out = null;
      try {
        out = new PrintStream(new GZIPOutputStream(new FileOutputStream(file)));
  // Dump the tablets
  // - Sort the tablets by their header row
        Iterator<Tablet>  it_tab  = bundles.tabletIterator();
  List<Tablet> tablets = new ArrayList<Tablet>();
        while (it_tab.hasNext()) { tablets.add(it_tab.next()); }
  Collections.sort(tablets, new Comparator<Tablet>() { public int compare(Tablet t0, Tablet t1) { return t0.fileHeader().compareTo(t1.fileHeader()); } } );

        it_tab = tablets.iterator(); String last_header = null;
  while (it_tab.hasNext()) { 
    Tablet tablet = it_tab.next(); 

    boolean needs_header = (last_header == null) || (tablet.fileHeader().equals(last_header) == false);
    if (last_header != null && needs_header) out.println("");
    tablet.save(out, needs_header);

    last_header = tablet.fileHeader();
  }

  // Dump the entity tags
  if (getRTParent().getNumberOfEntityTags() > 0) {
    out.println("");
          out.println(EntityTag.getFileHeader());
    for (int i=0;i<getRTParent().getNumberOfEntityTags();i++) out.println(getRTParent().getEntityTag(i).asFileLine());
    out.println("");
  }

  // Dump the timing marks
  if (getRTParent().getNumberOfTimeMarkers()> 0) {
    out.println("");
          out.println(TimeMarker.getFileHeader());
    for (int i=0;i<getRTParent().getNumberOfTimeMarkers();i++) out.println(getRTParent().getTimeMarker(i).asFileLine());
    out.println("");
        }

        // Application configuration
  out.println("");
  List<String> gui_config = getGUIConfiguration(true, save_visible_only);
  for (int i=0;i<gui_config.size();i++) out.println(gui_config.get(i));

        System.err.println("  Bundle Save Successful!");
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(this, "IOException: " + ioe, "File Save Error", JOptionPane.ERROR_MESSAGE);
      } finally { if (out != null) out.close(); }
    }

  /**
   * Return a list of strings (order matters) of the GUI settings.
   *
   *@param include_additional_settings include the additional information to fully restore the state of the component
   *@param save_visible_only           save the visible information only
   *
   *@return list of strings that encompass the GUI state
   */
  public List<String> getGUIConfiguration(boolean include_additional_settings, boolean save_visible_only) {
    List<String> list = new ArrayList<String>();
    String count_by = getCountBy(), color_by = getColorBy();
    // Gather up the post processors
    String       pps[]       = BundlesDT.listEnabledPostProcessors();
    StringBuffer postproc_sb = new StringBuffer();
    if (pps.length > 0) postproc_sb.append(Utils.encToURL(pps[0]));
    for (int i=1;i<pps.length;i++) postproc_sb.append("|" + pps[i]);
     // Add it all to the list
    list.add("#AC Application Configuration" +
       "|" + getLocationOnScreen().getX()  + "|" + getLocationOnScreen().getY() + 
       "|" + getSize().getWidth()          + "|" + getSize().getHeight()        +
       "|" + (count_by == null ? "null" : Utils.encToURL(count_by))  + 
             "|" + (color_by == null ? "null" : Utils.encToURL(color_by))  +
             "|" + Utils.encToURL(postproc_sb.toString()));

    // Save the colors out
    list.add("#AC Color|" + Utils.encToURL(RTColorManager.getTheme()));

    List<RTPanel> sorter = new ArrayList<RTPanel>();
    Iterator<RTPanel> it_panel = getRTParent().rtPanelIterator(); while (it_panel.hasNext()) sorter.add(it_panel.next());
    Collections.sort(sorter, new Comparator<RTPanel>() {
      public int compare(RTPanel p0, RTPanel p1) {
        if (p0.getWinUniq().equals(p1.getWinUniq())) return p0.getWinPos() - p1.getWinPos();
        else                                         return p0.getWinUniq().compareTo(p1.getWinUniq());
      }
    } );
    // - Print them into the file
    it_panel = sorter.iterator(); String last_uniq = "";
    while (it_panel.hasNext()) {
      RTPanel panel = it_panel.next();
      if (panel.getWinUniq().equals(last_uniq) == false) {
        Container container = panel.getParent(); while (container instanceof Window == false) container = container.getParent();
        Window window = (Window) container;
        double win_x = window.getLocationOnScreen().getX(),
               win_y = window.getLocationOnScreen().getY(),
               win_w = window.getSize().getWidth(),
               win_h = window.getSize().getHeight();

System.err.println("Checking Position (Minimization Save Problem?)");
System.err.println("winpos = " + win_x + " " + win_y + " " + win_w + " " + win_h);

        list.add("#AC RTPanelFrame|" + panel.getWinType()  + "|" + panel.getWinUniq() + 
           "|" + win_x + "|" + win_y + "|" + win_w + "|" + win_h);
        last_uniq = panel.getWinUniq();
      }
      list.add("#AC " + panel.getWinPos() + " " + panel.getConfig());
      if (include_additional_settings && panel.hasAdditionalConfig()) panel.addAdditionalConfig(list, save_visible_only);
    }
    return list;
  }

  /**
   * Load entity tags from a user chosen file.
   */
  public void loadEntityTagsFromFile() {
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
        File file = file_chooser.getSelectedFile();
  loadEntityTagsFromFile(file);
    }
  }

  /**
   * Load entity tags from a specific file.
   *
   *@param file specific file to load from
   */
  public void loadEntityTagsFromFile(File file) {
    try {
      List<EntityTag> list = new ArrayList<EntityTag>();
      BufferedReader in = new BufferedReader(new FileReader(file));
      String line = in.readLine(); if (line.equals(EntityTag.getFileHeader()) == false) {
        in.close();
        throw new IOException("File Header Does Not Match");
      }
      while ((line = in.readLine()) != null) { EntityTag et = new EntityTag(line); if (et.valid()) list.add(et); }
      getRTParent().addEntityTags(list);
      in.close();
    } catch (IOException ioe) {
      JOptionPane.showMessageDialog(entity_table, "IOException: " + ioe, "File Save Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Load entity tags from local database storage.
   */
  public void loadEntityTagsLocally() {
    List<EntityTag> new_ones = new ArrayList<EntityTag>();
    boolean result = RTStore.retrieveRelatedEntityTags(new_ones, getRTParent().getVisibleBundles().ts0(), getRTParent().getVisibleBundles().ts1());
    if (result) getRTParent().addEntityTags(new_ones);
    else        JOptionPane.showMessageDialog(this, "Error Retrieving Local Entity Tags", "Local DB Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Save entity tags from a user chosen file.
   */
  public void saveEntityTagsToFile() {
    if (file_chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        File file = file_chooser.getSelectedFile();
        PrintStream out = new PrintStream(new FileOutputStream(file));
        out.println(EntityTag.getFileHeader());
  for (int i=0;i<getRTParent().getNumberOfEntityTags();i++) out.println(getRTParent().getEntityTag(i).asFileLine());
  out.close();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(entity_table, "IOException: " + ioe, "File Save Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Save entity tags to the local database.
   */
  public void saveEntityTagsLocally() {
    boolean result = RTStore.updateEntityTags(getRTParent().getEntityTags());
    if (!result) JOptionPane.showMessageDialog(this, "Error Storing/Updating Local Entity Tags", "Local DB Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Convert the entity tags to a tablet for visualization and manipulation.  It's a one way transform...
   */
  public void convertEntityTagsToTablet() {
    // Find the correct tablet (or create it if it doesn't exist)
    String headers[] = { "timestamp", "timestamp_end", "entity", "tags", "source", "uuid", "createtime" };
    Tablet tablet = getRTParent().getRootBundles().findOrCreateTablet(headers);

    // For all of the entity tags, add them as bundles into the tablet
    for (int i=0;i<getRTParent().getNumberOfEntityTags();i++) {
      EntityTag entity_tag = getRTParent().getEntityTag(i);

      Map<String,String> attr = new HashMap<String,String>();
      attr.put("entity",     entity_tag.getEntity());
      attr.put("tags",       entity_tag.getTag());
      attr.put("source",     entity_tag.getSource());
      attr.put("uuid",       entity_tag.getUUID().toString());
      attr.put("createtime", Utils.exactDate(entity_tag.getCreateTime()));

      long ts0, ts1;
      if (entity_tag.isForever()) { ts0 = getRTParent().getRootBundles().ts0(); ts1 = getRTParent().getRootBundles().ts1();
      } else                      { ts0 = entity_tag.ts0();                     ts1 = entity_tag.ts1();                     }

      tablet.addBundle(attr, Utils.exactDate(ts0), Utils.exactDate(ts1));
    }

    // Finalize the new tablet and bundles
    // Reset the transforms to force the lookups to be created
    Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(getRTParent().getRootBundles());
    getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);

    // Update the dropdowns
    getRTParent().updateBys();
  }

  /**
   * Load time markers from a user chosen file.
   */
  public void loadTimeMarkersFromFile() {
    if (file_chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
        File file = file_chooser.getSelectedFile();
  loadTimeMarkersFromFile(file);
    }
  }

  /**
   * Load time markers from the specified file.
   *
   *@return file file to load time markers from
   */
  public void loadTimeMarkersFromFile(File file) {
    try {
      List<TimeMarker> list = new ArrayList<TimeMarker>();
      BufferedReader in = new BufferedReader(new FileReader(file));
      String line = in.readLine(); if (line.equals(TimeMarker.getFileHeader()) == false) {
        in.close();
        throw new IOException("File Header Does Not Match");
      }
      while ((line = in.readLine()) != null) { TimeMarker tm = new TimeMarker(line); if (tm.valid()) list.add(tm); }
      getRTParent().addTimeMarkers(list);
      in.close();
    } catch (IOException ioe) {
      JOptionPane.showMessageDialog(times_table, "IOException: " + ioe, "File Save Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Load time markers from the local database.
   */
  public void loadTimeMarkersLocally() {
    List<TimeMarker> new_ones = new ArrayList<TimeMarker>();
    boolean result = RTStore.retrieveRelatedTimeMarkers(new_ones, getRTParent().getVisibleBundles().ts0(), getRTParent().getVisibleBundles().ts1());
    if (result) getRTParent().addTimeMarkers(new_ones);
    else        JOptionPane.showMessageDialog(this, "Error Retrieving Local Time Markers", "Local DB Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Save time markers to the user chosen file.
   */
  public void saveTimeMarkersToFile() {
    if (file_chooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
      try {
        File file = file_chooser.getSelectedFile();
        PrintStream out = new PrintStream(new FileOutputStream(file));
        out.println(TimeMarker.getFileHeader());
  for (int i=0;i<getRTParent().getNumberOfTimeMarkers();i++) out.println(getRTParent().getTimeMarker(i).asFileLine());
  out.close();
      } catch (IOException ioe) {
        JOptionPane.showMessageDialog(times_table, "IOException: " + ioe, "File Save Error", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  /**
   * Save time markers to the local database.
   */
  public void saveTimeMarkersLocally() {
    boolean result = RTStore.updateTimeMarkers(getRTParent().getTimeMarkers());
    if (!result) JOptionPane.showMessageDialog(this, "Error Storing/Updating Local Time Markers", "Local DB Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * Exit the application.  Should probably make it user-safe by asking for
   * confirmation...
   */
  private void exit() {
    System.exit(0);
  }

  /**
   * Set the visible dataset as the root dataset for the application.  Useful
   * for when the noise/non-relevant data has been removed from the view and the
   * user wants to reclaim memory.
   */
  void setVisibleAsRoot() {
    // Get a positive confirmation
    if (JOptionPane.showConfirmDialog(this, "Really set visible as root?", "Set Visible As Root", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) return;

    // Execute the transition
    getRTParent().setRootBundles(getRTParent().getVisibleBundles());

    // Repaint the stack frame
    stack_component.repaint();
  }

  /**
   * Provide the user with a dialog of the fields in the application.  Allow user to
   * select fields for removal via a customized dialog.
   */
  private void removeFields() { new RemoveFieldsDialog(this); }

  /**
   * Show the dialog(s) to add a new field to the visible records.
   */
  private void addAndSetField() { new AddAndSetFieldDialog(this); }

  /**
   * Show the dialog to allow field swaps.
   */
  private void swapVisibleFieldValues() { new SwapVisibleFieldValuesDialog(this); }

  /**
   * Show the dialog(s) to manipulate tags.
   */
  private void manipulateTags() { new ManipulateTagsDialog(this); }

  /**
   * Clear out the root dataset.  leaves the entity tags and time markers
   * in the application.
   */
  private void zeroizeRoot() {
    if (JOptionPane.showConfirmDialog(this, "Really zeroize the root?", "Zeroize Root", JOptionPane.YES_NO_OPTION) != JOptionPane.YES_OPTION) return;
    getRTParent().zeroizeRoot();
  }

  /**
   * Return to the root dataset.
   */
  void stackTop() {
    rt.popAll();
  }

  /**
   * Refilter the last dataset
   */
  // private void rePushStack() { rt.repush(); }

  /**
   * De-dupe the records and push them onto the stack
   */
  private void pushDeDupe() {
    Bundles vis = getRTParent().getVisibleBundles();
    rt.push(vis.subset(BundlesUtils.dedupe(vis.bundleSet())));
  }
 
  /**
   * Go back to the top but remove the visible set (the stack will have two bundles on it -- root and (root - visible)).
   */
  private void topMinusVisible() {
    // Get the root and visible sets
    Bundles root = getRTParent().getRootBundles();    Set<Bundle> root_set = root.bundleSet();
    Bundles vis  = getRTParent().getVisibleBundles(); Set<Bundle> vis_set  = vis.bundleSet();

    // Create the subset
    Set<Bundle> subset = new HashSet<Bundle>();
    Iterator<Bundle> it = root_set.iterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); 
      if (vis_set.contains(bundle) == false) subset.add(bundle);
    }

    // Update the stack
    stackTop(); rt.push(root.subset(subset));
  }

  /**
   * Copy the stacks slice descriptions to the clipboard.  Not all components
   * will have a slice description implemented... and some won't be all
   * that useful.
   */
  private void copySliceDescriptionsToClipboard() {
    Utils.copyToClipboard(rt.stackDescriptions());
  }

  /**
   * Display the Join Tablet dialog.
   */
  private void joinTabletsDialog() {
    // Make sure there's a tablet loaded
    if (getRTParent().getRootBundles().tabletIterator().hasNext()) {
      /* JoinTabletsDialog dialog = */ new JoinTabletsDialog(this);
    } else {
      JOptionPane.showMessageDialog(this, "No data present.  Load a file first.", "No Tablets", JOptionPane.ERROR_MESSAGE);
    }
  }

  public void mousePressed  (MouseEvent e) { }
  public void mouseReleased (MouseEvent e) { }
  public void mouseClicked  (MouseEvent e) { }
  public void mouseEntered  (MouseEvent e) { }
  public void mouseExited   (MouseEvent e) { }

  /**
   * Popup Listener to dispay popup menu for a table.  The listener remembers which
   * row contained the mouse for processing.
   */
  class PopupListener extends MouseAdapter {
    JPopupMenu popup; int row_i; JTable table;
    public PopupListener(JPopupMenu popup0, JTable table) { this.popup = popup0; this.table = table; }
    public void mousePressed   (MouseEvent e) { maybeShowPopup(e); }
    public void mouseReleased  (MouseEvent e) { maybeShowPopup(e); }
    public void maybeShowPopup (MouseEvent e) {
      if (e.isPopupTrigger()) { row_i = table.convertRowIndexToModel(table.rowAtPoint(e.getPoint())); popup.show(e.getComponent(), e.getX(), e.getY()); } }
  }

  /**
   * Popup menu action to remove row(s) from time markers.
   *
   *@param row_i row index to remove
   */
  public void removeFromTimeMarkers(int row_i) {
    Set<Integer> set = new HashSet<Integer>(); int rows[] = times_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = times_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }
    if (row_i >= 0 && row_i < getRTParent().getNumberOfTimeMarkers()) {
      if (set.contains(row_i)) { // Remove multiple highlighted entries
        Arrays.sort(rows);
  for (int j=rows.length-1;j>=0;j--) getRTParent().removeTimeMarker(getRTParent().getTimeMarker(rows[j]));
      } else getRTParent().removeTimeMarker(getRTParent().getTimeMarker(row_i)); // Remove just the single row under the mouse
    }
  }

  /**
   * Popup menu action to remove row(s) from time markers local database.
   *
   *@param row_i row index to remove
   */
  public void removeFromTimeMarkersDB(int row_i) {
    Set<TimeMarker> to_remove = new HashSet<TimeMarker>();
    Set<Integer> set = new HashSet<Integer>(); int rows[] = times_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = times_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }
    if (row_i >= 0 && row_i < getRTParent().getNumberOfTimeMarkers()) {
      if (set.contains(row_i)) { // Remove multiple highlighted entries
        Arrays.sort(rows); for (int j=rows.length-1;j>=0;j--) to_remove.add(getRTParent().getTimeMarker(rows[j]));
      } else to_remove.add(getRTParent().getTimeMarker(row_i)); // Remove just the single row under the mouse
    }
    if (to_remove.size() > 0) {
      boolean result = RTStore.deleteTimeMarkers(to_remove);
      if (!result) JOptionPane.showMessageDialog(this, "Error Deleting Local Time Markers", "Local DB Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Popup menu action to remove row(s) from entity tags.
   *
   *@param row_i row index to remove
   */
  public void removeFromEntityTags(int row_i) {
    Set<Integer> set = new HashSet<Integer>(); int rows[] = entity_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = entity_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }
    if (row_i >= 0 && row_i < getRTParent().getNumberOfEntityTags()) {
      if (set.contains(row_i)) { // Remove multiple highlighted entries
        Arrays.sort(rows);
  for (int j=rows.length-1;j>=0;j--) getRTParent().removeEntityTag(getRTParent().getEntityTag(rows[j]));
      } else getRTParent().removeEntityTag(getRTParent().getEntityTag(row_i)); // Remove just the single row under the mouse
    }
  }

  /**
   * Popup menu action to remove row(s) from entity tags local database.
   *
   *@param row_i row index to remove
   */
  public void removeFromEntityTagsDB(int row_i) {
    Set<EntityTag> to_remove = new HashSet<EntityTag>();
    Set<Integer> set = new HashSet<Integer>(); int rows[] = times_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = times_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }
    if (row_i >= 0 && row_i < getRTParent().getNumberOfEntityTags()) {
      if (set.contains(row_i)) { // Remove multiple highlighted entries
        Arrays.sort(rows); for (int j=rows.length-1;j>=0;j--) to_remove.add(getRTParent().getEntityTag(rows[j]));
      } else to_remove.add(getRTParent().getEntityTag(row_i)); // Remove just the single row under the mouse
    }
    if (to_remove.size() > 0) {
      boolean result = RTStore.deleteEntityTags(to_remove);
      if (!result) JOptionPane.showMessageDialog(this, "Error Deleting Local Entity Tags", "Local DB Error", JOptionPane.ERROR_MESSAGE);
    }
  }

  /**
   * Popup menu action to change the selected rows to forever (from just to/from the original timeframes.
   *
   *@param row_i row index to apply the change to
   */
  public void setEntityTagsToForever(int row_i) {
    Set<Integer> set = new HashSet<Integer>(); int rows[] = entity_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = entity_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }
    if (row_i >= 0 && row_i < getRTParent().getNumberOfEntityTags()) {
      if (set.contains(row_i)) { for (int j=0;j<rows.length;j++) getRTParent().getEntityTag(rows[j]).setToForever();
      } else getRTParent().getEntityTag(row_i).setToForever();
    }
  }

  /**
   * Popup menu action to perform a boolean operation on the selected entities
   *
   *@param row_i row index to apply operation to
   *@param op    set operation to perform
   */
  public void applyEntityTagsSetOp(int row_i, StrSet.Op op) {
    // Get the selected rows as a set
    Set<Integer> set = new HashSet<Integer>(); int rows[] = entity_table.getSelectedRows(); for (int i=0;i<rows.length;i++) { rows[i] = entity_table.convertRowIndexToModel(rows[i]); set.add(rows[i]); }

    // Make sure the operation
    if (row_i >= 0 && row_i < getRTParent().getNumberOfEntityTags()) {
      Set<String> strset = new HashSet<String>();

      // Figure out if the selection is under the mouse... if selection, find rows and add to set
      if (set.contains(row_i)) { 
  for (int j=0;j<rows.length;j++) strset.add(getRTParent().getEntityTag(rows[j]).getEntity());
      } else strset.add(getRTParent().getEntityTag(row_i).getEntity());

      // Do the operation
      Set<String> result = StrSet.operation(op, getRTParent().getSelectedEntities(), strset);
      getRTParent().setSelectedEntities(result);
    }
  }

  /**
   * Show the dialog to query the kb sources.
   */
  public void kbQueryForSelectedDialog() { new KBQueryDialog(this, getRTParent().getSelectedEntities()); }

  /**
   * Class implementing a {@link TableModel} for the entity tags table.
   */
  class EntityTableModel implements TableModel, EntityTagListener {
    public EntityTableModel() { getRTParent().addEntityTagListener(this); }
    List<TableModelListener> listeners = new ArrayList<TableModelListener>();
    public void addTableModelListener(TableModelListener tml) { listeners.add(tml); }
    public void removeTableModelListener(TableModelListener tml) { listeners.remove(tml); }
    public Class<?> getColumnClass(int column_index) { return (new String()).getClass(); }
    public int      getColumnCount() { return 6; }
    public String   getColumnName(int column_index) {
      switch (column_index) { case 0:  return "Entity";
                              case 1:  return "Tag";
            case 2:  return "First Heard";
            case 3:  return "Last Heard";
            case 4:  return "UUID";
            case 5:  return "Creation";
            default: return "Say What?"; } }
    public int      getRowCount() { return getRTParent().getNumberOfEntityTags(); }
    public Object   getValueAt(int row_i, int col_i) {
      EntityTag et = getRTParent().getEntityTag(row_i);
      if (et == null) return null;
      switch (col_i) { case 0:  return et.getEntity();
                       case 1:  return et.getTag();
           case 2:  if (et.isForever()) return "\u221E"; // infinite symbol
                    else                return Utils.humanReadableDate(et.ts0());
           case 3:  if (et.isForever()) return "\u221E"; // infinite symbol
                    else                return Utils.humanReadableDate(et.ts1());
           case 4:  return et.getUUID().toString();
           case 5:  return Utils.humanReadableDate(et.getCreateTime());
           default: return "Say What?"; } }
    public boolean  isCellEditable(int row_i, int col_i) { return false; }
    public void     setValueAt(Object value, int row_i, int col_i) { }
    public void     newEntityTag(EntityTag et) { 
      int size = getRTParent().getNumberOfEntityTags();
      for (int i=0;i<listeners.size();i++) listeners.get(i).tableChanged(new TableModelEvent(this, size-1, size-1, 0, TableModelEvent.INSERT));
    }
    public void     entityTagListChanged()      { 
      for (int i=0;i<listeners.size();i++) listeners.get(i).tableChanged(new TableModelEvent(this));
    }
  }

  /**
   * Class implementing the {@link TableModel} for the time marker table.
   */
  class TimesTableModel implements TableModel, TimeMarkerListener {
    public TimesTableModel() { getRTParent().addTimeMarkerListener(this); }
    List<TableModelListener> listeners = new ArrayList<TableModelListener>();
    public void addTableModelListener(TableModelListener tml) { listeners.add(tml); }
    public void removeTableModelListener(TableModelListener tml) { listeners.remove(tml); }
    public Class<?> getColumnClass(int column_index) { return (new String()).getClass(); }
    public int      getColumnCount() { return 5; }
    public String   getColumnName(int column_index) {
      switch (column_index) { case 0:  return "Description";
            case 1:  return "First Heard";
            case 2:  return "Last Heard";
            case 3:  return "UUID";
            case 4:  return "Creation";
            default: return "Say What?"; } }
    public int      getRowCount() { return getRTParent().getNumberOfTimeMarkers(); }
    public Object   getValueAt(int row_i, int col_i) {
      TimeMarker tm = getRTParent().getTimeMarker(row_i);
      if (tm == null) return null;
      switch (col_i) { case 0:  return tm.getDescription();
           case 1:  return Utils.humanReadableDate(tm.ts0());
           case 2:  return Utils.humanReadableDate(tm.ts1());
           case 3:  return tm.getUUID().toString();
           case 4:  return Utils.humanReadableDate(tm.getCreateTime());
           default: return "Say What?"; } }
    public boolean  isCellEditable(int row_i, int col_i) { return col_i == 0; }
    public void     setValueAt(Object value, int row_i, int col_i) { 
      TimeMarker tm = getRTParent().getTimeMarker(row_i);
      if (tm == null || col_i != 0) return;
      tm.setDescription((String) value);
      getRTParent().refreshAll();
    }
    public void     newTimeMarker(TimeMarker tm) { 
      int size = getRTParent().getNumberOfTimeMarkers();
      for (int i=0;i<listeners.size();i++) listeners.get(i).tableChanged(new TableModelEvent(this, size-1, size-1, 0, TableModelEvent.INSERT));
    }
    public void     timeMarkerListChanged()      { 
      for (int i=0;i<listeners.size();i++) listeners.get(i).tableChanged(new TableModelEvent(this));
    }
  }

  /**
   * Component to view the current stack of bundles within the application.  Also provides
   * interactive capability to choose which part of the stack to go to.
   */
  class StackComponent extends JComponent implements MouseListener {
    private static final long serialVersionUID = -1660124941318924608L;

    /**
     * Keep a map of the geom to tablet or bundles
     */
    Map<Shape,Object> geom_to_data = new HashMap<Shape,Object>();

    /**
     * Construct the component by adding the mouse listener.
     */
    public StackComponent() { addMouseListener(this); }

    /**
     * Paint the component by pulling from the stacked bundles.
     *
     *@param g graphics primitive
     */
    public void paintComponent(Graphics g) { 
      Graphics2D g2d = (Graphics2D) g; Composite orig_comp = g2d.getComposite();
      g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      geom_to_data.clear();

      // Clear the background
      g2d.setColor(RTColorManager.getColor("background", "default"));
      int w = getWidth(), h = getHeight(); int txt_h = Utils.txtH(g2d, "0");
      g2d.fillRect(0,0,w,h);

      // Calculate the geometry
      int ins_w = txt_h+4, ins_h = txt_h, /* graph_w = w - 2*ins_w, */ graph_h = h - (2*ins_h+4);

      // Copy the stack
      List<Bundles> dupe = rt.dupeBundlesStack(); Bundles visible = rt.getVisibleBundles();
      
      // Draw each stack layer
      int x = ins_w; int total_buns = dupe.get(0).bundleSet().size();
      for (int i=0;i<dupe.size();i++) {
        // Setup the render for this slice
        Bundles bundles = dupe.get(i); int y = ins_h;
        Iterator<Tablet> it_tab = bundles.tabletIterator(); 
        List<Tablet> sorter = new ArrayList<Tablet>(); while (it_tab.hasNext()) sorter.add(it_tab.next());
        Collections.sort(sorter, new Comparator<Tablet>() { public int compare(Tablet t0, Tablet t1) { return t0.fileHeader().compareTo(t1.fileHeader()); } } );

        // Draw a representation for this bundles
        Ellipse2D ellipse = new Ellipse2D.Float(x+1, 1, ins_h-2, ins_h-2);
        geom_to_data.put(ellipse, bundles);
        g2d.setColor(RTColorManager.getColor("label", "default"));
        if (visible == bundles) g2d.fill(ellipse); else g2d.draw(ellipse);

        // Go through the sorted tablets
        it_tab = sorter.iterator(); while (it_tab.hasNext()) {
          Tablet tablet = it_tab.next();
          g2d.setColor(RTColorManager.getColor(tablet.fileHeader()));
          int bar_h = (tablet.bundleSet().size() * graph_h) / total_buns; 
          if (bar_h < 4) bar_h = 4;
          Rectangle2D rect = new Rectangle2D.Float(x, y, txt_h, bar_h); g2d.fill(rect);
          geom_to_data.put(rect, tablet);
          y += bar_h;
        }

        // If this isn't the visible bundle, make it a little transparent
        if (visible != bundles) {
          g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
          g2d.setColor(RTColorManager.getColor("background", "default"));
          g2d.fillRect(x, ins_h, txt_h, y - ins_h);
          g2d.setComposite(orig_comp);
        }

        // Go to the next column
        x += txt_h + 1;
      }

      // Print info about the visible data
      int  num_of_tablets = visible.tabletCount(),
           num_of_bundles = visible.bundleSet().size();
      long ts0            = visible.ts0(),
           ts1            = visible.ts1();
      String stats =          ((num_of_tablets == 1) ? ("1 Tablet") : (num_of_tablets + " Tablets"));
             stats += " | " + ((num_of_bundles == 1) ? ("1 Bundle") : (num_of_bundles + " Bundles"));
      String times = "", times2 = "";
      int    str_w = Utils.txtW(g2d, stats);
      g2d.setColor(RTColorManager.getColor("label", "default"));
      if (ts0 != Long.MAX_VALUE) {
        times  = Utils.exactDate(ts0);
        times2 = Utils.exactDate(ts1);
        int str_w2 = Utils.txtW(g2d, times);  if (str_w2 > str_w) str_w = str_w2;
            str_w2 = Utils.txtW(g2d, times2); if (str_w2 > str_w) str_w = str_w2;
      }
                                      g2d.drawString(stats,  w - 2 - str_w, h - 2*txt_h - 2);
      if (times.equals("")  == false) g2d.drawString(times,  w - 2 - str_w, h - 1*txt_h - 2);
      if (times2.equals("") == false) g2d.drawString(times2, w - 2 - str_w, h - 0*txt_h - 2);
    }

    public void mouseEntered (MouseEvent me) { }
    public void mouseExited  (MouseEvent me) { }
    public void mousePressed (MouseEvent me) { }
    public void mouseReleased(MouseEvent me) { }

    /**
     * Find the geometry under the click and push that onto the stack.  If no
     * geometry is found, pop the stack
     */
    public void mouseClicked (MouseEvent me) { 
      int mx = me.getX(), my = me.getY(); Object data = null;

      // Find the shape under the mouse
      Iterator<Shape> it = geom_to_data.keySet().iterator();
      while (it.hasNext()) { Shape shape = it.next(); if (shape.contains(mx, my)) data = geom_to_data.get(shape); }

      // Push or pop...
      if        (data == null)            { rt.pop(); 
      } else if (data instanceof Bundles) { rt.push((Bundles) data);
      } else if (data instanceof Tablet)  { rt.push(rt.getVisibleBundles().subset(((Tablet) data).bundleSet())); }
    }
  }

  /**
   * Indicator table names
   */
  protected final static String IT_TIMESTAMP_STR = "timestamp", IT_INDICATOR_STR = "indicator", IT_USER_STR = "user",
                                IT_URL_STR       = "url",       IT_TITLE_STR     = "title",
                                IT_TAGS_STR      = "tags",      IT_SOURCE_STR    = "source";

  /**
   * Find or create the indicator tablet
   */
  protected Tablet findOrCreateIndicatorTablet() {
    String hdr[] = new String[7];
    hdr[0] = IT_TIMESTAMP_STR; hdr[1] = IT_INDICATOR_STR; hdr[2] = IT_USER_STR;  hdr[3] = IT_TAGS_STR; 
    hdr[4] = IT_SOURCE_STR;    hdr[5] = IT_URL_STR;       hdr[6] = IT_TITLE_STR;
    return getRTParent().getRootBundles().findOrCreateTablet(hdr);
  }
}

/**
 *
 */
class TemporalOverlapDialog extends JDialog {
  private static final long serialVersionUID = -1169262256519234838L;

  // Parent control frame
  RTControlFrame rt_control_frame;

  // Records to operate over
  Bundles        visible;

  // Tablet to fields
  Map<Tablet,List<String>> tab_map = new HashMap<Tablet,List<String>>();

  /**
   * Constructor
   */
  public TemporalOverlapDialog(RTControlFrame rt_control_frame0) {
    super(rt_control_frame0, "Temporal Overlap Dialog", true);
    this.rt_control_frame = rt_control_frame0;

    visible = rt_control_frame.getRTParent().getVisibleBundles();

    // Go through the tablets ... find the ones with timestamps and then get their fields
    Iterator<Tablet> it_tab = visible.tabletIterator(); while (it_tab.hasNext()) {

      // Get the tablet... determine if it has timestamps
      Tablet tablet = it_tab.next(); if (tablet.hasTimeStamps()) {

        // Figure out which fields are in this tablet...
        List<String> tablet_fields = new ArrayList<String>();
        Iterator<String> it_fld = visible.getGlobals().fieldIterator(); while (it_fld.hasNext()) {
          String fld = it_fld.next();
          int    fld_i = visible.getGlobals().fieldIndex(fld);
          if (tablet.hasField(fld_i)) tablet_fields.add(fld);
        }

        tab_map.put(tablet, tablet_fields);
      }
    }

    if (tab_map.keySet().size() < 2) return; // Not worth displaying...

    setLayout(new BorderLayout(5,5));
    JPanel labels_panel = new JPanel(); labels_panel.setLayout(new GridLayout(5,1,5,5));
      labels_panel.add(new JLabel("Tablet 1"));
      labels_panel.add(new JLabel("Field 1"));
      labels_panel.add(new JLabel("Tablet 2"));
      labels_panel.add(new JLabel("Field 2"));
      labels_panel.add(new JLabel("Delta"));
    add("West", labels_panel);

    // Make strings for the tablets
    List<String> tablet_strs = new ArrayList<String>(); it_tab = tab_map.keySet().iterator(); while (it_tab.hasNext()) tablet_strs.add(it_tab.next().toString());

    JPanel combobox_panel = new JPanel(); combobox_panel.setLayout(new GridLayout(5,1,5,5));
      combobox_panel.add(tab1_cb  = new JComboBox(tablet_strs.toArray()));
        tab1_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateFields((String) tab1_cb.getSelectedItem(), fld1_cb); } } );
      combobox_panel.add(fld1_cb  = new JComboBox(fieldList((String) tab1_cb.getSelectedItem()).toArray()));
      combobox_panel.add(tab2_cb  = new JComboBox(tablet_strs.toArray()));
        tab2_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateFields((String) tab2_cb.getSelectedItem(), fld2_cb); } } );
      combobox_panel.add(fld2_cb  = new JComboBox(fieldList((String) tab2_cb.getSelectedItem()).toArray()));
      combobox_panel.add(delta_cb = new JComboBox(delta_strs));
    add("Center", combobox_panel);
    
    JPanel buttons_panel = new JPanel(new FlowLayout()); JButton bt;
      buttons_panel.add(bt = new JButton("Filter Bundles"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { filterRecords(); } } );
      buttons_panel.add(bt = new JButton("Cancel"));
        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { closeDialog(); } } );
    add("South", buttons_panel);

    pack(); setVisible(true);
  }

  /**
   * Close the dialog
   */
  private void closeDialog() { setVisible(false); dispose(); }

  /**
   * Apply the filter operation and push the results onto the stack.
   */
  private void filterRecords() {
    Tablet tab1 = findTablet((String) tab1_cb.getSelectedItem()), tab2 = findTablet((String) tab2_cb.getSelectedItem());
    String fld1 = (String) fld1_cb.getSelectedItem(),             fld2 = (String) fld2_cb.getSelectedItem();

    Set<Bundle> results = TimeAnalytics.overlapBetweenTabletsCheap(tab1, fld1, tab2, fld2, millisecondSlop());

    rt_control_frame.getRTParent().push(rt_control_frame.getRTParent().getVisibleBundles().subset(results));

    setVisible(false); dispose();
  }

  /**
   * Return the tablet matching the specified tablet string.
   */
  protected Tablet findTablet(String tab_str) {
    Iterator<Tablet> it_tab = tab_map.keySet().iterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next(); if (tablet.toString().equals(tab_str)) return tablet;
    }
    return null;
  }

  /**
   * Return the milliseconds slop for the analytic. ... how much the time can different before the
   * the two records are not considered at match...
   */
  public long millisecondSlop() {
    String str = (String) delta_cb.getSelectedItem();
    for (int i=0;i<delta_strs.length;i++) if (delta_strs[i].equals(str)) return delta_millis[i];
    return delta_millis[0];
  }

  /**
   * Options for the time slop
   * - These definitely need to stay paired together...
   */
  long   hour           = 60L*60L*1000L;
  String delta_strs[]   = { "One Hour", "Four Hours",   "One Day", "Three Days", "One Week",  "One Month",  "Three Months"  };
  long   delta_millis[] = { hour,       4L*hour,        24L*hour,  3L*24L*hour,  7L*24L*hour, 30L*24L*hour, 3L*30L*24L*hour };

  /**
   * Comboboxes...
   */
  JComboBox tab1_cb, 
            tab2_cb, 
            fld1_cb, 
            fld2_cb, 
            delta_cb;

  /**
   * Return a list of the fields for a tablet
   */
  protected List<String> fieldList(String tab_str) {
    Iterator<Tablet> it_tab = tab_map.keySet().iterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next(); if (tablet.toString().equals(tab_str)) return tab_map.get(tablet);
    }
    return new ArrayList<String>();
  }

  /**
   * Update the fields for a combobox based on the tablet string.
   */
  protected void updateFields(String tab_str, JComboBox fld_cb) {
    Iterator<Tablet> it_tab = tab_map.keySet().iterator(); while (it_tab.hasNext()) {
      Tablet tablet = it_tab.next(); if (tablet.toString().equals(tab_str)) {
        List<String> fields = tab_map.get(tablet);
        fld_cb.removeAllItems();
        for (int i=0;i<fields.size();i++) fld_cb.addItem(fields.get(i));
        return;
      }
    }
  }
}

/**
 * Dialog to configure the entity timesframes analytic.
 */
class EntityTimeframesDialog extends JDialog {
  private static final long serialVersionUID = -1169269956500234838L;
  /**
   * Parent Control Frame
   */
  RTControlFrame rt_control_frame;

  /**
   * Which fields to create entities for
   */
  JList<String>  entity_ls;

  /**
   * Which fields to tag those entities with
   */
  JList<String>  tags_ls;

  /**
   * If checked, include the entity data type as a column
   */
  JCheckBoxMenuItem include_data_type_cbmi;

  /**
   * Constructor
   */
  public EntityTimeframesDialog(RTControlFrame rt_control_frame0) {
    super(rt_control_frame0, "Entity Timeframes Dialog", true);
    this.rt_control_frame = rt_control_frame0;

    // Which field for entities?  which fields to add as tags?
    JPanel panel, p2; JButton bt;
    panel = new JPanel(new BorderLayout());

    p2 = new JPanel(); p2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Entity Field(s)"));
    p2.add(new JScrollPane(entity_ls = new JList<String>(KeyMaker.entityBlanks(rt_control_frame.getRTParent().getRootBundles().getGlobals()))));
    panel.add("Center", p2);

    p2 = new JPanel(); p2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLoweredBevelBorder(), "Tag Field(s) [Optional]"));
    p2.add(new JScrollPane(tags_ls = new JList<String>(KeyMaker.entityBlanks(rt_control_frame.getRTParent().getRootBundles().getGlobals()))));
    panel.add("East", p2);

    p2 = new JPanel(); p2.add(include_data_type_cbmi = new JCheckBoxMenuItem("Include Entity Type", true));
    panel.add("South", p2);

    add("Center", panel);

    // Buttons to cancel
    panel = new JPanel(new FlowLayout());
    panel.add(bt = new JButton("Create Tablet"));
    bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { createTablet(); } } );
    panel.add(bt = new JButton("Cancel"));
    bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { closeDialog(); } } );
    add("South", panel);

    pack(); setVisible(true);
  }

  /**
   * Close the dialog
   */
  private void closeDialog() {
    setVisible(false); dispose();
  }

  /**
   * Create the timeframes tablet and close the dialog.
   */
  private void createTablet() {
    List<String> entity_fields = entity_ls.getSelectedValuesList(),
                 tag_fields    = tags_ls.  getSelectedValuesList();

    // An entity field needed to have been selected...
    if (entity_fields.size() > 0) {
      // Root bundles
      Bundles bundles = rt_control_frame.getRTParent().getRootBundles();

      // Maps of the tallying...
      Map<String,Long>        first_heard    = new HashMap<String,Long>(),
                              last_heard     = new HashMap<String,Long>();
      Map<String,Set<String>> entity_tag_lu  = new HashMap<String,Set<String>>();
      Map<String,Set<String>> days_occur_map = new HashMap<String,Set<String>>();
      Map<String,Integer>     rec_count      = new HashMap<String,Integer>();

      // Go through the tablets
      Iterator<Tablet> it_tab = bundles.tabletIterator(); while (it_tab.hasNext()) {
        Tablet tablet = it_tab.next();

        // Go through the entiti fields and determine if there's match
        for (int field_i=0;field_i<entity_fields.size();field_i++) {
          if (KeyMaker.tabletCompletesBlank(tablet, entity_fields.get(field_i))) {
            KeyMaker entity_km = new KeyMaker(tablet, entity_fields.get(field_i));

            // Figure out which tag fields are there
            List<KeyMaker> tag_kms = new ArrayList<KeyMaker>();
            for (int tag_i=0;tag_i<tag_fields.size();tag_i++) {
              if (KeyMaker.tabletCompletesBlank(tablet, tag_fields.get(tag_i)))
                tag_kms.add(new KeyMaker(tablet, tag_fields.get(tag_i)));
            }

            // Go through the bundles... record first and last hear... and tags
            Iterator<Bundle> it_bundle = tablet.bundleIterator(); while (it_bundle.hasNext()) {
              Bundle bundle     = it_bundle.next();
              String entities[] = entity_km.stringKeys(bundle);

              for (int i=0;i<entities.length;i++) {
                String entity = entities[i]; if (entity.equals("") || entity.equals(BundlesDT.NOTSET)) continue;

                // Take care of the time book keeping
                if        (bundle.hasTime()) {
                  long ts0 = bundle.ts0(), ts1 = bundle.ts1();
                  if      (first_heard.containsKey(entity) == false) first_heard.put(entity, ts0);
                  else if (first_heard.get(entity) > ts0)            first_heard.put(entity, ts0);
                  if      (last_heard.containsKey(entity) == false) last_heard.put(entity, ts1);
                  else if (last_heard.get(entity) < ts1)            last_heard.put(entity, ts1);

                  if (rec_count.containsKey(entity) == false) rec_count.put(entity, 1);
                  else                                        rec_count.put(entity, rec_count.get(entity) + 1);

                  // Handle the timestamps for the days occuring maps... complicated for begin/end timestamps
                  long day_in_millis = 24L * 60L * 60L * 1000L;
                  if      (days_occur_map.containsKey(entity) == false) days_occur_map.put(entity, new HashSet<String>());
                  String  yyyymmdd     = Utils.dayDateStr(ts0); days_occur_map.get(entity).add(yyyymmdd);
                  String  yyyymmdd_end = Utils.dayDateStr(ts1); days_occur_map.get(entity).add(yyyymmdd_end);
                  ts0 += day_in_millis;
                  while (ts0 < ts1) {
                    yyyymmdd = Utils.dayDateStr(ts0); days_occur_map.get(entity).add(yyyymmdd);
                    ts0 += day_in_millis;
                  }
                }

                // Figure out the tagging
                for (int j=0;j<tag_kms.size();j++) {
                  String blank  = tag_kms.get(j).getBlank();
                  String tags[] = tag_kms.get(j).stringKeys(bundle);
                  for (int k=0;k<tags.length;k++) {
                    if (tags[k].equals("") || tags[k].equals(BundlesDT.NOTSET)) continue;
                    String tag = tags[k];
                    if (blank.contains(BundlesDT.DELIM) == false) tag = blank + "=" + tag; // Make it type value

                    if (entity_tag_lu.containsKey(entity) == false) entity_tag_lu.put(entity, new HashSet<String>());
                    entity_tag_lu.get(entity).add(tag);
                  }
                }
              }
            }
          }
        }
      }

      // Include the data type of the entity as a separate field
      boolean inc_data_type = include_data_type_cbmi.isSelected();

      // Make the tablet
      if (first_heard.keySet().size() > 0) {
        // Create the tablet
        String hdr[];

        if (inc_data_type) { String[] hdr1 = { "timestamp", "timestamp_end", "entity", "data_type", "DAYS_DELTA", "DAYS_OCCUR", "RECS", "tags" }; hdr = hdr1;
        } else             { String[] hdr2 = { "timestamp", "timestamp_end", "entity",              "DAYS_DELTA", "DAYS_OCCUR", "RECS", "tags" }; hdr = hdr2;  }

        Tablet tablet = rt_control_frame.getRTParent().getRootBundles().findOrCreateTablet(hdr);

        Iterator<String> it_entity = first_heard.keySet().iterator(); while (it_entity.hasNext()) {
          String      entity     = it_entity.next();
          long        ts0        = first_heard.get(entity),
                      ts1        = last_heard.get(entity);
          int         days_delta = (int) ((ts1 - ts0)/(24L*60L*60L*1000L)) + 1,
                      days_occur = days_occur_map.get(entity).size();
          String      tag        = "";
          Set<String> tags       = null;
          if (entity_tag_lu.containsKey(entity)) {
            tags = entity_tag_lu.get(entity); 
            if (tags.size() > 0) {
              StringBuffer sb = new StringBuffer();
              Iterator<String> it = tags.iterator(); sb.append(it.next()); while (it.hasNext()) sb.append("|" + it.next());
              tag = sb.toString();
            }
          } else tags = new HashSet<String>();

          // System.err.println(entity + "," + Utils.exactDate(ts0) + "," + Utils.exactDate(ts1) + "," + days + "," + tags);

          // Add the record
          Map<String,String> attr_map = new HashMap<String,String>();
          attr_map.put("entity",      entity);
          attr_map.put("DAYS_DELTA",  ""+days_delta);
          attr_map.put("DAYS_OCCUR",  ""+days_occur);
          attr_map.put("RECS",        ""+rec_count.get(entity));
          attr_map.put("tags",        Utils.normalizeTag(tag));

          if (inc_data_type) attr_map.put("data_type", ""+BundlesDT.getEntityDataType(entity));

          tablet.addBundle(attr_map, Utils.exactDate(ts0), Utils.exactDate(ts1));
        }

        // Finalize the new tablet and bundles
        // Reset the transforms to force the lookups to be created
        Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(rt_control_frame.getRTParent().getRootBundles());
        rt_control_frame.getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);

        // Update the dropdowns
        rt_control_frame.getRTParent().updateBys();
      }
    }
    closeDialog();
  }
}

/**
 * Dialog to swap the values in two fields for the visible set of records.
 */
class SwapVisibleFieldValuesDialog extends JDialog implements ItemListener {
  private static final long serialVersionUID = -1100124342303124608L;

  /**
   * Parent frame
   */
  protected RTControlFrame parent;
  
  /**
   * From these fields
   */
  JComboBox<String> fm_fields[],

  /**
   * To these fields
   */
                    to_fields[];

  /**
   * Swap button
   */
  JButton   swap_bt;

  /**
   * String to indicate that the combobox isn't set for swap
   */
  final String no_swap_str = BundlesDT.DELIM + "No Swap" + BundlesDT.DELIM;

  /**
   * Construct the gui
   */
  public SwapVisibleFieldValuesDialog(RTControlFrame parent) {
    super(parent, "Swap Field Values (Visible Bundles)", true);
    this.parent = parent;

    // Determine the fields
    String       fields[] = KeyMaker.blanks(parent.getRTParent().getRootBundles().getGlobals(), false, true, true);
    List<String> refined  = new ArrayList<String>(); refined.add(no_swap_str);
    for (int i=0;i<fields.length;i++) if (fields[i].indexOf(BundlesDT.DELIM) < 0) refined.add(fields[i]);
    fields = new String[refined.size()];
    for (int i=0;i<fields.length;i++) fields[i] = refined.get(i);

    // - JComboBoxes for the swap
    getContentPane().setLayout(new BorderLayout(5,5));
    JPanel panel = new JPanel(new GridLayout(0,2,5,5));
    fm_fields = new JComboBox[5]; to_fields = new JComboBox[5];
    for (int i=0;i<fm_fields.length;i++) {
      panel.add(fm_fields[i] = new JComboBox<String>(fields)); fm_fields[i].addItemListener(this);
      panel.add(to_fields[i] = new JComboBox<String>(fields)); to_fields[i].addItemListener(this);
    }
    getContentPane().add("Center", panel);

    // - Buttons
    panel = new JPanel(new FlowLayout());
    JButton bt;
    panel.add(bt      = new JButton("Close")); bt.     addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { close(); } } );
    panel.add(swap_bt = new JButton("Swap"));  swap_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { swapFields();  } } ); swap_bt.setEnabled(false);
    getContentPane().add("South", panel);

    // Show the dialog
    pack(); setVisible(true);
  }

  /**
   * Check to see if the settings are value -- if so, enable the swap button... else disable it.
   */
  public void itemStateChanged(ItemEvent ie) {
    BundlesG globals = parent.getRTParent().getRootBundles().getGlobals();
    int swaps = evaluateSettings(null, globals);
    if (swaps > 0) swap_bt.setEnabled(true); else swap_bt.setEnabled(false);
  }

  public int evaluateSettings(Bundles bundles, BundlesG globals) {
    int swaps = 0; boolean valid = true;

    for (int i=0;i<fm_fields.length;i++) {
      String fm = (String) fm_fields[i].getSelectedItem(), to = (String) to_fields[i].getSelectedItem();
      if (fm.equals(no_swap_str) && to.equals(no_swap_str)) { } else {
        if (fm.equals(no_swap_str) || to.equals(no_swap_str)) { valid = false; } else {
          if (fm.equals(to)) { valid = false; } else {
      boolean fm_scalar = globals.isScalar(globals.fieldIndex(fm)),
              to_scalar = globals.isScalar(globals.fieldIndex(to));
      if ((fm_scalar && to_scalar) || ((fm_scalar == false) && (to_scalar == false))) { 
        swaps++; 
              if (bundles != null) bundles.swapFieldValues(fm, to);
      } else { valid = false; }
    }
  }
      }
    }
    if (valid == false) swaps = 0;
    return swaps;
  }

  /**
   * Execute the swap
   */
  public void swapFields() {
    Bundles  visible = parent.getRTParent().getVisibleBundles();
    BundlesG globals = parent.getRTParent().getRootBundles().getGlobals();
    evaluateSettings(visible, globals);
    Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(parent.getRTParent().getRootBundles());
    globals.cleanse(bundles_set);
    parent.getRTParent().updateBys();
    parent.getRTParent().refreshPanelApplicationData();
    close();
  }

  /**
   * Close the dialog.
   */
  public void close() { setVisible(false); }
}

/**
 * Dialog to add and set a field for the visible records.  Need to make shortcut buttons
 * so that user can quickly use different settings without re-typing.
 */
class AddAndSetFieldDialog extends JDialog {
  private static final long serialVersionUID = -2420124949318124608L;

  /**
   * Parent frame
   */
  protected RTControlFrame parent;

  /**
   * Textfield for user specified field name
   */
  JTextField field_name_tf,

  /**
   * Value field
   */
             value_tf,
  /**
   * Field to display the datatype
   */
       datatype_tf;
  /**
   * ComboBox for an existing field
   */
  JComboBox<String>  existing_field_cb;

  /**
   * Checkbox to indicate that the field will be scalar (not enabled -- just reflects what the user specifies)
   */
  JCheckBox  scalar_cb;

  /**
   * Button to add/set the field/value
   */
  JButton    addset_bt;

  /**
   * Construct the dialog and display it.
   *
   *@param parent parent control frame
   */
  public AddAndSetFieldDialog(RTControlFrame parent) {
    super(parent, "Add Or Set Field Dialog (Visible Bundles)", false);
    this.parent = parent;
    getContentPane().setLayout(new BorderLayout(5,5));

    // Construct the GUI
    // - Options
    JPanel panel = new JPanel(new GridLayout(3,3,5,5));
    panel.add(new JLabel("Field Name")); panel.add(field_name_tf = new JTextField()); panel.add(existing_field_cb = new JComboBox<String>());
    panel.add(new JLabel("Value"));      panel.add(value_tf      = new JTextField()); panel.add(scalar_cb         = new JCheckBox("Scalar Field", false)); scalar_cb.setEnabled(false);
    panel.add(new JLabel("Data Type"));  panel.add(datatype_tf   = new JTextField()); datatype_tf.setEnabled(false);
    getContentPane().add("Center", panel);

    // - Buttons
    panel = new JPanel(new FlowLayout());
    JButton bt;
    panel.add(bt        = new JButton("Close"));   bt.       addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { close(); } } );
    panel.add(addset_bt = new JButton("Add/Set")); addset_bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addAndSet(); } } ); addset_bt.setEnabled(false);
    getContentPane().add("South", panel);

    // Add listeners
    existing_field_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent e) { checkComboBox(); } } );
    field_name_tf.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) { if (addset_bt.isEnabled()) addAndSet(); } } );
    value_tf.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent e) { if (addset_bt.isEnabled()) addAndSet(); } } );
    
    field_name_tf.addCaretListener(new CaretListener() { public void caretUpdate(CaretEvent e) { checkButtonState(); } } );
    value_tf.addCaretListener(new CaretListener() { public void caretUpdate(CaretEvent e) { checkButtonState(); parseDataType(); } } );

    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { setVisible(false); dispose(); } } );

    // Fill the combobox
    fillComboBox();
    
    // Show the dialog
    pack(); setVisible(true);
  }

  /**
   * Fill the combobox with the fields.
   */
  private void fillComboBox() {
    // Get a list of the fields
    String blanks[] = KeyMaker.blanks(parent.getRTParent().getRootBundles().getGlobals(), false, true, true, false);
    // Sort without case sensitivity
    Arrays.sort(blanks, new CaseInsensitiveComparator());
    // Add the "New Field" Option...
    List<String> fields_ls = new ArrayList<String>(); fields_ls.add("Add New Field...");
    // Don't add any fields that have a pipe in them (those are transforms, specialized...)
    for (int i=0;i<blanks.length;i++) if (blanks[i].indexOf("|") < 0) fields_ls.add(blanks[i]);
    // Convert to an array
    String fields[] = new String[fields_ls.size()]; fields_ls.toArray(fields);
    // Now add them to the combobox
    existing_field_cb.removeAllItems();
    for (int i=0;i<fields.length;i++) existing_field_cb.addItem(fields[i]);
  }

  /**
   * Manipulate the gui based on the combobox setting.
   */
  private void checkComboBox() {
    String field = (String) existing_field_cb.getSelectedItem(); if (field == null) { return; }
    if (field.equals("Add New Field...")) { field_name_tf.setEnabled(true); } else { 
      field_name_tf.setEnabled(false); scalar_cb.setEnabled(false);
      if (Utils.isAllUpper(field)) scalar_cb.setSelected(true); else scalar_cb.setSelected(false);
    }
    checkButtonState();
  }

  /**
   * Parse the user data type and display in the datatype textfield.
   */
  private void parseDataType() { // System.err.println("Value = \"" + value_tf.getText() + "\"");
                                 datatype_tf.setText("" + BundlesDT.getEntityDataType(value_tf.getText())); }

  /**
   * See if the add/set button should be enabled or not.
   */
  private void checkButtonState() {
    String field = (String) existing_field_cb.getSelectedItem();
    boolean field_valid = false;
    if (field.equals("Add New Field...")) { if (validFieldName(field = field_name_tf.getText())) { field_valid = true; } } else { field_valid = true; }

    String value = value_tf.getText();
    boolean value_valid = true;
    if (Utils.isAllUpper(field)) { value_valid = false; try { Integer.parseInt(value); value_valid = true; } catch (NumberFormatException nfe) { } }

    if (field_valid && value_valid) addset_bt.setEnabled(true); else addset_bt.setEnabled(false);
  }

  /**
   * Check to see if the field name is valid.
   */
  private boolean validFieldName(String str) {
    // Check for a blank field name or reserved ones...
    if (str.equals("") || str.toLowerCase().equals("tags")
                       || str.toLowerCase().equals("timestamp")
           || str.toLowerCase().equals("timestamp_end")
           || str.toLowerCase().equals("beg")
           || str.toLowerCase().equals("end")) return false;

    // Check for datatype names -- not a good idea to use these either (they can be used for file-based transforms)
    Iterator<BundlesDT.DT> it_dt = BundlesDT.dataTypesIterator();
    while (it_dt.hasNext()) { if (str.equals("" + it_dt.next())) return false; }

    // Check to make sure we have legit characters (probably should do this on file read as well...)
    for (int i=0;i<str.length();i++) {
      char c = str.charAt(i);
      if ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == ' ' || c == '-' || c == '_') { } else return false;
    }
    if (Utils.isAllUpper(str)) scalar_cb.setSelected(true); else scalar_cb.setSelected(false);
    return true;
  }

  /**
   * Close the dialog.
   */
  public void close() { setVisible(false); }

  /**
   * Add and set the field. Calls into the framework.
   */
  public void addAndSet() {
    // Execute the set operations
    Bundles root_bundles  = parent.getRTParent().getRootBundles(),
            toset_bundles = parent.getRTParent().getVisibleBundles();

    // Get the field from the appropriate place
    boolean field_valid = false, adding_new_field = false;
    String field = (String) existing_field_cb.getSelectedItem();
    if (field.equals("Add New Field...")) { if (validFieldName(field = field_name_tf.getText())) { field_valid = true; adding_new_field = true; } } else { field_valid = true; }

    // If it's valid, set it.
    if (field_valid) {
      root_bundles.getGlobals().setField(toset_bundles, root_bundles, field, value_tf.getText());

      // Update the bys
      parent.getRTParent().updateBys();

      // Update each panels application data
      parent.getRTParent().refreshPanelApplicationData();

      if (adding_new_field) {
        // Clear the textfield and disable
        field_name_tf.setText(""); field_name_tf.setEnabled(false);
        // Update the combobox and include the new field
        fillComboBox();
        existing_field_cb.setSelectedItem(field);
      }
    }
  }
}

/**
 * Dialog used to manipulate record tags.
 */
class ManipulateTagsDialog extends JDialog  implements CaretListener {
  private static final long serialVersionUID = -2190122422318124608L;

  /**
   * Parent frame
   */
  RTControlFrame parent;

  /**
   * Create a panel to manipulate record tags.
   *
   *@param parent parent frame
   */
  public ManipulateTagsDialog(RTControlFrame parent) {
    super(parent, "Manipulate Tags...", false); this.parent = parent; JButton bt; getContentPane().setLayout(new BorderLayout());

    JPanel panel   = new JPanel(new GridLayout(3,1,5,5)); panel.setBorder(BorderFactory.createTitledBorder("New/Additional Tag(s)"));
    JPanel sub     = new JPanel(new BorderLayout(5,5)); sub.add("West", new JLabel("Tags")); sub.add(tags_tf = new JTextField(30));
    panel.add(sub);
    panel.add(tags_label = new JLabel(""));
    panel.add(info_label = new JLabel(""));
    getContentPane().add("North", panel);

    // Add the tag cache info
    quick_cache_panel = new JPanel(new GridLayout(0,1,5,5)); quick_cache_panel.setBorder(BorderFactory.createTitledBorder("Quick Cache"));
    getContentPane().add("Center", new JScrollPane(quick_cache_panel));

    // Create the close panel
    panel = new JPanel(new FlowLayout());
    panel.add(bt                       = new JButton("Clear"));    bt.                 setToolTipText("Clear");
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clearTags(); } } );
    panel.add(bt = tags_replace_bt     = new JButton("Replace"));  tags_replace_bt.    setToolTipText("Replace");            tags_replace_bt.    setEnabled(false);
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { replaceTags(); } } );
    panel.add(bt = tags_add_bt         = new JButton("Add"));      tags_add_bt.        setToolTipText("Add To");             tags_add_bt.        setEnabled(false);
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addToTags(); } } );
    panel.add(bt = tags_replacetype_bt = new JButton("Replace*")); tags_replacetype_bt.setToolTipText("Replace Type-Value"); tags_replacetype_bt.setEnabled(false);
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { replaceTypeValueTags(); } } );
    panel.add(bt = tags_remove_bt      = new JButton("Remove*"));  tags_remove_bt.     setToolTipText("Remove Tag(s)");      tags_remove_bt.     setEnabled(false);
      bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeTags(); } } );

    panel.add(bt = new JButton("Close"));
    bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setVisible(false); dispose(); } } );
    getContentPane().add("South", panel);

    // Add the listeners
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { setVisible(false); dispose(); } } );
    tags_tf.addCaretListener(this);
    tags_tf.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addToTags(); } } );

    // Pack it and show it
    pack(); setVisible(true);
  }

  /**
   * Handle changes to the tags textfield.  Verify if the tag is correct and enable/disable the buttons accordingly.
   *
   *@param e caret event
   */
  public void caretUpdate(CaretEvent e) { 
    String str = Utils.stripSpaces(tags_tf.getText());
    if (str.equals("")) { disableAllButtons(); tags_label.setText(""); displayHelp(); } else {
      // Normalize the tag
      String       norm   = Utils.normalizeTag(str); tags_label.setText(norm);

      // Decompose the individual tokens
      List<String> tokens = Utils.tokenizeTags(norm); int simple_count = 0, typeval_count = 0, hier_count = 0, error_count = 0, tag_count = 0;
      for (int i=0;i<tokens.size();i++) {
        boolean simple  = Utils.tagIsSimple(tokens.get(i)),
                typeval = Utils.tagIsTypeValue(tokens.get(i)),
                hier    = Utils.tagIsHierarchical(tokens.get(i));
        if      ( simple && !typeval && !hier) { simple_count++;  tag_count++; }
        else if (!simple &&  typeval && !hier) { typeval_count++; tag_count++; }
        else if (!simple && !typeval &&  hier) { hier_count++;    tag_count++; }
        else                                   error_count++;
      }

      // Set the info string
      String info = tokens.size() + " Tag(s) - " + simple_count + " Simple, " + typeval_count + " TypeVal, " + hier_count + " Hier (" + error_count + " Errs)";
      info_label.setText(info);

      if (error_count > 0 || tag_count == 0) { disableAllButtons(); } else {
        if (simple_count == 0 && hier_count == 0 && typeval_count > 0) tags_replacetype_bt.setEnabled(true); else tags_replacetype_bt.setEnabled(false); 
        tags_replace_bt.setEnabled(true);
        tags_add_bt.setEnabled(true);
        tags_remove_bt.setEnabled(true);
      }
    }
  }

  /**
   * Draw a help string in the info label.
   */
  protected void displayHelp() { info_label.setText("Ex: simple_tag|type=value|parent::child|color=blue,black"); }

  /**
   * Disable all of the buttons (except clear -- that never gets disabled).
   */
  protected void disableAllButtons() { tags_replace_bt.setEnabled(false); tags_add_bt.setEnabled(false); tags_replacetype_bt.setEnabled(false); tags_remove_bt.setEnabled(false); }

  /**
   * Panel for the quick cache options.
   */
  JPanel quick_cache_panel;

  /**
   * Textfield for entering new tags.
   */
  JTextField tags_tf;

  /**
   * Label to describe current tags textfield.
   */
  JLabel     tags_label,
  /**
   * Label to show additional parsing information on the tag.
   */
             info_label;
  /**
   * Buttons for the tags modifiers.
   */
  JButton    tags_replace_bt,
             tags_add_bt,
       tags_replacetype_bt,
             tags_remove_bt;

  /**
   * Clear the tags for the visible records.
   */
  protected void clearTags() {
    parent.getRTParent().getVisibleBundles().clearTags();
    parent.getRTParent().refreshAll();
  }

  /**
   * Replace the tag for the visible records with the user specified tag.
   */
  protected void replaceTags() {
    Bundles root = parent.getRTParent().getRootBundles(), visible = parent.getRTParent().getVisibleBundles();
    cacheButton("replaceTags", Utils.normalizeTag(tags_tf.getText()));
    root.replaceTags(visible, Utils.normalizeTag(tags_tf.getText()));
    parent.getRTParent().updateBys(); parent.getRTParent().refreshAll();
  }

  /**
   * Add the specified tag to the existing tag.
   */
  protected void addToTags() {
    Bundles root = parent.getRTParent().getRootBundles(), visible = parent.getRTParent().getVisibleBundles();
    cacheButton("addToTags", Utils.normalizeTag(tags_tf.getText()));
    root.addTags(visible, Utils.normalizeTag(tags_tf.getText()));
    parent.getRTParent().updateBys(); parent.getRTParent().refreshAll();
  }

  /**
   * Replace the type value tags that match the user specified tag.
   */
  protected void replaceTypeValueTags() {
    Bundles root = parent.getRTParent().getRootBundles(), visible = parent.getRTParent().getVisibleBundles();
    cacheButton("replaceTypeValueTags", Utils.normalizeTag(tags_tf.getText()));
    root.replaceTypeValueTags(visible, Utils.normalizeTag(tags_tf.getText()));
    parent.getRTParent().updateBys(); parent.getRTParent().refreshAll();
  }

  /**
   * Remove the specific tags that the user has entered.
   */
  protected void removeTags() {
    cacheButton("removeTags", Utils.normalizeTag(tags_tf.getText()));
    parent.getRTParent().getVisibleBundles().removeTags(Utils.normalizeTag(tags_tf.getText()));
    parent.getRTParent().refreshAll();
  }

  /**
   * Set of cached buttons
   */
  Set<String> cached = new HashSet<String>();

  /**
   * Create a cache button (unless it already exists)
   */
  protected void cacheButton(String action, String tags) {
    // Make sure it's not a duplicate
    String key = action + " ==> " + tags; if (cached.contains(key)) return; else cached.add(key);

    // Add the button and its listener
    JButton bt; quick_cache_panel.add(bt = new JButton(key));
    bt.addActionListener(new CachedActionListener(action, tags));

    // Force the gui to update
    validate();
  }

  /**
   * Create a internal class to handle the cached action.
   */
  class CachedActionListener implements ActionListener {
    String action, tags;
    public CachedActionListener(String action, String tags) { this.action = action; this.tags = tags; }
    public void actionPerformed(ActionEvent ae) {
      boolean field_added = false;

      // Get the root and visible bundles
      Bundles root = parent.getRTParent().getRootBundles(), visible = parent.getRTParent().getVisibleBundles();

      // Select the correct options
      if        (action.equals("replaceTags"))          { field_added = root.   replaceTags         (visible, Utils.normalizeTag(tags));
      } else if (action.equals("addToTags"))            { field_added = root.   addTags             (visible, Utils.normalizeTag(tags));
      } else if (action.equals("replaceTypeValueTags")) { field_added = root.   replaceTypeValueTags(visible, Utils.normalizeTag(tags));
      } else if (action.equals("removeTags"))           {               visible.removeTags          (         Utils.normalizeTag(tags));
      } else throw new RuntimeException("Do Not Understand Cached Action \"" + action + "\"");

      // Adjust the interface if a field was added
      if (field_added) parent.getRTParent().updateBys();

      // Refresh the views
      parent.getRTParent().refreshAll();
    }
  }
}

/**
 * Dialog to list fields and enable user to interactive delete the fields.
 */
class RemoveFieldsDialog extends JDialog {
  private static final long serialVersionUID = -3190124387818124608L;

  /**
   * Parent frame
   */
  protected RTControlFrame parent;

  /**
   * Sorted list of fields
   */
  List<String> fields = new ArrayList<String>();

  /**
   * Checkboxes to keep field
   */
  JCheckBox    cbs[];

  /*
   * Constructor
   *
   *@param parent  parent frame
   */
  public RemoveFieldsDialog(RTControlFrame parent) {
    super(parent, "Remove Fields...", true); this.parent = parent;

    // Get a list of sorted fields
    BundlesG globals = parent.getRTParent().getRootBundles().getGlobals();
    Iterator<String> it = globals.fieldIterator(); while (it.hasNext()) fields.add(it.next());
    Collections.sort(fields, new CaseInsensitiveComparator());

    // Construct the gui
    cbs = new JCheckBox[fields.size()];
    JPanel cbs_panel = new JPanel(new GridLayout(fields.size()+1, 1, 5, 5)), labs_panel = new JPanel(new GridLayout(fields.size()+1, 1, 5, 5));
    cbs_panel.add(new JLabel("Keep")); labs_panel.add(new JLabel("Field"));
    for (int i=0;i<fields.size();i++) { labs_panel.add(new JLabel(fields.get(i))); cbs_panel.add(cbs[i] = new JCheckBox("", true)); }

    JPanel center = new JPanel(new BorderLayout()); center.add("Center", labs_panel); center.add("East", cbs_panel);
    getContentPane().add("Center", new JScrollPane(center));

    // Construct the actions
    JButton bt;
    JPanel buttons = new JPanel(new FlowLayout());
    buttons.add(bt = new JButton("Cancel"));        bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { close(); } } );
    buttons.add(bt = new JButton("Remove Fields")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { removeFields(); } } );
    getContentPane().add("South", buttons);

    // Set it to visible
    pack(); setVisible(true);
  }

  /**
   * Close the dialog.
   */
  protected void close() { setVisible(false); dispose(); }

  /**
   *
   */
  protected void removeFields() {
    // Gather up the fields to remove
    Set<String> to_remove = new HashSet<String>(); for (int i=0;i<fields.size();i++) if (cbs[i].isSelected() == false) to_remove.add(fields.get(i));

    // Execute the remove operations
    Bundles root_bundles = parent.getRTParent().getRootBundles();
    root_bundles.getGlobals().removeFields(root_bundles, to_remove);

    // Update the bys
    parent.getRTParent().updateBys();

    // Close out the dialog
    close();
  }
}

/**
 * Dialog to join two tablets together.
 */
class JoinTabletsDialog extends JDialog {
  private static final long serialVersionUID = -4192124387918104608L;

  /**
   * Parent frame
   */
  protected RTControlFrame parent;

  /**
   * Tablet that will be modified
   */
  JComboBox<String> tablet_to_mod_cb,

  /**
   * Tablet to pull the information from
   */
                    tablet_from_cb;

  /**
   * Info about the two tablets selected
   */
  JTextField tablet_info_tf;

  /**
   * Primary Join Field from the "to mod" tablet
   */
  JComboBox<String> pri_field_to_mod_cb,
  /**
   * Primary Join Field from the "from" tablet
   */
                    pri_field_from_cb,
  /**
   * Secondary Join Field from the "to mod" tablet -- can be "none"
   */
                    sec_field_to_mod_cb,
  /**
   * Secondary Join Field from the "from" tablet -- may be disabled if the sec_field_to_mod_cb is "none"
   */
            sec_field_from_cb;

  /**
   * Overlapping information textfield
   */
  JTextField overlap_info_tf;

  /**
   * Columns to add to the "to mod" tablet.... these are pulled from the "from" tablet
   */
  JList<String> to_add_cols_ls;

  /**
   * Suffix to add to the added columns to avoid conflict... can be blank
   */
  JTextField suffix_tf;

  /**
   * Type of join operation
   */
  JComboBox<String> join_type_cb;

  /**
   * "Add Columns" join type - add columns to the "to mod" tablet
   */
  final static String JOIN_TYPE_ADD_COLUMNS  = "Add Columns",

  /**
   * "Tag Rows" join type - each row in the "to mod" tablet will get a tag
   * if a matching row is found in the "from" tablet.  Handles the multiple
   * mapping problem.
   */
                      JOIN_TYPE_TAG_ROWS     = "Tag Rows",

  /**
   * "Tag Entities" join type - entity in the "primary to mod" combobox will be
   * tagged by the columns in the "from" tablet via the entity tagging schema.
   */
                      JOIN_TYPE_TAG_ENTITIES = "Tag Entities";

  final static String join_type_strs[] = { JOIN_TYPE_TAG_ENTITIES,
                                           JOIN_TYPE_ADD_COLUMNS,
                                           JOIN_TYPE_TAG_ROWS };

  /**
   * Indicates that the join should only occur with a certain time frame ... both tablets will need
   * to have timestamps...
   */
  JComboBox<String> time_cb;

  final static String TIME_NA_STR              = "No Time Constraints",
                      TIME_FIVE_MINUTES_STR    = "5 Mins",
                      TIME_THIRTY_MINUTES_STR  = "30 Mins",
                      TIME_ONE_HOUR_STR        = "1 Hour",
                      TIME_FOUR_HOURS_STR      = "4 Hours",
                      TIME_DAYS_STR            = "1 Day";

  final static String time_strs[] = { TIME_NA_STR, TIME_FIVE_MINUTES_STR, TIME_THIRTY_MINUTES_STR, 
                                      TIME_ONE_HOUR_STR, TIME_FOUR_HOURS_STR, TIME_DAYS_STR };

  /**
   * Convert the timestring to the milliseconds equivalent.
   *
   *@param time_str time string from the above list
   *
   *@return equivalent number of milliseconds... zero if there is no time constraint
   */
  private long timeInMillis(String time_str) {
    if      (time_str.equals(TIME_NA_STR))             return 0L;
    else if (time_str.equals(TIME_FIVE_MINUTES_STR))   return        5L * 60L * 1000L;
    else if (time_str.equals(TIME_THIRTY_MINUTES_STR)) return       30L * 60L * 1000L;
    else if (time_str.equals(TIME_ONE_HOUR_STR))       return       60L * 60L * 1000L;
    else if (time_str.equals(TIME_FOUR_HOURS_STR))     return  4L * 60L * 60L * 1000L;
    else if (time_str.equals(TIME_DAYS_STR))           return 24L * 60L * 60L * 1000L;
    else                                               return 0L; // should probably throw an exception...
  }

  /**
   * Text to describe the result (and any errors / concerns with the join)
   */
  JTextField result_info_tf;

  /**
   * Button to execute the join operation
   */
  JButton join_tablets_bt;

  /**
   * Converts a tablet string to the tablet
   */
  Map<String,Tablet> tablet_lu = new HashMap<String,Tablet>();

  /*
   * Constructor
   *
   *@param parent  parent frame
   */
  public JoinTabletsDialog(RTControlFrame parent) {
    super(parent, "Join Tablets...", true); this.parent = parent;

    // Put the tablet info into a string set
    Iterator<Tablet> it_tab = parent.getRTParent().getVisibleBundles().tabletIterator(); List<String> tablet_ls = new ArrayList<String>();
    while (it_tab.hasNext()) { 
      Tablet tablet = it_tab.next(); tablet_ls.add(tablet.toString()); 
      tablet_lu.put(tablet.toString(),tablet);
    }
    String tablet_strs[] = new String[tablet_ls.size()]; tablet_ls.toArray(tablet_strs);

    // Construct the GUI
    setLayout(new BorderLayout(5,5)); JPanel panel;
    JPanel main = new JPanel(new BorderLayout());

    panel = new JPanel(new GridLayout(9,1,2,2));
    panel.add(new JLabel("Join Type"));
    panel.add(new JLabel("Modify Tablet"));
    panel.add(new JLabel("Pull From"));
    panel.add(new JLabel("Primary Field"));
    panel.add(new JLabel("Secondary Field"));
    panel.add(new JLabel("Overlap Info"));
    panel.add(new JLabel("New Field(s) Suffix"));
    panel.add(new JLabel("Time Constraints"));
    panel.add(new JLabel("Results"));
    main.add("West", panel);

    panel = new JPanel(new GridLayout(9,1,2,2));
    panel.add(join_type_cb     = new JComboBox<String>(join_type_strs));
    panel.add(tablet_to_mod_cb = new JComboBox<String>(tablet_strs));     
    panel.add(tablet_from_cb   = new JComboBox<String>(tablet_strs));

    tablet_to_mod_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { tabletToModChanged(); } } );
    tablet_from_cb  .addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { tabletFromChanged();  } } );
    join_type_cb    .addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateInfoFields();   } } );

    // Primary join fields
    JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 2));
    panel2.add(new JLabel("To Mod Tab"));
    panel2.add(pri_field_to_mod_cb = new JComboBox<String>(tabletFields((String) tablet_to_mod_cb.getSelectedItem(), false)));
    panel2.add(new JLabel("From Tab"));
    panel2.add(pri_field_from_cb   = new JComboBox<String>(tabletFields((String) tablet_from_cb.getSelectedItem(), false)));
    panel.add(panel2);

    pri_field_to_mod_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateInfoFields();   } } );
    pri_field_from_cb.  addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateInfoFields();   } } );

    // Secondary join fields
    panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 2));
    panel2.add(new JLabel("To Mod Tab"));
    panel2.add(sec_field_to_mod_cb = new JComboBox<String>(tabletFields((String) tablet_to_mod_cb.getSelectedItem(), true)));
    panel2.add(new JLabel("From Tab"));
    panel2.add(sec_field_from_cb   = new JComboBox<String>(tabletFields((String) tablet_from_cb.getSelectedItem(), true)));
    panel.add(panel2);

    sec_field_to_mod_cb.addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateInfoFields();   } } );
    sec_field_from_cb.  addItemListener(new ItemListener() { public void itemStateChanged(ItemEvent ie) { updateInfoFields();   } } );

    panel.add(overlap_info_tf = new JTextField()); overlap_info_tf.setEditable(false);
    panel.add(suffix_tf = new JTextField());
      suffix_tf.addCaretListener(new CaretListener() { public void caretUpdate(CaretEvent e) { updateInfoFields(); } } );
    panel.add(time_cb   = new JComboBox<String>(time_strs));
    panel.add(result_info_tf = new JTextField()); result_info_tf.setEditable(false);

    main.add("Center", panel);

    add("Center", main);

    // Jlist of the columns from the "from" tablet
    panel = new JPanel(new BorderLayout(5,5));
    panel.add("North", new JLabel("Columns To Add"));
    panel.add("Center", new JScrollPane(to_add_cols_ls = new JList<String>(tabletFields((String) tablet_from_cb.getSelectedItem(), false))));

    to_add_cols_ls.addListSelectionListener(new ListSelectionListener() { public void valueChanged(ListSelectionEvent lse) { updateInfoFields(); } } );

    add("East", panel);

    // Buttons to join or cancel
    panel = new JPanel(new FlowLayout()); JButton bt;

    // - Join Tablets Button
    panel.add(join_tablets_bt = new JButton("Join Tablets"));  
      join_tablets_bt.setEnabled(false);
      join_tablets_bt.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) { joinTablets(); }
      } ); 

    // - Cancel Dialog Button
    panel.add(bt              = new JButton("Cancel"));        
      bt.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ae) { cancelDialog(); }
      } );

    add("South",panel);

    // Configure some of the GUI items
    configTime();
    updateInfoFields();

    pack();
    setVisible(true);
  }

  /**
   * The "To Mod" tablet changed - update the related widgets.
   */
  private void tabletToModChanged() {
    String tablet_str = (String) tablet_to_mod_cb.getSelectedItem();
    updateComboBox(pri_field_to_mod_cb, tabletFields(tablet_str, false));
    updateComboBox(sec_field_to_mod_cb, tabletFields(tablet_str, true));
    configTime();
  }

  /**
   * The "From" tablet changed - update the related widgets.
   */
  private void tabletFromChanged() {
    String tablet_str = (String) tablet_from_cb.getSelectedItem();
    updateComboBox(pri_field_from_cb, tabletFields(tablet_str, false));
    updateComboBox(sec_field_from_cb, tabletFields(tablet_str, true));
    to_add_cols_ls.setListData(tabletFields(tablet_str, false));
    configTime();
  }

  /**
   * Determine if both the from/to mod have timestamps -- if so, enable the time
   * constrain options widget.
   */
  private void configTime() {
    String tablet_to_mod_str = (String) tablet_to_mod_cb.getSelectedItem();
    Tablet tablet_to_mod     = tablet_lu.get(tablet_to_mod_str);
    String tablet_from_str   = (String) tablet_from_cb.getSelectedItem();
    Tablet tablet_from       = tablet_lu.get(tablet_from_str);
    if (tablet_to_mod.hasTimeStamps() && tablet_from.hasTimeStamps()) time_cb.setEnabled(true);
    else { time_cb.setEnabled(false); time_cb.setSelectedItem(TIME_NA_STR); }
  }

  /**
   * Helper method to update items in a JComboBox.
   *@param cb   combobox to update
   *@param strs strings to set the combobox to
   */
  private void updateComboBox(JComboBox<String> cb, String strs[]) { 
    cb.removeAllItems(); for (int i=0;i<strs.length;i++) cb.addItem(strs[i]); 
  } 

  /**
   * Update the two fields related to information about the join
   */
  private void updateInfoFields() { updateOverlapInfo(); updateResultsInfo(); }

  /**
   * Helper methods...
   */
  private Tablet getToModTablet() { return tablet_lu.get((String) tablet_to_mod_cb.getSelectedItem()); }
  private Tablet getFromTablet()  { return tablet_lu.get((String) tablet_from_cb.  getSelectedItem()); }

  /**
   *
   */
  private void updateOverlapInfo() {
    Tablet mod_tab = getToModTablet(),
           frm_tab = getFromTablet();
    String pri_to_mod = (String) pri_field_to_mod_cb.getSelectedItem(),
           pri_from   = (String) pri_field_from_cb.  getSelectedItem(),
           sec_to_mod = (String) sec_field_to_mod_cb.getSelectedItem(),
           sec_from   = (String) sec_field_from_cb.  getSelectedItem();

    // Make sure settings are in transition
    if (mod_tab == null || frm_tab == null || pri_to_mod == null || pri_from == null || sec_to_mod == null || sec_from == null) {
      overlap_info_tf.setText("Null Value Found - GUI in transition");
      return;
    }

    // Determine if it's a single field or two field join
    Set<String> mod_set, frm_set;
    if (sec_to_mod.equals(NONE_STR) == true || sec_from.equals(NONE_STR) == true) {
      mod_set = fillSet(mod_tab,pri_to_mod);
      frm_set = fillSet(frm_tab,pri_from);
    } else {
      mod_set = fillSet(mod_tab,pri_to_mod,sec_to_mod);
      frm_set = fillSet(frm_tab,pri_from,  sec_from);
    }

    // Get the sizes.. do the intersection
    int mod_sz = mod_set.size(),
        frm_sz = frm_set.size();
    mod_set.retainAll(frm_set);
    int int_sz = mod_set.size();
    
    // Set the GUI string
    overlap_info_tf.setText("" + mod_sz + " To Mod Vals / " + frm_sz + " From Vals / " + int_sz + " Intersection");
  }

  /**
   * Update the results of the join here... should probably pull from a framework method.
   */
  private void updateResultsInfo() {
    // First step is to check to see if all of the fields are set appropriately
    // - Variables to make the checks easier...
    Tablet       mod_tab     = getToModTablet(),
                 frm_tab     = getFromTablet();
    String       pri_to_mod  = (String) pri_field_to_mod_cb.getSelectedItem(),
                 pri_from    = (String) pri_field_from_cb.  getSelectedItem(),
                 sec_to_mod  = (String) sec_field_to_mod_cb.getSelectedItem(),
                 sec_from    = (String) sec_field_from_cb.  getSelectedItem(),
                 join_type   = (String) join_type_cb.       getSelectedItem(),
                 suffix      = (String) suffix_tf.          getText();
    List<String> to_add_cols = to_add_cols_ls.getSelectedValuesList();

    // Check one by one
    StringBuffer sb = new StringBuffer();

    // - Null Tablets?
    if (mod_tab    == null) { if (sb.length() != 0) sb.append(" | "); sb.append("To Modify Tablet is null"); }
    if (frm_tab    == null) { if (sb.length() != 0) sb.append(" | "); sb.append("From Tablet is null");      }
    if (pri_to_mod == null) { if (sb.length() != 0) sb.append(" | "); sb.append("Pri Mod is null");          }
    if (sec_to_mod == null) { if (sb.length() != 0) sb.append(" | "); sb.append("Sec Mod is null");          }
    if (pri_from   == null) { if (sb.length() != 0) sb.append(" | "); sb.append("Pri From null");            }
    if (sec_from   == null) { if (sb.length() != 0) sb.append(" | "); sb.append("Sec From null");            }
  

    // - Zero Overlap?
    if (noOverlap(mod_tab,pri_to_mod,sec_to_mod,frm_tab,pri_from,sec_from)) {
      if (sb.length() != 0) sb.append(" | "); sb.append("No Overlap");
    }
    // - No Selected "to add" columns?
    if (to_add_cols.size() == 0) {
      if (sb.length() != 0) sb.append(" | "); sb.append("No Selected Columns To Add");
    }

    // Bad suffix?
    if (suffixOk() == false) {
      if (sb.length() != 0) sb.append(" | "); sb.append("Suffix can only have [a-zA-Z0-9_.]");
    }

    // For entity tagging, can only be a primary field selected
    if (join_type.equals(JOIN_TYPE_TAG_ENTITIES) && sec_to_mod != null && sec_from != null &&
        (sec_to_mod.equals(NONE_STR) == false ||
         sec_from.equals  (NONE_STR) == false)) {
      if (sb.length() != 0) sb.append(" | "); sb.append("For Entity Tagging -- only primary used");
    }

    // Overlapping column names?
    if (join_type.equals(JOIN_TYPE_ADD_COLUMNS) && 
        to_add_cols.size() > 0                  &&
        columnsOverlap(tabletFields(mod_tab, false), suffix, to_add_cols)) {
      if (sb.length() != 0) sb.append(" | "); sb.append("Columns Overlap");
    }

    // If so... enable the Join Tablets button
    if (sb.length() > 0) { result_info_tf.setText(sb.toString());
                           join_tablets_bt.setEnabled(false); return;
    } else               { result_info_tf.setText("Okay"); 
                           join_tablets_bt.setEnabled(true); }
  }

  /**
   * Determine if any of the column names will overlap.
   *
   *@param column_names        column names for the to modify tablet
   *@param suffix              suffix for the new columns -- can be empty string
   *@param additional_columns  columns that will be added
   *
   *@return true if there is an overlapping column
   */
  private boolean columnsOverlap(String column_names[], String suffix, List<String> additional_columns) {
    if (suffix == null) suffix = "";

    Set<String> set = new HashSet<String>(); for (int i=0;i<column_names.length;i++) set.add(column_names[i]);
    for (int i=0;i<additional_columns.size();i++) {
      String new_column_name = additional_columns.get(i) + suffix;
      if (set.contains(new_column_name)) return true;
    }
    return false;
  }

  /**
   * Make sure the suffix is okay.
   *
   *@return true for okay suffix
   */
  private boolean suffixOk() {
    String suffix = suffix_tf.getText();
    if (suffix.equals("")) return true; // Blank suffix may be okay... unless there are column overlaps...

    for (int i=0;i<suffix.length();i++) {
      char c = suffix.charAt(i);
      if ((c >= 'a' && c <= 'z') ||
          (c >= 'A' && c <= 'Z') ||
          (c >= '0' && c <= '9') ||
          (c == '.' || c == '_')) { } else return false;
    }
    return true;
  }

  /**
   * Return true if there's no overlapping fields in the specified settings.
   */
  private boolean noOverlap(Tablet mod_tab, String mod_fld0, String mod_fld1,
                            Tablet frm_tab, String frm_fld0, String frm_fld1) {

    if (mod_tab == null || mod_fld0 == null || mod_fld1 == null ||
        frm_tab == null || frm_fld0 == null || frm_fld1 == null) return false; // If the GUI isn't fully done yet

    if (mod_fld1.equals(NONE_STR) || frm_fld1.equals(NONE_STR)) {
      // Simple single field join
      Set<String> mod_set = fillSet(mod_tab,mod_fld0),
                  frm_set = fillSet(frm_tab,frm_fld0);
      mod_set.retainAll(frm_set);
      return (mod_set.size() == 0);
    } else {
      // Two field join
      Set<String> mod_set = fillSet(mod_tab,mod_fld0,mod_fld1),
                  frm_set = fillSet(frm_tab,frm_fld0,frm_fld1);
      mod_set.retainAll(frm_set);
      return (mod_set.size() == 0);
    }
  }

  /**
   *
   */
  private Set<String> fillSet(Tablet tab, String pri) {
    Set<String> set = new HashSet<String>(); 
    if (tab == null || pri == null) return set; // Seems to have bad values on construction

    KeyMaker km = new KeyMaker(tab,pri); Iterator<Bundle> it = tab.bundleIterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); String keys[] = km.stringKeys(bundle);
      if (keys != null && keys.length > 0) for (int i=0;i<keys.length;i++) set.add(keys[i]);
    }
    return set;
  }

  /**
   *
   */
  private Set<String> fillSet(Tablet tab, String pri, String sec) {
    Set<String> set = new HashSet<String>();
    if (tab == null || pri == null || sec == null) return set; // Seems to have bad values on construction

    KeyMaker km0 = new KeyMaker(tab,pri), km1 = new KeyMaker(tab,sec);
    Iterator<Bundle> it = tab.bundleIterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); String keys0[] = km0.stringKeys(bundle);
      if (keys0 != null && keys0.length > 0) {
        String keys1[] = km1.stringKeys(bundle);
        if (keys1 != null && keys1.length > 0) {
          for (int i=0;i<keys0.length;i++)
            for (int j=0;j<keys1.length;j++)
              set.add(Utils.encToURL(keys0[i]) + "|" + Utils.encToURL(keys1[i]));
        }
      }
    }
    return set;
  }

  /**
   * Cancel the dialog.
   */
  private void cancelDialog() {
    setVisible(false);
    dispose();
  }

  /**
   * Join the tablets based on the settings.
   */
  private void joinTablets() {
    // Get a copy of the values...
    String       join_type   = (String) join_type_cb.       getSelectedItem();
    Tablet       mod_tab     = getToModTablet(),
                 frm_tab     = getFromTablet();
    String       pri_to_mod  = (String) pri_field_to_mod_cb.getSelectedItem(),
                 pri_from    = (String) pri_field_from_cb.  getSelectedItem(),
                 sec_to_mod  = (String) sec_field_to_mod_cb.getSelectedItem(),
                 sec_from    = (String) sec_field_from_cb.  getSelectedItem();
    List<String> to_add_cols = to_add_cols_ls.getSelectedValuesList();
    String       suffix      = suffix_tf.getText();
    String       time        = (String) time_cb.            getSelectedItem();
    if (mod_tab.hasTimeStamps() == false || frm_tab.hasTimeStamps() == false) { time = TIME_NA_STR; } // zero out time constrains if tablets don't have time
    long         time_l      = timeInMillis(time);

    // Execute the specified join type...  
    // -- Tag Entities is local (not part of the framework)
    // -- Other Join Types are handled by framework methods
    if        (join_type.equals(JOIN_TYPE_TAG_ENTITIES)) {

      addEntityTags(mod_tab, frm_tab, pri_to_mod, pri_from, to_add_cols, suffix, time_l);

      System.err.println("**\n** Need To Dedupe Entity Tags ... multiple identical rows will cause multiple tags...\n**");
      System.err.println("**\n** Need To Refresh The GUI?...\n**");

      setVisible(false); dispose();

    //
    // Join By Adding Columns -- Framework call
    //
    } else if (join_type.equals(JOIN_TYPE_ADD_COLUMNS))  {

      if (sec_to_mod.equals(NONE_STR)) sec_to_mod = null;
      if (sec_from.  equals(NONE_STR)) sec_from   = null;

      BundlesUtils.tabletJoinAddColumns(parent.getRTParent().getRootBundles(), 
                                        mod_tab, pri_to_mod, sec_to_mod, 
                                        frm_tab, pri_from,   sec_from, 
                                        to_add_cols, suffix, time_l);
      // Update the bys
      parent.getRTParent().updateBys();
      // Update each panels application data
      parent.getRTParent().refreshPanelApplicationData();

      setVisible(false); dispose();

    //
    // Join By Adding Row Tags -- Framework call
    //
    } else if (join_type.equals(JOIN_TYPE_TAG_ROWS))     {

      if (sec_to_mod.equals(NONE_STR)) sec_to_mod = null;
      if (sec_from.  equals(NONE_STR)) sec_from   = null;

      BundlesUtils.tabletJoinTagRows(parent.getRTParent().getRootBundles(), 
                                     mod_tab, pri_to_mod, sec_to_mod, 
                                     frm_tab, pri_from,   sec_from, 
                                     to_add_cols, suffix, time_l);

      // Update the bys
      parent.getRTParent().updateBys();
      // Update each panels application data
      parent.getRTParent().refreshPanelApplicationData();

      setVisible(false); dispose();

    } else JOptionPane.showMessageDialog(this, "Join Type Not Understood " + join_type, "Join Type Error", JOptionPane.ERROR_MESSAGE);
  }

  /**
   * String for the None Option in the secondary field for joining
   */
  final static String NONE_STR = "|None|";

  /**
   * Helper method for passing the tablet string key.
   */
  private String[] tabletFields(String tablet_str, boolean inc_none) {
    return tabletFields(tablet_lu.get(tablet_str), inc_none);
  }

  /**
   * Return the fields (as an array of Strings) in the specified tablet string.
   *
   *@param tablet     tablet to pull the fields from
   *@param inc_none   include the none string -- for the secondard fields
   */
  private String[] tabletFields(Tablet tablet, boolean inc_none) {
    Bundles      bundles   = parent.getRTParent().getVisibleBundles();
    List<String> fields_ls = new ArrayList<String>();

    // Go through the fields and add them to the list
    int     flds[]  = tablet.getFields();
    for (int fld_i=0;fld_i<flds.length;fld_i++) {
      if (flds[fld_i] != -1) fields_ls.add(bundles.getGlobals().fieldHeader(fld_i));
    }

    // Sort them
    Collections.sort(fields_ls);

    // Copy to an array and return
    String field_array[] = new String[inc_none ? (1 + fields_ls.size()) : (fields_ls.size())];
    int i = 0; if (inc_none) field_array[i++] = NONE_STR;
    for (int j=0;j<fields_ls.size();j++) field_array[i++] = fields_ls.get(j);

    return field_array;
  }

  /**
   * Add entity tags based on the parameters from the dialog (which are all passed in and
   * should be safe to use as is.)
   *
   *@param mod_tab     tablet that contains the entities to modify (which are the pri_to_mod field entries)
   *@param frm_tab     information about how to tag the entities
   *@param pri_to_mod  primary field from the mod_tab to join on
   *@param pri_from    primary field from the frm_tab to join on
   *@param to_add_cols tag type part... and the columns to fill the values from
   *@param suffix      suffix to add to the type part -- can be empty string
   *@param time_l      time constraint as milliseconds... if zero (0), then no time constraints
   */
  private void addEntityTags(Tablet       mod_tab, 
                             Tablet       frm_tab, 
                             String       pri_to_mod, 
                             String       pri_from, 
                             List<String> to_add_cols, 
                             String       suffix, 
                             long         time_l) {
    // What to add at the end... in bulk
    Map<String,Set<EntityTag>> tag_map = new HashMap<String,Set<EntityTag>>();

    // Find which entities are applicable... (may be shortsighted... should we only
    // apply entity tags to ones that exist in the to_mod tablet?  or every entity
    // that exists in the frm_tab?)
    Set<String> applies_to = new HashSet<String>();
    KeyMaker km    = new KeyMaker(mod_tab, pri_to_mod);
    Iterator<Bundle> it = mod_tab.bundleIterator(); while (it.hasNext()) {
      String keys[] = km.stringKeys(it.next());
      if (keys != null && keys.length > 0 && keys[0].equals(BundlesDT.NOTSET) == false) {
        for (int i=0;i<keys.length;i++) applies_to.add(keys[i]);
      }
    }

    // Collate from the frm_tab
    km = new KeyMaker(frm_tab, pri_from);

    KeyMaker kms[] = new KeyMaker[to_add_cols.size()];
    for (int i=0;i<kms.length;i++) kms[i] = new KeyMaker(frm_tab, to_add_cols.get(i));

    it = frm_tab.bundleIterator(); while (it.hasNext()) {
      Bundle bundle = it.next(); String keys[] = km.stringKeys(bundle);
      if (keys != null && keys.length > 0 && keys[0].equals(BundlesDT.NOTSET) == false) {
        // Figure out if we care about timestamps... then set them
        long ts0 = 0L, ts1 = 0L;
        if (time_l == 0L || frm_tab.hasTimeStamps() == false) {
          ts0 = ts1 = 0L;
        } else {
          if (frm_tab.hasDurations()) { ts0 =       bundle.ts0(); ts1 = bundle.ts1(); }
          else                        { ts0 =       bundle.ts0() - time_l;
                                        ts1 =       bundle.ts0() + time_l; }
        }

        // type / vals
        for (int i=0;i<kms.length;i++) {
          String type   = makeTagTypeSafe(to_add_cols.get(i) + suffix);
          String vals[] = kms[i].stringKeys(bundle);

          // Record the value pairs
          if (vals != null && vals.length > 0 && vals[0].equals(BundlesDT.NOTSET) == false) for (int j=0;j<vals.length;j++) {
            for (int k=0;k<keys.length;k++) {
              // entity to tag is keys[k] ... type is type ... value is vals[j]
              // timeframe is either ts0 +/- time_l ... or it's ts0 to ts1 ... or it's not time constrained (ts0 == 0L)

              if (applies_to.contains(keys[k])) { // only matching entities from the mod_tab

                EntityTag entity_tag = new EntityTag(keys[k], type + "=" + vals[j], ts0, ts1);
                if (ts0 == 0L) entity_tag.setToForever();

                if (tag_map.containsKey(keys[k]) == false) tag_map.put(keys[k], new HashSet<EntityTag>());
                tag_map.get(keys[k]).add(entity_tag);
              }
            }
          }
        }
      }
    }

    // Go through each entity... figure out if tags need to be deduped or combined
    Iterator<String> it_entity = tag_map.keySet().iterator(); while (it_entity.hasNext()) {
      String          entity = it_entity.next(); 
      Set<EntityTag>  set    = tag_map.get(entity);

      // Separate the each individual tag into it's own set
      Map<String,Set<EntityTag>> by_tag = new HashMap<String,Set<EntityTag>>();
      Iterator<EntityTag> it_et = set.iterator(); while (it_et.hasNext()) {
        EntityTag et = it_et.next();
        if (by_tag.containsKey(et.getTag()) == false) by_tag.put(et.getTag(), new HashSet<EntityTag>());
        by_tag.get(et.getTag()).add(et);
      }

      // Deal with each tag separately...
      Iterator<String> it_tag = by_tag.keySet().iterator(); while (it_tag.hasNext()) {
        String tag = it_tag.next();

        // If it's just one... just set it and move on
        if (by_tag.get(tag).size() == 1) {

          parent.getRTParent().addEntityTags(by_tag.get(tag));

        } else {
          // Sort the list
          List<EntityTag> list = new ArrayList<EntityTag>(); 
          list.addAll(by_tag.get(tag)); Collections.sort(list, new Comparator<EntityTag>() { 
            public int compare(EntityTag et1, EntityTag et2) {
              if      (et1.ts0() < et2.ts0()) return -1;
              else if (et1.ts0() > et2.ts0()) return  1;
              else                            return  0;
            }
          });

          // Deconflict the list ... dedupe... merge... etc.
          int i = 0; while (i < (list.size()-1)) {
            // System.err.print("list.get(i) = " + pp(list.get(i)) + " || list.get(i+1) = " + pp(list.get(i+1)));
            if         (list.get(i).ts0() == list.get(i+1).ts0() &&
                        list.get(i).ts1() == list.get(i+1).ts1()) {
              // System.err.println("  DUPE");
              list.remove(i+1);                     // Duplicate record
            } else if  (list.get(i+1).ts1() <= list.get(i).ts1()) {
              // System.err.println("  ENCAPS");
              list.remove(i+1);                     // Second record is encapsulated by first
            } else if  (list.get(i).ts1() >= list.get(i+1).ts0() &&
                        list.get(i).ts1() <= list.get(i+1).ts1()) {
              // System.err.println("  MERGE");
              list.get(i).ts1(list.get(i+1).ts1()); // Merge the records together
              list.remove(i+1); 
            } else { 
              // System.err.println("");              
              i++;                                 // Records aren't duplicates nor overlap
            }
          }

          // Add the records
          parent.getRTParent().addEntityTags(list);
        }
      }
    }
  }

  String pp(EntityTag et) { return et.getEntity() + " : " + et.getTag() + " : " + Utils.exactDate(et.ts0()) + " ==> " + Utils.exactDate(et.ts1()); }

  /**
   * Make a tag type string safe... not sure what the exact rules were but will stick
   * with just alphanumerics, periods, and underscores.
   */
  private String makeTagTypeSafe(String str) {
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<str.length();i++) {
      char c = str.charAt(i);
      if ((c >= 'a' && c <= 'z') ||
          (c >= 'A' && c <= 'Z') ||
          (c >= '0' && c <= '9') ||
          (c == '.' || c == '_')) sb.append(c);
      else sb.append("_");
    }
    return sb.toString();
  }
}

/**
 *
 */
class PasteIndicatorsDialog extends JDialog {
  private static final long serialVersionUID = -4469461156525234222L;
  /**
   * Parent rt control frame
   */
  RTControlFrame rt_control_frame;

  /**
   * Text area for pasteing indicators
   */
  JTextArea text_area;

  /**
   * Tags textfield
   */
  JTextField tags_tf;

  /**
   * Constructor -- put together the GUI and show it.
   *
   *@param rt control frame parent
   */
  public PasteIndicatorsDialog(RTControlFrame parent) {
    super(parent, "Paste Indicators", false); this.rt_control_frame = parent;

    getContentPane().setLayout(new BorderLayout());
    JButton bt; JPanel panel;

    panel = new JPanel(new FlowLayout());
    panel.add(bt = new JButton("Clr"));             bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { clearTextArea();     } } );
    panel.add(bt = new JButton("Replace"));         bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { pasteTextArea();     } } );
    panel.add(bt = new JButton("Deobf"));           bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { deobfuscate();       } } );
    panel.add(bt = new JButton("Xtract"));          bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { extractIndicators(); } } );
    panel.add(bt = new JButton("Xtract/No Times")); bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { noTimes();           } } );
    getContentPane().add("North", panel);

    getContentPane().add("Center", new JScrollPane(text_area = new JTextArea()));

    // Southern panel for adding and canceling
    panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("Tags"));
    panel.add(tags_tf = new JTextField(24));
    panel.add(bt = new JButton("Add Indicators"));  bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { addIndicators(); } } );
    panel.add(bt = new JButton("Close"));           bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { closeDialog(); } } );
    getContentPane().add("South", panel);

    pack(); setSize(640,480);

    setVisible(true);
  }

  /**
   * Clear the text area.
   */
  private void clearTextArea() {
    text_area.setText("");
  }

  /**
   * Paste from the clipboard into the text area.
   */
  private void pasteTextArea() {
    text_area.setText(Utils.getClipboardText(this));
  }

  /**
   * Deobfuscate any indicators that have a bracket [dot] bracket.
   */
  private void deobfuscate() {
    text_area.setText(Utils.deobfuscateEntity(text_area.getText()));
  }

  /**
   * Extract the entities from the text and return as a list.
   */
  private List<String> extractAndDeDupe(boolean include_times) {
    // Get the subtexts
    List<SubText> list = EntityExtractor.list(text_area.getText());

    if (include_times == false) {
      Iterator<SubText> it = list.iterator(); while (it.hasNext()) {
        SubText st = it.next(); if (st instanceof TimeStamp || st instanceof Interval) it.remove();
      }
    }

    // Sort by the type of subtext
    Collections.sort(list, new Comparator<SubText>() { public int compare(SubText st0, SubText st1) {
      if (st0.getType().equals(st1.getType())) {
        if (st0 instanceof Entity && st1 instanceof Entity) {
          Entity st0e = (Entity) st0, st1e = (Entity) st1;
          if (st0e.getDataType() == st1e.getDataType()) return st0.toString().compareTo(st1.toString());
          else {
            String dt0 = "" + st0e.getDataType(), dt1 = "" + st1e.getDataType();
            return dt0.compareTo(dt1);
          }
        }
        return st0.toString().compareTo(st1.toString());
      } else return st0.getType(). compareTo(st1.getType());
    } public boolean equals(Object obj) { return false; } } );

    // Deduplicate
    List<String> dedupe = new ArrayList<String>();
    Set<String>  set    = new HashSet<String>();
    Iterator<SubText> it = list.iterator(); while (it.hasNext()) {
      SubText st = it.next();
      if (set.contains(st.toString()) == false) { 
        dedupe.add(st.toString()); set.add(st.toString()); 
      }
    }

    return dedupe;
  }

  /**
   * Remove any timestamps...
   */
  private void noTimes() {
    // Get the subtexts
    List<String> list = extractAndDeDupe(false);

    // Clear text and add the extracted entities
    text_area.setText("");
    Iterator<String> it_str = list.iterator(); while (it_str.hasNext()) { text_area.append(it_str.next().toString() + "\n"); }
  }

  /**
   * Extract the indicators from unstructured text and replace the textarea with the indicators.
   */
  private void extractIndicators() {
    // Get the subtexts
    List<String> list = extractAndDeDupe(true);

    // Clear text and add the extracted entities
    text_area.setText("");
    Iterator<String> it_str = list.iterator(); while (it_str.hasNext()) { text_area.append(it_str.next().toString() + "\n"); }
  }

  /**
   * Close the dialog without adding indicators
   */
  private void closeDialog() { setVisible(false); dispose(); }

  /**
   * Add the indicators as a new table.
   */
  private void addIndicators() { 
    // Keep track of the bundles added
    Set<Bundle> bundles_added = new HashSet<Bundle>();
    
    // Get the subtexts
    List<SubText> list = EntityExtractor.list(text_area.getText());

    long ts = System.currentTimeMillis();

    Set<String> done = new HashSet<String>();

    Iterator<SubText> it = list.iterator(); while (it.hasNext()) {
      SubText subtext = it.next(); if (done.contains(subtext.toString())) continue;
      done.add(subtext.toString());

      //
      // Entity table additions
      //
      if        (subtext instanceof Entity)    {
        Map<String,String> attr_map = new HashMap<String,String>();
        attr_map.put(RTControlFrame.IT_INDICATOR_STR, subtext.toString());
        attr_map.put(RTControlFrame.IT_USER_STR,      RT.getUserName());
        attr_map.put(RTControlFrame.IT_TAGS_STR,      tags_tf.getText());
        attr_map.put(RTControlFrame.IT_URL_STR,       "");
        attr_map.put(RTControlFrame.IT_TITLE_STR,     "");
        attr_map.put(RTControlFrame.IT_SOURCE_STR,    "Paste Indicators");

        Tablet indicator_tablet = rt_control_frame.findOrCreateIndicatorTablet();
        Bundle bundle = indicator_tablet.addBundle(attr_map, Utils.exactDate(ts));
        bundles_added.add(bundle);

        // Set entity tags too (assuming that there are tags set)
        if (tags_tf.getText().equals("") == false) { rt_control_frame.getRTParent().tagEntity(subtext.toString(), tags_tf.getText()); }

      //
      // Timestamps
      //
      } else if (subtext instanceof TimeStamp) {
        rt_control_frame.getRTParent().addTimeMarker("Pasted", ((TimeStamp) subtext).getTimeStamp());

      //
      // Intervals
      //
      } else if (subtext instanceof Interval)  {
        rt_control_frame.getRTParent().addTimeMarker("Pasted", ((Interval) subtext).getMinTimeStamp(), ((Interval) subtext).getMaxTimeStamp());

      } else System.err.println("Do Not Understand Subtext \"" + subtext + "\"");

    }

    // Propagate changes across the application
    Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(rt_control_frame.getRTParent().getRootBundles());
    rt_control_frame.getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
    rt_control_frame.getRTParent().updatePanelsForNewBundles(bundles_added);
  }
}

/**
 * Dialog to manage the indicator filter used to ignore indicators from the
 * native messaging browser integration.
 */
class ManageIndicatorFiltersDialog extends JDialog implements TableModel {
  private static final long serialVersionUID = -2129469544523334811L;

  /**
   * Simple Table For Indicators ... allows for rows to be added
   */
  JTable table;

  /**
   * Original filter list stored in the RTPrefs
   */
  String filters_orig[];

  /**
   * Constructor
   */
  public ManageIndicatorFiltersDialog(RTControlFrame parent) {
    super(parent, "Manage Indicator Filter", false);

    filters_orig = RTPrefs.retrieveStrings(RT.native_messaging_store);
    if (filters_orig == null) filters_orig = new String[0];

    getContentPane().add("Center", new JScrollPane(table = new JTable(this) {
      public String getToolTipText(MouseEvent e) {
        return "IPv4, IPv4 CIDR, Domain, Simple Domain Wildcards";
      }
    }));

    JPanel buttons = new JPanel(new FlowLayout());

    JButton bt;
    buttons.add(bt = new JButton("Close"));
    getContentPane().add("South", buttons);

    bt.addActionListener(new ActionListener() { public void actionPerformed(ActionEvent ae) { setVisible(false); dispose(); } } );
    addWindowListener(new WindowAdapter() { public void windowClosing(WindowEvent we) { setVisible(false); dispose(); } } );

    pack(); setVisible(true);
  }

  /**
   * Table Model Contract
   */
  public void    addTableModelListener   (TableModelListener listener) { }
  public void    removeTableModelListener(TableModelListener listener) { }
  public Object  getValueAt(               int rowIndex, int colIndex) { if (rowIndex < filters_orig.length) return filters_orig[rowIndex]; 
                                                                         else                                return ""; }
  public boolean isCellEditable(           int rowIndex, int colIndex) { return true; }
  public int     getRowCount()                                         { return filters_orig.length + 1; }
  public int     getColumnCount()                                      { return 1; }
  public String  getColumnName(                          int colIndex) { return "Filter"; }
  public Class   getColumnClass(                         int colIndex) { return String.class; }

  public void    setValueAt(Object object, int rowIndex, int colIndex) { 
    String str = (String) object;
    if (str == null || str.equals("")) {
      if (rowIndex < filters_orig.length) {
        // Remove an element from the filters list
        String new_filters[] = new String[filters_orig.length-1]; 
        int i=0; for (int j=0;j<filters_orig.length;j++) {
          if (j == rowIndex) continue;
          new_filters[i++] = filters_orig[j];
        }

        filters_orig = new_filters;
        RTPrefs.store(RT.native_messaging_store, filters_orig);
      }
    } else {
      if (rowIndex < filters_orig.length) {
        // Change the value 
        filters_orig[rowIndex] = str;

        RTPrefs.store(RT.native_messaging_store, filters_orig);
      } else {
        // Add a value
        String new_filters[] = new String[filters_orig.length+1];
        System.arraycopy(filters_orig, 0, new_filters, 0, filters_orig.length);
        new_filters[new_filters.length-1] = str;

        filters_orig = new_filters;
        RTPrefs.store(RT.native_messaging_store, filters_orig);
      }
    }
  }
}

