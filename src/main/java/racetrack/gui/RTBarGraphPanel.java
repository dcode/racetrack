/* 

Copyright 2019 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;

import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;

import java.text.SimpleDateFormat;

import java.util.Set;
import java.util.Iterator;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Map;
import java.util.HashMap;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.BundlesG;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;
import racetrack.util.Utils;
import racetrack.visualization.RTColorManager;

/**
 * Visualization for showing a bar graph... incomplete.
 *
 *@author  D. Trimm
 *@version 1.2
 */
public class RTBarGraphPanel extends RTPanel {
  private static final long serialVersionUID = -3330124342999124608L;

  /**
   * X-axis binning option
   */
  JComboBox<String>     bin_cb;

  /**
   * X ordering
   */
  JRadioButtonMenuItem x_orders[],

  /**
   * Y scale
   */
                       y_scales[],

  /**
   * Bar widths
   */
                       bar_widths[],

  /**
   * Modes for visualization
   */
                       modes[],

  /**
   * Labeling
   */
                       labels[];

  /**
   * Strings representing ordering options
   */
  final static String  ORDER_DEFAULT_STR      = "Default Order",
                       ORDER_MAX_STR          = "Max Order",
                       ORDER_MIN_STR          = "Min Order",
                       ORDER_ALPHA_STR        = "Alphabetical Order";

  /**
   * Strings representing scaling for the y axis
   */
  final static String  LINEAR_SCALE_STR       = "Linear",
                       LOG_SCALE_STR          = "Log";

  /**
   * Strings representing barwidth
   */
  final static String  BARW_8_STR = "8 Pixels",
                       BARW_4_STR = "4 Pixels",
                       BARW_2_STR = "2 Pixels",
                       BARW_1_STR = "1 Pixels";

  /** 
   * Modes for the application
   */
  final static String MODE_STACKED_STR          = "Stacked",
                      MODE_SEPARATE_STR         = "Separate (By Color)",
                      MODE_PERCENTAGE_STR       = "Percentage",
                      MODE_BOXPLOT_STANDARD_STR = "BoxPlot (Standard)",
                      MODE_BOXPLOT_MINIMAL_STR  = "BoxPlot (Minimal)";

  /**
   * Labeling options
   */
  final static String LABEL_NONE_STR            = "No Labels",
                      LABEL_STANDARD_STR        = "Standard",
                      LABEL_STANDARD_SHORT_STR  = "Standard (Short)",
                      LABEL_OVERLAY_STR         = "Overlay";

  /**
   * Arrays for visualization configuration
   */
  final static String x_order_strs[]    = { ORDER_DEFAULT_STR, ORDER_MAX_STR, ORDER_MIN_STR, ORDER_ALPHA_STR };

  final static String y_scale_strs[]    = { LINEAR_SCALE_STR, LOG_SCALE_STR };

  final static String bar_width_strs[]  = { BARW_8_STR, BARW_4_STR, BARW_2_STR, BARW_1_STR };

  final static String mode_strs[]       = { MODE_STACKED_STR, MODE_SEPARATE_STR, MODE_PERCENTAGE_STR, MODE_BOXPLOT_STANDARD_STR, MODE_BOXPLOT_MINIMAL_STR };

  final static String label_strs[]      = { LABEL_NONE_STR, LABEL_STANDARD_STR, LABEL_STANDARD_SHORT_STR, LABEL_OVERLAY_STR };

  /**
   * Construct a new panel with the specified GUI parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt      GUI parent
   */
  public RTBarGraphPanel (RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt) {
    super(win_type,win_pos,win_uniq,rt);   
    // Main component
    add("Center", component = new RTBarGraphComponent());

    for (int j=0;j<5;j++) {

      if (j != 0) getRTPopupMenu().addSeparator();

      String strs[] = null; String label = "";
      switch (j) {
        case 0:  strs = x_order_strs;   label = "X Order";   break;
        case 1:  strs = y_scale_strs;   label = "Y Scale";   break;
        case 2:  strs = bar_width_strs; label = "Bar Width"; break;
        case 3:  strs = mode_strs;      label = "Mode";      break;
        case 4:  strs = label_strs;     label = "Labels";    break;
      }

      JRadioButtonMenuItem rbs[]; ButtonGroup bg;

      bg = new ButtonGroup(); getRTPopupMenu().add(new JLabel(label));
      rbs = new JRadioButtonMenuItem[strs.length]; for (int i=0;i<strs.length;i++) { 
        rbs[i] = new JRadioButtonMenuItem(strs[i], i == 0); getRTPopupMenu().add(rbs[i]); bg.add(rbs[i]); defaultListener(rbs[i]);
      }

      switch (j) {
        case 0:  x_orders   = rbs; break;
        case 1:  y_scales   = rbs; break;
        case 2:  bar_widths = rbs; break;
        case 3:  modes      = rbs; break;
        case 4:  labels     = rbs; break;
      }
    }

    // Configuration panel
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("X Bin")); panel.add(bin_cb  = new JComboBox<String>());

    // Add the default listeners
    defaultListener(bin_cb);

    // Fill the comboboxes
    updateBys();
    add("South", panel);
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "barchart"; }

  /**
   * Get the bin for the x-axis
   *
   *@return bin for x-axis
   */
  public String       bin           ()           { return (String) bin_cb.getSelectedItem(); }

  /**
   * Set the bin for the x-axis
   *
   *@param str bin for x-axis
   */
  public void         bin           (String str) { bin_cb.setSelectedItem(str); }

  /**
   * Return the scale to use on the x-axis
   *
   *@return x-axis scale
   */
  public String       xOrder        ()           { return selectedItem(x_orders); }

  /**
   * Set the scale to use on the x-axis.
   *
   *@param str x-axis scale
   */
  public void         xOrder        (String str) { setSelectedItem(x_orders, str); }

  /**
   * Return the bar width for the x bars.
   *
   *@return bar width in pixels
   */
  public String barWidth() { return selectedItem(bar_widths); }

  /**
   * Set the bar width for the x bars.
   * 
   *@param bar width string
   */
  public void barWidth(String str) { setSelectedItem(bar_widths, str); }

  /**
   * Return the bar width as an integer.
   *
   *@param str input string
   *
   *@return bar width value
   */
  public int barWidthValue(String str) { 
          if      (str.equals(BARW_8_STR)) return 8;
          else if (str.equals(BARW_4_STR)) return 4;
          else if (str.equals(BARW_2_STR)) return 2;
          else if (str.equals(BARW_1_STR)) return 1;
          else                             return 4;
  }

  /**
   * Return the scale to use on the y-axis.  Only applies to non-box plot modes.
   *
   *@return y-axis scale
   */
  public String       yScale        ()           { return selectedItem(y_scales); }

  /**
   * Set the scale to usse on the y-axis
   *
   *@param str y-axis scale
   */
  public void         yScale        (String str) { setSelectedItem(y_scales, str); }

  /**
   * Return the mode of the application.  Currently supports box-plot or counting modes.  Could
   * eventually incorporate sums, maxes, mins, standard deviations, etc.
   *
   *@return application mode
   */
  public String       mode          ()           { return selectedItem(modes); }

  /**
   * Set the mode of the application.
   *
   *@param str mode of the string application
   */
  public void         mode          (String str) { setSelectedItem(modes, str); }

  /**
   * Return the labeling option
   *
   *@return labeling option string
   */
  public String      labelOption    ()           { return selectedItem(labels); }

  /**
   * Set the labeling option
   *
   *@param f true to draw labels
   */
  public void         labelOption    (String str)  { setSelectedItem(labels, str); }

  /**
   * Get the configuration of this component as a string.  Supposed to be used for
   * bookmarking a view so that it can be re-rendered.
   *
   *@return string representing configuration of the panel
   */
  public String  getConfig()  { return "RTBarGraphPanel" + BundlesDT.DELIM +
                                       "mode="           + Utils.encToURL(mode())               + BundlesDT.DELIM +
                                       "bin="            + Utils.encToURL(bin())                + BundlesDT.DELIM +
                                       "xorder="         + Utils.encToURL(xOrder())             + BundlesDT.DELIM +
                                       "barwidth="       + Utils.encToURL(barWidth())           + BundlesDT.DELIM +
                                       "yscale="         + Utils.encToURL(yScale())             + BundlesDT.DELIM +
                                       "labels="         + Utils.encToURL(labelOption()); }

  /**
   * Set the configuration of this component from the string representation.
   *
   *@return previously returned string from the getConfig() method
   */
  public void    setConfig(String str) {
    StringTokenizer st = new StringTokenizer(str,BundlesDT.DELIM);
    if (st.nextToken().equals("RTBarGraphPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not A RTBarGraphPanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      if      (type.equals("mode"))        mode(Utils.decFmURL(value));
      else if (type.equals("bin"))         bin(Utils.decFmURL(value));
      else if (type.equals("xorder"))      xOrder(Utils.decFmURL(value));
      else if (type.equals("barwidth"))    barWidth(Utils.decFmURL(value));
      else if (type.equals("yscale"))      yScale(Utils.decFmURL(value));
      else if (type.equals("labels"))      labelOption(Utils.decFmURL(value));
      else throw new RuntimeException("Do Not Understand Type-Value Pair \"" + type + "\"=\"" + value + "\"");
    }
  }

  /**
   * Update the comboboxes for selecting global fields when new data is
   * loaded.
   */
  public void         updateBys() { 
    if (bin_cb  != null) updateEntityBys(bin_cb); 
  }

  /**
   * Generic method to update a combobox.
   *
   *@param cb combobox to update
   */
  public void         updateEntityBys(JComboBox<String> cb) {
    String strs[]; Object sel = cb.getSelectedItem();
    cb.removeAllItems();
    strs = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), false, true, true, true);
    for (int i=0;i<strs.length;i++) cb.addItem(strs[i]);
    if (sel == null) cb.setSelectedIndex(0); else cb.setSelectedItem(sel);
  }

  /**
   * Generic method to update a combobox with just scalar fields.
   *
   *@param cb combobox to update
   */
  public void         updateScalarBys(JComboBox<String> cb) {
    String strs[]; Object sel = cb.getSelectedItem();
    cb.removeAllItems();
    strs = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals(), true, true, true, false);
    cb.addItem(BundlesDT.COUNT_BY_DEFAULT);
    for (int i=0;i<strs.length;i++) cb.addItem(strs[i]);
    if (sel == null) cb.setSelectedIndex(0); else cb.setSelectedItem(sel);
  }

  /**
   * Component that handles painting and interacting with the visualization.
   */
  public class RTBarGraphComponent extends RTComponent {
    private static final long serialVersionUID = -6550124342318124608L;

    /**
     * Key pressed interface for KeyListener.  Just passes to the parent for handling...
     *
     *@param ke key event
     */
    public void keyPressed(KeyEvent ke) { super.keyPressed(ke); }

    /**
     * Key released interface for KeyListener.  Just passes to the parent for handling...
     *
     *@param ke key event
     */
    public void keyReleased(KeyEvent ke) { super.keyReleased(ke); }

    /**
     *
     */
    public void keyTyped(KeyEvent ke) { 
      super.keyTyped(ke);
      if (ke.getKeyChar() == ' ') {
        if (yScale().equals(LINEAR_SCALE_STR)) yScale(LOG_SCALE_STR);
        else                                   yScale(LINEAR_SCALE_STR);
      }
    }

    /**
     * Copy a screenshot of the rendering to the clipboard.
     *
     *@param shft shift key down
     *@param alt  alt key down
     */
    @Override
    public void copyToClipboard    (boolean shft, boolean alt) {
      RenderContext myrc = (RenderContext) getRTComponent().rc;
      if (shft == true && myrc != null)  Utils.copyToClipboard(myrc.getBase());
    }

    /**
     * Return all of the shapes in the current rendering.
     *
     *@return set of rendered shapes
     */
    public Set<Shape>      allShapes()                     { 
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Shape>();
      return myrc.geom_to_key.keySet();
    }

    /**
     * Return the rendered shapes that correspond to the specified bundles.
     *
     *@param  bundles bundles/records to match for shapes
     *
     *@return set of shapes that correspond to the specified bundles
     */
    public Set<Shape>  shapes(Set<Bundle> bundles) { 
      Set<Shape> shapes = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Bundle> it = bundles.iterator();
      while (it.hasNext()) {
        Bundle bundle = it.next();
        if (myrc.bundle_to_keys.containsKey(bundle)) {
          Iterator<String> it_key = myrc.bundle_to_keys.get(bundle).iterator();
          while (it_key.hasNext()) {
            String key = it_key.next();
            shapes.add(myrc.key_to_geom.get(key));
          }
        }
      }
      return shapes;
    }

    /**
     * Return the bundles records associated with the specified shape.  Note that the shape
     * cannot be generic and must have been returned by this component.
     *
     *@param  shape shape to lookup for records
     *
     *@return set of bundles that correspond to the shape
     */
    public Set<Bundle> shapeBundles(Shape shape)       {
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Bundle>();
      Set<Bundle> set = new HashSet<Bundle>();
      String key = myrc.geom_to_key.get(shape);
      if (key != null) {
        Set<Bundle> set_to_add = myrc.counter_context.getBundles(key);
        if (set_to_add != null) set.addAll(set_to_add);
        else System.err.println("RTBarGraphPanel.shapeBundles() - null pointer for key \"" + key + "\"");
      }
      else System.err.println("Key For \"" + shape + "\" Is Null");
      return set;
    }

    /**
     * Find the rendered shapes that overlap with the specified shape.  Note that the specified
     * shape can be generic.
     *
     *@param  shape general shape to match against rendered shapes
     *
     *@return rendered shapes that overlap with the specified shape
     */
    public Set<Shape>  overlappingShapes(Shape shape)  { 
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return new HashSet<Shape>();
      Set<Shape> shapes = new HashSet<Shape>();
      Iterator<Shape> it = myrc.geom_to_key.keySet().iterator();
      while (it.hasNext()) {
        Shape rendered_shape = it.next();
        if (Utils.genericIntersects(shape, rendered_shape)) shapes.add(rendered_shape);
      }
      return shapes;
    }

    /**
     * Return the rendered shapes that contain the specified x and y coordinate.
     *
     *@param  x x-coordinate
     *@param  y y-coordinate
     *
     *@return set of shapes that contain the x/y coordinate
     */
    public Set<Shape>  containingShapes(int x, int y)  { 
      Set<Shape> shapes = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc; if (myrc == null) return shapes;
      Iterator<Shape> it = myrc.geom_to_key.keySet().iterator();
      while (it.hasNext()) {
        Shape rendered_shape = it.next();
        if (rendered_shape.contains(x,y)) shapes.add(rendered_shape);
      }
      return shapes;
    }

    /**
     * Return the shape used to match shapes directly under the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape under mouse
     */
    public Shape getZeroOrderShape(int x, int y) { return new Rectangle2D.Double(x,0,1,getHeight()); }

    /**
     * Return the shape used to match shapes directly near the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape near mouse
     *
     */
    public Shape getFirstOrderShape(int x, int y) { return new Rectangle2D.Double(x,y,1,1); }

    /**
     * Return the shape used to match shapes directly further from the mouse.
     *
     *@param  x x-coordinate of mouse
     *@param  y y-coordinate of mouse
     *
     *@return shape further from mouse mouse
     */
    public Shape getSecondOrderShape(int x, int y) { return new Rectangle2D.Double(x,y,1,1); }

    /**
     * Create a render context with the specified render ID.  The render context will be used to 
     * create the actual visualization.  The render id ensures that unused/unneeded visualization
     * renderings will be canceled.
     *
     *@param  id render id for aborting unnecessary renders
     *
     *@return    render context based on visible dataset and GUI parameters
     */
    public RTRenderContext render(short id) {
      clearNoMappingSet();
      Bundles bs               = getRenderBundles();
      String  count_by         = getRTParent().getCountBy(),
              color_by         = getRTParent().getColorBy();

      String  mode             = mode(),
              bin_hdr          = bin(),
              x_order          = xOrder(),
              y_scale          = yScale(),
              bar_width_str    = barWidth(),
              label_option     = labelOption();

      if (bs != null && count_by != null && bin_hdr != null && count_by != null) {
        RenderContext myrc = new RenderContext(id, bs, count_by, color_by, mode, bin_hdr, x_order, y_scale, bar_width_str, label_option, getWidth(), getHeight());
        return myrc;
      } else return null;
    }

    /**
     * Class to perform the actual rendering of the view.  This class uses several additional inner classes
     * to map the axes and to perform statistical operations on the data.
     */
    public class RenderContext extends RTRenderContext {
      /**
       * Data set to render
       */
      Bundles bs; 
      /**
       * Width (in pixels) of the rendering
       */
      int     w, 
      /**
       * Height (in pixels) of the rendering
       */
              h; 
      /**
       * Field used to count the bars by (or perform stats on)
       */
      String  count_by, 
      /**
       * Field used to color the entities
       */
              color_by, 
      /**
       * Mode to use - boxplot, count/sum
       */
              mode,
      /**
       * Entity to use for the scatter plot
       */
              bin_hdr, 
      /**
       * Order for x
       */
              x_order, 

      /**
       * Scale to use for the y-axis
       */
              y_scale;

      /**
       * Bar width 
       */
      int     bar_width;

      /**
       * Labeling option
       */
      String  label_option;

      /**
       * Counter context for each point in the scatter plot
       */
      BundlesCounterContext            counter_context;

      /**
       * X inset for rendering
       */
      int                              x_ins, 

      /**
       * Y inset for rendering
       */
                                       y_ins, 
      /**
       * XY Graph width in pixels
       */
                                       graph_w, 
      /**
       * XY Graph height in pixels
       */
                                       graph_h;

      /**
       * Maps world coordinates to screen coordinates
       */
      Mapper                           xmap;

      /**
       * Special key maker if the x axis is timebased
       */
      KeyMaker                         time_km = null;

      /**
       * map from the geometry to the associated key
       */
      Map<Shape,String>  geom_to_key = new HashMap<Shape,String>();

      /**
       * Map from the panel key to the asssociated geometry
       */
      Map<String,Shape>  key_to_geom = new HashMap<String,Shape>();

      /**
       *
       */
      Map<Bundle,Set<String>> bundle_to_keys = new HashMap<Bundle,Set<String>>();

      /**
       * Construct the render context with the specified dataset and GUI configurations.  Use the render ID to 
       * ensure that out-of-date renderings are canceled as soon as possible.
       *
       *@param id                render id to abort unnecessary renderings
       *@param bs                dataset to render
       *@param count_by          Field used to count the entities by (for width of icon in xy grid)
       *@param color_by          Field used to color the entities
       *@param mode              Mode of application
       *@param bin_hdr           Entity to use for the x-axis
       *@param x_order           Order of the x axis
       *@param y_scale           Scale to use for the y-axis
       *@param bar_width_str     Bar width as a string
       *@param label_option      Labeling option
       *@param w                 Width (in pixels) of the rendering
       *@param h                 Height (in pixels) of the rendering
       */

      // RenderContext myrc = new RenderContext(id, bs, count_by, color_by, mode, bin_hdr, x_order, y_scale, bar_width, label_option, getWidth(), getHeight());
      
      public               RenderContext(short   id, 
                                         Bundles bs, 
                                         String  count_by, 
                                         String  color_by, 
                                         String  mode, 
                                         String  bin_hdr,
                                         String  x_order, 
                                         String  y_scale, 
                                         String  bar_width_str, 
                                         String  label_option, 
                                         int     w, 
                                         int     h) {
        render_id = id; this.bs = bs; this.w = w; this.h = h; this.count_by = count_by; this.color_by = color_by; this.mode = mode; 
        this.bin_hdr = bin_hdr; this.x_order = x_order;
        this.bar_width = barWidthValue(bar_width_str);

        this.y_scale = y_scale; this.label_option = label_option;

        BundlesG globals = getRTParent().getRootBundles().getGlobals();

        // Initialize the counter context and the mapper
        counter_context = new BundlesCounterContext(bs, count_by, color_by);
        xmap = new Mapper();

        // Figure out if this mode aggregates records on a per time-frame basis
        boolean                             y_is_categorical = false;
        // KeyMaker                            time_framer_km   = null;

        // Determine the type (numerical/nominal versus categorical) of data
        if      (count_by.equals(KeyMaker.RECORD_COUNT_STR))     y_is_categorical = false;
        else if (globals.isScalar(globals.fieldIndex(count_by))) y_is_categorical = false;
        else                                                     y_is_categorical = true;

        // Go through the tablets
        Iterator<Tablet> it_t = bs.tabletIterator();
        while (it_t.hasNext() && currentRenderID() == getRenderID()) {
          Tablet tablet = it_t.next(); KeyMaker ekm = null, ykm = null;

          // Can the tablet contribute to "count_by"
          // boolean tablet_can_count = count_by.equals(KeyMaker.RECORD_COUNT_STR) || KeyMaker.tabletCompletesBlank(tablet, count_by);

          // Check to see what the bundle can provide
          if      (KeyMaker.tabletCompletesBlank(tablet, bin_hdr))  ekm  = new KeyMaker(tablet, bin_hdr);
          boolean bundle_y_counting = false;
          if      (count_by.equals(KeyMaker.RECORD_COUNT_STR))      { ykm  = null; bundle_y_counting = true; }
          else if (KeyMaker.tabletCompletesBlank(tablet, count_by)) { ykm  = new KeyMaker(tablet, count_by); }

          // If it provides something, go through the bundles
          if (ekm != null && (bundle_y_counting || ykm != null) && (tablet.hasTimeStamps())) {

            // Make the periodic time functions stretch the entire period
            if (ekm.isTimeBased() && ekm.linearTime() == false) addPeriodicSpacers(ekm);

            // Go through the bundles
            Iterator<Bundle> it_b = tablet.bundleIterator(); if (ekm.isTimeBased()) time_km = ekm;
            while (it_b.hasNext() && currentRenderID() == getRenderID()) {
              Bundle bundle = it_b.next(); bundle_to_keys.put(bundle, new HashSet<String>());
              String strs[] = ekm.stringKeys(bundle); int strs_i[] = null; if (ekm.isTimeBased() == false) strs_i  = ekm.intKeys(bundle);

              // Create the y-axis values
              int ys[] = null; // String ystrs[] = null; 
              if (bundle_y_counting) { 
                ys = new int[1]; ys[0] = 1; 
              } else                 { 
                if (y_is_categorical) { } else { ys = ykm.intKeys(bundle);  }
                // ystrs = ykm.stringKeys(bundle); 
              }

              for (int i=0;i<strs.length;i++) {
                  String key = strs[i];

                  // Store of the bundle to keys information
                  bundle_to_keys.get(bundle).add(key);

                  // Figure out the ordering for the x-map
                  long   key_l;
                  if (ekm.isTimeBased()) key_l = ekm.timeStampKey(bundle); else key_l = (strs_i[i] & 0x00ffffffffL);
                  xmap.add(key, key_l);

                  // Even though the counter context isn't used for boxplots, we need it
                  // to correlate shapes to bundles...
                  counter_context.count(bundle, key);

                  // Track bundles
                  if (bin_map.containsKey(key) == false) bin_map.put(key, new HashSet<Bundle>());
                  bin_map.get(key).add(bundle);
              }
            }
          } else {
            Iterator<Bundle> it_b = tablet.bundleIterator();
            while (it_b.hasNext()) addToNoMappingSet(it_b.next());
          }
         }

        // Fill in extra slots in the mapper for linear time areas that have no input
        if (time_km != null && time_km.linearTime()) {
          long ts = bs.ts0(); long inc = bs.ts1() - ts;
          if        (bin_hdr.equals(KeyMaker.BY_YEAR_STR))                    { inc = 1000L * 60L * 60L * 24L * 364L; // Shortest Year
          } else if (bin_hdr.equals(KeyMaker.BY_YEAR_MONTH_STR))              { inc = 1000L * 60L * 60L * 24L * 28L;  // Shortest Month
          } else if (bin_hdr.equals(KeyMaker.BY_YEAR_MONTH_DAY_STR))          { inc = 1000L * 60L * 60L * 24L;        // One Day
          } else if (bin_hdr.equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_STR))     { inc = 1000L * 60L * 60L;              // One Hour
          } else if (bin_hdr.equals(KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_STR)) { inc = 1000L * 60L;                    // One Minute
          } else if (bin_hdr.equals(KeyMaker.BY_AUTOTIME_STR))                { // abc
          } else System.err.println("Unexpected Time Binning \"" + bin_hdr + "\" - possible misrendering...");

          // Add all timeframes ... this is a problem if the time range is very great and the increment is small
          while (ts < bs.ts1()) {
            String ts_str = time_km.toString(ts);
            xmap.add(ts_str, ts);
            ts += inc;
          }
        }

      }

      /**
       *
       */
      protected void addPeriodicSpacers(KeyMaker ekm)
            {
              xmap.add("Min", ekm.minPeriodicValue());
              xmap.add("Max", ekm.maxPeriodicValue());

              // Fill in the middles so that things are spaced correctly... complete hack but don't want to mess with the mapper since it's used elsewhere correctly
              long diff = ekm.maxPeriodicValue() - ekm.minPeriodicValue();
              if (diff <= 1000L) {
                for (long l=ekm.minPeriodicValue();l<ekm.maxPeriodicValue()-1;l++) {
                  String s = "" + l; while (s.length() < 3) s = "0" + s;
                  xmap.add(s, l);
                }
              } else if (diff == 82800000L)   { // Hours
                long inc = 60L * 60L * 1000L; long l = inc;
                for (int i=1;i<23;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
              } else if (diff == 59000L)      { // Seconds
                long inc = 1000L; long l = inc;
                for (int i=1;i<59;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
              } else if (diff == 3540000L)    { // Minutes
                long inc = 60L*1000L; long l = inc;
                for (int i=1;i<59;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
              } else if (diff == 3599000L)    { // Minutes & Seconds
                System.err.println("RTBoxPlot - Not All Data May Render -- make window large"); // Indicate that something is out-of-bounds
                long inc = 1000L; long l = inc;
                for (int i=1;i<3599;i++,l+=inc) {
                  int min = i/60, sec = i%60;
                  String min_str; if (min < 10) min_str = "0" + min; else min_str = "" + min;
                  String sec_str; if (sec < 10) sec_str = "0" + sec; else sec_str = "" + sec;
                  xmap.add(min_str + ":" + sec_str, l);
                }
              } else if (diff == 86340000L)   { // Hour & Minutes
                System.err.println("RTBoxPlot - Not All Data May Render -- make window large"); // Indicate that something is out-of-bounds
                long inc = 60L*1000L; long l = inc;
                for (int i=1;i<24*60;i++,l+=inc) {
                  int hor = i/60, min = i%60;
                  String hor_str; if (hor < 10) hor_str = "0" + hor; else hor_str = "" + hor;
                  String min_str; if (min < 10) min_str = "0" + min; else min_str = "" + min;
                  xmap.add(hor_str + ":" + min_str, l);
                }
              } else {
                System.err.println("Do Not Understand Periodic Diff = " + diff);
              }
            }

      /**
       * Minimum y screen coordinate
       */
      double ys_min,
      /**
       * Maximum y screen coordinate
       */
             ys_max;

      /**
       * Map for from the bin to the bundle set
       */
      Map<String,Set<Bundle>> bin_map = new HashMap<String,Set<Bundle>>();

      /**
       * Class to map the world coordinates for entities to screen coordinates.
       */
      class Mapper {
        /**
         *
         */
        long xw_min, 
        /**
         *
         */
             xw_max;
        /**
         * Bin to the world coordinate
         */
        Map<String,Long>   map   = new HashMap<String,Long>();
        /**
         * Reverse mapping from the world coordinate back to the bin
         */
        Map<Long,String>   rmap  = new HashMap<Long,String>();
        /**
         * World x coordinates for current rendering
         */
        List<Long>         xws   = new ArrayList<Long>();
        /**
         * Inset of the graph
         */
        int                ins,
        /**
         * Length of the graph
         */
                           len,
        /**
         * Recommended bar width
         */
                           bar_w = bar_width;
        /**
         *
         */
        public void add(String entity, long world_x) {
          System.err.println("mapper.add(" + entity + "," + world_x + ")"); // DEBUG
          // Keep track of all of the entities
          if (map.containsKey(entity) == false) {
            if (map.size() == 0) xw_min = xw_max = world_x; // First time - set the min and max world coordinate x

            // Set the forward and reverse map...  add to the set... flag that the data is no longer sorted
            map.put(entity,world_x); rmap.put(world_x,entity); xws.add(world_x); xws_sorted = false;

            if (xw_min > world_x) xw_min = world_x; // keep track of the min world coordinate x
            if (xw_max < world_x) xw_max = world_x; // keep track of the max world coordinate x

          } else if (map.get(entity) != world_x) { 
            System.err.println("Mismatch between stored mapping and added mapping... \"" + entity + "\" => \"" + world_x + 
                               "\" ... found \"" + map.get(entity) + "\""); 
          }
        }


        /**
         * Calculate the specific mapping for a value.  Only used by the linear-time-based
         * labeling system.
         */
        public int  calculateMapping(long l) {
          if        (xws.size() == 1) {
            return ins;
          } else if (xws.size() == 2) { 
            if (xws.get(0) == l) return ins; else return ins + len/2; 
          } else                      {
            return (int) (1 + ins + (len * (l - xw_min))/(xw_max - xw_min));
          }
        }

        /**
         * Return the recommended bar width.
         */
        public int barWidth() { return bar_w; }

        /**
         * Flag to indicate if xws is already sorted
         */
        boolean xws_sorted = false;

        /**
         *
         */
        public void calculateMapping(String x_scale, int ins, int len, KeyMaker time_km) { 
          if (xws_sorted == false) { Collections.sort(xws); xws_sorted = true; } this.ins = ins; this.len = len;
          if        (xws.size() == 1) {
            conversion.put(xws.get(0), ins + len/2);
            bar_w = len; if (bar_w < 1) bar_w = 1;
          } else if (xws.size() == 2) {
            conversion.put(xws.get(0), ins + len/4);
            conversion.put(xws.get(1), ins + 3*len/4);
            bar_w = len/3; if (bar_w < 1) bar_w = 1;
          } else {
            int divisor = xws.size()-1; if (divisor < 1) divisor = 1;
            // Equal spacing - Just whatever ordering if made by the algorithm
            for (int i=0;i<xws.size();i++) { conversion.put(xws.get(i), (int) (1 + ins + (i*len)/(divisor))); }
          } // else throw new RuntimeException("Scale \"" + x_scale + "\" Not Understood");
        }

        /**
         * Return the number of mapped entities.
         *
         *@return number of entities
         */
        public int size() { return xws.size(); }

        /**
         * Conversion from world coordinates to screen coordinates
         */
        Map<Long,Integer> conversion = new HashMap<Long,Integer>();

        /**
         * Calculate the x-position of an entity in screen space.
         *
         *@param  entity entity to position
         *
         *@return screen x-coordinate
         */
        public int  toScreen(String entity) { 
          return conversion.get(map.get(entity));
        }

        /**
         * Return the earliest entity on the x-axis.  Useful for first labels...
         *
         *@return first entity
         */
        public String firstEntity() { return rmap.get(xws.get(0)); }

        /**
         * Return the last entity on the x-axis.  Useful for last labels...
         *
         *@return last entity
         */
        public String lastEntity() { return rmap.get(xws.get(xws.size()-1)); }
      }

      /**
       * Return the width of the rendering in pixels.
       *
       *@return width in pixels
       */
      public int           getRCWidth()  { return w; }

      /**
       * Return the height of the rendering in pixels
       *
       *@return height in pixels
       */
      public int           getRCHeight() { return h; }

      /**
       * Colors for labels in time
       */
      private final Color            COLOR_YEAR   = RTColorManager.getColor("label", "year"),
                                     COLOR_MONTH  = RTColorManager.getColor("label", "month"),
                                     COLOR_DAY    = RTColorManager.getColor("label", "day"),
                                     COLOR_HOUR   = RTColorManager.getColor("label", "hour"),
                                     COLOR_MINUTE = RTColorManager.getColor("label", "minute");

      /** 
       * Draw markers for time-based graphs.
       *
       *@param g2d graphics primitive
       */
      private void drawTimeGrid(Graphics2D g2d) {
        SimpleDateFormat major_sdf = null, minor_sdf = null; Color major_color = null, minor_color = null;
        long time_diff = bs.ts1() - bs.ts0();
        if        (time_diff > 3 *Utils.YEARS)  { major_sdf = new SimpleDateFormat("yyyy"); major_color = COLOR_YEAR;
                                                  minor_sdf = new SimpleDateFormat("MMM");  minor_color = COLOR_MONTH;
        } else if (time_diff > 1 *Utils.YEARS)  { major_sdf = new SimpleDateFormat("yyyy"); major_color = COLOR_YEAR;
                                                  minor_sdf = new SimpleDateFormat("MMM");  minor_color = COLOR_MONTH;
        } else if (time_diff > 6 *Utils.MONTHS) { major_sdf = new SimpleDateFormat("yyyy"); major_color = COLOR_YEAR;
                                                  minor_sdf = new SimpleDateFormat("MMM");  minor_color = COLOR_MONTH;
        } else if (time_diff > 1 *Utils.MONTHS) { major_sdf = new SimpleDateFormat("MMM");  major_color = COLOR_MONTH;
                                                  minor_sdf = new SimpleDateFormat("dd");   minor_color = COLOR_DAY;
        } else if (time_diff > 12*Utils.DAYS)   { major_sdf = new SimpleDateFormat("MMM");  major_color = COLOR_MONTH;
                                                  minor_sdf = new SimpleDateFormat("dd");   minor_color = COLOR_DAY;
        } else if (time_diff > 3 *Utils.DAYS)   { major_sdf = new SimpleDateFormat("dd");   major_color = COLOR_DAY;
                                                  minor_sdf = new SimpleDateFormat("HH");   minor_color = COLOR_HOUR;
        } else if (time_diff > 1 *Utils.DAYS)   { major_sdf = new SimpleDateFormat("dd");   major_color = COLOR_DAY;
                                                  minor_sdf = new SimpleDateFormat("HH");   minor_color = COLOR_HOUR;
        } else                                  { major_sdf = new SimpleDateFormat("HH");   major_color = COLOR_HOUR;
                                                  minor_sdf = new SimpleDateFormat("MM");   minor_color = COLOR_MINUTE;
        }
        major_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        minor_sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        String last_major = major_sdf.format(new Date(bs.ts0())),
               last_minor = minor_sdf.format(new Date(bs.ts0()));
        long inc = (bs.ts1() - bs.ts0())/graph_w;
        Area taken = new Area(); Rectangle2D rect; int txt_w = 0, txt_h = 0, spc = 2;
        for (long ts=bs.ts0()+1;ts<=bs.ts1();ts+=inc) {
          String major = major_sdf.format(new Date(ts));
          int    sx    = xmap.calculateMapping(ts);
          if        (major.equals(last_major) == false) {
            txt_w = Utils.txtW(g2d, major); txt_h = Utils.txtH(g2d, major);
            rect = new Rectangle2D.Float(sx - txt_w/2 - spc, 0, txt_w + 2*spc, txt_h);
            if (taken.intersects(rect) == false) { 
              g2d.setColor(major_color);
              g2d.drawString(major, sx - txt_w/2, txt_h); 
              last_major = major; 
              taken.add(new Area(rect));
            }
          } 
        }
        for (long ts=bs.ts0()+1;ts<=bs.ts1();ts+=inc) {
          String minor = minor_sdf.format(new Date(ts));
          int    sx    = xmap.calculateMapping(ts);
          if (minor.equals(last_minor) == false) {
            txt_w = Utils.txtW(g2d, minor); txt_h = Utils.txtH(g2d, minor);
            rect = new Rectangle2D.Float(sx - txt_w/2 - spc, txt_h+1, txt_w + 2*spc, txt_h);
            if (taken.intersects(rect) == false) { 
              g2d.setColor(minor_color);
              g2d.drawString(minor, sx - txt_w/2, (int) (txt_h*2)); 
              last_minor = minor; 
              taken.add(new Area(rect));
            }
          }
        }
      }

      /**
       * Copy of the rendered image
       */
      BufferedImage base_bi = null;

      /**
       * Render the previously calculated values to the actual image buffer.  Save a copy in case
       * it is requested again.
       *
       *@return rendered image
       */
      public BufferedImage getBase()     { 
        if (base_bi == null) {
         Graphics2D g2d = null;
         try {
          base_bi = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) base_bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(base_bi, g2d);
          int txt_h = Utils.txtH(g2d, "0"), LOG_1_H = 10; // String max_log_str = "notset";

          // Figure out the geometry
          x_ins = y_ins = 16;
          if (time_km != null && time_km.linearTime()) { y_ins = 2*Utils.txtH(g2d,"1") + 5; } // More space for the labels...
          graph_w = w - 2*x_ins;
          graph_h = h - 2*y_ins;
          
          // Figure out the scale and mappings
          xmap.calculateMapping(x_order, x_ins,      graph_w,        time_km);        // Initial cut of the mapping
          int w2 = xmap.barWidth()/2;
          xmap.calculateMapping(x_order, x_ins + w2, graph_w - 2*w2, time_km); // Re-adjust to consider the bar width

          // This is here because the xmap may not cover every possible integer value in the range of the data
          // - However, this prevents the display from fully using the display area -- i.e., bar widths are one
          // if (x_scale.equals(LINEAR_SCALE_STR) && time_km == null) w2 = 1;

          // Draw labels, axes, etc...
          Iterator<String> it;
          int max_label_w = 0;
          if (label_option.equals(LABEL_STANDARD_STR)) {
            it = counter_context.binIterator();
            while (it.hasNext()) {
              String str   = it.next();
              int    str_w = Utils.txtW(g2d, str);
              if (str_w > max_label_w) max_label_w = str_w;
            }
            graph_h = h - (y_ins + max_label_w + 2);
          }
          
          // - Axes
          g2d.setColor(RTColorManager.getColor("axis", "major"));
          g2d.drawLine(x_ins, y_ins + graph_h, x_ins + graph_w, y_ins + graph_h);
          g2d.drawLine(x_ins, y_ins,           x_ins,           y_ins + graph_h);
          Utils.drawRotatedString(g2d, "" + ys_min,  x_ins, y_ins + graph_h);
          Utils.drawRotatedString(g2d, "" + ys_max,  x_ins, y_ins + Utils.txtW(g2d, "" + ys_max));

          // For the log scale, draw lines for context...
          if (y_scale.equals(LOG_SCALE_STR)) {
            g2d.setColor(RTColorManager.getColor("axis", "minor"));
            long log = 1;
            while (log < ys_max) {
              int sy = (int) (y_ins + graph_h - LOG_1_H - ((graph_h - LOG_1_H)*Math.log(log))/Math.log(counter_context.totalMaximum()));
              g2d.drawLine(x_ins+1,sy,x_ins+graph_w-1,sy);
              log *= 10;
            }
          }

          // Draw the y-axis string, color it different for log scale
          String y_str = count_by;
          if (y_scale.equals(LOG_SCALE_STR)) { g2d.setColor(RTColorManager.getColor("label", "log")); y_str += " (Log)";
          } else                             { g2d.setColor(RTColorManager.getColor("label", "linear")); }
          Utils.drawRotatedString(g2d, y_str,  x_ins, y_ins + graph_h/2 + Utils.txtW(g2d, y_str)/2);

          // If it's in linear time mode, draw context for the timeframes
          if (time_km != null && time_km.linearTime() && x_order.equals(ORDER_DEFAULT_STR)) { drawTimeGrid(g2d); }

          // Draw the bars
          Area label_area = new Area();

            it = counter_context.binIterator();
            while (it.hasNext()) {
              String key   = it.next();
              int    sx    = xmap.toScreen(key); // , sy;

              /* sy = */ renderStackedBar(g2d, key, sx, w2, LOG_1_H);
              
              // Labels...
              if (label_option.equals(LABEL_STANDARD_STR)) { 
                  Rectangle2D label_rect = new Rectangle2D.Double(sx - txt_h/2 - 1, y_ins + graph_h, txt_h + 2, h - y_ins - graph_h);
                  if (label_area.intersects(label_rect) == false) {
                    g2d.setColor(RTColorManager.getColor("label", "major"));
                    Utils.drawRotatedString(g2d, key, sx + txt_h/2, h - max_label_w + Utils.txtW(g2d, key) + 3); 
                    label_area.add(new Area(label_rect));
                  }
              }
            }

          // If not drawing labels, just draw the first and last strings
          if (label_option.equals(LABEL_NONE_STR)) {
            g2d.setColor(RTColorManager.getColor("label", "major"));
            g2d.drawString(xmap.firstEntity(), x_ins,                                               h - 2);
            g2d.drawString(xmap.lastEntity(),  x_ins + graph_w - Utils.txtW(g2d,xmap.lastEntity()), h - 2);
            String str = bin_hdr;
            if (x_order.startsWith(ORDER_DEFAULT_STR)) { g2d.setColor(RTColorManager.getColor("label", "equal")); str += " (Equal)"; } else g2d.setColor(RTColorManager.getColor("label", "linear"));
            g2d.drawString(str, x_ins + graph_w/2 - Utils.txtW(g2d,str)/2, h - 2);
          }
         } finally { if (g2d != null) g2d.dispose(); }
        }
        return base_bi;
      }

      /**
       *
       */
      protected int renderStackedBar(Graphics2D g2d, String key, int sx, int w2, int LOG_1_H) {
              // Calculate the bar height
              int    bar_h;
              if (y_scale.equals(LINEAR_SCALE_STR)) {
                bar_h = (int) (counter_context.totalNormalized(key)*graph_h);
                if (counter_context.totalNormalized(key) > 0.0 && bar_h < 1) bar_h = 1;
              } else {
                if        (counter_context.total(key) > 1.0) {
                  bar_h = LOG_1_H + (int) (((graph_h - LOG_1_H)*Math.log(counter_context.total(key)))/Math.log(counter_context.totalMaximum()));
                } else if (counter_context.total(key) > 0.0) {
                  bar_h = LOG_1_H;
                } else {
                  bar_h = 0;
                }
              }
              int sy = y_ins + graph_h - bar_h;

              // Update the geometry to bin mapping
              Rectangle2D rect = new Rectangle2D.Double(sx - w2, sy, 2*w2+1, bar_h);
              geom_to_key.put(rect, key);
              key_to_geom.put(key, rect);

              // Determine if the bar should be in color or not
              if (color_by != null) {
                int y_inc = y_ins + graph_h;
                List<String> cbins = counter_context.getColorBinsSortedByCount();
                for (int i=cbins.size()-1;i>=0;i--) {
                  String cbin   = cbins.get(i);
                  double ctotal = counter_context.total(key,cbin);
                  if (ctotal > 0L) {
                    int sub_h = (int) ((ctotal * bar_h)/counter_context.binColorTotal(key));
                    if (sub_h > 0) {
                      g2d.setColor(RTColorManager.getColor(cbin));
                      g2d.fillRect(sx - w2, y_inc - sub_h, 2*w2+1, sub_h);
                      y_inc -= sub_h;
                    }
                  }  
                }
                if (y_inc != sy) {
                  g2d.setColor(RTColorManager.getColor("set", "multi"));
                  g2d.fillRect(sx - w2, sy, 2*w2+1, y_inc - sy);
                }
              } else {
                g2d.setColor(counter_context.binColor(key));
                g2d.fillRect(sx - w2, sy, 2*w2+1, bar_h);
              }
              
              return sy;
      }
    }
  }
}

