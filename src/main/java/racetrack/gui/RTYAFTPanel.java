/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.gui;

import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BundlesDT;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;
import racetrack.util.Utils;
import racetrack.visualization.RTColorManager;

/**
 *
 *@author  D. Trimm
 *@version 0.9
 */
public class RTYAFTPanel extends RTPanel {
  /**
   * 
   */
  private static final long serialVersionUID = 2261155646337112043L;

  /**
   * Mapper initial string ... null if not ever set. ... null if not ever set.
   */
  String mapper_init_str = null;

  /**
   * Dropdown menu for overriding the global count-by method
   */
  JComboBox<String> count_cb,

  /**
   * Dropdown menu for how time will be mapped
   */
                    mapper_cb;

  /**
   * Checkbox for logarithmic scaling
   */
  JCheckBoxMenuItem log_cbmi, 

  /**
   * Checkbox to fix the timeframe
   */
                    fix_cbmi;

  //
  // This section all needs to work together... any changes on anything need to ensure integrity across all of this
  // vvvv
  //

  /**
   * Constants for timeframes in milliseconds.
   */
  public final static long SECONDS     = 1000L,
                           SECONDSx10  = 10L  * SECONDS,  // Delta is 10x
                           MINUTES     = 60L  * SECONDS,  // Delta is  6x
                           MINUTESx10  = 10L  * MINUTES,  // Delta is 10x
                           HOURS       = 60L  * MINUTES,  // Delta is  6x
                           HOURSx4     =  4L  * HOURS,    // Delta is  4x
                           DAYS        = 24L  * HOURS,    // Delta is  6x
                           MONTHS      = 30L  * DAYS,     // Delta is 30x // Approximately...
                           QUARTERS   =  91L  * DAYS,     // Delta is  3x // Approximately...
                           YEARS       = 365L * DAYS;     // Delta is  4x // Approximately...

  public final static long timeframe_constants[] = { SECONDS,     SECONDSx10,
                                                     MINUTES,     MINUTESx10,
                                                     HOURS,       HOURSx4,
                                                     DAYS,
                                                     MONTHS,      QUARTERS,
                                                     YEARS };

  // Minimum bar width
  public final static double min_bar_w =  3.0;

  /**
   * Return the minimum bar width.
   */
  public static double minBarWidth() { return min_bar_w; }

  /**
   * Adjusts the minimum bar width.  Prototyped but removed -- didn't really feel
   * like the right interactions.
   */
  /*
  private void minBarWidth(int new_min_bar_w) {
    if (new_min_bar_w < 3)   new_min_bar_w = 3;
    if (new_min_bar_w > 60)  new_min_bar_w = 60;
    min_bar_w = new_min_bar_w;
    max_bar_w = new_min_bar_w + 93.0;
    getRTComponent().render();
  }
  */

  // Maximum bar width
  public final static double max_bar_w = 96.0;

  /**
   * Return the maximum bar width.
   */
  public static double maxBarWidth() { return max_bar_w; }

  /**
   * Calculate the timeframe constant based on the the time delta to display and the graph width.
   * Start with the finest granularity and then move up...
   *
   *@param time_delta difference between min and max time
   *@param graph_w    graph width
   *
   *@return constant for the timeframe in milliseconds (constants above)
   */
  public static long calculateTimeBin(long time_delta, int graph_w) {
    for (int i=0;i<timeframe_constants.length;i++) {
      double bar_w = ((double) graph_w) / (((double) time_delta) / timeframe_constants[i]);
      if (bar_w >= minBarWidth()) return timeframe_constants[i];
    }
    return YEARS;
  }

  /**
   * Calculate the bar width -- mirrors the above...
   */
  public static int calculateBarWidth(long time_delta, int graph_w) {
    for (int i=0;i<timeframe_constants.length;i++) {
      double bar_w = ((double) graph_w) / (((double) time_delta) / timeframe_constants[i]);
      if (bar_w >= minBarWidth()) return (int) bar_w;
    }
    return (int) minBarWidth();
  }

  /**
   * Return the keymaker string for the specified (linear) timeframe constant.
   *
   *@param timeframe_constant timeframe constant (constants above)
   *
   *@return keymaker blank string
   */
  public static String keyMakerBlank(long timeframe_constant) {
    if      (timeframe_constant == SECONDS)    return KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_SEC_STR;
    else if (timeframe_constant == SECONDSx10) return KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_SECx10_STR;
    else if (timeframe_constant == MINUTES)    return KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MIN_STR;
    else if (timeframe_constant == MINUTESx10) return KeyMaker.BY_YEAR_MONTH_DAY_HOUR_MINx10_STR;
    else if (timeframe_constant == HOURS)      return KeyMaker.BY_YEAR_MONTH_DAY_HOUR_STR;
    else if (timeframe_constant == HOURSx4)    return KeyMaker.BY_YEAR_MONTH_DAY_HOURx4_STR;
    else if (timeframe_constant == DAYS)       return KeyMaker.BY_YEAR_MONTH_DAY_STR;
    else if (timeframe_constant == MONTHS)     return KeyMaker.BY_YEAR_MONTH_STR;
    else if (timeframe_constant == QUARTERS)   return KeyMaker.BY_YEAR_QUARTER_STR;
    else if (timeframe_constant == YEARS)      return KeyMaker.BY_YEAR_STR;
    else                                       return KeyMaker.BY_YEAR_STR;
  }

  /**
   * Fills in the xmap (to account for horizontal spacing within the visualization).  As a side
   * effect, it also calculates the number of lines need for labels at the top and fills
   * in a related data structure.
   */
  public static int fillXMapForLinear(XMap xmap, long timeframe_constant, Bundles bs, long my_ts0, long my_ts1) {
    // KeyMaker km        = new KeyMaker(getRTParent().getVisibleBundles().tabletIterator().next(), keyMakerBlank(timeframe_constant));
    KeyMaker km        = new KeyMaker(bs.tabletIterator().next(), keyMakerBlank(timeframe_constant));
    Calendar gmtcal    = Calendar.getInstance(TimeZone.getTimeZone("GMT")); gmtcal.setTimeInMillis(my_ts0);
    int      top_lines = 1;

    // Calendar field to increment
    int inc_field; int inc_amount = 1;
    if      (timeframe_constant == SECONDS)     { inc_field = Calendar.SECOND;                         top_lines = 2; } // year-month-day hour:minute | second
    else if (timeframe_constant == SECONDSx10)  { inc_field = Calendar.SECOND;        inc_amount = 10; top_lines = 2; } // year-month-day hour:minute | second
    else if (timeframe_constant == MINUTES)     { inc_field = Calendar.MINUTE;                         top_lines = 2; } // year-month-day hour        | minute
    else if (timeframe_constant == MINUTESx10)  { inc_field = Calendar.MINUTE;        inc_amount = 10; top_lines = 2; } // year-month-day hour        | minute
    else if (timeframe_constant == HOURS)       { inc_field = Calendar.HOUR;                           top_lines = 2; } // year-month-day             | hour
    else if (timeframe_constant == HOURSx4)     { inc_field = Calendar.HOUR;          inc_amount = 4;  top_lines = 2; } // year-month-day             | hour
    else if (timeframe_constant == DAYS)        { inc_field = Calendar.DAY_OF_MONTH;                   top_lines = 2; } // year-month                 | day
    else if (timeframe_constant == MONTHS)      { inc_field = Calendar.MONTH;                          top_lines = 2; } // year                       | month
    else if (timeframe_constant == QUARTERS)    { inc_field = Calendar.MONTH;         inc_amount = 3;  top_lines = 2; } // year                       | quarter
    else if (timeframe_constant == YEARS)       { inc_field = Calendar.YEAR;                           top_lines = 1; } // year
    else                                        { inc_field = Calendar.YEAR;                           top_lines = 1; } // year

    // Add them to the xmap... but cap @ 4000...
    long   km_l = km.timeStampKey(gmtcal.getTimeInMillis());
    String str  = km.toString(gmtcal.getTimeInMillis());
    xmap.add(str, km_l);

    int iters = 0; while (iters < 4000 && gmtcal.getTimeInMillis() <= my_ts1) {
      gmtcal.add(inc_field, inc_amount); iters++;
      km_l = km.timeStampKey(gmtcal.getTimeInMillis());
      str  = km.toString(gmtcal.getTimeInMillis());
      xmap.add(str, km_l);
    }

    return top_lines;
  }

  //
  // ^^^^
  // This section all needs to work together... any changes on anything need to ensure integrity across all of this
  //

  /**
   * Create a default instance with the specified GUI parent.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt       application reference
   */
  public RTYAFTPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt) { this(win_type,win_pos,win_uniq,rt,true); }

  /**
   * Create a default instance with the specified GUI parent.  This pre-sets the mapper string...
   *
   *@param win_type         type of window this panel is embedded into
   *@param win_pos          position of panel within window
   *@param win_uniq         UUID for parent window
   *@param rt               application reference
   *@param mapper_init_str  mapper initial string
   */
  public RTYAFTPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt, String mapper_init_str) { 
    this(win_type,win_pos,win_uniq,rt,false); 

    // Set the initial mapping if valid
    this.mapper_init_str = mapper_init_str;
    if (mapper_init_str != null) mapper_cb.setSelectedItem(mapper_init_str);
  }

  /**
   * Create an instance with the specified GUI parent and include
   * the configuration panel if specified.
   *
   *@param win_type type of window this panel is embedded into
   *@param win_pos  position of panel within window
   *@param win_uniq UUID for parent window
   *@param rt            GUI parent
   *@param include_panel true to include the panel
   */
  public RTYAFTPanel(RTPanelFrame.Type win_type, int win_pos, String win_uniq, RT rt, boolean include_panel) { 
    super(win_type, win_pos, win_uniq, rt); JMenuItem mi;

    // Center component for visualization
    add("Center", component = new RTYAFTComponent());

    // Bottom panel for configuration
    JPanel panel = new JPanel(new FlowLayout());
    panel.add(new JLabel("Count"));  panel.add(count_cb  = new JComboBox<String>());
    panel.add(new JLabel("Map"));    panel.add(mapper_cb = new JComboBox<String>(filteredKeyMapperStrings()));

    // Render Options
    getRTPopupMenu().add(log_cbmi     = new JCheckBoxMenuItem("Log Scale"));
    getRTPopupMenu().add(fix_cbmi     = new JCheckBoxMenuItem("Fix Timeframe"));

    // Listeners
    // defaultListener(type_cb);
    defaultListener(count_cb);
    defaultListener(mapper_cb);

    defaultListener(log_cbmi);
    defaultListener(fix_cbmi);

    if (include_panel) add("South", panel);

    updateBys();
  }

  /**
   * Filtered key mapper strings... just the ones that should work in this component...
   */
  private String[] filteredKeyMapperStrings() {
    // Add very specific settings
    List<String> list = new ArrayList<String>();

    list.add(KeyMaker.BY_AUTOTIME_STR);      // Only linear time available...

    list.add(KeyMaker.BY_QUARTER_STR);       // All the versions of periodic mappings...
    list.add(KeyMaker.BY_MONTH_STR);
    list.add(KeyMaker.BY_MONTH_DAY_STR);
    list.add(KeyMaker.BY_DAYOFWEEK_STR);
    list.add(KeyMaker.BY_DAYOFWEEK_HOUR_STR);
    list.add(KeyMaker.BY_HOUR_STR);
    list.add(KeyMaker.BY_HOUR_MINUTE_STR);
    list.add(KeyMaker.BY_MINUTE_STR);
    list.add(KeyMaker.BY_MINUTE_SECOND_STR);
    list.add(KeyMaker.BY_SECOND_STR);
    list.add(KeyMaker.BY_MILLIS_STR);

    // Cast it back as an array
    String strs[] = new String[list.size()]; for (int i=0;i<strs.length;i++) strs[i] = list.get(i); return strs;
  }

  /**
   * Return an alphanumeric prefix representing this panel.
   *
   *@return prefix for panel type
   */
  public String     getPrefix() { return "yaftp"; }

  /**
   * Return if the scale should be logarithmic
   *
   *@return true if log scale is to be used
   */
  public boolean logScale()               { return log_cbmi.isSelected();   }

  /**
   * Set the logscale flag
   *
   *@param b true if log scale is to be used
   */
  public void    logScale(boolean b)      {        log_cbmi.setSelected(b); }

  /**
   * Return true if the rendering should use a fixed timeframe
   *
   *@return true for fixed timeframe
   */
  public boolean fixedTime()              { return fix_cbmi.isSelected();   }

  /**
   * Set the fixed timeframe flag
   *
   *@param b true if fixed timeframe is to be used
   */
  public void    fixedTime(boolean b)     {        fix_cbmi.setSelected(b);   }

  /**
   * Return the mapping function for the time-based function.
   *
   *@return time mapper string
   */
  public String  mapper()                 { return (String) mapper_cb.getSelectedItem(); }

  /**
   * Set the mapping function for the time-based function.  See the constants for choices.
   *
   *@param s time mapper string
   */
  public void    mapper(String s)         {                 mapper_cb.setSelectedItem(s); }

  /**
   * Return the method to count records.
   *
   *@return field used for counting
   */
  public String  countBy()                { return (String) count_cb.getSelectedItem(); }

  /**
   * Set the method for counting records.  Includes a default value so that the global
   * count by method will be used.
   *
   *@param s field to use for counting
   */
  public void    countBy(String s)        {                 count_cb.setSelectedItem(s); }

  /**
   * Get the configuration of this component as a string.  Intended for
   * bookmarking and recalling GUI configuration.  Need to include the 
   * fixed times for the fixed time flag...
   *
   *@return string representing GUI configuration
   */
  @Override
  public String getConfig() {
    return "RTYAFTPanel"                               + BundlesDT.DELIM +
           "count="      + Utils.encToURL(countBy())   + BundlesDT.DELIM +
           "mapper="     + Utils.encToURL(mapper())    + BundlesDT.DELIM +
           "fixed="      + fixedTime()                 + BundlesDT.DELIM +
           "log="        + logScale();
  }

  /**
   * Set the configuration of the panel.  Could be used for recalling
   * views.
   *
   *@param str configuration string
   */
  public void   setConfig(String str) {
    StringTokenizer st = new StringTokenizer(str,BundlesDT.DELIM);
    if (st.nextToken().equals("RTYAFTPanel") == false) throw new RuntimeException("setConfig(" + str + ") - Not An RTYetAnotherTimePanel");
    while (st.hasMoreTokens()) {
      StringTokenizer st2 = new StringTokenizer(st.nextToken(), "=");
      String type = st2.nextToken(), value = st2.hasMoreTokens() ? st2.nextToken() : "";
      if      (type.equals("count"))     count_cb.setSelectedItem(Utils.decFmURL(value));
      else if (type.equals("mapper"))    mapper_cb.setSelectedItem(Utils.decFmURL(value));
      else if (type.equals("fixed"))     fix_cbmi.setSelected(value.toLowerCase().equals("true"));
      else if (type.equals("log"))       log_cbmi.setSelected(value.toLowerCase().equals("true"));
      else throw new RuntimeException("Do Not Understand Type Value \"" + type + "\" = \"" + value + "\"");
    }
  }

  /**
   * Update the dropdown box for the global application fields.
   */
  @Override
  public void updateBys() {
    String strs[]; Object sel = count_cb.getSelectedItem();
    count_cb.removeAllItems();
    strs = KeyMaker.blanks(getRTParent().getRootBundles().getGlobals());
    count_cb.addItem(BundlesDT.COUNT_BY_DEFAULT);
    count_cb.addItem(BundlesDT.COUNT_BY_BUNS);
    for (int i=0;i<strs.length;i++) count_cb.addItem(strs[i]);
    if (sel == null) count_cb.setSelectedIndex(0); else count_cb.setSelectedItem(sel);
  }

  /**
   * GUI component within the panel to handle painting the rendering
   * and interaction.
   */
  public class RTYAFTComponent extends RTComponent {
    /**
     * 
     */
    private static final long serialVersionUID = -5853965121741665730L;

    /**
     * Construct the component by adding the necessary listeners.
     */
    public RTYAFTComponent() { super(); }

    /**
     * Adjusts the minimum bar width with the scroll wheel.
     */
    // @Override
    // public void mouseWheelMoved(MouseWheelEvent mwe) { minBarWidth((int) (minBarWidth() + mwe.getWheelRotation())); }

    /**
     * Return the shapes associated with the specified bundles in the current view.
     *
     *@param  bundles specific bundles to match to shapes
     *
     *@return shapes that correspond to the bundles
     */
    @Override
    public Set<Shape>  shapes(Set<Bundle> bundles) {
      Set<Shape> set = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc;
      if (myrc != null) {
        Iterator<Bundle> it = bundles.iterator();
        while (it.hasNext()) {
          Bundle bundle = it.next();
          String bins[] = myrc.bundle_to_bins.get(bundle);
          if (bins != null && bins.length > 0) {
            for (int i=0;i<bins.length;i++) set.add(myrc.bin_to_shape.get(bins[i]));
          }
        }
      }
      return set;
    }
    
    /**
     * Return the shapes that correspond to the specific bundle
     *
     *@param  bundle bundle to match for shapes
     *
     *@return corresponding shapes
     */
    @Override
    public Set<Shape>  shapes(Bundle bundle) { 
      Set<Bundle> set = new HashSet<Bundle>(); set.add(bundle); 
      return shapes(set); 
    }

    /**
     * For a specific shape, return the bundles that make up that shape.  Note
     * that the specified shape cannot be generic -- it must be a shape object
     * that was created by this component.
     *
     *@param  shape shape record created by this component to match for bundles
     *
     *@return bundles that compose the shape
     */
    @Override
    public Set<Bundle> shapeBundles(Shape shape) {
      RenderContext myrc = (RenderContext) rc;
      if (myrc != null) {
        return myrc.counter_context.getBundles(myrc.shape_to_bin.get(shape));
      } else return new HashSet<Bundle>();
    }

    /**
     * Return all of the shapes in the current rendering.
     *
     *@return set of all shapes in current rendering
     */
    @Override
    public Set<Shape> allShapes() {
      RenderContext myrc = (RenderContext) rc;
      if (myrc != null) { return myrc.allShapes(); } else return new HashSet<Shape>();
    }

    /**
     * For a general shape, find all of the overlapping shapes in the current
     * rendering.
     *
     *@param  shape_to_check general shape to match against
     *
     *@return scene-specific shapes overlapping the shape_to_check
     */
    @Override
    public Set<Shape> overlappingShapes(Shape shape_to_check) {
      Set<Shape> set = new HashSet<Shape>();
      RenderContext myrc = (RenderContext) rc;
      if (myrc != null) {
        Iterator<Shape> it = allShapes().iterator();
        while (it.hasNext()) {
          Shape shape = it.next();
          if (Utils.genericIntersects(shape, shape_to_check)) set.add(shape);
        }
      } 
      return set;
    }

    /**
     * Render the current scene with the visible data and panel GUI
     * configuration settings.  Use a unique render id to ensure only
     * the most up-to-date rendering exists.
     *
     *@param  render_id unique render id to ensure only one renderer is running
     *
     *@return render context with current data and settings
     */
    @Override
    public RTRenderContext render(short render_id) {
      clearNoMappingSet();
      if (isVisible() == false) { repaint(); return null; }
      Bundles bs = getRenderBundles();
      String count_by = getRTParent().getCountBy(), color_by = getRTParent().getColorBy();
      String count_by_setting = countBy();
      if (count_by_setting != null && count_by_setting.equals(BundlesDT.COUNT_BY_DEFAULT) == false) count_by = count_by_setting;

      if (bs != null && count_by != null) {
        RenderContext myrc = new RenderContext(render_id, bs, count_by, color_by, mapper(), logScale(), fixedTime(), getWidth(), getHeight(), (RenderContext) rc);
        return myrc;
      } else return null;
    }

    /**
     * Class representing the current rendering.  Responsible for taking the
     * visible dataset and GUI settings and renderig the appropriate scene.
     * Also maintains information on bundle to shapes for filtering and
     * brushing.
     */
    class RenderContext extends RTRenderContext {
      /**
       * Dataset to render
       */
      Bundles bs; 
      /**
       * Width of rendering in pixels
       */
      int     w, 
      /**
       * Height of rendering in pixels
       */
              h; 
      /**
       * Field to count by (height of bars in chart)
       */
      String  count_by, 
      /**
       * Field to color the bars by
       */
              color_by; 

      /**
       * Mapping of time values to x-coordinates.  Derived from the KeyMaker time strings...
       */
      String  mapper_str; 

      /**
       * Use a logarithmic scale
       */
      boolean log_scale,
      /**
       * Fix the timeframe so that it doesn't vary with the data
       */
              fixed;

      /**
       * Time delta (for linear time bin determination)
       */
      long    my_time_delta = 1L, 

      /**
       * First timestamp (linear time)
       */
              my_ts0, 

      /**
       * Last timestamp (linear time);
       */
              my_ts1;

      /**
       * Map from time values to x coordinates
       */
      XMap xmap = new XMap();

      /**
       * Number of lines needed at the top for labels
       */
      int top_line_count = 1;

      /**
       * Timeframe constant
       */
      long time_bins;

      /**
       * left-side x inset for actual graph (needs space for labels)
       */
      int     graph_x_ins, 

      /**
       * Right-side x inset for actual graph
       */
              graph_x_rgt, 

      /**
       * Actual graph width
       */
              graph_w,

      /**
       * Top y inset for actual graph (needs space for labels and time markers)
       */
              graph_y_ins, 

      /**
       * Bottom y inset for actual graph (needs space for more labels)
       */
              graph_y_bot, 

      /**
       * Actual graph height
       */
              graph_h;

      /**
       * Counter context for accumulating bundles for each bar
       */
      BundlesCounterContext counter_context;
      /**
       * Map to correspond bins (bars in this case) to the shape record
       */
      Map<String,Shape>     bin_to_shape   = new HashMap<String,Shape>();
      /**
       * Map to correspond shape to a bin
       */
      Map<Shape,String>     shape_to_bin   = new HashMap<Shape,String>();
      /**
       * All of the shapes in the dataset
       */
      Set<Shape>            all_shapes     = new HashSet<Shape>();
      /**
       * Map to convert a bundle to all of it corresponding bins
       */
      Map<Bundle,String[]>  bundle_to_bins = new HashMap<Bundle,String[]>();

      /**
       * Return the width of the rendering in pixels.
       *
       *@return width of rendering
       */
      @Override
      public int     getRCWidth()      { return w; }

      /**
       * Return the height of the rendering in pixels.
       *
       *@return height of rendering
       */
      @Override
      public int     getRCHeight()     { return h; }

      /**
       * Construct the render context with the specified datasets and parameters.
       *
       *@param id           render id - used to abort superceded renderings
       *@param bs           dataset
       *@param count_by     how to count the data
       *@param color_by     how to color the bars
       *@param mapper_str   string denoting mapping type
       *@param log_scale    flag for logarithmic scale
       *@param fixed        flag for fixed timeframe
       *@param w            width of rendering in pixels
       *@param h            height of rendering in pixels
       *@param previous     previous rendering - used to capture fixed positions
       */
      public RenderContext(short id, Bundles bs, String count_by, String color_by, String mapper_str,
                           boolean log_scale, boolean fixed,
                           int w, int h, RenderContext previous) {
        render_id = id; this.bs = bs; this.w = w; this.h = h; this.count_by = count_by; this.color_by = color_by;
        this.mapper_str = mapper_str; this.log_scale = log_scale; this.fixed = fixed;

        if (this.w <= 0) this.w = 1; if (this.h <= 0) this.h = 1;
        
        // Allocate the counter context
        counter_context = new BundlesCounterContext(bs, count_by, color_by);

        // Figure out the dimensions horizontally
        graph_x_ins = 20; graph_x_rgt = 8; graph_w = w - (graph_x_ins + graph_x_rgt); 

        // Make a default keymaker to figure out either linear time granularity or bins for periodic time
        KeyMaker km = new KeyMaker(bs.tabletIterator().next(), this.mapper_str); if (km.linearTime()) {
          // Calculate the new time delta // or copy the previous one for fixed timeframes
          if (fixed && previous != null) { 
            my_time_delta = previous.my_time_delta;
            my_ts0        = previous.my_ts0;
            my_ts1        = previous.my_ts1;
            min_ts_value  = previous.min_ts_value;
            max_ts_value  = previous.max_ts_value;
          } else { 
            my_time_delta = bs.ts1() - bs.ts0(); 
            my_ts0        = bs.ts0();             // range to display min
            my_ts1        = bs.ts1();             // range to display max
            min_ts_value  = Long.MAX_VALUE;       // actual found min
            max_ts_value  = Long.MIN_VALUE;       // actual found max
          }

          linear_time = true;

          // Make sure that the begin and end time aren't equal...
          if (my_ts0 >= my_ts1) { my_ts1 = my_ts0 + 1L; my_time_delta = my_ts1 - my_ts0; }

          // Determine what granularity to show...  then fill the xmap accordingly
          time_bins = calculateTimeBin(my_time_delta, graph_w); top_line_count = fillXMapForLinear(xmap, time_bins, bs, my_ts0, my_ts1);

          // Bin the records
          Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
            Tablet tablet = it_tab.next(); if (tabletContributes(tablet, count_by)) {
              km = new KeyMaker(tablet, keyMakerBlank(time_bins));
              Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                Bundle bundle = it_bun.next();
                long   key    = km.timeStampKey(bundle); String key_str = "" + key;
                if (fixed == false || previous == null) {
                  if (key > max_ts_value) max_ts_value = key;
                  if (key < min_ts_value) min_ts_value = key;
                }
                if (bin_to_long.containsKey(key_str) == false) bin_to_long.put(key_str, key);
                counter_context.count(bundle, key_str);

                // Update interactivity state
                if (bundle_to_bins.containsKey(bundle) == false) {
                  String as_array[] = new String[1]; as_array[0] = key_str;
                  bundle_to_bins.put(bundle, as_array);
                } else {
                  Set<String> set = new HashSet<String>(); String as_array[] = bundle_to_bins.get(bundle);
                  for (int i=0;i<as_array.length;i++) set.add(as_array[i]);
                  set.add(key_str);
                  as_array = new String[set.size()];
                  Iterator<String> it_str = set.iterator(); int i=0; while (it_str.hasNext()) as_array[i] = it_str.next();
                  bundle_to_bins.put(bundle, as_array);
                }
              }
            } else addToNoMappingSet(tablet);
          }
        } else {
          min_ts_value = km.minPeriodicValue();
          max_ts_value = km.maxPeriodicValue();
          linear_time  = false;

          // Below -- mostly copied from BoxPlot ... note:  needs to be added in order and completely // it's not sorted later
          // ... had to adjust the ranges and the starts... maybe the other implementation did that somewhere else?
          // ... had to separate out the millis version from the others due to the 2 vs 3 digits
          // ... had to remove month/day items that don't exist...
          long diff = km.maxPeriodicValue() - km.minPeriodicValue();
          if (diff < 1000L) {
            for (long l=km.minPeriodicValue();l<=km.maxPeriodicValue();l++) {
              String s = "" + l; while (s.length() < 2) s = "0" + s;
              
              // Need to remove days of the year that don't occur
              if (mapper_str.equals(KeyMaker.BY_MONTH_DAY_STR)) {
                String str = km.toString(l);
                if (str.equals("Feb-30") || str.equals("Feb-31") || str.equals("Apr-31") || 
                    str.equals("Jun-31") || str.equals("Sep-31") || str.equals("Nov-31")) continue;
                month_day_lu.put(s, str);
              }

              xmap.add(s, l);
            }
          } else if (diff == 1000L)       { // Millis
            for (long l=km.minPeriodicValue();l<=km.maxPeriodicValue();l++) {
              String s = "" + l; while (s.length() < 3) s = "0" + s;
              xmap.add(s, l);
            }
          } else if (diff == 82800000L)   { // Hours
            long inc = km.getTimeIncrement(); /* 60L * 60L * 1000L; */ long l = 0L;
            for (int i=0;i<24;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
          } else if (diff == 59000L)      { // Seconds
            long inc = km.getTimeIncrement(); /* 1000L; */             long l = 0L;
            for (int i=0;i<60;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
          } else if (diff == 3540000L)    { // Minutes
            long inc = km.getTimeIncrement(); /* 60L*1000L; */         long l = 0L;
            for (int i=0;i<60;i++,l+=inc) xmap.add((i < 10 ? "0"+i : ""+i), l);
          } else if (diff == 3599000L)    { // Minutes & Seconds // too many points for most screens...
            long inc = km.getTimeIncrement(); /* 1000L; */             long l = 0L; render_error_str = "Too Many Points for Minutes/Seconds Mapping";
            for (int i=0;i<3600;i++,l+=inc) {
              int min = i/60, sec = i%60;
              String min_str; if (min < 10) min_str = "0" + min; else min_str = "" + min;
              String sec_str; if (sec < 10) sec_str = "0" + sec; else sec_str = "" + sec;
              xmap.add(min_str + ":" + sec_str, l);
            }
          } else if (diff == 86340000L)   { // Hour & Minutes
            long inc = km.getTimeIncrement(); /* 60L*1000L; */         long l = 0L;
            for (int i=0;i<24*60;i++,l+=inc) {
              int hor = i/60, min = i%60;
              String hor_str; if (hor < 10) hor_str = "0" + hor; else hor_str = "" + hor;
              String min_str; if (min < 10) min_str = "0" + min; else min_str = "" + min;
              xmap.add(hor_str + ":" + min_str, l);
            }
          } else {
            System.err.println("Do Not Understand Periodic Diff = " + diff);
          }
          // Above -- mostly copied from BoxPlot

          // Bin the records
          Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
            Tablet tablet = it_tab.next(); if (tabletContributes(tablet, count_by)) {
              km = new KeyMaker(tablet, this.mapper_str);
              Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext() && currentRenderID() == getRenderID()) {
                Bundle bundle = it_bun.next();
                long   key    = km.timeStampKey(bundle); String key_str = "" + key;
                if (bin_to_long.containsKey(key_str) == false) bin_to_long.put(key_str, key);
                counter_context.count(bundle, key_str);

                // Update interactivity state
                if (bundle_to_bins.containsKey(bundle) == false) {
                  String as_array[] = new String[1]; as_array[0] = key_str;
                  bundle_to_bins.put(bundle, as_array);
                } else {
                  Set<String> set = new HashSet<String>(); String as_array[] = bundle_to_bins.get(bundle);
                  for (int i=0;i<as_array.length;i++) set.add(as_array[i]);
                  set.add(key_str);
                  as_array = new String[set.size()];
                  Iterator<String> it_str = set.iterator(); int i=0; while (it_str.hasNext()) as_array[i] = it_str.next();
                  bundle_to_bins.put(bundle, as_array);
                }
              }
            } else addToNoMappingSet(tablet);
          }
        }
      }

      /**
       * Lookup values for the long strings to their original long format... 
       */
      Map<String,Long> bin_to_long = new HashMap<String,Long>();

      /**
       * Min time value seen
       */
      long    min_ts_value,

      /**
       * Max time value seen
       */
              max_ts_value;

      /**
       * Linear time render
       */
      boolean linear_time;

      /**
       * Used to capture error messages for user
       */
      String render_error_str = null;

      /**
       * Determine if a tablet can contribute -- specifically, does it
       * have time data?  and does it satisfy the count_by string...
       *
       *@param tablet    tablet to evaluate
       *@param count_by  count by option
       *
       *@return true if tablet contributes
       */
      private boolean tabletContributes(Tablet tablet, String count_by) {
        if (tablet.hasTimeStamps()) {
          return count_by.equals(KeyMaker.RECORD_COUNT_STR) || KeyMaker.tabletCompletesBlank(tablet, count_by);
        } else return false;
      }

      /**
       * Render the labels along the top of the visualization
       * - closely paired with the fillXMapForLinear() for linear mappings
       */
      private void renderTopLabels(Graphics2D g2d, int bar_w, int bar_gap) {

        //
        // Linear
        //
        if (mapper_str.equals(KeyMaker.BY_AUTOTIME_STR)) {
          char delim = '\''; // can't do null... so will use this instead
          // Mostly a copy of the structure from the fillXMapForLinear() method
          if      (time_bins == SECONDS)     { delim = ':';  } // year-month-day hour:minute | second
          else if (time_bins == SECONDSx10)  { delim = ':';  } // year-month-day hour:minute | second
          else if (time_bins == MINUTES)     { delim = ':';  } // year-month-day hour        | minute
          else if (time_bins == MINUTESx10)  { delim = ':';  } // year-month-day hour        | minute
          else if (time_bins == HOURS)       { delim = ' ';  } // year-month-day             | hour
          else if (time_bins == HOURSx4)     { delim = ' ';  } // year-month-day             | hour
          else if (time_bins == DAYS)        { delim = '-';  } // year-month                 | day
          else if (time_bins == MONTHS)      { delim = '-';  } // year                       | month
          else if (time_bins == QUARTERS)    { delim = '-';  } // year                       | quarter
          else                               { delim = '\''; } // year

          int    first_x         = -1, second_x         = -1;
          String last_first      = "", last_second      = "";
          String last_first_hash = "", last_second_hash = "";

          Iterator<String> it = xmap.labels.iterator(); while (it.hasNext()) {
            String label = it.next();

            // Figure out the first and second lines...  may not always be a second line
            String first = "", second = "";
            if (delim == '\'') { first = label; } else {
              String str = label; if (str.indexOf(delim) >= 0) {
                first  = str.substring(0,str.lastIndexOf(delim));
                second = str.substring(str.lastIndexOf(delim)+1,str.length());
              } else first = str;
            }

            int x = graph_x_ins + xmap.label_to_xbin.get(label) * (bar_w + bar_gap);

            // Draw Hash marks
            if (first.equals(last_first_hash)   == false) {
              g2d.setColor(RTColorManager.getColor("axis", "minor"));
              g2d.drawLine(x, 0, x, graph_y_ins + graph_h);
              last_first_hash = first;
            }
            if (delim != '\'' && second.equals(last_second_hash) == false) {
              // g2d.drawLine(x, Utils.txtH(g2d, second) + 2, x, graph_y_ins + graph_h);
              last_second_hash = second;
            }

            // Draw Labels
            if (first.equals(last_first) == false    && x >= first_x) {
              g2d.setColor(RTColorManager.getColor("label", "default"));
              g2d.drawString(first, x,        Utils.txtH(g2d,first));
              last_first = first;   first_x  = x + (int) (1.1 * Utils.txtW(g2d, first));

              if (delim != '\'') g2d.drawString(second, x, 1 + 2*Utils.txtH(g2d,second));
            }
            if (second.equals(last_second) == false && x >= second_x) {
              // g2d.setColor(RTColorManager.getColor("label", "default"));
              // g2d.drawString(second, x, 1 + 2*Utils.txtH(g2d,second));
              last_second = second; second_x = x + (int) (1.1 * Utils.txtW(g2d, second));
            }
          }

        //
        // Periodic
        //
        } else {
          // No general solution... so making something that's totally not dumb
          Set<String>        to_draw = new HashSet<String>(); // Specific labels to draw... if null, try to draw them all...
          Map<String,String> trans   = null;                  // Translates specific strings in the map to more human readable strings

          // Copied from the list in the KeyMaker class
          if         (mapper_str.equals(KeyMaker.BY_SECOND_STR))         { to_draw.add("00");  to_draw.add("15");  to_draw.add("30");  to_draw.add("45");
          } else if  (mapper_str.equals(KeyMaker.BY_MILLIS_STR))         { to_draw.add("000"); to_draw.add("250"); to_draw.add("500"); to_draw.add("750");
          } else if  (mapper_str.equals(KeyMaker.BY_MINUTE_SECOND_STR))  { to_draw.add("00:00"); to_draw.add("15:00"); to_draw.add("30:00"); to_draw.add("45:00");
          } else if  (mapper_str.equals(KeyMaker.BY_MINUTE_STR))         { to_draw.add("00");  to_draw.add("15");  to_draw.add("30");  to_draw.add("45");
          } else if  (mapper_str.equals(KeyMaker.BY_HOUR_MINUTE_STR))    { to_draw.add("00:00"); to_draw.add("04:00"); to_draw.add("08:00");
                                                                           to_draw.add("12:00"); to_draw.add("16:00"); to_draw.add("20:00");
          } else if  (mapper_str.equals(KeyMaker.BY_HOUR_STR))           { to_draw.add("00");  to_draw.add("06");  to_draw.add("12");  to_draw.add("18");
          } else if  (mapper_str.equals(KeyMaker.BY_DAYOFWEEK_HOUR_STR)) { trans = dayOfWeekHourTranslation(); to_draw.addAll(trans.keySet());
          } else if  (mapper_str.equals(KeyMaker.BY_DAYOFWEEK_STR))      { trans = dayOfWeekTranslation();     to_draw.addAll(trans.keySet());
          } else if  (mapper_str.equals(KeyMaker.BY_MONTH_DAY_STR))      { trans = monthDayTranslation();      to_draw.addAll(trans.keySet());
          } else if  (mapper_str.equals(KeyMaker.BY_MONTH_STR))          { trans = monthTranslation();         to_draw.addAll(trans.keySet());
          } else if  (mapper_str.equals(KeyMaker.BY_QUARTER_STR))        { trans = quarterTranslation();       to_draw.addAll(trans.keySet());
          } else {
            g2d.setColor(RTColorManager.getColor("label","errorfg"));
            g2d.drawString("Do Not Understand Periodic Time \"" + mapper_str + "\"", 10, Utils.txtH(g2d, "0") + 2);
          }

          if (to_draw == null || to_draw.size() > 0) {
            Iterator<String> it = xmap.label_to_xbin.keySet().iterator(); while (it.hasNext()) {
              String str = it.next(); if (to_draw == null || to_draw.contains(str)) {
                int x = graph_x_ins + xmap.label_to_xbin.get(str) * (bar_w + bar_gap);

                g2d.setColor(RTColorManager.getColor("label", "default"));
                if (trans != null && trans.containsKey(str)) g2d.drawString(trans.get(str), x + 2, 1 + Utils.txtH(g2d,trans.get(str)));
                else                                         g2d.drawString(str,            x + 2, 1 + Utils.txtH(g2d,str));

                if (to_draw != null && to_draw.contains(str)) {
                  g2d.setColor(RTColorManager.getColor("axis", "minor"));
                  g2d.drawLine(x, 0, x, graph_y_ins + graph_h);
                }
              }
            }
          }
        }
      }

      // Translations for day of week hour
      private Map<String,String> dayOfWeekHourTranslation() {
        Map<String,String> map = new HashMap<String,String>();
        map.put("00",  "Sun 00"); map.put("24",  "Mon 00"); map.put("48",  "Tue 00"); map.put("72",  "Wed 00");
        map.put("96",  "Thu 00"); map.put("120", "Fri 00"); map.put("144", "Sat 00");
        return map;
      }

      // Translations to the days of the week
      private Map<String,String> dayOfWeekTranslation() {
        Map<String,String> map = new HashMap<String,String>();
        map.put("00","Sun"); map.put("01","Mon"); map.put("02","Tue"); map.put("03","Wed"); map.put("04","Thu"); map.put("05","Fri"); map.put("06","Sat");
        return map;
      }

      // Translations for day of week hour
      private Map<String,String> month_day_lu = new HashMap<String,String>(); // Filled in during the creation... not 100% sure of how the math works for day of year... esp. leap or non-leap years
      private Map<String,String> monthDayTranslation() {
        Map<String,String> map = new HashMap<String,String>();
        Iterator<String> it = month_day_lu.keySet().iterator(); while (it.hasNext()) {
          String key = it.next(); String value = month_day_lu.get(key); if (value.endsWith("-01")) map.put(key,value);
        }
        return map;
      }

      // Translations to the months
      private Map<String,String> monthTranslation() {
        Map<String,String> map = new HashMap<String,String>();
        map.put("00","Jan"); map.put("01","Feb"); map.put("02","Mar"); map.put("03","Apr"); map.put("04","May"); map.put("05","Jun");
        map.put("06","Jul"); map.put("07","Aug"); map.put("08","Sep"); map.put("09","Oct"); map.put("10","Nov"); map.put("11","Dec");
        return map;
      }

      // Translations to the months
      private Map<String,String> quarterTranslation() {
        Map<String,String> map = new HashMap<String,String>();
        map.put("00","Q1"); map.put("01","Q2"); map.put("02","Q3"); map.put("03","Q4");
        return map;
      }

      /**
       * Rendering - saved so it can be recalled easily (for repaints)
       */
      BufferedImage base_bi;

      /**
       * Render the shapes to an image for painting the component.
       *
       *@return rendered image
       */
      @Override
      public BufferedImage getBase() {
        if (base_bi != null) return base_bi;
        Graphics2D g2d = null; BufferedImage bi = null;
        try {
          bi = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB); g2d = (Graphics2D) bi.getGraphics();
          g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
          RTColorManager.renderVisualizationBackground(bi, g2d);

          // Figure out the dimensions (vertical)
          graph_y_ins = top_line_count * Utils.txtH(g2d,"0") + 4; ; graph_y_bot = Utils.txtH(g2d,"0") + 4; ; graph_h = h - (graph_y_ins + graph_y_bot);

          // Determine the bar width and bar_gap
          int bar_w, bar_gap = 0;
          int bins = xmap.ts_to_xbin.keySet().size();
          bar_w = graph_w / bins;
          if (bar_w < minBarWidth()) bar_w = (int) minBarWidth();
          if (bar_w > maxBarWidth()) bar_w = (int) maxBarWidth();
          if (mapper_str.equals(KeyMaker.BY_MILLIS_STR)      || 
              mapper_str.equals(KeyMaker.BY_HOUR_MINUTE_STR) ||
              mapper_str.equals(KeyMaker.BY_MINUTE_SECOND_STR)) bar_w = 1;
          if (bar_w > 3) { bar_w--; bar_gap = 1; }

          int actual_graph_w = (bar_w + bar_gap) * xmap.xbin_to_label.size() + 2;

          // Render the axes
          g2d.setColor(RTColorManager.getColor("axis","major"));
          g2d.drawLine(graph_x_ins, graph_y_ins,           graph_x_ins,                  graph_y_ins + graph_h);
          g2d.drawLine(graph_x_ins, graph_y_ins + graph_h, graph_x_ins + actual_graph_w, graph_y_ins + graph_h);

          // Render the top labels
          renderTopLabels(g2d, bar_w, bar_gap);

          // - draw a red arrow if the actual graph_w cannot be rendered
          if (actual_graph_w > (w-1)) {
            g2d.setColor(RTColorManager.getColor("label","errorfg"));
            g2d.drawLine(w,       graph_y_ins + graph_h + graph_y_ins/3, w - 30,  graph_y_ins + graph_h + graph_y_ins/3);
            g2d.drawLine(w,       graph_y_ins + graph_h + graph_y_ins/3, w -  8,  graph_y_ins + graph_h + graph_y_ins/3 + 8);
            g2d.drawLine(w,       graph_y_ins + graph_h + graph_y_ins/3, w -  8,  graph_y_ins + graph_h + graph_y_ins/3 - 8);
          }

          // Iterate over the bins
          Iterator<String> it_bin = counter_context.binIterator(); while (it_bin.hasNext()) {
            String bin = it_bin.next(); long bin_l = bin_to_long.get(bin);

            // Determine the x coordinate for this bar
            int xs = 0;
            if (linear_time) {
              xs = xmap.ts_to_xbin.get(bin_l) * (bar_w + bar_gap);
            } else { 
              xs = xmap.ts_to_xbin.get(bin_l) * (bar_w + bar_gap);
            }

            // Render the graphic for this bar
            // - complete space possible
            Rectangle2D overall_bar = new Rectangle2D.Double(graph_x_ins + xs,graph_y_ins,bar_w,graph_h);

            // - bar height
            int    bar_h, LOG_1_H = 4;
            if (log_scale == false) {
              bar_h = (int) (counter_context.totalNormalized(bin)*graph_h);
              if (counter_context.totalNormalized(bin) > 0.0 && bar_h < 1) bar_h = 1;
            } else {
              if        (counter_context.total(bin) > 1.0) {
                bar_h = LOG_1_H + (int) (((graph_h - LOG_1_H)*Math.log(counter_context.total(bin)))/Math.log(counter_context.totalMaximum()));
              } else if (counter_context.total(bin) > 0.0) {
                bar_h = LOG_1_H;
              } else {
                bar_h = 0;
              }
            }
            int sy = graph_y_ins + graph_h - bar_h;

            // - actual bar geometry
            Rectangle2D bar = new Rectangle2D.Double(graph_x_ins + xs,graph_y_ins+graph_h-bar_h,bar_w,bar_h);

            // Determine if color by is necessary
            if (color_by != null) {
              int y_inc = graph_y_ins + graph_h;
              List<String> cbins = counter_context.getColorBinsSortedByCount();
              for (int i=cbins.size()-1;i>=0;i--) {
                String cbin   = cbins.get(i);
                double ctotal = counter_context.total(bin,cbin);
                if (ctotal > 0L) {
                  int sub_h = (int) ((ctotal * bar_h)/counter_context.binColorTotal(bin));
                  if (sub_h > 0) {
                    g2d.setColor(RTColorManager.getColor(cbin));
                    g2d.fillRect(graph_x_ins + xs, y_inc - sub_h, bar_w, sub_h);
                    y_inc -= sub_h;
                  }
                }
              }
              if (y_inc != sy) {
                g2d.setColor(RTColorManager.getColor("set", "multi"));
                g2d.fillRect(graph_x_ins + xs, sy, bar_w, y_inc - sy);
              }
            } else {
              g2d.setColor(counter_context.binColor(bin));
              g2d.fill(bar);
            }


            // Update the pointers for interactivity
            bin_to_shape.put(bin,overall_bar); shape_to_bin.put(overall_bar,bin); all_shapes.add(overall_bar);
          }

          // Labels ... x-axis
          g2d.setColor(RTColorManager.getColor("label", "default")); String str = null;
          if (mapper_str.equals(KeyMaker.BY_AUTOTIME_STR)) {
            str = keyMakerBlank(time_bins); 
          } else str = mapper_str;
          g2d.drawString(str, graph_x_ins + actual_graph_w/2 - Utils.txtW(g2d,str)/2, graph_y_ins + graph_h + 2 + Utils.txtH(g2d,str));

          // Labels ... y-axis
          if (log_scale) g2d.setColor(RTColorManager.getColor("label", "log"));
          else           g2d.setColor(RTColorManager.getColor("label", "linear"));
          Utils.drawRotatedString(g2d, count_by, graph_x_ins - 3,  graph_y_ins + graph_h/2 + Utils.txtW(g2d,count_by)/2);

        } finally { if (g2d != null) g2d.dispose(); } // Clean-up

        return (base_bi = bi);
      }

      /**
       *
       */
      public Set<Shape> allShapes() { return all_shapes; }
    }
  }
}

