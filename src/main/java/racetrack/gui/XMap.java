/* 

Copyright 2022 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.gui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Map for the bins to the locations on the screens and labels...
 */
public class XMap { 
  public Map<Long,Integer>    ts_to_xbin    = new HashMap<Long,Integer>();
  public Map<Long,String>     ts_to_label   = new HashMap<Long,String>();
  public Map<Integer,String>  xbin_to_label = new HashMap<Integer,String>();
  public Map<String,Integer>  label_to_xbin = new HashMap<String,Integer>();
  public List<String>         labels        = new ArrayList<String>();

  // Needs to be added in order...
  public void add(String str, long ts) {
    int xbin = ts_to_xbin.keySet().size();
    ts_to_xbin.   put(ts,  xbin);
    ts_to_label.  put(ts,  str);
    xbin_to_label.put(xbin,str);
    label_to_xbin.put(str,xbin);
    labels.add(str);
  }

  // For debug
  public String toString() {
    StringBuffer sb = new StringBuffer();
    Iterator<Integer> it = xbin_to_label.keySet().iterator(); while (it.hasNext()) {
      int key = it.next();
      sb.append(key + " => " + xbin_to_label.get(key) + "\r\n");
    }
    return sb.toString();
  }
}

