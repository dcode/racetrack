/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.cleaning;

import java.io.*;
import java.util.*;

import racetrack.util.Utils;

/**
 * Convert the Enron dataset into a CSV compatible with RACETrack.
 */
public class EnronToCSV {
  /**
   * CSV Output Print Stream
   */
  PrintStream csv_out;

  /**
   * Constructor -- recurse through the passed directory and convert files as they are discovered.
   *
   *@param dir parent or local directory within the Enron dataset
   */
  public EnronToCSV(File dir) throws IOException { 
    csv_out = new PrintStream(new FileOutputStream(new File("rt.csv")));
    csv_out.println("timestamp,subject,reply,forward,from,body,tags");
    recurse(dir, 0); 
  }

  /**
   * List the files in the specified directory.  For regular files, parse them into CSV lines.  For
   * directories, recurse into them.
   *
   *@param dir   directory to examine
   *@param level indentation level for printing output
   */
  protected void recurse(File dir, int level) throws IOException {
    System.err.print(".");
    // indent(level, System.err); System.err.println("Examining Directory \"" + dir + "\"...");
    if (dir.isDirectory()) {
      File files[] = dir.listFiles(); for (int i=0;i<files.length;i++) {
        if (files[i].isDirectory()) recurse(files[i], level+1); else parseFile(files[i], level);
      }
    }
  }

  /**
   * Parse a file into its CSV rows.
   *
   *@param file  file to parse
   *@param level indentation level for printing output
   */
  protected void parseFile(File file, int level) throws IOException {
    // indent(level+2, System.err); System.err.println("Parsing File \"" + file + "\"...");
    BufferedReader in = new BufferedReader(new FileReader(file));

    // State variables
    boolean in_to = false, in_cc = false, in_bcc = false, blank_line_seen = false, ignore_rest = false;  // State variables
    StringBuffer body = new StringBuffer();
    String       subject   = "", 
                 from      = "";
    long         timestamp = 0L;
    List<String> to = new ArrayList<String>(), cc = new ArrayList<String>(), bcc = new ArrayList<String>();

    // Read each line... fill in the state
    String line; while ((line = in.readLine()) != null) {
      // System.err.println("Line = \"" + line + "\"");
      if        (ignore_rest) {

      } else if (blank_line_seen) {
        if (line.indexOf("--Original Message--") >= 0) { ignore_rest = true; } else {
          if (body.length() > 0) body.append(" ");
          body.append(line);
        }
      } else {
        if (line.equals("")) blank_line_seen = true; else {

          // Most of these are just parsing the specified email field
          if        (line.startsWith("Date:"))    { in_to = in_cc = in_bcc = false;
                                                    try { timestamp = parseTimeStamp(trim(line.substring(("Date:").length(),line.length()))); 
                                                    } catch (StringIndexOutOfBoundsException sioobe) {
                                                      System.err.println("TimeStamp For Line \"" + line + "\" Messed Up...");
                                                      in.close(); return;
                                                    }
          } else if (line.startsWith("From:"))    { in_to = in_cc = in_bcc = false; 
                                                    from = extractEmails(line.substring(("From:").length(),line.length())).get(0);
          } else if (line.startsWith("Subject:")) { in_to = in_cc = in_bcc = false;
                                                    subject = line.substring(("Subject:").length()+1,line.length());
          } else if (line.startsWith("To:"))      { in_to = true;  in_cc  = false; in_bcc = false;
                                                    to.addAll(extractEmails(line.substring(("To:").length(), line.length())));
          } else if (line.startsWith("Cc:"))      { in_to = false; in_cc  = true;  in_bcc = false;
                                                    cc.addAll(extractEmails(line.substring(("Cc:").length(), line.length())));
          } else if (line.startsWith("Bcc:"))     { in_to = false; in_cc  = false; in_bcc = true;
                                                    bcc.addAll(extractEmails(line.substring(("bcc:").length(), line.length())));

          // Really just to stop parsing email addresses...
          } else if (line.startsWith("Mime-Version:")              ||
                     line.startsWith("Content-Type:")              ||
                     line.startsWith("Content-Transfer-Encoding:") ||
                     line.startsWith("X-"))       { in_to = in_cc = in_bcc = false;

          // Various lists of email addresses
          } else if (in_to)  { to.addAll(extractEmails(line));
          } else if (in_cc)  { cc.addAll(extractEmails(line));
          } else if (in_bcc) { bcc.addAll(extractEmails(line));
          } else { /* System.err.println("Ignoring Line \"" + line + "\"");  */ }
        }
      }
    }
    in.close();

    // Normalize subject lines if they look like forwards or replies
    boolean reply   = false; if (subject.toLowerCase().startsWith("re: ")) { reply   = true; subject = subject.substring(4,subject.length()); }
                             if (subject.toLowerCase().equals    ("re:"))  { reply   = true; subject = "";                                    }
    boolean forward = false; if (subject.toLowerCase().startsWith("fw: ")) { forward = true; subject = subject.substring(4,subject.length()); }
                             if (subject.toLowerCase().equals    ("fw:"))  { forward = true; subject = "";                                    }

    // Make the tags for the to, cc, and bcc fields
    StringBuffer tags = new StringBuffer();
    Iterator<String> it;
    it = to. iterator(); while (it.hasNext()) { if (tags.length() > 0) tags.append("|"); tags.append("to="  + it.next()); }
    it = cc. iterator(); while (it.hasNext()) { if (tags.length() > 0) tags.append("|"); tags.append("cc="  + it.next()); }
    it = bcc.iterator(); while (it.hasNext()) { if (tags.length() > 0) tags.append("|"); tags.append("bcc=" + it.next()); }

    // Found bad timestamps in the data... so... constrain to only valid timestamps
    if (timestamp > Utils.parseTimeStamp("1990-01-01") && timestamp < Utils.parseTimeStamp("2020-01-01")) {
      csv_out.println(Utils.exactDate(timestamp)      + "," +
                      Utils.encToURL(subject)         + "," +
                      reply                           + "," +
                      forward                         + "," +
                      Utils.encToURL(from)            + "," +
                      Utils.encToURL(body.toString()) + "," +
                      tags);
    }
  }

  /**
   * Indent a print stream.  Mostly used for debugging...
   *
   *@param spaces spaces to insert -- multiplied by 2
   *@param out    output print stream
   */
  protected void indent(int spaces, PrintStream out) { for (int i=0;i<spaces*2;i++) out.print(" "); }

  /**
   * For a file line with emails, parse them out and return as a List.
   *
   *@param str file line to parse
   *
   *@return list of email addresses found
   */
  protected List<String> extractEmails(String str) {
    List<String> ret = new ArrayList<String>();
    StringTokenizer st = new StringTokenizer(str, ",");
    while (st.hasMoreTokens()) {
      String email_part = trim(st.nextToken().toLowerCase()); if (email_part.equals("") == false) {
        if (email_part.indexOf("<") >= 0) {
          int i0 = email_part.indexOf("<."),
              i1 = email_part.lastIndexOf(">");
           email_part = email_part.substring(i0+2,i1);
          // System.err.println("\"" + email_part + "\"");
          ret.add(email_part);
        } else ret.add(email_part);
      }
    }

    // if (ret.size() == 0 || allEmails(ret) == false) System.err.println("** Email Lines \"" + str + "\""); // Getting some " " lines...

    if (ret.size() > 0 && allEmails(ret) == false)  System.err.println("Not An Email? \"" + str + "\"");

    return ret;
  }

  /**
   * Removing pre or post whitespace characters.
   *
   *@param str string to process
   *
   *@return string without white spaces at beginning/ending
   */
  protected String trim(String str) {
    while (str.startsWith(" ") || str.startsWith("\t")) str = str.substring(1,str.length());
    while (str.endsWith  (" ") || str.endsWith  ("\t")) str = str.substring(0,str.length()-1);
    return str;
  }

  /**
   * Determine if everything in the list is an email.  If not, return false.
   *
   *@param list list of email strings
   *
   *@return true if everything looked like an email
   */
  protected boolean allEmails(List<String> list) {
    for (int i=0;i<list.size();i++) {
      if (list.get(i).indexOf("@") < 0 || list.get(i).indexOf(".") < 0) {
        System.err.println("Not An Email \"" + list.get(i) + "\"");
        return false;
      }
    }
    return true;
  }

  /**
   * Parse a timestamp.
   *
   *@param str string to parse
   *
   *@return timestamp in millis
   */
  protected long parseTimeStamp(String str) {
    str = str.substring(str.indexOf(",")+2,str.length());
    String timezone = str.substring(str.lastIndexOf("(")+1,str.lastIndexOf(")"));
    str = str.substring(0,str.lastIndexOf(" "));
    str = str.substring(0,str.lastIndexOf(" "));

    StringTokenizer st = new StringTokenizer(str, " :");
    String day_str = st.nextToken(), mon_str = st.nextToken(), yer_str = st.nextToken(),
           hor_str = st.nextToken(), min_str = st.nextToken(), sec_str = st.nextToken();

    int mon;
    if      (mon_str.toLowerCase().equals("jan")) mon =  1;
    else if (mon_str.toLowerCase().equals("feb")) mon =  2;
    else if (mon_str.toLowerCase().equals("mar")) mon =  3;
    else if (mon_str.toLowerCase().equals("apr")) mon =  4;
    else if (mon_str.toLowerCase().equals("may")) mon =  5;
    else if (mon_str.toLowerCase().equals("jun")) mon =  6;
    else if (mon_str.toLowerCase().equals("jul")) mon =  7;
    else if (mon_str.toLowerCase().equals("aug")) mon =  8;
    else if (mon_str.toLowerCase().equals("sep")) mon =  9;
    else if (mon_str.toLowerCase().equals("oct")) mon = 10;
    else if (mon_str.toLowerCase().equals("nov")) mon = 11;
    else if (mon_str.toLowerCase().equals("dec")) mon = 12;
    else throw new RuntimeException("Do Not Understand Month String \"" + mon_str + "\"");

    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone(timezone));
    cal.set(Integer.parseInt(yer_str), mon-1, Integer.parseInt(day_str), Integer.parseInt(hor_str), Integer.parseInt(min_str), Integer.parseInt(sec_str));
    long ts = cal.getTimeInMillis();

    // System.err.println("\"" + str + "\" ... tz = \"" + timezone + "\" ... reformat = " + Utils.exactDate(ts));

    return ts;
  }

  /**
   * Call the parser with the command line argument directory.
   *
   *@param args command line arguments -- first (and only) should be the parent directory of the data
   */
  public static void main(String args[]) {
    try { new EnronToCSV(new File(args[0]));
    } catch (IOException      ioe) { System.err.println("IOException: "      + ioe); ioe.printStackTrace(System.err);
    } catch (RuntimeException re)  { System.err.println("RuntimeException: " + re);  re.printStackTrace(System.err);
    }
  }
}
