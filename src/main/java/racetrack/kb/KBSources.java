/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.kb;

import java.util.ArrayList;
import java.util.List;

/**
 * Knowledge sources... really just anything that has an API lookup for data...
 */
public class KBSources {
  public static KBSource[] getSources() {
    // Add each source
    List<KBSource> sources_al = new ArrayList<KBSource>();
    sources_al.add(new GoogleReverseLookupKBSource());
    sources_al.add(new DummyKBSource(100, 0.0));
    sources_al.add(new DummyKBSource(10,  0.2));
    sources_al.add(new DummyKBSource(500, 0.1));

    // Convert to an array
    KBSource sources_array[] = new KBSource[sources_al.size()];
    for (int i=0;i<sources_array.length;i++) sources_array[i] = sources_al.get(i);

    return sources_array;
  }
}
