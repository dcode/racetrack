/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.kb;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import racetrack.framework.BundlesDT;

import racetrack.util.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Reverse lookup service from google.  Described on the following page.
 *
 * https://medium.com/@aimhuge/reverse-dns-lookup-in-google-sheets-234c75966e55
 */
public class GoogleReverseLookupKBSource implements KBSource {
  /**
   * Return the name of the KB Source
   *
   *@return name of data source
   */
  public String kbSourceName() { return "Google Reverse Lookup"; }

  /**
   * Return a prefix to be used with naming tags and headers.
   *
   *@return prefix string for naming
   */
  public String kbPrefix() { return "grl"; }

  /**
   * Return the a set of data types that this KB source handles.
   *
   *@param set set of data types processed by this data source
   */
  public Set<BundlesDT.DT> handlesDataTypes() {  Set<BundlesDT.DT> set = new HashSet<BundlesDT.DT>(); set.add(BundlesDT.DT.IPv4); return set; }

  /**
   * Provide selectable options -- in this case, none.
   */
  public List<String> selectableOptions() { return null; }

  /**
   * Return the query params... none for this service.
   *
   *@return set of query params (empty)
   */
  public Set<String> queryParams() { 
    Set<String> set = new HashSet<String>(); 
    set.add(REMOVE_NONPUBLIC_IPS_STR); // Reverse resolutions would be useless
    return set;
  }

  /**
   * Execute a query for indicators.  Return a list of attribute maps -- one
   * per record returned.  Can be heterogeneous.  Should be threaded to avoid
   * blocking...
   *
   *@param entities           entities to query
   *@param query_params       source specific query parameters (as strings) -- can be null
   *@param succeeded          queries that succeeded (even if it returned no results)
   *@param failed             queries that failed / threw an exception
   *@param progress_listener  listener that handles information about the progress of the query -- can be null
   */
  public List<Map<String,String>> query(Set<String>        entities, 
                                        Map<String,String> query_params, 
                                        Set<String>        succeeded,
                                        Set<String>        failed,
                                        KBProgressListener progress_listener) {
    List<Map<String,String>> ret = new ArrayList<Map<String,String>>(); if (entities != null && entities.size() > 0) {
      Queue<String> queue = new LinkedList<String>(); queue.addAll(entities);

      // Create the worker threads
      WorkerThread threads[] = new WorkerThread[16]; for (int i=0;i<threads.length;i++) {
        threads[i] = new WorkerThread(queue, query_params, succeeded, failed, progress_listener, ret);
        threads[i].start();
      }

      // Rejoin the threads
      for (int i=0;i<threads.length;i++) { try { threads[i].join(); } catch (InterruptedException ie) { } }
    }
    return ret;
  }

  /**
   * Worker thread to perform a single query.
   */
  class WorkerThread extends Thread {
    Queue<String>             queue; 
    Map<String,String>        query_params;
    KBProgressListener        progress_listener;
    List<Map<String,String>>  ret;
    int                       queue_size_init;
    Set<String>               succeeded,
                              failed;

    /**
     * Constructor
     */
    public WorkerThread(Queue<String> queue0, Map<String,String> query_params0, Set<String> succeeded0, Set<String> failed0,  KBProgressListener progress_listener0, List<Map<String,String>> ret0) {
      this.queue = queue0; this.query_params = query_params0; this.succeeded = succeeded0; this.failed = failed0;
      this.progress_listener = progress_listener0; this.ret = ret0; 
      synchronized (queue) { queue_size_init = queue.size(); if (queue_size_init == 0) queue_size_init = 1; }
    }

    /**
     * Return the next entity from the queue in a thread-safe manner.
     *
     *@return next entity for query or null if the queue is empty or if the progress listener returns false
     */
    public String nextEntity() { 
      String entity = null;  int queue_size = 0;

      // Synchronized access to the queue
      synchronized (queue) { if (queue.size() > 0) { entity = queue.remove(); } queue_size = queue.size(); } 

      // Update the progress listener if it's set and check for early termination
      if (progress_listener != null && progress_listener.percentComplete(((float) queue_size)/queue_size_init) == false) entity = null;

      return entity; 
    }
     
    /**
     * Get an entity, do the lookup, and add it to the results.
     */
    public void run() {
      String entity = null; while ((entity = nextEntity()) != null && Utils.isIPv4(entity)) {
        try {
          // Construct query string
          URL url = new URL("https://dns.google.com/resolve?name=" + Utils.reverseIPv4(entity) + ".in-addr.arpa&type=PTR");

          // Request results
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setRequestMethod("GET");
          conn.setRequestProperty("Content-Type", "application/json; utf-8");
          conn.setRequestProperty("Accept", "application/json");
          // conn.setDoOutput(true); // If it's a post...

          // Read the response
          StringBuffer sb = new StringBuffer();
          BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
          String line = null; while ((line = br.readLine()) != null) sb.append(line.trim());
      
          // Parse the results & add to return variable
          // System.err.println("Results = \"" + sb.toString() + "\"");
          JsonObject root = new JsonParser().parse(sb.toString()).getAsJsonObject();

          // Does it have answers?
          if (root.getAsJsonPrimitive("RA").getAsBoolean()) {
            JsonArray answer_array = root.getAsJsonArray("Answer");
            for (int i=0;i<answer_array.size();i++) {
              JsonObject  answer = (JsonObject) answer_array.get(i);
              JsonElement data = answer.getAsJsonPrimitive("data");
              if (data != null) {
                String domain = data.getAsString();
                // System.err.println("Domain = \"" + domain + "\"");

                // Encode the results and (safely) add to the return
                Map<String,String> map = new HashMap<String,String>();
                map.put("timestamp",       Utils.exactDate(System.currentTimeMillis()));
                map.put("query",           entity);
                map.put("ip",              entity);
                map.put("domain",          domain);
                map.put("source",          kbSourceName());

                synchronized (ret) { ret.add(map); }
              }
            }
          }
          synchronized (succeeded) { succeeded.add(entity); }
        } catch (MalformedURLException        mue) {
          if (progress_listener != null) { progress_listener.exceptionThrown(mue, entity);
          } else { mue_count++;
           if (mue_count == 1 || mue_count == 100 || mue_count == 1000) {
             System.err.println("MalformedURLException : " + mue + " [count = " + mue_count + "]");
           }
          } 
          synchronized (failed) { failed.add(entity); }
        } catch (UnsupportedEncodingException uee) {
          if (progress_listener != null) { progress_listener.exceptionThrown(uee, entity);
          } else { uee_count++;
           if (uee_count == 1 || uee_count == 100 || uee_count == 1000) {
             System.err.println("UnsupportedEncodingException : " + uee + " [count = " + uee_count + "]");
           }
          }
          synchronized (failed) { failed.add(entity); }
        } catch (IOException                  ioe) { 
          if (progress_listener != null) { progress_listener.exceptionThrown(ioe, entity);
          } else { ioe_count++;
           if (ioe_count == 1 || ioe_count == 100 || ioe_count == 1000) {
             System.err.println("IOException : " + ioe + " [count = " + ioe_count + "]");
           }
          }
          synchronized (failed) { failed.add(entity); }
        }
      }
    }
  }

  // Counters for exceptions thrown
  int mue_count = 0, uee_count = 0, ioe_count = 0;

  /**
   *
   */
  public static void main(String args[]) {
    Set<String> entities = new HashSet<String>();
    for (int i=0;i<args.length;i++) entities.add(args[i]);

    KBSource service = new GoogleReverseLookupKBSource();

    Set<String> succeeded = new HashSet<String>(),
                failed    = new HashSet<String>();

    List<Map<String,String>> results = service.query(entities, null, succeeded, failed, null);
    for (int i=0;i<results.size();i++) { System.err.println(results.get(i)); }

    System.err.println(); System.err.println("Succeeded: " + succeeded);
    System.err.println(); System.err.println("Failed: " + failed);
  }
}

