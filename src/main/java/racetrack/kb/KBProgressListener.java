/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.kb;

/**
 * Progress listener for KB queries.
 */
public interface KBProgressListener {
  /**
   * Notification on the percentage complete to the user interface.  Includes the current entity as well as a message.
   *
   *@param percent_complete percentage complete
   *@param entity           current query entity
   *@param message          additional information on the status of the service
   *
   *@return true to indicate that the service should continue API calls
   */
  public boolean percentComplete(double percent_complete, String entity, String message);

  /**
   * Notification on the percentage complete to the user interface.
   *
   *@param percent_complete
   *
   *@return true  to indicate that the service should continue API calls
   */
  public boolean percentComplete(double percent_complete);

  /**
   * Exception thrown by the API...
   */
  public void exceptionThrown(Throwable throwable, String query);
}


