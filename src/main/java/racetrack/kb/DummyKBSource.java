/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.kb;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.BundlesDT;

import racetrack.util.Utils;

/**
 * Dummy knowledge source for testing how it all works together.
 */
public class DummyKBSource implements KBSource {
  /**
   * Customized constructor
   *
   *@param wait_time0       wait_time between queries
   *@param exception_rate0  how often a query raises an exception
   */
  public DummyKBSource(long wait_time0, double exception_rate0) {
    this.wait_time      = wait_time0;
    this.exception_rate = exception_rate0;
  }

  /**
   * Wait time between queries
   */
  long wait_time = 0L;

  /**
   * How often an exception occurs
   */
  double exception_rate = 0.0;

  /**
   * Default constructor
   */
  public DummyKBSource() { this(0L, 0.0); }

  /**
   * Return the name of the KB Source
   *
   *@return name of data source
   */
  public String kbSourceName() { return "Dummy KB Source (" + wait_time + "," + exception_rate + ")"; }

  /**
   * Return a prefix to be used with naming tags and headers.
   *
   *@return prefix string for naming
   */
  public String kbPrefix() { return "dumb_" + wait_time + "_" + exception_rate; }

  /**
   * Return the query params that this service requires.  Will be hard-coded strings :(
   *
   * Strings listed next...
   *
   *@return set of query params required
   */
  public Set<String> queryParams() {
    Set<String> query_params = new HashSet<String>();
    query_params.add(MAX_RECS_STR);
    query_params.add(TIMEFRAME_STR);
    query_params.add(REMOVE_NONPUBLIC_IPS_STR);
    query_params.add(PRIVATE_IPS_ONLY_STR);
    return query_params;
  }

  /**
   * Return the a set of data types that this KB source handles.
   *
   *@param set set of data types processed by this data source
   */
  public Set<BundlesDT.DT> handlesDataTypes() {
    Set<BundlesDT.DT> set = new HashSet<BundlesDT.DT>();
    set.add(BundlesDT.DT.IPv4);
    set.add(BundlesDT.DT.DOMAIN);
    return set;
  }

  /**
   * Return an example list of options.
   */
  public List<String> selectableOptions() {
    List<String> list = new ArrayList<String>();
    
    list.add("Provide Only Private IPs");
    list.add("Do Not Handle Domains");
    list.add("Do Not Handle IPv4");

    return list;
  }

  /**
   * Execute a query for indicators.  Return a list of attribute maps -- one
   * per record returned.  Can be heterogeneous.  Should be threaded to avoid
   * blocking...
   *
   *
   * Some conventions for the return maps:
   *
   * timestamp             initial timestamp for the record
   * timestamp_end         end timestamp for the record
   * query                 query string that returned this record
   * source                kb source name
   *
   *
   *@param entities           entities to query
   *@param query_params       source specific query parameters (as strings) -- can be null
   *@param succeeded          entities that were successfully queried
   *@param failed             entities that failed to query
   *@param progress_listener  listener that handles information about the progress of the query -- can be null
   */
  public List<Map<String,String>> query(Set<String> entities, Map<String,String> query_params, Set<String> succeeded, Set<String> failed, KBProgressListener progress_listener) {
    List<Map<String,String>> ret = new ArrayList<Map<String,String>>();

    boolean keep_going = true; int done = 0;

    Iterator<String> it = entities.iterator(); while (it.hasNext() && keep_going) {
      String entity = it.next();
      if (Math.random() < exception_rate) {
        System.err.println("Simulated Exception: " + kbSourceName() + " : " + entity);
        failed.add(entity);
      } else {
        if (Math.random() < 0.7) {
          if        (Utils.isDomain(entity)) {
            int recs = (int) (Math.random() * 3);
            for (int i=0;i<recs;i++) ret.add(randomDomainResolution(entity));
          } else if (Utils.isIPv4(entity))   {
            if (Math.random() < 0.6) { // random netflow record
              int recs = (int) (Math.random() * 50);
              for (int i=0;i<recs;i++) ret.add(randomNetflow(entity));
            } else { // random dns name
              int recs = (int) (Math.random() * 5);
              for (int i=0;i<recs;i++) ret.add(randomDomainResolution(entity));
            }
          }
        }
        succeeded.add(entity);
        done++; // number of entities done
      }

      if (progress_listener != null) { keep_going = progress_listener.percentComplete(((double) done)/entities.size(), entity, ""); }

      // Make a delay
      try { Thread.sleep(wait_time); } catch (InterruptedException ie) { }
    }

    return ret;
  }

  /**
   * Make a random netflow record
   */
  private Map<String,String> randomNetflow(String ip) {
    Map<String,String> map = new HashMap<String,String>();
    map.put("timestamp",     randomTimeStamp());
    map.put("timestamp_end", Utils.exactDate(Utils.parseTimeStamp(map.get("timestamp")) + ((long) (Math.random() * 10000))));

    if (Math.random() < 0.5) { map.put("sip", makeIP()); map.put("dip", ip);       }
    else                     { map.put("sip", ip);       map.put("dip", makeIP()); }

    return map;
  }

  /**
   * Make a random domain resolution record
   */
  private Map<String,String> randomDomainResolution(String entity) {
    Map<String,String> map = new HashMap<String,String>();
    map.put("timestamp", randomTimeStamp());

    if (Utils.isIPv4(entity)) { map.put("domain", makeDomain()); map.put("ip", entity);   }
    else                      { map.put("domain", entity);       map.put("ip", makeIP()); }

    return map;
  }

  /**
   * Return a random timestamp... something recent... last 30 days...
   */
  private String randomTimeStamp() {
    long ts = System.currentTimeMillis() - ((long) (Math.random() * 30 * 24 * 60 * 60 * 1000));
    return Utils.exactDate(ts);
  }

  /**
   * Make a random IP address
   */
  private String makeIP() { return randomOctet() + "." + randomOctet() + "." + randomOctet() + "." + randomOctet(); }
  private int randomOctet() { return (((int) (Math.random() * 10000))%256); }

  /**
   * Make a random domain
   */
  private String makeDomain() { return randomCharString() + "." + randomCharString() + "." + randomTLD(); }
  private String randomCharString() {
    int len = (int) (3 + Math.random() * 4);
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<len;i++) sb.append(randomChar());
    return sb.toString();
  }
  private char randomChar() { return (char) ('a' + ((int) (Math.random() * 1000))%26); }
  private String randomTLD() {
    Double r = Math.random();
    if      (r < 0.5) return "com";
    else if (r < 0.7) return "org";
    else if (r < 0.8) return "net";
    else if (r < 0.9) return "edu";
    else              return "xxx";
  }
}

