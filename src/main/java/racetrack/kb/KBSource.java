/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.kb;

import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.BundlesDT;

/**
 * Knowledge source... for a specific API
 */
public interface KBSource {
  /**
   * Return the name of the KB Source
   *
   *@return name of data source
   */
  public String kbSourceName();

  /**
   * Return a prefix to be used with naming tags and headers.
   *
   *@return prefix string for naming
   */
  public String kbPrefix();

  /**
   * Return the query params that this service requires.  Will be hard-coded strings :(
   *
   * Strings listed next...
   *
   *@return set of query params required
   */
  public Set<String> queryParams();
  
  /**
   * Maximum records returned option
   */
  public static final String MAX_RECS_STR             = "Max Recs",

  /**
   * Timeframe for query option
   */
                             TIMEFRAME_STR            = "Timeframe",

  /**
   * Removal of non-public (i.e., private -- 10.0.0.0/8,etc) prior to calling the query method
   */
                             REMOVE_NONPUBLIC_IPS_STR = "Remove Non-Public IPs",

  /**
   * Only allow private ips
   */
                             PRIVATE_IPS_ONLY_STR     = "Private IPs Only";

  /**
   * Return the a set of data types that this KB source handles.
   *
   *@param set set of data types processed by this data source
   */
  public Set<BundlesDT.DT> handlesDataTypes();

  /**
   * Returns a list of selectable options.  These options can be brought up in a dialog in the specified order.
   * Null return indicates no selectable options.
   *
   *@return selectable options as an ordered list.... null values indicates no selectable options.
   */
  public List<String> selectableOptions();

  /**
   * Execute a query for indicators.  Return a list of attribute maps -- one
   * per record returned.  Can be heterogeneous.  Should be threaded to avoid
   * blocking...
   *
   *
   * Some conventions for the return maps:
   *
   * timestamp             initial timestamp for the record
   * timestamp_end         end timestamp for the record
   * query                 query string that returned this record
   * source                kb source name
   *
   *
   *@param entities           entities to query
   *@param query_params       source specific query parameters (as strings) -- can be null -- for selectable options, these will be set here.
   *@param succeeded          queries that successfully queried
   *@param failed             queries that failed / threw an exception
   *@param progress_listener  listener that handles information about the progress of the query -- can be null
   *
   *@return a list of results -- results are attribute maps -- can be heterogenous
   */
  public List<Map<String,String>> query(Set<String>        entities, 
                                        Map<String,String> query_params, 
                                        Set<String>        succeeded,
                                        Set<String>        failed,
                                        KBProgressListener progress_listener);
}

