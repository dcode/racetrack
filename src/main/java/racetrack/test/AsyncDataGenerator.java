/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.Tablet;

import racetrack.gui.RTControlFrame;

import racetrack.util.Utils;

/**
 * Generates data asynchronously and adds it to the app... stops after 50K records.
 */
public class AsyncDataGenerator implements Runnable {

  /**
   * Control frame to provide data to...
   */
  private RTControlFrame rtcf;

  /**
   *
   */
  public AsyncDataGenerator(RTControlFrame rtcf0) { this.rtcf = rtcf0; }

  /**
   *
   */
  public void run() {
    int records_made = 0; while (records_made < 50000) {
      // Wait for upto 10 seconds
      try { Thread.sleep((long) (Math.random() * 10000L)); } catch (InterruptedException ie) { }

      // Make between 100 and 1100 records
      int recs_to_make = (int) (Math.random() * 1000 + 100);

      // Randomize the type record made
      double rv = Math.random();
      if      (rv < 0.75) createAndAddFlow(recs_to_make);
      else                createAndAddDNS (recs_to_make);

      // Keep track of the records made
      records_made += recs_to_make;
    }
  } 


  /**
   *
   */
  private void createAndAddFlow(int rec_count) {
    List<Map<String,String>> recs = new ArrayList<Map<String,String>>();
    for (int i=0;i<rec_count;i++) {
      String sip  = "192.168." + randOctetClient() + "." + randOctetClient(),
             dip  = "192.168." + randOctetServer() + "." + randOctetServer();
      int    spt  = (int) (10000 + Math.random() * 50000);
      int    pro_and_port[] = randServerPort();
      int    pro            = pro_and_port[0];
      int    dpt            = pro_and_port[1];
      int    OCTS           = (int) (100 + Math.random() * 10000);
      int    PKTS           = OCTS/80;
      long   timeframe      = 30L * 24L * 60L * 60L * 1000L;
      long   ts             = (long) (System.currentTimeMillis() - timeframe + timeframe * Math.random());

      Map<String,String> attr = new HashMap<String,String>();
      attr.put("sip", sip);
      attr.put("dip", dip);
      attr.put("spt", ""+spt);
      attr.put("dpt", ""+dpt);
      attr.put("pro", ""+pro);
      attr.put("OCTS", ""+OCTS);
      attr.put("PKTS", ""+PKTS);
      attr.put("timestamp", Utils.exactDate(ts));

      recs.add(attr);
    }

    genericAddToApp(recs);
  } 

  private int randOctetClient() { return (int) (Math.random() * 8); }
  private int randOctetServer() { return (int) (12 + Math.random() * 14); }

  private int[] randServerPort() {
    int ret[] = new int[2];

    double rv = Math.random();
    if      (rv < 0.5) { ret[0] = 6;  ret[1] = 443; }
    else if (rv < 0.8) { ret[0] = 6;  ret[1] = 80;  }
    else               { ret[0] = 17; ret[1] = 53;  }

    return ret;
  }

  /**
   *
   */
  private void createAndAddDNS(int rec_count) {
    List<Map<String,String>> recs = new ArrayList<Map<String,String>>();
    for (int i=0;i<rec_count;i++) {
      String ip             = "192.168." + randOctetServer() + "." + randOctetServer();
      long   timeframe      = 30L * 24L * 60L * 60L * 1000L;
      long   ts             = (long) (System.currentTimeMillis() - timeframe + timeframe * Math.random());

      String domain         = randomWord() + "." + randomWord() + "." + randomTLD();

      Map<String,String> attr = new HashMap<String,String>();
      attr.put("ip",        ip);
      attr.put("domain",    domain);
      attr.put("timestamp", Utils.exactDate(ts));
      recs.add(attr);
    }

    genericAddToApp(recs);
  }


  String words[] = { "a", "an", "the", "if", "but", "what", "who", "why", "what", "is", "be", 
                     "he", "she", "you", "your", "some", "any", "not", "and", "or", "time", 
                     "www", "server", "client", "internet", "web" };
  private String randomWord(){
    int i = ((int) (Math.random() * 65000))%words.length;
    return words[i];
  }

  private String randomTLD() {
    double rv = Math.random();
    if      (rv < 0.4) return "com";
    else if (rv < 0.6) return "org";
    else if (rv < 0.8) return "edu";
    else               return "net";
  }

  /**
   * Every map in the list should have the exact same fields (keys).
   */
  private void genericAddToApp(List<Map<String,String>> recs) {
    if (recs != null && recs.size() > 0) {
      // Create the header first
      Map<String,String> attr = recs.get(0); String hdr[] = new String[attr.keySet().size()];
      Iterator<String> it = attr.keySet().iterator(); for (int i=0;i<hdr.length;i++) hdr[i] = it.next();

      // Find or create the tablet
      Tablet tablet = rtcf.getRTParent().getRootBundles().findOrCreateTablet(hdr);

      // Add the records
      Set<Bundle> bundles_added = new HashSet<Bundle>();
      for (int i=0;i<recs.size();i++) {
        attr = recs.get(i);

        Map<String,String> attr_copy = new HashMap<String,String>(attr); attr_copy.remove("timestamp"); attr_copy.remove("timestamp_end");

        Bundle bundle;
        if        (attr.containsKey("timestamp") && attr.containsKey("timestamp_end")) { bundle = tablet.addBundle(attr_copy, attr.get("timestamp"), attr.get("timestamp_end"));
        } else if (attr.containsKey("timestamp"))                                      { bundle = tablet.addBundle(attr_copy, attr.get("timestamp"));
        } else                                                                         { bundle = tablet.addBundle(attr_copy); }

        if (bundle != null) bundles_added.add(bundle);
      }

      // Propagate changes across the application
      Set<Bundles> bundles_set = new HashSet<Bundles>(); bundles_set.add(rtcf.getRTParent().getRootBundles());
      rtcf.getRTParent().getRootBundles().getGlobals().cleanse(bundles_set);
      rtcf.getRTParent().updatePanelsForNewBundles(bundles_added);
    }
  }
}


