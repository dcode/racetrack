/* 

Copyright 2018 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.test;

import racetrack.graph.GraphFactory;
import racetrack.graph.GraphLayouts;
import racetrack.graph.GraphUtils;
import racetrack.graph.MyGraph;
import racetrack.graph.UniGraph;

import racetrack.util.Utils;

import java.awt.geom.Point2D;

import java.awt.image.BufferedImage;

import java.io.FileOutputStream;
import java.io.PrintStream;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;

/**
 * Test the listed layouts versus the graphs in the graph factory class.
 *
 *@version 0.1
 */
public class GraphLayoutTests {
  /**
   *
   */
  public static void main(String args[]) {
    try {
      // Create the stats files
      PrintStream stats = new PrintStream(new FileOutputStream("graph_layout_tests.csv"));
      stats.println("graph_type,layout,MILLISECONDS,NODES,EDGES,layout_failed");

      // Iterate over the graph factory types
      Iterator<GraphFactory.Type> it_type = GraphFactory.graphTypeIterator(); while (it_type.hasNext()) {
        // Get the graph type and make one
        GraphFactory.Type type      = it_type.next();
              MyGraph           graph     = GraphFactory.createInstance(type, null);
              int               edges     = GraphUtils.countEdges(graph);

        System.out.println("**\n** GraphFactor.Type = " + type + "\n**\n");

              // Get the layouts
              String            layouts[] = GraphLayouts.getLayoutAlgorithms();

              // Apply each layout to the graph
        for (int i=0;i<layouts.length;i++) {
          System.out.println("  **\n  ** Layout Algorithm = " + layouts[i] + "\n  **\n");

          Map<String,Point2D> world_map = new HashMap<String,Point2D>(); Map<String,Point2D> copy_map = new HashMap<String,Point2D>();
                for (int j=0;j<graph.getNumberOfEntities();j++) {
                  Point2D pt = new Point2D.Double(Math.random(), Math.random());
                  world_map.put(graph.getEntityDescription(j), pt);
                  copy_map.put(graph.getEntityDescription(j), pt);
                }

                long ts0 = System.currentTimeMillis();
          (new GraphLayouts()).executeLayoutAlgorithm(layouts[i], graph, null, world_map, null, null, null);
                long ts1 = System.currentTimeMillis();

                // If the layout failed - it's probably because a selection was not passed in -- try inserting one
          if (mapsEqual(world_map, copy_map)) {
                Set<String> selection = new HashSet<String>(); selection.add(graph.getEntityDescription(0));

                ts0 = System.currentTimeMillis();
          (new GraphLayouts()).executeLayoutAlgorithm(layouts[i], graph, selection, world_map, null, null, null);
                ts1 = System.currentTimeMillis();

                // If it failed again, try adding another selection
                if (mapsEqual(world_map, copy_map)) {
                  selection.add(graph.getEntityDescription(graph.getNumberOfEntities() - 1));

                  ts0 = System.currentTimeMillis();
            (new GraphLayouts()).executeLayoutAlgorithm(layouts[i], graph, selection, world_map, null, null, null);
                  ts1 = System.currentTimeMillis();
                }
              }

              // Render the image and write it to a file
              BufferedImage bi = GraphUtils.render(new UniGraph(graph), world_map);
              String filename = type + "_" + layouts[i] + ".png";
              if (mapsEqual(world_map, copy_map) == false) ImageIO.write(bi, "PNG", new FileOutputStream(filename));

              // Write the stats to a file
              stats.println(Utils.encToURL("" + type)   + "," +
                      Utils.encToURL(layouts[i])  + "," +
                      (ts1 - ts0)                 + "," +
                      graph.getNumberOfEntities() + "," +
                      edges                       + "," +
                      mapsEqual(world_map, copy_map));
              }
      }
      stats.close();
    } catch (Throwable t) {
      System.err.println("Throwable: " + t);
      t.printStackTrace(System.err);
    }
  }

  /**
   * Compare two maps - return true if they are exactly the same and false otherwise.
   *
   *@param map1 first map
   *@param map2 second map
   *
   *@return true if maps are exactly equivalent
   */
  public static boolean mapsEqual(Map<String,Point2D> map1, Map<String,Point2D> map2) {
    Iterator<String> it;

    it = map1.keySet().iterator(); while (it.hasNext()) {
      String s = it.next();
      if (map2.containsKey(s)             == false) return false;
      if (map1.get(s).equals(map2.get(s)) == false) return false;
    }

    it = map2.keySet().iterator(); while (it.hasNext()) {
      String s = it.next();
      if (map1.containsKey(s)             == false) return false;
      if (map1.get(s).equals(map2.get(s)) == false) return false;
    }
    
    return true;
  }
}

