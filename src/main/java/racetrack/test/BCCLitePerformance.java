/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.test;

import java.io.File;

import java.util.ArrayList;
import java.util.Iterator;

import racetrack.framework.Bundle;
import racetrack.framework.Bundles;
import racetrack.framework.BundlesCounterContext;
import racetrack.framework.BCCLite;
import racetrack.framework.BundlesRecs;
import racetrack.framework.BundlesUtils;
import racetrack.framework.KeyMaker;
import racetrack.framework.Tablet;

/**
 * Measures the performance difference between the BCCLite class and the BundlesCounterContext class.
 * - Primary difference is that BCCLite doesn't maintain much state as compared to the full BundlesCounterContext.
 */
public class BCCLitePerformance {
  /**
   *
   */
  public BCCLitePerformance(Bundles bundles) {
    String blanks[] = KeyMaker.blanks(bundles.getGlobals(), false, true, true, false);
    System.err.println("blanks.length = " + blanks.length + " ... total combos = " + blanks.length * blanks.length * blanks.length);

    long t0, t1;

    t0 = System.currentTimeMillis();
    bccLite(bundles, blanks[0], blanks[1], blanks[2]);
    t1 = System.currentTimeMillis();
    printTiming(t0, t1, "BCCLite (Single)");

    t0 = System.currentTimeMillis();
    bundlesCounterContext(bundles, blanks[0], blanks[1], blanks[2]);
    t1 = System.currentTimeMillis();
    printTiming(t0, t1, "BundlesCounterContext (Single)");

    t0 = System.currentTimeMillis();
    for (int bin_i=0;bin_i<blanks.length;bin_i++) {
      for (int count_by_i=0;count_by_i<blanks.length;count_by_i++) {
        for (int color_by_i=0;color_by_i<blanks.length;color_by_i++) {
          bccLite(bundles, blanks[bin_i], blanks[count_by_i], blanks[color_by_i]);
        }
      }
    }
    t1 = System.currentTimeMillis();
    printTiming(t0, t1, "BCCLite");

    t0 = System.currentTimeMillis();
    for (int bin_i=0;bin_i<blanks.length;bin_i++) {
      for (int count_by_i=0;count_by_i<blanks.length;count_by_i++) {
        for (int color_by_i=0;color_by_i<blanks.length;color_by_i++) {
          bundlesCounterContext(bundles, blanks[bin_i], blanks[count_by_i], blanks[color_by_i]);
        }
      }
    }
    t1 = System.currentTimeMillis();
    printTiming(t0, t1, "BundlesCounterContext");
  }

  private void bccLite(Bundles bs, String bin_by, String count_by, String color_by) {
    BCCLite bcc = new BCCLite(bs, count_by, color_by);
    Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
      Tablet   tablet = it_tab.next();
      KeyMaker km = new KeyMaker(tablet, bin_by);
      Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); String keys[] = km.stringKeys(bundle);
        for (int i=0;i<keys.length;i++) bcc.count(bundle, keys[i]);
      }
    }
  }

  private void bundlesCounterContext(Bundles bs, String bin_by, String count_by, String color_by) {
    BundlesCounterContext bcc = new BundlesCounterContext(bs, count_by, color_by);
    Iterator<Tablet> it_tab = bs.tabletIterator(); while (it_tab.hasNext()) {
      Tablet   tablet = it_tab.next();
      KeyMaker km = new KeyMaker(tablet, bin_by);
      Iterator<Bundle> it_bun = tablet.bundleIterator(); while (it_bun.hasNext()) {
        Bundle bundle = it_bun.next(); String keys[] = km.stringKeys(bundle);
        for (int i=0;i<keys.length;i++) bcc.count(bundle, keys[i]);
      }
    }
  }

  /**
   * Load a data file... then pass it to the BCCLitePerformance class.
   */
  public static void main(String args[]) {
    try {
      if (args.length == 0) { System.err.println("Usage:  java BCCLitePerformance rt-csv-file"); System.exit(0); }

      System.err.println("Parsing \"" + args[0] + "\"...");
      long t0 = System.currentTimeMillis();
        Bundles bundles = new BundlesRecs();
        BundlesUtils.parse(bundles, null, new File(args[0]), new ArrayList<String>(), 0);
      long t1 = System.currentTimeMillis();

      printTiming(t0, t1, "Parse \"" + args[0] + "\"");

      new BCCLitePerformance(bundles);

    } catch (Throwable throwable) {
      System.err.println("Throwable: " + throwable);
      throwable.printStackTrace(System.err);
    }

  }

  private static void printTiming(long t0, long t1, String desc) {
    System.err.println("Desc: \"" + desc + "\" (" + (t1 - t0) + " ms) | " + ((t1-t0)/1000) + " seconds");
  }
}

