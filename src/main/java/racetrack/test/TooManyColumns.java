/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/
package racetrack.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Generates files that vary in the number of columns but have the same link node graph and same
 * record count.  Make file with high variability in cells as well...  Used to compare performance 
 * of the application.
 *
 *@author  D. Trimm
 *@version 1.0
 */
public class TooManyColumns {
  public static void main(String args[]) {
    try {
      int RECS      = 40000,  // Number of records to create
          NODES     = 6000,   // Number of nodes to create ... may not be exact since the edges will be used for the file
          EDGES     = 20000,  // Number of edges
          COLS      = 64,     // Number of columns in the large file
          LIMVALUES = 10;     // For the non-var large file, the number of unique values in a column

      PrintStream tmc_small     = new PrintStream(new FileOutputStream("tmc_small.csv")),
                  tmc_large     = new PrintStream(new FileOutputStream("tmc_large.csv")),
                  tmc_large_var = new PrintStream(new FileOutputStream("tmc_large_var.csv"));

      // Create random nodes
      Set<String> nodes = new HashSet<String>();
      while (nodes.size()  < NODES) nodes.add(randomString(10));
      List<String> nodes_ls = new ArrayList<String>(); nodes_ls.addAll(nodes);

      // Create random edges
      Set<String> edges = new HashSet<String>();
      while (edges.size() < EDGES) {
        int node_i = ((int) (Math.random() * nodes_ls.size() * 3))%nodes_ls.size(),
            node_j = ((int) (Math.random() * nodes_ls.size() * 3))%nodes_ls.size();
        if (node_i == node_j) continue;
        edges.add(nodes_ls.get(node_i) + "," + nodes_ls.get(node_j));
      }
      List<String> edges_ls = new ArrayList<String>(); edges_ls.addAll(edges);

      // Create the hdr for the large files and also the variability in the cells
      String hdr[] = new String[COLS]; for (int i=0;i<hdr.length;i++) hdr[i] = randomString(4); // Hopefully no dupes...
      Map<String,List<String>> limvalues = new HashMap<String,List<String>>(); for (int i=0;i<hdr.length;i++) {
        limvalues.put(hdr[i], new ArrayList<String>());
        for (int j=0;j<LIMVALUES;j++) limvalues.get(hdr[i]).add(randomString(8));
      }
      StringBuffer hdr_sb = new StringBuffer(); for (int i=0;i<hdr.length;i++) hdr_sb.append("," + hdr[i]);
      
      // Write the header
      tmc_small.    println("from,to");
      tmc_large.    println("from,to" + hdr_sb.toString());
      tmc_large_var.println("from,to" + hdr_sb.toString());

      // Write the files -- first every edge
      int recs_written = 0;
      for (int i=0;i<edges_ls.size();i++) {
        tmc_small.    println(edges_ls.get(i));

        tmc_large.    print(edges_ls.get(i));
        for (int j=0;j<hdr.length;j++) tmc_large.print("," + randomEntry(limvalues.get(hdr[j])));
        tmc_large.println();

        tmc_large_var.print(edges_ls.get(i));
        for (int j=0;j<hdr.length;j++) tmc_large_var.print("," + randomString(6));
        tmc_large_var.println();

        recs_written++;
      }

      // Then random edges to get to the the necessary record counts
      while (recs_written < RECS) {
        String edge = randomEntry(edges_ls);

        tmc_small.    println(edge);

        tmc_large.    print(edge);
        for (int j=0;j<hdr.length;j++) tmc_large.print("," + randomEntry(limvalues.get(hdr[j])));
        tmc_large.println();

        tmc_large_var.print(edge);
        for (int j=0;j<hdr.length;j++) tmc_large_var.print("," + randomString(6));
        tmc_large_var.println();

        recs_written++;
      }

      tmc_large_var.close();
      tmc_large.close();
      tmc_small.close();
    } catch (IOException ioe) {
      System.err.println("IOException: " + ioe);
    }
  }

  /**
   * Create a random string of the specified number of characters.  Use a-z only to avoid parsing issues.
   */
  public static String randomString(int chars) {
    StringBuffer sb = new StringBuffer();
    for (int i=0;i<chars;i++) {
      char c = ((char) ('a' + ((int) (52*Math.random()))%26));
      sb.append(c);
    }
    return sb.toString();
  }

  /**
   * Return a randomm entry from a list.
   */
  public static String randomEntry(List<String> list) {
    int i = ((int) (Math.random() * list.size() * 3))%list.size();
    return list.get(i);
  }
}

