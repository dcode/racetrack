// simple test to figure out if this script is working...
// document.body.style.backgroundColor="orange" 

/**
 * List of known tlds ... extracted from the following:
 *
 * https://en.wikipedia.org/wiki/List_of_Internet_top-level_domains
 */
tlds = [".OOO", ".aarp", ".abarth", ".abb", ".abbott", ".abbvie", ".abc", ".abogado", ".abudhabi", ".ac", ".academy", ".accenture", ".accountant", ".accountants", ".aco", ".active", ".actor", ".ad", ".ads", ".adult", ".ae", ".aeg", ".aero", ".aetna", ".af", ".afl", ".africa", ".ag", ".agakhan", ".agency", ".ai", ".aig", ".aigo", ".airbus", ".airforce", ".airtel", ".akdn", ".al", ".alfaromeo", ".alibaba", ".alipay", ".allfinanz", ".allstate", ".ally", ".alsace", ".alstom", ".am", ".amazon", ".americanexpress", ".amex", ".amica", ".amsterdam", ".analytics", ".android", ".anz", ".ao", ".aol", ".apartments", ".app", ".apple", ".aq", ".aquarelle", ".ar", ".arab", ".aramco", ".archi", ".army", ".arpa", ".art", ".arte", ".as", ".asia", ".associates", ".at", ".attorney", ".au", ".auction", ".audi", ".audible", ".audio", ".auspost", ".author", ".auto", ".autos", ".aw", ".aws", ".ax", ".axa", ".az", ".azure", ".ba", ".baby", ".baidu", ".bananarepublic", ".band", ".bank", ".bar", ".barcelona", ".barclaycard", ".barclays", ".barefoot", ".bargains", ".baseball", ".basketball", ".bauhaus", ".bayern", ".bb", ".bbc", ".bbt", ".bbva", ".bcg", ".bcn", ".bd", ".be", ".beauty", ".beer", ".bentley", ".berlin", ".best", ".bestbuy", ".bet", ".bf", ".bg", ".bh", ".bharti", ".bi", ".bible", ".bid", ".bike", ".bing", ".bingo", ".bio", ".biz", ".bj", ".black", ".blackfriday", ".blanco", ".blockbuster", ".blog", ".bloomberg", ".blue", ".bm", ".bms", ".bmw", ".bn", ".bnl", ".bnpparibas", ".bo", ".boehringer", ".bom", ".bond", ".boo", ".book", ".booking", ".boots", ".bosch", ".bostik", ".boston", ".bot", ".boutique", ".box", ".bq", ".br", ".bradesco", ".bridgestone", ".broadway", ".broker", ".brother", ".brussels", ".bs", ".bt", ".budapest", ".bugatti", ".build", ".builders", ".business", ".buy", ".buzz", ".bw", ".by", ".bz", ".bzh", ".ca", ".cab", ".cafe", ".cal", ".call", ".calvinklein", ".cam", ".camera", ".camp", ".cancerresearch", ".canon", ".capetown", ".capital", ".capitalone", ".car", ".caravan", ".cards", ".care", ".career", ".careers", ".cars", ".cartier", ".casa", ".case", ".cash", ".casino", ".cat", ".catering", ".catholic", ".cba", ".cbn", ".cbre", ".cbs", ".cc", ".cd", ".center", ".ceo", ".cern", ".cf", ".cfa", ".cfd", ".cg", ".ch", ".chanel", ".channel", ".chase", ".chat", ".cheap", ".chintai", ".christmas", ".chrome", ".chrysler", ".church", ".ci", ".cipriani", ".circle", ".cisco", ".citadel", ".citi", ".citic", ".city", ".ck", ".cl", ".claims", ".cleaning", ".click", ".clinic", ".clinique", ".clothing", ".cloud", ".club", ".clubmed", ".cm", ".cn", ".co", ".coach", ".codes", ".coffee", ".college", ".cologne", ".com", ".comcast", ".commbank", ".community", ".company", ".compare", ".computer", ".condos", ".construction", ".consulting", ".contact", ".contractors", ".cooking", ".cool", ".coop", ".corsica", ".country", ".coupon", ".coupons", ".courses", ".cr", ".credit", ".creditcard", ".creditunion", ".cricket", ".crown", ".crs", ".cruise", ".cruises", ".cu", ".cuisinella", ".cv", ".cw", ".cx", ".cy", ".cymru", ".cz", ".dabur", ".dad", ".dance", ".data", ".date", ".dating", ".datsun", ".day", ".de", ".deal", ".dealer", ".deals", ".degree", ".delivery", ".dell", ".deloitte", ".delta", ".democrat", ".dental", ".dentist", ".desi", ".design", ".dev", ".dhl", ".diamonds", ".diet", ".digital", ".direct", ".directory", ".discount", ".discover", ".dish", ".diy", ".dj", ".dk", ".dm", ".dnp", ".do", ".docs", ".doctor", ".dodge", ".dog", ".doha", ".domains", ".dot", ".download", ".drive", ".dubai", ".duck", ".dunlop", ".dupont", ".durban", ".dvag", ".dz", ".earth", ".eat", ".ec", ".eco", ".edeka", ".edu", ".education", ".ee", ".eg", ".eh", ".email", ".emerck", ".energy", ".eng", ".engineer", ".engineering", ".enterprises", ".epost", ".epson", ".equipment", ".er", ".ericsson", ".erni", ".es", ".esq", ".estate", ".esurance", ".et", ".etisalat", ".eu", ".eurovision", ".eus", ".events", ".everbank", ".example", ".exchange", ".expert", ".exposed", ".express", ".extraspace", ".fage", ".fail", ".fairwinds", ".faith", ".family", ".fan", ".fans", ".farm", ".farmers", ".fashion", ".fast", ".fedex", ".feedback", ".ferrari", ".ferrero", ".fi", ".fiat", ".fidelity", ".film", ".final", ".finance", ".financial", ".fire", ".firestone", ".firmdale", ".fish", ".fishing", ".fit", ".fitness", ".fj", ".fk", ".flickr", ".flights", ".flir", ".florist", ".flowers", ".flsmidth", ".fly", ".fm", ".fo", ".foo", ".food", ".foodnetwork", ".football", ".ford", ".forex", ".forsale", ".forum", ".foundation", ".fox", ".fr", ".free", ".fresenius", ".frl", ".frogans", ".frontdoor", ".frontier", ".fujitsu", ".fujixerox", ".fun", ".fund", ".furniture", ".futbol", ".fyi", ".ga", ".gal", ".gallery", ".gallo", ".gallup", ".game", ".games", ".gap", ".garden", ".gay", ".gbiz", ".gd", ".gdn", ".ge", ".gea", ".gent", ".genting", ".geo", ".gf", ".gg", ".gh", ".gi", ".gift", ".gifts", ".gives", ".giving", ".gl", ".glass", ".gle", ".global", ".globo", ".gm", ".gmail", ".gmbh", ".gmo", ".gmx", ".gn", ".godaddy", ".gold", ".goldpoint", ".golf", ".goodyear", ".goog", ".google", ".gop", ".gov", ".gp", ".gq", ".gr", ".grainger", ".graphics", ".gratis", ".green", ".gripe", ".grocery", ".group", ".gs", ".gt", ".gu", ".guardian", ".gucci", ".guide", ".guitars", ".guru", ".gw", ".gy", ".hair", ".hamburg", ".hangout", ".haus", ".hbo", ".hdfc", ".hdfcbank", ".health", ".healthcare", ".help", ".helsinki", ".here", ".hermes", ".hiphop", ".hisamitsu", ".hitachi", ".hiv", ".hk", ".hkt", ".hm", ".hn", ".hockey", ".holdings", ".holiday", ".homegoods", ".homes", ".homesense", ".honda", ".honeywell", ".horse", ".hospital", ".host", ".hosting", ".hot", ".hoteles", ".hotels", ".hotmail", ".house", ".how", ".hr", ".hsbc", ".ht", ".hu", ".hughes", ".hyatt", ".hyundai", ".ibm", ".ice", ".id", ".ie", ".ieee", ".ifm", ".ikano", ".il", ".im", ".imdb", ".immo", ".immobilien", ".in", ".industries", ".infiniti", ".info", ".ing", ".ink", ".institute", ".insurance", ".insure", ".int", ".intel", ".international", ".intuit", ".invalid", ".investments", ".io", ".ipiranga", ".iq", ".ir", ".irish", ".is", ".iselect", ".ist", ".istanbul", ".it", ".itau", ".itv", ".iveco", ".jaguar", ".java", ".jcb", ".jcp", ".je", ".jeep", ".jetzt", ".jewelry", ".jm", ".jo", ".jobs", ".joburg", ".joy", ".jp", ".jpmorgan", ".juegos", ".juniper", ".kaufen", ".kddi", ".ke", ".kerryhotels", ".kerrylogistics", ".kerryproperties", ".kfh", ".kg", ".kh", ".ki", ".kia", ".kid", ".kids", ".kim", ".kinder", ".kindle", ".kitchen", ".kiwi", ".km", ".kn", ".koeln", ".komatsu", ".kp", ".kpmg", ".kr", ".krd", ".kred", ".kuokgroup", ".kw", ".ky", ".kyoto", ".kz", ".la", ".lacaixa", ".ladbrokes", ".lamborghini", ".lancaster", ".lancia", ".lancome", ".land", ".landrover", ".lanxess", ".lasalle", ".lat", ".latrobe", ".law", ".lawyer", ".lb", ".lc", ".lds", ".lease", ".legal", ".lego", ".lexus", ".lgbt", ".li", ".liaison", ".lidl", ".life", ".lifeinsurance", ".lifestyle", ".lighting", ".like", ".lilly", ".limited", ".limo", ".lincoln", ".linde", ".link", ".lipsy", ".live", ".living", ".lixil", ".lk", ".loan", ".loans", ".local", ".localhost", ".locker", ".locus", ".lol", ".london", ".lotte", ".lotto", ".love", ".lpl", ".lplfinancial", ".lr", ".ls", ".lt", ".ltd", ".ltda", ".lu", ".lundbeck", ".lupin", ".luxe", ".luxury", ".lv", ".ly", ".ma", ".macys", ".madrid", ".maif", ".mail", ".maison", ".makeup", ".man", ".management", ".mango", ".map", ".market", ".marketing", ".markets", ".marriott", ".maserati", ".mattel", ".mba", ".mc", ".mckinsey", ".md", ".me", ".med", ".media", ".meet", ".melbourne", ".meme", ".memorial", ".men", ".menu", ".metlife", ".mg", ".mh", ".miami", ".microsoft", ".mil", ".mini", ".mint", ".mit", ".mitsubishi", ".mk", ".ml", ".mlb", ".mm", ".mma", ".mn", ".mo", ".mobi", ".mobile", ".mobily", ".moda", ".moe", ".moi", ".mom", ".monash", ".money", ".monster", ".mormon", ".mortgage", ".moscow", ".moto", ".motorcycles", ".mov", ".movie", ".movistar", ".mp", ".mq", ".mr", ".ms", ".msd", ".mt", ".mtn", ".mtr", ".mu", ".museum", ".music", ".mutual", ".mv", ".mw", ".mx", ".my", ".mz", ".na", ".nadex", ".nagoya", ".name", ".nationwide", ".nato", ".natura", ".navy", ".nba", ".nc", ".ne", ".nec", ".net", ".netflix", ".network", ".neustar", ".new", ".newholland", ".news", ".nexus", ".nf", ".nfl", ".ng", ".ngo", ".nhk", ".ni", ".nico", ".nike", ".nikon", ".ninja", ".nissan", ".nissay", ".nl", ".no", ".nokia", ".northwesternmutual", ".norton", ".now", ".np", ".nr", ".nra", ".nrw", ".ntt", ".nu", ".nyc", ".nz", ".obi", ".observer", ".off", ".office", ".okinawa", ".om", ".omega", ".one", ".ong", ".onion", ".onl", ".online", ".ooo", ".open", ".oracle", ".orange", ".org", ".organic", ".origins", ".osaka", ".otsuka", ".ovh", ".pa", ".page", ".panasonic", ".paris", ".partners", ".parts", ".party", ".passagens", ".pay", ".pccw", ".pe", ".pet", ".pf", ".pfizer", ".pg", ".ph", ".pharmacy", ".philips", ".phone", ".photo", ".photography", ".photos", ".physio", ".piaget", ".pics", ".pictet", ".pictures", ".pid", ".pin", ".ping", ".pink", ".pioneer", ".pizza", ".pk", ".pl", ".place", ".play", ".playstation", ".plumbing", ".plus", ".pm", ".pn", ".pohl", ".poker", ".politie", ".porn", ".post", ".pr", ".praxi", ".press", ".prime", ".pro", ".prod", ".productions", ".prof", ".progressive", ".promo", ".properties", ".property", ".protection", ".pru", ".prudential", ".ps", ".pt", ".pub", ".pw", ".pwc", ".py", ".qa", ".qpon", ".quebec", ".quest", ".qvc", ".racing", ".radio", ".re", ".read", ".realestate", ".realtor", ".realty", ".recipes", ".red", ".redstone", ".rehab", ".reise", ".reisen", ".reit", ".reliance", ".ren", ".rent", ".rentals", ".repair", ".report", ".republican", ".rest", ".restaurant", ".review", ".reviews", ".rexroth", ".rich", ".ricoh", ".rio", ".rip", ".rmit", ".ro", ".rocher", ".rocks", ".rodeo", ".rogers", ".room", ".rs", ".rsvp", ".ru", ".rugby", ".ruhr", ".run", ".rw", ".rwe", ".ryukyu", ".sa", ".saarland", ".safe", ".safety", ".sakura", ".sale", ".samsung", ".sandvik", ".sandvikcoromant", ".sanofi", ".sap", ".sarl", ".save", ".saxo", ".sb", ".sbi", ".sbs", ".sc", ".sca", ".scb", ".schaeffler", ".schmidt", ".scholarships", ".school", ".schule", ".schwarz", ".science", ".scjohnson", ".scor", ".scot", ".sd", ".se", ".search", ".seat", ".secure", ".security", ".seek", ".select", ".sener", ".services", ".ses", ".seven", ".sew", ".sex", ".sexy", ".sfr", ".sg", ".sh", ".shangrila", ".sharp", ".shaw", ".shell", ".shiksha", ".shoes", ".shop", ".shopping", ".shouji", ".show", ".showtime", ".shriram", ".si", ".sic", ".silk", ".sina", ".singles", ".site", ".sk", ".ski", ".skin", ".sky", ".skype", ".sl", ".sling", ".sm", ".smart", ".smile", ".sn", ".sncf", ".so", ".soccer", ".social", ".softbank", ".software", ".sohu", ".solar", ".solutions", ".song", ".sony", ".soy", ".space", ".spiegel", ".spot", ".spreadbetting", ".sr", ".ss", ".st", ".stada", ".staples", ".star", ".starhub", ".statebank", ".statefarm", ".statoil", ".stc", ".stcgroup", ".stockholm", ".storage", ".store", ".stream", ".studio", ".study", ".style", ".su", ".sucks", ".supplies", ".supply", ".support", ".surf", ".surgery", ".suzuki", ".sv", ".swatch", ".swiftcover", ".swiss", ".sx", ".sy", ".sydney", ".symantec", ".systems", ".sz", ".taipei", ".talk", ".taobao", ".target", ".tatamotors", ".tatar", ".tattoo", ".tax", ".taxi", ".tc", ".td", ".tdk", ".team", ".tech", ".technology", ".tel", ".telecity", ".telefonica", ".temasek", ".tennis", ".test", ".teva", ".tf", ".tg", ".th", ".theater", ".theatre", ".tickets", ".tienda", ".tiffany", ".tips", ".tires", ".tirol", ".tj", ".tjx", ".tk", ".tl", ".tm", ".tn", ".to", ".today", ".tokyo", ".tools", ".top", ".toray", ".toshiba", ".total", ".tours", ".town", ".toyota", ".toys", ".tp", ".tr", ".trade", ".trading", ".training", ".travel", ".travelchannel", ".travelers", ".travelersinsurance", ".trust", ".tt", ".tube", ".tui", ".tunes", ".tushu", ".tv", ".tvs", ".tw", ".tz", ".ua", ".ubs", ".uconnect", ".ug", ".uk", ".unicom", ".university", ".uno", ".uol", ".ups", ".us", ".uy", ".uz", ".va", ".vacations", ".vanguard", ".vc", ".ve", ".vegas", ".ventures", ".verisign", ".vermögensberater", ".vermögensberatung", ".versicherung", ".vet", ".vg", ".vi", ".viajes", ".video", ".vig", ".viking", ".villas", ".vip", ".virgin", ".visa", ".vision", ".vista", ".vistaprint", ".vivo", ".vlaanderen", ".vn", ".vodka", ".volkswagen", ".volvo", ".vote", ".voting", ".voto", ".voyage", ".vu", ".vuelos", ".wales", ".walmart", ".walter", ".wang", ".wanggou", ".watch", ".watches", ".weather", ".weatherchannel", ".web", ".webcam", ".weber", ".website", ".wed", ".wedding", ".weibo", ".weir", ".wf", ".whoswho", ".wien", ".wiki", ".williamhill", ".win", ".windows", ".wine", ".winners", ".wme", ".wolterskluwer", ".woodside", ".work", ".works", ".world", ".wow", ".ws", ".wtc", ".wtf", ".xbox", ".xerox", ".xfinity", ".xihuan", ".xin", ".xxx", ".xyz", ".yachts", ".yahoo", ".yamaxun", ".yandex", ".ye", ".yodobashi", ".yoga", ".yokohama", ".you", ".youtube", ".yt", ".za", ".zappos", ".zara", ".zero", ".zip", ".zippo", ".zm", ".zone", ".zuerich", ".zw", ".ελ", ".ευ", ".бг", ".бел", ".дети", ".ею", ".католик", ".ком", ".мкд", ".мон", ".москва", ".онлайн", ".орг", ".рус", ".рф", ".сайт", ".срб", ".укр", ".қаз", ".հայ", ".ישראל", ".קום", ".ابوظبي", ".اتصالات", ".ارامكو", ".الاردن", ".البحرين", ".الجزائر", ".السعودية", ".المغرب", ".اليمن", ".امارات", ".ایران", ".بارت", ".بازار", ".بيتك", ".بھارت", ".تونس", ".سودان", ".سورية", ".شبكة", ".عراق", ".عرب", ".عمان", ".فلسطين", ".قطر", ".كاثوليك", ".كوم", ".مصر", ".مليسيا", ".موبايلي", ".موريتانيا", ".موقع", ".پاکستان", ".ڀارت", ".कॉम", ".नेट", ".भारत", ".भारतम्", ".भारोत", ".संगठन", ".বাংলা", ".ভারত", ".ভাৰত", ".ਭਾਰਤ", ".ભારત", ".ଭାରତ", ".இந்தியா", ".இலங்கை", ".சிங்கப்பூர்", ".భారత్", ".ಭಾರತ", ".ഭാരതം", ".ලංකා", ".คอม", ".ไทย", ".ລາວ", ".გე", ".みんな", ".クラウド", ".グーグル", ".コム", ".ストア", ".セール", ".ファッション", ".ポイント", ".世界", ".中信", ".中国", ".中國", ".中文网", ".佛山", ".八卦", ".公司", ".公益", ".台湾", ".台灣", ".商城", ".商标", ".嘉里", ".嘉里大酒店", ".在线", ".大众汽车", ".工行", ".广东", ".慈善", ".我爱你", ".新加坡", ".机构", ".淡马锡", ".澳門", ".澳门", ".移动", ".网址", ".网站", ".网络", ".联通", ".诺基亚", ".谷歌", ".集团", ".電訊盈科", ".飞利浦", ".香格里拉", ".香港", ".닷넷", ".닷컴", ".삼성", ".한국"];

tlds_set = new Set(); for (i=0;i<tlds.length;i++) tlds_set.add(tlds[i]);


/**
 * Remove brackets from an obfuscated IPv4 address string.
 */
function removeBrackets(str) { s = ""; for (i=0;i<str.length;i++) { if (str[i] == '[' || str[i] == ']') { } else s += str[i]; } return s; }

/**
 * Extract IPv4 Addresses from text.
 *
 * Derived from:  https://stackoverflow.com/questions/4460586/javascript-regular-expression-to-check-for-ip-addresses
 */
function extractIPv4Indicators(text) {
  ipv4_set = new Set();
  ipv4_regex     = RegExp("(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)", "g");
  while ((array1 = ipv4_regex.exec(text))  !== null) { 
    // Check for CIDR's...
    if (text[ipv4_regex.lastIndex] != '/') { ipv4_set.add(array1[0]); }
  }
  ipv4_regex2    = RegExp("(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\[[.]\\](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)", "g");
  while ((array1 = ipv4_regex2.exec(text)) !== null) { ipv4_set.add(removeBrackets(array1[0])); }
  ipv4_regex3    = RegExp("(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\[[.]\\](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\[[.]\\](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\[[.]\\](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)", "g");
  while ((array1 = ipv4_regex3.exec(text)) !== null) { ipv4_set.add(removeBrackets(array1[0])); }
  return ipv4_set;
}

/**
 * Extract IPv4 CIDRs
 */
function extractIPv4CIDRs(text) {
  cidr_set = new Set();
  cidr_regex     = RegExp("(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[.](25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)[/](\\d){1,2}[^\\d]", "g");
  while ((array1 = cidr_regex.exec(text))  !== null) { 
    str = array1[0];
    str = str.substring(0,str.length-1); // Remove the extra character...
    cidr_set.add(str);
  }
  return cidr_set;
}

function tldOfFQDN(s) { return s.substring(s.lastIndexOf("."),s.length); }

/**
 * FQDN Indicators ... limit to known TLD's...
 * 
 * Derived From:  https://stackoverflow.com/questions/54814665/javascript-regular-expression-for-matching-domain-names-fqdn
 */
function extractFQDNIndicators(text) {
  fqdn_set   = new Set();

  // fqdn_regex1 = RegExp("(([\w\-]+|\*)\.)*([\w\-]+)\.(\w+)(\:(\d+))?"); // without the escaped backslashes...
  fqdn_regex1 = RegExp("(([\\w\\-]+|\\*)[.])*([\\w\\-]+)[.](\\w+)", "g");
  while ((array1 = fqdn_regex1.exec(text)) != null) { 
    tld = tldOfFQDN(array1[0]);
    if (tlds_set.has(tld)) fqdn_set.add(array1[0]); 
  }

  return fqdn_set;
}

/**
 * Union two sets together.
 *
 * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
 */
function setsUnion(setA, setB) {
    let _union = new Set(setA)
    for (let elem of setB) { _union.add(elem) }
    return _union
}

/**
 * Determine if two sets are equal.
 */
function setsEqual(setA, setB) {
  if (setA.size != setB.size) return false;
  for (let elem of setB) {
    if (setA.has(elem) == false) return false;
  }
  return true;
}

/**
 * Extract indicators from a text element.
 */
function extractIndicators(text) {
  indicators_set = new Set();
  indicators_set = setsUnion(indicators_set, extractIPv4Indicators(text));
  indicators_set = setsUnion(indicators_set, extractIPv4CIDRs(text));
  indicators_set = setsUnion(indicators_set, extractFQDNIndicators(text));
  return indicators_set;
}

/**
 * Keep track of the last indicator set sent to the host application
 */
var last_indicators_set = new Set();

/**
 * Extract indicators visible in the window.  If the force_send flag is true,
 * send the indicators even if they were already sent before.
 */
function extractVisibleIndicators(event_type, force_send) {
  var leaves = new Array(); // Leaves aren't sufficient...
  var all    = new Array();
  var els = document.body.getElementsByTagName("*");
  for (var i = 0; i < els.length; i++) {
    if (els[i].children.length === 0) { leaves.push(els[i]); }
    all.push(els[i]);
  }

  indicators_set = new Set();

  all.forEach(a => {
    domRect = a.getBoundingClientRect();
    if ((domRect.height*1.5) > window.innerHeight ||
        (domRect.top >= window.innerHeight-30 || domRect.bottom <= 30)) {
      // Do Nothing ... element is either too big or not within the viewport
    } else {
      indicators_set = setsUnion(indicators_set, extractIndicators(a.innerText));
    }
  } );

  // As long as it's not the same as the last indicator set, tell the host application
  if (force_send || setsEqual(last_indicators_set, indicators_set) == false) {
    last_indicators_set = indicators_set;
    chrome.runtime.sendMessage({event:           event_type,
                                url:             document.URL,
                                title:           document.title,
                                indicators:      Array.from(indicators_set)}, function(response) { });
  }
}

/**
 * Add the on scroll listener
 */
window.onscroll = function() {myOnScrollListener()};

/**
 * On Scroll Event -- attempt to figure out the visible elements and then extract the
 * indicators from them...  If the indicator set changed from the last time, tell the
 * host application.
 */
function myOnScrollListener() {
  extractVisibleIndicators("page scroll event", false);
}

/**
 * On Page Focus
 */
document.addEventListener("visibilitychange", function() {
  if (document.hidden) {
    chrome.runtime.sendMessage({event:           'document hidden',
                                url:             document.URL,
                                title:           document.title,
                                indicators:      Array.from()}, function(response) { });
  } else {
    chrome.runtime.sendMessage({event:           'document shown',
                                url:             document.URL,
                                title:           document.title,
                                indicators:      Array.from(extractIndicators(document.body.innerText))}, function(response) { });
    extractVisibleIndicators("document shown visible indicators", true);
  }
} );

/**
 * On page load 
 */
chrome.runtime.sendMessage({event:           'page info',
                            url:             document.URL,
                            title:           document.title,
                            indicators:      Array.from(extractIndicators(document.body.innerText))}, function(response) { });

