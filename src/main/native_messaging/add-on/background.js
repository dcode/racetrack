/**
 * On startup, connect to the "racetrack" app.
 */
var port = browser.runtime.connectNative("racetrack_integration");

/**
 * Listen for messages from the app ... nothing here yet... just a debug statement
 */
port.onMessage.addListener((response) => {
  console.log("Received: " + response);
});

/**
 * browser action icon...  embedded the actions into the popup context menu...
 * ... but this could be used for something else
 */
browser.browserAction.onClicked.addListener(() => {
  // console.log("Sending: icon clicked");
  // port.postMessage({ event: "icon clicked"});
});

/**
 * Captures tab switches
 *
 * Derived from https://stackoverflow.com/questions/46079716/firefox-webextension-api-how-to-get-the-url-of-the-active-tab
 *
 * onUpdated ... works on tab switches and page loads
 */
chrome.tabs.onUpdated.addListener(() => {
  // Gets the URL in the active tab
  function logTabs(tabs) {
    let tab = tabs[0]; // Safe to assume there will only be one result
    port.postMessage({ event:     "active tab", 
                       url:       tab.url } );
  }
  //  title: tab.document.title,
  browser.tabs.query({currentWindow: true, active: true}).then(logTabs, console.error);
});

/**
 * content_script_listener
 *
 * Derived from the following:
 *
 * https://developer.chrome.com/extensions/messaging
 */
chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    port.postMessage(request);
  } );

/**
 * Context Menus
 *
 * Derived from the following:
 *
 * https://medium.com/@joaoguedes.ishida/send-data-from-a-firefox-web-extension-to-a-python-script-and-create-a-simple-playlisting-app-for-a9436ac84624
 */

chrome.contextMenus.create({
  id:"harvest_indicators",
  type:"normal",
  title:"Harvest Indicators",
  contexts:["all"]
});

chrome.contextMenus.create({
  id:"harvest_visible_indicators",
  type:"normal",
  title:"Harvest Visible Indicators",
  contexts:["all"]
});

chrome.contextMenus.create({
  id:"select_all_indicators",
  type:"normal",
  title:"Select Indicators",
  contexts:["all"]
});

chrome.contextMenus.create({
  id:"select_visible_indicators",
  type:"normal",
  title:"Select Visible Indicators",
  contexts:["all"]
});

/**
 * Upon clicks, do the following actions
 */
chrome.contextMenus.onClicked.addListener((info,tab) => {
  if        (info.menuItemId == 'harvest_indicators')         {
    port.postMessage({ event: "harvest indicators" });
  } else if (info.menuItemId == 'harvest_visible_indicators') {
    port.postMessage({ event: "harvest visible indicators" });
  } else if (info.menuItemId == 'select_all_indicators')      {
    port.postMessage({ event: "select all indicators" });
  } else if (info.menuItemId == 'select_visible_indicators')  {
    port.postMessage({ event: "select visible indicators" });
  }
} );

