In the app folder:

- Copy the .sh (linux) or the .bat (windows) somewhere appropriate
- Create the racetrack.jar file -- make sure it works
- Modify the .sh (linux) or the .bat (windows) to point to the .jar file... make sure that the java executable path is also set
- Modify the racetrack_integration.json file to reflect the correct path to the racetrack script (either .sh or .bat file)
- (Linux) copy the racetrack_integration.json file into the ~/.mozilla/native-messaging-hosts director
- (Windows)

In the add-on folder:

- The manifest.json file may require modifications to fix the path variables for the icons.

(Windows)

From:  https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging

-  HKEY_CURRENT_USER\Software\Mozilla\NativeMessagingHosts\racetrack_integration
   The default value for this key should be the path to the application manifest: ex. C:\Users\<myusername>\racetrack\native-messaging\app\racetrack-integration.json

-  HKEY_LOCAL_MACHINE\Software\Mozilla\NativeMessagingHosts\racetrack_integration
   Idem, the default value for this key should be the path to the application manifest.

(Linux)

- In firefox, type "about:debugging" into the URL field.  Then click on "This Firefox" in the setup section.  From there, load the manifest.json file from the racetrack native_messaging add_on folder.

