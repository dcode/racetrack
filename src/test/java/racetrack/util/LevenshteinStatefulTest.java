/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Class to test out the Utils.java
 */
public class LevenshteinStatefulTest {
  @Test
  public void levelschteinStateful() {
    assertEquals((new LevenshteinStateful("abc",         "abc")).editDistance(),         0);
    assertEquals((new LevenshteinStateful("abc",         "def")).editDistance(),         3);
    assertEquals((new LevenshteinStateful("aBc",         "abc")).editDistance(),         1);
    assertEquals((new LevenshteinStateful("kitten",      "sitting")).editDistance(),     3);
    assertEquals((new LevenshteinStateful("san francis", "san Francis")).editDistance(), 1);
  }
}
