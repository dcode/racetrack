/* 

Copyright 2020 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.util;

import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Class to test out the Utils.java
 */
public class NormalizeBroLogsTest {
  @Test
  public void separateTests() {
    NormalizeBroLogs normalizer = new NormalizeBroLogs();

    for (int i=0;i<3;i++) {
      char sep = ',', non_sep = '\t';
      if      (i == 1) { sep = '|';   non_sep = ','; }
      else if (i == 2) { sep = '\t';  non_sep = ','; }

      for (int j=0;j<50;j++) {
        int token_count = randInt();
        String tokens[] = new String[token_count];
        for (int k=0;k<tokens.length;k++) {
          tokens[k] = randomToken(sep,non_sep);
        }
        StringBuffer sb = new StringBuffer();
        for (int k=0;k<tokens.length;k++) {
          if (k != 0) sb.append(sep);
          sb.append(tokens[k]);
        }

        List<String> list = normalizer.separate(sb.toString(),sep);
        assertEquals(list.size(), tokens.length);
        for (int k=0;k<list.size();k++) assertEquals(list.get(k),tokens[k]);
      }
    }
  }
  public int    randInt()  { return (int) (1 + Math.random() * 6); }
  public String randomToken(char sep, char non_sep) {
    double r = Math.random();
    if      (r < 0.25) return "";
    else if (r < 0.40) return "\\\\" + sep;
    else if (r < 0.50) return ""     + non_sep;
    else if (r < 0.60) return "\\\\" + non_sep;
    else if (r < 0.70) return "\\\\" + sep + randChar() + randChar() + randChar();
    else if (r < 0.75) return                randChar() + randChar() + randChar() + "\\\\" + sep;
    else if (r < 0.80) return "\\\\" + sep + randChar() + randChar() + "\\\\" + sep + randChar() + randChar() + "\\\\" + sep;
    else               return "" + randChar() + randChar() + randChar() + randChar() + randChar();
  }
  public char   randChar() { 
    int  r = (int) (Math.random() * 100000);
    char c = (char) ('a' + (r%26));
    return c;
  }
}

