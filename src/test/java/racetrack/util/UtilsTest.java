/* 

Copyright 2021 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Class to test out the Utils.java
 */
public class UtilsTest {
  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * IPv4 Conversions
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void parseIPv4Tests() {
    assertEquals(Utils.strToIPInt("1.2.3.4"),         0x01020304);
    assertEquals(Utils.strToIPInt("255.255.255.255"), 0xffffffff);
    assertEquals(Utils.strToIPInt("0.0.0.0"),         0x00000000);

    String ip;
    ip = "1.2.3.4";         assertEquals(Utils.strToIPInt(ip),Utils.strToIPInt(Utils.intToIPString(Utils.strToIPInt(ip))));
    ip = "127.0.0.1";       assertEquals(Utils.strToIPInt(ip),Utils.strToIPInt(Utils.intToIPString(Utils.strToIPInt(ip))));
    ip = "255.255.255.255"; assertEquals(Utils.strToIPInt(ip),Utils.strToIPInt(Utils.intToIPString(Utils.strToIPInt(ip))));
    ip = "0.0.0.0";         assertEquals(Utils.strToIPInt(ip),Utils.strToIPInt(Utils.intToIPString(Utils.strToIPInt(ip))));
    ip = "0.0.0.5";         assertEquals(Utils.strToIPInt(ip),Utils.strToIPInt(Utils.intToIPString(Utils.strToIPInt(ip))));
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * IPv4 CIDR Conversions
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void cidrTests() {
    assertEquals(Utils.cidrMask("192.168.0.0/16"), 0xffff0000);
    assertEquals(Utils.cidrMask("1.2.3.4/22"),     0xfffffc00);
    assertEquals(Utils.cidrMask("10.0.0.0/8"),     0xff000000);

    assertEquals(Utils.cidrBits("192.168.0.0/16"), Utils.strToIPInt("192.168.0.0"));
    assertEquals(Utils.cidrBits("192.168.1.0/16"), Utils.strToIPInt("192.168.0.0"));
    assertEquals(Utils.cidrBits("192.168.2.3/16"), Utils.strToIPInt("192.168.0.0"));

    assertTrue(Utils.cidrMatch(0x88880000, 0xffff0000, 0x88880000));

    assertTrue (Utils.ipMatchesCIDR("192.168.10.20", "192.168.0.0/16"));
    assertFalse(Utils.ipMatchesCIDR("192.168.10.20", "192.167.0.0/16"));
    assertTrue (Utils.ipMatchesCIDR("10.1.2.3",      "10.0.0.0/8"));
    assertFalse(Utils.ipMatchesCIDR("11.1.2.3",      "10.0.0.0/8"));

    for (int i=0;i<256;i++) assertTrue(Utils.ipMatchesCIDR(i + ".1.2.3", i + ".0.0.0/8"));

    assertTrue (Utils.isIPv4CIDR("10.0.0.0/8"));
    assertTrue (Utils.isIPv4CIDR("192.168.0.0/16"));
    assertTrue (Utils.isIPv4CIDR("1.2.3.0/24"));

    assertFalse(Utils.isIPv4CIDR("192.168.0.0/34"));
    assertFalse(Utils.isIPv4CIDR("www.example.com.5/24"));
  }

  @Test
  public void cidrFormattingTests() {
    assertEquals(Utils.ipv4CIDR08("1.2.3.4"), "1.0.0.0");
    assertEquals(Utils.ipv4CIDR16("1.2.3.4"), "1.2.0.0");
    assertEquals(Utils.ipv4CIDR24("1.2.3.4"), "1.2.3.0");
  }

  @Test
  public void ipv4OctFormattingTests() {
    assertEquals(Utils.ipv4Octet00("1.2.3.4"), "1");
    assertEquals(Utils.ipv4Octet01("1.2.3.4"), "2");
    assertEquals(Utils.ipv4Octet02("1.2.3.4"), "3");
    assertEquals(Utils.ipv4Octet03("1.2.3.4"), "4");
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Domain Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void domainTLDTest() {
    assertEquals(Utils.domainTLD("cnn.com"),                      "com");
    assertEquals(Utils.domainTLD("www.wikipedia.org"),            "org");
    assertEquals(Utils.domainTLD("much.longer.string.goes.here"), "here");
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * MD5 Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void isMD5Tests() {
    assertTrue(Utils.isMD5("16646e51825e6af404ddbd7ca5a606f0"));
    assertTrue(Utils.isMD5("16646E51825e6af404ddbd7ca5a606F0"));
    assertTrue(Utils.isMD5("a6646e51825e6af404ddbd7ca5a606ff"));

    assertFalse(Utils.isMD5("a6646e51825e6af404ddbd7ca5a606f"));
    assertFalse(Utils.isMD5("g6646e51825e6af404ddbd7ca5a606ff"));
    assertFalse(Utils.isMD5("1a6646e51825e6af404ddbd7ca5a606ff"));
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Hex String Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void isHexStringTests() {
    assertTrue(Utils.isHexString("00"));
    assertTrue(Utils.isHexString("  00"));
    assertTrue(Utils.isHexString("00  "));
    assertTrue(Utils.isHexString("00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff"));
    assertTrue(Utils.isHexString("Aa Bb Cc Dd Ee Ff"));
    assertTrue(Utils.isHexString("00  11    22     33     44       55    66 77 88 99 Aa Bb Cc Dd Ee Ff    "));
    assertTrue(Utils.isHexString("      00  11    22     33     44       55    66 77 88 99 Aa Bb Cc Dd Ee Ff    "));
    assertTrue(Utils.isHexString("00  11    22     33     44       55    66 77 88 99 Aa Bb Cc Dd Ee Ff"));
    assertTrue(Utils.isHexString("00 01 02 03 04 05 06 07 07 08 09 0a 0b 0c 0d 0e 0f"));
    assertTrue(Utils.isHexString("00 12 13\r\n00 11 22 33\r\n00 22 33 44 55"));

    assertFalse(Utils.isHexString("zz"));
    assertFalse(Utils.isHexString("00 0"));
    assertFalse(Utils.isHexString("000"));
    assertFalse(Utils.isHexString("aaa"));
    assertFalse(Utils.isHexString("aaaa"));
    assertFalse(Utils.isHexString("ffff"));
    assertFalse(Utils.isHexString("ffff aa bb cc"));
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Email Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */

   /**
    * Valid email addresses from wikipedia article on acceptable email characters
    * source:  https://en.wikipedia.org/wiki/Email_address#Local-part
    */
   @Test
   public void isEmailTests() {
      String strs[] = { "prettyandsimple@example.com",
                        "very.common@example.com",
                        "disposable.style.email.with+symbol@example.com",
                        "other.email-with-dash@example.com",
                        "x@example.com",
                        "x.y@example.com",
                        "x..y@example.com",
                        "x.........y......z@example.com",
                        "\"much.more_unusual\"@example.com",
                        // "\"very.unusual.@.unusual.com\"@example.com",
                        // "\"very.(),:;<>[]\\\".VERY.\\\"very@\\\\ \\\"very\\\".unusual\"@strange.example.com",
                        "example-indeed@strange-example.com",
                        "admin@mailserver1",
                        // "#!$%&'*+-/=?^_`{}|~@example.org",
                        // "\"()<>[]:,;@\\\\\\\"!#$%&'-/=?^_`{}| ~.a\"@example.org",
                        // "\" \"@example.org",
                        "example@localhost",
                        "example@s.solutions",
                        "user@localserver",
                        "user@tt"
                        // "user@[IPv6:2001:DB8::1]" 
			};
    for (int i=0;i<strs.length;i++) {
      // System.err.println(strs[i]);
      assertTrue(Utils.isEmail(strs[i]));
    }
  }

  @Test
  public void emailDomainTests() {
    assertEquals(Utils.emailDomain("example@s.solutions"), "s.solutions");
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Domain Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void isDomainTests() {
    assertTrue(Utils.isDomain("example.com"));
    assertTrue(Utils.isDomain("more.example.com"));
    assertTrue(Utils.isDomain("lots.more.example.com"));
    assertTrue(Utils.isDomain("192.168.xxx.xxx"));

    assertTrue(Utils.isDomain("some-dash-example.com"));
    assertTrue(Utils.isDomain("some_underscore_example.com"));
    assertTrue(Utils.isDomain("mix-of-dashes.some_underscore_example.com"));

    assertTrue(Utils.isDomain("news.co.uk"));
    assertTrue(Utils.isDomain("news.co.ca"));

    assertFalse(Utils.isDomain("com"));
    assertFalse(Utils.isDomain("1.2.3.4"));
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * URL Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void isURLTests() {
    String str = "http://www.abcdef.com/this/is/a/test/page.html";
    assertTrue  (Utils.isURL(str));
    assertEquals(Utils.urlDomain(str), "www.abcdef.com");
    assertEquals(Utils.urlPage(str),   "page.html");
    assertEquals(Utils.urlPath(str),   "/this/is/a/test");
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * UserAgent Methods
   * =======================================================================================================================
   * =======================================================================================================================
   */
  // abc

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Timestamp Tests
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void parseTimeStampGranularity() {
    assertEquals(Utils.parseTimeStamp("2017-12-12 13"),    Utils.parseTimeStamp("2017-12-12 13:00"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13"),    Utils.parseTimeStamp("2017-12-12 13:00:00"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13"),    Utils.parseTimeStamp("2017-12-12 13:00:00.0"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13"),    Utils.parseTimeStamp("2017-12-12 13:00:00.00"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13"),    Utils.parseTimeStamp("2017-12-12 13:00:00.000"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13:00"), Utils.parseTimeStamp("2017-12-12 13:00:00"));

    assertEquals(Utils.parseTimeStamp("2017-12-12 13:59"), Utils.parseTimeStamp("2017-12-12 13:59:00"));
    assertEquals(Utils.parseTimeStamp("2017-12-12 13:59"), Utils.parseTimeStamp("2017-12-12 13:59:00.0"));
  }

  @Test
  public void parseTimeStampCompareMillis() {
    assertEquals(Utils.parseTimeStamp("2017-01-02 12:34:24"), Utils.parseTimeStamp("2017-01-02 12:34:24.0"));
    assertEquals(Utils.parseTimeStamp("2017-01-02 12:34:24"), Utils.parseTimeStamp("2017-01-02 12:34:24.00"));
    assertEquals(Utils.parseTimeStamp("2017-01-02 12:34:24"), Utils.parseTimeStamp("2017-01-02 12:34:24.000"));
    assertEquals(Utils.parseTimeStamp("2017-01-02 12:34:24"), Utils.parseTimeStamp("2017-01-02 12:34:24.0000"));
    assertEquals(Utils.parseTimeStamp("2017-01-02 12:34:24"), Utils.parseTimeStamp("2017-01-02 12:34:24.0006123812"));

    assertEquals(Utils.parseTimeStamp("2010-11-22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.1"));
    assertEquals(Utils.parseTimeStamp("2010-11-22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.10"));
    assertEquals(Utils.parseTimeStamp("2010-11-22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.100"));
    assertEquals(Utils.parseTimeStamp("2010-11-22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.10067"));
    assertEquals(Utils.parseTimeStamp("2010-11-22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.1008912"));
  }

  @Test
  public void parseTimeStampDelims() {
    assertEquals(Utils.parseTimeStamp("2010/11/22 01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.100"));
    assertEquals(Utils.parseTimeStamp("2010/11/22T01:34:24.1"), Utils.parseTimeStamp("2010-11-22 01:34:24.100"));
    assertEquals(Utils.parseTimeStamp("2010/11/22T01:34:24.1"), Utils.parseTimeStamp("2010-11-22T01:34:24.100"));
  }

  @Test
  public void parseTimeStampFormats() {
    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("20140506 123223"));
    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("20140506 123223Z"));
    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("Tue, May 6 12:32:23 2014"));
    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("Tue May 6 12:32:23 2014"));
    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("Tue May 06 12:32:23 2014"));

    assertEquals(Utils.parseTimeStamp("2014-05-06 12:32:23"), Utils.parseTimeStamp("Tue, May 06 12:32:23 2014"));
    assertEquals(Utils.parseTimeStamp("Tue, May 06 12:32:23 2014"), Utils.parseTimeStamp("tue, may 06 12:32:23 2014"));
    assertEquals(Utils.parseTimeStamp("Tue, May 06 12:32:23 2014"), Utils.parseTimeStamp("Tue, may 06 12:32:23 2014"));
    assertEquals(Utils.parseTimeStamp("Tue, May 06 12:32:23 2014"), Utils.parseTimeStamp("tue, May 06 12:32:23 2014"));

    assertEquals(Utils.parseTimeStamp("Sun, Jan 01 00:00:00 2017"), Utils.parseTimeStamp("2017-01-01"));
    assertEquals(Utils.parseTimeStamp("Thu, Feb 02 00:00:00 2017"), Utils.parseTimeStamp("2017-02-02"));
    assertEquals(Utils.parseTimeStamp("Wed, Mar 01 00:00:00 2017"), Utils.parseTimeStamp("2017-03-01"));
    assertEquals(Utils.parseTimeStamp("Sat, Apr 01 00:00:00 2017"), Utils.parseTimeStamp("2017-04-01"));
    assertEquals(Utils.parseTimeStamp("Mon, May 01 00:00:00 2017"), Utils.parseTimeStamp("2017-05-01"));
    assertEquals(Utils.parseTimeStamp("Thu, Jun 01 00:00:00 2017"), Utils.parseTimeStamp("2017-06-01"));
    assertEquals(Utils.parseTimeStamp("Tue, Jul 04 00:00:00 2017"), Utils.parseTimeStamp("2017-07-04"));
    assertEquals(Utils.parseTimeStamp("Fri, Aug 04 00:00:00 2017"), Utils.parseTimeStamp("2017-08-04"));
    assertEquals(Utils.parseTimeStamp("Fri, Sep 01 00:00:00 2017"), Utils.parseTimeStamp("2017-09-01"));
    assertEquals(Utils.parseTimeStamp("Sun, Oct 01 00:00:00 2017"), Utils.parseTimeStamp("2017-10-01"));
    assertEquals(Utils.parseTimeStamp("Wed, Nov 01 00:00:00 2017"), Utils.parseTimeStamp("2017-11-01"));
    assertEquals(Utils.parseTimeStamp("Fri, Dec 01 00:00:00 2017"), Utils.parseTimeStamp("2017-12-01"));

    assertEquals(Utils.parseTimeStamp("sun, Jan 01 00:00:00 2017"), Utils.parseTimeStamp("2017-01-01"));
    assertEquals(Utils.parseTimeStamp("thu, Feb 02 00:00:00 2017"), Utils.parseTimeStamp("2017-02-02"));
    assertEquals(Utils.parseTimeStamp("wed, Mar 01 00:00:00 2017"), Utils.parseTimeStamp("2017-03-01"));
    assertEquals(Utils.parseTimeStamp("sat, Apr 01 00:00:00 2017"), Utils.parseTimeStamp("2017-04-01"));
    assertEquals(Utils.parseTimeStamp("mon, May 01 00:00:00 2017"), Utils.parseTimeStamp("2017-05-01"));
    assertEquals(Utils.parseTimeStamp("thu, Jun 01 00:00:00 2017"), Utils.parseTimeStamp("2017-06-01"));
    assertEquals(Utils.parseTimeStamp("tue, Jul 04 00:00:00 2017"), Utils.parseTimeStamp("2017-07-04"));
    assertEquals(Utils.parseTimeStamp("fri, Aug 04 00:00:00 2017"), Utils.parseTimeStamp("2017-08-04"));
    assertEquals(Utils.parseTimeStamp("fri, Sep 01 00:00:00 2017"), Utils.parseTimeStamp("2017-09-01"));
    assertEquals(Utils.parseTimeStamp("sun, Oct 01 00:00:00 2017"), Utils.parseTimeStamp("2017-10-01"));
    assertEquals(Utils.parseTimeStamp("wed, Nov 01 00:00:00 2017"), Utils.parseTimeStamp("2017-11-01"));
    assertEquals(Utils.parseTimeStamp("fri, Dec 01 00:00:00 2017"), Utils.parseTimeStamp("2017-12-01"));

    assertEquals(Utils.parseTimeStamp("Sun, jan 01 00:00:00 2017"), Utils.parseTimeStamp("2017-01-01"));
    assertEquals(Utils.parseTimeStamp("Thu, feb 02 00:00:00 2017"), Utils.parseTimeStamp("2017-02-02"));
    assertEquals(Utils.parseTimeStamp("Wed, mar 01 00:00:00 2017"), Utils.parseTimeStamp("2017-03-01"));
    assertEquals(Utils.parseTimeStamp("Sat, apr 01 00:00:00 2017"), Utils.parseTimeStamp("2017-04-01"));
    assertEquals(Utils.parseTimeStamp("Mon, may 01 00:00:00 2017"), Utils.parseTimeStamp("2017-05-01"));
    assertEquals(Utils.parseTimeStamp("Thu, jun 01 00:00:00 2017"), Utils.parseTimeStamp("2017-06-01"));
    assertEquals(Utils.parseTimeStamp("Tue, jul 04 00:00:00 2017"), Utils.parseTimeStamp("2017-07-04"));
    assertEquals(Utils.parseTimeStamp("Fri, aug 04 00:00:00 2017"), Utils.parseTimeStamp("2017-08-04"));
    assertEquals(Utils.parseTimeStamp("Fri, sep 01 00:00:00 2017"), Utils.parseTimeStamp("2017-09-01"));
    assertEquals(Utils.parseTimeStamp("Sun, oct 01 00:00:00 2017"), Utils.parseTimeStamp("2017-10-01"));
    assertEquals(Utils.parseTimeStamp("Wed, nov 01 00:00:00 2017"), Utils.parseTimeStamp("2017-11-01"));
    assertEquals(Utils.parseTimeStamp("Fri, dec 01 00:00:00 2017"), Utils.parseTimeStamp("2017-12-01"));

    assertEquals(Utils.parseTimeStamp("20140506"),            Utils.parseTimeStamp("2014-05-06"));
    assertEquals(Utils.parseTimeStamp("20140506"),            Utils.parseTimeStamp("2014-05-06 00"));
    assertEquals(Utils.parseTimeStamp("20140506"),            Utils.parseTimeStamp("2014-05-06 00:00"));
    assertEquals(Utils.parseTimeStamp("20140506"),            Utils.parseTimeStamp("2014-05-06 00:00:00"));
    assertEquals(Utils.parseTimeStamp("20140506"),            Utils.parseTimeStamp("2014-05-06 00:00:00.000"));
  }

  @Test
  public void isTimeStampTests() { 
    assertTrue(Utils.isTimeStamp("2017-12-25"));
    assertTrue(Utils.isTimeStamp("2017-12-25 12"));
    assertTrue(Utils.isTimeStamp("2017-12-25 12:24"));
    assertTrue(Utils.isTimeStamp("2017-12-25 12:24:32"));
    assertTrue(Utils.isTimeStamp("2017-12-25 12:25:36.0"));
    assertTrue(Utils.isTimeStamp("20171225 123242Z"));
    assertTrue(Utils.isTimeStamp("20171225 1232Z"));
    assertTrue(Utils.isTimeStamp("Sat Jan 30 12:23:00 1914"));
    assertTrue(Utils.isTimeStamp("Tue, Oct 20 01:23:41 1915"));
    assertTrue(Utils.isTimeStamp("Sun, Jun 12 01:32:14 1867"));
    assertTrue(Utils.isTimeStamp("Sun, Jun 12 01:32:14 1867"));
    assertTrue(Utils.isTimeStamp("Sun,  Jun 12 01:32:14 1867"));
    assertTrue(Utils.isTimeStamp("Sun, Jun   12   01:32:14   1867"));

    assertFalse(Utils.isTimeStamp("2017-13-25"));
    assertFalse(Utils.isTimeStamp("2017-12-35"));
    assertFalse(Utils.isTimeStamp("2017-12-25 24"));
    assertFalse(Utils.isTimeStamp("2017-12-25 12:62"));
    assertFalse(Utils.isTimeStamp("2017-12-25 12:24:60"));
    assertFalse(Utils.isTimeStamp("2017-12-25 12:25:36.a"));
    assertFalse(Utils.isTimeStamp("20171325 123242Z"));
    assertFalse(Utils.isTimeStamp("20171235 1232Z"));
    assertFalse(Utils.isTimeStamp("Yun Jan 30 12:23 1914"));
    assertFalse(Utils.isTimeStamp("Tue, Dek 20 01:23:41 1915"));
    assertFalse(Utils.isTimeStamp("Sun, Jun 12 01:32:14 186a"));
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * URL Encoding Tests
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void urlEncodingTests() {
    String strs[] = {
      "abc",
      "more,more",
      ",,,,",
      "12340-8fq23oasjdfpoijqweorhih23*&^)(*&(*UO@IJOIJPOFIJ()*&U)98730917UIOJLKJ:LAJF:LKHOIPHQ",
      "\n\n\r\t\r\t\t\t\t\t\t\t\t",
      "\u4521\ua21f",
      "12309812%12312314123abc%1231513232%1231231",
      " \t \t \t \t \t",
      "[ ]\t \t[ ]\u1234"
    };
    for (int i=0;i<strs.length;i++) { assertEquals(strs[i], Utils.decFmURL(Utils.encToURL(strs[i]))); 
    }
  }

  /**
   * =======================================================================================================================
   * =======================================================================================================================
   * Uncategorized Tests
   * =======================================================================================================================
   * =======================================================================================================================
   */
  @Test
  public void levenshteinDistanceTests() {
    assertEquals(Utils.levenshteinDistance("abc",         "abc"),         0);
    assertEquals(Utils.levenshteinDistance("abc",         "def"),         3);
    assertEquals(Utils.levenshteinDistance("aBc",         "abc"),         1);
    assertEquals(Utils.levenshteinDistance("kitten",      "sitting"),     3);
    assertEquals(Utils.levenshteinDistance("san francis", "san Francis"), 1);
  }

  /**
   * Hex character parsing test.
   */
  @Test
  public void parseHexCharsTests() {
    byte bs[]; String bs_str;

    bs = new byte[3]; bs[0] = 0x00; bs[1] = 0x01; bs[2] = 0x02;
    bs_str = "00 01 02";    parseHexCharsTestValidate(bs, bs_str);
    bs_str = "0 1 2";       parseHexCharsTestValidate(bs, bs_str);
    bs_str = "00\t01\t02";  parseHexCharsTestValidate(bs, bs_str);
    bs_str = "00\r01\n02";  parseHexCharsTestValidate(bs, bs_str);
    bs_str = "000102";      parseHexCharsTestValidate(bs, bs_str);
    bs_str = "00    0102";  parseHexCharsTestValidate(bs, bs_str);

    bs = new byte[5]; bs[0] = (byte) 0xff; bs[1] = (byte) 0xab; bs[2] = (byte) 0x99; bs[3] = (byte) 0x45; bs[4] = (byte) 0x76; 
    bs_str = "ffab994576";                 parseHexCharsTestValidate(bs, bs_str);
    bs_str = "  ff\t\tab\r\r99\t\t45\t76"; parseHexCharsTestValidate(bs, bs_str);
    bs_str = "ff ab 99 45 76";             parseHexCharsTestValidate(bs, bs_str);
    bs_str = "ff   ab994576";              parseHexCharsTestValidate(bs, bs_str);
  }
  private void parseHexCharsTestValidate(byte bs[], String bs_str) {
    byte parsed[] = Utils.parseHexChars(bs_str);
    assertEquals(parsed.length, bs.length);
    for (int i=0;i<bs.length;i++) assertEquals(parsed[i], bs[i]);
  }

  /**
   * Excel CSV tests
   */
  @Test
  public void tokenizeExcelLineTests() {
    String examples[]   = { "this,is,a,test",
                            "this,is,a,",
                            "\"this,is,a\"",
                            ",,",
                            "this,\"is,a\",test",
                            "\"\"\"with quotes\"\"\"",
                            "\"\"\"with,quotes\"\"\",\"\"\"with more quotes\"\"\""
                          };
    String valids[][]   = { { "this", "is", "a", "test" },
                            { "this", "is", "a", "" },
                            { "this,is,a" },
                            { "", "", "" },
                            { "this", "is,a", "test" },
                            { "\"with quotes\"" },
                            { "\"with,quotes\"", "\"with more quotes\"" },
                          };

    for (int i=0;i<examples.length;i++) { compareTokenizeExcelResults(Utils.tokenizeExcelLine(examples[i]), valids[i]); }
  }
  private void compareTokenizeExcelResults(String ret[], String valid[]) {
    assertEquals(ret.length, valid.length);
    for (int i=0;i<ret.length;i++) assertEquals(ret[i],valid[i]);
  }

  /**
   *
   */
  @Test
  public void filterMatchesTests() {
    String filter[] =  {     "127.0.0.0/8",
                             "192.168.0.0/16",
                             "10.0.0.0/8",
                             "1.2.3.4",
                             "5.6.7.8",
                             "*.whatever.com",
                             "more.com",
                             "*.match.com" };
    String matches[] = {     "127.0.0.1",
                             "127.1.2.3",
                             "192.168.10.10",
                             "1.2.3.4",
                             "5.6.7.8",
                             "whatever.com",
                             "something.whatever.com",
                             "really.something.whatever.com",
                             "more.com",
                             "match.com",
                             "more.match.com" };
    String non_matches[] = { "20.20.20.20",
                             "evenmore.com" };
    for (int i=0;i<matches.length;    i++) {
      // System.err.println("Checking For Match \"" + matches[i] + "\"");
      assertEquals(Utils.filterMatches(filter,     matches[i]), true);
    }
    for (int i=0;i<non_matches.length;i++) {
      // System.err.println("Checking For Non-Match \"" + non_matches[i] + "\"");
      assertEquals(Utils.filterMatches(filter, non_matches[i]), false);
    }
  }
}


