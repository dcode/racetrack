/* 

Copyright 2017 David Trimm

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package racetrack.transform;

import racetrack.framework.BundlesDT;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Class to test out the MACAddress class.
 */
public class MACAddressTest {
  @Test
  public void testMACAddressOrg() {
    MACAddress mac_addr = MACAddress.getInstance();

    assertEquals(mac_addr.getOrg("00:00:6d:00:00:00"), "CRAY COMMUNICATIONS, LTD.");
    assertEquals(mac_addr.getOrg("00:00:6D:00:00:00"), "CRAY COMMUNICATIONS, LTD.");
    assertEquals(mac_addr.getOrg("00:00:6D:f0:f0:f0"), "CRAY COMMUNICATIONS, LTD.");

    assertEquals(mac_addr.getOrg("00:02:55:01:02:03"), "IBM Corp");
    assertEquals(mac_addr.getOrg("00:02:55:f1:f2:f3"), "IBM Corp");

    assertEquals(mac_addr.getOrg("0f:ff:ff:00:11:22"), BundlesDT.NOTSET);

  }
}
